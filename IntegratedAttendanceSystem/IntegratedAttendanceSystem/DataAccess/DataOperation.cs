﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;


namespace IntegratedAttendanceSystem.DataAccess
{
    public static class DataOperation
    {
        public static DataTable GetDataTableWithParameter(string StoredProcedureName, params SqlParameter[] parameters)
        {
            SqlConnection con = null;
            DataTable dt = new DataTable();

            try
            {
                con = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLDB"].ToString());
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                con.Open();
                SqlCommand cmd1 = new SqlCommand(StoredProcedureName, con);
                cmd1.CommandType = CommandType.StoredProcedure;
                if (parameters != null)
                    parameters.ToList().ForEach(p =>
                      cmd1.Parameters.AddWithValue(p.ParameterName, p.Value)
                    );
                SqlDataAdapter da = new SqlDataAdapter(cmd1);
                da.Fill(dt);
                con.Close();
            }
            catch (Exception)
            {
                throw;

            }
            finally
            {
                con.Close();
            }
            return dt;
        }
        public static DataTable GetDataTableWithoutParameter(string StoredProcedureName)
        {
            SqlConnection con = null;
            DataTable dt = new DataTable();

            try
            {
                con = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLDB"].ToString());

                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                con.Open();
                SqlCommand cmd1 = new SqlCommand(StoredProcedureName, con);
                cmd1.CommandType = CommandType.StoredProcedure;
               
                SqlDataAdapter da = new SqlDataAdapter(cmd1);
                da.Fill(dt);
                con.Close();
            }
            catch
            {
                throw;

            }
            finally
            {
                con.Close();
            }
            return dt;


        }
        public static int InsUpdDel(string StoredProcedureName, params SqlParameter[] parameters)
        {
            int i = -1;
            SqlConnection con = null;
            try
            {

                con = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLDB"].ToString());
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                con.Open();
                SqlCommand cmd = new SqlCommand(StoredProcedureName, con);
                cmd.CommandType = CommandType.StoredProcedure;
                if (parameters != null)
                    parameters.ToList().ForEach(p =>
                      cmd.Parameters.AddWithValue(p.ParameterName, p.Value)
                    );
                i = cmd.ExecuteNonQuery();
                con.Close();
                i = 1;

            }
            catch
            {
                throw;
            }
            finally
            {
                con.Close();
            }
            return i;
        }

        public static SqlDataReader GetDataReaderWithParameter(string StoredProcedureName, params SqlParameter[] parameters)
        {
            SqlConnection con = null;
            SqlDataReader idr = null;
            try
            {

                con = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLDB"].ToString());

                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                con.Open();
                SqlCommand cmd1 = new SqlCommand(StoredProcedureName, con);
                cmd1.CommandType = CommandType.StoredProcedure;
                if (parameters != null)
                    parameters.ToList().ForEach(p =>
                      cmd1.Parameters.AddWithValue(p.ParameterName, p.Value)
                    );
               // SqlDataAdapter da = new SqlDataAdapter(cmd1);
                 idr = cmd1.ExecuteReader();
                con.Close();
            }
            catch
            {
                throw;

            }
            finally
            {
                con.Close();
            }
            return idr;


        }
        public static SqlDataReader GetDataReaderWithoutParameter(string StoredProcedureName)
        {
            SqlConnection con = null;
            SqlDataReader idr = null;
            try
            {

                con = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLDB"].ToString());

                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                con.Open();
                SqlCommand cmd1 = new SqlCommand(StoredProcedureName, con);
                cmd1.CommandType = CommandType.StoredProcedure;
               
                // SqlDataAdapter da = new SqlDataAdapter(cmd1);
                idr = cmd1.ExecuteReader();
                con.Close();
            }
            catch
            {
                throw;

            }
            finally
            {
                con.Close();
            }
            return idr;


        }
    }
}