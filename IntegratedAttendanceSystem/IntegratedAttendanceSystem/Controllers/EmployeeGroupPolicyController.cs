﻿using IntegratedAttendanceSystem.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Mvc;

namespace IntegratedAttendanceSystem.Controllers
{
    public class EmployeeGroupPolicyController : Controller
    {
        /// <summary>
        /// This variable is use to assign the hosted url of web api.
        /// it may vary according to systems.
        /// </summary>
        string APIUrl = ConfigurationManager.AppSettings["APIURL"].ToString();
        // GET: EmployeeGroupPolicy
        public ActionResult EmployeeGroupPolicy()
        {
            try
            {

            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
            }
            return View();
        }
        public ActionResult CreateEmployeeGroupPolicy()
        {
            try
            {

            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
            }
            return View();
        }

        //// Get all company
        public JsonResult ListAllShift()
        {
            EmployeeGroupPolicy jm = new EmployeeGroupPolicy();
            List<Shift_list> lst = new List<Shift_list>();
            try
            {
                string userName = Session["UserName"].ToString();
                string CompanyCode = Session["CompanyCode"].ToString();;
                //jm.LastModifiedBy = userName;
                Shift_list cm = new Shift_list();
                using (var client = new HttpClient())
                {
                    //Passing service base url  
                    client.BaseAddress = new Uri(APIUrl);
                    client.DefaultRequestHeaders.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    //Sending request to find web api REST service resource GetAllEmployeeGroupPolicys using HttpClient  
                    var res = client.PostAsJsonAsync("EmployeeGroupPolicy/GetShiftList", jm).Result;

                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        var lstResponse = res.Content.ReadAsStringAsync().Result;
                        //Deserializing the response recieved from web api and storing into the EmployeeGroupPolicy list  
                        lst = JsonConvert.DeserializeObject<List<Shift_list>>(lstResponse);
                        if (lst.Count > 0)
                        {
                            foreach (Shift_list cms in lst)
                            {
                                cm.ShiftCode = cms.ShiftCode;
                                cm.ShiftName = cms.ShiftName;
                            }
                            return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json("error", JsonRequestBehavior.AllowGet);
                    }
                }
                return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return Json("error", JsonRequestBehavior.AllowGet);
            }
        }


        //// Get all Group
        public JsonResult ListAlGroup()
        {
            List<Group_list> lst = new List<Group_list>();
            EmployeeGroupPolicy jpm = new EmployeeGroupPolicy();
            try
            {
                string userName = Session["UserName"].ToString();
                string CompanyCode = Session["CompanyCode"].ToString();;
                //jpm.LastModifiedBy = userName;
                Group_list gm = new Group_list();
                using (var client = new HttpClient())
                {
                    //Passing service base url  
                    client.BaseAddress = new Uri(APIUrl);
                    client.DefaultRequestHeaders.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    //Sending request to find web api REST service resource GetAllEmployeeGroupPolicys using HttpClient  
                    var res = client.PostAsJsonAsync("EmployeeGroupPolicy/GetGroupList", jpm).Result;

                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        var lstResponse = res.Content.ReadAsStringAsync().Result;
                        //Deserializing the response recieved from web api and storing into the EmployeeGroupPolicy list  
                        lst = JsonConvert.DeserializeObject<List<Group_list>>(lstResponse);
                        if (lst.Count > 0)
                        {
                            foreach (Group_list gms in lst)
                            {
                                gm.GroupCode = gms.GroupName;
                                gm.GroupName = gms.GroupName;

                            }
                            return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json("error", JsonRequestBehavior.AllowGet);
                    }
                }
                return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return Json("error", JsonRequestBehavior.AllowGet);
            }
        }
        //// Get all company
        public JsonResult ListAllCompany()
        {
            EmployeeGroupPolicy jm = new EmployeeGroupPolicy();
            List<Company_list> lst = new List<Company_list>();
            try
            {
                string userName = Session["UserName"].ToString();
                string CompanyCode = Session["CompanyCode"].ToString();;
                //jm.LastModifiedBy = userName;
                Company_list cm = new Company_list();
                using (var client = new HttpClient())
                {
                    //Passing service base url  
                    client.BaseAddress = new Uri(APIUrl);
                    client.DefaultRequestHeaders.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    //Sending request to find web api REST service resource GetAllEmployeeGroupPolicys using HttpClient  
                    var res = client.PostAsJsonAsync("EmployeeGroupPolicy/GetCompanyList", jm).Result;

                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        var lstResponse = res.Content.ReadAsStringAsync().Result;
                        //Deserializing the response recieved from web api and storing into the EmployeeGroupPolicy list  
                        lst = JsonConvert.DeserializeObject<List<Company_list>>(lstResponse);
                        if (lst.Count > 0)
                        {
                            foreach (Company_list cms in lst)
                            {
                                cm.CompanyCode = cms.CompanyCode;
                                cm.CompanyName = cms.CompanyName;
                            }
                            return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json("error", JsonRequestBehavior.AllowGet);
                    }
                }
                return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return Json("error", JsonRequestBehavior.AllowGet);
            }
        }
        //// Get all department
        public JsonResult ListAlDepartment()
        {
            EmployeeGroupPolicy em = new EmployeeGroupPolicy();
            List<Department_list> lst = new List<Department_list>();
            try
            {
                Department_list dm = new Department_list();

                using (var client = new HttpClient())
                {
                    string userName = Session["UserName"].ToString();
                    string CompanyCode = Session["CompanyCode"].ToString();;
                    //em.LastModifiedBy = userName;
                    //Passing service base url  
                    client.BaseAddress = new Uri(APIUrl);
                    client.DefaultRequestHeaders.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    //Sending request to find web api REST service resource GetAllEmployeeGroupPolicys using HttpClient  
                    var res = client.PostAsJsonAsync("EmployeeGroupPolicy/GetDepartmentList", em).Result;

                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        var lstResponse = res.Content.ReadAsStringAsync().Result;
                        //Deserializing the response recieved from web api and storing into the EmployeeGroupPolicy list  
                        lst = JsonConvert.DeserializeObject<List<Department_list>>(lstResponse);
                        if (lst.Count > 0)
                        {
                            foreach (Department_list dms in lst)
                            {
                                dm.DepartmentCode = dms.DepartmentCode;
                                dm.DepartmentName = dms.DepartmentName;
                            }
                            return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json("error", JsonRequestBehavior.AllowGet);
                    }
                }
                return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return Json("error", JsonRequestBehavior.AllowGet);
            }
        }

        ////Get all designation list
        public JsonResult ListAllDesignation()
        {
            EmployeeGroupPolicy jm = new EmployeeGroupPolicy();
            List<Designation_list> lst = new List<Designation_list>();
            try
            {
                Designation_list dm = new Designation_list();

                using (var client = new HttpClient())
                {
                    string userName = Session["UserName"].ToString();
                    string CompanyCode = Session["CompanyCode"].ToString();;
                    //jm.LastModifiedBy = userName;
                    //Passing service base url  
                    client.BaseAddress = new Uri(APIUrl);
                    client.DefaultRequestHeaders.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    //Sending request to find web api REST service resource GetAllEmployeeGroupPolicys using HttpClient  
                    var res = client.PostAsJsonAsync("EmployeeGroupPolicy/GetDesignationList", jm).Result;

                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        var lstResponse = res.Content.ReadAsStringAsync().Result;
                        //Deserializing the response recieved from web api and storing into the EmployeeGroupPolicy list  
                        lst = JsonConvert.DeserializeObject<List<Designation_list>>(lstResponse);
                        if (lst.Count > 0)
                        {
                            foreach (Designation_list dms in lst)
                            {
                                dm.DesignationCode = dms.DesignationCode;
                                dm.DesignationName = dms.DesignationName;

                            }
                            return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json("error", JsonRequestBehavior.AllowGet);
                    }
                }
                return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return Json("error", JsonRequestBehavior.AllowGet);
            }
        }

        ////Get all Category list
        public JsonResult ListAllCategory(string isInsert)
        {
            EmployeeGroupPolicy Em = new EmployeeGroupPolicy();
            List<CatMaster_list> lst = new List<CatMaster_list>();
            try
            {
                CatMaster_list CatM = new CatMaster_list();
                using (var client = new HttpClient())
                {
                    string userName = Session["UserName"].ToString();
                    string CompanyCode = Session["CompanyCode"].ToString();;
                    //jpm.IsInsert = isInsert.Trim();
                    //jpm.LastModifiedBy = userName.Trim();
                    //Passing service base url  
                    client.BaseAddress = new Uri(APIUrl);
                    client.DefaultRequestHeaders.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    //Sending request to find web api REST service resource GetAllEmployeeGroupPolicys using HttpClient  
                    var res = client.PostAsJsonAsync("EmployeeGroupPolicy/GetCategoryList", Em).Result;

                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        var lstResponse = res.Content.ReadAsStringAsync().Result;
                        //Deserializing the response recieved from web api and storing into the EmployeeGroupPolicy list  
                        lst = JsonConvert.DeserializeObject<List<CatMaster_list>>(lstResponse);
                        if (lst.Count > 0)
                        {
                            foreach (CatMaster_list catms in lst)
                            {
                                CatM.CategoryCode = catms.CategoryCode;
                                CatM.CategoryName = catms.CategoryName;
                            }
                            return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json("error", JsonRequestBehavior.AllowGet);
                    }
                }
                return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
            }
        }

        ////Get all Location/Section/Division list
        public JsonResult ListAllLocation(string isInsert)
        {
            EmployeeGroupPolicy Em = new EmployeeGroupPolicy();
            List<Division_list> lst = new List<Division_list>();
            try
            {
                Division_list CatM = new Division_list();
                using (var client = new HttpClient())
                {
                    string userName = Session["UserName"].ToString();
                    string CompanyCode = Session["CompanyCode"].ToString();;
                    //jpm.IsInsert = isInsert.Trim();
                    //jpm.LastModifiedBy = userName.Trim();
                    //Passing service base url  
                    client.BaseAddress = new Uri(APIUrl);
                    client.DefaultRequestHeaders.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    //Sending request to find web api REST service resource GetAllEmployeeGroupPolicys using HttpClient  
                    var res = client.PostAsJsonAsync("EmployeeGroupPolicy/GetLocationList", Em).Result;

                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        var lstResponse = res.Content.ReadAsStringAsync().Result;
                        //Deserializing the response recieved from web api and storing into the EmployeeGroupPolicy list  
                        lst = JsonConvert.DeserializeObject<List<Division_list>>(lstResponse);
                        if (lst.Count > 0)
                        {
                            foreach (Division_list catms in lst)
                            {
                                CatM.DivisionCode = catms.DivisionCode;
                                CatM.DivisionName = catms.DivisionName;
                            }
                            return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json("error", JsonRequestBehavior.AllowGet);
                    }
                }
                return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
            }
        }

        ////Get all Bank list
        public JsonResult ListAllBank(string isInsert)
        {
            EmployeeGroupPolicy Em = new EmployeeGroupPolicy();
            List<Bank_list> lst = new List<Bank_list>();
            try
            {
                Bank_list BnkM = new Bank_list();
                using (var client = new HttpClient())
                {
                    string userName = Session["UserName"].ToString();
                    string CompanyCode = Session["CompanyCode"].ToString();;
                    //jpm.IsInsert = isInsert.Trim();
                    //jpm.LastModifiedBy = userName.Trim();
                    //Passing service base url  
                    client.BaseAddress = new Uri(APIUrl);
                    client.DefaultRequestHeaders.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    //Sending request to find web api REST service resource GetAllEmployeeGroupPolicys using HttpClient  
                    var res = client.PostAsJsonAsync("EmployeeGroupPolicy/GetBankList", Em).Result;

                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        var lstResponse = res.Content.ReadAsStringAsync().Result;
                        //Deserializing the response recieved from web api and storing into the EmployeeGroupPolicy list  
                        lst = JsonConvert.DeserializeObject<List<Bank_list>>(lstResponse);
                        if (lst.Count > 0)
                        {
                            foreach (Bank_list BnkMs in lst)
                            {
                                BnkM.BankCode = BnkMs.BankCode;
                                BnkM.BankName = BnkMs.BankName;
                            }
                            return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json("error", JsonRequestBehavior.AllowGet);
                    }
                }
                return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
            }
        }

        ////Get all HOD list
        public JsonResult ListAllHOD(string isInsert)
        {
            EmployeeGroupPolicy Em = new EmployeeGroupPolicy();
            List<HOD_list> lst = new List<HOD_list>();
            try
            {
                HOD_list BnkM = new HOD_list();
                using (var client = new HttpClient())
                {
                    string userName = Session["UserName"].ToString();
                    string CompanyCode = Session["CompanyCode"].ToString();;
                    //jpm.IsInsert = isInsert.Trim();
                    //jpm.LastModifiedBy = userName.Trim();
                    //Passing service base url  
                    client.BaseAddress = new Uri(APIUrl);
                    client.DefaultRequestHeaders.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    //Sending request to find web api REST service resource GetAllEmployeeGroupPolicys using HttpClient  
                    var res = client.PostAsJsonAsync("EmployeeGroupPolicy/GetHODList", Em).Result;

                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        var lstResponse = res.Content.ReadAsStringAsync().Result;
                        //Deserializing the response recieved from web api and storing into the EmployeeGroupPolicy list  
                        lst = JsonConvert.DeserializeObject<List<HOD_list>>(lstResponse);
                        if (lst.Count > 0)
                        {
                            foreach (HOD_list BnkMs in lst)
                            {
                                BnkM.HODCode = BnkMs.HODCode;
                                BnkM.HODName = BnkMs.HODName;
                            }
                            return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json("error", JsonRequestBehavior.AllowGet);
                    }
                }
                return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
            }
        }

        ////Get all EmployeeGroupPolicy Group list
        public JsonResult ListAllGroup(string isInsert)
        {
            EmployeeGroupPolicy Em = new EmployeeGroupPolicy();
            List<EmployeeGroup_list> lst = new List<EmployeeGroup_list>();
            try
            {
                EmployeeGroup_list BnkM = new EmployeeGroup_list();
                using (var client = new HttpClient())
                {
                    string userName = Session["UserName"].ToString();
                    string CompanyCode = Session["CompanyCode"].ToString();
                    //jpm.IsInsert = isInsert.Trim();
                    //jpm.LastModifiedBy = userName.Trim();
                    //Passing service base url  
                    client.BaseAddress = new Uri(APIUrl);
                    client.DefaultRequestHeaders.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    //Sending request to find web api REST service resource GetAllEmployeeGroupPolicys using HttpClient  
                    var res = client.PostAsJsonAsync("EmployeeGroupPolicy/GetEmployeeGroupPolicyGroupList", Em).Result;

                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        var lstResponse = res.Content.ReadAsStringAsync().Result;
                        //Deserializing the response recieved from web api and storing into the EmployeeGroupPolicy list  
                        lst = JsonConvert.DeserializeObject<List<EmployeeGroup_list>>(lstResponse);
                        if (lst.Count > 0)
                        {
                            foreach (EmployeeGroup_list BnkMs in lst)
                            {
                                BnkM.GroupID = BnkMs.GroupID;
                                BnkM.GroupName = BnkMs.GroupName;
                            }
                            return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json("error", JsonRequestBehavior.AllowGet);
                    }
                }
                return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
            }
        }

        ////Get all EmployeeGroupPolicy list
        [HttpPost]
        public ActionResult GetAllEmployeeGroupPolicy()
        {
            EmployeeGroupPolicy em = new EmployeeGroupPolicy();
            List<EmployeeGroupPolicy> lst = new List<EmployeeGroupPolicy>();
            try
            {
                string userName = Session["UserName"].ToString();
                string CompanyCode = Session["CompanyCode"].ToString();;
                //em.LastModifiedBy = userName;
                string draw = Request.Form.GetValues("draw")[0];
                string order = Request.Form.GetValues("order[0][column]")[0];
                string orderDir = Request.Form.GetValues("order[0][dir]")[0];
                int startRec = Convert.ToInt32(Request.Form.GetValues("start")[0]);
                int pageSize = Convert.ToInt32(Request.Form.GetValues("length")[0]);
                string search = Request.Form.GetValues("search[value]").FirstOrDefault();
                ///

                ///
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIUrl);

                    var res = client.PostAsJsonAsync("EmployeeGroupPolicy/GetAllEmployeeGroupPolicy", em).Result;

                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        var lstResponse = res.Content.ReadAsStringAsync().Result;
                        //Deserializing the response recieved from web api and storing into the EmployeeGroupPolicy list  
                        lst = JsonConvert.DeserializeObject<List<EmployeeGroupPolicy>>(lstResponse);

                        if (lst.Count > 0)
                        {
                            foreach (EmployeeGroupPolicy jms in lst)
                            {
                                em.sGroupID = jms.sGroupID;
                                em.sGroupName = jms.sGroupName;
                                em.sCOMPANYCODE = jms.sCOMPANYCODE;
                                em.sISROUNDTHECLOCKWORK = jms.sISROUNDTHECLOCKWORK;
                                em.sPERMISLATEARRIVAL = jms.sPERMISLATEARRIVAL;
                                em.sPERMISEARLYDEPRT = jms.sPERMISEARLYDEPRT;
                                em.sMAXDAYMIN = jms.sMAXDAYMIN;
                                em.sISOUTWORK = jms.sISOUTWORK;
                                em.sISTIMELOSSALLOWED = jms.sISTIMELOSSALLOWED;
                                em.sISHALFDAY = jms.sISHALFDAY;
                                em.sISSHORT = jms.sISSHORT;
                                em.sTIME = jms.sTIME;
                                em.sSHORT = jms.sSHORT;
                                em.sHALF = jms.sHALF;
                                em.sHLFAfter = jms.sHLFAfter;
                                em.sHLFBefore = jms.sHLFBefore;
                            }
                        }
                        if (Request.IsAjaxRequest())
                        {
                            ///

                            // Get Exp Records.   
                            var allJobdes = lst;
                            //
                            IEnumerable<EmployeeGroupPolicy> filteredjob = lst;

                            long TotalRecordsCount = lst.Count();

                            #region filters  

                            if (!string.IsNullOrEmpty(search))
                            {
                                filteredjob = allJobdes.Where(
                                u => u.sGroupID.ToUpper().Contains(search.ToUpper())
                                || u.sGroupName.ToUpper().Contains(search.ToUpper())
                                || u.sCOMPANYCODE.ToUpper().Contains(search.ToUpper())
                                || u.sISROUNDTHECLOCKWORK.ToUpper().Contains(search.ToUpper())
                                || u.sPERMISLATEARRIVAL.ToString().ToUpper().Contains(search.ToUpper())
                                || u.sPERMISEARLYDEPRT.ToString().ToUpper().Contains(search.ToUpper())
                                || u.sMAXDAYMIN.ToUpper().Contains(search.ToUpper())
                                || u.sISOUTWORK.ToUpper().Contains(search.ToUpper())
                                || u.sISTIMELOSSALLOWED.ToUpper().Contains(search.ToUpper())
                                || u.sISHALFDAY.ToUpper().Contains(search.ToUpper())
                                || u.sISSHORT.ToUpper().Contains(search.ToUpper())
                                || u.sTIME.ToString().ToUpper().Contains(search.ToUpper())
                                || u.sSHORT.ToString().ToUpper().Contains(search.ToUpper())
                                || u.sHALF.ToString().ToUpper().Contains(search.ToUpper())
                                || u.sHLFAfter.ToString().ToUpper().Contains(search.ToUpper())
                                || u.sHLFBefore.ToString().ToUpper().Contains(search.ToUpper())
                                ).ToList();
                            }
                            #endregion
                            long FilteredRecordCount = lst.Count();

                            #region Sorting  

                            // Sorting     
                            switch (order)
                            {
                                case "0":
                                    filteredjob = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredjob.OrderByDescending(p => p.sGroupID) : filteredjob.OrderBy(p => p.sGroupID);
                                    break;                                
                                case "1":
                                    filteredjob = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredjob.OrderByDescending(p => p.sGroupID) : filteredjob.OrderBy(p => p.sGroupID);
                                    break;
                                case "2":
                                    filteredjob = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredjob.OrderByDescending(p => p.sGroupID) : filteredjob.OrderBy(p => p.sGroupID);
                                    break;
                                case "3":
                                    filteredjob = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredjob.OrderByDescending(p => p.sGroupName) : filteredjob.OrderBy(p => p.sGroupName);
                                    break;
                                case "4":
                                    filteredjob = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredjob.OrderByDescending(p => p.sCOMPANYCODE) : filteredjob.OrderBy(p => p.sCOMPANYCODE);
                                    break;
                                case "5":
                                    filteredjob = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredjob.OrderByDescending(p => p.sPERMISLATEARRIVAL) : filteredjob.OrderBy(p => p.sPERMISLATEARRIVAL);
                                    break;
                                case "6":
                                    filteredjob = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredjob.OrderByDescending(p => p.sPERMISEARLYDEPRT) : filteredjob.OrderBy(p => p.sPERMISEARLYDEPRT);
                                    break;
                                case "7":
                                    filteredjob = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredjob.OrderByDescending(p => p.sMAXDAYMIN) : filteredjob.OrderBy(p => p.sMAXDAYMIN);
                                    break;
                                case "8":
                                    filteredjob = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredjob.OrderByDescending(p => p.sISOUTWORK) : filteredjob.OrderBy(p => p.sISOUTWORK);
                                    break;
                                case "9":
                                    filteredjob = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredjob.OrderByDescending(p => p.sISTIMELOSSALLOWED) : filteredjob.OrderBy(p => p.sISTIMELOSSALLOWED);
                                    break;
                                case "10":
                                    filteredjob = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredjob.OrderByDescending(p => p.sISHALFDAY) : filteredjob.OrderBy(p => p.sISHALFDAY);
                                    break;
                                case "11":
                                    filteredjob = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredjob.OrderByDescending(p => p.sISSHORT) : filteredjob.OrderBy(p => p.sISSHORT);
                                    break;
                                case "12":
                                    filteredjob = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredjob.OrderByDescending(p => p.sTIME) : filteredjob.OrderBy(p => p.sTIME);
                                    break;
                                case "13":
                                    filteredjob = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredjob.OrderByDescending(p => p.sSHORT) : filteredjob.OrderBy(p => p.sSHORT);
                                    break;
                                case "14":
                                    filteredjob = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredjob.OrderByDescending(p => p.sHALF) : filteredjob.OrderBy(p => p.sHALF);
                                    break;
                                case "15":
                                    filteredjob = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredjob.OrderByDescending(p => p.sHLFAfter) : filteredjob.OrderBy(p => p.sHLFAfter);
                                    break;
                                case "16":
                                    filteredjob = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredjob.OrderByDescending(p => p.sHLFBefore) : filteredjob.OrderBy(p => p.sHLFBefore);
                                    break;
                                default:
                                    filteredjob = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredjob.OrderByDescending(p => p.sGroupID) : filteredjob.OrderBy(p => p.sGroupID);
                                    break;
                            }
                            #endregion

                            var listjob = filteredjob.Skip(startRec).Take(pageSize).ToList()
                                .Select(d => new EmployeeGroupPolicy()
                                {
                                    sGroupID = d.sGroupID,
                                    sGroupName = d.sGroupName,
                                    sCOMPANYCODE = d.sCOMPANYCODE,
                                    sISROUNDTHECLOCKWORK = d.sISROUNDTHECLOCKWORK,
                                    sPERMISLATEARRIVAL = d.sPERMISLATEARRIVAL,
                                    sPERMISEARLYDEPRT = d.sPERMISEARLYDEPRT,
                                    sMAXDAYMIN = d.sMAXDAYMIN,
                                    sISOUTWORK = d.sISOUTWORK,
                                    sISTIMELOSSALLOWED = d.sISTIMELOSSALLOWED,
                                    sISHALFDAY = d.sISHALFDAY,
                                    sISSHORT = d.sISSHORT,
                                    sTIME = d.sTIME,
                                    sSHORT = d.sSHORT,
                                    sHALF = d.sHALF,
                                    sHLFAfter = d.sHLFAfter,
                                    sHLFBefore = d.sHLFBefore,

                                }).ToList();

                            if (listjob == null)
                                listjob = new List<EmployeeGroupPolicy>();

                            return this.Json(new
                            {
                                draw = Convert.ToInt32(draw),
                                recordsTotal = TotalRecordsCount,
                                recordsFiltered = FilteredRecordCount,
                                data = listjob
                            }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json("error", JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return Json("error", JsonRequestBehavior.AllowGet);
            }
            return View();
        }

        ////Add EmployeeGroupPolicy list      
        public JsonResult Add(EmployeeGroupPolicy EmpGP)
        {
            try
            {
                EmpGP.LastModifiedBy = Session["UserName"].ToString();
                EmpGP.sCOMPANYCODE = Session["CompanyCode"].ToString();
                EmpGP.LoginTerminalNameIP = Session["LoginIP"].ToString() + " " + Session["LoginTerminalName"].ToString();

                using (var client = new HttpClient())
                {
                    //Passing service base url  
                    client.BaseAddress = new Uri(APIUrl);

                    var res = client.PostAsJsonAsync("EmployeeGroupPolicy/AddEmployeeGroupPolicy", EmpGP).Result;

                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        return Json("success", JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json("error", JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                ViewBag.Message = "Error occured, contact to administrator";
                return Json("error", JsonRequestBehavior.AllowGet);
            }
        }


        // get jd as per selection of company and department
        //[HttpGet]
        //public JsonResult BindJD(string compcode, string depcode,string isInsert)
        //{
        //    EmployeeGroupPolicy jpm = new EmployeeGroupPolicy();
        //    List<JDMaster_list> lst = new List<JDMaster_list>();
        //    try
        //    {
        //        JDMaster_list JML = new JDMaster_list();
        //        using (var client = new HttpClient())
        //        {

        //            string userName = Session["UserName"].ToString();

        //            jpm.CompCode = compcode.Trim();
        //            jpm.DepCode = depcode.Trim();
        //            jpm.LastModifiedBy = userName.Trim();
        //            jpm.IsInsert = isInsert.Trim();
        //            //Passing service base url  
        //            client.BaseAddress = new Uri(APIUrl);
        //            client.DefaultRequestHeaders.Clear();
        //            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        //            //Sending request to find web api REST service resource GetAllEmployeeGroupPolicys using HttpClient  
        //            var res = client.PostAsJsonAsync("EmployeeGroupPolicy/GetJDList", jpm).Result;

        //            //Checking the response is successful or not which is sent using HttpClient  
        //            if (res.IsSuccessStatusCode)
        //            {
        //                var lstResponse = res.Content.ReadAsStringAsync().Result;
        //                //Deserializing the response recieved from web api and storing into the EmployeeGroupPolicy list  
        //                lst = JsonConvert.DeserializeObject<List<JDMaster_list>>(lstResponse);
        //                if (lst.Count > 0)
        //                {
        //                    foreach (JDMaster_list dms in lst)
        //                    {
        //                        JML.JobMasterId = dms.JobMasterId;
        //                        JML.JobMasterName = dms.JobMasterName;
        //                    }
        //                    return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
        //                }
        //            }
        //            else
        //            {
        //                return Json("error", JsonRequestBehavior.AllowGet);
        //            }
        //        }
        //        return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception ex)
        //    {
        //        ExceptionLogging.SendErrorToText(ex);
        //        return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
        //    }

        //}
        //get age as per the selection of JD
        //public JsonResult BindAge(string jobID)
        //{
        //    EmployeeGroupPolicy jpm = new EmployeeGroupPolicy();
        //    List<JobPosAgeMaster_list> lst = new List<JobPosAgeMaster_list>();
        //    try
        //    {
        //        JobPosAgeMaster_list JML = new JobPosAgeMaster_list();
        //        using (var client = new HttpClient())
        //        {

        //            string userName = Session["UserName"].ToString();

        //            //jpm.JobId = jobID.Trim();
        //            //jpm.LastModifiedBy = userName.Trim();
        //            //Passing service base url  
        //            client.BaseAddress = new Uri(APIUrl);
        //            client.DefaultRequestHeaders.Clear();
        //            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        //            //Sending request to find web api REST service resource GetAllEmployeeGroupPolicys using HttpClient  
        //            var res = client.PostAsJsonAsync("EmployeeGroupPolicy/GetAgeList", jpm).Result;

        //            //Checking the response is successful or not which is sent using HttpClient  
        //            if (res.IsSuccessStatusCode)
        //            {
        //                var lstResponse = res.Content.ReadAsStringAsync().Result;
        //                //Deserializing the response recieved from web api and storing into the EmployeeGroupPolicy list  
        //                lst = JsonConvert.DeserializeObject<List<JobPosAgeMaster_list>>(lstResponse);
        //                if (lst.Count > 0)
        //                {
        //                    foreach (JobPosAgeMaster_list dms in lst)
        //                    {
        //                        JML.AgeId = dms.AgeId;
        //                        JML.AgeDesc = dms.AgeDesc;
        //                    }
        //                    return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
        //                }
        //            }
        //            else
        //            {
        //                return Json("error", JsonRequestBehavior.AllowGet);
        //            }
        //        }
        //        return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception ex)
        //    {
        //        ExceptionLogging.SendErrorToText(ex);
        //        return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
        //    }
        //}
        //get interviewmode list
        //public JsonResult BindIterviewMode(string isInsert)
        //{
        //    EmployeeGroupPolicy jpm = new EmployeeGroupPolicy();
        //    List<JobPosIMMaster_list> lst = new List<JobPosIMMaster_list>();
        //    try
        //    {
        //        JobPosIMMaster_list JML = new JobPosIMMaster_list();
        //        using (var client = new HttpClient())
        //        {

        //            string userName = Session["UserName"].ToString();

        //            jpm.IsInsert = isInsert.Trim();
        //            jpm.LastModifiedBy = userName.Trim();
        //            //Passing service base url  
        //            client.BaseAddress = new Uri(APIUrl);
        //            client.DefaultRequestHeaders.Clear();
        //            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        //            //Sending request to find web api REST service resource GetAllEmployeeGroupPolicys using HttpClient  
        //            var res = client.PostAsJsonAsync("EmployeeGroupPolicy/GetInterviewMode", jpm).Result;

        //            //Checking the response is successful or not which is sent using HttpClient  
        //            if (res.IsSuccessStatusCode)
        //            {
        //                var lstResponse = res.Content.ReadAsStringAsync().Result;
        //                //Deserializing the response recieved from web api and storing into the EmployeeGroupPolicy list  
        //                lst = JsonConvert.DeserializeObject<List<JobPosIMMaster_list>>(lstResponse);
        //                if (lst.Count > 0)
        //                {
        //                    foreach (JobPosIMMaster_list dms in lst)
        //                    {
        //                        JML.IMCode = dms.IMCode;
        //                        JML.IMDesc = dms.IMDesc;
        //                    }
        //                    return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
        //                }
        //            }
        //            else
        //            {
        //                return Json("error", JsonRequestBehavior.AllowGet);
        //            }
        //        }
        //        return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception ex)
        //    {
        //        ExceptionLogging.SendErrorToText(ex);
        //        return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
        //    }

        //}
        //getqualification list
        //public JsonResult BindQualification(string isInsert)
        //{
        //    EmployeeGroupPolicy jpm = new EmployeeGroupPolicy();
        //    List<JobPosQuaification_list> lst = new List<JobPosQuaification_list>();
        //    try
        //    {
        //        JobPosQuaification_list JML = new JobPosQuaification_list();
        //        using (var client = new HttpClient())
        //        {
        //            string userName = Session["UserName"].ToString();

        //            jpm.IsInsert = isInsert.Trim();
        //            jpm.LastModifiedBy = userName.Trim();
        //            //Passing service base url  
        //            client.BaseAddress = new Uri(APIUrl);
        //            client.DefaultRequestHeaders.Clear();
        //            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        //            //Sending request to find web api REST service resource GetAllEmployeeGroupPolicys using HttpClient  
        //            var res = client.PostAsJsonAsync("EmployeeGroupPolicy/GetQualification", jpm).Result;

        //            //Checking the response is successful or not which is sent using HttpClient  
        //            if (res.IsSuccessStatusCode)
        //            {
        //                var lstResponse = res.Content.ReadAsStringAsync().Result;
        //                //Deserializing the response recieved from web api and storing into the EmployeeGroupPolicy list  
        //                lst = JsonConvert.DeserializeObject<List<JobPosQuaification_list>>(lstResponse);
        //                if (lst.Count > 0)
        //                {
        //                    foreach (JobPosQuaification_list dms in lst)
        //                    {
        //                        JML.QID = dms.QID;
        //                        JML.QDescc = dms.QDescc;
        //                    }
        //                    return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
        //                }
        //            }
        //            else
        //            {
        //                return Json("error", JsonRequestBehavior.AllowGet);
        //            }
        //        }
        //        return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception ex)
        //    {
        //        ExceptionLogging.SendErrorToText(ex);
        //        return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
        //    }

        //}
        //get language list
        //public JsonResult BindLanguage(string isInsert)
        //{
        //    EmployeeGroupPolicy jpm = new EmployeeGroupPolicy();
        //    List<JobPosLanguage_list> lst = new List<JobPosLanguage_list>();
        //    try
        //    {
        //        JobPosLanguage_list JML = new JobPosLanguage_list();
        //        using (var client = new HttpClient())
        //        {
        //            string userName = Session["UserName"].ToString();

        //            jpm.IsInsert = isInsert.Trim();
        //            jpm.LastModifiedBy = userName.Trim();
        //            //Passing service base url  
        //            client.BaseAddress = new Uri(APIUrl);
        //            client.DefaultRequestHeaders.Clear();
        //            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        //            //Sending request to find web api REST service resource GetAllEmployeeGroupPolicys using HttpClient  
        //            var res = client.PostAsJsonAsync("EmployeeGroupPolicy/GetLanguage", jpm).Result;

        //            //Checking the response is successful or not which is sent using HttpClient  
        //            if (res.IsSuccessStatusCode)
        //            {
        //                var lstResponse = res.Content.ReadAsStringAsync().Result;
        //                //Deserializing the response recieved from web api and storing into the EmployeeGroupPolicy list  
        //                lst = JsonConvert.DeserializeObject<List<JobPosLanguage_list>>(lstResponse);
        //                if (lst.Count > 0)
        //                {
        //                    foreach (JobPosLanguage_list dms in lst)
        //                    {
        //                        JML.LangCode = dms.LangCode;
        //                        JML.LangDesc = dms.LangDesc;
        //                    }
        //                    return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
        //                }
        //            }
        //            else
        //            {
        //                return Json("error", JsonRequestBehavior.AllowGet);
        //            }
        //        }
        //        return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception ex)
        //    {
        //        ExceptionLogging.SendErrorToText(ex);
        //        return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
        //    }

        //}

        //get certification list
        //public JsonResult BindCertification(string isInsert)
        //{
        //    EmployeeGroupPolicy em = new EmployeeGroupPolicy();
        //    List<JobPosCertificate_list> lst = new List<JobPosCertificate_list>();
        //    try
        //    {
        //        JobPosCertificate_list JML = new JobPosCertificate_list();
        //        using (var client = new HttpClient())
        //        {
        //            string userName = Session["UserName"].ToString();

        //            em.IsInsert = isInsert.Trim();
        //            em.LastModifiedBy = userName.Trim();
        //            //Passing service base url  
        //            client.BaseAddress = new Uri(APIUrl);
        //            client.DefaultRequestHeaders.Clear();
        //            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        //            //Sending request to find web api REST service resource GetAllEmployeeGroupPolicys using HttpClient  
        //            var res = client.PostAsJsonAsync("EmployeeGroupPolicy/GetCertification", em).Result;

        //            //Checking the response is successful or not which is sent using HttpClient  
        //            if (res.IsSuccessStatusCode)
        //            {
        //                var lstResponse = res.Content.ReadAsStringAsync().Result;
        //                //Deserializing the response recieved from web api and storing into the EmployeeGroupPolicy list  
        //                lst = JsonConvert.DeserializeObject<List<JobPosCertificate_list>>(lstResponse);
        //                if (lst.Count > 0)
        //                {
        //                    foreach (JobPosCertificate_list dms in lst)
        //                    {
        //                        JML.CertCode = dms.CertCode;
        //                        JML.CertName = dms.CertName;
        //                    }
        //                    return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
        //                }
        //            }
        //            else
        //            {
        //                return Json("error", JsonRequestBehavior.AllowGet);
        //            }
        //        }
        //        return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception ex)
        //    {
        //        ExceptionLogging.SendErrorToText(ex);
        //        return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
        //    }

        //}
        //get Job category list


        //get specialization field as per the selection of Qualification
        //public JsonResult BindSpecializationField(string appendStr)
        //{
        //    EmployeeGroupPolicy jpm = new EmployeeGroupPolicy();
        //    List<JobPosSpeclzn_list> lst = new List<JobPosSpeclzn_list>();
        //    try
        //    {
        //        JobPosSpeclzn_list JML = new JobPosSpeclzn_list();
        //        using (var client = new HttpClient())
        //        {
        //            string userName = Session["UserName"].ToString();
        //            jpm.QID = appendStr.Trim();
        //            jpm.LastModifiedBy = userName.Trim();
        //            //Passing service base url  
        //            client.BaseAddress = new Uri(APIUrl);
        //            client.DefaultRequestHeaders.Clear();
        //            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        //            //Sending request to find web api REST service resource GetAllEmployeeGroupPolicys using HttpClient  
        //            var res = client.PostAsJsonAsync("EmployeeGroupPolicy/GetSpecializationField", jpm).Result;

        //            //Checking the response is successful or not which is sent using HttpClient  
        //            if (res.IsSuccessStatusCode)
        //            {
        //                var lstResponse = res.Content.ReadAsStringAsync().Result;
        //                //Deserializing the response recieved from web api and storing into the EmployeeGroupPolicy list  
        //                lst = JsonConvert.DeserializeObject<List<JobPosSpeclzn_list>>(lstResponse);
        //                if (lst.Count > 0)
        //                {
        //                    foreach (JobPosSpeclzn_list dms in lst)
        //                    {
        //                        JML.SpecLznID = dms.SpecLznID;
        //                        JML.SpecLznDesc = dms.SpecLznDesc;
        //                    }
        //                    return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
        //                }
        //            }
        //            else
        //            {
        //                return Json("error", JsonRequestBehavior.AllowGet);
        //            }
        //        }
        //        return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception ex)
        //    {
        //        ExceptionLogging.SendErrorToText(ex);
        //        return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
        //    }

        //}
        ////get primary skill set list list as per the selection of JD
        //public JsonResult BindPrimarySillset(string jobID)
        //{
        //    EmployeeGroupPolicy jpm = new EmployeeGroupPolicy();
        //    List<JobPosPrimarySkill_list> lst = new List<JobPosPrimarySkill_list>();
        //    try
        //    {
        //        JobPosPrimarySkill_list JML = new JobPosPrimarySkill_list();
        //        using (var client = new HttpClient())
        //        {

        //            string userName = Session["UserName"].ToString();

        //            jpm.JobId = jobID.Trim();
        //            jpm.LastModifiedBy = userName.Trim();
        //            //Passing service base url  
        //            client.BaseAddress = new Uri(APIUrl);
        //            client.DefaultRequestHeaders.Clear();
        //            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        //            //Sending request to find web api REST service resource GetAllEmployeeGroupPolicys using HttpClient  
        //            var res = client.PostAsJsonAsync("EmployeeGroupPolicy/GetPrimarySkill", jpm).Result;

        //            //Checking the response is successful or not which is sent using HttpClient  
        //            if (res.IsSuccessStatusCode)
        //            {
        //                var lstResponse = res.Content.ReadAsStringAsync().Result;
        //                //Deserializing the response recieved from web api and storing into the EmployeeGroupPolicy list  
        //                lst = JsonConvert.DeserializeObject<List<JobPosPrimarySkill_list>>(lstResponse);
        //                if (lst.Count > 0)
        //                {
        //                    foreach (JobPosPrimarySkill_list dms in lst)
        //                    {
        //                        JML.pskillID = dms.pskillID;
        //                        JML.pskillName = dms.pskillName;
        //                    }
        //                    return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
        //                }
        //            }
        //            else
        //            {
        //                return Json("error", JsonRequestBehavior.AllowGet);
        //            }
        //        }
        //        return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception ex)
        //    {
        //        ExceptionLogging.SendErrorToText(ex);
        //        return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
        //    }

        //}
        ////get secondary skill set as per the selection of JD
        //public JsonResult BindSecondarySillset(string jobID)
        //{
        //    EmployeeGroupPolicy jpm = new EmployeeGroupPolicy();
        //    List<JobPosSecondarySkill_list> lst = new List<JobPosSecondarySkill_list>();
        //    try
        //    {
        //        JobPosSecondarySkill_list JML = new JobPosSecondarySkill_list();
        //        using (var client = new HttpClient())
        //        {

        //            string userName = Session["UserName"].ToString();

        //            jpm.JobId = jobID.Trim();
        //            jpm.LastModifiedBy = userName.Trim();
        //            //Passing service base url  
        //            client.BaseAddress = new Uri(APIUrl);
        //            client.DefaultRequestHeaders.Clear();
        //            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        //            //Sending request to find web api REST service resource GetAllEmployeeGroupPolicys using HttpClient  
        //            var res = client.PostAsJsonAsync("EmployeeGroupPolicy/GetSecondarySkill", jpm).Result;

        //            //Checking the response is successful or not which is sent using HttpClient  
        //            if (res.IsSuccessStatusCode)
        //            {
        //                var lstResponse = res.Content.ReadAsStringAsync().Result;
        //                //Deserializing the response recieved from web api and storing into the EmployeeGroupPolicy list  
        //                lst = JsonConvert.DeserializeObject<List<JobPosSecondarySkill_list>>(lstResponse);
        //                if (lst.Count > 0)
        //                {
        //                    foreach (JobPosSecondarySkill_list dms in lst)
        //                    {
        //                        JML.sskillID = dms.sskillID;
        //                        JML.sskillName = dms.sskillName;
        //                    }
        //                    return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
        //                }
        //            }
        //            else
        //            {
        //                return Json("error", JsonRequestBehavior.AllowGet);
        //            }
        //        }
        //        return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception ex)
        //    {
        //        ExceptionLogging.SendErrorToText(ex);
        //        return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
        //    }

        //}
        ////get  total experience as per the selection of JD
        //public JsonResult BindExpList(string jobID)
        //{
        //    EmployeeGroupPolicy jpm = new EmployeeGroupPolicy();
        //    List<JobPosTotalExp_list> lst = new List<JobPosTotalExp_list>();
        //    try
        //    {
        //        JobPosTotalExp_list JML = new JobPosTotalExp_list();
        //        using (var client = new HttpClient())
        //        {

        //            string userName = Session["UserName"].ToString();

        //            jpm.JobId = jobID.Trim();
        //            jpm.LastModifiedBy = userName.Trim();
        //            //Passing service base url  
        //            client.BaseAddress = new Uri(APIUrl);
        //            client.DefaultRequestHeaders.Clear();
        //            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        //            //Sending request to find web api REST service resource GetAllEmployeeGroupPolicys using HttpClient  
        //            var res = client.PostAsJsonAsync("EmployeeGroupPolicy/GetTotalExp", jpm).Result;

        //            //Checking the response is successful or not which is sent using HttpClient  
        //            if (res.IsSuccessStatusCode)
        //            {
        //                var lstResponse = res.Content.ReadAsStringAsync().Result;
        //                //Deserializing the response recieved from web api and storing into the EmployeeGroupPolicy list  
        //                lst = JsonConvert.DeserializeObject<List<JobPosTotalExp_list>>(lstResponse);
        //                if (lst.Count > 0)
        //                {
        //                    foreach (JobPosTotalExp_list dms in lst)
        //                    {
        //                        JML.ExpId = dms.ExpId;
        //                        JML.ExpDesc = dms.ExpDesc;
        //                    }
        //                    return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
        //                }
        //            }
        //            else
        //            {
        //                return Json("error", JsonRequestBehavior.AllowGet);
        //            }
        //        }
        //        return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception ex)
        //    {
        //        ExceptionLogging.SendErrorToText(ex);
        //        return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
        //    }

        //}

        ////get  relevant  experience as per the selection of JD
        //public JsonResult BindRelvExpList(string jobID)
        //{
        //    EmployeeGroupPolicy jpm = new EmployeeGroupPolicy();
        //    List<JobPosRelv_list> lst = new List<JobPosRelv_list>();
        //    try
        //    {
        //        JobPosRelv_list JML = new JobPosRelv_list();
        //        using (var client = new HttpClient())
        //        {

        //            string userName = Session["UserName"].ToString();

        //            jpm.JobId = jobID.Trim();
        //            jpm.LastModifiedBy = userName.Trim();
        //            //Passing service base url  
        //            client.BaseAddress = new Uri(APIUrl);
        //            client.DefaultRequestHeaders.Clear();
        //            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        //            //Sending request to find web api REST service resource GetAllEmployeeGroupPolicys using HttpClient  
        //            var res = client.PostAsJsonAsync("EmployeeGroupPolicy/GetRelvExp", jpm).Result;

        //            //Checking the response is successful or not which is sent using HttpClient  
        //            if (res.IsSuccessStatusCode)
        //            {
        //                var lstResponse = res.Content.ReadAsStringAsync().Result;
        //                //Deserializing the response recieved from web api and storing into the EmployeeGroupPolicy list  
        //                lst = JsonConvert.DeserializeObject<List<JobPosRelv_list>>(lstResponse);
        //                if (lst.Count > 0)
        //                {
        //                    foreach (JobPosRelv_list dms in lst)
        //                    {
        //                        JML.RelvExpId = dms.RelvExpId;
        //                        JML.RelvExpDesc = dms.RelvExpDesc;
        //                    }
        //                    return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
        //                }
        //            }
        //            else
        //            {
        //                return Json("error", JsonRequestBehavior.AllowGet);
        //            }
        //        }
        //        return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception ex)
        //    {
        //        ExceptionLogging.SendErrorToText(ex);
        //        return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
        //    }

        //}

        //get all job position list
        //public ActionResult GetAllJobPos()
        //{
        //    EmployeeGroupPolicy jm = new EmployeeGroupPolicy();
        //    List<EmployeeGroupPolicy> lst = new List<EmployeeGroupPolicy>();
        //    try
        //    {
        //        string userName = Session["UserName"].ToString();
        //        //jm.LastModifiedBy = userName;
        //        string draw = Request.Form.GetValues("draw")[0];
        //        string order = Request.Form.GetValues("order[0][column]")[0];
        //        string orderDir = Request.Form.GetValues("order[0][dir]")[0];
        //        int startRec = Convert.ToInt32(Request.Form.GetValues("start")[0]);
        //        int pageSize = Convert.ToInt32(Request.Form.GetValues("length")[0]);
        //        string search = Request.Form.GetValues("search[value]").FirstOrDefault();

        //        ///

        //        ///
        //        using (var client = new HttpClient())
        //        {

        //            client.BaseAddress = new Uri(APIUrl);

        //            var res = client.PostAsJsonAsync("EmployeeGroupPolicy/GetAllJobPos", jm).Result;

        //            //Checking the response is successful or not which is sent using HttpClient  
        //            if (res.IsSuccessStatusCode)
        //            {

        //                var lstResponse = res.Content.ReadAsStringAsync().Result;
        //                //Deserializing the response recieved from web api and storing into the EmployeeGroupPolicy list  
        //                lst = JsonConvert.DeserializeObject<List<EmployeeGroupPolicy>>(lstResponse);

        //                if (lst.Count > 0)
        //                {
        //                    foreach (EmployeeGroupPolicy jms in lst)
        //                    {
        //                        jm.sPAYCODE = jms.sPAYCODE;
        //                        jm.sEMPNAME = jms.sEMPNAME;
        //                        jm.sACTIVE = jms.sACTIVE;
        //                    }
        //                }
        //                if (Request.IsAjaxRequest())
        //                {
        //                    ///

        //                    // Get Exp Records.   
        //                    var allJobdes = lst;
        //                    //
        //                    IEnumerable<EmployeeGroupPolicy> filteredjob = lst;

        //                    long TotalRecordsCount = lst.Count();

        //                    #region filters  

        //                    if (!string.IsNullOrEmpty(search))
        //                    {
        //                        filteredjob = allJobdes.Where(
        //                        u => u.sPAYCODE.ToUpper().Contains(search.ToUpper())
        //                        || u.sEMPNAME.ToUpper().Contains(search.ToUpper())
        //                        || u.sACTIVE.ToUpper().Contains(search.ToUpper())
        //                        ).ToList();
        //                    }
        //                    #endregion
        //                    long FilteredRecordCount = lst.Count();

        //                    #region Sorting  

        //                    // Sorting     
        //                    switch (order)
        //                    {
        //                        case "0":
        //                            filteredjob = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredjob.OrderByDescending(p => p.sPAYCODE) : filteredjob.OrderBy(p => p.sPAYCODE);
        //                            break;
        //                        case "1":
        //                            filteredjob = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredjob.OrderByDescending(p => p.sPAYCODE) : filteredjob.OrderBy(p => p.sPAYCODE);
        //                            break;
        //                        case "2":
        //                            filteredjob = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredjob.OrderByDescending(p => p.sPAYCODE) : filteredjob.OrderBy(p => p.sPAYCODE);
        //                            break;
        //                        case "3":
        //                            filteredjob = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredjob.OrderByDescending(p => p.sEMPNAME) : filteredjob.OrderBy(p => p.sEMPNAME);
        //                            break;
        //                        default:
        //                            filteredjob = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredjob.OrderByDescending(p => p.sACTIVE) : filteredjob.OrderBy(p => p.sACTIVE);
        //                            break;
        //                    }
        //                    #endregion

        //                    var listjob = filteredjob.Skip(startRec).Take(pageSize).ToList()
        //                        .Select(d => new EmployeeGroupPolicy()
        //                        {
        //                            sPAYCODE = d.sPAYCODE,
        //                            sEMPNAME = d.sEMPNAME,
        //                            sACTIVE = d.sACTIVE

        //                        }).ToList();

        //                    if (listjob == null)
        //                        listjob = new List<EmployeeGroupPolicy>();

        //                    return this.Json(new
        //                    {
        //                        draw = Convert.ToInt32(draw),
        //                        recordsTotal = TotalRecordsCount,
        //                        recordsFiltered = FilteredRecordCount,
        //                        data = listjob
        //                    }, JsonRequestBehavior.AllowGet);

        //                }
        //            }
        //            else
        //            {
        //                return Json("error", JsonRequestBehavior.AllowGet);
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ExceptionLogging.SendErrorToText(ex);
        //        ViewBag.Message = "Error occured, contact to administrator";
        //        return Json("error", JsonRequestBehavior.AllowGet);
        //    }



        //    return View();
        //}
        // code for insert job position
        //public JsonResult Add(EmployeeGroupPolicy JPM)
        //{
        //    try
        //    {
        //        //JPM.LastModifiedBy = Session["UserName"].ToString();

        //        //JPM.LoginTerminalNameIP = Session["LoginIP"].ToString() + " " + Session["LoginTerminalName"].ToString();
        //        using (var client = new HttpClient())
        //        {
        //            //Passing service base url  
        //            client.BaseAddress = new Uri(APIUrl);

        //            var res = client.PostAsJsonAsync("EmployeeGroupPolicy/InsertJobPos", JPM).Result;

        //            //Checking the response is successful or not which is sent using HttpClient  
        //            if (res.IsSuccessStatusCode)
        //            {
        //                var serlzData = res.Content.ReadAsStringAsync().Result;
        //                var deSerlzData = JsonConvert.DeserializeObject(serlzData);
        //                return Json(deSerlzData, JsonRequestBehavior.AllowGet);
        //            }
        //            else
        //            {
        //                return Json("error", JsonRequestBehavior.AllowGet);
        //            }
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        ExceptionLogging.SendErrorToText(ex);
        //        ViewBag.Message = "Error occured, contact to administrator";
        //        return Json("error", JsonRequestBehavior.AllowGet);
        //    }
        //}

        //code for update job postion
        public JsonResult Update(EmployeeGroupPolicy EMPGP)
        {
            try
            {
                EMPGP.LastModifiedBy = Session["UserName"].ToString();
                EMPGP.sCOMPANYCODE = Session["CompanyCode"].ToString();
                EMPGP.LoginTerminalNameIP = Session["LoginIP"].ToString() + " " + Session["LoginTerminalName"].ToString();

                using (var client = new HttpClient())
                {
                    //Passing service base url  
                    client.BaseAddress = new Uri(APIUrl);

                    var res = client.PostAsJsonAsync("EmployeeGroupPolicy/UpdateEmployeeGroupPolicy", EMPGP).Result;

                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        return Json("success", JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json("error", JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return Json("error", JsonRequestBehavior.AllowGet);
            }
        }
        //code for get job position by ID
        public JsonResult GetbyID(string ID)
        {
            try
            {
                EmployeeGroupPolicy EMPGP = new EmployeeGroupPolicy();
                string userName = Session["UserName"].ToString();
                string CompanyCode = Session["CompanyCode"].ToString();
                EMPGP.sGroupID = ID;
                EMPGP.sCOMPANYCODE = CompanyCode;
                //JPM.LastModifiedBy = userName;
                using (var client = new HttpClient())
                {
                    //Passing service base url  
                    client.BaseAddress = new Uri(APIUrl);

                    var res = client.PostAsJsonAsync("EmployeeGroupPolicy/GetGetEmpGroup", EMPGP).Result;

                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        var lstResponse = res.Content.ReadAsStringAsync().Result;

                        return Json(lstResponse, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json("error", JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return Json("error", JsonRequestBehavior.AllowGet);
            }

        }
        //delete job position master
        public JsonResult Delete(string ID)
        {
            try
            {
                EmployeeGroupPolicy em = new EmployeeGroupPolicy();
                //JPM.PayCode = ID.Trim();
                //JPM.LastModifiedBy = Session["UserName"].ToString();
                //JPM.LoginTerminalNameIP = Session["LoginIP"].ToString() + " " + Session["LoginTerminalName"].ToString();
                using (var client = new HttpClient())
                {
                    //Passing service base url  
                    client.BaseAddress = new Uri(APIUrl);

                    var res = client.PostAsJsonAsync("EmployeeGroupPolicy/DeleteRecord", em).Result;

                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        var serlzData = res.Content.ReadAsStringAsync().Result;
                        var deSerlzData = JsonConvert.DeserializeObject(serlzData);
                        return Json(deSerlzData, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json("error", JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return Json("error", JsonRequestBehavior.AllowGet);
            }
        }
        //get auto id
        public JsonResult GetAutoGenerateID()
        {
            try
            {
                EmployeeGroupPolicy JPM = new EmployeeGroupPolicy();
                //JPM.LastModifiedBy = Session["UserName"].ToString();
                //JPM.LoginTerminalNameIP = Session["LoginIP"].ToString() + " " + Session["LoginTerminalName"].ToString();
                using (var client = new HttpClient())
                {
                    //Passing service base url  
                    client.BaseAddress = new Uri(APIUrl);

                    var res = client.PostAsJsonAsync("EmployeeGroupPolicy/GetAutoID", JPM).Result;

                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        var serlzData = res.Content.ReadAsStringAsync().Result;
                        var deSerlzData = JsonConvert.DeserializeObject(serlzData);
                        return Json(deSerlzData, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json("error", JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return Json("error", JsonRequestBehavior.AllowGet);
            }

        }
    }
}