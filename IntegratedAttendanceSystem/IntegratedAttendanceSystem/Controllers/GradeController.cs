﻿using IntegratedAttendanceSystem.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace IntegratedAttendanceSystem.Controllers
{
    public class GradeController : Controller
    {
        // GET: Grade
        string APIUrl = ConfigurationManager.AppSettings["APIURL"].ToString();
        public ActionResult Grade()
        {
            try
            {
                return View();
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return View();
            }
        }
        public JsonResult List()
        {
            try
            {
                GradeModel GradeModel = new GradeModel();
                List<GradeModel> lst = new List<GradeModel>();
                GradeModel.LastModifiedBy = Session["UserName"].ToString();
                GradeModel.LoginTerminalNameIP = Session["LoginIP"].ToString() + " " + Session["LoginTerminalName"].ToString();

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIUrl);
                    var res = client.PostAsJsonAsync("Grade/GetAllList", GradeModel).Result;
                    if (res.IsSuccessStatusCode)
                    {
                        var lstResponse = res.Content.ReadAsStringAsync().Result;
                        lst = JsonConvert.DeserializeObject<List<GradeModel>>(lstResponse);
                    }
                }
                return Json(lst, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
            }

        }
        //Get all Categry Master for jquery data table
        [HttpPost]
        public ActionResult GetAllGrade()
        {
            GradeModel CateMst = new GradeModel();
            List<GradeModel> lst = new List<GradeModel>();
            try
            {
                string userName = Session["UserName"].ToString();
                CateMst.LastModifiedBy = userName;
                string draw = Request.Form.GetValues("draw")[0];
                string order = Request.Form.GetValues("order[0][column]")[0];
                string orderDir = Request.Form.GetValues("order[0][dir]")[0];
                int startRec = Convert.ToInt32(Request.Form.GetValues("start")[0]);
                int pageSize = Convert.ToInt32(Request.Form.GetValues("length")[0]);
                string search = Request.Form.GetValues("search[value]").FirstOrDefault();

                ///

                ///
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIUrl);

                    var res = client.PostAsJsonAsync("Grade/GetAllList", CateMst).Result;

                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {

                        var lstResponse = res.Content.ReadAsStringAsync().Result;
                        //Deserializing the response recieved from web api and storing into the Employee list  
                        lst = JsonConvert.DeserializeObject<List<GradeModel>>(lstResponse);

                        if (lst.Count > 0)
                        {
                            foreach (GradeModel CatM in lst)
                            {
                                CateMst.IsActive = CatM.IsActive;
                                CateMst.GradeCode = CatM.GradeCode;
                                CateMst.GradeName = CatM.GradeName;
                                CateMst.CompanyCode = CatM.CompanyCode;
                            }
                        }
                        if (Request.IsAjaxRequest())
                        {
                            ///

                            // Get Exp Records.   
                            var allGrade = lst;
                            //
                            IEnumerable<GradeModel> filteredGrade = lst;

                            long TotalRecordsCount = lst.Count();

                            #region filters  

                            if (!string.IsNullOrEmpty(search))
                            {
                                filteredGrade = allGrade.Where(
                                u => u.IsActive.ToUpper().Contains(search.ToUpper())
                                || u.GradeCode.ToUpper().Contains(search.ToUpper())
                                || u.GradeName.ToUpper().Contains(search.ToUpper())
                                || u.CompanyCode.ToUpper().Contains(search.ToUpper())
                                ).ToList();
                            }
                            #endregion
                            long FilteredRecordCount = lst.Count();

                            #region Sorting  

                            // Sorting     
                            switch (order)
                            {
                                case "0":
                                    filteredGrade = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredGrade.OrderByDescending(p => p.GradeCode) : filteredGrade.OrderBy(p => p.GradeCode);
                                    break;
                                case "1":
                                    filteredGrade = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredGrade.OrderByDescending(p => p.GradeCode) : filteredGrade.OrderBy(p => p.GradeCode);
                                    break;
                                case "2":
                                    filteredGrade = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredGrade.OrderByDescending(p => p.IsActive) : filteredGrade.OrderBy(p => p.IsActive);
                                    break;
                                case "3":
                                    filteredGrade = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredGrade.OrderByDescending(p => p.GradeCode) : filteredGrade.OrderBy(p => p.GradeCode);
                                    break;
                                case "4":
                                    filteredGrade = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredGrade.OrderByDescending(p => p.GradeName) : filteredGrade.OrderBy(p => p.GradeName);
                                    break;
                                case "5":
                                    filteredGrade = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredGrade.OrderByDescending(p => p.CompanyCode) : filteredGrade.OrderBy(p => p.CompanyCode);
                                    break;
                                default:
                                    filteredGrade = filteredGrade.OrderByDescending(p => p.GradeCode);
                                    break;
                            }
                            #endregion

                            var listCate = filteredGrade.Skip(startRec).Take(pageSize).ToList()
                                .Select(pcm => new GradeModel()
                                {
                                    IsActive = pcm.IsActive,
                                    GradeCode = pcm.GradeCode,
                                    GradeName = pcm.GradeName,
                                    CompanyCode = pcm.CompanyCode,

                                }).ToList();

                            if (listCate == null)
                                listCate = new List<GradeModel>();

                            return this.Json(new
                            {
                                draw = Convert.ToInt32(draw),
                                recordsTotal = TotalRecordsCount,
                                recordsFiltered = FilteredRecordCount,
                                data = listCate
                            }, JsonRequestBehavior.AllowGet);

                        }
                    }
                    else
                    {
                        return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
            }
            return View();
        }
        //To Add new Grade record
        public JsonResult Add(GradeModel CATEADD)
        {
            try
            {
                CATEADD.LastModifiedBy = Session["UserName"].ToString();
                CATEADD.LoginTerminalNameIP = Session["LoginIP"].ToString() + " " + Session["LoginTerminalName"].ToString();
                CATEADD.CompanyCode = Session["Companycode"].ToString();

                using (var client = new HttpClient())
                {
                    //Passing service base url  
                    client.BaseAddress = new Uri(APIUrl);
                    var res = client.PostAsJsonAsync("Grade/InsertRecord", CATEADD).Result;
                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        return Json("success", JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
            }
        }
        //feth Grade Master based on code
        public JsonResult GetbyID(string ID)
        {
            try
            {
                GradeModel CATEID = new GradeModel();
                string userName = Session["UserName"].ToString();
                CATEID.GradeCode = ID.Trim();
                CATEID.LastModifiedBy = userName;
                using (var client = new HttpClient())
                {
                    //Passing service base url  
                    client.BaseAddress = new Uri(APIUrl);
                    var res = client.PostAsJsonAsync("Grade/GetSpecRec", CATEID).Result;
                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        var lstResponse = res.Content.ReadAsStringAsync().Result;
                        return Json(lstResponse, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
            }
        }
        //Update Grade Master
        public JsonResult Update(GradeModel GRDUPD)
        {
            try
            {
                GRDUPD.LastModifiedBy = Session["UserName"].ToString();
                GRDUPD.LoginTerminalNameIP = Session["LoginIP"].ToString() + " " + Session["LoginTerminalName"].ToString();
                GRDUPD.CompanyCode = Session["CompanyCode"].ToString();
                using (var client = new HttpClient())
                {
                    //Passing service base url  
                    client.BaseAddress = new Uri(APIUrl);
                    var res = client.PostAsJsonAsync("Grade/UpdateRecord", GRDUPD).Result;
                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        return Json("success", JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
            }
        }
        //Delete Grade Master
        public JsonResult Delete(string ID)
        {
            try
            {
                GradeModel CATEDEL = new GradeModel();
                CATEDEL.GradeCode = ID;
                CATEDEL.LastModifiedBy = Session["UserName"].ToString();
                CATEDEL.LoginTerminalNameIP = Session["LoginIP"].ToString() + " " + Session["LoginTerminalName"].ToString();
                CATEDEL.CompanyCode = Session["CompanyCode"].ToString();
                using (var client = new HttpClient())
                {
                    //Passing service base url  
                    client.BaseAddress = new Uri(APIUrl);
                    var res = client.PostAsJsonAsync("Grade/DeleteRecord", CATEDEL).Result;
                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        return Json("success", JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
            }

        }
        //feth Grade Master based on  code
        public JsonResult GetbyGradecode(string ID)
        {
            try
            {
                GradeModel CATEID = new GradeModel();
                string userName = Session["UserName"].ToString();
                CATEID.GradeCode = ID;
                CATEID.LastModifiedBy = userName;
                using (var client = new HttpClient())
                {
                    //Passing service base url  
                    client.BaseAddress = new Uri(APIUrl);
                    var res = client.PostAsJsonAsync("Grade/GetbyGradecode", CATEID).Result;
                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        var lstResponse = res.Content.ReadAsStringAsync().Result;
                        return Json(lstResponse, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
            }
        }
    }
}