﻿using IntegratedAttendanceSystem.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace IntegratedAttendanceSystem.Controllers
{
    public class HODController : Controller
    {
        // GET: HOD
        string APIUrl = ConfigurationManager.AppSettings["APIURL"].ToString();
        public ActionResult HOD()
        {
            try
            {
                return View();
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return View();
            }
        }
        public JsonResult List()
        {
            try
            {
                HODModel HODModel = new HODModel();
                List<HODModel> lst = new List<HODModel>();
                HODModel.LastModifiedBy = Session["UserName"].ToString();
                HODModel.LoginTerminalNameIP = Session["LoginIP"].ToString() + " " + Session["LoginTerminalName"].ToString();

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIUrl);
                    var res = client.PostAsJsonAsync("HOD/GetAllList", HODModel).Result;
                    if (res.IsSuccessStatusCode)
                    {
                        var lstResponse = res.Content.ReadAsStringAsync().Result;
                        lst = JsonConvert.DeserializeObject<List<HODModel>>(lstResponse);
                    }
                }
                return Json(lst, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
            }

        }
        //Get all Categry Master for jquery data table
        [HttpPost]
        public ActionResult GetAllHOD()
        {
            HODModel CateMst = new HODModel();
            List<HODModel> lst = new List<HODModel>();
            try
            {
                string userName = Session["UserName"].ToString();
                CateMst.LastModifiedBy = userName;
                string draw = Request.Form.GetValues("draw")[0];
                string order = Request.Form.GetValues("order[0][column]")[0];
                string orderDir = Request.Form.GetValues("order[0][dir]")[0];
                int startRec = Convert.ToInt32(Request.Form.GetValues("start")[0]);
                int pageSize = Convert.ToInt32(Request.Form.GetValues("length")[0]);
                string search = Request.Form.GetValues("search[value]").FirstOrDefault();

                ///

                ///
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIUrl);

                    var res = client.PostAsJsonAsync("HOD/GetAllList", CateMst).Result;

                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {

                        var lstResponse = res.Content.ReadAsStringAsync().Result;
                        //Deserializing the response recieved from web api and storing into the Employee list  
                        lst = JsonConvert.DeserializeObject<List<HODModel>>(lstResponse);

                        if (lst.Count > 0)
                        {
                            foreach (HODModel CatM in lst)
                            {
                                //CateMst.IsActive = CatM.IsActive;
                                CateMst.HODCode = CatM.HODCode;
                                CateMst.HODName = CatM.HODName;
                                CateMst.Paycode = CatM.Paycode;
                                CateMst.HODMail = CatM.HODMail;
                            }
                        }
                        if (Request.IsAjaxRequest())
                        {
                            ///

                            // Get Exp Records.   
                            var allHOD = lst;
                            //
                            IEnumerable<HODModel> filteredHOD = lst;

                            long TotalRecordsCount = lst.Count();

                            #region filters  

                            if (!string.IsNullOrEmpty(search))
                            {
                                filteredHOD = allHOD.Where(
                                u =>  u.HODCode.ToUpper().Contains(search.ToUpper())
                                || u.HODName.ToUpper().Contains(search.ToUpper())
                                || u.Paycode.ToUpper().Contains(search.ToUpper())
                                || u.HODMail.ToUpper().Contains(search.ToUpper())
                                ).ToList();
                            }
                            #endregion
                            long FilteredRecordCount = lst.Count();

                            #region Sorting  

                            // Sorting     
                            switch (order)
                            {
                                case "0":
                                    filteredHOD = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredHOD.OrderByDescending(p => p.HODCode) : filteredHOD.OrderBy(p => p.HODCode);
                                    break;
                                case "1":
                                    filteredHOD = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredHOD.OrderByDescending(p => p.HODCode) : filteredHOD.OrderBy(p => p.HODCode);
                                    break;                             
                                case "2":
                                    filteredHOD = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredHOD.OrderByDescending(p => p.HODCode) : filteredHOD.OrderBy(p => p.HODCode);
                                    break;
                                case "3":
                                    filteredHOD = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredHOD.OrderByDescending(p => p.HODName) : filteredHOD.OrderBy(p => p.HODName);
                                    break;  
                                case "4":
                                    filteredHOD = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredHOD.OrderByDescending(p => p.Paycode) : filteredHOD.OrderBy(p => p.Paycode);
                                    break;   
                                case "5":
                                    filteredHOD = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredHOD.OrderByDescending(p => p.HODMail) : filteredHOD.OrderBy(p => p.HODMail);
                                    break;                               
                                default:
                                    filteredHOD = filteredHOD.OrderByDescending(p => p.HODCode);
                                    break;
                            }
                            #endregion

                            var listCate = filteredHOD.Skip(startRec).Take(pageSize).ToList()
                                .Select(pcm => new HODModel()
                                {
                                    //IsActive = pcm.IsActive,
                                    HODCode = pcm.HODCode,
                                    HODName = pcm.HODName,
                                    Paycode = pcm.Paycode,
                                    HODMail = pcm.HODMail,
                                    
                                }).ToList();

                            if (listCate == null)
                                listCate = new List<HODModel>();

                            return this.Json(new
                            {
                                draw = Convert.ToInt32(draw),
                                recordsTotal = TotalRecordsCount,
                                recordsFiltered = FilteredRecordCount,
                                data = listCate
                            }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
            }
            return View();
        }
        //To Add new HOD record   
        public JsonResult Add(HODModel CATEADD)
        {
            try
            {
                CATEADD.LastModifiedBy = Session["UserName"].ToString();
                CATEADD.LoginTerminalNameIP = Session["LoginIP"].ToString() + " " + Session["LoginTerminalName"].ToString();
                using (var client = new HttpClient())
                {
                    //Passing service base url  
                    client.BaseAddress = new Uri(APIUrl);
                    var res = client.PostAsJsonAsync("HOD/InsertRecord", CATEADD).Result;
                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        return Json("success", JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
            }
        }
        //feth HOD Master based on  code
        public JsonResult GetbyID(string ID)
        {
            try
            {
                HODModel CATEID = new HODModel();
                string userName = Session["UserName"].ToString();
                CATEID.HODCode = ID;
                CATEID.LastModifiedBy = userName;
                using (var client = new HttpClient())
                {
                    //Passing service base url  
                    client.BaseAddress = new Uri(APIUrl);
                    var res = client.PostAsJsonAsync("HOD/GetSpecRec", CATEID).Result;
                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        var lstResponse = res.Content.ReadAsStringAsync().Result;
                        return Json(lstResponse, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
            }
        }
        //Update HOD Master 
        public JsonResult Update(HODModel CATEUPD)
        {
            try
            {
                CATEUPD.LastModifiedBy = Session["UserName"].ToString();
                CATEUPD.LoginTerminalNameIP = Session["LoginIP"].ToString() + " " + Session["LoginTerminalName"].ToString();
                using (var client = new HttpClient())
                {
                    //Passing service base url  
                    client.BaseAddress = new Uri(APIUrl);
                    var res = client.PostAsJsonAsync("HOD/UpdateRecord", CATEUPD).Result;
                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        return Json("success", JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
            }
        }
        //Delete HOD Master
        public JsonResult Delete(string ID)
        {
            try
            {
                HODModel CATEDEL = new HODModel();
                CATEDEL.HODCode = ID;
                CATEDEL.LastModifiedBy = Session["UserName"].ToString();
                CATEDEL.LoginTerminalNameIP = Session["LoginIP"].ToString() + " " + Session["LoginTerminalName"].ToString();
                using (var client = new HttpClient())
                {
                    //Passing service base url  
                    client.BaseAddress = new Uri(APIUrl);
                    var res = client.PostAsJsonAsync("HOD/DeleteRecord", CATEDEL).Result;
                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        return Json("success", JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
            }

        }
        //feth HOD Master based on  code
        public JsonResult GetbyHODcode(string ID)
        {
            try
            {
                HODModel CATEID = new HODModel();
                string userName = Session["UserName"].ToString();
                CATEID.HODCode = ID;
                CATEID.LastModifiedBy = userName;
                using (var client = new HttpClient())
                {
                    //Passing service base url  
                    client.BaseAddress = new Uri(APIUrl);
                    var res = client.PostAsJsonAsync("HOD/GetHODcode", CATEID).Result;
                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        var lstResponse = res.Content.ReadAsStringAsync().Result;
                        return Json(lstResponse, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult EmployeecodeList()
        {
            DataMaintenanceModel AdvLonStpMdl = new DataMaintenanceModel();
            List<DataMaintenanceModel.EmployeeCode_list> lst = new List<DataMaintenanceModel.EmployeeCode_list>();
            try
            {
                string userName = Session["UserName"].ToString();
                DataMaintenanceModel.EmployeeCode_list AdvLonStpMdlEmpCode = new DataMaintenanceModel.EmployeeCode_list();
                using (var client = new HttpClient())
                {
                    //Passing service base url  
                    client.BaseAddress = new Uri(APIUrl);
                    client.DefaultRequestHeaders.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                    var res = client.PostAsJsonAsync("HOD/GetAllEmployeeCodeList", AdvLonStpMdl).Result;

                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        var lstResponse = res.Content.ReadAsStringAsync().Result;
                        //Deserializing the response recieved from web api and storing into the Employee list  
                        lst = JsonConvert.DeserializeObject<List<DataMaintenanceModel.EmployeeCode_list>>(lstResponse);
                        if (lst.Count > 0)
                        {
                            foreach (DataMaintenanceModel.EmployeeCode_list AdvLonStpEmpCode in lst)
                            {
                                AdvLonStpMdlEmpCode.EmployeeCode = AdvLonStpEmpCode.EmployeeCode;
                                AdvLonStpMdlEmpCode.EmployeeName = AdvLonStpEmpCode.EmployeeName;
                            }
                            return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
                    }
                }
                return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
            }

        }
    }
}