﻿using IntegratedAttendanceSystem.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using IntegratedAttendanceSystem.Models;
using System.Net.Http.Headers;

namespace IntegratedAttendanceSystem.Controllers
{
    public class EmployeeMasterController : Controller
    {
        string APIUrl = ConfigurationManager.AppSettings["APIURL"].ToString();
        // GET: EmployeeMaster
        public ActionResult EmployeeMaster()
        {
            return View();
        }
        public ActionResult EmployeeMasterEdit()
        {
            return View();
        }
        public JsonResult ListAllCompanyDDL1()
        {
            EmployeeMasterModel dqualM = new EmployeeMasterModel();
            List<EmployeeMasterModel> lst = new List<EmployeeMasterModel>();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIUrl);

                    var res = client.GetAsync("EmployeeMaster/ListAllCompany").Result;
                    if (res.IsSuccessStatusCode)
                    {
                        var lstResponse = res.Content.ReadAsStringAsync().Result;
                        lst = JsonConvert.DeserializeObject<List<EmployeeMasterModel>>(lstResponse);
                    }
                }
                return Json(lst, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ex.ToString();
                ViewBag.Message = "Error occured, contact to administrator";
                return Json("error", JsonRequestBehavior.AllowGet);
            }
            //return Json(detailsqualification.ListAllQualification(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult ListAllCompanyDDL()
        {
            EmployeeMasterModel EmpMasterMdl = new EmployeeMasterModel();
            List<EmployeeMasterModel.Company_list> lst = new List<EmployeeMasterModel.Company_list>();
            try
            {
                //string userName = Session["UserName"].ToString();
                EmployeeMasterModel.Company_list FrmlStpCode = new EmployeeMasterModel.Company_list();
                using (var client = new HttpClient())
                {
                    //Passing service base url  
                    client.BaseAddress = new Uri(APIUrl);
                    client.DefaultRequestHeaders.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                    var res = client.PostAsJsonAsync("Employee/ListAllCompany", EmpMasterMdl).Result;

                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        var lstResponse = res.Content.ReadAsStringAsync().Result;
                        //Deserializing the response recieved from web api and storing into the Employee list  
                        lst = JsonConvert.DeserializeObject<List<EmployeeMasterModel.Company_list>>(lstResponse);
                        if (lst.Count > 0)
                        {
                            foreach (EmployeeMasterModel.Company_list Fsm in lst)
                            {
                                FrmlStpCode.CompanyCode = Fsm.CompanyCode;
                                FrmlStpCode.CompanyName = Fsm.CompanyName;
                            }
                            return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
                    }
                }
                return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
            }

        }
    }
}