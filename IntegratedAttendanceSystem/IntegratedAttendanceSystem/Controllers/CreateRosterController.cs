﻿using IntegratedAttendanceSystem.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace IntegratedAttendanceSystem.Controllers
{
    public class CreateRosterController : Controller
    {
        // GET: Company
        string APIUrl = ConfigurationManager.AppSettings["APIURL"].ToString();
        public ActionResult CreateRoster()
        {
            try
            {
                return View();
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return View();
            }
        }
        public JsonResult List()
        {
            try
            {
                CompanyModel Company = new CompanyModel();
                List<CompanyModel> lst = new List<CompanyModel>();
                Company.LastModifiedBy = Session["UserName"].ToString();
                Company.LoginTerminalNameIP = Session["LoginIP"].ToString() + " " + Session["LoginTerminalName"].ToString();
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIUrl);
                    var res = client.PostAsJsonAsync("Company/GetAllList", Company).Result;

                    if (res.IsSuccessStatusCode)
                    {
                        var lstResponse = res.Content.ReadAsStringAsync().Result;
                        lst = JsonConvert.DeserializeObject<List<CompanyModel>>(lstResponse);
                    }
                }
                return Json(lst, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
            }
        }
        //// Get all company
        public JsonResult ListAllCompany()
        {
            EmployeeGroupPolicy jm = new EmployeeGroupPolicy();
            List<Company_list> lst = new List<Company_list>();
            try
            {
                string userName = Session["UserName"].ToString();
                string CompanyCode = Session["CompanyCode"].ToString(); ;
                //jm.LastModifiedBy = userName;
                Company_list cm = new Company_list();
                using (var client = new HttpClient())
                {
                    //Passing service base url  
                    client.BaseAddress = new Uri(APIUrl);
                    client.DefaultRequestHeaders.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    //Sending request to find web api REST service resource GetAllEmployeeGroupPolicys using HttpClient  
                    var res = client.PostAsJsonAsync("EmployeeGroupPolicy/GetCompanyList", jm).Result;

                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        var lstResponse = res.Content.ReadAsStringAsync().Result;
                        //Deserializing the response recieved from web api and storing into the EmployeeGroupPolicy list  
                        lst = JsonConvert.DeserializeObject<List<Company_list>>(lstResponse);
                        if (lst.Count > 0)
                        {
                            foreach (Company_list cms in lst)
                            {
                                cm.CompanyCode = cms.CompanyCode;
                                cm.CompanyName = cms.CompanyName;
                            }
                            return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json("error", JsonRequestBehavior.AllowGet);
                    }
                }
                return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return Json("error", JsonRequestBehavior.AllowGet);
            }
        }

        //// Get all Location
        public JsonResult ListAllLocation()
        {
            EmployeeGroupPolicy jm = new EmployeeGroupPolicy();
            List<Division_list> lst = new List<Division_list>();
            try
            {
                string userName = Session["UserName"].ToString();
                string CompanyCode = Session["CompanyCode"].ToString(); ;
                //jm.LastModifiedBy = userName;
                Division_list cm = new Division_list();
                using (var client = new HttpClient())
                {
                    //Passing service base url  
                    client.BaseAddress = new Uri(APIUrl);
                    client.DefaultRequestHeaders.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    //Sending request to find web api REST service resource GetAllEmployeeGroupPolicys using HttpClient  
                    var res = client.PostAsJsonAsync("EmployeeGroupPolicy/GetLocationList", jm).Result;

                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        var lstResponse = res.Content.ReadAsStringAsync().Result;
                        //Deserializing the response recieved from web api and storing into the EmployeeGroupPolicy list  
                        lst = JsonConvert.DeserializeObject<List<Division_list>>(lstResponse);
                        if (lst.Count > 0)
                        {
                            foreach (Division_list cms in lst)
                            {
                                cm.DivisionCode = cms.DivisionCode;
                                cm.DivisionName = cms.DivisionName;
                            }
                            return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json("error", JsonRequestBehavior.AllowGet);
                    }
                }
                return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return Json("error", JsonRequestBehavior.AllowGet);
            }
        }

        //// Get all Employee
        public JsonResult ListAllEmployee()
        {
            RosterModel Rodter = new RosterModel();
            List<Division_list> lst = new List<Division_list>();
            try
            {
                Rodter.sUserName = Session["UserName"].ToString();
                Rodter.sCompanyCodeGlobel = Session["CompanyCode"].ToString(); ;
                //jm.LastModifiedBy = userName;
                Division_list cm = new Division_list();
                using (var client = new HttpClient())
                {
                    //Passing service base url  
                    client.BaseAddress = new Uri(APIUrl);
                    client.DefaultRequestHeaders.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    //Sending request to find web api REST service resource GetAllEmployeeGroupPolicys using HttpClient  
                    var res = client.PostAsJsonAsync("EmployeeGroupPolicy/GetLocationList", Rodter).Result;

                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        var lstResponse = res.Content.ReadAsStringAsync().Result;
                        //Deserializing the response recieved from web api and storing into the EmployeeGroupPolicy list  
                        lst = JsonConvert.DeserializeObject<List<Division_list>>(lstResponse);
                        if (lst.Count > 0)
                        {
                            foreach (Division_list cms in lst)
                            {
                                cm.DivisionCode = cms.DivisionCode;
                                cm.DivisionName = cms.DivisionName;
                            }
                            return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json("error", JsonRequestBehavior.AllowGet);
                    }
                }
                return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return Json("error", JsonRequestBehavior.AllowGet);
            }
        }

        //Get all Company Master for jquery data table
        [HttpPost]
        public ActionResult GetAllCompany()
        {
            CompanyModel CompMst = new CompanyModel();
            List<CompanyModel> lst = new List<CompanyModel>();
            try
            {
                string userName = Session["UserName"].ToString();
                CompMst.LastModifiedBy = userName;
                string draw = Request.Form.GetValues("draw")[0];
                string order = Request.Form.GetValues("order[0][column]")[0];
                string orderDir = Request.Form.GetValues("order[0][dir]")[0];
                int startRec = Convert.ToInt32(Request.Form.GetValues("start")[0]);
                int pageSize = Convert.ToInt32(Request.Form.GetValues("length")[0]);
                string search = Request.Form.GetValues("search[value]").FirstOrDefault();

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIUrl);
                    var res = client.PostAsJsonAsync("CreateRoster/GetAllList", CompMst).Result;
                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        var lstResponse = res.Content.ReadAsStringAsync().Result;
                        //Deserializing the response recieved from web api and storing into the Employee list  
                        lst = JsonConvert.DeserializeObject<List<CompanyModel>>(lstResponse);
                        if (lst.Count > 0)
                        {
                            foreach (CompanyModel pcm in lst)
                            {
                                CompMst.IsActive = pcm.IsActive;
                                CompMst.Code = pcm.Code;
                                CompMst.Name = pcm.Name;
                                CompMst.ShortName = pcm.ShortName;
                                CompMst.IndustryNature = pcm.IndustryNature;
                                CompMst.Address = pcm.Address;
                                CompMst.PhoneNo = pcm.PhoneNo;
                                CompMst.EmailId = pcm.EmailId;
                                CompMst.RegistationNo = pcm.RegistationNo;
                                CompMst.PANNo = pcm.PANNo;
                                CompMst.TANNo = pcm.TANNo;
                                CompMst.PFNo = pcm.PFNo;
                                CompMst.LCNo = pcm.LCNo;
                            }
                        }
                        if (Request.IsAjaxRequest())
                        {
                            // Get Exp Records.   
                            var allCompany = lst;
                            //
                            IEnumerable<CompanyModel> filteredCompany = lst;
                            long TotalRecordsCount = lst.Count();
                            #region filters  
                            if (!string.IsNullOrEmpty(search))
                            {
                                filteredCompany = allCompany.Where(
                                u => u.IsActive.ToUpper().Contains(search.ToUpper())
                                || u.Code.ToUpper().Contains(search.ToUpper())
                                || u.Name.ToUpper().Contains(search.ToUpper())
                                || u.ShortName.ToUpper().Contains(search.ToUpper())
                                || u.IndustryNature.ToUpper().Contains(search.ToUpper())
                                || u.Address.ToUpper().Contains(search.ToUpper())
                                || u.PhoneNo.ToUpper().Contains(search.ToUpper())
                                || u.EmailId.ToUpper().Contains(search.ToUpper())
                                || u.RegistationNo.ToUpper().Contains(search.ToUpper())
                                || u.PANNo.ToUpper().Contains(search.ToUpper())
                                || u.TANNo.ToUpper().Contains(search.ToUpper())
                                || u.PFNo.ToUpper().Contains(search.ToUpper())
                                || u.LCNo.ToUpper().Contains(search.ToUpper())
                                ).ToList();
                            }
                            #endregion
                            long FilteredRecordCount = lst.Count();
                            #region Sorting  
                            // Sorting     
                            switch (order)
                            {
                                case "0":
                                    filteredCompany = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredCompany.OrderByDescending(p => p.Code) : filteredCompany.OrderBy(p => p.Code);
                                    break;
                                case "1":
                                    filteredCompany = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredCompany.OrderByDescending(p => p.Code) : filteredCompany.OrderBy(p => p.Code);
                                    break;
                                case "2":
                                    filteredCompany = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredCompany.OrderByDescending(p => p.IsActive) : filteredCompany.OrderBy(p => p.IsActive);
                                    break;
                                case "3":
                                    filteredCompany = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredCompany.OrderByDescending(p => p.Code) : filteredCompany.OrderBy(p => p.Code);
                                    break;
                                case "4":
                                    filteredCompany = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredCompany.OrderByDescending(p => p.Name) : filteredCompany.OrderBy(p => p.Name);
                                    break;
                                case "5":
                                    filteredCompany = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredCompany.OrderByDescending(p => p.ShortName) : filteredCompany.OrderBy(p => p.ShortName);
                                    break;
                                case "6":
                                    filteredCompany = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredCompany.OrderByDescending(p => p.IndustryNature) : filteredCompany.OrderBy(p => p.IndustryNature);
                                    break;
                                case "7":
                                    filteredCompany = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredCompany.OrderByDescending(p => p.Address) : filteredCompany.OrderBy(p => p.Address);
                                    break;
                                case "8":
                                    filteredCompany = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredCompany.OrderByDescending(p => p.PhoneNo) : filteredCompany.OrderBy(p => p.PhoneNo);
                                    break;
                                case "9":
                                    filteredCompany = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredCompany.OrderByDescending(p => p.EmailId) : filteredCompany.OrderBy(p => p.EmailId);
                                    break;
                                case "10":
                                    filteredCompany = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredCompany.OrderByDescending(p => p.RegistationNo) : filteredCompany.OrderBy(p => p.RegistationNo);
                                    break;
                                case "11":
                                    filteredCompany = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredCompany.OrderByDescending(p => p.PANNo) : filteredCompany.OrderBy(p => p.PANNo);
                                    break;
                                case "12":
                                    filteredCompany = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredCompany.OrderByDescending(p => p.TANNo) : filteredCompany.OrderBy(p => p.TANNo);
                                    break;
                                case "13":
                                    filteredCompany = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredCompany.OrderByDescending(p => p.PFNo) : filteredCompany.OrderBy(p => p.PFNo);
                                    break;
                                case "14":
                                    filteredCompany = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredCompany.OrderByDescending(p => p.LCNo) : filteredCompany.OrderBy(p => p.LCNo);
                                    break;
                                default:
                                    filteredCompany = filteredCompany.OrderByDescending(p => p.Code);
                                    break;
                            }
                            #endregion

                            var listComp = filteredCompany.Skip(startRec).Take(pageSize).ToList()
                                .Select(pcm => new CompanyModel()
                                {
                                    IsActive = pcm.IsActive,
                                    Code = pcm.Code,
                                    Name = pcm.Name,
                                    ShortName = pcm.ShortName,
                                    IndustryNature = pcm.IndustryNature,
                                    Address = pcm.Address,
                                    PhoneNo = pcm.PhoneNo,
                                    EmailId = pcm.EmailId,
                                    RegistationNo = pcm.RegistationNo,
                                    PANNo = pcm.PANNo,
                                    TANNo = pcm.TANNo,
                                    PFNo = pcm.PFNo,
                                    LCNo = pcm.LCNo,
                                }).ToList();

                            if (listComp == null)
                                listComp = new List<CompanyModel>();

                            return this.Json(new
                            {
                                draw = Convert.ToInt32(draw),
                                recordsTotal = TotalRecordsCount,
                                recordsFiltered = FilteredRecordCount,
                                data = listComp
                            }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
            }
            return View();
        }
        //To Add new Company record   
        public JsonResult Create(RosterModel C_Roster)
        {
            try
            {
                C_Roster.sUserName = Session["UserName"].ToString();
                C_Roster.sCompanyCodeGlobel = Session["CompanyCode"].ToString();
                C_Roster.LoginTerminalNameIP = Session["LoginIP"].ToString() + " " + Session["LoginTerminalName"].ToString();

                using (var client = new HttpClient())
                {
                    //Passing service base url  
                    client.BaseAddress = new Uri(APIUrl);
                    var res = client.PostAsJsonAsync("CreateRoster/CreateRoster", C_Roster).Result;
                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        return Json("success", JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
            }
        }
        //feth Company Master based on  code
        public JsonResult GetbyID(string ID)
        {
            try
            {
                CompanyModel CM = new CompanyModel();
                string userName = Session["UserName"].ToString();
                CM.Code = ID;
                CM.LastModifiedBy = userName;
                using (var client = new HttpClient())
                {
                    //Passing service base url  
                    client.BaseAddress = new Uri(APIUrl);
                    var res = client.PostAsJsonAsync("Company/GetSpecRec", CM).Result;
                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        var lstResponse = res.Content.ReadAsStringAsync().Result;
                        return Json(lstResponse, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
            }
        }
        //Update IAS Company Master 
        public JsonResult UpdateRoster(RosterModel U_Roster)
        {
            try
            {
                U_Roster.LastModifiedBy = Session["UserName"].ToString();
                U_Roster.LoginTerminalNameIP = Session["LoginIP"].ToString() + " " + Session["LoginTerminalName"].ToString();
                using (var client = new HttpClient())
                {
                    //Passing service base url  
                    client.BaseAddress = new Uri(APIUrl);
                    var res = client.PostAsJsonAsync("CreateRoster/UpdateRoster", U_Roster).Result;
                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        return Json("success", JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
            }

        }
        //Delete  Company Master
        public JsonResult Delete(string ID)
        {
            try
            {
                CompanyModel CMDEL = new CompanyModel();
                CMDEL.Code = ID;
                CMDEL.LastModifiedBy = Session["UserName"].ToString();
                CMDEL.LoginTerminalNameIP = Session["LoginIP"].ToString() + " " + Session["LoginTerminalName"].ToString();
                using (var client = new HttpClient())
                {
                    //Passing service base url  
                    client.BaseAddress = new Uri(APIUrl);
                    var res = client.PostAsJsonAsync("Company/DeleteRecord", CMDEL).Result;
                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        return Json("success", JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
            }
        }
        //feth Company Master based on  code
        public JsonResult GetbyCompcode(string ID)
        {
            try
            {
                CompanyModel CM = new CompanyModel();
                string userName = Session["UserName"].ToString();
                CM.Code = ID;
                CM.LastModifiedBy = userName;
                using (var client = new HttpClient())
                {
                    //Passing service base url  
                    client.BaseAddress = new Uri(APIUrl);
                    var res = client.PostAsJsonAsync("Company/GetCompcode", CM).Result;
                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        var lstResponse = res.Content.ReadAsStringAsync().Result;
                        return Json(lstResponse, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
            }
        }
    }
}