﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IntegratedAttendanceSystem.Models;
using System.Configuration;
using Newtonsoft.Json;
using System.Net.Http;
using System.Net.Http.Headers;


namespace IntegratedAttendanceSystem.Controllers
{
    public class DeviceController : Controller
    {
        string APIUrl = ConfigurationManager.AppSettings["APIURL"].ToString();
        // GET: Device
        public ActionResult Device()
        {
            try
            {
                return View();
            }
            catch (Exception ex)
            {
                ex.ToString();
                ViewBag.Message = "Error occured, contact to administrator";
                return View();
            }
        }
        public JsonResult List()
        {
            DeviceModel DeptMast = new DeviceModel();
            List<DeviceModel> lst = new List<DeviceModel>();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIUrl);

                    var res = client.PostAsJsonAsync("Device/GetAllDevice", DeptMast).Result;
                    if (res.IsSuccessStatusCode)
                    {
                        var lstResponse = res.Content.ReadAsStringAsync().Result;
                        lst = JsonConvert.DeserializeObject<List<DeviceModel>>(lstResponse);
                    }
                }
                return Json(lst, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ex.ToString();
                ViewBag.Message = "Error occured, contact to administrator";
                return Json("error", JsonRequestBehavior.AllowGet);
            }
        }     
        [HttpPost]
        public ActionResult GetAllDevice()
        {
            DeviceModel DeptMast = new DeviceModel();
            List<DeviceModel> lst = new List<DeviceModel>();
            try
            {
                string draw = Request.Form.GetValues("draw")[0];
                string order = Request.Form.GetValues("order[0][column]")[0];
                string orderDir = Request.Form.GetValues("order[0][dir]")[0];
                int startRec = Convert.ToInt32(Request.Form.GetValues("start")[0]);
                int pageSize = Convert.ToInt32(Request.Form.GetValues("length")[0]);
                string search = Request.Form.GetValues("search[value]").FirstOrDefault();

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIUrl);
                    var res = client.PostAsJsonAsync("Device/GetAllDevice", DeptMast).Result;

                    if (res.IsSuccessStatusCode)
                    {
                        var lstResponse = res.Content.ReadAsStringAsync().Result;
                        lst = JsonConvert.DeserializeObject<List<DeviceModel>>(lstResponse);

                        if (lst.Count > 0)
                        {
                            foreach (DeviceModel DeptMstLst in lst)
                            {
                                DeptMast.DeviceCode = DeptMstLst.DeviceCode;
                                DeptMast.DeviceName = DeptMstLst.DeviceName;
                                DeptMast.IsActive = DeptMstLst.IsActive;
                            }
                        }

                        //==
                        if (Request.IsAjaxRequest())
                        {
                            // Get Device Master Records. 
                            var allDept = lst; //langDB.GetAllLanguage();
                            IEnumerable<DeviceModel> filteredLang = lst; //langDB.GetAllDevice();
                            long TotalRecordsCount = lst.Count(); //langDB.GetAllDevice().Count();
                            #region filters  
                            if (!string.IsNullOrEmpty(search))
                            {
                                filteredLang = allDept.Where(
                                u => u.IsActive.ToUpper().Contains(search.ToUpper())
                                || u.DeviceCode.ToUpper().Contains(search.ToUpper())
                                || u.DeviceName.ToUpper().Contains(search.ToUpper())
                                || u.IsActive.ToUpper().Contains(search.ToUpper())
                                ).ToList();
                            }
                            #endregion
                            long FilteredRecordCount = lst.Count(); //langDB.GetAllDevice().Count();
                            #region Sorting  
                            // Sorting     
                            switch (order)
                            {
                                case "0":
                                    filteredLang = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredLang.OrderByDescending(p => p.DeviceCode) : filteredLang.OrderBy(p => p.DeviceCode);
                                    break;
                                case "1":
                                    filteredLang = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredLang.OrderByDescending(p => p.DeviceCode) : filteredLang.OrderBy(p => p.DeviceCode);
                                    break;
                                case "2":
                                    filteredLang = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredLang.OrderByDescending(p => p.DeviceCode) : filteredLang.OrderBy(p => p.DeviceCode);
                                    break;
                                case "3":
                                    filteredLang = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredLang.OrderByDescending(p => p.IsActive) : filteredLang.OrderBy(p => p.IsActive);
                                    break;
                                case "4":
                                    filteredLang = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredLang.OrderByDescending(p => p.DeviceName) : filteredLang.OrderBy(p => p.DeviceName);
                                    break;
                                default:
                                    filteredLang = filteredLang.OrderByDescending(p => p.DeviceCode);
                                    break;
                            }
                            #endregion

                            var listLang = filteredLang.Skip(startRec).Take(pageSize).ToList()
                                .Select(d => new DeviceModel()
                                {

                                    IsActive = d.IsActive,
                                    DeviceCode = d.DeviceCode,
                                    DeviceName = d.DeviceName
                                }).ToList();

                            if (listLang == null)
                                listLang = new List<DeviceModel>();

                            return this.Json(new
                            {
                                draw = Convert.ToInt32(draw),
                                recordsTotal = TotalRecordsCount,
                                recordsFiltered = FilteredRecordCount,
                                data = listLang
                            }, JsonRequestBehavior.AllowGet);

                        }
                        //==
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                ViewBag.Message = "Error occured, contact to administrator";
                return Json("error", JsonRequestBehavior.AllowGet);
            }
            return View();
        }
        // Get all Device data
        //public JsonResult GetAutoGenerateCode()
        //{
        //    try
        //    {
        //        DeviceModel DeptMastMod = new DeviceModel();
        //        DeviceModel objDeviceModel = new DeviceModel();
        //        List<DeviceModel> lst = new List<DeviceModel>();

        //        using (var client = new HttpClient())
        //        {
        //            client.BaseAddress = new Uri(APIUrl);

        //            var res = client.PostAsJsonAsync("Device/GetAllDevice", DeptMastMod).Result;
        //            if (res.IsSuccessStatusCode)
        //            {
        //                var lstResponse = res.Content.ReadAsStringAsync().Result;
        //                lst = JsonConvert.DeserializeObject<List<DeviceModel>>(lstResponse);
        //            }
        //        }
        //        var lastcode = lst.ToList().OrderByDescending(m => m.DeviceCode).FirstOrDefault();
        //        if (lastcode == null)
        //        {
        //            objDeviceModel.DeviceCode = "00001";
        //        }
        //        else
        //        {
        //            objDeviceModel.DeviceCode = (Convert.ToInt32(lastcode.DeviceCode) + 1).ToString("D5").PadLeft(0);
        //        }
        //        return Json(objDeviceModel.DeviceCode, JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception ex)
        //    {
        //        ex.ToString();
        //        ViewBag.Message = "Error occured, contact to administrator";
        //        return Json("error", JsonRequestBehavior.AllowGet);
        //    }
        //}
        public JsonResult ListAllDevice()
        {
            DeviceModel DeptMast = new DeviceModel();
            List<DeviceModel> lst = new List<DeviceModel>();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIUrl);

                    var res = client.PostAsJsonAsync("Device/GetAllDevice", DeptMast).Result;
                    if (res.IsSuccessStatusCode)
                    {
                        var lstResponse = res.Content.ReadAsStringAsync().Result;
                        lst = JsonConvert.DeserializeObject<List<DeviceModel>>(lstResponse);
                    }
                }
                return Json(lst, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ex.ToString();
                ViewBag.Message = "Error occured, contact to administrator";
                return Json("error", JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult Add(DeviceModel DeptMast)
        {
            try
            {
                DeptMast.LastModifiedBy = Session["UserName"].ToString();
                DeptMast.LoginTerminalNameIP = Session["LoginIP"].ToString() + " " + Session["LoginTerminalName"].ToString();

                using (var client = new HttpClient())
                {
                    //Passing service base url  
                    client.BaseAddress = new Uri(APIUrl);

                    var res = client.PostAsJsonAsync("Device/AddDevice", DeptMast).Result;

                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        return Json("success", JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json("error", JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                ViewBag.Message = "Error occured, contact to administrator";
                return Json("error", JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetbyID(string ID)
        {
            try
            {
                DeviceModel DeptMast = new DeviceModel();
                string userName = Session["UserName"].ToString();
                DeptMast.DeviceCode = ID;
                DeptMast.LastModifiedBy = userName;
                using (var client = new HttpClient())
                {
                    //Passing service base url  
                    client.BaseAddress = new Uri(APIUrl);

                    var res = client.PostAsJsonAsync("Device/GetSpecRec", DeptMast).Result;

                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        var lstResponse = res.Content.ReadAsStringAsync().Result;

                        return Json(lstResponse, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
            }

        }
        //public JsonResult GetbyID(string DeviceCode)
        //{
        //    try
        //    {
        //        DeviceModel DeptMast = new DeviceModel();
        //        List<DeviceModel> lst = new List<DeviceModel>();

        //        using (var client = new HttpClient())
        //        {
        //            client.BaseAddress = new Uri(APIUrl);

        //            var res = client.PostAsJsonAsync("Device/GetAllDevice", DeptMast).Result;
        //            if (res.IsSuccessStatusCode)
        //            {
        //                var lstResponse = res.Content.ReadAsStringAsync().Result;
        //                lst = JsonConvert.DeserializeObject<List<DeviceModel>>(lstResponse);
        //            }
        //        }
        //        var DeptCode = lst.Find(x => x.DeviceCode.Equals(DeviceCode.ToString()));
        //        return Json(DeptCode, JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception ex)
        //    {
        //        ex.ToString();
        //        ViewBag.Message = "Error occured, contact to administrator";
        //        return Json("error", JsonRequestBehavior.AllowGet);
        //    }
        //}
        public JsonResult Update(DeviceModel DeptMast)
        {
            try
            {
                DeptMast.LastModifiedBy = Session["UserName"].ToString();
                DeptMast.LoginTerminalNameIP = Session["LoginIP"].ToString() + " " + Session["LoginTerminalName"].ToString();
                using (var client = new HttpClient())
                {
                    //Passing service base url  
                    client.BaseAddress = new Uri(APIUrl);

                    var res = client.PostAsJsonAsync("Device/UpdateDevice", DeptMast).Result;

                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        return Json("success", JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json("error", JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                ViewBag.Message = "Error occured, contact to administrator";
                return Json("error", JsonRequestBehavior.AllowGet);
            }

        }   
        public JsonResult Delete(string ID)
        {
            try
            {
                DeviceModel DeptMast = new DeviceModel();
                DeptMast.DeviceCode = ID;
                DeptMast.LastModifiedBy = Session["UserName"].ToString();
                DeptMast.LoginTerminalNameIP = Session["LoginIP"].ToString() + " " + Session["LoginTerminalName"].ToString();
                using (var client = new HttpClient())
                {
                    //Passing service base url  
                    client.BaseAddress = new Uri(APIUrl);

                    var res = client.PostAsJsonAsync("Device/DeleteDevice", DeptMast).Result;

                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        return Json("success", JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json("error", JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                ViewBag.Message = "Error occured, contact to administrator";
                return Json("error", JsonRequestBehavior.AllowGet);
            }
        }
        //feth Device Master based on  code
        public JsonResult GetbyDeptCode(string ID)
        {
            try
            {
                DeviceModel DeptMast = new DeviceModel();
                string userName = Session["UserName"].ToString();
                DeptMast.DeviceCode = ID;
                DeptMast.LastModifiedBy = userName;
                using (var client = new HttpClient())
                {
                    //Passing service base url  
                    client.BaseAddress = new Uri(APIUrl);

                    var res = client.PostAsJsonAsync("Device/GetDeptcode", DeptMast).Result;

                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        var lstResponse = res.Content.ReadAsStringAsync().Result;

                        return Json(lstResponse, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
            }

        }
    }
}