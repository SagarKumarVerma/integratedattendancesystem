﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IntegratedAttendanceSystem.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            Session["UserName"] = "Administrator";
            Session["CompanyCode"] = "C01";
            Session["LoginIP"] = "192.168.1.16";
            Session["LoginTerminalName"] = "TimeWatch-PC";
            Session["isAutogenerateID"] = "N";
            
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}