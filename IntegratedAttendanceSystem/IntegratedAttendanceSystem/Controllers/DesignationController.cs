﻿using IntegratedAttendanceSystem.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace IntegratedAttendanceSystem.Controllers
{
    public class DesignationController : Controller
    {
        // GET: Designation
        string APIUrl = ConfigurationManager.AppSettings["APIURL"].ToString();
        public ActionResult Designation()
        {
            try
            {
                return View();
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return View();
            }
        }
        public JsonResult List()
        {
            try
            {
                DesignationModel DesignationMod = new DesignationModel();
                List<DesignationModel> lst = new List<DesignationModel>();
                DesignationMod.LastModifiedBy = Session["UserName"].ToString();
                DesignationMod.LoginTerminalNameIP = Session["LoginIP"].ToString() + " " + Session["LoginTerminalName"].ToString();

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIUrl);
                    var res = client.PostAsJsonAsync("Designation/GetAllDesignation", DesignationMod).Result;
                    if (res.IsSuccessStatusCode)
                    {
                        var lstResponse = res.Content.ReadAsStringAsync().Result;
                        lst = JsonConvert.DeserializeObject<List<DesignationModel>>(lstResponse);
                    }
                }
                return Json(lst, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
            }

        }
        //Get all Designation Master for jquery data table
        [HttpPost]
        public ActionResult GetAllDesignation()
        {
            DesignationModel DesignationMst = new DesignationModel();
            List<DesignationModel> lst = new List<DesignationModel>();
            try
            {
                string userName = Session["UserName"].ToString();
                DesignationMst.LastModifiedBy = userName;
                string draw = Request.Form.GetValues("draw")[0];
                string order = Request.Form.GetValues("order[0][column]")[0];
                string orderDir = Request.Form.GetValues("order[0][dir]")[0];
                int startRec = Convert.ToInt32(Request.Form.GetValues("start")[0]);
                int pageSize = Convert.ToInt32(Request.Form.GetValues("length")[0]);
                string search = Request.Form.GetValues("search[value]").FirstOrDefault();

                ///

                ///
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIUrl);

                    var res = client.PostAsJsonAsync("Designation/GetAllDesignation", DesignationMst).Result;

                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {

                        var lstResponse = res.Content.ReadAsStringAsync().Result;
                        //Deserializing the response recieved from web api and storing into the Employee list  
                        lst = JsonConvert.DeserializeObject<List<DesignationModel>>(lstResponse);

                        if (lst.Count > 0)
                        {
                            foreach (DesignationModel DesignationM in lst)
                            {
                                DesignationMst.DesignationCode = DesignationM.DesignationCode;
                                DesignationMst.DesignationName = DesignationM.DesignationName;
                            }
                        }
                        if (Request.IsAjaxRequest())
                        {
                            // Get Exp Records.   
                            var allDesignation = lst;
                            //
                            IEnumerable<DesignationModel> filteredDesignation = lst;

                            long TotalRecordsCount = lst.Count();

                            #region filters  

                            if (!string.IsNullOrEmpty(search))
                            {
                                filteredDesignation = allDesignation.Where(
                                u => u.DesignationCode.ToUpper().Contains(search.ToUpper())
                                || u.DesignationName.ToUpper().Contains(search.ToUpper())
                                ).ToList();
                            }
                            #endregion
                            long FilteredRecordCount = lst.Count();

                            #region Sorting  

                            // Sorting     
                            switch (order)
                            {
                                case "0":
                                    filteredDesignation = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredDesignation.OrderByDescending(p => p.DesignationCode) : filteredDesignation.OrderBy(p => p.DesignationCode);
                                    break;
                                case "1":
                                    filteredDesignation = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredDesignation.OrderByDescending(p => p.DesignationCode) : filteredDesignation.OrderBy(p => p.DesignationCode);
                                    break;
                                case "2":
                                    filteredDesignation = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredDesignation.OrderByDescending(p => p.DesignationCode) : filteredDesignation.OrderBy(p => p.DesignationCode);
                                    break;
                                case "3":
                                    filteredDesignation = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredDesignation.OrderByDescending(p => p.DesignationName) : filteredDesignation.OrderBy(p => p.DesignationName);
                                    break;                                
                                default:
                                    filteredDesignation = filteredDesignation.OrderByDescending(p => p.DesignationCode);
                                    break;
                            }
                            #endregion

                            var listDesignation = filteredDesignation.Skip(startRec).Take(pageSize).ToList()
                                .Select(pcm => new DesignationModel()
                                {
                                    DesignationCode = pcm.DesignationCode,
                                    DesignationName = pcm.DesignationName,

                                }).ToList();

                            if (listDesignation == null)
                                listDesignation = new List<DesignationModel>();

                            return this.Json(new
                            {
                                draw = Convert.ToInt32(draw),
                                recordsTotal = TotalRecordsCount,
                                recordsFiltered = FilteredRecordCount,
                                data = listDesignation
                            }, JsonRequestBehavior.AllowGet);

                        }
                    }
                    else
                    {
                        return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
            }
            return View();
        }
        //To Add new Designation record   
        public JsonResult Add(DesignationModel DESGADD)
        {
            try
            {
                DESGADD.LastModifiedBy = Session["UserName"].ToString();
                DESGADD.LoginTerminalNameIP = Session["LoginIP"].ToString() + " " + Session["LoginTerminalName"].ToString();
                using (var client = new HttpClient())
                {
                    //Passing service base url  
                    client.BaseAddress = new Uri(APIUrl);
                    var res = client.PostAsJsonAsync("Designation/InsertRecord", DESGADD).Result;
                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        return Json("success", JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
                    }
                }

            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
            }
        }
        //feth Designation Master based on  code
        public JsonResult GetbyID(string ID)
        {
            try
            {
                DesignationModel LOCID = new DesignationModel();
                string userName = Session["UserName"].ToString();
                LOCID.DesignationCode = ID;
                LOCID.LastModifiedBy = userName;
                using (var client = new HttpClient())
                {
                    //Passing service base url  
                    client.BaseAddress = new Uri(APIUrl);
                    var res = client.PostAsJsonAsync("Designation/GetSpecRec", LOCID).Result;
                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        var lstResponse = res.Content.ReadAsStringAsync().Result;
                        return Json(lstResponse, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
            }
        }
        //Update Designation Master 
        public JsonResult Update(DesignationModel LOCUPD)
        {
            try
            {
                LOCUPD.LastModifiedBy = Session["UserName"].ToString();
                LOCUPD.LoginTerminalNameIP = Session["LoginIP"].ToString() + " " + Session["LoginTerminalName"].ToString();
                using (var client = new HttpClient())
                {
                    //Passing service base url  
                    client.BaseAddress = new Uri(APIUrl);
                    var res = client.PostAsJsonAsync("Designation/UpdateRecord", LOCUPD).Result;
                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        return Json("success", JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
            }

        }
        //Delete Designation Master
        public JsonResult Delete(string ID)
        {
            try
            {
                DesignationModel LOCEDEL = new DesignationModel();
                LOCEDEL.DesignationCode = ID;
                LOCEDEL.LastModifiedBy = Session["UserName"].ToString();
                LOCEDEL.LoginTerminalNameIP = Session["LoginIP"].ToString() + " " + Session["LoginTerminalName"].ToString();
                using (var client = new HttpClient())
                {
                    //Passing service base url  
                    client.BaseAddress = new Uri(APIUrl);
                    var res = client.PostAsJsonAsync("Designation/DeleteRecord", LOCEDEL).Result;
                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        return Json("success", JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
            }

        }

        //feth Designation Master based on  code
        public JsonResult GetbyDesignation(string ID)
        {
            try
            {
                DesignationModel LOCID = new DesignationModel();
                string userName = Session["UserName"].ToString();
                LOCID.DesignationCode = ID;
                LOCID.LastModifiedBy = userName;
                using (var client = new HttpClient())
                {
                    //Passing service base url  
                    client.BaseAddress = new Uri(APIUrl);
                    var res = client.PostAsJsonAsync("Designation/GetDesignation", LOCID).Result;
                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        var lstResponse = res.Content.ReadAsStringAsync().Result;
                        return Json(lstResponse, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult ListAllDesignation()
        {
            DesignationModel DeptMast = new DesignationModel();
            List<DesignationModel> lst = new List<DesignationModel>();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIUrl);

                    var res = client.PostAsJsonAsync("Designation/GetAllDesignation", DeptMast).Result;
                    if (res.IsSuccessStatusCode)
                    {
                        var lstResponse = res.Content.ReadAsStringAsync().Result;
                        lst = JsonConvert.DeserializeObject<List<DesignationModel>>(lstResponse);
                    }
                }
                return Json(lst, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ex.ToString();
                ViewBag.Message = "Error occured, contact to administrator";
                return Json("error", JsonRequestBehavior.AllowGet);
            }
        }
    }
}