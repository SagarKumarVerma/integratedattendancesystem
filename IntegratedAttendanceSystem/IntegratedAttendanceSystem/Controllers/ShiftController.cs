﻿using IntegratedAttendanceSystem.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace IntegratedAttendanceSystem.Controllers
{
    public class ShiftController : Controller
    {
        // GET: Shift
        string APIUrl = ConfigurationManager.AppSettings["APIURL"].ToString();
        public ActionResult Shift()
        {
            try
            {
                return View();
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return View();
            }
        }
        public JsonResult List()
        {
            try
            {
                ShiftModel Shift = new ShiftModel();
                List<ShiftModel> lst = new List<ShiftModel>();
                Shift.LastModifiedBy = Session["UserName"].ToString();
                Shift.LoginTerminalNameIP = Session["LoginIP"].ToString() + " " + Session["LoginTerminalName"].ToString();
                Shift.CompanyCode = Session["CompanyCode"].ToString();

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIUrl);
                    var res = client.PostAsJsonAsync("Shift/GetAllList", Shift).Result;
                    if (res.IsSuccessStatusCode)
                    {
                        var lstResponse = res.Content.ReadAsStringAsync().Result;
                        lst = JsonConvert.DeserializeObject<List<ShiftModel>>(lstResponse);
                    }
                }
                return Json(lst, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
            }
        }
        //Get all Shift Master for jquery data table
        [HttpPost]
        public ActionResult GetAllShift()
        {
            ShiftModel ShiftMst = new ShiftModel();
            List<ShiftModel> lst = new List<ShiftModel>();
            try
            {
                ShiftMst.CompanyCode = Session["CompanyCode"].ToString();
                //ShiftMst.CompanyCode = Session["CompanyCode"].ToString();
                string userName = "";
                try
                {
                    userName = Session["UserName"].ToString();
                }
                catch
                {

                }

                ShiftMst.LastModifiedBy = userName;
                ShiftMst.UserType = userName;

                string draw = Request.Form.GetValues("draw")[0];
                string order = Request.Form.GetValues("order[0][column]")[0];
                string orderDir = Request.Form.GetValues("order[0][dir]")[0];
                int startRec = Convert.ToInt32(Request.Form.GetValues("start")[0]);
                int pageSize = Convert.ToInt32(Request.Form.GetValues("length")[0]);
                string search = Request.Form.GetValues("search[value]").FirstOrDefault();
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIUrl);
                    var res = client.PostAsJsonAsync("Shift/GetAllList", ShiftMst).Result;
                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        var lstResponse = res.Content.ReadAsStringAsync().Result;
                        //Deserializing the response recieved from web api and storing into the Employee list  
                        lst = JsonConvert.DeserializeObject<List<ShiftModel>>(lstResponse);
                        if (lst.Count > 0)
                        {
                            foreach (ShiftModel pcm in lst)
                            {
                                ShiftMst.CompanyCode = pcm.CompanyCode;
                                ShiftMst.SHIFT = pcm.SHIFT;
                                ShiftMst.ShiftDiscription = pcm.ShiftDiscription;
                                ShiftMst.ShiftStartTime = pcm.ShiftStartTime;
                                ShiftMst.ShiftEndTime = pcm.ShiftEndTime;
                                ShiftMst.ShiftHours = pcm.ShiftHours;
                                ShiftMst.LunchStartTime = pcm.LunchStartTime;
                                ShiftMst.LunchDuration = pcm.LunchDuration;
                                ShiftMst.LunchEndTime = pcm.LunchEndTime;
                                ShiftMst.LunchDeduction = pcm.LunchDeduction;
                                //ShiftMst.FlexibleLunchDeduction = pcm.FlexibleLunchDeduction;
                                ShiftMst.Overtimedeductafter = pcm.Overtimedeductafter;
                                ShiftMst.Overtimestartafter = pcm.Overtimestartafter;
                                ShiftMst.Overtimededuction = pcm.Overtimededuction;
                                ShiftMst.ShiftPosition = pcm.ShiftPosition;
                                //ShiftMst.LastModifiedBy = pcm.LastModifiedBy;
                            }
                        }
                        if (Request.IsAjaxRequest())
                        {
                            // Get Exp Records.   
                            var allShift = lst;
                            //
                            IEnumerable<ShiftModel> filteredShift = lst;
                            long TotalRecordsCount = lst.Count();
                            #region filters  
                            if (!string.IsNullOrEmpty(search))
                            {
                                filteredShift = allShift.Where(
                                u => u.CompanyCode.ToUpper().Contains(search.ToUpper())
                                || u.SHIFT.ToUpper().Contains(search.ToUpper())
                                || u.ShiftDiscription.ToUpper().Contains(search.ToUpper())
                                //|| u.ShiftStartTime.ToUpper().Contains(search.ToUpper())
                                //|| u.ShiftEndTime.ToUpper().Contains(search.ToUpper())
                                //|| u.ShiftHours.ToUpper().Contains(search.ToUpper())
                                //|| u.LunchStartTime.ToUpper().Contains(search.ToUpper())
                                //|| u.LunchDuration.ToUpper().Contains(search.ToUpper())
                                //|| u.LunchEndTime.ToUpper().Contains(search.ToUpper())
                                //|| u.LunchDeduction.ToUpper().Contains(search.ToUpper())
                                //|| u.Overtimedeductafter.ToUpper().Contains(search.ToUpper())
                                //|| u.Overtimestartafter.ToUpper().Contains(search.ToUpper())
                                //|| u.Overtimededuction.ToUpper().Contains(search.ToUpper())
                                || u.ShiftPosition.ToUpper().Contains(search.ToUpper())
                                //|| u.LastModifiedBy.ToUpper().Contains(search.ToUpper())
                                ).ToList();
                            }
                            #endregion
                            long FilteredRecordCount = lst.Count();
                            #region Sorting  
                            // Sorting     
                            switch (order)
                            {
                                case "0":
                                    filteredShift = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredShift.OrderByDescending(p => p.SHIFT) : filteredShift.OrderBy(p => p.SHIFT);
                                    break;
                                case "1":
                                    filteredShift = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredShift.OrderByDescending(p => p.SHIFT) : filteredShift.OrderBy(p => p.SHIFT);
                                    break;
                                case "2":
                                    filteredShift = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredShift.OrderByDescending(p => p.CompanyCode) : filteredShift.OrderBy(p => p.CompanyCode);
                                    break;
                                case "3":
                                    filteredShift = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredShift.OrderByDescending(p => p.SHIFT) : filteredShift.OrderBy(p => p.SHIFT);
                                    break;
                                case "4":
                                    filteredShift = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredShift.OrderByDescending(p => p.ShiftDiscription) : filteredShift.OrderBy(p => p.ShiftDiscription);
                                    break;
                                case "5":
                                    filteredShift = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredShift.OrderByDescending(p => p.ShiftStartTime) : filteredShift.OrderBy(p => p.ShiftStartTime);
                                    break;
                                case "6":
                                    filteredShift = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredShift.OrderByDescending(p => p.ShiftEndTime) : filteredShift.OrderBy(p => p.ShiftEndTime);
                                    break;
                                case "7":
                                    filteredShift = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredShift.OrderByDescending(p => p.ShiftHours) : filteredShift.OrderBy(p => p.ShiftHours);
                                    break;
                                default:
                                    filteredShift = filteredShift.OrderByDescending(p => p.SHIFT);
                                    break;
                            }
                            #endregion

                            var listComp = filteredShift.Skip(startRec).Take(pageSize).ToList()
                                .Select(pcm => new ShiftModel()
                                {
                                    CompanyCode = pcm.CompanyCode,
                                    SHIFT = pcm.SHIFT,
                                    ShiftDiscription = pcm.ShiftDiscription,
                                    ShiftStartTime = pcm.ShiftStartTime,
                                    ShiftEndTime = pcm.ShiftEndTime,
                                    ShiftHours = pcm.ShiftHours,
                                    LunchStartTime = pcm.LunchStartTime,
                                    LunchDuration = pcm.LunchDuration,
                                    LunchEndTime = pcm.LunchEndTime,
                                    LunchDeduction = pcm.LunchDeduction,
                                    //FlexibleLunchDeduction = pcm.FlexibleLunchDeduction,
                                    Overtimedeductafter = pcm.Overtimedeductafter,
                                    Overtimestartafter = pcm.Overtimestartafter,
                                    Overtimededuction = pcm.Overtimededuction,
                                    ShiftPosition = pcm.ShiftPosition,
                                }).ToList();

                            if (listComp == null)
                                listComp = new List<ShiftModel>();

                            return this.Json(new
                            {
                                draw = Convert.ToInt32(draw),
                                recordsTotal = TotalRecordsCount,
                                recordsFiltered = FilteredRecordCount,
                                data = listComp
                            }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
            }
            return View();
        }
        //To Add new Shift record   
        public JsonResult Add(ShiftModel SMADD)
        {
            try
            {
                SMADD.LastModifiedBy = Session["UserName"].ToString();
                SMADD.LoginTerminalNameIP = Session["LoginIP"].ToString() + " " + Session["LoginTerminalName"].ToString();
                using (var client = new HttpClient())
                {
                    //Passing service base url  
                    client.BaseAddress = new Uri(APIUrl);
                    var res = client.PostAsJsonAsync("Shift/InsertRecord", SMADD).Result;
                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        return Json("success", JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
            }
        }
        //feth Shift Master based on  code
        public JsonResult GetbyID(string ID)
        {
            try
            {
                ShiftModel SM = new ShiftModel();
                string userName = Session["UserName"].ToString();
                SM.SHIFT = ID;
                SM.LastModifiedBy = userName;
                SM.CompanyCode = Session["CompanyCode"].ToString();

                using (var client = new HttpClient())
                {
                    //Passing service base url  
                    client.BaseAddress = new Uri(APIUrl);
                    var res = client.PostAsJsonAsync("Shift/GetSpecRec", SM).Result;
                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        var lstResponse = res.Content.ReadAsStringAsync().Result;
                        return Json(lstResponse, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
            }
        }
        //Update IAS Shift Master 
        public JsonResult Update(ShiftModel CMUPD)
        {
            try
            {
                CMUPD.LastModifiedBy = Session["UserName"].ToString();
                CMUPD.LoginTerminalNameIP = Session["LoginIP"].ToString() + " " + Session["LoginTerminalName"].ToString();
                using (var client = new HttpClient())
                {
                    //Passing service base url  
                    client.BaseAddress = new Uri(APIUrl);
                    var res = client.PostAsJsonAsync("Shift/UpdateRecord", CMUPD).Result;
                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        return Json("success", JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
            }

        }
        //Delete  Shift Master
        public JsonResult Delete(string ID)
        {
            try
            {
                ShiftModel CMDEL = new ShiftModel();
                CMDEL.SHIFT = ID;
                CMDEL.LastModifiedBy = Session["UserName"].ToString();
                CMDEL.LoginTerminalNameIP = Session["LoginIP"].ToString() + " " + Session["LoginTerminalName"].ToString();
                CMDEL.CompanyCode = Session["CompanyCode"].ToString();
                using (var client = new HttpClient())
                {
                    //Passing service base url  
                    client.BaseAddress = new Uri(APIUrl);
                    var res = client.PostAsJsonAsync("Shift/DeleteRecord", CMDEL).Result;
                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        return Json("success", JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
            }
        }
        //feth Shift Master based on  code
        public JsonResult GetbySHIFT(string ID)
        {
            try
            {
                ShiftModel CM = new ShiftModel();
                string userName = Session["UserName"].ToString();
                CM.SHIFT = ID;
                CM.LastModifiedBy = userName;
                using (var client = new HttpClient())
                {
                    //Passing service base url  
                    client.BaseAddress = new Uri(APIUrl);
                    var res = client.PostAsJsonAsync("Shift/GetCompcode", CM).Result;
                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        var lstResponse = res.Content.ReadAsStringAsync().Result;
                        return Json(lstResponse, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
            }
        }

        //// Get all company
        public JsonResult ListAllCompany()
        {
            Employee jm = new Employee();
            List<Company_list> lst = new List<Company_list>();
            try
            {
                string userName = Session["UserName"].ToString();
                //jm.LastModifiedBy = userName;
                Company_list cm = new Company_list();
                using (var client = new HttpClient())
                {
                    //Passing service base url  
                    client.BaseAddress = new Uri(APIUrl);
                    client.DefaultRequestHeaders.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                    var res = client.PostAsJsonAsync("Shift/GetCompanyList1", jm).Result;

                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        var lstResponse = res.Content.ReadAsStringAsync().Result;
                        //Deserializing the response recieved from web api and storing into the Employee list  
                        lst = JsonConvert.DeserializeObject<List<Company_list>>(lstResponse);
                        if (lst.Count > 0)
                        {
                            foreach (Company_list cms in lst)
                            {
                                cm.CompanyCode = cms.CompanyCode;
                                cm.CompanyName = cms.CompanyName;
                            }
                            return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json("error", JsonRequestBehavior.AllowGet);
                    }
                }
                return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return Json("error", JsonRequestBehavior.AllowGet);
            }
        }
    }
}