﻿using IntegratedAttendanceSystem.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Mvc;

namespace IntegratedAttendanceSystem.Controllers
{
    public class EmployeeController : Controller
    {
        /// <summary>
        /// This variable is use to assign the hosted url of web api.
        /// it may vary according to systems.
        /// </summary>
        string APIUrl = ConfigurationManager.AppSettings["APIURL"].ToString();
        // GET: Employee
        public ActionResult Employee()
        {
            try
            {

            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
            }
            return View();
        }
        public ActionResult CreateEmployee()
        {
            try
            {

            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
            }
            return View();
        }
        //// Get all Group
        public JsonResult ListAlGroup()
        {
            List<Group_list> lst = new List<Group_list>();
            Employee jpm = new Employee();
            try
            {
                string userName = Session["UserName"].ToString();
                //jpm.LastModifiedBy = userName;
                Group_list gm = new Group_list();
                using (var client = new HttpClient())
                {
                    //Passing service base url  
                    client.BaseAddress = new Uri(APIUrl);
                    client.DefaultRequestHeaders.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                    var res = client.PostAsJsonAsync("Employee/GetGroupList", jpm).Result;

                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        var lstResponse = res.Content.ReadAsStringAsync().Result;
                        //Deserializing the response recieved from web api and storing into the Employee list  
                        lst = JsonConvert.DeserializeObject<List<Group_list>>(lstResponse);
                        if (lst.Count > 0)
                        {
                            foreach (Group_list gms in lst)
                            {
                                gm.GroupCode = gms.GroupName;
                                gm.GroupName = gms.GroupName;

                            }
                            return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json("error", JsonRequestBehavior.AllowGet);
                    }
                }
                return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return Json("error", JsonRequestBehavior.AllowGet);
            }
        }
        //// Get all company
        public JsonResult ListAllCompany()
        {
            Employee jm = new Employee();
            List<Company_list> lst = new List<Company_list>();
            try
            {
                string userName = Session["UserName"].ToString();
                //jm.LastModifiedBy = userName;
                Company_list cm = new Company_list();
                using (var client = new HttpClient())
                {
                    //Passing service base url  
                    client.BaseAddress = new Uri(APIUrl);
                    client.DefaultRequestHeaders.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                    var res = client.PostAsJsonAsync("Employee/GetCompanyList1", jm).Result;

                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        var lstResponse = res.Content.ReadAsStringAsync().Result;
                        //Deserializing the response recieved from web api and storing into the Employee list  
                        lst = JsonConvert.DeserializeObject<List<Company_list>>(lstResponse);
                        if (lst.Count > 0)
                        {
                            foreach (Company_list cms in lst)
                            {
                                cm.CompanyCode = cms.CompanyCode;
                                cm.CompanyName = cms.CompanyName;
                            }
                            return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json("error", JsonRequestBehavior.AllowGet);
                    }
                }
                return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return Json("error", JsonRequestBehavior.AllowGet);
            }
        }
        //// Get all department
        public JsonResult ListAlDepartment()
        {
            Employee em = new Employee();
            List<Department_list> lst = new List<Department_list>();
            try
            {
                Department_list dm = new Department_list();

                using (var client = new HttpClient())
                {
                    string userName = Session["UserName"].ToString();
                    //em.LastModifiedBy = userName;
                    //Passing service base url  
                    client.BaseAddress = new Uri(APIUrl);
                    client.DefaultRequestHeaders.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                    var res = client.PostAsJsonAsync("Employee/GetDepartmentList", em).Result;

                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        var lstResponse = res.Content.ReadAsStringAsync().Result;
                        //Deserializing the response recieved from web api and storing into the Employee list  
                        lst = JsonConvert.DeserializeObject<List<Department_list>>(lstResponse);
                        if (lst.Count > 0)
                        {
                            foreach (Department_list dms in lst)
                            {
                                dm.DepartmentCode = dms.DepartmentCode;
                                dm.DepartmentName = dms.DepartmentName;

                            }
                            return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json("error", JsonRequestBehavior.AllowGet);
                    }
                }
                return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return Json("error", JsonRequestBehavior.AllowGet);
            }

        }

        ////Get all designation list
        public JsonResult ListAllDesignation()
        {
            Employee jm = new Employee();
            List<Designation_list> lst = new List<Designation_list>();
            try
            {
                Designation_list dm = new Designation_list();

                using (var client = new HttpClient())
                {
                    string userName = Session["UserName"].ToString();
                    //jm.LastModifiedBy = userName;
                    //Passing service base url  
                    client.BaseAddress = new Uri(APIUrl);
                    client.DefaultRequestHeaders.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                    var res = client.PostAsJsonAsync("Employee/GetDesignationList", jm).Result;

                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        var lstResponse = res.Content.ReadAsStringAsync().Result;
                        //Deserializing the response recieved from web api and storing into the Employee list  
                        lst = JsonConvert.DeserializeObject<List<Designation_list>>(lstResponse);
                        if (lst.Count > 0)
                        {
                            foreach (Designation_list dms in lst)
                            {
                                dm.DesignationCode = dms.DesignationCode;
                                dm.DesignationName = dms.DesignationName;

                            }
                            return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json("error", JsonRequestBehavior.AllowGet);
                    }
                }
                return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return Json("error", JsonRequestBehavior.AllowGet);
            }
        }

        ////Get all Category list
        public JsonResult ListAllCategory(string isInsert)
        {
            Employee Em = new Employee();
            List<CatMaster_list> lst = new List<CatMaster_list>();
            try
            {
                CatMaster_list CatM = new CatMaster_list();
                using (var client = new HttpClient())
                {
                    string userName = Session["UserName"].ToString();
                    //jpm.IsInsert = isInsert.Trim();
                    //jpm.LastModifiedBy = userName.Trim();
                    //Passing service base url  
                    client.BaseAddress = new Uri(APIUrl);
                    client.DefaultRequestHeaders.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                    var res = client.PostAsJsonAsync("Employee/GetCategoryList", Em).Result;

                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        var lstResponse = res.Content.ReadAsStringAsync().Result;
                        //Deserializing the response recieved from web api and storing into the Employee list  
                        lst = JsonConvert.DeserializeObject<List<CatMaster_list>>(lstResponse);
                        if (lst.Count > 0)
                        {
                            foreach (CatMaster_list catms in lst)
                            {
                                CatM.CategoryCode = catms.CategoryCode;
                                CatM.CategoryName = catms.CategoryName;
                            }
                            return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json("error", JsonRequestBehavior.AllowGet);
                    }
                }
                return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
            }
        }

        ////Get all Location/Section/Division list
        public JsonResult ListAllLocation(string isInsert)
        {
            Employee Em = new Employee();
            List<Division_list> lst = new List<Division_list>();
            try
            {
                Division_list CatM = new Division_list();
                using (var client = new HttpClient())
                {
                    string userName = Session["UserName"].ToString();
                    //jpm.IsInsert = isInsert.Trim();
                    //jpm.LastModifiedBy = userName.Trim();
                    //Passing service base url  
                    client.BaseAddress = new Uri(APIUrl);
                    client.DefaultRequestHeaders.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                    var res = client.PostAsJsonAsync("Employee/GetLocationList", Em).Result;

                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        var lstResponse = res.Content.ReadAsStringAsync().Result;
                        //Deserializing the response recieved from web api and storing into the Employee list  
                        lst = JsonConvert.DeserializeObject<List<Division_list>>(lstResponse);
                        if (lst.Count > 0)
                        {
                            foreach (Division_list catms in lst)
                            {
                                CatM.DivisionCode = catms.DivisionCode;
                                CatM.DivisionName = catms.DivisionName;
                            }
                            return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json("error", JsonRequestBehavior.AllowGet);
                    }
                }
                return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
            }
        }

        ////Get all Bank list
        public JsonResult ListAllBank(string isInsert)
        {
            Employee Em = new Employee();
            List<Bank_list> lst = new List<Bank_list>();
            try
            {
                Bank_list BnkM = new Bank_list();
                using (var client = new HttpClient())
                {
                    string userName = Session["UserName"].ToString();
                    //jpm.IsInsert = isInsert.Trim();
                    //jpm.LastModifiedBy = userName.Trim();
                    //Passing service base url  
                    client.BaseAddress = new Uri(APIUrl);
                    client.DefaultRequestHeaders.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                    var res = client.PostAsJsonAsync("Employee/GetBankList", Em).Result;

                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        var lstResponse = res.Content.ReadAsStringAsync().Result;
                        //Deserializing the response recieved from web api and storing into the Employee list  
                        lst = JsonConvert.DeserializeObject<List<Bank_list>>(lstResponse);
                        if (lst.Count > 0)
                        {
                            foreach (Bank_list BnkMs in lst)
                            {
                                BnkM.BankCode = BnkMs.BankCode;
                                BnkM.BankName = BnkMs.BankName;
                            }
                            return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json("error", JsonRequestBehavior.AllowGet);
                    }
                }
                return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
            }
        }

        ////Get all HOD list
        public JsonResult ListAllHOD(string isInsert)
        {
            Employee Em = new Employee();
            List<HOD_list> lst = new List<HOD_list>();
            try
            {
                HOD_list BnkM = new HOD_list();
                using (var client = new HttpClient())
                {
                    string userName = Session["UserName"].ToString();
                    //jpm.IsInsert = isInsert.Trim();
                    //jpm.LastModifiedBy = userName.Trim();
                    //Passing service base url  
                    client.BaseAddress = new Uri(APIUrl);
                    client.DefaultRequestHeaders.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                    var res = client.PostAsJsonAsync("Employee/GetHODList", Em).Result;

                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        var lstResponse = res.Content.ReadAsStringAsync().Result;
                        //Deserializing the response recieved from web api and storing into the Employee list  
                        lst = JsonConvert.DeserializeObject<List<HOD_list>>(lstResponse);
                        if (lst.Count > 0)
                        {
                            foreach (HOD_list BnkMs in lst)
                            {
                                BnkM.HODCode = BnkMs.HODCode;
                                BnkM.HODName = BnkMs.HODName;
                            }
                            return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json("error", JsonRequestBehavior.AllowGet);
                    }
                }
                return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
            }
        }

        ////Get all Employee Group list
        public JsonResult ListAllEmployeeGroup(string isInsert)
        {
            Employee Em = new Employee();
            List<EmployeeGroup_list> lst = new List<EmployeeGroup_list>();
            try
            {
                EmployeeGroup_list BnkM = new EmployeeGroup_list();
                using (var client = new HttpClient())
                {
                    string userName = Session["UserName"].ToString();
                    //jpm.IsInsert = isInsert.Trim();
                    //jpm.LastModifiedBy = userName.Trim();
                    //Passing service base url  
                    client.BaseAddress = new Uri(APIUrl);
                    client.DefaultRequestHeaders.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                    var res = client.PostAsJsonAsync("Employee/GetEmployeeGroupList", Em).Result;

                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        var lstResponse = res.Content.ReadAsStringAsync().Result;
                        //Deserializing the response recieved from web api and storing into the Employee list  
                        lst = JsonConvert.DeserializeObject<List<EmployeeGroup_list>>(lstResponse);
                        if (lst.Count > 0)
                        {
                            foreach (EmployeeGroup_list BnkMs in lst)
                            {
                                BnkM.GroupID = BnkMs.GroupID;
                                BnkM.GroupName = BnkMs.GroupName;
                            }
                            return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json("error", JsonRequestBehavior.AllowGet);
                    }
                }
                return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
            }
        }

        ////Get all Employee list
        [HttpPost]
        public ActionResult GetAllEmployee()
        {
            Employee em = new Employee();
            List<Employee> lst = new List<Employee>();
            try
            {
                string userName = Session["UserName"].ToString();
                //em.LastModifiedBy = userName;
                string draw = Request.Form.GetValues("draw")[0];
                string order = Request.Form.GetValues("order[0][column]")[0];
                string orderDir = Request.Form.GetValues("order[0][dir]")[0];
                int startRec = Convert.ToInt32(Request.Form.GetValues("start")[0]);
                int pageSize = Convert.ToInt32(Request.Form.GetValues("length")[0]);
                string search = Request.Form.GetValues("search[value]").FirstOrDefault();

                ///

                ///
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIUrl);

                    var res = client.PostAsJsonAsync("Employee/GetAllEmployee", em).Result;

                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {

                        var lstResponse = res.Content.ReadAsStringAsync().Result;
                        //Deserializing the response recieved from web api and storing into the Employee list  
                        lst = JsonConvert.DeserializeObject<List<Employee>>(lstResponse);

                        if (lst.Count > 0)
                        {
                            foreach (Employee ems in lst)
                            {
                                em.sPAYCODE = ems.sPAYCODE;
                                em.sEMPNAME = ems.sEMPNAME;
                                em.sACTIVE = ems.sACTIVE;
                                em.sGUARDIANNAME = ems.sGUARDIANNAME;
                                em.sDateOFBIRTH = ems.sDateOFBIRTH;
                                em.sDateOFJOIN = ems.sDateOFJOIN;
                                em.sPRESENTCARDNO = ems.sPRESENTCARDNO;
                                em.sCOMPANYCODE = ems.sCOMPANYCODE;
                                em.sDivisionCode = ems.sDivisionCode;
                                em.sCATCode = ems.sCATCode;
                                em.sDepartmentCode = ems.sDepartmentCode;
                                em.sGradeCode = ems.sGradeCode;
                                em.sSEX = ems.sSEX;
                                em.sISMARRIED = ems.sISMARRIED;
                                em.sDESIGNATIONCODE = ems.sDESIGNATIONCODE;
                                em.sSSN = ems.sSSN;
                            }
                        }
                        if (Request.IsAjaxRequest())
                        {
                            ///

                            // Get Exp Records.   
                            var allJobdes = lst;
                            //
                            IEnumerable<Employee> filteredjob = lst;

                            long TotalRecordsCount = lst.Count();

                            #region filters  

                            if (!string.IsNullOrEmpty(search))
                            {
                                filteredjob = allJobdes.Where(
                                u => u.sPAYCODE.ToUpper().Contains(search.ToUpper())
                                || u.sEMPNAME.ToUpper().Contains(search.ToUpper())
                                || u.sACTIVE.ToUpper().Contains(search.ToUpper())
                                || u.sGUARDIANNAME.ToUpper().Contains(search.ToUpper())
                                || u.sDateOFBIRTH.ToUpper().Contains(search.ToUpper())
                                || u.sDateOFJOIN.ToUpper().Contains(search.ToUpper())
                                || u.sPRESENTCARDNO.ToUpper().Contains(search.ToUpper())
                                || u.sCOMPANYCODE.ToUpper().Contains(search.ToUpper())
                                || u.sDivisionCode.ToUpper().Contains(search.ToUpper())
                                || u.sCATCode.ToUpper().Contains(search.ToUpper())
                                || u.sDepartmentCode.ToUpper().Contains(search.ToUpper())
                                || u.sGradeCode.ToUpper().Contains(search.ToUpper())
                                || u.sSEX.ToUpper().Contains(search.ToUpper())
                                || u.sISMARRIED.ToUpper().Contains(search.ToUpper())
                                || u.sDESIGNATIONCODE.ToUpper().Contains(search.ToUpper())
                                || u.sSSN.ToUpper().Contains(search.ToUpper())
                                ).ToList();
                            }
                            #endregion
                            long FilteredRecordCount = lst.Count();

                            #region Sorting  

                            // Sorting     
                            switch (order)
                            {
                                case "0":
                                    filteredjob = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredjob.OrderByDescending(p => p.sSSN) : filteredjob.OrderBy(p => p.sSSN);
                                    break;
                                case "1":
                                    filteredjob = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredjob.OrderByDescending(p => p.sSSN) : filteredjob.OrderBy(p => p.sSSN);
                                    break;
                                case "2":
                                    filteredjob = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredjob.OrderByDescending(p => p.sPAYCODE) : filteredjob.OrderBy(p => p.sPAYCODE);
                                    break;
                                case "3":
                                    filteredjob = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredjob.OrderByDescending(p => p.sEMPNAME) : filteredjob.OrderBy(p => p.sEMPNAME);
                                    break;
                                case "4":
                                    filteredjob = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredjob.OrderByDescending(p => p.sACTIVE) : filteredjob.OrderBy(p => p.sACTIVE);
                                    break;
                                case "5":
                                    filteredjob = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredjob.OrderByDescending(p => p.sGUARDIANNAME) : filteredjob.OrderBy(p => p.sGUARDIANNAME);
                                    break;
                                case "6":
                                    filteredjob = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredjob.OrderByDescending(p => p.sDateOFBIRTH) : filteredjob.OrderBy(p => p.sDateOFBIRTH);
                                    break;
                                case "7":
                                    filteredjob = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredjob.OrderByDescending(p => p.sDateOFJOIN) : filteredjob.OrderBy(p => p.sDateOFJOIN);
                                    break;
                                case "8":
                                    filteredjob = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredjob.OrderByDescending(p => p.sPRESENTCARDNO) : filteredjob.OrderBy(p => p.sPRESENTCARDNO);
                                    break;
                                case "9":
                                    filteredjob = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredjob.OrderByDescending(p => p.sCOMPANYCODE) : filteredjob.OrderBy(p => p.sCOMPANYCODE);
                                    break;
                                case "10":
                                    filteredjob = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredjob.OrderByDescending(p => p.sDivisionCode) : filteredjob.OrderBy(p => p.sDivisionCode);
                                    break;
                                case "11":
                                    filteredjob = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredjob.OrderByDescending(p => p.sCATCode) : filteredjob.OrderBy(p => p.sCATCode);
                                    break;
                                case "12":
                                    filteredjob = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredjob.OrderByDescending(p => p.sDepartmentCode) : filteredjob.OrderBy(p => p.sDepartmentCode);
                                    break;
                                case "13":
                                    filteredjob = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredjob.OrderByDescending(p => p.sGradeCode) : filteredjob.OrderBy(p => p.sGradeCode);
                                    break;
                                case "14":
                                    filteredjob = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredjob.OrderByDescending(p => p.sSEX) : filteredjob.OrderBy(p => p.sSEX);
                                    break;
                                case "15":
                                    filteredjob = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredjob.OrderByDescending(p => p.sISMARRIED) : filteredjob.OrderBy(p => p.sISMARRIED);
                                    break;
                                case "16":
                                    filteredjob = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredjob.OrderByDescending(p => p.sDESIGNATIONCODE) : filteredjob.OrderBy(p => p.sDESIGNATIONCODE);
                                    break;
                                default:
                                    filteredjob = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredjob.OrderByDescending(p => p.sPAYCODE) : filteredjob.OrderBy(p => p.sPAYCODE);
                                    break;
                            }
                            #endregion

                            var listjob = filteredjob.Skip(startRec).Take(pageSize).ToList()
                                .Select(d => new Employee()
                                {
                                    sPAYCODE = d.sPAYCODE,
                                    sEMPNAME = d.sEMPNAME,
                                    sACTIVE = d.sACTIVE,
                                    sGUARDIANNAME = d.sGUARDIANNAME,
                                    sDateOFBIRTH = d.sDateOFBIRTH,
                                    sDateOFJOIN = d.sDateOFJOIN,
                                    sPRESENTCARDNO = d.sPRESENTCARDNO,
                                    sCOMPANYCODE = d.sCOMPANYCODE,
                                    sDivisionCode = d.sDivisionCode,
                                    sSEX = d.sSEX,
                                    sISMARRIED = d.sISMARRIED,
                                    sCATCode = d.sCATCode,
                                    sDepartmentCode = d.sDepartmentCode,
                                    sGradeCode = d.sGradeCode,
                                    sDESIGNATIONCODE = d.sDESIGNATIONCODE,
                                    sSSN = d.sSSN

                                }).ToList();

                            if (listjob == null)
                                listjob = new List<Employee>();

                            return this.Json(new
                            {
                                draw = Convert.ToInt32(draw),
                                recordsTotal = TotalRecordsCount,
                                recordsFiltered = FilteredRecordCount,
                                data = listjob
                            }, JsonRequestBehavior.AllowGet);

                        }
                    }
                    else
                    {
                        return Json("error", JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return Json("error", JsonRequestBehavior.AllowGet);
            }
            return View();
        }

        ////Get all Employee Group list
        public JsonResult ListAllGrade(string isInsert)
        {
            Employee Em = new Employee();
            List<Grade_list> lst = new List<Grade_list>();
            try
            {
                Grade_list GrdM = new Grade_list();
                using (var client = new HttpClient())
                {
                    string userName = Session["UserName"].ToString();
                    //jpm.IsInsert = isInsert.Trim();
                    //jpm.LastModifiedBy = userName.Trim();
                    //Passing service base url  
                    client.BaseAddress = new Uri(APIUrl);
                    client.DefaultRequestHeaders.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                    var res = client.PostAsJsonAsync("Employee/GetGradeList", Em).Result;

                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        var lstResponse = res.Content.ReadAsStringAsync().Result;
                        //Deserializing the response recieved from web api and storing into the Employee list  
                        lst = JsonConvert.DeserializeObject<List<Grade_list>>(lstResponse);
                        if (lst.Count > 0)
                        {
                            foreach (Grade_list GrdMs in lst)
                            {
                                GrdM.GradeCode = GrdMs.GradeCode;
                                GrdM.GradeName = GrdMs.GradeName;
                            }
                            return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json("error", JsonRequestBehavior.AllowGet);
                    }
                }
                return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
            }
        }

        ////Add Employee list    
        [HttpPost]
        public JsonResult Add(Employee EmpMast)
        {
            try
            {
                //if (Request.Files.Count > 0)
                //{
                //    HttpFileCollectionBase files = Request.Files;

                //    HttpPostedFileBase file = files[0];
                //    string fname = EmpMast.sSSN.ToString().Trim().ToUpper()+".JPEG";

                //    // Checking for Internet Explorer  
                //    //if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                //    //{
                //    //    string[] testfiles = file.FileName.Split(new char[] { '\\' });
                //    //    fname = testfiles[testfiles.Length - 1];
                //    //}
                //    //else
                //    //{
                //    //    fname = file.FileName;
                //    //}
                //    // Get the complete folder path and store the file inside it.  

                //    fname = Path.Combine(Server.MapPath("~/EMPPHOTO/"), fname);
                //    file.SaveAs(fname);

                //    // Returns message that successfully uploaded  

                //    return Json("File Uploaded Successfully!");
                //}


                EmpMast.LastModifiedBy = Session["UserName"].ToString();
                EmpMast.LoginTerminalNameIP = Session["LoginIP"].ToString() + " " + Session["LoginTerminalName"].ToString();

                using (var client = new HttpClient())
                {
                    //Passing service base url  
                    client.BaseAddress = new Uri(APIUrl);

                    var res = client.PostAsJsonAsync("Employee/AddEmployee", EmpMast).Result;

                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        return Json("success", JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json("error", JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                ViewBag.Message = "Error occured, contact to administrator";
                return Json("error", JsonRequestBehavior.AllowGet);
            }
            //return Json(null);
        }

        //code for get job position by ID
        public JsonResult GetbyPaycode(string ID)
        {
            try
            {
                Employee EMP = new Employee();
                string userName = Session["UserName"].ToString();
                EMP.sSSN = ID;
                //JPM.LastModifiedBy = userName;
                using (var client = new HttpClient())
                {
                    //Passing service base url  
                    client.BaseAddress = new Uri(APIUrl);

                    var res = client.PostAsJsonAsync("Employee/GetEmployee", EMP).Result;

                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        var lstResponse = res.Content.ReadAsStringAsync().Result;

                        return Json(lstResponse, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json("error", JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return Json("error", JsonRequestBehavior.AllowGet);
            }

        }

        // get jd as per selection of company and department
        //[HttpGet]
        //public JsonResult BindJD(string compcode, string depcode,string isInsert)
        //{
        //    Employee jpm = new Employee();
        //    List<JDMaster_list> lst = new List<JDMaster_list>();
        //    try
        //    {
        //        JDMaster_list JML = new JDMaster_list();
        //        using (var client = new HttpClient())
        //        {

        //            string userName = Session["UserName"].ToString();

        //            jpm.CompCode = compcode.Trim();
        //            jpm.DepCode = depcode.Trim();
        //            jpm.LastModifiedBy = userName.Trim();
        //            jpm.IsInsert = isInsert.Trim();
        //            //Passing service base url  
        //            client.BaseAddress = new Uri(APIUrl);
        //            client.DefaultRequestHeaders.Clear();
        //            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        //            //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
        //            var res = client.PostAsJsonAsync("Employee/GetJDList", jpm).Result;

        //            //Checking the response is successful or not which is sent using HttpClient  
        //            if (res.IsSuccessStatusCode)
        //            {
        //                var lstResponse = res.Content.ReadAsStringAsync().Result;
        //                //Deserializing the response recieved from web api and storing into the Employee list  
        //                lst = JsonConvert.DeserializeObject<List<JDMaster_list>>(lstResponse);
        //                if (lst.Count > 0)
        //                {
        //                    foreach (JDMaster_list dms in lst)
        //                    {
        //                        JML.JobMasterId = dms.JobMasterId;
        //                        JML.JobMasterName = dms.JobMasterName;
        //                    }
        //                    return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
        //                }
        //            }
        //            else
        //            {
        //                return Json("error", JsonRequestBehavior.AllowGet);
        //            }
        //        }
        //        return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception ex)
        //    {
        //        ExceptionLogging.SendErrorToText(ex);
        //        return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
        //    }

        //}
        //get age as per the selection of JD
        public JsonResult BindAge(string jobID)
        {
            Employee jpm = new Employee();
            List<JobPosAgeMaster_list> lst = new List<JobPosAgeMaster_list>();
            try
            {
                JobPosAgeMaster_list JML = new JobPosAgeMaster_list();
                using (var client = new HttpClient())
                {

                    string userName = Session["UserName"].ToString();

                    //jpm.JobId = jobID.Trim();
                    //jpm.LastModifiedBy = userName.Trim();
                    //Passing service base url  
                    client.BaseAddress = new Uri(APIUrl);
                    client.DefaultRequestHeaders.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                    var res = client.PostAsJsonAsync("Employee/GetAgeList", jpm).Result;

                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        var lstResponse = res.Content.ReadAsStringAsync().Result;
                        //Deserializing the response recieved from web api and storing into the Employee list  
                        lst = JsonConvert.DeserializeObject<List<JobPosAgeMaster_list>>(lstResponse);
                        if (lst.Count > 0)
                        {
                            foreach (JobPosAgeMaster_list dms in lst)
                            {
                                JML.AgeId = dms.AgeId;
                                JML.AgeDesc = dms.AgeDesc;
                            }
                            return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json("error", JsonRequestBehavior.AllowGet);
                    }
                }
                return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
            }

        }
        //get interviewmode list
        //public JsonResult BindIterviewMode(string isInsert)
        //{
        //    Employee jpm = new Employee();
        //    List<JobPosIMMaster_list> lst = new List<JobPosIMMaster_list>();
        //    try
        //    {
        //        JobPosIMMaster_list JML = new JobPosIMMaster_list();
        //        using (var client = new HttpClient())
        //        {

        //            string userName = Session["UserName"].ToString();

        //            jpm.IsInsert = isInsert.Trim();
        //            jpm.LastModifiedBy = userName.Trim();
        //            //Passing service base url  
        //            client.BaseAddress = new Uri(APIUrl);
        //            client.DefaultRequestHeaders.Clear();
        //            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        //            //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
        //            var res = client.PostAsJsonAsync("Employee/GetInterviewMode", jpm).Result;

        //            //Checking the response is successful or not which is sent using HttpClient  
        //            if (res.IsSuccessStatusCode)
        //            {
        //                var lstResponse = res.Content.ReadAsStringAsync().Result;
        //                //Deserializing the response recieved from web api and storing into the Employee list  
        //                lst = JsonConvert.DeserializeObject<List<JobPosIMMaster_list>>(lstResponse);
        //                if (lst.Count > 0)
        //                {
        //                    foreach (JobPosIMMaster_list dms in lst)
        //                    {
        //                        JML.IMCode = dms.IMCode;
        //                        JML.IMDesc = dms.IMDesc;
        //                    }
        //                    return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
        //                }
        //            }
        //            else
        //            {
        //                return Json("error", JsonRequestBehavior.AllowGet);
        //            }
        //        }
        //        return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception ex)
        //    {
        //        ExceptionLogging.SendErrorToText(ex);
        //        return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
        //    }

        //}
        //getqualification list
        //public JsonResult BindQualification(string isInsert)
        //{
        //    Employee jpm = new Employee();
        //    List<JobPosQuaification_list> lst = new List<JobPosQuaification_list>();
        //    try
        //    {
        //        JobPosQuaification_list JML = new JobPosQuaification_list();
        //        using (var client = new HttpClient())
        //        {
        //            string userName = Session["UserName"].ToString();

        //            jpm.IsInsert = isInsert.Trim();
        //            jpm.LastModifiedBy = userName.Trim();
        //            //Passing service base url  
        //            client.BaseAddress = new Uri(APIUrl);
        //            client.DefaultRequestHeaders.Clear();
        //            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        //            //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
        //            var res = client.PostAsJsonAsync("Employee/GetQualification", jpm).Result;

        //            //Checking the response is successful or not which is sent using HttpClient  
        //            if (res.IsSuccessStatusCode)
        //            {
        //                var lstResponse = res.Content.ReadAsStringAsync().Result;
        //                //Deserializing the response recieved from web api and storing into the Employee list  
        //                lst = JsonConvert.DeserializeObject<List<JobPosQuaification_list>>(lstResponse);
        //                if (lst.Count > 0)
        //                {
        //                    foreach (JobPosQuaification_list dms in lst)
        //                    {
        //                        JML.QID = dms.QID;
        //                        JML.QDescc = dms.QDescc;
        //                    }
        //                    return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
        //                }
        //            }
        //            else
        //            {
        //                return Json("error", JsonRequestBehavior.AllowGet);
        //            }
        //        }
        //        return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception ex)
        //    {
        //        ExceptionLogging.SendErrorToText(ex);
        //        return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
        //    }

        //}
        //get language list
        //public JsonResult BindLanguage(string isInsert)
        //{
        //    Employee jpm = new Employee();
        //    List<JobPosLanguage_list> lst = new List<JobPosLanguage_list>();
        //    try
        //    {
        //        JobPosLanguage_list JML = new JobPosLanguage_list();
        //        using (var client = new HttpClient())
        //        {
        //            string userName = Session["UserName"].ToString();

        //            jpm.IsInsert = isInsert.Trim();
        //            jpm.LastModifiedBy = userName.Trim();
        //            //Passing service base url  
        //            client.BaseAddress = new Uri(APIUrl);
        //            client.DefaultRequestHeaders.Clear();
        //            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        //            //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
        //            var res = client.PostAsJsonAsync("Employee/GetLanguage", jpm).Result;

        //            //Checking the response is successful or not which is sent using HttpClient  
        //            if (res.IsSuccessStatusCode)
        //            {
        //                var lstResponse = res.Content.ReadAsStringAsync().Result;
        //                //Deserializing the response recieved from web api and storing into the Employee list  
        //                lst = JsonConvert.DeserializeObject<List<JobPosLanguage_list>>(lstResponse);
        //                if (lst.Count > 0)
        //                {
        //                    foreach (JobPosLanguage_list dms in lst)
        //                    {
        //                        JML.LangCode = dms.LangCode;
        //                        JML.LangDesc = dms.LangDesc;
        //                    }
        //                    return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
        //                }
        //            }
        //            else
        //            {
        //                return Json("error", JsonRequestBehavior.AllowGet);
        //            }
        //        }
        //        return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception ex)
        //    {
        //        ExceptionLogging.SendErrorToText(ex);
        //        return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
        //    }

        //}

        //get certification list
        //public JsonResult BindCertification(string isInsert)
        //{
        //    Employee em = new Employee();
        //    List<JobPosCertificate_list> lst = new List<JobPosCertificate_list>();
        //    try
        //    {
        //        JobPosCertificate_list JML = new JobPosCertificate_list();
        //        using (var client = new HttpClient())
        //        {
        //            string userName = Session["UserName"].ToString();

        //            em.IsInsert = isInsert.Trim();
        //            em.LastModifiedBy = userName.Trim();
        //            //Passing service base url  
        //            client.BaseAddress = new Uri(APIUrl);
        //            client.DefaultRequestHeaders.Clear();
        //            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        //            //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
        //            var res = client.PostAsJsonAsync("Employee/GetCertification", em).Result;

        //            //Checking the response is successful or not which is sent using HttpClient  
        //            if (res.IsSuccessStatusCode)
        //            {
        //                var lstResponse = res.Content.ReadAsStringAsync().Result;
        //                //Deserializing the response recieved from web api and storing into the Employee list  
        //                lst = JsonConvert.DeserializeObject<List<JobPosCertificate_list>>(lstResponse);
        //                if (lst.Count > 0)
        //                {
        //                    foreach (JobPosCertificate_list dms in lst)
        //                    {
        //                        JML.CertCode = dms.CertCode;
        //                        JML.CertName = dms.CertName;
        //                    }
        //                    return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
        //                }
        //            }
        //            else
        //            {
        //                return Json("error", JsonRequestBehavior.AllowGet);
        //            }
        //        }
        //        return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception ex)
        //    {
        //        ExceptionLogging.SendErrorToText(ex);
        //        return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
        //    }

        //}
        //get Job category list


        //get specialization field as per the selection of Qualification
        //public JsonResult BindSpecializationField(string appendStr)
        //{
        //    Employee jpm = new Employee();
        //    List<JobPosSpeclzn_list> lst = new List<JobPosSpeclzn_list>();
        //    try
        //    {
        //        JobPosSpeclzn_list JML = new JobPosSpeclzn_list();
        //        using (var client = new HttpClient())
        //        {
        //            string userName = Session["UserName"].ToString();
        //            jpm.QID = appendStr.Trim();
        //            jpm.LastModifiedBy = userName.Trim();
        //            //Passing service base url  
        //            client.BaseAddress = new Uri(APIUrl);
        //            client.DefaultRequestHeaders.Clear();
        //            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        //            //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
        //            var res = client.PostAsJsonAsync("Employee/GetSpecializationField", jpm).Result;

        //            //Checking the response is successful or not which is sent using HttpClient  
        //            if (res.IsSuccessStatusCode)
        //            {
        //                var lstResponse = res.Content.ReadAsStringAsync().Result;
        //                //Deserializing the response recieved from web api and storing into the Employee list  
        //                lst = JsonConvert.DeserializeObject<List<JobPosSpeclzn_list>>(lstResponse);
        //                if (lst.Count > 0)
        //                {
        //                    foreach (JobPosSpeclzn_list dms in lst)
        //                    {
        //                        JML.SpecLznID = dms.SpecLznID;
        //                        JML.SpecLznDesc = dms.SpecLznDesc;
        //                    }
        //                    return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
        //                }
        //            }
        //            else
        //            {
        //                return Json("error", JsonRequestBehavior.AllowGet);
        //            }
        //        }
        //        return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception ex)
        //    {
        //        ExceptionLogging.SendErrorToText(ex);
        //        return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
        //    }

        //}
        ////get primary skill set list list as per the selection of JD
        //public JsonResult BindPrimarySillset(string jobID)
        //{
        //    Employee jpm = new Employee();
        //    List<JobPosPrimarySkill_list> lst = new List<JobPosPrimarySkill_list>();
        //    try
        //    {
        //        JobPosPrimarySkill_list JML = new JobPosPrimarySkill_list();
        //        using (var client = new HttpClient())
        //        {

        //            string userName = Session["UserName"].ToString();

        //            jpm.JobId = jobID.Trim();
        //            jpm.LastModifiedBy = userName.Trim();
        //            //Passing service base url  
        //            client.BaseAddress = new Uri(APIUrl);
        //            client.DefaultRequestHeaders.Clear();
        //            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        //            //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
        //            var res = client.PostAsJsonAsync("Employee/GetPrimarySkill", jpm).Result;

        //            //Checking the response is successful or not which is sent using HttpClient  
        //            if (res.IsSuccessStatusCode)
        //            {
        //                var lstResponse = res.Content.ReadAsStringAsync().Result;
        //                //Deserializing the response recieved from web api and storing into the Employee list  
        //                lst = JsonConvert.DeserializeObject<List<JobPosPrimarySkill_list>>(lstResponse);
        //                if (lst.Count > 0)
        //                {
        //                    foreach (JobPosPrimarySkill_list dms in lst)
        //                    {
        //                        JML.pskillID = dms.pskillID;
        //                        JML.pskillName = dms.pskillName;
        //                    }
        //                    return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
        //                }
        //            }
        //            else
        //            {
        //                return Json("error", JsonRequestBehavior.AllowGet);
        //            }
        //        }
        //        return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception ex)
        //    {
        //        ExceptionLogging.SendErrorToText(ex);
        //        return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
        //    }

        //}
        ////get secondary skill set as per the selection of JD
        //public JsonResult BindSecondarySillset(string jobID)
        //{
        //    Employee jpm = new Employee();
        //    List<JobPosSecondarySkill_list> lst = new List<JobPosSecondarySkill_list>();
        //    try
        //    {
        //        JobPosSecondarySkill_list JML = new JobPosSecondarySkill_list();
        //        using (var client = new HttpClient())
        //        {

        //            string userName = Session["UserName"].ToString();

        //            jpm.JobId = jobID.Trim();
        //            jpm.LastModifiedBy = userName.Trim();
        //            //Passing service base url  
        //            client.BaseAddress = new Uri(APIUrl);
        //            client.DefaultRequestHeaders.Clear();
        //            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        //            //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
        //            var res = client.PostAsJsonAsync("Employee/GetSecondarySkill", jpm).Result;

        //            //Checking the response is successful or not which is sent using HttpClient  
        //            if (res.IsSuccessStatusCode)
        //            {
        //                var lstResponse = res.Content.ReadAsStringAsync().Result;
        //                //Deserializing the response recieved from web api and storing into the Employee list  
        //                lst = JsonConvert.DeserializeObject<List<JobPosSecondarySkill_list>>(lstResponse);
        //                if (lst.Count > 0)
        //                {
        //                    foreach (JobPosSecondarySkill_list dms in lst)
        //                    {
        //                        JML.sskillID = dms.sskillID;
        //                        JML.sskillName = dms.sskillName;
        //                    }
        //                    return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
        //                }
        //            }
        //            else
        //            {
        //                return Json("error", JsonRequestBehavior.AllowGet);
        //            }
        //        }
        //        return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception ex)
        //    {
        //        ExceptionLogging.SendErrorToText(ex);
        //        return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
        //    }

        //}
        ////get  total experience as per the selection of JD
        //public JsonResult BindExpList(string jobID)
        //{
        //    Employee jpm = new Employee();
        //    List<JobPosTotalExp_list> lst = new List<JobPosTotalExp_list>();
        //    try
        //    {
        //        JobPosTotalExp_list JML = new JobPosTotalExp_list();
        //        using (var client = new HttpClient())
        //        {

        //            string userName = Session["UserName"].ToString();

        //            jpm.JobId = jobID.Trim();
        //            jpm.LastModifiedBy = userName.Trim();
        //            //Passing service base url  
        //            client.BaseAddress = new Uri(APIUrl);
        //            client.DefaultRequestHeaders.Clear();
        //            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        //            //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
        //            var res = client.PostAsJsonAsync("Employee/GetTotalExp", jpm).Result;

        //            //Checking the response is successful or not which is sent using HttpClient  
        //            if (res.IsSuccessStatusCode)
        //            {
        //                var lstResponse = res.Content.ReadAsStringAsync().Result;
        //                //Deserializing the response recieved from web api and storing into the Employee list  
        //                lst = JsonConvert.DeserializeObject<List<JobPosTotalExp_list>>(lstResponse);
        //                if (lst.Count > 0)
        //                {
        //                    foreach (JobPosTotalExp_list dms in lst)
        //                    {
        //                        JML.ExpId = dms.ExpId;
        //                        JML.ExpDesc = dms.ExpDesc;
        //                    }
        //                    return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
        //                }
        //            }
        //            else
        //            {
        //                return Json("error", JsonRequestBehavior.AllowGet);
        //            }
        //        }
        //        return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception ex)
        //    {
        //        ExceptionLogging.SendErrorToText(ex);
        //        return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
        //    }

        //}

        ////get  relevant  experience as per the selection of JD
        //public JsonResult BindRelvExpList(string jobID)
        //{
        //    Employee jpm = new Employee();
        //    List<JobPosRelv_list> lst = new List<JobPosRelv_list>();
        //    try
        //    {
        //        JobPosRelv_list JML = new JobPosRelv_list();
        //        using (var client = new HttpClient())
        //        {

        //            string userName = Session["UserName"].ToString();

        //            jpm.JobId = jobID.Trim();
        //            jpm.LastModifiedBy = userName.Trim();
        //            //Passing service base url  
        //            client.BaseAddress = new Uri(APIUrl);
        //            client.DefaultRequestHeaders.Clear();
        //            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        //            //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
        //            var res = client.PostAsJsonAsync("Employee/GetRelvExp", jpm).Result;

        //            //Checking the response is successful or not which is sent using HttpClient  
        //            if (res.IsSuccessStatusCode)
        //            {
        //                var lstResponse = res.Content.ReadAsStringAsync().Result;
        //                //Deserializing the response recieved from web api and storing into the Employee list  
        //                lst = JsonConvert.DeserializeObject<List<JobPosRelv_list>>(lstResponse);
        //                if (lst.Count > 0)
        //                {
        //                    foreach (JobPosRelv_list dms in lst)
        //                    {
        //                        JML.RelvExpId = dms.RelvExpId;
        //                        JML.RelvExpDesc = dms.RelvExpDesc;
        //                    }
        //                    return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
        //                }
        //            }
        //            else
        //            {
        //                return Json("error", JsonRequestBehavior.AllowGet);
        //            }
        //        }
        //        return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception ex)
        //    {
        //        ExceptionLogging.SendErrorToText(ex);
        //        return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
        //    }

        //}

        //get all job position list
        public ActionResult GetAllJobPos()
        {
            Employee jm = new Employee();
            List<Employee> lst = new List<Employee>();
            try
            {
                string userName = Session["UserName"].ToString();
                //jm.LastModifiedBy = userName;
                string draw = Request.Form.GetValues("draw")[0];
                string order = Request.Form.GetValues("order[0][column]")[0];
                string orderDir = Request.Form.GetValues("order[0][dir]")[0];
                int startRec = Convert.ToInt32(Request.Form.GetValues("start")[0]);
                int pageSize = Convert.ToInt32(Request.Form.GetValues("length")[0]);
                string search = Request.Form.GetValues("search[value]").FirstOrDefault();

                ///

                ///
                using (var client = new HttpClient())
                {

                    client.BaseAddress = new Uri(APIUrl);

                    var res = client.PostAsJsonAsync("Employee/GetAllJobPos", jm).Result;

                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {

                        var lstResponse = res.Content.ReadAsStringAsync().Result;
                        //Deserializing the response recieved from web api and storing into the Employee list  
                        lst = JsonConvert.DeserializeObject<List<Employee>>(lstResponse);

                        if (lst.Count > 0)
                        {
                            foreach (Employee jms in lst)
                            {
                                jm.sPAYCODE = jms.sPAYCODE;
                                jm.sEMPNAME = jms.sEMPNAME;
                                jm.sACTIVE = jms.sACTIVE;
                            }
                        }
                        if (Request.IsAjaxRequest())
                        {
                            ///

                            // Get Exp Records.   
                            var allJobdes = lst;
                            //
                            IEnumerable<Employee> filteredjob = lst;

                            long TotalRecordsCount = lst.Count();

                            #region filters  

                            if (!string.IsNullOrEmpty(search))
                            {
                                filteredjob = allJobdes.Where(
                                u => u.sPAYCODE.ToUpper().Contains(search.ToUpper())
                                || u.sEMPNAME.ToUpper().Contains(search.ToUpper())
                                || u.sACTIVE.ToUpper().Contains(search.ToUpper())
                                ).ToList();
                            }
                            #endregion
                            long FilteredRecordCount = lst.Count();

                            #region Sorting  

                            // Sorting     
                            switch (order)
                            {
                                case "0":
                                    filteredjob = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredjob.OrderByDescending(p => p.sPAYCODE) : filteredjob.OrderBy(p => p.sPAYCODE);
                                    break;
                                case "1":
                                    filteredjob = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredjob.OrderByDescending(p => p.sPAYCODE) : filteredjob.OrderBy(p => p.sPAYCODE);
                                    break;
                                case "2":
                                    filteredjob = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredjob.OrderByDescending(p => p.sPAYCODE) : filteredjob.OrderBy(p => p.sPAYCODE);
                                    break;
                                case "3":
                                    filteredjob = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredjob.OrderByDescending(p => p.sEMPNAME) : filteredjob.OrderBy(p => p.sEMPNAME);
                                    break;
                                default:
                                    filteredjob = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredjob.OrderByDescending(p => p.sACTIVE) : filteredjob.OrderBy(p => p.sACTIVE);
                                    break;
                            }
                            #endregion

                            var listjob = filteredjob.Skip(startRec).Take(pageSize).ToList()
                                .Select(d => new Employee()
                                {
                                    sPAYCODE = d.sPAYCODE,
                                    sEMPNAME = d.sEMPNAME,
                                    sACTIVE = d.sACTIVE

                                }).ToList();

                            if (listjob == null)
                                listjob = new List<Employee>();

                            return this.Json(new
                            {
                                draw = Convert.ToInt32(draw),
                                recordsTotal = TotalRecordsCount,
                                recordsFiltered = FilteredRecordCount,
                                data = listjob
                            }, JsonRequestBehavior.AllowGet);

                        }
                    }
                    else
                    {
                        return Json("error", JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return Json("error", JsonRequestBehavior.AllowGet);
            }



            return View();
        }
        // code for insert job position
        //public JsonResult Add(Employee JPM)
        //{
        //    try
        //    {
        //        //JPM.LastModifiedBy = Session["UserName"].ToString();

        //        //JPM.LoginTerminalNameIP = Session["LoginIP"].ToString() + " " + Session["LoginTerminalName"].ToString();
        //        using (var client = new HttpClient())
        //        {
        //            //Passing service base url  
        //            client.BaseAddress = new Uri(APIUrl);

        //            var res = client.PostAsJsonAsync("Employee/InsertJobPos", JPM).Result;

        //            //Checking the response is successful or not which is sent using HttpClient  
        //            if (res.IsSuccessStatusCode)
        //            {
        //                var serlzData = res.Content.ReadAsStringAsync().Result;
        //                var deSerlzData = JsonConvert.DeserializeObject(serlzData);
        //                return Json(deSerlzData, JsonRequestBehavior.AllowGet);
        //            }
        //            else
        //            {
        //                return Json("error", JsonRequestBehavior.AllowGet);
        //            }
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        ExceptionLogging.SendErrorToText(ex);
        //        ViewBag.Message = "Error occured, contact to administrator";
        //        return Json("error", JsonRequestBehavior.AllowGet);
        //    }
        //}

        //code for update job postion
        public JsonResult Update(Employee JPM)
        {
            try
            {
                //JPM.LastModifiedBy = Session["UserName"].ToString();

                //JPM.LoginTerminalNameIP = Session["LoginIP"].ToString() + " " + Session["LoginTerminalName"].ToString();
                using (var client = new HttpClient())
                {
                    //Passing service base url  
                    client.BaseAddress = new Uri(APIUrl);

                    var res = client.PostAsJsonAsync("Employee/Update", JPM).Result;

                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        return Json("success", JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json("error", JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return Json("error", JsonRequestBehavior.AllowGet);
            }
        }
        //code for get Employee by ID
        public JsonResult GetbyID(string ID)
        {
            try
            {
                Employee emp = new Employee();
                string userName = Session["UserName"].ToString();
                emp.sPAYCODE = ID;
                //JPM.LastModifiedBy = userName;
                using (var client = new HttpClient())
                {
                    //Passing service base url  
                    client.BaseAddress = new Uri(APIUrl);

                    var res = client.PostAsJsonAsync("Employee/GetbyEmployeecode", emp).Result;

                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        var lstResponse = res.Content.ReadAsStringAsync().Result;

                        return Json(lstResponse, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json("error", JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return Json("error", JsonRequestBehavior.AllowGet);
            }

        }
        //delete job position master
        public JsonResult Delete(string ID)
        {
            try
            {
                Employee em = new Employee();
                em.sSSN = ID.Trim();
                //JPM.LastModifiedBy = Session["UserName"].ToString();
                //JPM.LoginTerminalNameIP = Session["LoginIP"].ToString() + " " + Session["LoginTerminalName"].ToString();
                using (var client = new HttpClient())
                {
                    //Passing service base url  
                    client.BaseAddress = new Uri(APIUrl);

                    var res = client.PostAsJsonAsync("Employee/DeleteRecord", em).Result;

                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        var serlzData = res.Content.ReadAsStringAsync().Result;
                        var deSerlzData = JsonConvert.DeserializeObject(serlzData);
                        return Json(deSerlzData, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json("error", JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return Json("error", JsonRequestBehavior.AllowGet);
            }
        }
        //get auto id
        public JsonResult GetAutoGenerateID()
        {
            try
            {
                Employee JPM = new Employee();
                //JPM.LastModifiedBy = Session["UserName"].ToString();
                //JPM.LoginTerminalNameIP = Session["LoginIP"].ToString() + " " + Session["LoginTerminalName"].ToString();
                using (var client = new HttpClient())
                {
                    //Passing service base url  
                    client.BaseAddress = new Uri(APIUrl);

                    var res = client.PostAsJsonAsync("Employee/GetAutoID", JPM).Result;

                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        var serlzData = res.Content.ReadAsStringAsync().Result;
                        var deSerlzData = JsonConvert.DeserializeObject(serlzData);
                        return Json(deSerlzData, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json("error", JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return Json("error", JsonRequestBehavior.AllowGet);
            }
        }
    }
}