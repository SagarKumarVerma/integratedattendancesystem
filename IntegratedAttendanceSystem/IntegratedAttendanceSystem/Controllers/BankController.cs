﻿using IntegratedAttendanceSystem.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace IntegratedAttendanceSystem.Controllers
{
    public class BankController : Controller
    {
        // GET: Bank
        string APIUrl = ConfigurationManager.AppSettings["APIURL"].ToString();
        public ActionResult Bank()
        {
            try
            {
                return View();
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return View();
            }
        }
        public JsonResult List()
        {
            try
            {
                BankModel BankMod = new BankModel();
                List<BankModel> lst = new List<BankModel>();
                BankMod.LastModifiedBy = Session["UserName"].ToString();
                BankMod.LoginTerminalNameIP = Session["LoginIP"].ToString() + " " + Session["LoginTerminalName"].ToString();

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIUrl);
                    var res = client.PostAsJsonAsync("Bank/GetAllList", BankMod).Result;
                    if (res.IsSuccessStatusCode)
                    {
                        var lstResponse = res.Content.ReadAsStringAsync().Result;
                        lst = JsonConvert.DeserializeObject<List<BankModel>>(lstResponse);
                    }
                }
                return Json(lst, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
            }

        }
        //Get all Bank Master for jquery data table
        [HttpPost]
        public ActionResult GetAllBank()
        {
            BankModel BankMst = new BankModel();
            List<BankModel> lst = new List<BankModel>();
            try
            {
                string userName = Session["UserName"].ToString();
                BankMst.LastModifiedBy = userName;
                string draw = Request.Form.GetValues("draw")[0];
                string order = Request.Form.GetValues("order[0][column]")[0];
                string orderDir = Request.Form.GetValues("order[0][dir]")[0];
                int startRec = Convert.ToInt32(Request.Form.GetValues("start")[0]);
                int pageSize = Convert.ToInt32(Request.Form.GetValues("length")[0]);
                string search = Request.Form.GetValues("search[value]").FirstOrDefault();

                ///

                ///
                using (var client = new HttpClient())
                {

                    client.BaseAddress = new Uri(APIUrl);

                    var res = client.PostAsJsonAsync("Bank/GetAllList", BankMst).Result;

                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {

                        var lstResponse = res.Content.ReadAsStringAsync().Result;
                        //Deserializing the response recieved from web api and storing into the Employee list  
                        lst = JsonConvert.DeserializeObject<List<BankModel>>(lstResponse);

                        if (lst.Count > 0)
                        {
                            foreach (BankModel BankM in lst)
                            {
                                BankMst.BankCode = BankM.BankCode;
                                BankMst.BankName = BankM.BankName;
                                BankMst.IFSCCode = BankM.IFSCCode;
                                BankMst.Address = BankM.Address;
                                BankMst.ShortName = BankM.ShortName;
                            }
                        }
                        if (Request.IsAjaxRequest())
                        {
                            // Get Exp Records.   
                            var allBank = lst;
                            //
                            IEnumerable<BankModel> filteredBank = lst;

                            long TotalRecordsCount = lst.Count();

                            #region filters  

                            if (!string.IsNullOrEmpty(search))
                            {
                                filteredBank = allBank.Where(
                                u => u.BankCode.ToUpper().Contains(search.ToUpper())
                                || u.BankName.ToUpper().Contains(search.ToUpper())
                                || u.IFSCCode.ToUpper().Contains(search.ToUpper())
                                || u.Address.ToUpper().Contains(search.ToUpper())
                                || u.ShortName.ToUpper().Contains(search.ToUpper())
                                ).ToList();
                            }
                            #endregion
                            long FilteredRecordCount = lst.Count();

                            #region Sorting  

                            // Sorting     
                            switch (order)
                            {
                                case "0":
                                    filteredBank = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredBank.OrderByDescending(p => p.BankCode) : filteredBank.OrderBy(p => p.BankCode);
                                    break;
                                case "1":
                                    filteredBank = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredBank.OrderByDescending(p => p.BankCode) : filteredBank.OrderBy(p => p.BankCode);
                                    break;
                                case "2":
                                    filteredBank = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredBank.OrderByDescending(p => p.BankCode) : filteredBank.OrderBy(p => p.BankCode);
                                    break;
                                case "3":
                                    filteredBank = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredBank.OrderByDescending(p => p.BankName) : filteredBank.OrderBy(p => p.BankName);
                                    break;
                                case "4":
                                    filteredBank = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredBank.OrderByDescending(p => p.IFSCCode) : filteredBank.OrderBy(p => p.IFSCCode);
                                    break;
                                case "5":
                                    filteredBank = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredBank.OrderByDescending(p => p.Address) : filteredBank.OrderBy(p => p.Address);
                                    break;
                                case "6":
                                    filteredBank = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredBank.OrderByDescending(p => p.ShortName) : filteredBank.OrderBy(p => p.ShortName);
                                    break;
                                default:
                                    filteredBank = filteredBank.OrderByDescending(p => p.BankCode);
                                    break;
                            }
                            #endregion

                            var listBank = filteredBank.Skip(startRec).Take(pageSize).ToList()
                                .Select(pcm => new BankModel()
                                {
                                    BankCode = pcm.BankCode,
                                    BankName = pcm.BankName,
                                    IFSCCode = pcm.IFSCCode,
                                    Address = pcm.Address,
                                    ShortName = pcm.ShortName,

                                }).ToList();

                            if (listBank == null)
                                listBank = new List<BankModel>();

                            return this.Json(new
                            {
                                draw = Convert.ToInt32(draw),
                                recordsTotal = TotalRecordsCount,
                                recordsFiltered = FilteredRecordCount,
                                data = listBank
                            }, JsonRequestBehavior.AllowGet);

                        }
                    }
                    else
                    {
                        return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
            }
            return View();
        }
        //To Add new Bank record   
        public JsonResult Add(BankModel BNKADD)
        {
            try
            {
                BNKADD.LastModifiedBy = Session["UserName"].ToString();
                BNKADD.LoginTerminalNameIP = Session["LoginIP"].ToString() + " " + Session["LoginTerminalName"].ToString();
                using (var client = new HttpClient())
                {
                    //Passing service base url  
                    client.BaseAddress = new Uri(APIUrl);
                    var res = client.PostAsJsonAsync("Bank/InsertRecord", BNKADD).Result;
                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        return Json("success", JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
                    }
                }

            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
            }
        }
        //feth Bank Master based on  code
        public JsonResult GetbyID(string ID)
        {
            try
            {
                BankModel BNKID = new BankModel();
                string userName = Session["UserName"].ToString();
                BNKID.BankCode = ID;
                BNKID.LastModifiedBy = userName;
                using (var client = new HttpClient())
                {
                    //Passing service base url  
                    client.BaseAddress = new Uri(APIUrl);
                    var res = client.PostAsJsonAsync("Bank/GetSpecRec", BNKID).Result;
                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        var lstResponse = res.Content.ReadAsStringAsync().Result;
                        return Json(lstResponse, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
            }

        }
        //Update Bank Master 
        public JsonResult Update(BankModel BNKUPD)
        {
            try
            {
                BNKUPD.LastModifiedBy = Session["UserName"].ToString();
                BNKUPD.LoginTerminalNameIP = Session["LoginIP"].ToString() + " " + Session["LoginTerminalName"].ToString();
                using (var client = new HttpClient())
                {
                    //Passing service base url  
                    client.BaseAddress = new Uri(APIUrl);
                    var res = client.PostAsJsonAsync("Bank/UpdateRecord", BNKUPD).Result;
                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        return Json("success", JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
            }

        }
        //Delete Bank Master
        public JsonResult Delete(string ID)
        {
            try
            {
                BankModel BNKEDEL = new BankModel();
                BNKEDEL.BankCode = ID;
                BNKEDEL.LastModifiedBy = Session["UserName"].ToString();
                BNKEDEL.LoginTerminalNameIP = Session["LoginIP"].ToString() + " " + Session["LoginTerminalName"].ToString();
                using (var client = new HttpClient())
                {
                    //Passing service base url  
                    client.BaseAddress = new Uri(APIUrl);
                    var res = client.PostAsJsonAsync("Bank/DeleteRecord", BNKEDEL).Result;
                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        return Json("success", JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
            }

        }
        //feth Bank Master based on  code
        public JsonResult GetbyBankcode(string ID)
        {
            try
            {
                BankModel BNKID = new BankModel();
                string userName = Session["UserName"].ToString();
                BNKID.BankCode = ID;
                BNKID.LastModifiedBy = userName;
                using (var client = new HttpClient())
                {
                    //Passing service base url  
                    client.BaseAddress = new Uri(APIUrl);
                    var res = client.PostAsJsonAsync("Bank/GetBankcode", BNKID).Result;
                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        var lstResponse = res.Content.ReadAsStringAsync().Result;
                        return Json(lstResponse, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
            }
        }
    }
}