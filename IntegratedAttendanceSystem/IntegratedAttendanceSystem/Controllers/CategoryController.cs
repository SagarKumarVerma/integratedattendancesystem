﻿using IntegratedAttendanceSystem.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace IntegratedAttendanceSystem.Controllers
{
    public class CategoryController : Controller
    {
        // GET: Category
        string APIUrl = ConfigurationManager.AppSettings["APIURL"].ToString();
        public ActionResult Category()
        {
            try
            {
                return View();
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return View();
            }
        }
        public JsonResult List()
        {
            try
            {
                CategoryModel CateModel = new CategoryModel();
                List<CategoryModel> lst = new List<CategoryModel>();
                CateModel.LastModifiedBy = Session["UserName"].ToString();
                CateModel.LoginTerminalNameIP = Session["LoginIP"].ToString() + " " + Session["LoginTerminalName"].ToString();

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIUrl);
                    var res = client.PostAsJsonAsync("Category/GetAllList", CateModel).Result;
                    if (res.IsSuccessStatusCode)
                    {
                        var lstResponse = res.Content.ReadAsStringAsync().Result;
                        lst = JsonConvert.DeserializeObject<List<CategoryModel>>(lstResponse);
                    }
                }
                return Json(lst, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
            }

        }
        //Get all Categry Master for jquery data table
        [HttpPost]
        public ActionResult GetAllCategory()
        {
            CategoryModel CateMst = new CategoryModel();
            List<CategoryModel> lst = new List<CategoryModel>();
            try
            {
                string userName = Session["UserName"].ToString();
                CateMst.LastModifiedBy = userName;
                string draw = Request.Form.GetValues("draw")[0];
                string order = Request.Form.GetValues("order[0][column]")[0];
                string orderDir = Request.Form.GetValues("order[0][dir]")[0];
                int startRec = Convert.ToInt32(Request.Form.GetValues("start")[0]);
                int pageSize = Convert.ToInt32(Request.Form.GetValues("length")[0]);
                string search = Request.Form.GetValues("search[value]").FirstOrDefault();

                ///

                ///
                using (var client = new HttpClient())
                {

                    client.BaseAddress = new Uri(APIUrl);

                    var res = client.PostAsJsonAsync("Category/GetAllList", CateMst).Result;

                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {

                        var lstResponse = res.Content.ReadAsStringAsync().Result;
                        //Deserializing the response recieved from web api and storing into the Employee list  
                        lst = JsonConvert.DeserializeObject<List<CategoryModel>>(lstResponse);

                        if (lst.Count > 0)
                        {
                            foreach (CategoryModel CatM in lst)
                            {
                                CateMst.IsActive = CatM.IsActive;
                                CateMst.CategoryCode = CatM.CategoryCode;
                                CateMst.CategoryName = CatM.CategoryName;
                            }
                        }
                        if (Request.IsAjaxRequest())
                        {
                            ///

                            // Get Exp Records.   
                            var allCategory = lst;
                            //
                            IEnumerable<CategoryModel> filteredCategory = lst;

                            long TotalRecordsCount = lst.Count();

                            #region filters  

                            if (!string.IsNullOrEmpty(search))
                            {
                                filteredCategory = allCategory.Where(
                                u => u.IsActive.ToUpper().Contains(search.ToUpper())
                                || u.CategoryCode.ToUpper().Contains(search.ToUpper())
                                || u.CategoryName.ToUpper().Contains(search.ToUpper())
                                ).ToList();
                            }
                            #endregion
                            long FilteredRecordCount = lst.Count();

                            #region Sorting  

                            // Sorting     
                            switch (order)
                            {
                                case "0":
                                    filteredCategory = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredCategory.OrderByDescending(p => p.CategoryCode) : filteredCategory.OrderBy(p => p.CategoryCode);
                                    break;
                                case "1":
                                    filteredCategory = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredCategory.OrderByDescending(p => p.CategoryCode) : filteredCategory.OrderBy(p => p.CategoryCode);
                                    break;
                                case "2":
                                    filteredCategory = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredCategory.OrderByDescending(p => p.IsActive) : filteredCategory.OrderBy(p => p.IsActive);
                                    break;
                                case "3":
                                    filteredCategory = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredCategory.OrderByDescending(p => p.CategoryCode) : filteredCategory.OrderBy(p => p.CategoryCode);
                                    break;
                                case "4":
                                    filteredCategory = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredCategory.OrderByDescending(p => p.CategoryName) : filteredCategory.OrderBy(p => p.CategoryName);
                                    break;
                               
                                default:
                                    filteredCategory = filteredCategory.OrderByDescending(p => p.CategoryCode);
                                    break;
                            }
                            #endregion

                            var listCate = filteredCategory.Skip(startRec).Take(pageSize).ToList()
                                .Select(pcm => new CategoryModel()
                                {
                                    IsActive = pcm.IsActive,
                                    CategoryCode = pcm.CategoryCode,
                                    CategoryName = pcm.CategoryName,
                                    
                                }).ToList();

                            if (listCate == null)
                                listCate = new List<CategoryModel>();

                            return this.Json(new
                            {
                                draw = Convert.ToInt32(draw),
                                recordsTotal = TotalRecordsCount,
                                recordsFiltered = FilteredRecordCount,
                                data = listCate
                            }, JsonRequestBehavior.AllowGet);

                        }
                    }
                    else
                    {
                        return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
            }
            return View();
        }
        //To Add new Category record   
        public JsonResult Add(CategoryModel CATEADD)
        {
            try
            {
                CATEADD.LastModifiedBy = Session["UserName"].ToString();
                CATEADD.LoginTerminalNameIP = Session["LoginIP"].ToString() + " " + Session["LoginTerminalName"].ToString();
                using (var client = new HttpClient())
                {
                    //Passing service base url  
                    client.BaseAddress = new Uri(APIUrl);
                    var res = client.PostAsJsonAsync("Category/InsertRecord", CATEADD).Result;
                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        return Json("success", JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
                    }
                }

            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
            }
        }
        //feth Category Master based on  code
        public JsonResult GetbyID(string ID)
        {
            try
            {
                CategoryModel CATEID = new CategoryModel();
                string userName = Session["UserName"].ToString();
                CATEID.CategoryCode = ID;
                CATEID.LastModifiedBy = userName;
                using (var client = new HttpClient())
                {
                    //Passing service base url  
                    client.BaseAddress = new Uri(APIUrl);
                    var res = client.PostAsJsonAsync("Category/GetSpecRec", CATEID).Result;
                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        var lstResponse = res.Content.ReadAsStringAsync().Result;
                        return Json(lstResponse, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
            }

        }
        //Update Category Master 
        public JsonResult Update(CategoryModel CATEUPD)
        {
            try
            {
                CATEUPD.LastModifiedBy = Session["UserName"].ToString();
                CATEUPD.LoginTerminalNameIP = Session["LoginIP"].ToString() + " " + Session["LoginTerminalName"].ToString();
                using (var client = new HttpClient())
                {
                    //Passing service base url  
                    client.BaseAddress = new Uri(APIUrl);
                    var res = client.PostAsJsonAsync("Category/UpdateRecord", CATEUPD).Result;
                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        return Json("success", JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
            }

        }
        //Delete Category Master
        public JsonResult Delete(string ID)
        {
            try
            {
                CategoryModel CATEDEL = new CategoryModel();
                CATEDEL.CategoryCode = ID;
                CATEDEL.LastModifiedBy = Session["UserName"].ToString();
                CATEDEL.LoginTerminalNameIP = Session["LoginIP"].ToString() + " " + Session["LoginTerminalName"].ToString();
                using (var client = new HttpClient())
                {
                    //Passing service base url  
                    client.BaseAddress = new Uri(APIUrl);
                    var res = client.PostAsJsonAsync("Category/DeleteRecord", CATEDEL).Result;
                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        return Json("success", JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
            }

        }
        //feth Category Master based on  code
        public JsonResult GetbyCatecode(string ID)
        {
            try
            {
                CategoryModel CATEID = new CategoryModel();
                string userName = Session["UserName"].ToString();
                CATEID.CategoryCode = ID;
                CATEID.LastModifiedBy = userName;
                using (var client = new HttpClient())
                {
                    //Passing service base url  
                    client.BaseAddress = new Uri(APIUrl);
                    var res = client.PostAsJsonAsync("Category/GetCatecode", CATEID).Result;
                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        var lstResponse = res.Content.ReadAsStringAsync().Result;
                        return Json(lstResponse, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
            }
        }
    }
}