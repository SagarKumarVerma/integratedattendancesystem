﻿using IntegratedAttendanceSystem.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Mvc;

namespace MVCEventCalendar.Controllers
{
    public class DataMaintenanceController : Controller
    {
        // GET: Home
        string APIUrl = ConfigurationManager.AppSettings["APIURL"].ToString();
        public ActionResult DataMaintenance()
        {
            try
            {
                return View();
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return View();
            }
        }

        public JsonResult GetEvents()
        {
            try
            {
                CompanyModel Company = new CompanyModel();
                List<CompanyModel> lst = new List<CompanyModel>();
                Company.LastModifiedBy = Session["UserName"].ToString();
                Company.LoginTerminalNameIP = Session["LoginIP"].ToString() + " " + Session["LoginTerminalName"].ToString();
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIUrl);
                    var res = client.PostAsJsonAsync("Company/GetAllList", Company).Result;

                    if (res.IsSuccessStatusCode)
                    {
                        var lstResponse = res.Content.ReadAsStringAsync().Result;
                        lst = JsonConvert.DeserializeObject<List<CompanyModel>>(lstResponse);
                    }
                }
                return Json(lst, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetEvents1()
        {
            try
            {
                DataMaintenanceModel DM_mPunch = new DataMaintenanceModel();
                List<DataMaintenanceModel> lst = new List<DataMaintenanceModel>();
                DM_mPunch.LastModifiedBy = Session["UserName"].ToString();
                DM_mPunch.LoginTerminalNameIP = Session["LoginIP"].ToString() + " " + Session["LoginTerminalName"].ToString();
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIUrl);
                    var res = client.PostAsJsonAsync("DataMaintenance/GetAllAtt", DM_mPunch).Result;

                    if (res.IsSuccessStatusCode)
                    {
                        var lstResponse = res.Content.ReadAsStringAsync().Result;
                        lst = JsonConvert.DeserializeObject<List<DataMaintenanceModel>>(lstResponse);
                    }
                }
                return Json(lst, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult EmployeecodeList()
        {
            DataMaintenanceModel AdvLonStpMdl = new DataMaintenanceModel();
            List<DataMaintenanceModel.EmployeeCode_list> lst = new List<DataMaintenanceModel.EmployeeCode_list>();
            try
            {
                string userName = Session["UserName"].ToString();
                DataMaintenanceModel.EmployeeCode_list AdvLonStpMdlEmpCode = new DataMaintenanceModel.EmployeeCode_list();
                using (var client = new HttpClient())
                {
                    //Passing service base url  
                    client.BaseAddress = new Uri(APIUrl);
                    client.DefaultRequestHeaders.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                    var res = client.PostAsJsonAsync("DataMaintenance/GetAllEmployeeCodeList", AdvLonStpMdl).Result;

                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        var lstResponse = res.Content.ReadAsStringAsync().Result;
                        //Deserializing the response recieved from web api and storing into the Employee list  
                        lst = JsonConvert.DeserializeObject<List<DataMaintenanceModel.EmployeeCode_list>>(lstResponse);
                        if (lst.Count > 0)
                        {
                            foreach (DataMaintenanceModel.EmployeeCode_list AdvLonStpEmpCode in lst)
                            {
                                AdvLonStpMdlEmpCode.EmployeeCode = AdvLonStpEmpCode.EmployeeCode;
                                AdvLonStpMdlEmpCode.EmployeeName = AdvLonStpEmpCode.EmployeeName;
                            }
                            return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
                    }
                }
                return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
            }

        }

        public JsonResult GetAtt(DataMaintenanceModel AttData)
        {
            //DataMaintenanceModel AdvLonStpMdl = new DataMaintenanceModel();
            List<DataMaintenanceModel> lst = new List<DataMaintenanceModel>();
            try
            {
                string userName = Session["UserName"].ToString();
                DataMaintenanceModel AttLstData = new DataMaintenanceModel();
                using (var client = new HttpClient())
                {
                    //Passing service base url  
                    client.BaseAddress = new Uri(APIUrl);
                    client.DefaultRequestHeaders.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                    var res = client.PostAsJsonAsync("DataMaintenance/GetAttList", AttData).Result;

                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        //var lstResponse = res.Content.ReadAsStringAsync().Result;
                        //Deserializing the response recieved from web api and storing into the Employee list  
                        //lst = JsonConvert.DeserializeObject<List<DataMaintenanceModel>>(lstResponse);
                        //if (lst.Count > 0)
                        //{
                        //    foreach (DataMaintenanceModel lstAtt in lst)
                        //    {
                        //        AttLstData.In1 = lstAtt.In1;
                        //        AttLstData.Out2 = lstAtt.Out2;
                        //    }
                        //    return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
                        //}
                        var lstResponse = res.Content.ReadAsStringAsync().Result;

                        return Json(lstResponse, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
                    }
                }
                return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
            }

        }

        // Get Selected Paycode
        public JsonResult SelectedEmployeecodeList(string ID)
        {
            try
            {
                DataMaintenanceModel Empdetails = new DataMaintenanceModel();
                string userName = Session["UserName"].ToString();
                Empdetails.EmpCode = ID;
                Empdetails.LastModifiedBy = userName;
                using (var client = new HttpClient())
                {
                    //Passing service base url  
                    client.BaseAddress = new Uri(APIUrl);
                    var res = client.PostAsJsonAsync("DataMaintenance/SelectedEmployeecodeList", Empdetails).Result;
                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        var lstResponse = res.Content.ReadAsStringAsync().Result;
                        return Json(lstResponse, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
            }

        }

        //To Save Punch In MachineRawpunch   
        public JsonResult Add(DataMaintenanceModel mPunch)
        {
            try
            {
                mPunch.LastModifiedBy = Session["UserName"].ToString();
                mPunch.LoginTerminalNameIP = Session["LoginIP"].ToString() + " " + Session["LoginTerminalName"].ToString();
                using (var client = new HttpClient())
                {
                    //Passing service base url  
                    client.BaseAddress = new Uri(APIUrl);
                    var res = client.PostAsJsonAsync("DataMaintenance/InsertManualPunch", mPunch).Result;
                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        return Json("success", JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult CallBackDateProcess(DataMaintenanceModel mPunch)
        {
            try
            {
                mPunch.LastModifiedBy = Session["UserName"].ToString();
                mPunch.LoginTerminalNameIP = Session["LoginIP"].ToString() + " " + Session["LoginTerminalName"].ToString();
                using (var client = new HttpClient())
                {
                    //Passing service base url  
                    client.BaseAddress = new Uri(APIUrl);
                    var res = client.PostAsJsonAsync("DataMaintenance/CallBackDateProcess", mPunch).Result;
                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        return Json("success", JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
            }
        }

        //[HttpPost]
        //public JsonResult SaveEvent(Event e)
        //{
        //    var status = false;
        //    using (MyDatabaseEntities dc = new MyDatabaseEntities())
        //    {
        //        if (e.EventID > 0)
        //        {
        //            //Update the event
        //            var v = dc.Events.Where(a => a.EventID == e.EventID).FirstOrDefault();
        //            if (v != null)
        //            {
        //                v.Subject = e.Subject;
        //                v.Start = e.Start;
        //                v.End = e.End;
        //                v.Description = e.Description;
        //                v.IsFullDay = e.IsFullDay;
        //                v.ThemeColor = e.ThemeColor;
        //            }
        //        }
        //        else
        //        {
        //            dc.Events.Add(e);
        //        }

        //        dc.SaveChanges();
        //        status = true;

        //    }
        //    return new JsonResult { Data = new { status = status } };
        //}

        //[HttpPost]
        //public JsonResult DeleteEvent(int eventID)
        //{
        //    var status = false;
        //    using (MyDatabaseEntities dc = new MyDatabaseEntities())
        //    {
        //        var v = dc.Events.Where(a => a.EventID == eventID).FirstOrDefault();
        //        if (v != null)
        //        {
        //            dc.Events.Remove(v);
        //            dc.SaveChanges();
        //            status = true;
        //        }
        //    }
        //    return new JsonResult { Data = new { status = status} };
        //}

        public JsonResult List()
        {
            try
            {
                ShiftModel Shift = new ShiftModel();
                List<ShiftModel> lst = new List<ShiftModel>();
                Shift.LastModifiedBy = Session["UserName"].ToString();
                Shift.LoginTerminalNameIP = Session["LoginIP"].ToString() + " " + Session["LoginTerminalName"].ToString();
                Shift.CompanyCode = Session["CompanyCode"].ToString();

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIUrl);
                    var res = client.PostAsJsonAsync("DataMaintenance/GetAllList", Shift).Result;
                    if (res.IsSuccessStatusCode)
                    {
                        var lstResponse = res.Content.ReadAsStringAsync().Result;
                        lst = JsonConvert.DeserializeObject<List<ShiftModel>>(lstResponse);
                    }
                }
                return Json(lst, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetAttList()
        {
            DataMaintenanceModel DataMai = new DataMaintenanceModel();
            List<DataMaintenanceModel> lst = new List<DataMaintenanceModel>();
            try
            {
                DataMai.Company = Session["CompanyCode"].ToString();
                //ShiftMst.CompanyCode = Session["CompanyCode"].ToString();

                string userName = Session["UserName"].ToString();
                DataMai.LastModifiedBy = userName;
                //DataMai.UserType = userName;

                string draw = Request.Form.GetValues("draw")[0];
                string order = Request.Form.GetValues("order[0][column]")[0];
                string orderDir = Request.Form.GetValues("order[0][dir]")[0];
                int startRec = Convert.ToInt32(Request.Form.GetValues("start")[0]);
                int pageSize = Convert.ToInt32(Request.Form.GetValues("length")[0]);
                string search = Request.Form.GetValues("search[value]").FirstOrDefault();

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIUrl);
                    var res = client.PostAsJsonAsync("DataMaintenance/GetAttList", DataMai).Result;
                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        var lstResponse = res.Content.ReadAsStringAsync().Result;
                        //Deserializing the response recieved from web api and storing into the Employee list  
                        lst = JsonConvert.DeserializeObject<List<DataMaintenanceModel>>(lstResponse);
                        if (lst.Count > 0)
                        {
                            foreach (DataMaintenanceModel pcm in lst)
                            {
                                DataMai.EnrollmentCode = pcm.EnrollmentCode;
                                DataMai.Company = pcm.Company;
                                DataMai.Department = pcm.Department;
                                DataMai.Category = pcm.Category;
                                DataMai.EmpCode = pcm.EmpCode;
                                DataMai.SSN = pcm.SSN;
                                DataMai.EmpName = pcm.EmpName;
                                DataMai.AttStatus = pcm.AttStatus;
                                DataMai.DateOffice = pcm.DateOffice;
                                DataMai.DateOffice_P = pcm.DateOffice_P;
                                DataMai.In1 = pcm.In1;
                                //ShiftMst.FlexibleLunchDeduction = pcm.FlexibleLunchDeduction;
                                DataMai.Out1 = pcm.Out1;
                                DataMai.In2 = pcm.In2;
                                DataMai.Out2 = pcm.Out2;
                                DataMai.PunchTime = pcm.PunchTime;
                                DataMai.HoursWorked = pcm.HoursWorked;
                            }
                        }
                        //if (Request.IsAjaxRequest())
                        //{
                        //    // Get Exp Records.   
                        //    var allShift = lst;
                        //    //
                        //    IEnumerable<DataMaintenanceModel> filteredShift = lst;
                        //    long TotalRecordsCount = lst.Count();
                        //    #region filters  
                        //    if (!string.IsNullOrEmpty(search))
                        //    {
                        //        filteredShift = allShift.Where(
                        //        u => u.Company.ToUpper().Contains(search.ToUpper())
                        //        || u.EmpCode.ToUpper().Contains(search.ToUpper())
                        //        //|| u.ShiftDiscription.ToUpper().Contains(search.ToUpper())
                        //        //|| u.ShiftStartTime.ToUpper().Contains(search.ToUpper())
                        //        //|| u.ShiftEndTime.ToUpper().Contains(search.ToUpper())
                        //        //|| u.ShiftHours.ToUpper().Contains(search.ToUpper())
                        //        //|| u.LunchStartTime.ToUpper().Contains(search.ToUpper())
                        //        //|| u.LunchDuration.ToUpper().Contains(search.ToUpper())
                        //        //|| u.LunchEndTime.ToUpper().Contains(search.ToUpper())
                        //        //|| u.LunchDeduction.ToUpper().Contains(search.ToUpper())
                        //        //|| u.Overtimedeductafter.ToUpper().Contains(search.ToUpper())
                        //        //|| u.Overtimestartafter.ToUpper().Contains(search.ToUpper())
                        //        //|| u.Overtimededuction.ToUpper().Contains(search.ToUpper())
                        //        //|| u.ShiftPosition.ToUpper().Contains(search.ToUpper())
                        //        //|| u.LastModifiedBy.ToUpper().Contains(search.ToUpper())
                        //        ).ToList();
                        //    }
                        //    #endregion
                        //    long FilteredRecordCount = lst.Count();
                        //    #region Sorting  
                        //    // Sorting     
                        //    switch (order)
                        //    {
                        //        case "0":
                        //            filteredShift = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredShift.OrderByDescending(p => p.SHIFT) : filteredShift.OrderBy(p => p.SHIFT);
                        //            break;
                        //        case "1":
                        //            filteredShift = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredShift.OrderByDescending(p => p.SHIFT) : filteredShift.OrderBy(p => p.SHIFT);
                        //            break;
                        //        case "2":
                        //            filteredShift = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredShift.OrderByDescending(p => p.Company) : filteredShift.OrderBy(p => p.Company);
                        //            break;
                        //        case "3":
                        //            filteredShift = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredShift.OrderByDescending(p => p.SHIFT) : filteredShift.OrderBy(p => p.SHIFT);
                        //            break;
                        //        case "4":
                        //            filteredShift = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredShift.OrderByDescending(p => p.ShiftDiscription) : filteredShift.OrderBy(p => p.ShiftDiscription);
                        //            break;
                        //        case "5":
                        //            filteredShift = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredShift.OrderByDescending(p => p.ShiftStartTime) : filteredShift.OrderBy(p => p.ShiftStartTime);
                        //            break;
                        //        case "6":
                        //            filteredShift = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredShift.OrderByDescending(p => p.ShiftEndTime) : filteredShift.OrderBy(p => p.ShiftEndTime);
                        //            break;
                        //        case "7":
                        //            filteredShift = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredShift.OrderByDescending(p => p.ShiftHours) : filteredShift.OrderBy(p => p.ShiftHours);
                        //            break;
                        //        default:
                        //            filteredShift = filteredShift.OrderByDescending(p => p.SHIFT);
                        //            break;
                        //    }
                        //    #endregion

                        //    var listComp = filteredShift.Skip(startRec).Take(pageSize).ToList()
                        //        .Select(pcm => new DataMaintenanceModel()
                        //        {
                        //            Company = pcm.Company,
                        //            EmpCode = pcm.EmpCode,
                        //            //ShiftDiscription = pcm.ShiftDiscription,
                        //            //ShiftStartTime = pcm.ShiftStartTime,
                        //            //ShiftEndTime = pcm.ShiftEndTime,
                        //            //ShiftHours = pcm.ShiftHours,
                        //            //LunchStartTime = pcm.LunchStartTime,
                        //            //LunchDuration = pcm.LunchDuration,
                        //            //LunchEndTime = pcm.LunchEndTime,
                        //            //LunchDeduction = pcm.LunchDeduction,
                        //            ////FlexibleLunchDeduction = pcm.FlexibleLunchDeduction,
                        //            //Overtimedeductafter = pcm.Overtimedeductafter,
                        //            //Overtimestartafter = pcm.Overtimestartafter,
                        //            //Overtimededuction = pcm.Overtimededuction,
                        //            //ShiftPosition = pcm.ShiftPosition,
                        //        }).ToList();

                        //    if (listComp == null)
                        //        listComp = new List<DataMaintenanceModel>();

                        //    return this.Json(new
                        //    {
                        //        draw = Convert.ToInt32(draw),
                        //        recordsTotal = TotalRecordsCount,
                        //        recordsFiltered = FilteredRecordCount,
                        //        data = listComp
                        //    }, JsonRequestBehavior.AllowGet);
                        //}
                    }
                    else
                    {
                        return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
            }
            return View();
        }
    }
}