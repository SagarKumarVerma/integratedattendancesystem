﻿using IntegratedAttendanceSystem.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace IntegratedAttendanceSystem.Controllers
{
    public class DataMaintenanceController : Controller
    {
        // GET: DataMaintenance
        string APIUrl = ConfigurationManager.AppSettings["APIURL"].ToString();
        public ActionResult DataMaintenance()
        {
            try
            {
                return View();
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return View();
            }
        }

        public JsonResult GetEvents()
        {
            //using (MyDatabaseEntities dc = new MyDatabaseEntities())
            //{
            //var events = dc.Events.ToList();
            //return new JsonResult { Data = events, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            //}
            return null;
        }

        // Get all Paycode
        public JsonResult EmployeecodeList()
        {
            DataMaintenanceModel AdvLonStpMdl = new DataMaintenanceModel();
            List<DataMaintenanceModel.EmployeeCode_list> lst = new List<DataMaintenanceModel.EmployeeCode_list>();
            try
            {
                string userName = Session["UserName"].ToString();
                DataMaintenanceModel.EmployeeCode_list AdvLonStpMdlEmpCode = new DataMaintenanceModel.EmployeeCode_list();
                using (var client = new HttpClient())
                {
                    //Passing service base url  
                    client.BaseAddress = new Uri(APIUrl);
                    client.DefaultRequestHeaders.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                    var res = client.PostAsJsonAsync("DataMaintenance/GetAllEmployeeCodeList", AdvLonStpMdl).Result;

                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        var lstResponse = res.Content.ReadAsStringAsync().Result;
                        //Deserializing the response recieved from web api and storing into the Employee list  
                        lst = JsonConvert.DeserializeObject<List<DataMaintenanceModel.EmployeeCode_list>>(lstResponse);
                        if (lst.Count > 0)
                        {
                            foreach (DataMaintenanceModel.EmployeeCode_list AdvLonStpEmpCode in lst)
                            {
                                AdvLonStpMdlEmpCode.EmployeeCode = AdvLonStpEmpCode.EmployeeCode;
                                AdvLonStpMdlEmpCode.EmployeeName = AdvLonStpEmpCode.EmployeeName;
                            }
                            return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
                    }
                }
                return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
            }

        }
        // Get Selected Paycode
        public JsonResult SelectedEmployeecodeList(string ID)
        {
            try
            {
                DataMaintenanceModel AdvLonStpMdldetl = new DataMaintenanceModel();
                string userName = Session["UserName"].ToString();
                AdvLonStpMdldetl.EmpCode = ID;
                AdvLonStpMdldetl.LastModifiedBy = userName;
                using (var client = new HttpClient())
                {
                    //Passing service base url  
                    client.BaseAddress = new Uri(APIUrl);
                    var res = client.PostAsJsonAsync("DataMaintenance/SelectedEmployeecodeList", AdvLonStpMdldetl).Result;
                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        var lstResponse = res.Content.ReadAsStringAsync().Result;
                        return Json(lstResponse, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
            }

        }
        //To Add new Advance/Loan record
        public JsonResult Add(DataMaintenanceModel AdvLonStpMdl)
        {
            try
            {
                AdvLonStpMdl.LastModifiedBy = Session["UserName"].ToString();
                AdvLonStpMdl.LoginTerminalNameIP = Session["LoginIP"].ToString() + " " + Session["LoginTerminalName"].ToString();

                using (var client = new HttpClient())
                {
                    //Passing service base url  
                    client.BaseAddress = new Uri(APIUrl);
                    var res = client.PostAsJsonAsync("DataMaintenance/AddDataMaintenance", AdvLonStpMdl).Result;
                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        return Json("success", JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json("error", JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                ViewBag.Message = "Error occured, contact to administrator";
                return Json("error", JsonRequestBehavior.AllowGet);
            }
        }
        //code for get auto generate id
        [HttpPost]
        public JsonResult GetIdentityNumber(string vEmpCode, string MonYear)
        {
            try
            {
                string EmpCode = vEmpCode.Trim();
                string AdvanceMonthDate = MonYear.Trim();
                DataMaintenanceModel AdvLonStpMdldetl = new DataMaintenanceModel();
                string userName = Session["UserName"].ToString();
                AdvLonStpMdldetl.EmpCode = EmpCode;
                AdvLonStpMdldetl.AdvanceMonthDate = AdvanceMonthDate;
                AdvLonStpMdldetl.LastModifiedBy = userName;
                using (var client = new HttpClient())
                {
                    //Passing service base url  
                    client.BaseAddress = new Uri(APIUrl);
                    client.DefaultRequestHeaders.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                    var res = client.PostAsJsonAsync("DataMaintenance/GetIdentityNumber", AdvLonStpMdldetl).Result;

                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        var autoID = res.Content.ReadAsStringAsync().Result;
                        var deSerlzID = JsonConvert.DeserializeObject(autoID);
                        return Json(deSerlzID, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json("error", JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                // ExceptionLogging.SendErrorToText(ex);
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return Json("error", JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult BindEmployeeAdvanceLoanList(string ID)
        {
            try
            {
                string EmpCode = ID.Trim();
                DataMaintenanceModel AdvLonStpMdl = new DataMaintenanceModel();
                List<DataMaintenanceModel> lst = new List<DataMaintenanceModel>();
                AdvLonStpMdl.LastModifiedBy = Session["UserName"].ToString();
                AdvLonStpMdl.EmpCode = EmpCode;
                AdvLonStpMdl.LoginTerminalNameIP = Session["LoginIP"].ToString() + " " + Session["LoginTerminalName"].ToString();
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIUrl);
                    var res = client.PostAsJsonAsync("DataMaintenance/BindEmployeeAdvanceLoanList", AdvLonStpMdl).Result;
                    if (res.IsSuccessStatusCode)
                    {
                        var lstResponse = res.Content.ReadAsStringAsync().Result;
                        lst = JsonConvert.DeserializeObject<List<DataMaintenanceModel>>(lstResponse);
                    }
                }
                return Json(lst, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
            }
        }
        //Get  All Employee AdvanceLoan List for jquery data table
        [HttpPost]
        public ActionResult GetAllEmployeeAdvanceLoanList(string ID)
        {
            string EmpCode = ID.Trim();
            DataMaintenanceModel AdvLonStpMdl = new DataMaintenanceModel();
            List<DataMaintenanceModel> lst = new List<DataMaintenanceModel>();
            try
            {
                string userName = Session["UserName"].ToString();
                AdvLonStpMdl.LastModifiedBy = userName;
                AdvLonStpMdl.EmpCode = EmpCode;
                string draw = Request.Form.GetValues("draw")[0];
                string order = Request.Form.GetValues("order[0][column]")[0];
                string orderDir = Request.Form.GetValues("order[0][dir]")[0];
                int startRec = Convert.ToInt32(Request.Form.GetValues("start")[0]);
                int pageSize = Convert.ToInt32(Request.Form.GetValues("length")[0]);
                string search = Request.Form.GetValues("search[value]").FirstOrDefault();
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIUrl);
                    var res = client.PostAsJsonAsync("DataMaintenance/BindEmployeeAdvanceLoanList", AdvLonStpMdl).Result;
                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        var lstResponse = res.Content.ReadAsStringAsync().Result;
                        //Deserializing the response recieved from web api and storing into the Employee list  
                        lst = JsonConvert.DeserializeObject<List<DataMaintenanceModel>>(lstResponse);
                        if (lst.Count > 0)
                        {
                            foreach (DataMaintenanceModel AdvLonStp in lst)
                            {
                                AdvLonStpMdl.IdentityNumber = AdvLonStp.IdentityNumber;
                                AdvLonStpMdl.AdvanceLoanType = AdvLonStp.AdvanceLoanType;
                                AdvLonStpMdl.AdvanceMonthDate = AdvLonStp.AdvanceMonthDate;
                                AdvLonStpMdl.RequestDate = AdvLonStp.RequestDate;
                                AdvLonStpMdl.TransactionFromMonth = AdvLonStp.TransactionFromMonth;
                                AdvLonStpMdl.AdvanceAmount = AdvLonStp.AdvanceAmount;
                                AdvLonStpMdl.IntrestRate = AdvLonStp.IntrestRate;
                                AdvLonStpMdl.InstallmentAmount = AdvLonStp.InstallmentAmount;
                                AdvLonStpMdl.NumberOfInstallment = AdvLonStp.NumberOfInstallment;
                                AdvLonStpMdl.DeductFromSalary = AdvLonStp.DeductFromSalary;
                            }
                        }
                        if (Request.IsAjaxRequest())
                        {
                            // Get Exp Records.   
                            var allDataMaintenance = lst;
                            //
                            IEnumerable<DataMaintenanceModel> filteredDataMaintenance = lst;
                            long TotalRecordsCount = lst.Count();
                            #region filters  
                            if (!string.IsNullOrEmpty(search))
                            {
                                filteredDataMaintenance = allDataMaintenance.Where(
                                u => u.IdentityNumber.ToUpper().Contains(search.ToUpper())
                                || u.AdvanceLoanType.ToUpper().Contains(search.ToUpper())
                                || u.AdvanceMonthDate.ToUpper().Contains(search.ToUpper())
                                || u.RequestDate.ToUpper().Contains(search.ToUpper())
                                || u.TransactionFromMonth.ToUpper().Contains(search.ToUpper())
                                || u.AdvanceAmount.ToUpper().Contains(search.ToUpper())
                                || u.IntrestRate.ToUpper().Contains(search.ToUpper())
                                || u.InstallmentAmount.ToUpper().Contains(search.ToUpper())
                                || u.NumberOfInstallment.ToUpper().Contains(search.ToUpper())
                                || u.DeductFromSalary.ToUpper().Contains(search.ToUpper())
                                ).ToList();
                            }
                            #endregion
                            long FilteredRecordCount = lst.Count();
                            #region Sorting  
                            // Sorting     
                            switch (order)
                            {
                                case "0":
                                    filteredDataMaintenance = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredDataMaintenance.OrderByDescending(p => p.IdentityNumber) : filteredDataMaintenance.OrderBy(p => p.IdentityNumber);
                                    break;
                                case "1":
                                    filteredDataMaintenance = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredDataMaintenance.OrderByDescending(p => p.IdentityNumber) : filteredDataMaintenance.OrderBy(p => p.IdentityNumber);
                                    break;
                                case "2":
                                    filteredDataMaintenance = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredDataMaintenance.OrderByDescending(p => p.IdentityNumber) : filteredDataMaintenance.OrderBy(p => p.IdentityNumber);
                                    break;
                                case "3":
                                    filteredDataMaintenance = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredDataMaintenance.OrderByDescending(p => p.AdvanceLoanType) : filteredDataMaintenance.OrderBy(p => p.AdvanceLoanType);
                                    break;
                                case "4":
                                    filteredDataMaintenance = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredDataMaintenance.OrderByDescending(p => p.AdvanceMonthDate) : filteredDataMaintenance.OrderBy(p => p.AdvanceMonthDate);
                                    break;
                                case "5":
                                    filteredDataMaintenance = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredDataMaintenance.OrderByDescending(p => p.RequestDate) : filteredDataMaintenance.OrderBy(p => p.RequestDate);
                                    break;
                                case "6":
                                    filteredDataMaintenance = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredDataMaintenance.OrderByDescending(p => p.TransactionFromMonth) : filteredDataMaintenance.OrderBy(p => p.TransactionFromMonth);
                                    break;
                                case "7":
                                    filteredDataMaintenance = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredDataMaintenance.OrderByDescending(p => p.AdvanceAmount) : filteredDataMaintenance.OrderBy(p => p.AdvanceAmount);
                                    break;
                                case "8":
                                    filteredDataMaintenance = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredDataMaintenance.OrderByDescending(p => p.IntrestRate) : filteredDataMaintenance.OrderBy(p => p.IntrestRate);
                                    break;
                                case "9":
                                    filteredDataMaintenance = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredDataMaintenance.OrderByDescending(p => p.InstallmentAmount) : filteredDataMaintenance.OrderBy(p => p.InstallmentAmount);
                                    break;
                                case "10":
                                    filteredDataMaintenance = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredDataMaintenance.OrderByDescending(p => p.NumberOfInstallment) : filteredDataMaintenance.OrderBy(p => p.NumberOfInstallment);
                                    break;
                                case "11":
                                    filteredDataMaintenance = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredDataMaintenance.OrderByDescending(p => p.DeductFromSalary) : filteredDataMaintenance.OrderBy(p => p.DeductFromSalary);
                                    break;
                                default:
                                    filteredDataMaintenance = filteredDataMaintenance.OrderByDescending(p => p.IdentityNumber);
                                    break;
                            }
                            #endregion

                            var listAdvanceLoan = filteredDataMaintenance.Skip(startRec).Take(pageSize).ToList()
                                .Select(AdvLonStp => new DataMaintenanceModel()
                                {
                                    IdentityNumber = AdvLonStp.IdentityNumber,
                                    AdvanceLoanType = AdvLonStp.AdvanceLoanType,
                                    AdvanceMonthDate = AdvLonStp.AdvanceMonthDate,
                                    RequestDate = AdvLonStp.RequestDate,
                                    AdvanceAmount = AdvLonStp.AdvanceAmount,
                                    IntrestRate = AdvLonStp.IntrestRate,
                                    TransactionFromMonth = AdvLonStp.TransactionFromMonth,
                                    InstallmentAmount = AdvLonStp.InstallmentAmount,
                                    NumberOfInstallment = AdvLonStp.NumberOfInstallment,
                                    DeductFromSalary = AdvLonStp.DeductFromSalary,
                                }).ToList();

                            if (listAdvanceLoan == null)
                                listAdvanceLoan = new List<DataMaintenanceModel>();

                            return this.Json(new
                            {
                                draw = Convert.ToInt32(draw),
                                recordsTotal = TotalRecordsCount,
                                recordsFiltered = FilteredRecordCount,
                                data = listAdvanceLoan
                            }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
            }
            return View();
        }
        //Delete  Advance/Loan Master
        public JsonResult EmployeeAdvanceLoanDelete(string EmployeeCode, string IdentityNumber)
        {
            try
            {
                DataMaintenanceModel AdvLonStpMdl = new DataMaintenanceModel();
                AdvLonStpMdl.EmpCode = EmployeeCode;
                AdvLonStpMdl.IdentityNumber = IdentityNumber;
                AdvLonStpMdl.LastModifiedBy = Session["UserName"].ToString();
                AdvLonStpMdl.LoginTerminalNameIP = Session["LoginIP"].ToString() + " " + Session["LoginTerminalName"].ToString();
                using (var client = new HttpClient())
                {
                    //Passing service base url  
                    client.BaseAddress = new Uri(APIUrl);
                    var res = client.PostAsJsonAsync("DataMaintenance/DeleteRecord", AdvLonStpMdl).Result;
                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        return Json("success", JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult BindEmployeeAdvanceLoanListIDWise(string EmployeeCode, string IdentityNumber)
        {
            try
            {
                string EmpCode = EmployeeCode.Trim();
                string IDNumber = IdentityNumber;
                DataMaintenanceModel AdvLonStpMdl = new DataMaintenanceModel();
                List<DataMaintenanceModel> lst = new List<DataMaintenanceModel>();
                AdvLonStpMdl.LastModifiedBy = Session["UserName"].ToString();
                AdvLonStpMdl.EmpCode = EmpCode;
                AdvLonStpMdl.IdentityNumber = IDNumber;
                AdvLonStpMdl.LoginTerminalNameIP = Session["LoginIP"].ToString() + " " + Session["LoginTerminalName"].ToString();
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIUrl);
                    var res = client.PostAsJsonAsync("DataMaintenance/BindEmployeeAdvanceLoanListIDWise", AdvLonStpMdl).Result;
                    if (res.IsSuccessStatusCode)
                    {
                        var lstResponse = res.Content.ReadAsStringAsync().Result;
                        lst = JsonConvert.DeserializeObject<List<DataMaintenanceModel>>(lstResponse);
                    }
                }
                return Json(lst, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
            }
        }
        //Get  All Employee AdvanceLoan Installment List ID Wise for jquery data table
        [HttpPost]
        public ActionResult GetAllEmployeeAdvanceLoanListIDWise(string ID)
        {

            DataMaintenanceModel AdvLonStpMdl = new DataMaintenanceModel();
            List<DataMaintenanceModel> lst = new List<DataMaintenanceModel>();
            try
            {
                string userName = Session["UserName"].ToString();
                AdvLonStpMdl.LastModifiedBy = userName;
                string[] arr = ID.Split('~');
                AdvLonStpMdl.EmpCode = arr[0];
                AdvLonStpMdl.IdentityNumber = arr[1];
                string draw = Request.Form.GetValues("draw")[0];
                string order = Request.Form.GetValues("order[0][column]")[0];
                string orderDir = Request.Form.GetValues("order[0][dir]")[0];
                int startRec = Convert.ToInt32(Request.Form.GetValues("start")[0]);
                int pageSize = Convert.ToInt32(Request.Form.GetValues("length")[0]);
                string search = Request.Form.GetValues("search[value]").FirstOrDefault();
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIUrl);
                    var res = client.PostAsJsonAsync("DataMaintenance/BindEmployeeAdvanceLoanListIDWise", AdvLonStpMdl).Result;
                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        var lstResponse = res.Content.ReadAsStringAsync().Result;
                        //Deserializing the response recieved from web api and storing into the Employee list  
                        lst = JsonConvert.DeserializeObject<List<DataMaintenanceModel>>(lstResponse);
                        if (lst.Count > 0)
                        {
                            foreach (DataMaintenanceModel AdvLonStp in lst)
                            {
                                AdvLonStpMdl.AdvanceMonthDate = AdvLonStp.AdvanceMonthDate;
                                AdvLonStpMdl.NumberOfInstallment = AdvLonStp.NumberOfInstallment;
                                AdvLonStpMdl.InstallmentAmount = AdvLonStp.InstallmentAmount;
                                AdvLonStpMdl.PaidAmountPerMonth = AdvLonStp.PaidAmountPerMonth;
                                AdvLonStpMdl.IntrestRate = AdvLonStp.IntrestRate;
                                AdvLonStpMdl.BalanceAmount = AdvLonStp.BalanceAmount;
                                AdvLonStpMdl.DeductFromSalary = AdvLonStp.DeductFromSalary;
                            }
                        }
                        if (Request.IsAjaxRequest())
                        {
                            // Get Exp Records.   
                            var allDataMaintenance = lst;
                            //
                            IEnumerable<DataMaintenanceModel> filteredDataMaintenance = lst;
                            long TotalRecordsCount = lst.Count();
                            #region filters  
                            if (!string.IsNullOrEmpty(search))
                            {
                                filteredDataMaintenance = allDataMaintenance.Where(
                                u => u.AdvanceMonthDate.ToUpper().Contains(search.ToUpper())
                                || u.NumberOfInstallment.ToUpper().Contains(search.ToUpper())
                                || u.InstallmentAmount.ToUpper().Contains(search.ToUpper())
                                || u.PaidAmountPerMonth.ToUpper().Contains(search.ToUpper())
                                || u.IntrestRate.ToUpper().Contains(search.ToUpper())
                                || u.BalanceAmount.ToUpper().Contains(search.ToUpper())
                                || u.DeductFromSalary.ToUpper().Contains(search.ToUpper())
                                ).ToList();
                            }
                            #endregion
                            long FilteredRecordCount = lst.Count();
                            #region Sorting  
                            // Sorting     
                            switch (order)
                            {
                                case "0":
                                    filteredDataMaintenance = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredDataMaintenance.OrderByDescending(p => p.AdvanceMonthDate) : filteredDataMaintenance.OrderBy(p => p.AdvanceMonthDate);
                                    break;
                                case "1":
                                    filteredDataMaintenance = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredDataMaintenance.OrderByDescending(p => p.NumberOfInstallment) : filteredDataMaintenance.OrderBy(p => p.NumberOfInstallment);
                                    break;
                                case "2":
                                    filteredDataMaintenance = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredDataMaintenance.OrderByDescending(p => p.InstallmentAmount) : filteredDataMaintenance.OrderBy(p => p.InstallmentAmount);
                                    break;
                                case "3":
                                    filteredDataMaintenance = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredDataMaintenance.OrderByDescending(p => p.PaidAmountPerMonth) : filteredDataMaintenance.OrderBy(p => p.PaidAmountPerMonth);
                                    break;
                                case "4":
                                    filteredDataMaintenance = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredDataMaintenance.OrderByDescending(p => p.IntrestRate) : filteredDataMaintenance.OrderBy(p => p.IntrestRate);
                                    break;
                                case "5":
                                    filteredDataMaintenance = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredDataMaintenance.OrderByDescending(p => p.BalanceAmount) : filteredDataMaintenance.OrderBy(p => p.BalanceAmount);
                                    break;
                                case "6":
                                    filteredDataMaintenance = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredDataMaintenance.OrderByDescending(p => p.DeductFromSalary) : filteredDataMaintenance.OrderBy(p => p.DeductFromSalary);
                                    break;
                                default:
                                    filteredDataMaintenance = filteredDataMaintenance.OrderByDescending(p => p.IdentityNumber);
                                    break;
                            }
                            #endregion

                            var listAdvanceLoan = filteredDataMaintenance.Skip(startRec).Take(pageSize).ToList()
                                .Select(AdvLonStp => new DataMaintenanceModel()
                                {
                                    AdvanceMonthDate = AdvLonStp.AdvanceMonthDate,
                                    NumberOfInstallment = AdvLonStp.NumberOfInstallment,
                                    InstallmentAmount = AdvLonStp.InstallmentAmount,
                                    PaidAmountPerMonth = AdvLonStp.PaidAmountPerMonth,
                                    IntrestRate = AdvLonStp.IntrestRate,
                                    BalanceAmount = AdvLonStp.BalanceAmount,
                                    DeductFromSalary = AdvLonStp.DeductFromSalary,
                                }).ToList();

                            if (listAdvanceLoan == null)
                                listAdvanceLoan = new List<DataMaintenanceModel>();

                            return this.Json(new
                            {
                                draw = Convert.ToInt32(draw),
                                recordsTotal = TotalRecordsCount,
                                recordsFiltered = FilteredRecordCount,
                                data = listAdvanceLoan
                            }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
            }
            return View();
        }

    }


    //public class DataMaintenanceController : Controller
    //{
    //    // GET: Home
    //    public ActionResult Index()
    //    {
    //        return View();
    //    }

    //    public JsonResult GetEvents()
    //    {
    //        //using (MyDatabaseEntities dc = new MyDatabaseEntities())
    //        //{
    //        //var events = dc.Events.ToList();
    //        //return new JsonResult { Data = events, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
    //        //}
    //        return null;
    //    }

    //    //[HttpPost]
    //    //public JsonResult SaveEvent(Event e)
    //    //{
    //    //    var status = false;
    //    //    using (MyDatabaseEntities dc = new MyDatabaseEntities())
    //    //    {
    //    //        if (e.EventID > 0)
    //    //        {
    //    //            //Update the event
    //    //            var v = dc.Events.Where(a => a.EventID == e.EventID).FirstOrDefault();
    //    //            if (v != null)
    //    //            {
    //    //                v.Subject = e.Subject;
    //    //                v.Start = e.Start;
    //    //                v.End = e.End;
    //    //                v.Description = e.Description;
    //    //                v.IsFullDay = e.IsFullDay;
    //    //                v.ThemeColor = e.ThemeColor;
    //    //            }
    //    //        }
    //    //        else
    //    //        {
    //    //            dc.Events.Add(e);
    //    //        }

    //    //        dc.SaveChanges();
    //    //        status = true;

    //    //    }
    //    //    return new JsonResult { Data = new { status = status } };
    //    //}

    //    //[HttpPost]
    //    //public JsonResult DeleteEvent(int eventID)
    //    //{
    //    //    var status = false;
    //    //    using (MyDatabaseEntities dc = new MyDatabaseEntities())
    //    //    {
    //    //        var v = dc.Events.Where(a => a.EventID == eventID).FirstOrDefault();
    //    //        if (v != null)
    //    //        {
    //    //            dc.Events.Remove(v);
    //    //            dc.SaveChanges();
    //    //            status = true;
    //    //        }
    //    //    }
    //    //    return new JsonResult { Data = new { status = status } };
    //    //}
    //}
}