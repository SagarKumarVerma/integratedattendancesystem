﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace IntegratedAttendanceSystem.Models
{
    public class Employee
    {
        public const string EmployeeMaster = "Employee Master";
        public const string PayCode = "PayCode";

        public string sACTIVE { get; set; }
        public string sPAYCODE { get; set; }
        public string sEMPNAME { get; set; }
        public string sGUARDIANNAME { get; set; }
        public string sDateOFBIRTH { get; set; }
        public string sDateOFJOIN { get; set; }
        public string sPRESENTCARDNO { get; set; }
        public string sCOMPANYCODE { get; set; }
        public List<Company_list> Company { get; set; }
        public string sDivisionCode { get; set; }
        public List<Division_list> sDivision { get; set; }
        public string sCATCode { get; set; }
        public List<CatMaster_list> sCAT { get; set; }
        public string sSEX { get; set; }
        public string sISMARRIED { get; set; }
        public string sQUALIFICATION { get; set; }
        public string sEXPERIENCE { get; set; }
        public string sDESIGNATIONCODE { get; set; }
        public List<Designation_list> sDESIGNATION { get; set; }
        public string sADDRESS1 { get; set; }
        public string sPINCODE1 { get; set; }
        public string sTELEPHONE1 { get; set; }
        public string sE_MAIL1 { get; set; }
        public string sADDRESS2 { get; set; }
        public string sPINCODE2 { get; set; }
        public string sTELEPHONE2 { get; set; }
        public string sBLOODGROUP { get; set; }
        public string sEMPPHOTO { get; set; }
        public byte[] PHOTO { get; set; }
        public string sEMPSIGNATURE { get; set; }
        public byte[] SIGNATURE { get; set; }
        public string sDepartmentCode { get; set; }
        public List<Department_list> sDepartment { get; set; }
        public string sGradeCode { get; set; }
        public List<Grade_list> sGrade { get; set; }
        public string sLeavingdate { get; set; }
        public string sLeavingReason { get; set; }
        public string sVehicleNo { get; set; }
        public string sPFNO { get; set; }
        public string sPF_NO { get; set; }
        public string sESINO { get; set; }
        public string sAUTHORISEDMACHINE { get; set; }
        public string sEMPTYPE { get; set; }
        public string sBankAcc { get; set; }
        public string sbankCODE { get; set; }
        public List<Bank_list> sbank { get; set; }
        public string sReporting1 { get; set; }
        public string sReporting2 { get; set; }
        public string sBRANCHCODE { get; set; }
        public string sDESPANSARYCODE { get; set; }
        public string sheadid { get; set; }
        public string sEmail_CC { get; set; }
        public string sLeaveApprovalStages { get; set; }
        public string sIsFixedShift { get; set; }
        public string sinvoluntary { get; set; }
        public string sEmpBioData { get; set; }
        public string sdateofexp { get; set; }
        public string sDisable { get; set; }
        public string sGroup_Index { get; set; }
        public string sOnRoll { get; set; }
        public string steamid { get; set; }
        public string sPSCODE { get; set; }
        public string sSUBAREACODE { get; set; }
        public string sSECTIONID { get; set; }
        public string sPOSITIONCODE { get; set; }
        public string strainer { get; set; }
        public string strainee { get; set; }
        public string sTraineeLevel { get; set; }
        public string sRockWellid { get; set; }
        public string sTemplate_Send { get; set; }
        public string sheadid_2 { get; set; }
        public string shead_id { get; set; }
        public string sHOD_Codd { get; set; }
        public string sHOD_Code { get; set; }
        public string slogid { get; set; }
        public string sGroupCode { get; set; }
        public List<Group_list> Group { get; set; }
        public string sClassCode { get; set; }
        public string sReligionCode { get; set; }
        public string sValidityStartDate { get; set; }
        public string sValidityEndDate { get; set; }
        public string sSSN { get; set; }
        public string sDeviceGroupID { get; set; }
        public string sClientID { get; set; }
        public string sGroupID { get; set; }
        public DateTime CreatedDate { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime LastModifiedDate { get; set; }
        public string LoginTerminalNameIP { get; set; }
        public string DepartmentCode { get; set; }
        public string DepartmentName { get; set; }
        public string GroupName { get; set; }
        public string DesignationCode { get; set; }
        public string DesignationName { get; set; }
        public string GradeCode { get; set; }
        public string GradeName { get; set; }

        public string BankCode { get; set; }
        public string BankName { get; set; }

        public string DivisionCode { get; set; }
        public string DivisionName { get; set; }

        public string CategoryCode { get; set; }
        public string CategoryName { get; set; }

        public string HODCode { get; set; }
        public string HODName { get; set; }

        public string GroupID { get; set; }
        public string UID { get; set; }
        public string sPANNo { get; set; }
        //public string GroupName { get; set; }
    }

    public class Company_list
    {
        public string CompanyCode { get; set; }
        public string CompanyName { get; set; }
    }
    public class Department_list
    {
        public string DepartmentCode { get; set; }
        public string DepartmentName { get; set; }
    }
    public class Group_list
    {
        public string GroupCode { get; set; }
        public string GroupName { get; set; }
    }
    public class Designation_list
    {
        public string DesignationCode { get; set; }
        public string DesignationName { get; set; }

    }
    public class CatMaster_list
    {
        public string CategoryCode { get; set; }
        public string CategoryName { get; set; }
    }
    public class Grade_list
    {
        public string GradeCode { get; set; }
        public string GradeName { get; set; }
    }
    public class Bank_list
    {
        public string BankCode { get; set; }
        public string BankName { get; set; }
    }
    public class Division_list
    {
        public string DivisionCode { get; set; }
        public string DivisionName { get; set; }
    }
    public class HOD_list
    {
        public string HODCode { get; set; }
        public string HODName { get; set; }
    }
    public class EmployeeGroup_list
    {
        public string GroupID { get; set; }
        public string GroupName { get; set; }
    }

    public class JobPosJDMaster_list
    {
        public string JobMasterId { get; set; }
        public string JobMasterName { get; set; }
    }
    public class JobPosAgeMaster_list
    {
        public string AgeId { get; set; }
        public string AgeDesc { get; set; }
    }

    public class JobPosIMMaster_list
    {
        public string IMCode { get; set; }
        public string IMDesc { get; set; }
    }
    public class JobPosQuaification_list
    {
        public string QID { get; set; }
        public string QDescc { get; set; }
    }
    public class JobPosLanguage_list
    {
        public string LangCode { get; set; }
        public string LangDesc { get; set; }
    }
    public class JobPosCertificate_list
    {
        public string CertCode { get; set; }
        public string CertName { get; set; }
    }
    public class JobPosSpeclzn_list
    {
        public string SpecLznID { get; set; }
        public string SpecLznDesc { get; set; }
    }
    public class JobPosPrimarySkill_list
    {
        public string pskillID { get; set; }
        public string pskillName { get; set; }
    }
    public class JobPosSecondarySkill_list
    {
        public string sskillID { get; set; }
        public string sskillName { get; set; }
    }
    public class JobPosTotalExp_list
    {
        public string ExpId { get; set; }
        public string ExpDesc { get; set; }
    }
    public class JobPosRelv_list
    {
        public string RelvExpId { get; set; }
        public string RelvExpDesc { get; set; }
    }
}