﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using context = System.Web.HttpContext;

namespace IntegratedAttendanceSystem.Models
{
    public static class ExceptionLogging
    {
        public static void SendErrorToText(Exception ex)
        {


            try
            {
                string filepath = context.Current.Server.MapPath("~/IAS_WebErrorLogDetails/");  //Text File Path

                if (!Directory.Exists(filepath))
                {
                    Directory.CreateDirectory(filepath);

                }
                filepath = filepath + DateTime.Today.ToString("dd-MM-yy") + ".txt";   //Text File Name
                if (!File.Exists(filepath))
                {


                    File.Create(filepath).Dispose();

                }
                using (StreamWriter sw = File.AppendText(filepath))
                {
                    string  message = "-----------IAS WEB Exception Log Written  Details on " + " " + DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt") + "-----------------";
                    message += Environment.NewLine;
                    message += string.Format("URL: {0}", context.Current.Request.Url.ToString());
                    message += Environment.NewLine;
                    message += string.Format("Error Message: {0}", ex.Message);
                    message += Environment.NewLine;
                    message += string.Format("StackTrace: {0}", ex.StackTrace);
                    message += Environment.NewLine;
                    message += string.Format("Source: {0}", ex.Source);
                    message += Environment.NewLine;
                    message += string.Format("TargetSite: {0}", ex.TargetSite.ToString());
                    message += Environment.NewLine;
                    message += "----------------------------------------------------------------------------------------------------------";
                    message += Environment.NewLine;
                    sw.WriteLine(message);
                    sw.Flush();
                    sw.Close();

                }

            }
            catch (Exception e)
            {
                e.ToString();

            }
        }
    }
}