﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IntegratedAttendanceSystem.Models
{
    public static class AppConstant
    {
        #region ////***common use*****////
        public const string CREATED = "Created";
        public const string MODIFIED = "Modified";
        public const string DELETED = "Deleted";
        public const string JSONERRORLOGMSG = "error";
        public const string ERRORLOGMSG = "Error occured, contact to administrator";
        #endregion

        #region  Reports Common Use 
        //Report Header
        public const int CompanyNameSize = 32;
        public const int DepartmentNameSize = 32;
        public const int GroupNameSize = 32;

        public const bool ReportHeaderBold = true;
        public const int ReportHeaderSize = 12;
        public const string ReportHeaderHorizontalAlignment = "ExcelHorizontalAlignment.Center";

        //Table Header
        public const bool TableHeaderBold = true;
        public const int TableHeaderSize = 10;
        public const string TableHeaderHorizontalAlignment = "ExcelHorizontalAlignment.Center";
        public const string TableHeaderVerticalAlignment = "ExcelVerticalAlignment.Center";
        public const string TableHeaderBorder = "cell.Style.Border";
        public const string TableHeaderBorderStyle_Top_Bottom_Left_Right = "ExcelBorderStyle.Thin";

        // Table Body
        public const bool TableBodyeBold = false;
        public const int TableBodySize = 10;
        public const string TableBodyBorder = "cell.Style.Border";
        public const string TableBodyBorderStyle_Top_Bottom_Left_Right = "ExcelBorderStyle.Thin";

        #endregion
    }
}