﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace IntegratedAttendanceSystem.Models
{
    public class EmployeeMasterModel
    {
        public const string EmployeeMaster = "Employee Master";
        public const string PayCode = "PayCode";

        public string sACTIVE { get; set; }
        public string sPAYCODE { get; set; }
        public string sEMPNAME { get; set; }
        public string sGUARDIANNAME { get; set; }
        public string sDateOFBIRTH { get; set; }
        public string sDateOFJOIN { get; set; }
        public string sPRESENTCARDNO { get; set; }
        //public string sCOMPANYCODE { get; set; }
        public List<Company_list> Company { get; set; }
        //public string sDivisionCode { get; set; }
        public List<DivisionCode_list> sDivision { get; set; }
        // public string sCAT { get; set; }
        public List<CAT_list> sCAT { get; set; }
        public string sSEX { get; set; }
        public string sISMARRIED { get; set; }
        public string sQUALIFICATION { get; set; }
        public string sEXPERIENCE { get; set; }
        //public string sDESIGNATION { get; set; }
        public List<DESIGNATION_list> sDESIGNATION { get; set; }
        public string sADDRESS1 { get; set; }
        public string sPINCODE1 { get; set; }
        public string sTELEPHONE1 { get; set; }
        public string sE_MAIL1 { get; set; }
        public string sADDRESS2 { get; set; }
        public string sPINCODE2 { get; set; }
        public string sTELEPHONE2 { get; set; }
        public string sBLOODGROUP { get; set; }
        public string sEMPPHOTO { get; set; }
        public string sEMPSIGNATURE { get; set; }
        //public string sDepartmentCode { get; set; }
        public List<Department_list> sDepartment { get; set; }
        //public string sGradeCode { get; set; }
        public List<Grade_list> sGrade { get; set; }
        public string sLeavingdate { get; set; }
        public string sLeavingReason { get; set; }
        public string sVehicleNo { get; set; }
        public string sPFNO { get; set; }
        public string sPF_NO { get; set; }
        public string sESINO { get; set; }
        public string sAUTHORISEDMACHINE { get; set; }
        public string sEMPTYPE { get; set; }
        public string sBankAcc { get; set; }
        //public string sbankCODE { get; set; }
        public List<Bank_list> sbank { get; set; }
        public string sReporting1 { get; set; }
        public string sReporting2 { get; set; }
        public string sBRANCHCODE { get; set; }
        public string sDESPANSARYCODE { get; set; }
        public string sheadid { get; set; }
        public string sEmail_CC { get; set; }
        public string sLeaveApprovalStages { get; set; }
        public string sIsFixedShift { get; set; }
        public string sinvoluntary { get; set; }
        public string sEmpBioData { get; set; }
        public string sdateofexp { get; set; }
        public string sDisable { get; set; }
        public string sGroup_Index { get; set; }
        public string sOnRoll { get; set; }
        public string steamid { get; set; }
        public string sPSCODE { get; set; }
        public string sSUBAREACODE { get; set; }
        public string sSECTIONID { get; set; }
        public string sPOSITIONCODE { get; set; }
        public string strainer { get; set; }
        public string strainee { get; set; }
        public string sTraineeLevel { get; set; }
        public string sRockWellid { get; set; }
        public string sTemplate_Send { get; set; }
        public string sheadid_2 { get; set; }
        public string shead_id { get; set; }
        public string sHOD_Codd { get; set; }
        public string sHOD_Code { get; set; }
        public string slogid { get; set; }
        //public string sGroupCode { get; set; }
        public List<Group_list> GroupCode { get; set; }
        public string sClassCode { get; set; }
        public string sReligionCode { get; set; }
        public string sValidityStartDate { get; set; }
        public string sValidityEndDate { get; set; }
        public string sSSN { get; set; }
        public string sDeviceGroupID { get; set; }
        public string sClientID { get; set; }
        public string sGroupID { get; set; }

        public class Company_list
        {
            public string CompanyCode { get; set; }
            public string CompanyName { get; set; }
        }
        public class DivisionCode_list
        {
            public string DivisionCode { get; set; }
            public string DivisionName { get; set; }
        }
        public class CAT_list
        {
            public string CATCode { get; set; }
            public string CATName { get; set; }
        }
        public class DESIGNATION_list
        {
            public string DesignationCode { get; set; }
            public string DesignationName { get; set; }
        }
        public class Department_list
        {
            public string DepartmentCode { get; set; }
            public string DepartmentName { get; set; }
        }
        public class Grade_list
        {
            public string GradeCode { get; set; }
            public string GradeName { get; set; }
        }
        public class Bank_list
        {
            public string BankCode { get; set; }
            public string BankName { get; set; }
        }
        public class Group_list
        {
            public string GroupCode { get; set; }
            public string GroupName { get; set; }
        }
    }
}