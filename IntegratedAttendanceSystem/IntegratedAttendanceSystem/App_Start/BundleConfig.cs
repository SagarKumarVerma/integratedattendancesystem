﻿using System.Web;
using System.Web.Optimization;

namespace IntegratedAttendanceSystem
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                       "~/Custom_Script/plugins/jquery/jquery.min.js",
                       //"~/Custom_Script/dist/js/skills.js",
                       "~/Custom_Script/plugins/bootstrap/js/bootstrap.bundle.min.js",
                       "~/Custom_Script/plugins/chart.js/Chart.min.js",
                       "~/Custom_Script/plugins/sparklines/sparkline.js",
                       "~/Custom_Script/plugins/jqvmap/jquery.vmap.min.js",
                       "~/Custom_Script/plugins/jqvmap/maps/jquery.vmap.usa.js",
                       "~/Custom_Script/plugins/jquery-knob/jquery.knob.min.js",
                       "~/Custom_Script/plugins/moment/moment.min.js",
                       "~/Custom_Script/plugins/daterangepicker/daterangepicker.js",
                       "~/Custom_Script/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js",
                       "~/Custom_Script/plugins/summernote/summernote-bs4.min.js",
                       "~/Custom_Script/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js",
                       "~/Custom_Script/dist/js/adminlte.js",
                       "~/Custom_Script/dist/js/demo.js"

                       )
               );
            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                        "~/Custom_Script/plugins/jquery-ui/jquery-ui.min.js"));
            bundles.Add(new ScriptBundle("~/bundles/jqtable").Include(
                  "~/Custom_Script/plugins/datatables/jquery.dataTables.min.js"));
            bundles.Add(new ScriptBundle("~/bundles/multiselect").Include(
        "~/Scripts/muliselect.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));
            //bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
            //            "~/Scripts/jquery-{version}.js"));

            //bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
            //            "~/Scripts/jquery.validate*"));

            //// Use the development version of Modernizr to develop with and learn from. Then, when you're
            //// ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            //bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
            //            "~/Scripts/modernizr-*"));

            //bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
            //          "~/Scripts/bootstrap.js"));

            //bundles.Add(new StyleBundle("~/Content/css").Include(
            //          "~/Content/bootstrap.css",
            //          "~/Content/site.css"));
        }
    }
}
