CREATE TABLE [dbo].[tblShiftMaster](
	[SHIFT] [char](3) NOT NULL,
	[STARTTIME] [datetime] NULL,
	[ENDTIME] [datetime] NULL,
	[LUNCHTIME] [datetime] NULL,
	[LUNCHDURATION] [int] NULL,
	[LUNCHENDTIME] [datetime] NULL,
	[ORDERINF12] [int] NULL,
	[OTSTARTAFTER] [float] NULL,
	[OTDEDUCTHRS] [int] NULL,
	[LUNCHDEDUCTION] [int] NULL,
	[SHIFTPOSITION] [char](7) NULL,
	[SHIFTDURATION] [float] NULL,
	[OTDEDUCTAFTER] [float] NULL,
	[CompanyCode] [varchar](4) NOT NULL,
	[ShiftDiscription] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
	[LastModifiedDate] [datetime] NULL,
	[LastModifiedBy] [varchar](50) NULL,
 CONSTRAINT [PK_tblShiftMaster] PRIMARY KEY CLUSTERED 
(
	[SHIFT] ASC,
	[CompanyCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
Drop procedure TblShiftMaster_Add
go
Create procedure TblShiftMaster_Add     
(       
    @CompanyCode char(4),
    @SHIFT char(4),
    @ShiftDiscription Varchar(50),        
    @STARTTIME datetime,    
    @ENDTIME datetime,
    @LUNCHTIME datetime, 
    @LUNCHDURATION char(13), 
    @LUNCHENDTIME datetime, 
    @OTSTARTAFTER Varchar(25) ,
    @OTDEDUCTHRS int, 
    @LUNCHDEDUCTION int,    
    @SHIFTPOSITION Varchar(12),    
    @SHIFTDURATION Varchar(20),
    @OTDEDUCTAFTER Varchar(20),
    @LastModifiedBy Varchar(20)
)    
as     
Begin     
    Insert into TblShiftMaster (CompanyCode,SHIFT,STARTTIME,ENDTIME,LUNCHTIME,LUNCHDURATION,LUNCHENDTIME,OTSTARTAFTER,OTDEDUCTHRS,LUNCHDEDUCTION,SHIFTPOSITION,SHIFTDURATION,OTDEDUCTAFTER,ShiftDiscription,CreatedDate,LastModifiedDate,LastModifiedBy)     
    Values (@CompanyCode,@SHIFT,convert(datetime,@STARTTIME,108),convert(datetime,@ENDTIME,108),convert(datetime,@LUNCHTIME,108),@LUNCHDURATION,convert(datetime,@LUNCHENDTIME,108),@OTSTARTAFTER,@OTDEDUCTHRS,@LUNCHDEDUCTION,@SHIFTPOSITION,@SHIFTDURATION,@OTDEDUCTAFTER,@ShiftDiscription,GETDATE(),GETDATE(),@LastModifiedBy)     
End
go
--Update Record Shift Master
Drop procedure TblShiftMaster_Upd
go
Create procedure TblShiftMaster_Upd      
(   
	@CompanyCode char(4),
    @SHIFT char(4),
    @ShiftDiscription Varchar(50),        
    @STARTTIME Varchar(10),    
    @ENDTIME Varchar(100) ,
	@LUNCHTIME Varchar(200), 
    @LUNCHDURATION char(13), 
    @LUNCHENDTIME Varchar(100),    
	@OTSTARTAFTER float,
    @OTDEDUCTHRS Varchar(50), 
    @LUNCHDEDUCTION	 varchar(25),    
    @SHIFTPOSITION Varchar(12),    
    @SHIFTDURATION float,
	@OTDEDUCTAFTER float,
	@LastModifiedBy Varchar(20) 
)      
as      
begin      
   Update TblShiftMaster  set
   CompanyCode=@CompanyCode,  
   SHIFT=@SHIFT, 
   ShiftDiscription=@ShiftDiscription, 
   STARTTIME=@STARTTIME,
   ENDTIME=@ENDTIME, 
   LUNCHTIME=@LUNCHTIME, 
   LUNCHDURATION = @LUNCHDURATION,    
   LUNCHENDTIME=@LUNCHENDTIME,
   OTSTARTAFTER=@OTSTARTAFTER,
   OTDEDUCTHRS=@OTDEDUCTHRS,
   LUNCHDEDUCTION=@LUNCHDEDUCTION, 
   SHIFTPOSITION=@SHIFTPOSITION, 
   SHIFTDURATION=@SHIFTDURATION, 
   OTDEDUCTAFTER=@OTDEDUCTAFTER, 
   LastModifiedBy=@LastModifiedBy,
   LastModifiedDate=GETDATE()    
   where SHIFT=@SHIFT and CompanyCode=@CompanyCode
End
go
--Delete Record Shift Master
Drop procedure TblShiftMaster_Del
go
Create procedure TblShiftMaster_Del     
(      
   @CompanyCode char(4),
   @SHIFT char(4)  
)      
as       
begin      
   Delete from TblShiftMaster where SHIFT=@SHIFT and CompanyCode=@CompanyCode     
End
go
--Get all records Shift Master
Drop procedure TblShiftMaster_AllRec
go
Create procedure TblShiftMaster_AllRec   
as    
Begin    
select ltrim(rtrim(CompanyCode)) as CompanyCode, ltrim(rtrim(SHIFT)) as SHIFT,STARTTIME,ENDTIME,LUNCHTIME,LUNCHDURATION,LUNCHENDTIME,OTSTARTAFTER,OTDEDUCTHRS,LUNCHDEDUCTION,SHIFTPOSITION,SHIFTDURATION,OTDEDUCTAFTER,CompanyCode,ShiftDiscription  FROM TblShiftMaster 
End
go
--Get all records Shift
Drop procedure TblShiftMaster_GetData
go
Create procedure TblShiftMaster_GetData 
(
@Shift varchar(4),
@CompanyCode varchar(4)
)  
as    
Begin    
   select ltrim(rtrim(CompanyCode)) as CompanyCode, ltrim(rtrim(SHIFT)) as SHIFT,STARTTIME,ENDTIME,LUNCHTIME,LUNCHDURATION,LUNCHENDTIME,OTSTARTAFTER,OTDEDUCTHRS,LUNCHDEDUCTION,SHIFTPOSITION,SHIFTDURATION,OTDEDUCTAFTER,CompanyCode,ShiftDiscription,CreatedDate,LastModifiedDate,LastModifiedBy  FROM TblShiftMaster WHERE SHIFT=@SHIFT and CompanyCode= @CompanyCode  
End
GO
--CheckIsCompanyCode Company Master
drop procedure TblShiftMaster_CheckIsCode
GO
CREATE procedure TblShiftMaster_CheckIsCode
(
@Shift varchar(4),
@CompanyCode varchar(4)
)  
as      
begin
SELECT count(SHIFT) cnt  FROM TblShiftMaster where SHIFT=@SHIFT and CompanyCode=@CompanyCode  
end
GO
