CREATE TABLE [dbo].[tblHOD](
	[HODCode] [char](4) NOT NULL,
	[HODName] [char](4) NULL,
	[HODMail] [char](50) NULL,
	[Paycode] [varchar](10) NOT NULL,
	[CreatedDate] [DATETIME] NULL,
    [LastModifiedDate] [DATETIME]  NULL,
	[LastModifiedBy] varchar(50),
CONSTRAINT [PK_tblHOD] PRIMARY KEY CLUSTERED 
(
	[HODCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
Drop procedure TblHODMaster_Add
go
Create procedure TblHODMaster_Add     
(   
    @HODCode char(4),
    @HODName nvarchar(4),
	@HODMail nvarchar(50),
	@Paycode Varchar(10),
    @LastModifiedBy Varchar(20)
)    
as     
Begin     
    Insert into tblHOD (HODCode,HODName,HODMail,Paycode,CreatedDate,LastModifiedBy,LastModifiedDate)     
    Values (@HODCode,@HODName,@HODMail,@Paycode,GETDATE(),@LastModifiedBy,GETDATE())     
End
go
Drop procedure TblHODMaster_Upd
go
Create procedure TblHODMaster_Upd      
(      
    @HODCode char(4),
    @HODName nvarchar(4),
	@HODMail nvarchar(50),
	@Paycode Varchar(10),
    @LastModifiedBy Varchar(20)
)      
as      
begin      
   Update tblHOD       
   set HODName=@HODName,
   HODMail=@HODMail,
   Paycode=@Paycode,
   LastModifiedBy=@LastModifiedBy,  
   LastModifiedDate=GETDATE()
   where HODCode=@HODCode      
End
go
Drop procedure TblHODMaster_Del
go
Create procedure TblHODMaster_Del     
(      
    @HODCode char(4)    
)      
as       
begin      
   Delete from tblHOD where HODCode =@HODCode      
End
go
Drop procedure TblHODMaster_AllRec
go
Create procedure TblHODMaster_AllRec   
as    
Begin    
select HODCode,HODName,Paycode,HODMail,CreatedDate ,LastModifiedBy,LastModifiedDate from tblHOD
End
go
Drop procedure TblHODMaster_GetHODData 
go
Create procedure TblHODMaster_GetHODData 
(
 @HODCode char(4)   
)  
as    
Begin    
   SELECT * FROM tblHOD where HODCode= @HODCode   
End

GO
Drop procedure TblHODMaster_CatNum 
go
Create procedure TblHODMaster_CatNum 
(
 @HODCode char(4)    
)  
as    
Begin    
     SELECT top 1 HODCode FROM tblHOD order by HODCode desc

End
GO
--CheckIsHODCode Payroll HOD Master
drop procedure TblHODMaster_CheckIsCode
GO
CREATE procedure TblHODMaster_CheckIsCode
(
@HODCode varchar(4)
)  
as      
begin
SELECT count(HODCode) cnt  FROM tblHOD where HODCode=@HODCode 
end
GO