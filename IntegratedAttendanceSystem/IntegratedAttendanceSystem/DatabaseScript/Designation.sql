CREATE TABLE [dbo].[tblDesignation](
	[DesignationCode] [char](3) NOT NULL,
	[DesignationName] [char](35) NULL,
	[CreatedDate] [datetime] NULL,
	[LastModifiedDate] [datetime] NULL,
	[LastModifiedBy] [varchar](50) NULL,
 CONSTRAINT [PK_tblDesignation] PRIMARY KEY CLUSTERED 
(
	[DesignationCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
Drop procedure tblDesignation_Add
go
Create procedure tblDesignation_Add     
(   
    @DesignationCode char(5),
    @DesignationName nvarchar(50),
    @LastModifiedBy Varchar(20)   
)    
as     
Begin     
    Insert into tblDesignation (DesignationCode,DesignationName,CreatedDate,LastModifiedBy,LastModifiedDate)     
    Values (@DesignationCode,@DesignationName,GETDATE(),@LastModifiedBy,GETDATE())     
End

go
Drop procedure tblDesignation_Upd
go
Create procedure tblDesignation_Upd      
(      
    @DesignationCode char(5),
    @DesignationName nvarchar(50),
    @LastModifiedBy Varchar(20) 
)      
as
begin      
   Update tblDesignation       
   set DesignationName=@DesignationName,
   LastModifiedBy=@LastModifiedBy,  
   LastModifiedDate=GETDATE()   
   where DesignationCode=@DesignationCode      
End
go
Drop procedure tblDesignation_Del
go
Create procedure tblDesignation_Del     
(      
    @DesignationCode char(5)    
)      
as       
begin      
   Delete from tblDesignation where DesignationCode=@DesignationCode      
End

go
Drop procedure tblDesignation_AllRec
go
Create procedure tblDesignation_AllRec   
as    
Begin    
select DesignationCode,DesignationName,CreatedDate ,LastModifiedBy,LastModifiedDate from tblDesignation     
End
go
Drop procedure tblDesignation_GetDesignationData 
go
Create procedure tblDesignation_GetDesignationData 
(
 @DesignationCode char(5)   
)  
as    
Begin    
   SELECT * FROM tblDesignation WHERE DesignationCode= @DesignationCode   
End

GO
Drop procedure tblDesignation_DeptNum 
go
Create procedure tblDesignation_DeptNum 
(
 @DesignationCode char(5)    
)  
as    
Begin    
     SELECT top 1 DesignationCode FROM tblDesignation order by DesignationCode desc

End
GO
--CheckIsDesignationCode Payroll Designation Master
drop procedure tblDesignation_CheckIsCode
GO
CREATE procedure tblDesignation_CheckIsCode
(
@DesignationCode varchar(5)
)  
as      
begin
SELECT count(DesignationCode) cnt  FROM tblDesignation where DesignationCode=@DesignationCode 
end
GO


