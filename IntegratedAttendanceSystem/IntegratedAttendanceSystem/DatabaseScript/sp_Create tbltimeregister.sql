alter procedure tablecreation
as
declare @month INT = 1;
declare @tableName varchar(max)='';
WHILE @month <= 12 

BEGIN
   BEGIN TRY   

	  if(@month>9)
		begin
			set @tableName=ltrim(rtrim('tbltimeregister_'+CAST(@month as char(2))+CAST(YEAR(getdate()) as char(4))))
			--print @tableName
		end
	  else
		begin
			set @tableName=ltrim(rtrim('tbltimeregister_0'+CAST(@month as char(1))+CAST(YEAR(getdate()) as char(4))))
			--print @tableName
		end
		SET @month = @month + 1 
	  declare @SQString varchar(max)=''
	  declare @SQStringPk varchar(max)=''
	  --SET @month = @month + 1
	  --print @tableName
	  --print @month

	SET @SQString = 'SELECT *  INTO ' + @tableName + '  FROM tbltimeregister  WHERE 1 = 2 ';
	--set @SQStringPk=
	SET @SQStringPk  = 'ALTER TABLE ' + @tableName + '  ADD CONSTRAINT ' + @tableName + '_Pk PRIMARY KEY (PAYCODE, DateOFFICE)';

	--print @SQString
	--print @SQStringPk
	EXEC (@SQString);
	 EXEC (@SQStringPk);
   END TRY
   BEGIN CATCH;   
   SELECT
    ERROR_NUMBER() AS ErrorNumber,
    ERROR_STATE() AS ErrorState,
    ERROR_SEVERITY() AS ErrorSeverity,
    ERROR_PROCEDURE() AS ErrorProcedure,
    ERROR_LINE() AS ErrorLine,
    ERROR_MESSAGE() AS ErrorMessage;
      IF @month > 12	 
      BEGIN
         BREAK
      END
   END CATCH
END


--ALTER TABLE tbltimeregister_012021     ADD CONSTRAINT ltrim(rtrim(tbltimeregister_012021   ))_Pk PRIMARY KEY (PAYCODE, DateOFFICE)

--execute tablecreation

--select * from tbltimeregister_012021
--select * from tbltimeregister_022021
--select * from tbltimeregister_032021
--select * from tbltimeregister_042021
--select * from tbltimeregister_052021
--select * from tbltimeregister_062021
--select * from tbltimeregister_072021
--select * from tbltimeregister_082021
--select * from tbltimeregister_092021
--select * from tbltimeregister_102021
--select * from tbltimeregister_112021
--select * from tbltimeregister_122021

--drop table tbltimeregister_012021
--go
--drop table tbltimeregister_022021
--go
--drop table tbltimeregister_032021
--go
--drop table tbltimeregister_042021
--go
--drop table tbltimeregister_052021
--go
--drop table tbltimeregister_062021
--go
--drop table tbltimeregister_072021
--go
--drop table tbltimeregister_082021
--go
--drop table tbltimeregister_092021
--go
--drop table tbltimeregister_102021
--go
--drop table tbltimeregister_112021
--go
--drop table tbltimeregister_122021
