CREATE TABLE [dbo].[tblCategory](
    [Id] [bigint] IDENTITY(1,1) NOT NULL,
    [IsActive] [varchar](1) NULL,
	[CategoryCode] [char](5) NOT NULL,
	[CategoryName] [nvarchar](50) NULL,
	[CreatedDate] [DATETIME] NULL,
    [LastModifiedDate] [DATETIME]  NULL,
	[LastModifiedBy] varchar(50),
PRIMARY KEY CLUSTERED 
(
	[CategoryCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

GO
Drop procedure TblCategoryMaster_Add
go
Create procedure TblCategoryMaster_Add     
(   
    @CategoryCode char(5),
    @CategoryName nvarchar(50),        
    @IsActive Varchar(1),        
    @LastModifiedBy Varchar(20)   
)    
as     
Begin     
    Insert into tblCategory (CategoryCode,CategoryName,IsActive,CreatedDate,LastModifiedBy,LastModifiedDate)     
    Values (@CategoryCode,@CategoryName,@IsActive,GETDATE(),@LastModifiedBy,GETDATE())     
End

go
Drop procedure TblCategoryMaster_Upd
go
Create procedure TblCategoryMaster_Upd      
(      
     @CategoryCode char(5),
    @CategoryName nvarchar(50),        
    @IsActive Varchar(1),        
    @LastModifiedBy Varchar(20) 
)      
as      
begin      
   Update tblCategory       
   set CategoryName=@CategoryName,          
   IsActive=@IsActive,    
   LastModifiedBy=@LastModifiedBy,  
   LastModifiedDate=GETDATE()   
   where CategoryCode=@CategoryCode      
End
go
Drop procedure TblCategoryMaster_Del
go
Create procedure TblCategoryMaster_Del     
(      
    @CategoryCode char(5)    
)      
as       
begin      
   Delete from tblCategory where CategoryCode =@CategoryCode      
End

go
Drop procedure TblCategoryMaster_AllRec
go
Create procedure TblCategoryMaster_AllRec   
as    
Begin    
select CategoryCode,CategoryName,case when IsActive='Y' then 'Active' else 'InActive' end  As IsActive,CreatedDate ,LastModifiedBy,LastModifiedDate from tblCategory     
End
go
Drop procedure TblCategoryMaster_GetCategoryData 
go
Create procedure TblCategoryMaster_GetCategoryData 
(
 @CategoryCode char(5)   
)  
as    
Begin    
   SELECT * FROM tblCategory where CategoryCode= @CategoryCode   
End

GO
Drop procedure TblCategoryMaster_CatNum 
go
Create procedure TblCategoryMaster_CatNum 
(
 @CategoryCode char(5)    
)  
as    
Begin    
     SELECT top 1 CategoryCode FROM tblCategory order by CategoryCode desc

End
GO
--CheckIsCategoryCode Payroll Category Master
drop procedure TblCategoryMaster_CheckIsCode
GO
CREATE procedure TblCategoryMaster_CheckIsCode
(
@CategoryCode varchar(5)
)  
as      
begin
SELECT count(CategoryCode) cnt  FROM tblCategory where CategoryCode=@CategoryCode 
end
GO