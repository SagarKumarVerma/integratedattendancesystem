﻿/// <reference path="jquery-1.9.1.intellisense.js" />
const errorMsg = "Error occured, contact to administrator";
//Load Data in Table when documents is ready
$(document).ready(function () {
    var Bankcode = $('#txtBankCode').val().trim();
    BindTableList();
    $("#tblThEdit").removeClass("sorting_asc").addClass("sorting_disabled");
    $(document).ready(function () {
        $("#lnk1").click(function () {

        });
        $("#btnAdd").click(function () {
            $("#myModal").modal("show");
            FocusWhileAdd();
        });
    });
});
function FocusWhileAdd() {
    if ($("#hdnAutoIdFlag").val() == "Y") {
        $("#txtBankCode").attr("disabled", "disabled");
        $('#myModal').on('shown.bs.modal', function () {
            $('#txtBankName').focus();
        })
        $('#divcode').show();
    }
    else {
        $("#txtBankCode").removeAttr('disabled');
        $('#divcode').show();
        $('#myModal').on('shown.bs.modal', function () {
            $('#txtBankCode').focus();
        });
    }
}
//cancel function
function cancel() {
    $('#tblBank').DataTable().destroy();
    BindTableList();
    ClerData();
}
//code for bind Jquery server side datatable 
function BindTableList() {
    $.ajax({
        url: "/Bank/List",
        type: "GET",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (result) {
            if (result == "error") {
                alert(errorMsg);
                return false;
            }
            else {
                var table = $('#tblBank').DataTable({
                    "order": [[3, "Asc"]],
                    "scrollX": true,
                    "scrollY": true,
                    "ordering": true,
                    "processing": true,
                    "serverSide": true,
                    "filter": true,
                    "orderMulti": true,
                    "autoWidth": false,
                    "scrollCollapse": true,

                    "ajax": {
                        "url": "/Bank/GetAllBank",
                        async: false,
                        "type": "POST",
                        "datatype": "json"
                    },

                    "aoColumns": [
                        { "mDataProp": "BankCode" },
                        { "mDataProp": "BankCode" },
                        { "mDataProp": "BankCode" },
                        { "mDataProp": "BankName" },
                        { "mDataProp": "IFSCCode" },
                        { "mDataProp": "Address" },
                        { "mDataProp": "ShortName" },

                    ],
                    "columnDefs": [

                    ],
                    "fnRowCallback": function (nRow, aData, iDisplayIndex) {
                        var hdnlang = $("#hdn_lang").val();
                        if (hdnlang == "E") {
                            $('td:eq(0)', nRow).html("<a href='javascript:void(0);' onclick=getbyID('" + aData.BankCode.trim() + "')><i class='fa fa-pen'></i> Edit</a>");
                        }
                        else {
                            $('td:eq(0)', nRow).html("<a href='javascript:void(0);' onclick=getbyID('" + aData.BankCode.trim() + "')><i class='fa fa-pen'></i> يحرر</a>");
                        }
                        if (hdnlang == "E") {
                            $('td:eq(1)', nRow).html("<a href='javascript:void(0);' onclick=Delete('" + aData.BankCode.trim() + "')><i class='fa fa-trash'></i> Delete</a>");
                        }
                        else {
                            $('td:eq(1)', nRow).html("<a href='javascript:void(0);' onclick=Delete('" + aData.BankCode.trim() + "')><i class='fa fa-trash'></i> حذف</a>");
                        }
                        $('td:eq(5)', nRow).html("<textarea disabled style='width:400px; height:100px'>" + aData.Address + "</textarea>");
                        return nRow;
                    },
                    'columnDefs': [{
                        'targets': [0, 1], // column index (start from 0)
                        'orderable': false, // set orderable false for selected columns
                    }]

                });
            }

        },
        error: function (errormessage) {
            alert(errormessage.responseText);
        }
    });
}
//function for add Bank Master Function
function Add() {
    var res = validate();
    if (res == false) {
        $("#btnAddNew").attr("disabled", "disabled");
        return false;
    }
    else {
        $("#btnAddNew").removeAttr('disabled');
    }
    var BnkObj = {
        BankCode: $('#txtBankCode').val().trim(),
        BankName: $('#txtBankName').val(),
        IFSCCode: $('#txtBankIFSCCode').val(),
        Address: $('#txtBankAddress').val(),
        ShortName: $('#txtBankShortName').val(),
        AutoGeneratedFlag: $("#hdnAutoIdFlag").val()
    };
    $.ajax({
        url: "/Bank/Add",
        data: JSON.stringify(BnkObj),
        type: "POST",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (result) {
            if (result == "error") {
                alert(errorMsg);
                return false;
            }
            else {
                insertedmessage();
                ClerData();
               // GetAutoID();
            }
        },
        error: function (errormessage) {
            alert(errormessage.responseText);
        }
    });
}
//function for update Bank record
function Update() {
    var res = validate();
    if (res == false) {
        $("#btnAddNew").attr("disabled", "disabled");
        return false;
    }
    else {
        $("#btnAddNew").removeAttr('disabled');
    }
    var BnkObj = {
        BankCode: $('#txtBankCode').val().trim(),
        BankName: $('#txtBankName').val(),
        IFSCCode: $('#txtBankIFSCCode').val(),
        Address: $('#txtBankAddress').val(),
        ShortName: $('#txtBankShortName').val(),
        AutoGeneratedFlag: $("#hdnAutoIdFlag").val()
    };
    $.ajax({
        url: "/Bank/Update",
        data: JSON.stringify(BnkObj),
        type: "POST",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (result) {
            if (result == "error") {
                alert(errorMsg);
                return false;
            }
            else {
                updatedmessgae();
                ClerData();
                $('#tblBank').DataTable().destroy();
                BindTableList();
                $('#myModal').modal('hide');
            }
        },
        error: function (errormessage) {
            alert(errormessage.responseText);
        }
    });
}
//function for delete Bank  records
function Delete(BankCode) {
    var ans = confirm(deleteconfirmmsg);
    if (ans) {
        $.ajax({
            url: "/Bank/Delete/" + BankCode,
            async: false,
            type: "POST",
            contentType: "application/json;charset=UTF-8",
            dataType: "json",
            success: function (result) {
                if (result == "error") {
                    alert(errorMsg);
                    return false;
                }
                else {
                    deletedmessage();
                    $('#tblBank').DataTable().destroy();
                    BindTableList();
                }
            },
            error: function (errormessage) {
                alert(errormessage.responseText);
            }
        });
    }
}
//Function for getting the Data Based upon Bank Code
function getbyID(BankCode) {
    $('#divcode').show();
    $('#txtBankCode').css('border-color', 'lightgrey');
    $('#txtBankName').css('border-color', 'lightgrey');
    $('#txtBankIFSCCode').css('border-color', 'lightgrey');
    $('#txtBankAddress').css('border-color', 'lightgrey');
    $('#txtBankShortName').css('border-color', 'lightgrey');

    $.ajax({
        url: "/Bank/GetbyID/" + BankCode,
        async: false,
        typr: "GET",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        success: function (result) {
            if (result == "error") {
                alert(errorMsg);
                return false;
            }
            else {
                var parseResult = jQuery.parseJSON(result);
                $('#txtBankCode').val(parseResult.BankCode);
                $('#txtBankName').val(parseResult.BankName);
                $('#txtBankIFSCCode').val(parseResult.IFSCCode);
                $('#txtBankAddress').val(parseResult.Address);
                $('#txtBankShortName').val(parseResult.ShortName);
                $("#txtBankCode").attr("disabled", "disabled");
                $('#myModal').modal('show');
                $('#myModal').on('shown.bs.modal', function () {
                    $('#txtBankCode').focus();
                })
                $('#btnUpdate').show();
                $('#btnAddNew').hide();
            }
        },
        error: function (errormessage) {
            alert(errormessage.responseText);
        }
    });
    return false;
}
//Function for clearing the textboxes
function ClerData() {

    $('#txtBankCode').val("");
    $('#txtBankName').val("");
    $('#txtBankIFSCCode').val("");
    $('#txtBankAddress').val("");
    $('#txtBankShortName').val("");
    
    $('#btnUpdate').hide();
    $('#btnAddNew').show();

    $('#txtBankCode').css('border-color', 'lightgrey');
    $('#txtBankName').css('border-color', 'lightgrey');
    $('#txtBankIFSCCode').css('border-color', 'lightgrey');
    $('#txtBankAddress').css('border-color', 'lightgrey');
    $('#txtBankShortName').css('border-color', 'lightgrey');
    RemoveError('#txtBankCode');
    RemoveError('#txtBankName');
    RemoveError('#txtBankIFSCCode');
    RemoveError('#txtBankAddress');
    RemoveError('#txtBankShortName');
}
//Valdidation Check
function validate() {
    var isValid = true;
    if ($('#txtBankCode').val().trim() == "") {
        $('#txtBankCode').css('border-color', 'Red');
        RequiredFieldError('#txtBankCode', 'Input', 'Bank Code');
        $("#btnAddNew").attr("disabled", "disabled");
        isValid = false;
    }
    else {
        if (!RequiredFieldTypeError('#txtBankCode', $('#txtBankCode').val().trim(), 'StringAndNumeric')) {
            $('#txtBankCode').css('border-color', 'Red');
            $("#btnAddNew").attr("disabled", "disabled");
            isValid = false;
        }
        else {
            $('#txtBankCode').css('border-color', 'lightgrey');
            $("#btnAddNew").removeAttr('disabled');
            RemoveError('#txtBankCode');
        }
    }
    if ($('#txtBankName').val().trim() == "") {
        $('#txtBankName').css('border-color', 'Red');
        RequiredFieldError('#txtBankName', 'Input', 'Bank Name');
        $("#btnAddNew").attr("disabled", "disabled");
        isValid = false;
    }
    else {
        if (!RequiredFieldTypeError('#txtBankName', $('#txtBankName').val(), 'String')) {
            $("#btnAddNew").attr("disabled", "disabled");
            $('#txtBankName').css('border-color', 'Red');
            isValid = false;
        }
        else {
            $("#btnAddNew").removeAttr('disabled');
            $('#txtBankName').css('border-color', 'lightgrey');
            RemoveError('#txtBankName');
        }
    }
    if ($('#txtBankIFSCCode').val().trim() != "") {
        if (!RequiredFieldTypeError('#txtBankIFSCCode', $('#txtBankIFSCCode').val(), 'BankIFSCCode')) {
            $("#btnAddNew").attr("disabled", "disabled");
            $('#txtBankIFSCCode').css('border-color', 'Red');
            isValid = false;
        }
        else {
            if ($('#txtBankIFSCCode').val().trim().length != 11) {
                $('#txtBankIFSCCode').css('border-color', 'Red');
                RequiredFieldError('#txtBankIFSCCode', 'BankIFSCCode', 'Indian Financial System Code');
                $("#btnAddNew").attr("disabled", "disabled");
                isValid = false;
            }
            else {
                $("#btnAddNew").removeAttr('disabled');
                $('#txtBankIFSCCode').css('border-color', 'lightgrey');
                RemoveError('#txtBankIFSCCode');
            }
        }
    }
    else {
        $("#btnAddNew").removeAttr('disabled');
        $('#txtBankIFSCCode').css('border-color', 'lightgrey');
        RemoveError('#txtBankIFSCCode');
    }
    if ($('#txtBankAddress').val().trim() != "") {
        if (!RequiredFieldTypeError('#txtBankAddress', $('#txtBankAddress').val(), 'Address')) {
            $('#txtBankAddress').css('border-color', 'Red');
            $("#btnAddNew").attr("disabled", "disabled");
            isValid = false;
        }
        else {
            $("#btnAddNew").removeAttr('disabled');
            $('#txtBankAddress').css('border-color', 'lightgrey');
            RemoveError('#txtBankAddress');
        }
    }
    else {
        $("#btnAddNew").removeAttr('disabled');
        $('#txtBankAddress').css('border-color', 'lightgrey');
    }
    if ($('#txtBankShortName').val().trim() == "") {
        $('#txtBankShortName').css('border-color', 'Red');
        RequiredFieldError('#txtBankShortName', 'Input', 'Short Name');
        $("#btnAddNew").attr("disabled", "disabled");
        isValid = false;
    }
    else {
        if (!RequiredFieldTypeError('#txtBankShortName', $('#txtBankShortName').val(), 'String')) {
            $("#btnAddNew").attr("disabled", "disabled");
            $('#txtBankShortName').css('border-color', 'Red');
            isValid = false;
        }
        else {
            $("#btnAddNew").removeAttr('disabled');
            $('#txtBankShortName').css('border-color', 'lightgrey');
            RemoveError('#txtBankShortName');
        }
    }
    RemoveError('#txtBankCode');
    RemoveError('#txtBankName');
    RemoveError('#txtBankIFSCCode');
    RemoveError('#txtBankAddress');
    RemoveError('#txtBankShortName');
    return isValid;
}
//Check Bank Code using jquery
function BankCodeCheck(Bankcode) {
    RemoveError('#txtBankCode');
    $('#divcode').show();
    $('#txtBankCode').css('border-color', 'lightgrey');
    if (!RequiredFieldTypeError('#txtBankCode', $('#txtBankCode').val().trim(), 'StringAndNumeric')) {
        $("#btnAddNew").attr("disabled", "disabled");
        return false;
    }
    else {
        $("#btnAddNew").removeAttr('disabled');
    }
    $.ajax({
        url: "/Bank/GetbyBankcode/" + Bankcode,
        async: false,
        typr: "GET",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        success: function (result) {
            if (result == "error") {
                alert(errorMsg);
                return false;
            }
            else {
                var parseResult = (result);
                if (parseResult == 1) {
                    $('#txtBankCode').css('border-color', 'Red');
                    $("#btnAddNew").attr("disabled", "disabled");
                    RequiredFieldError('#txtBankCode', 'BankCodeExist', 'Bank Code');
                    return false;
                }
                else {
                    $('#txtBankCode').css('border-color', 'lightgrey');
                    $("#btnAddNew").removeAttr('disabled');
                    RemoveError('#txtBankCode');
                }               
            }
        },
        error: function (errormessage) {
            alert(errormessage.responseText);
        }
    });
    return false;
}

function CommonValidation() {
    var isValid = true;
    RemoveError('#txtBankCode');
    RemoveError('#txtBankName');
    RemoveError('#txtBankIFSCCode');
    RemoveError('#txtBankAddress');
    RemoveError('#txtBankShortName');
   
    if ($('#txtBankCode').val().trim() == "") {
        $('#txtBankCode').css('border-color', 'Red');
        RequiredFieldError('#txtBankCode', 'Input', 'Bank Code');
        $("#btnAddNew").attr("disabled", "disabled");
        isValid = false;
    }
    else {
        if (!RequiredFieldTypeError('#txtBankCode', $('#txtBankCode').val().trim(), 'StringAndNumeric')) {
            $('#txtBankCode').css('border-color', 'Red');
            $("#btnAddNew").attr("disabled", "disabled");
            isValid = false;
        }
        else {
            $('#txtBankCode').css('border-color', 'lightgrey');
            $("#btnAddNew").removeAttr('disabled');
            RemoveError('#txtBankCode');
        }
    }
    if ($('#txtBankName').val().trim() == "") {
        $('#txtBankName').css('border-color', 'Red');
        RequiredFieldError('#txtBankName', 'Input', 'Bank Name');
        $("#btnAddNew").attr("disabled", "disabled");
        isValid = false;
    }
    else {
        if (!RequiredFieldTypeError('#txtBankName', $('#txtBankName').val(), 'String')) {
            $("#btnAddNew").attr("disabled", "disabled");
            $('#txtBankName').css('border-color', 'Red');
            isValid = false;
        }
        else {
            $("#btnAddNew").removeAttr('disabled');
            $('#txtBankName').css('border-color', 'lightgrey');
            RemoveError('#txtBankName');
        }
    }
    if ($('#txtBankIFSCCode').val().trim() != "") {
        if ($('#txtBankIFSCCode').val().trim().length != 11) {
            $('#txtBankIFSCCode').css('border-color', 'Red');
            RequiredFieldTypeError('#txtBankIFSCCode', $('#txtBankIFSCCode').val(), 'BankIFSCCode');
            $("#btnAddNew").attr("disabled", "disabled");
            isValid = false;
        }
        else {
            $('#txtBankIFSCCode').css('border-color', 'lightgrey');
            RemoveError('#txtBankIFSCCode');
        }
    }
    else {
        $('#txtBankIFSCCode').css('border-color', 'lightgrey');
        RemoveError('#txtBankIFSCCode');
    }
    if ($('#txtBankAddress').val().trim() != "") {
        if (!RequiredFieldTypeError('#txtBankAddress', $('#txtBankAddress').val(), 'Address')) {
            $('#txtBankAddress').css('border-color', 'Red');
            $("#btnAddNew").attr("disabled", "disabled");
            isValid = false;
        }
        else {
            $("#btnAddNew").removeAttr('disabled');
            $('#txtBankAddress').css('border-color', 'lightgrey');
            RemoveError('#txtBankAddress');
        }
    }
    else {
        $("#btnAddNew").removeAttr('disabled');
        $('#txtBankAddress').css('border-color', 'lightgrey');
    }
    if ($('#txtBankShortName').val().trim() == "") {
        $('#txtBankShortName').css('border-color', 'Red');
        RequiredFieldError('#txtBankShortName', 'Input', 'Short Name');
        $("#btnAddNew").attr("disabled", "disabled");
        isValid = false;
    }
    else {
        if (!RequiredFieldTypeError('#txtBankShortName', $('#txtBankShortName').val(), 'String')) {
            $("#btnAddNew").attr("disabled", "disabled");
            $('#txtBankShortName').css('border-color', 'Red');
            isValid = false;
        }
        else {
            $("#btnAddNew").removeAttr('disabled');
            $('#txtBankShortName').css('border-color', 'lightgrey');
            RemoveError('#txtBankShortName');
        }
    }
    return isValid;
}