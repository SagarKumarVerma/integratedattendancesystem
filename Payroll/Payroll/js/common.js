﻿const deleteconfirmmsg = "You are about to delete a record. Are you sure to proceed?";
function insertedmessage() {
        alert("Record saved successfully. Please verify the actual ID assigned. It will be near about the provisional ID shown to you.");
}
function CaptureMessage() {
    alert("Processing successful.");
}
function updatedmessgae() {
        alert("Selected record updated successfully.");
        
    }
function deletedmessage() {
        alert("Your selected record has been deleted.");
    }
function errormessage() {
        alert("Error occured, contact to administrator.");
    }
////only number validation allow on on key press
function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}
//single and double  qoutes not allow validation
function QoutesValidation(evt)
{
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if ((charCode ==34) || (charCode ==39))
        return false;
    return true;
}
function IsEmail(email) {
    var regex = /^[a-zA-Z0-9,!#\$%&'\*\+/=\?\^_`\{\|}~-]+(\.[a-zA-Z0-9,!#\$%&'\*\+/=\?\^_`\{\|}~-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*\.([a-zA-Z]{2,})$/;
    if (!regex.test(email)) {
        return false;
    } else {
        return true;
    }
}

