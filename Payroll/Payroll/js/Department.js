﻿const errorMsg = "Error occured, contact to administrator";
$(document).ready(function () {
    var Deptcode = $('#txtDepartmentCode').val().trim();
    BindTableList();
    $("#tblThEdit").removeClass("sorting_asc").addClass("sorting_disabled");
    $(document).ready(function () {
        $("#lnk1").click(function () {

        });
        $("#btnAdd").click(function () {
            $("#myModal").modal("show");
            $("#ddlIsActive").attr("disabled", "disabled");
            // bindDepartmentList();
            FocusWhileAdd();
        });

    });
});
//code for focus based on auto id confuguration
function FocusWhileAdd() {
    if ($("#hdnAutoIdFlag").val() == "Y") {
        $("#ddlIsActive").attr("disabled", "disabled");
        // GetAutoID();
        $('#myModal').on('shown.bs.modal', function () {
            $('#txtDepartmentCode').focus();
        })
        $('#divcode').show();
    }
    else {
        $("#txtDepartmentCode").removeAttr("disabled", "disabled");
        $('#divcode').show();
        $('#myModal').on('shown.bs.modal', function () {
            $('#txtDepartmentCode').focus();
        });
    }
}
//JQ Sagar
function cancel() {
    $('#tblDepartment').DataTable().destroy();
    BindTableList();
    ClerData();

}
// Code for bind Jquery server side datatable 
function BindTableList() {
    $.ajax({
        url: "/Department/List",
        type: "GET",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (result) {
            if (result == "error") {
                alert(errorMsg);
                return false;
            }
            else {
                var table = $('#tblDepartment').DataTable({
                    "order": [[3, "Asc"]],
                    "scrollX": true,
                    "scrollY": true,
                    "ordering": true,
                    "processing": true,
                    "serverSide": true,
                    "filter": true,
                    "orderMulti": true,
                    "autoWidth": false,
                    "scrollCollapse": true,

                    "ajax": {
                        "url": "/Department/GetAllDepartment",
                        async: false,
                        "type": "POST",
                        "datatype": "json"
                    },
                    "aoColumns": [
                        { "mDataProp": "DepartmentCode" },
                        { "mDataProp": "DepartmentCode" },
                        { "mDataProp": "IsActive" },
                        { "mDataProp": "DepartmentCode" },
                        { "mDataProp": "DepartmentName" }
                    ],
                    "columnDefs": [

                    ],
                    "fnRowCallback": function (nRow, aData, iDisplayIndex) {
                        var hdnlang = $("#hdn_lang").val();
                        if (hdnlang == "E") {
                            $('td:eq(0)', nRow).html("<a href='javascript:void(0);' onclick=getbyID('" + aData.DepartmentCode.trim() + "')><i class='fa fa-pen'></i> Edit</a>");
                        }
                        else {
                            $('td:eq(0)', nRow).html("<a href='javascript:void(0);' onclick=getbyID('" + aData.DepartmentCode.trim() + "')><i class='fa fa-pen'></i> يحرر</a>");
                        }
                        if (hdnlang == "E") {
                            $('td:eq(1)', nRow).html("<a href='javascript:void(0);' onclick=Delete('" + aData.DepartmentCode.trim() + "')><i class='fa fa-trash'></i> Delete</a>");
                        }
                        else {
                            $('td:eq(1)', nRow).html("<a href='javascript:void(0);' onclick=Delete('" + aData.DepartmentCode.trim() + "')><i class='fa fa-trash'></i> حذف</a>");
                        }
                        return nRow;
                    },
                    'columnDefs': [{
                        'targets': [0, 1], // column index (start from 0)
                        'orderable': false, // set orderable false for selected columns
                    }]
                });
            }

        },
        error: function (errormessage) {
            alert(errormessage.responseText);
        }
    });
}
//code for generate autoid
//function GetAutoID() {
//    $.ajax({
//        url: "/Department/GetAutoGenerateCode/",
//        async: false,
//        typr: "GET",
//        contentType: "application/json;charset=UTF-8",
//        dataType: "json",
//        success: function (result) {

//            $("#txtDepartmentCode").val(result);
//        },
//        error: function (errormessage) {
//            alert(errormessage.responseText);
//        }
//    });
//    return false;
//}
//Load Data function

//function for updating Department record
function Update() {
    var res = validate();
    if (res == false) {
        $("#btnAddNew").attr("disabled", "disabled");
        return false;
    }
    else {
        $("#btnAddNew").removeAttr('disabled');
    }
    var DeptObj = {
        IsActive: $('#ddlIsActive').val(),
        DepartmentCode: $('#txtDepartmentCode').val().trim(),
        DepartmentName: $('#txtDepartmentName').val()
    };
    $.ajax({
        url: "/Department/Update",
        //async: false,
        data: JSON.stringify(DeptObj),
        type: "POST",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (result) {
            if (parseInt(result) == 2) {
                alert('Department  can not be updated  because it is used ');
                return false;
            }
            if (parseInt(result) == -1) {
                alert(errorMsg);
                return false;
            }
            if (result == "error") {
                alert(errorMsg);
                return false;
            }
            //# Modified
            updatedmessgae();
            ClerData();
            $('#tblDepartment').DataTable().destroy();
            BindTableList();
            $('#myModal').modal('hide');
        },
        error: function (errormessage) {
            alert(errormessage.responseText);
        }
    });
}
//Add Data Function 
function Add() {
    var res = validate();
    if (res == false) {
        $("#btnAddNew").attr("disabled", "disabled");
        return false;
    }
    else {
        $("#btnAddNew").removeAttr('disabled');
    }
    var DeptObj = {
        IsActive: $('#ddlIsActive').val(),
        DepartmentCode: $('#txtDepartmentCode').val().trim(),
        DepartmentName: $('#txtDepartmentName').val()
    };
    $.ajax({
        url: "/Department/Add",
        async: false,
        data: JSON.stringify(DeptObj),
        type: "POST",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (result) {
            if (result == "error") {
                alert(errorMsg);
                return false;
            }
            else {
                insertedmessage();
                ClerData();
                //GetAutoID();
            }
        },
        error: function (errormessage) {
            alert(errormessage.responseText);
        }
    });
}
//Function for getting the Data Based upon Department Code
function getbyID(DepartmentCode) {
    $('#divcode').show();
    $('#ddlIsActive').css('border-color', 'lightgrey');
    $('#txtDepartmentCode').css('border-color', 'lightgrey');
    $('#txtDepartmentName').css('border-color', 'lightgrey');
   
    $.ajax({
        url: "/Department/GetbyID/" + DepartmentCode,
        async: false,
        typr: "GET",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        success: function (result) {
            if (result == "error") {
                alert(errorMsg);
                return false;
            }
            else {
                var parseResult = jQuery.parseJSON(result);
                //bindDepartmentList();
                if (parseResult.IsActive == 'True') {
                    $('#ddlIsActive').val('Y');
                }
                else {
                    $('#ddlIsActive').val('N');
                }
                $('#txtDepartmentCode').val(parseResult.DepartmentCode);
                $('#txtDepartmentName').val(parseResult.DepartmentName);
                $("#txtDepartmentCode").attr("disabled", "disabled");
                $("#ddlIsActive").removeAttr('disabled');
                $('#myModal').modal('show');
                $('#myModal').on('shown.bs.modal', function () {
                    $('#txtDepartmentName').focus();
                })
                $('#btnUpdate').show();
                $('#btnAddNew').hide();
            }
        },
        error: function (errormessage) {
            alert(errormessage.responseText);
        }
    });
    return false;
}
//function for deleting  records
function Delete(DepartmentCode) {
    //var ans = confirm("Are you sure you want to delete this Record?");
    var ans = confirm(deleteconfirmmsg);
    if (ans) {
        $.ajax({
            url: "/Department/Delete/" + DepartmentCode,
            async: false,
            type: "POST",
            contentType: "application/json;charset=UTF-8",
            dataType: "json",
            success: function (result) {
                if (parseInt(result) == 2) {
                    alert('Record can not be deleted  because its used ');
                    return false;
                }
                if (parseInt(result) == -1) {
                    alert(errorMsg);
                    return false;
                }
                //# Modified
                if (result == "error") {
                    alert(errorMsg);
                    return false;
                }
                deletedmessage();
                $('#tblDepartment').DataTable().destroy();
                BindTableList();
                //if (result == 'success') {
                //    alert("Language deleted successfully");
                //    cancel();
                //}
                //# 

            },
            error: function (errormessage) {
                alert(errormessage.responseText);
            }
        });
    }

}
//Function for clearing the textboxes
function ClerData() { 
    $('#ddlIsActive').val("");
    $("select#ddlIsActive")[0].selectedIndex = 0;
    $('#txtDepartmentCode').val("");
    $('#txtDepartmentName').val("");
    $('#btnUpdate').hide();
    $('#btnAddNew').show();
    $('#ddlIsActive').css('border-color', 'lightgrey');
    $('#txtDepartmentCode').css('border-color', 'lightgrey');
    $('#txtDepartmentName').css('border-color', 'lightgrey');
    $("#ddlIsActive").attr("disabled", "disabled");
    RemoveError('#txtDepartmentCode');
    RemoveError('#txtDepartmentName'); 

}
//single and double  qoutes not allow validation
function QoutesValidation(evt) {
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if ((charCode == 34) || (charCode == 39))
        return false;
    return true;
}
//Bind Department List
function bindDepartmentList() {
    $.ajax({
        url: "/Department/ListAllDepartment",
        async: false,
        type: "GET",
        contentType: "application/json; charset=utf-8",
        datatype: "JSON",
        success: function (result) {
            $('#myModal').modal('show');
        },
        error: function (data) { }
    });
}
//Valdidation using jquery
function validate() {
    var isValid = true;
    RemoveError('#txtDepartmentCode');
    RemoveError('#txtDepartmentName');
    if ($('#ddlIsActive').val().trim() == "") {
        $('#ddlIsActive').css('border-color', 'Red');
        $("#btnAddNew").attr("disabled", "disabled");
        isValid = false;
    }
    else {
        $("#btnAddNew").removeAttr('disabled');
        $('#ddlIsActive').css('border-color', 'lightgrey');
    }
    if ($('#txtDepartmentCode').val().trim() == "") {
        $('#txtDepartmentCode').css('border-color', 'Red');
        RequiredFieldError('#txtDepartmentCode', 'Input', 'Department Code');
        $("#btnAddNew").attr("disabled", "disabled");
        isValid = false;
    }
    else {
        if (!RequiredFieldTypeError('#txtDepartmentCode', $('#txtDepartmentCode').val().trim(), 'StringAndNumeric')) {
            $('#txtDepartmentCode').css('border-color', 'Red');
            $("#btnAddNew").attr("disabled", "disabled");
            isValid = false;
        }
        else {
            $('#txtDepartmentCode').css('border-color', 'lightgrey');
            $("#btnAddNew").removeAttr('disabled');
            RemoveError('#txtDepartmentCode');
        }
    }
    if ($('#txtDepartmentName').val().trim() == "") {
        $('#txtDepartmentName').css('border-color', 'Red');
        RequiredFieldError('#txtDepartmentName', 'Input', 'Department Name');
        $("#btnAddNew").attr("disabled", "disabled");
        isValid = false;
    }
    else {
        if (!RequiredFieldTypeError('#txtDepartmentName', $('#txtDepartmentName').val(), 'String')) {
            $("#btnAddNew").attr("disabled", "disabled");
            $('#txtDepartmentName').css('border-color', 'Red');
            isValid = false;
        }
        else {
            $("#btnAddNew").removeAttr('disabled');
            $('#txtDepartmentName').css('border-color', 'lightgrey');
            RemoveError('#txtDepartmentName');
        }
    }
    return isValid;
}
//Check Department Code using jquery
function DepartmentCodeCheck(Deptcode) {
    RemoveError('#txtDepartmentCode');
    $('#divcode').show();
    $('#txtDepartmentCode').css('border-color', 'lightgrey');
    if (!RequiredFieldTypeError('#txtDepartmentCode', $('#txtDepartmentCode').val().trim(), 'StringAndNumeric')) {
        $("#btnAddNew").attr("disabled", "disabled");
        return false;
    }
    else {
        $("#btnAddNew").removeAttr('disabled');
    }
    $.ajax({
        url: "/Department/GetbyDeptcode/" + Deptcode,
        async: false,
        typr: "GET",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        success: function (result) {
            if (result == "error") {
                alert(errorMsg);
                return false;
            }
            else {
                var parseResult = (result);
                if (parseResult == 1) {
                    $('#txtDepartmentCode').css('border-color', 'Red');
                    $("#btnAddNew").attr("disabled", "disabled");
                    RequiredFieldError('#txtDepartmentCode', 'DepartmentCodeExist', 'Department Code');
                    return false;
                }
                else {
                    $('#txtDepartmentCode').css('border-color', 'lightgrey');
                    $("#btnAddNew").removeAttr('disabled');
                    RemoveError('#txtDepartmentCode');
                }
            }
        },
        error: function (errormessage) {
            alert(errormessage.responseText);
        }
    });
    return false;
}
//Valdidation On Change
function CommonValidation() {
    var isValid = true;
    RemoveError('#txtDepartmentCode');
    RemoveError('#txtDepartmentName');  
    if ($('#ddlIsActive').val().trim() == "") {
        $('#ddlIsActive').css('border-color', 'Red');
        $("#btnAddNew").attr("disabled", "disabled");
        isValid = false;
    }
    else {
        $("#btnAddNew").removeAttr('disabled');
        $('#ddlIsActive').css('border-color', 'lightgrey');
    }
    if ($('#txtDepartmentCode').val().trim() == "") {
        $('#txtDepartmentCode').css('border-color', 'Red');
        RequiredFieldError('#txtDepartmentCode', 'Input', 'Department Code');
        $("#btnAddNew").attr("disabled", "disabled");
        isValid = false;
    }
    else {
        if (!RequiredFieldTypeError('#txtDepartmentCode', $('#txtDepartmentCode').val().trim(), 'StringAndNumeric')) {
            $('#txtDepartmentCode').css('border-color', 'Red');
            $("#btnAddNew").attr("disabled", "disabled");
            isValid = false;
        }
        else {
            $('#txtDepartmentCode').css('border-color', 'lightgrey');
            $("#btnAddNew").removeAttr('disabled');
            RemoveError('#txtDepartmentCode');
        }
    }
    if ($('#txtDepartmentName').val().trim() == "") {
        $('#txtDepartmentName').css('border-color', 'Red');
        RequiredFieldError('#txtDepartmentName', 'Input', 'Department Name');
        $("#btnAddNew").attr("disabled", "disabled");
        isValid = false;
    }
    else {
        if (!RequiredFieldTypeError('#txtDepartmentName', $('#txtDepartmentName').val(), 'String')) {
            $("#btnAddNew").attr("disabled", "disabled");
            $('#txtDepartmentName').css('border-color', 'Red');
            isValid = false;
        }
        else {
            $("#btnAddNew").removeAttr('disabled');
            $('#txtDepartmentName').css('border-color', 'lightgrey');
            RemoveError('#txtDepartmentName');
        }
    }
    return isValid;
}
