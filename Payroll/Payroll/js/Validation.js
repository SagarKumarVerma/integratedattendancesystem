﻿function IsEmail(email) {
    var regex = /^[a-zA-Z0-9,!#\$%&'\*\+/=\?\^_`\{\|}~-]+(\.[a-zA-Z0-9,!#\$%&'\*\+/=\?\^_`\{\|}~-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*\.([a-zA-Z]{2,})$/;
    if (!regex.test(email)) {
        return false;
    } else {
        return true;
    }
}
function AllowDecimal(name) {
    var regex = /^[0-9](\.[0-9]+)?$/;
    if (!regex.test(name)) {
        return false;
    } else {
        return true;
    }
}
function AllowNumeric(name) {
    var regex = /^[0-9]+$/;
    if (!regex.test(name)) {
        return false;
    } else {
        return true;
    }
}
function AllowAlphabetsandNumbers(name) {
    var regex = /^[A-Za-z0-9]+$/;
    if (!regex.test(name)) {
        return false;
    } else {
        return true;
    }
}
function Address(name) {
    var regex = /^[a-zA-Z0-9\s/,.'-]{3,}$/;
    if (!regex.test(name)) {
        return false;
    } else {
        return true;
    }
}
function AllowAlphaNumericWithSpace(name) {
    var regex = /^[a-zA-Z .]*$/;
    if (!regex.test(name)) {
        return false;
    } else {
        return true;
    }
}
function AllowAlphaNumericWithSlash(name) {
    var regex = /^[a-zA-Z0-9\s/]+$/;
    if (!regex.test(name)) {
        return false;
    } else {
        return true;
    }
}
function PANNumber(name) {
    var regex = /^([A-Z]){5}([0-9]){4}([A-Z]){1}$/;
    if (!regex.test(name)) {
        return false;
    } else {
        return true;
    }
}
function BankIFSCCode(name) {
    var regex = /^[A-Z]{4}0[A-Z0-9]{6}$/;
    if (!regex.test(name)) {
        return false;
    } else {
        return true;
    }
}
function PFNumber(name) {
    var regex = /^[a-zA-Z0-9\s/]+$/;
    if (!regex.test(name)) {
        return false;
    } else {
        return true;
    }
}
function PGS(name) {
    var regex = /^[a-zA-Z0-9\s/-]+$/;
    if (!regex.test(name)) {
        return false;
    } else {
        return true;
    }
}
function AadharNumber(name) {
    var regex = /^[2-9]{1}[0-9]{3}\s{1}[0-9]{4}\s{1}[0-9]{4}$/;
    if (!regex.test(name)) {
        return false;
    } else {
        return true;
    }
}
function TANNumber(name) {
    var regex = /^([A-Z]){4}([0-9]){5}([A-Z]){1}$/;
    if (!regex.test(name)) {
        return false;
    } else {
        return true;
    }
}
function BankAC(name) {
    var regex = /^([0-9]{11})|([0-9]{2}-[0-9]{3}-[0-9]{6})$/;
    if (!regex.test(name)) {
        return false;
    } else {
        return true;
    }
}
//function AllowTelephone(name)
function PostalCode(name) {
    var regex = /(^\d{6}$)/;
    if (!regex.test(name)) {
        return false;
    } else {
        return true;
    }
}
function ValidatePassword(pwd) {
    var pattern = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{6,})");
    if (!pattern.test(pwd)) {
        return false;
    }
    else
        return true;
}
function onlyAlphabets(e, t) {
    try {
        if (window.event) {
            var charCode = window.event.keyCode;
        }
        else if (e) {
            var charCode = e.which;
        }
        else { return true; }
        if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123))
            return true;
        else
            return false;
    }
    catch (err) {
        //alert(err.Description);
    }
}
function RequiredFieldSuccess(controlId, FieldType, FieldName) {
    switch (FieldType) {
        case 'Image':
            $(controlId).after("<span class='success'>" + FieldName + " uploaded Successfully.</span>");
            break;
    }
}
function RequiredFieldTypeError(controlId, controlvalue, FieldType) {
    var nflag = "";
    switch (FieldType) {
        case 'String':
            nflag = AllowAlphaNumericWithSpace(controlvalue);
            if (!nflag) {
                $(controlId).after("<span class='error'>Only alphabets are allowed.</span>");
                $(controlId).addClass('error-input-inner');
            }
            break;
        case 'Address':
            nflag = Address(controlvalue);
            if (!nflag) {
                $(controlId).after("<span class='error'>Only /,.'- space with minmum Length 3  are allowed.</span>");
                $(controlId).addClass('error-input-inner');
            }
            break;
        case 'Numeric':
            nflag = AllowNumeric(controlvalue);
            if (!nflag) {
                $(controlId).after("<span class='error'>Only numeric value is allowed.</span>");
                $(controlId).addClass('error-input-inner');
            }
            break;
        case 'Decimal':
            nflag = AllowDecimal(controlvalue);
            if (!nflag) {
                $(controlId).after("<span class='error'>Please Enter Decimal Value.</span>");
                $(controlId).addClass('error-input-inner');
            }
            break;
        case 'Email':
            nflag = IsEmail(controlvalue);
            if (!nflag) {
                $(controlId).after("<span class='error'>Please Enter Valid Email ID. For eg:sales@timewatchindia.com.</span>");
                $(controlId).addClass('error-input-inner');
            }
            break;
        case 'StringAndNumeric':
            nflag = AllowAlphabetsandNumbers(controlvalue);
            if (!nflag) {
                $(controlId).after("<span class='error'>Special Characters,Space Not Allowed!</span>");
                $(controlId).addClass('error-input-inner');
            }
            break;
        case 'PANNo':
            nflag = PANNumber(controlvalue);
            if (!nflag) {
                $(controlId).after("<span class='error'>First five characters are letters, next four are numerals, last character is a letter.. For eg:AAAPZ1234C.</span>");
                $(controlId).addClass('error-input-inner');
            }
            break;
        case 'PFNo':
            nflag = PFNumber(controlvalue);
            if (!nflag) {
                $(controlId).after("<span class='error'>Invalid Provident Fund Number.</span>");
                $(controlId).addClass('error-input-inner');
            }
            break;
        case 'ESINo':
            nflag = PFNumber(controlvalue);
            if (!nflag) {
                $(controlId).after("<span class='error'>Invalid Employees State Insurance Number.</span>");
                $(controlId).addClass('error-input-inner');
            }
            break;
        case 'UANNo':
            nflag = PFNumber(controlvalue);
            if (!nflag) {
                $(controlId).after("<span class='error'>Invalid Universal Account Number.</span>");
                $(controlId).addClass('error-input-inner');
            }
            break;
        case 'TANNo':
            nflag = TANNumber(controlvalue);
            if (!nflag) {
                $(controlId).after("<span class='error'>First four characters are letters, next five are numerals, last character is a letter.</span>");
                $(controlId).addClass('error-input-inner');
            }
            break;
        case 'StringNumericAndSlash':
            nflag = AllowAlphaNumericWithSlash(controlvalue);
            if (!nflag) {
                $(controlId).after("<span class='error'>Only alphabets,numeric with slash (/) are allowed.</span>");
                $(controlId).addClass('error-input-inner');
            }
            break;
        case 'PostalCode':
            nflag = PostalCode(controlvalue);
            if (!nflag) {
                $(controlId).after("<span class='error'>Only numeric are allowed. For eg:110076.</span>");
                $(controlId).addClass('error-input-inner');
            }
            break;
        case 'Telephone':
            //nflag = AllowTelephone(controlvalue);
            //if (!nflag) {

            $(controlId).after("<span class='error'>Please Enter Valid Telephone Number. For eg:099xxxxxx99.</span>");
            $(controlId).addClass('error-input-inner');
            //}
            break;
        case 'Password':
            nflag = ValidatePassword(controlvalue);
            if (!nflag) {
                $(controlId).after("<span class='error'>Password must be 6 characters long, and must contain at least one uppercase, one lowercase, one number and one special character.</span>");
                $(controlId).addClass('error-input-inner');
            }
            break;
        case 'BankIFSCCode':
            nflag = BankIFSCCode(controlvalue);
            if (!nflag) {
                $(controlId).after("<span class='error'>Invalid Indian Financial System Code. eg. ABCD0123456.</span>");
                $(controlId).addClass('error-input-inner');
            }
            break;
        case 'PGS':
            nflag = PGS(controlvalue);
            if (!nflag) {
                $(controlId).after("<span class='error'>Invalid Pension and Group Schemes Number.</span>");
                $(controlId).addClass('error-input-inner');
            }
            break;
        case 'InsurancePolicy':
            nflag = PGS(controlvalue);
            if (!nflag) {
                $(controlId).after("<span class='error'>Invalid Insurance Policy Number.</span>");
                $(controlId).addClass('error-input-inner');
            }
            break;
        case 'AadharNumber':
            nflag = AadharNumber(controlvalue);
            if (!nflag) {
                $(controlId).after("<span class='error'>Invalid Aadhar Number.</span>");
                $(controlId).addClass('error-input-inner');
            }
            break;
        case 'BankAC':
            nflag = BankAC(controlvalue);
            if (!nflag) {
                $(controlId).after("<span class='error'>Invalid Bank Account Number.</span>");
                $(controlId).addClass('error-input-inner');
            }
            break;
    }
    return nflag;
}
function RemoveError(controlId) {
    if ($(controlId).next("span").length > 0) {
        $(controlId).removeClass('error-input-inner');
        $(controlId).next("span").remove();
    }
}

function Alphanumeric(e) {
    var keyCode = e.keyCode || e.which;
    //Regex for Valid Characters i.e. Alphabets and Numbers.
    var regex = /^[A-Za-z0-9]+$/;
    //Validate TextBox value against the Regex.
    var isValid = regex.test(String.fromCharCode(keyCode));
    if (!isValid) {
        // $(isValid).after("<span class='error'>Company Code  already exist!</span>");
        alert("Only Alphabets and Numbers allowed.");
    }
    return isValid;
};
function setTwoNumberDecimal(el) {
    el.value = parseFloat(el.value).toFixed(2);
    if (el.value > 0) {
        $(el).css('border-color', 'lightgrey');
    }
    else {
        $(el).css('border-color', 'Red');
    }
};
//function setTwoNumberDecimalGross(el) {
//    el.value = parseFloat(el.value).toFixed(2);
//    if (el.value > 0 && el.value < $('#txtBasic').val()) {
//        $(el).css('border-color', 'lightgrey');
//    }
//    else {
//        $(el).css('border-color', 'Red');
//        RequiredFieldError('#txtBasic', 'Basic', 'Basic Amount');
//    }
//};
function setTwoNumberDecimalBasic(el) {
    el.value = parseFloat(el.value).toFixed(2);
    RemoveError('#txtBasic');
    if ((el.value > 0) && (el.value < parseFloat($('#txtGrossSalary').val()))) {
        $(el).css('border-color', 'lightgrey');
        RemoveError('#txtBasic');
    }
    else {
        $(el).css('border-color', 'Red');
        RequiredFieldError('#txtBasic', 'Basic', 'Basic Amount');
    }
};
function setTwoNumberDecimalDA(el) {
    el.value = parseFloat(el.value).toFixed(2);
    RemoveError('#txtDearnessAllowance');
    if ((el.value) > 0 && (el.value < parseFloat($('#txtBasic').val()))) {
        $(el).css('border-color', 'lightgrey');
        RemoveError('#txtDearnessAllowance');
    }
    else {
        $(el).css('border-color', 'Red');
        RequiredFieldError('#txtDearnessAllowance', 'DA', 'Dearness Allowance Amount');
    }
};
//function validatedate(e) {
//    alert(e.value);
//    var inputText = e.value;
//    if (inputText == "") {
//        return false;
//    }
//    var dateformat = /^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/;
//    // Match the date format through regular expression
//    if (inputText.match(dateformat)) {

//        //Test which seperator is used '/' or '-'
//        var opera1 = inputText.split('/');
//        var opera2 = inputText.split('-');
//        lopera1 = opera1.length;
//        lopera2 = opera2.length;
//        // Extract the string into month, date and year
//        if (lopera1 > 1) {
//            var pdate = inputText.split('/');
//        }
//        else if (lopera2 > 1) {
//            var pdate = inputText.split('-');
//        }
//        var dd = parseInt(pdate[0]);
//        var mm = parseInt(pdate[1]);
//        var yy = parseInt(pdate[2]);
//        // Create list of days of a month [assume there is no leap year by default]
//        var ListofDays = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
//        if (mm == 1 || mm > 2) {
//            if (dd > ListofDays[mm - 1]) {
//                alert('Invalid date format!');
//                return false;
//            }
//        }
//        if (mm == 2) {
//            var lyear = false;
//            if ((!(yy % 4) && yy % 100) || !(yy % 400)) {
//                lyear = true;
//            }
//            if ((lyear == false) && (dd >= 29)) {
//                alert('Invalid date format!');
//                return false;
//            }
//            if ((lyear == true) && (dd > 29)) {
//                alert('Invalid date format!');
//                return false;
//            }
//        }
//    }
//    else {
//        alert("Invalid date format!");
//        return false;
//    }
//}
function RequiredFieldError(controlId, FieldType, FieldName) {
    switch (FieldType) {
        case 'CompanyCodeExist':
            $(controlId).after("<span class='error'>Company Code  already exist!</span>");
            break;
        case 'DepartmentCodeExist':
            $(controlId).after("<span class='error'>Department Code already exist!</span>");
            break;
        case 'CategoryCodeExist':
            $(controlId).after("<span class='error'>Category Code already exist!</span>");
            break;
        case 'LocationCodeExist':
            $(controlId).after("<span class='error'>Location Code already exist!</span>");
            break;
        case 'BankCodeExist':
            $(controlId).after("<span class='error'>Location Code already exist!</span>");
            break;
        case 'Input':
            $(controlId).after("<span class='error'>Please Enter " + FieldName + "</span>");
            $(controlId).addClass('error-input-inner');
            break;
        case 'Select':
            $(controlId).after("<span class='error'>Please Select " + FieldName + "</span>");
            $(controlId).addClass('error-input-inner');
            break;
        case 'Basic':
            $(controlId).after("<span class='error'>" + FieldName + " should be less Gross Amount.</span>");
            $(controlId).addClass('error-input-inner');
            break;
        case 'DA':
            $(controlId).after("<span class='error'>" + FieldName + " should be less Basic Amount.</span>");
            $(controlId).addClass('error-input-inner');
            break;
        case 'PAN':
            $(controlId).after("<span class='error'>Length of " + FieldName + " should be equal 10.</span>");
            $(controlId).addClass('error-input-inner');
            break;
        case 'TAN':
            $(controlId).after("<span class='error'>Length of " + FieldName + " should be equal 10.</span>");
            $(controlId).addClass('error-input-inner');
            break;
        case 'PostalCode':
            $(controlId).after("<span class='error'>Length of " + FieldName + " should be equal 6.</span>");
            $(controlId).addClass('error-input-inner');
            break;
    }
}
function validatedate(e) {
    $("#frmdt").hide();
    //alert(e.controlId);
    var inputText = e.value;

    //alert(inputText);
    if (inputText == "") {
        return false;
    }
    var dateformat = /^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/;
    // Match the date format through regular expression
    if (inputText.match(dateformat)) {

        //Test which seperator is used '/' or '-'
        var opera1 = inputText.split('/');
        var opera2 = inputText.split('-');
        lopera1 = opera1.length;
        lopera2 = opera2.length;
        // Extract the string into month, date and year
        if (lopera1 > 1) {
            var pdate = inputText.split('/');
        }
        else if (lopera2 > 1) {
            var pdate = inputText.split('-');
        }
        var dd = parseInt(pdate[0]);
        var mm = parseInt(pdate[1]);
        var yy = parseInt(pdate[2]);
        // Create list of days of a month [assume there is no leap year by default]
        var ListofDays = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
        if (mm == 1 || mm > 2) {
            if (dd > ListofDays[mm - 1]) {
                //alert('Invalid date format!');
                //inputText.val("");
                $(e).after("<span class='error'>Only alphabets are allowed.</span>");
                $(e).addClass('error-input-inner');
                return false;
            }
        }
        if (mm == 2) {
            var lyear = false;
            if ((!(yy % 4) && yy % 100) || !(yy % 400)) {
                lyear = true;
            }
            if ((lyear == false) && (dd >= 29)) {
                //alert('Invalid date format!');
                //inputText.val("");
                // $(e).after("<span class='error'>Invalid date format!</span>");
                $("#frmdt").show();
                $(e).addClass('error-input-inner');
                return false;
            }
            if ((lyear == true) && (dd > 29)) {
                //alert('Invalid date format!');
                //inputText.val("");
                $(e).after("<span class='error'>Only alphabets are allowed.</span>");
                $(e).addClass('error-input-inner');
                return false;
            }
        }
    }
    else {
        $("#frmdt").show();
        //$(e).after("<span class='error'>Only alphabets are allowed.</span>");
        $(e).addClass('error-input-inner');
        return false;
    }
}

