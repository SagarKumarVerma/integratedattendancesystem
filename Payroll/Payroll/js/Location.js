﻿/// <reference path="jquery-1.9.1.intellisense.js" />
const errorMsg = "Error occured, contact to administrator";
//Load Data in Table when documents is ready
$(document).ready(function () {
    var Loccode = $('#txtLocationCode').val().trim();
    BindTableList();
    $("#tblThEdit").removeClass("sorting_asc").addClass("sorting_disabled");
    $(document).ready(function () {
        $("#lnk1").click(function () {

        });
        $("#btnAdd").click(function () {
            $("#myModal").modal("show");
            FocusWhileAdd();
        });
    });
});
function FocusWhileAdd() {
    if ($("#hdnAutoIdFlag").val() == "Y") {
        $("#txtLocationCode").attr("disabled", "disabled");
        $('#myModal').on('shown.bs.modal', function () {
            $('#txtLocationName').focus();
        })
        $('#divcode').show();
    }
    else {
        $("#txtLocationCode").removeAttr('disabled');
        $('#divcode').show();
        $('#myModal').on('shown.bs.modal', function () {
            $('#txtLocationCode').focus();
        });
    }
}
//cancel function
function cancel() {
    $('#tblLocation').DataTable().destroy();
    BindTableList();
    ClerData();
}
//code for bind Jquery server side datatable 
function BindTableList() {
    $.ajax({
        url: "/Location/List",
        type: "GET",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (result) {
            if (result == "error") {
                alert(errorMsg);
                return false;
            }
            else {
                var table = $('#tblLocation').DataTable({
                    "order": [[3, "Asc"]],
                    "scrollX": true,
                    "scrollY": true,
                    "ordering": true,
                    "processing": true,
                    "serverSide": true,
                    "filter": true,
                    "orderMulti": true,
                    "autoWidth": false,
                    "scrollCollapse": true,

                    "ajax": {
                        "url": "/Location/GetAllLocation",
                        async: false,
                        "type": "POST",
                        "datatype": "json"
                    },
                    "aoColumns": [
                        { "mDataProp": "LocationCode" },
                        { "mDataProp": "LocationCode" },
                        { "mDataProp": "LocationCode" },
                        { "mDataProp": "LocationName" },
                        { "mDataProp": "LocationCity" },
                        { "mDataProp": "LocationState" },
                        { "mDataProp": "LocationPostalCode" },
                        { "mDataProp": "LocationCountry" },
                        { "mDataProp": "LocationAddress1" },
                        { "mDataProp": "LocationAddress2" },
                        { "mDataProp": "LocationContactEmailID" },
                    ],
                    "columnDefs": [

                    ],
                    "fnRowCallback": function (nRow, aData, iDisplayIndex) {
                        var hdnlang = $("#hdn_lang").val();
                        if (hdnlang == "E") {
                            $('td:eq(0)', nRow).html("<a href='javascript:void(0);' onclick=getbyID('" + aData.LocationCode.trim() + "')><i class='fa fa-pen'></i> Edit</a>");
                        }
                        else {
                            $('td:eq(0)', nRow).html("<a href='javascript:void(0);' onclick=getbyID('" + aData.LocationCode.trim() + "')><i class='fa fa-pen'></i> يحرر</a>");
                        }
                        if (hdnlang == "E") {
                            $('td:eq(1)', nRow).html("<a href='javascript:void(0);' onclick=Delete('" + aData.LocationCode.trim() + "')><i class='fa fa-trash'></i> Delete</a>");
                        }
                        else {
                            $('td:eq(1)', nRow).html("<a href='javascript:void(0);' onclick=Delete('" + aData.LocationCode.trim() + "')><i class='fa fa-trash'></i> حذف</a>");
                        }
                        $('td:eq(8)', nRow).html("<textarea disabled style='width:400px; height:100px'>" + aData.LocationAddress1 + "</textarea>");
                        $('td:eq(9)', nRow).html("<textarea disabled style='width:400px; height:100px'>" + aData.LocationAddress2 + "</textarea>");
                        return nRow;
                    },
                    'columnDefs': [{
                        'targets': [0, 1], // column index (start from 0)
                        'orderable': false, // set orderable false for selected columns
                    }]

                });
            }

        },
        error: function (errormessage) {
            alert(errormessage.responseText);
        }
    });
}
//function for add Location Master Function
function Add() {
    var res = validate();
    if (res == false) {
        $("#btnAddNew").attr("disabled", "disabled");
        return false;
    }
    else {
        $("#btnAddNew").removeAttr('disabled');
    }
    var LOCObj = {
        LocationCode: $('#txtLocationCode').val().trim(),
        LocationName: $('#txtLocationName').val(),
        LocationCity: $('#txtLocationCity').val(),
        LocationState: $('#txtLocationState').val(),
        LocationPostalCode: $('#txtLocationPostalCode').val(),
        LocationCountry: $('#txtLocationCountry').val(),
        LocationAddress1: $('#txtLocationAddress1').val(),
        LocationAddress2: $('#txtLocationAddress2').val(),
        LocationContactEmailID: $('#txtLocationContactEmailID').val(),
        AutoGeneratedFlag: $("#hdnAutoIdFlag").val()
    };
    $.ajax({
        url: "/Location/Add",
        data: JSON.stringify(LOCObj),
        type: "POST",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (result) {
            if (result == "error") {
                alert(errorMsg);
                return false;
            }
            else {
                insertedmessage();
                ClerData();
                // GetAutoID();
            }
        },
        error: function (errormessage) {
            alert(errormessage.responseText);
        }
    });
}
//function for update Location record
function Update() {
    var res = validate();
    if (res == false) {
        $("#btnAddNew").attr("disabled", "disabled");
        return false;
    }
    else {
        $("#btnAddNew").removeAttr('disabled');
    }
    var LOCObj = {
        LocationCode: $('#txtLocationCode').val().trim(),
        LocationName: $('#txtLocationName').val(),
        LocationCity: $('#txtLocationCity').val(),
        LocationState: $('#txtLocationState').val(),
        LocationPostalCode: $('#txtLocationPostalCode').val(),
        LocationCountry: $('#txtLocationCountry').val(),
        LocationAddress1: $('#txtLocationAddress1').val(),
        LocationAddress2: $('#txtLocationAddress2').val(),
        LocationContactEmailID: $('#txtLocationContactEmailID').val(),
        AutoGeneratedFlag: $("#hdnAutoIdFlag").val()
    };
    $.ajax({
        url: "/Location/Update",
        data: JSON.stringify(LOCObj),
        type: "POST",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (result) {
            if (result == "error") {
                alert(errorMsg);
                return false;
            }
            else {
                updatedmessgae();
                ClerData();
                $('#tblLocation').DataTable().destroy();
                BindTableList();
                $('#myModal').modal('hide');
            }
        },
        error: function (errormessage) {
            alert(errormessage.responseText);
        }
    });
}
//function for delete Location  records
function Delete(LocationCode) {
    var ans = confirm(deleteconfirmmsg);
    if (ans) {
        $.ajax({
            url: "/Location/Delete/" + LocationCode,
            async: false,
            type: "POST",
            contentType: "application/json;charset=UTF-8",
            dataType: "json",
            success: function (result) {
                if (result == "error") {
                    alert(errorMsg);
                    return false;
                }
                else {
                    deletedmessage();
                    $('#tblLocation').DataTable().destroy();
                    BindTableList();
                }
            },
            error: function (errormessage) {
                alert(errormessage.responseText);
            }
        });
    }
}
//Function for getting the Data Based upon Location Code
function getbyID(LocationCode) {
    $('#divcode').show();
    $('#txtLocationCode').css('border-color', 'lightgrey');
    $('#txtLocationName').css('border-color', 'lightgrey');
    $('#txtLocationCity').css('border-color', 'lightgrey');
    $('#txtLocationState').css('border-color', 'lightgrey');
    $('#txtLocationPostalCode').css('border-color', 'lightgrey');
    $('#txtLocationCountry').css('border-color', 'lightgrey');
    $('#txtLocationAddress1').css('border-color', 'lightgrey');
    $('#txtLocationAddress2').css('border-color', 'lightgrey');
    $('#txtLocationContactEmailID').css('border-color', 'lightgrey');

    $.ajax({
        url: "/Location/GetbyID/" + LocationCode,
        async: false,
        typr: "GET",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        success: function (result) {
            if (result == "error") {
                alert(errorMsg);
                return false;
            }
            else {
                var parseResult = jQuery.parseJSON(result);
                $('#txtLocationCode').val(parseResult.LocationCode);
                $('#txtLocationName').val(parseResult.LocationName);
                $('#txtLocationCity').val(parseResult.LocationCity);
                $('#txtLocationState').val(parseResult.LocationState);
                $('#txtLocationPostalCode').val(parseResult.LocationPostalCode);
                $('#txtLocationCountry').val(parseResult.LocationCountry);
                $('#txtLocationAddress1').val(parseResult.LocationAddress1);
                $('#txtLocationAddress2').val(parseResult.LocationAddress2);
                $('#txtLocationContactEmailID').val(parseResult.LocationContactEmailID);
                $("#txtLocationCode").attr("disabled", "disabled");
                $('#myModal').modal('show');
                $('#myModal').on('shown.bs.modal', function () {
                    $('#txtLocationCode').focus();
                })
                $('#btnUpdate').show();
                $('#btnAddNew').hide();
            }
        },
        error: function (errormessage) {
            alert(errormessage.responseText);
        }
    });
    return false;
}
//Function for clearing the textboxes
function ClerData() {
    $('#txtLocationCode').val("");
    $('#txtLocationName').val("");
    $('#txtLocationCity').val("");
    $('#txtLocationState').val("");
    $('#txtLocationPostalCode').val("");
    $('#txtLocationCountry').val("");
    $('#txtLocationAddress1').val("");
    $('#txtLocationAddress2').val("");
    $('#txtLocationContactEmailID').val("");

    $('#btnUpdate').hide();
    $('#btnAddNew').show();

    $('#txtLocationCode').css('border-color', 'lightgrey');
    $('#txtLocationName').css('border-color', 'lightgrey');
    $('#txtLocationCity').css('border-color', 'lightgrey');
    $('#txtLocationState').css('border-color', 'lightgrey');
    $('#txtLocationPostalCode').css('border-color', 'lightgrey');
    $('#txtLocationCountry').css('border-color', 'lightgrey');
    $('#txtLocationAddress1').css('border-color', 'lightgrey');
    $('#txtLocationAddress2').css('border-color', 'lightgrey');
    $('#txtLocationContactEmailID').css('border-color', 'lightgrey');
    RemoveError('#txtLocationCode');
    RemoveError('#txtLocationName');
    RemoveError('#txtLocationCity');
    RemoveError('#txtLocationState');
    RemoveError('#txtLocationCountry');
    RemoveError('#txtLocationPostalCode');
    RemoveError('#txtLocationAddress1');
    RemoveError('#txtLocationAddress2');
    RemoveError('#txtLocationContactEmailID');

}
//Valdidation Check
function validate() {
    var isValid = true;
    RemoveError('#txtLocationCode');
    RemoveError('#txtLocationName');
    RemoveError('#txtLocationCity');
    RemoveError('#txtLocationState');
    RemoveError('#txtLocationCountry');
    RemoveError('#txtLocationPostalCode');
    RemoveError('#txtLocationAddress1');
    RemoveError('#txtLocationAddress2');
    RemoveError('#txtLocationContactEmailID');
    
    if ($('#txtLocationCode').val().trim() == "") {
        $('#txtLocationCode').css('border-color', 'Red');
        RequiredFieldError('#txtLocationCode', 'Input', 'Location Code');
        $("#btnAddNew").attr("disabled", "disabled");
        isValid = false;
    }
    else {
        if (!RequiredFieldTypeError('#txtLocationCode', $('#txtLocationCode').val().trim(), 'StringAndNumeric')) {
            $('#txtLocationCode').css('border-color', 'Red');
            $("#btnAddNew").attr("disabled", "disabled");
            isValid = false;
        }
        else {
            $('#txtLocationCode').css('border-color', 'lightgrey');
            $("#btnAddNew").removeAttr('disabled');
            RemoveError('#txtLocationCode');
        }
    }
    if ($('#txtLocationName').val().trim() == "") {
        $('#txtLocationName').css('border-color', 'Red');
        RequiredFieldError('#txtLocationName', 'Input', 'Location Name');
        $("#btnAddNew").attr("disabled", "disabled");
        isValid = false;
    }
    else {
        if (!RequiredFieldTypeError('#txtLocationName', $('#txtLocationName').val(), 'String')) {
            $("#btnAddNew").attr("disabled", "disabled");
            $('#txtLocationName').css('border-color', 'Red');
            isValid = false;
        }
        else {
            $("#btnAddNew").removeAttr('disabled');
            $('#txtLocationName').css('border-color', 'lightgrey');
            RemoveError('#txtLocationName');
        }
    }
    if ($('#txtLocationCity').val().trim() != "") {
        if (!RequiredFieldTypeError('#txtLocationCity', $('#txtLocationCity').val(), 'String')) {
            $('#txtLocationCity').css('border-color', 'Red');
            $("#btnAddNew").attr("disabled", "disabled");
            isValid = false;
        }
        else {
            $("#btnAddNew").removeAttr('disabled');
            $('#txtLocationCity').css('border-color', 'lightgrey');
            RemoveError('#txtLocationCity');
        }
    }
    else {
        $("#btnAddNew").removeAttr('disabled');
        $('#txtLocationCity').css('border-color', 'lightgrey');
        RemoveError('#txtLocationCity');
    }
    if ($('#txtLocationState').val().trim() != "") {
        if (!RequiredFieldTypeError('#txtLocationState', $('#txtLocationState').val(), 'String')) {
            $('#txtLocationState').css('border-color', 'Red');
            $("#btnAddNew").attr("disabled", "disabled");
            isValid = false;
        }
        else {
            $("#btnAddNew").removeAttr('disabled');
            $('#txtLocationState').css('border-color', 'lightgrey');
            RemoveError('#txtLocationState');
        }
    }
    else {
        $("#btnAddNew").removeAttr('disabled');
        $('#txtLocationState').css('border-color', 'lightgrey');
        RemoveError('#txtLocationState');
    }
    if ($('#txtLocationCountry').val().trim() != "") {
        if (!RequiredFieldTypeError('#txtLocationCountry', $('#txtLocationCountry').val(), 'String')) {
            $('#txtLocationCountry').css('border-color', 'Red');
            $("#btnAddNew").attr("disabled", "disabled");
            isValid = false;
        }
        else {
            $("#btnAddNew").removeAttr('disabled');
            $('#txtLocationCountry').css('border-color', 'lightgrey');
            RemoveError('#txtLocationCountry');
        }
    }
    else {
        $("#btnAddNew").removeAttr('disabled');
        $('#txtLocationCountry').css('border-color', 'lightgrey');
        RemoveError('#txtLocationCountry');
    }
    if ($('#txtLocationPostalCode').val().trim() != "") {
        if (!RequiredFieldTypeError('#txtLocationPostalCode', $('#txtLocationPostalCode').val(), 'PostalCode')) {
            $("#btnAddNew").attr("disabled", "disabled");
            $('#txtLocationPostalCode').css('border-color', 'Red');
            isValid = false;
        }
        else {
            if ($('#txtLocationPostalCode').val().trim().length != 10) {
                $('#txtLocationPostalCode').css('border-color', 'Red');
                RequiredFieldError('#txtLocationPostalCode', 'PostalCode', 'Postal Code');
                $("#btnAddNew").attr("disabled", "disabled");
                isValid = false;
            }
            else {
                $("#btnAddNew").removeAttr('disabled');
                $('#txtLocationPostalCode').css('border-color', 'lightgrey');
                RemoveError('#txtLocationPostalCode');
            }
        }
    }
    else {
        $("#btnAddNew").removeAttr('disabled');
        $('#txtPANNo').css('border-color', 'lightgrey');
        RemoveError('#txtPANNo');
    }
    if ($('#txtLocationAddress1').val().trim() != "") {
        if (!RequiredFieldTypeError('#txtLocationAddress1', $('#txtLocationAddress1').val(), 'Address')) {
            $('#txtLocationAddress1').css('border-color', 'Red');
            $("#btnAddNew").attr("disabled", "disabled");
            isValid = false;
        }
        else {
            $("#btnAddNew").removeAttr('disabled');
            $('#txtLocationAddress1').css('border-color', 'lightgrey');
            RemoveError('#txtLocationAddress1');
        }
    }
    else {
        $("#btnAddNew").removeAttr('disabled');
        $('#txtLocationAddress1').css('border-color', 'lightgrey');
    }
    if ($('#txtLocationAddress2').val().trim() != "") {
        if (!RequiredFieldTypeError('#txtLocationAddress2', $('#txtLocationAddress2').val(), 'Address')) {
            $('#txtLocationAddress2').css('border-color', 'Red');
            $("#btnAddNew").attr("disabled", "disabled");
            isValid = false;
        }
        else {
            $("#btnAddNew").removeAttr('disabled');
            $('#txtLocationAddress2').css('border-color', 'lightgrey');
            RemoveError('#txtLocationAddress2');
        }
    }
    else {
        $("#btnAddNew").removeAttr('disabled');
        $('#txtLocationAddress2').css('border-color', 'lightgrey');
    }
    if ($('#txtLocationContactEmailID').val().trim() != "") {
        if (!RequiredFieldTypeError('#txtLocationContactEmailID', $('#txtLocationContactEmailID').val(), 'Email')) {
            $('#txtLocationContactEmailID').css('border-color', 'Red');
            $("#btnAddNew").attr("disabled", "disabled");
            isValid = false;
        }
        else {
            $('#txtLocationContactEmailID').css('border-color', 'lightgrey');
            RemoveError('#txtLocationContactEmailID');
            $("#btnAddNew").removeAttr('disabled');
        }
    }
    else {
        $('#txtLocationContactEmailID').css('border-color', 'lightgrey');
        RemoveError('#txtLocationContactEmailID');
        $("#btnAddNew").removeAttr('disabled');
    }
    return isValid;
}
//Check Location Code using jquery
function LocationCodeCheck(Loccode) {
    RemoveError('#txtLocationCode');
    $('#divcode').show();
    $('#txtLocationCode').css('border-color', 'lightgrey');
    if (!RequiredFieldTypeError('#txtLocationCode', $('#txtLocationCode').val().trim(), 'StringAndNumeric')) {
        $("#btnAddNew").attr("disabled", "disabled");
        return false;
    }
    else {
        $("#btnAddNew").removeAttr('disabled');
    }
    $.ajax({
        url: "/Location/GetbyLoccode/" + Loccode,
        async: false,
        typr: "GET",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        success: function (result) {
            if (result == "error") {
                alert(errorMsg);
                return false;
            }
            else {
                var parseResult = (result);                
                if (parseResult == 1) {
                    $('#txtLocationCode').css('border-color', 'Red');
                    $("#btnAddNew").attr("disabled", "disabled");
                    RequiredFieldError('#txtLocationCode', 'LocationCodeExist', 'Location Code');
                    return false;
                }
                else {
                    $('#txtLocationCode').css('border-color', 'lightgrey');
                    $("#btnAddNew").removeAttr('disabled');
                    RemoveError('#txtLocationCode');
                }
            }
        },
        error: function (errormessage) {
            alert(errormessage.responseText);
        }
    });
    return false;
}
//Valdidation On Change
function CommonValidation() {
    var isValid = true;
    RemoveError('#txtLocationCode');
    RemoveError('#txtLocationName');
    RemoveError('#txtLocationCity');
    RemoveError('#txtLocationState');
    RemoveError('#txtLocationCountry');
    RemoveError('#txtLocationPostalCode');
    RemoveError('#txtLocationAddress1');
    RemoveError('#txtLocationAddress2');
    RemoveError('#txtLocationContactEmailID');

    if ($('#txtLocationCode').val().trim() == "") {
        $('#txtLocationCode').css('border-color', 'Red');
        RequiredFieldError('#txtLocationCode', 'Input', 'Location Code');
        $("#btnAddNew").attr("disabled", "disabled");
        isValid = false;
    }
    else {
        if (!RequiredFieldTypeError('#txtLocationCode', $('#txtLocationCode').val().trim(), 'StringAndNumeric')) {
            $('#txtLocationCode').css('border-color', 'Red');
            $("#btnAddNew").attr("disabled", "disabled");
            isValid = false;
        }
        else {
            $('#txtLocationCode').css('border-color', 'lightgrey');
            $("#btnAddNew").removeAttr('disabled');
            RemoveError('#txtLocationCode');
        }
    }
    if ($('#txtLocationName').val().trim() == "") {
        $('#txtLocationName').css('border-color', 'Red');
        RequiredFieldError('#txtLocationName', 'Input', 'Location Name');
        $("#btnAddNew").attr("disabled", "disabled");
        isValid = false;
    }
    else {
        if (!RequiredFieldTypeError('#txtLocationName', $('#txtLocationName').val(), 'String')) {
            $("#btnAddNew").attr("disabled", "disabled");
            $('#txtLocationName').css('border-color', 'Red');
            isValid = false;
        }
        else {
            $("#btnAddNew").removeAttr('disabled');
            $('#txtLocationName').css('border-color', 'lightgrey');
            RemoveError('#txtLocationName');
        }
    }
    if ($('#txtLocationCity').val().trim() != "") {
        if (!RequiredFieldTypeError('#txtLocationCity', $('#txtLocationCity').val(), 'String')) {
            $('#txtLocationCity').css('border-color', 'Red');
            $("#btnAddNew").attr("disabled", "disabled");
            isValid = false;
        }
        else {
            $("#btnAddNew").removeAttr('disabled');
            $('#txtLocationCity').css('border-color', 'lightgrey');
            RemoveError('#txtLocationCity');
        }
    }
    else {
        $("#btnAddNew").removeAttr('disabled');
        $('#txtLocationCity').css('border-color', 'lightgrey');
        RemoveError('#txtLocationCity');
    }
    if ($('#txtLocationState').val().trim() != "") {
        if (!RequiredFieldTypeError('#txtLocationState', $('#txtLocationState').val(), 'String')) {
            $('#txtLocationState').css('border-color', 'Red');
            $("#btnAddNew").attr("disabled", "disabled");
            isValid = false;
        }
        else {
            $("#btnAddNew").removeAttr('disabled');
            $('#txtLocationState').css('border-color', 'lightgrey');
            RemoveError('#txtLocationState');
        }
    }
    else {
        $("#btnAddNew").removeAttr('disabled');
        $('#txtLocationState').css('border-color', 'lightgrey');
        RemoveError('#txtLocationState');
    }
    if ($('#txtLocationCountry').val().trim() != "") {
        if (!RequiredFieldTypeError('#txtLocationCountry', $('#txtLocationCountry').val(), 'String')) {
            $('#txtLocationCountry').css('border-color', 'Red');
            $("#btnAddNew").attr("disabled", "disabled");
            isValid = false;
        }
        else {
            $("#btnAddNew").removeAttr('disabled');
            $('#txtLocationCountry').css('border-color', 'lightgrey');
            RemoveError('#txtLocationCountry');
        }
    }
    else {
        $("#btnAddNew").removeAttr('disabled');
        $('#txtLocationCountry').css('border-color', 'lightgrey');
        RemoveError('#txtLocationCountry');
    }
    if ($('#txtLocationPostalCode').val().trim() != "") {
        if (!RequiredFieldTypeError('#txtLocationPostalCode', $('#txtLocationPostalCode').val(), 'PostalCode')) {
            $("#btnAddNew").attr("disabled", "disabled");
            $('#txtLocationPostalCode').css('border-color', 'Red');
            isValid = false;
        }
        else {
            if ($('#txtLocationPostalCode').val().trim().length != 10) {
                $('#txtLocationPostalCode').css('border-color', 'Red');
                RequiredFieldError('#txtLocationPostalCode', 'PostalCode', 'Postal Code');
                $("#btnAddNew").attr("disabled", "disabled");
                isValid = false;
            }
            else {
                $("#btnAddNew").removeAttr('disabled');
                $('#txtLocationPostalCode').css('border-color', 'lightgrey');
                RemoveError('#txtLocationPostalCode');
            }
        }
    }
    else {
        $("#btnAddNew").removeAttr('disabled');
        $('#txtPANNo').css('border-color', 'lightgrey');
        RemoveError('#txtPANNo');
    }
    if ($('#txtLocationAddress1').val().trim() != "") {
        if (!RequiredFieldTypeError('#txtLocationAddress1', $('#txtLocationAddress1').val(), 'Address')) {
            $('#txtLocationAddress1').css('border-color', 'Red');
            $("#btnAddNew").attr("disabled", "disabled");
            isValid = false;
        }
        else {
            $("#btnAddNew").removeAttr('disabled');
            $('#txtLocationAddress1').css('border-color', 'lightgrey');
            RemoveError('#txtLocationAddress1');
        }
    }
    else {
        $("#btnAddNew").removeAttr('disabled');
        $('#txtLocationAddress1').css('border-color', 'lightgrey');
    }
    if ($('#txtLocationAddress2').val().trim() != "") {
        if (!RequiredFieldTypeError('#txtLocationAddress2', $('#txtLocationAddress2').val(), 'Address')) {
            $('#txtLocationAddress2').css('border-color', 'Red');
            $("#btnAddNew").attr("disabled", "disabled");
            isValid = false;
        }
        else {
            $("#btnAddNew").removeAttr('disabled');
            $('#txtLocationAddress2').css('border-color', 'lightgrey');
            RemoveError('#txtLocationAddress2');
        }
    }
    else {
        $("#btnAddNew").removeAttr('disabled');
        $('#txtLocationAddress2').css('border-color', 'lightgrey');
    }
    if ($('#txtLocationContactEmailID').val().trim() != "") {
        if (!RequiredFieldTypeError('#txtLocationContactEmailID', $('#txtLocationContactEmailID').val(), 'Email')) {
            $('#txtLocationContactEmailID').css('border-color', 'Red');
            $("#btnAddNew").attr("disabled", "disabled");
            isValid = false;
        }
        else {
            $('#txtLocationContactEmailID').css('border-color', 'lightgrey');
            RemoveError('#txtLocationContactEmailID');
            $("#btnAddNew").removeAttr('disabled');
        }
    }
    else {
        $('#txtLocationContactEmailID').css('border-color', 'lightgrey');
        RemoveError('#txtLocationContactEmailID');
        $("#btnAddNew").removeAttr('disabled');
    }
    return isValid;
}
