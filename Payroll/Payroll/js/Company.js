﻿/// <reference path="jquery-1.9.1.intellisense.js" />
const errorMsg = "Error occured, contact to administrator";
//Load Data in Table when documents is ready
$(document).ready(function () {
    var Compcode = $('#txtCode').val().trim();
    BindTableList();
    $("#tblThEdit").removeClass("sorting_asc").addClass("sorting_disabled");
    $(document).ready(function () {
        $("#lnk1").click(function () {

        });
        $("#btnAdd").click(function () {
            $("#myModal").modal("show");
            FocusWhileAdd();
            $("#ddlIsActive").attr("disabled", "disabled");
        });

    });
});
//Sagar
function FocusWhileAdd() {
    if ($("#hdnAutoIdFlag").val() == "Y") {
        $("#txtCode").attr("disabled", "disabled");
        $('#myModal').on('shown.bs.modal', function () {
            $('#txtName').focus();
        })
        $('#divcode').show();
    }
    else {
        $("#txtCode").removeAttr('disabled');
        $('#divcode').show();
        $('#myModal').on('shown.bs.modal', function () {
            $('#txtCode').focus();
        });
    }
}
//cancel function
function cancel() {
    $('#tblCompany').DataTable().destroy();
    BindTableList();
    ClerData();

}
//Code for bind Jquery server side datatable 
function BindTableList() {
    $.ajax({
        url: "/Company/List",
        type: "GET",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (result) {
            if (result == "error") {
                alert(errorMsg);
                return false;
            }
            else {
                var table = $('#tblCompany').DataTable({
                    "order": [[3, "Asc"]],
                    "scrollX": true,
                    "scrollY": true,
                    "ordering": true,
                    "processing": true,
                    "serverSide": true,
                    "filter": true,
                    "orderMulti": true,
                    "autoWidth": false,
                    "scrollCollapse": true,

                    "ajax": {
                        "url": "/Company/GetAllCompany",
                        async: false,
                        "type": "POST",
                        "datatype": "json"
                    },

                    "aoColumns": [
                        { "mDataProp": "Code" },
                        { "mDataProp": "Code" },
                        { "mDataProp": "IsActive" },
                        { "mDataProp": "Code" },
                        { "mDataProp": "Name" },
                        { "mDataProp": "ShortName" },
                        { "mDataProp": "IndustryNature" },
                        { "mDataProp": "Address" },
                        { "mDataProp": "PhoneNo" },
                        { "mDataProp": "EmailId" },
                        { "mDataProp": "RegistationNo" },
                        { "mDataProp": "PANNo" },
                        { "mDataProp": "TANNo" },
                        { "mDataProp": "PFNo" },
                        { "mDataProp": "LCNo" },
                    ],
                    "columnDefs": [

                    ],
                    "fnRowCallback": function (nRow, aData, iDisplayIndex) {
                        var hdnlang = $("#hdn_lang").val();
                        if (hdnlang == "E") {
                            $('td:eq(0)', nRow).html("<a href='javascript:void(0);' onclick=getbyID('" + aData.Code.trim() + "')><i class='fa fa-pen'></i> Edit</a>");
                        }
                        else {
                            $('td:eq(0)', nRow).html("<a href='javascript:void(0);' onclick=getbyID('" + aData.Code.trim() + "')><i class='fa fa-pen'></i> يحرر</a>");
                        }
                        if (hdnlang == "E") {
                            $('td:eq(1)', nRow).html("<a href='javascript:void(0);' onclick=Delete('" + aData.Code.trim() + "')><i class='fa fa-trash'></i> Delete</a>");
                        }
                        else {
                            $('td:eq(1)', nRow).html("<a href='javascript:void(0);' onclick=Delete('" + aData.Code.trim() + "')><i class='fa fa-trash'></i> حذف</a>");
                        }
                        $('td:eq(7)', nRow).html("<textarea disabled style='width:400px; height:100px'>" + aData.Address + "</textarea>");
                        return nRow;
                    },
                    'columnDefs': [{
                        'targets': [0, 1], // column index (start from 0)
                        'orderable': false, // set orderable false for selected columns
                    }]

                });
            }

        },
        error: function (errormessage) {
            alert(errormessage.responseText);
        }
    });

}
//Add Data Function 
function Add() {
    var res = validate();
    if (res == false) {
        $("#btnAddNew").attr("disabled", "disabled");
        return false;
    }
    else {
        $("#btnAddNew").removeAttr('disabled');
    }
    //var codecheck = codecheck(Compcode);
    //if (codecheck == false) {
    //    return false;
    //}
    var CompObj = {
        IsActive: $('#ddlIsActive').val(),
        Code: $('#txtCode').val().trim(),
        Name: $('#txtName').val(),
        ShortName: $('#txtShortName').val(),
        IndustryNature: $('#txtIndustryNature').val(),
        Address: $('#txtAddress').val(),
        PhoneNo: $('#txtPhoneNo').val(),
        EmailId: $('#txtEmailId').val(),
        RegistationNo: $('#txtRegistationNo').val(),
        PANNo: $('#txtPANNo').val(),
        TANNo: $('#txtTANNo').val(),
        PFNo: $('#txtPFNo').val(),
        LCNo: $('#txtLCNo').val(),
        AutoGeneratedFlag: $("#hdnAutoIdFlag").val()

    };
    $.ajax({
        url: "/Company/Add",
        data: JSON.stringify(CompObj),
        type: "POST",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (result) {
            if (result == "error") {
                alert(errorMsg);
                return false;
            }
            else {
                insertedmessage();
                ClerData();
                //GetAutoID();
            }
        },
        error: function (errormessage) {
            alert(errormessage.responseText);
        }
    });
}
//Function for getting the Data Based upon Company Code
function getbyID(Code) {
    $('#divcode').show();
    $('#ddlIsActive').css('border-color', 'lightgrey');
    $('#txtCode').css('border-color', 'lightgrey');
    $('#txtName').css('border-color', 'lightgrey');
    $('#txtShortName').css('border-color', 'lightgrey');
    $('#txtIndustryNature').css('border-color', 'lightgrey');
    $('#txtAddress').css('border-color', 'lightgrey');
    $("#txtPhoneNo").css('border-color', 'lightgrey');
    $("#txtEmailId").css('border-color', 'lightgrey');
    $("#txtRegistationNo").css('border-color', 'lightgrey');
    $('#txtPANNo').css('border-color', 'lightgrey');
    $('#txtTANNo').css('border-color', 'lightgrey');
    $('#txtPFNo').css('border-color', 'lightgrey');
    $('#txtLCNo').css('border-color', 'lightgrey');

    $.ajax({
        url: "/Company/GetbyID/" + Code,
        async: false,
        typr: "GET",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        success: function (result) {
            if (result == "error") {
                alert(errorMsg);
                return false;
            }
            else {
                var parseResult = jQuery.parseJSON(result);
                if (parseResult.IsActive == 'True') {
                    $('#ddlIsActive').val('Y');
                }
                else {
                    $('#ddlIsActive').val('N');
                }
                $('#txtCode').val(parseResult.Code);
                $('#txtName').val(parseResult.Name);
                $('#txtShortName').val(parseResult.ShortName);
                $('#txtIndustryNature').val(parseResult.IndustryNature);
                $('#txtAddress').val(parseResult.Address);
                $('#txtPhoneNo').val(parseResult.PhoneNo);
                $('#txtEmailId').val(parseResult.EmailId);
                $('#txtRegistationNo').val(parseResult.RegistationNo);
                $('#txtPANNo').val(parseResult.PANNo);
                $('#txtTANNo').val(parseResult.TANNo);
                $('#txtPFNo').val(parseResult.PFNo);
                $('#txtLCNo').val(parseResult.LCNo);
                $("#ddlIsActive").removeAttr('disabled');
                $("#txtCode").attr("disabled", "disabled");
                $('#myModal').modal('show');
                $('#myModal').on('shown.bs.modal', function () {
                    $('#txtName').focus();
                })
                $('#btnUpdate').show();
                $('#btnAddNew').hide();
            }
        },
        error: function (errormessage) {
            alert(errormessage.responseText);
        }
    });
    return false;
}
//function for updating Company record
function Update() {
    var res = validate();
    if (res == false) {
        $("#btnAddNew").attr("disabled", "disabled");
        return false;
    }
    else {
        $("#btnAddNew").removeAttr('disabled');
    }
    var CompanyObj = {
        IsActive: $('#ddlIsActive').val(),
        Code: $('#txtCode').val().trim(),
        Name: $('#txtName').val(),
        ShortName: $('#txtShortName').val(),
        IndustryNature: $('#txtIndustryNature').val(),
        Address: $('#txtAddress').val(),
        PhoneNo: $('#txtPhoneNo').val(),
        EmailId: $('#txtEmailId').val(),
        RegistationNo: $('#txtRegistationNo').val(),
        PANNo: $('#txtPANNo').val(),
        TANNo: $('#txtTANNo').val(),
        PFNo: $('#txtPFNo').val(),
        LCNo: $('#txtLCNo').val()
    };
    $.ajax({
        url: "/Company/Update",
        data: JSON.stringify(CompanyObj),
        type: "POST",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (result) {
            if (result == "error") {
                alert(errorMsg);
                return false;
            }
            else {
                updatedmessgae();
                //alert("JobDescription modified successfully");
                ClerData();
                $('#tblCompany').DataTable().destroy();
                BindTableList();
                $('#myModal').modal('hide');
            }
        },
        error: function (errormessage) {
            alert(errormessage.responseText);
        }
    });
}
//function for deleting  records
function Delete(Code) {
    var ans = confirm(deleteconfirmmsg);
    if (ans) {
        $.ajax({
            url: "/Company/Delete/" + Code,
            async: false,
            type: "POST",
            contentType: "application/json;charset=UTF-8",
            dataType: "json",
            success: function (result) {
                if (result == "error") {
                    alert(errorMsg);
                    return false;
                }
                else {
                    deletedmessage();
                    $('#tblCompany').DataTable().destroy();
                    BindTableList();
                }
            },
            error: function (errormessage) {
                alert(errormessage.responseText);
            }
        });
    }
}
//Function for clearing the textboxes
function ClerData() {
    $('#ddlIsActive').val("");
    $("select#ddlIsActive")[0].selectedIndex = 0;
    $('#txtCode').val("");
    $('#txtName').val("");
    $('#txtShortName').val("");
    $('#txtIndustryNature').val("");
    $('#txtAddress').val("");
    $('#txtPhoneNo').val("");
    $('#txtEmailId').val("");
    $('#txtRegistationNo').val("");
    $('#txtPANNo').val("");
    $('#txtTANNo').val("");
    $('#txtPFNo').val("");
    $('#txtLCNo').val("");
    $('#btnUpdate').hide();
    $('#btnAddNew').show();

    $('#ddlIsActive').css('border-color', 'lightgrey');
    $('#txtCode').css('border-color', 'lightgrey');
    $('#txtName').css('border-color', 'lightgrey');
    $('#txtShortName').css('border-color', 'lightgrey');
    $('#txtIndustryNature').css('border-color', 'lightgrey');
    $('#txtAddress').css('border-color', 'lightgrey');
    $("#txtPhoneNo").css('border-color', 'lightgrey');
    $("#txtEmailId").css('border-color', 'lightgrey');
    $("#ddlIsActive").attr("disabled", "disabled");
    $("#txtRegistationNo").css('border-color', 'lightgrey');
    $('#txtPANNo').css('border-color', 'lightgrey');
    $('#txtTANNo').css('border-color', 'lightgrey');
    $('#txtLCNo').css('border-color', 'lightgrey');
    RemoveError('#txtCode');
    RemoveError('#txtName');
    RemoveError('#txtPhoneNo');
    RemoveError('#txtEmailId');
    RemoveError('#txtPANNo');
    RemoveError('#txtTANNo');
    RemoveError('#txtShortName');
}
//Check Company Code using jquery
function CompanyCodeCheck(Compcode) {
    RemoveError('#txtCode');
    $('#divcode').show();
    $('#txtCode').css('border-color', 'lightgrey');
    if (!RequiredFieldTypeError('#txtCode', $('#txtCode').val().trim(), 'StringAndNumeric')) {
        $("#btnAddNew").attr("disabled", "disabled");
        return false;
    }
    else {
        $("#btnAddNew").removeAttr('disabled');
    }

    $.ajax({
        url: "/Company/GetbyCompcode/" + Compcode,
        async: false,
        typr: "GET",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        success: function (result) {
            if (result == "error") {
                alert(errorMsg);
                return false;
            }
            else {
                var parseResult = (result);
                if (parseResult == 1) {
                    $('#txtCode').css('border-color', 'Red');
                    $("#btnAddNew").attr("disabled", "disabled");
                    RequiredFieldError('#txtCode', 'CompanyCodeExist', 'Company Code');
                    return false;
                }
                else {
                    $('#txtCode').css('border-color', 'lightgrey');
                    $("#btnAddNew").removeAttr('disabled');
                    RemoveError('#txtCode');
                }
            }
        },
        error: function (errormessage) {
            alert(errormessage.responseText);
        }
    });
    return false;
}
//Valdidation using jquery
function validate() {
    var isValid = true;
   
    if ($('#ddlIsActive').val().trim() == "") {
        $('#ddlIsActive').css('border-color', 'Red');
        isValid = false;
    }
    else {
        $('#ddlIsActive').css('border-color', 'lightgrey');
    }
    if ($('#txtCode').val().trim() == "") {
        $('#txtCode').css('border-color', 'Red');
        RequiredFieldError('#txtCode', 'Input', 'Company Code');
        $("#btnAddNew").attr("disabled", "disabled");
        isValid = false;
    }
    else {
        if (!RequiredFieldTypeError('#txtCode', $('#txtCode').val().trim(), 'StringAndNumeric')) {
            $('#txtCode').css('border-color', 'Red');
            $("#btnAddNew").attr("disabled", "disabled");
            isValid = false;
        }
        else {
            $('#txtCode').css('border-color', 'lightgrey');
            $("#btnAddNew").removeAttr('disabled');
            RemoveError('#txtCode');
        }
    }
    if ($('#txtName').val().trim() == "") {
        $('#txtName').css('border-color', 'Red');
        RequiredFieldError('#txtName', 'Input', 'Company Name');
        $("#btnAddNew").attr("disabled", "disabled");
        isValid = false;
    }
    else {
        if (!RequiredFieldTypeError('#txtName', $('#txtName').val(), 'String')) {
            $('#txtName').css('border-color', 'Red');
            $("#btnAddNew").attr("disabled", "disabled");
            isValid = false;
        }
        else {
            $("#btnAddNew").removeAttr('disabled');
            $('#txtName').css('border-color', 'lightgrey');
            RemoveError('#txtName');
        }
    }
    if ($('#txtShortName').val().trim() == "") {
        $('#txtShortName').css('border-color', 'Red');
        RequiredFieldError('#txtShortName', 'Input', 'Short Name');
        $("#btnAddNew").attr("disabled", "disabled");
        isValid = false;
    }
    else {
        if (!RequiredFieldTypeError('#txtShortName', $('#txtShortName').val(), 'String')) {
            $("#btnAddNew").attr("disabled", "disabled");
            $('#txtShortName').css('border-color', 'Red');
            isValid = false;
        }
        else {
            $("#btnAddNew").removeAttr('disabled');
            $('#txtShortName').css('border-color', 'lightgrey');
            RemoveError('#txtShortName');
        }
    }
    if ($('#txtIndustryNature').val().trim() != "") {
        if (!RequiredFieldTypeError('#txtIndustryNature', $('#txtIndustryNature').val(), 'String')) {
            $('#txtIndustryNature').css('border-color', 'Red');
            $("#btnAddNew").attr("disabled", "disabled");
            isValid = false;
        }
        else {
            $("#btnAddNew").removeAttr('disabled');
            $('#txtIndustryNature').css('border-color', 'lightgrey');
            RemoveError('#txtIndustryNature');
        }
    }
    else {
        $("#btnAddNew").removeAttr('disabled');
        $('#txtIndustryNature').css('border-color', 'lightgrey');
        RemoveError('#txtIndustryNature');
    }
    if ($('#txtAddress').val().trim() != "") {
        if (!RequiredFieldTypeError('#txtAddress', $('#txtAddress').val(), 'Address')) {
            $('#txtAddress').css('border-color', 'Red');
            $("#btnAddNew").attr("disabled", "disabled");
            isValid = false;
        }
        else {
            $("#btnAddNew").removeAttr('disabled');
            $('#txtAddress').css('border-color', 'lightgrey');
            RemoveError('#txtAddress');
        }
    }
    else {
        $("#btnAddNew").removeAttr('disabled');
        $('#txtAddress').css('border-color', 'lightgrey');
    }
    if ($('#txtRegistationNo').val().trim() != "") {
        if (!RequiredFieldTypeError('#txtRegistationNo', $('#txtRegistationNo').val(), 'StringAndNumeric')) {
            $('#txtRegistationNo').css('border-color', 'Red');
            $("#btnAddNew").attr("disabled", "disabled");
            isValid = false;
        }
        else {
            $("#btnAddNew").removeAttr('disabled');
            $('#txtRegistationNo').css('border-color', 'lightgrey');
            RemoveError('#txtRegistationNo');
        }
    }
    else {
        $("#btnAddNew").removeAttr('disabled');
        $('#txtRegistationNo').css('border-color', 'lightgrey');
        RemoveError('#txtRegistationNo');
    }
    if ($('#txtPhoneNo').val().trim() != "") {
        if ($('#txtPhoneNo').val().trim().length != 11) {
            $('#txtPhoneNo').css('border-color', 'Red');
            RequiredFieldTypeError('#txtPhoneNo', $('#txtPhoneNo').val(), 'Telephone');
            isValid = false;
        }
        else {
            $('#txtPhoneNo').css('border-color', 'lightgrey');
            RemoveError('#txtPhoneNo');
        }
    }
    else {
        $('#txtPhoneNo').css('border-color', 'lightgrey');
        RemoveError('#txtPhoneNo');
    }
    if ($('#txtEmailId').val().trim() != "") {
        if (!RequiredFieldTypeError('#txtEmailId', $('#txtEmailId').val(), 'Email')) {
            $('#txtEmailId').css('border-color', 'Red');
            $("#btnAddNew").attr("disabled", "disabled");
            isValid = false;
        }
        else {
            $('#txtEmailId').css('border-color', 'lightgrey');
            RemoveError('#txtEmailId');
            $("#btnAddNew").removeAttr('disabled');
        }
    }
    else {
        $('#txtEmailId').css('border-color', 'lightgrey');
        RemoveError('#txtEmailId');
        $("#btnAddNew").removeAttr('disabled');
    }
    if ($('#txtPANNo').val().trim() != "") {
        if (!RequiredFieldTypeError('#txtPANNo', $('#txtPANNo').val(), 'PANNo')) {
            $("#btnAddNew").attr("disabled", "disabled");
            $('#txtPANNo').css('border-color', 'Red');
            isValid = false;
        }
        else {
            if ($('#txtPANNo').val().trim().length != 10) {
                $('#txtPANNo').css('border-color', 'Red');
                RequiredFieldError('#txtPANNo', 'PAN', 'Permanent Account Number');
                $("#btnAddNew").attr("disabled", "disabled");
                isValid = false;
            }
            else {
                $("#btnAddNew").removeAttr('disabled');
                $('#txtPANNo').css('border-color', 'lightgrey');
                RemoveError('#txtPANNo');
            }
        }
    }
    else {
        $("#btnAddNew").removeAttr('disabled');
        $('#txtPANNo').css('border-color', 'lightgrey');
        RemoveError('#txtPANNo');
    }
    if ($('#txtLCNo').val().trim() != "") {
        if (!RequiredFieldTypeError('#txtLCNo', $('#txtLCNo').val(), 'StringNumericAndSlash')) {
            $('#txtLCNo').css('border-color', 'Red');
            $("#btnAddNew").attr("disabled", "disabled");
            isValid = false;
        }
        else {
            $("#btnAddNew").removeAttr('disabled');
            $('#txtLCNo').css('border-color', 'lightgrey');
            RemoveError('#txtLCNo');
        }
    }
    else {
        $("#btnAddNew").removeAttr('disabled');
        $('#txtLCNo').css('border-color', 'lightgrey');
        RemoveError('#txtLCNo');
    }    
    if ($('#txtPFNo').val().trim() != "") {
        if (!RequiredFieldTypeError('#txtPFNo', $('#txtPFNo').val(), 'PFNo')) {
            $('#txtPFNo').css('border-color', 'Red');
            $("#btnAddNew").attr("disabled", "disabled");
            isValid = false;
        }
        else {
            $("#btnAddNew").removeAttr('disabled');
            $('#txtPFNo').css('border-color', 'lightgrey');
            RemoveError('#txtPFNo');
        }
    }
    else {
        $("#btnAddNew").removeAttr('disabled');
        $('#txtPFNo').css('border-color', 'lightgrey');
        RemoveError('#txtPFNo');
    } 
    if ($('#txtTANNo').val().trim() != "") {
        if (!RequiredFieldTypeError('#txtTANNo', $('#txtTANNo').val(), 'TANNo')) {
            $("#btnAddNew").attr("disabled", "disabled");
            $('#txtTANNo').css('border-color', 'Red');
            isValid = false;
        }
        else {
            if ($('#txtTANNo').val().trim().length != 10) {
                $('#txtTANNo').css('border-color', 'Red');
                RequiredFieldError('#txtTANNo', 'TAN', 'Tax Deduction and Collection Account Number');
                $("#btnAddNew").attr("disabled", "disabled");
                isValid = false;
            }
            else {
                $("#btnAddNew").removeAttr('disabled');
                $('#txtTANNo').css('border-color', 'lightgrey');
                RemoveError('#txtTANNo');
            }
        }
    }
    else {
        $("#btnAddNew").removeAttr('disabled');
        $('#txtTANNo').css('border-color', 'lightgrey');
        RemoveError('#txtTANNo');
    }

    RemoveError('#txtCode');
    RemoveError('#txtName');
    RemoveError('#txtPhoneNo');
    RemoveError('#txtEmailId');
    RemoveError('#txtPANNo');
    RemoveError('#txtTANNo');
    RemoveError('#txtShortName');
    RemoveError('#txtIndustryNature');
    RemoveError('#txtAddress');
    RemoveError('#txtRegistationNo');
    RemoveError('#txtLCNo');
    RemoveError('#txtPFNo');
    RemoveError('#txtTANNo');
    return isValid;
}
//Valdidation OnChange
function CommonValidation() {
    var isValid = true;
    RemoveError('#txtCode');
    RemoveError('#txtName');
    RemoveError('#txtPhoneNo');
    RemoveError('#txtEmailId');
    RemoveError('#txtPANNo');
    RemoveError('#txtShortName');
    RemoveError('#txtIndustryNature');
    RemoveError('#txtAddress');
    RemoveError('#txtRegistationNo');
    RemoveError('#txtLCNo');
    RemoveError('#txtPFNo');
    RemoveError('#txtTANNo');
    if ($('#ddlIsActive').val().trim() == "") {
        $('#ddlIsActive').css('border-color', 'Red');
        $("#btnAddNew").attr("disabled", "disabled");
        isValid = false;
    }
    else {
        $("#btnAddNew").removeAttr('disabled');
        $('#ddlIsActive').css('border-color', 'lightgrey');
    }
    if ($('#txtCode').val().trim() == "") {
        $('#txtCode').css('border-color', 'Red');
        RequiredFieldError('#txtCode', 'Input', 'Company Code');
        $("#btnAddNew").attr("disabled", "disabled");
        isValid = false;
    }
    else {
        if (!RequiredFieldTypeError('#txtCode', $('#txtCode').val().trim(), 'StringAndNumeric')) {
            $('#txtCode').css('border-color', 'Red');
            $("#btnAddNew").attr("disabled", "disabled");
            isValid = false;
        }
        else {
            $('#txtCode').css('border-color', 'lightgrey');
            $("#btnAddNew").removeAttr('disabled');
            RemoveError('#txtCode');
        }
    }
    if ($('#txtName').val().trim() == "") {
        $('#txtName').css('border-color', 'Red');
        RequiredFieldError('#txtName', 'Input', 'Company Name');
        $("#btnAddNew").attr("disabled", "disabled");
        isValid = false;
    }
    else {
        if (!RequiredFieldTypeError('#txtName', $('#txtName').val(), 'String')) {
            $("#btnAddNew").attr("disabled", "disabled");
            $('#txtName').css('border-color', 'Red');
            isValid = false;
        }
        else {
            $("#btnAddNew").removeAttr('disabled');
            $('#txtName').css('border-color', 'lightgrey');
            RemoveError('#txtName');
        }
    }
    if ($('#txtShortName').val().trim() == "") {
        $('#txtShortName').css('border-color', 'Red');
        RequiredFieldError('#txtShortName', 'Input', 'Short Name');
        $("#btnAddNew").attr("disabled", "disabled");
        isValid = false;
    }
    else {
        if (!RequiredFieldTypeError('#txtShortName', $('#txtShortName').val(), 'String')) {
            $("#btnAddNew").attr("disabled", "disabled");
            $('#txtShortName').css('border-color', 'Red');
            isValid = false;
        }
        else {
            $("#btnAddNew").removeAttr('disabled');
            $('#txtShortName').css('border-color', 'lightgrey');
            RemoveError('#txtShortName');
        }
    }
    if ($('#txtIndustryNature').val().trim() != "") {
        if (!RequiredFieldTypeError('#txtIndustryNature', $('#txtIndustryNature').val(), 'String')) {
            $('#txtIndustryNature').css('border-color', 'Red');
            $("#btnAddNew").attr("disabled", "disabled");
            isValid = false;
        }
        else {
            $("#btnAddNew").removeAttr('disabled');
            $('#txtIndustryNature').css('border-color', 'lightgrey');
            RemoveError('#txtIndustryNature');
        }
    }
    else {
        $("#btnAddNew").removeAttr('disabled');
        $('#txtIndustryNature').css('border-color', 'lightgrey');
        RemoveError('#txtIndustryNature');
    }
    if ($('#txtAddress').val().trim() != "") {
        if (!RequiredFieldTypeError('#txtAddress', $('#txtAddress').val(), 'Address')) {
            $('#txtAddress').css('border-color', 'Red');
            $("#btnAddNew").attr("disabled", "disabled");
            isValid = false;
        }
        else {
            $("#btnAddNew").removeAttr('disabled');
            $('#txtAddress').css('border-color', 'lightgrey');
            RemoveError('#txtAddress');
        }
    }
    else {
        $("#btnAddNew").removeAttr('disabled');
        $('#txtAddress').css('border-color', 'lightgrey');
    }
    if ($('#txtRegistationNo').val().trim() != "") {
        if (!RequiredFieldTypeError('#txtRegistationNo', $('#txtRegistationNo').val(), 'StringAndNumeric')) {
            $('#txtRegistationNo').css('border-color', 'Red');
            $("#btnAddNew").attr("disabled", "disabled");
            isValid = false;
        }
        else {
            $("#btnAddNew").removeAttr('disabled');
            $('#txtRegistationNo').css('border-color', 'lightgrey');
            RemoveError('#txtRegistationNo');
        }
    }
    else {
        $("#btnAddNew").removeAttr('disabled');
        $('#txtRegistationNo').css('border-color', 'lightgrey');
        RemoveError('#txtRegistationNo');
    }
    if ($('#txtPhoneNo').val().trim() != "") {
        if ($('#txtPhoneNo').val().trim().length != 11) {
            $('#txtPhoneNo').css('border-color', 'Red');
            RequiredFieldTypeError('#txtPhoneNo', $('#txtPhoneNo').val(), 'Telephone');
            $("#btnAddNew").attr("disabled", "disabled");
            isValid = false;
        }
        else {
            $("#btnAddNew").removeAttr('disabled');
            $('#txtPhoneNo').css('border-color', 'lightgrey');
            RemoveError('#txtPhoneNo');
        }
    }
    else {
        $("#btnAddNew").removeAttr('disabled');
        $('#txtPhoneNo').css('border-color', 'lightgrey');
        RemoveError('#txtPhoneNo');
    }
    if ($('#txtEmailId').val().trim() != "") {
        if (!RequiredFieldTypeError('#txtEmailId', $('#txtEmailId').val(), 'Email')) {
            $('#txtEmailId').css('border-color', 'Red');
            $("#btnAddNew").attr("disabled", "disabled");
            isValid = false;
        }
        else {
            $('#txtEmailId').css('border-color', 'lightgrey');
            RemoveError('#txtEmailId');
            $("#btnAddNew").removeAttr('disabled');
        }
    }
    else {
        $('#txtEmailId').css('border-color', 'lightgrey');
        RemoveError('#txtEmailId');
        $("#btnAddNew").removeAttr('disabled');
    }
    if ($('#txtPANNo').val().trim() != "") {
        if (!RequiredFieldTypeError('#txtPANNo', $('#txtPANNo').val(), 'PANNo')) {
            $("#btnAddNew").attr("disabled", "disabled");
            $('#txtPANNo').css('border-color', 'Red');
            isValid = false;
        }
        else {
            if ($('#txtPANNo').val().trim().length != 10) {
                $('#txtPANNo').css('border-color', 'Red');
                RequiredFieldError('#txtPANNo', 'PAN', 'Permanent Account Number');
                $("#btnAddNew").attr("disabled", "disabled");
                isValid = false;
            }
            else {
                $("#btnAddNew").removeAttr('disabled');
                $('#txtPANNo').css('border-color', 'lightgrey');
                RemoveError('#txtPANNo');
            }
        }
    }
    else {
        $("#btnAddNew").removeAttr('disabled');
        $('#txtPANNo').css('border-color', 'lightgrey');
        RemoveError('#txtPANNo');
    }
    if ($('#txtLCNo').val().trim() != "") {
        if (!RequiredFieldTypeError('#txtLCNo', $('#txtLCNo').val(), 'StringNumericAndSlash')) {
            $('#txtLCNo').css('border-color', 'Red');
            $("#btnAddNew").attr("disabled", "disabled");
            isValid = false;
        }
        else {
            $("#btnAddNew").removeAttr('disabled');
            $('#txtLCNo').css('border-color', 'lightgrey');
            RemoveError('#txtLCNo');
        }
    }
    else {
        $("#btnAddNew").removeAttr('disabled');
        $('#txtLCNo').css('border-color', 'lightgrey');
        RemoveError('#txtLCNo');
    } 
    if ($('#txtPFNo').val().trim() != "") {
        if (!RequiredFieldTypeError('#txtPFNo', $('#txtPFNo').val(), 'PFNo')) {
            $('#txtPFNo').css('border-color', 'Red');
            $("#btnAddNew").attr("disabled", "disabled");
            isValid = false;
        }
        else {
            $("#btnAddNew").removeAttr('disabled');
            $('#txtPFNo').css('border-color', 'lightgrey');
            RemoveError('#txtPFNo');
        }
    }
    else {
        $("#btnAddNew").removeAttr('disabled');
        $('#txtPFNo').css('border-color', 'lightgrey');
        RemoveError('#txtPFNo');
    }   
    if ($('#txtTANNo').val().trim() != "") {
        if (!RequiredFieldTypeError('#txtTANNo', $('#txtTANNo').val(), 'TANNo')) {
            $("#btnAddNew").attr("disabled", "disabled");
            $('#txtTANNo').css('border-color', 'Red');
            isValid = false;
        }
        else {
            if ($('#txtTANNo').val().trim().length != 10) {
                $('#txtTANNo').css('border-color', 'Red');
                RequiredFieldError('#txtTANNo', 'TAN', 'Tax Deduction and Collection Account Number');
                $("#btnAddNew").attr("disabled", "disabled");
                isValid = false;
            }
            else {
                $("#btnAddNew").removeAttr('disabled');
                $('#txtTANNo').css('border-color', 'lightgrey');
                RemoveError('#txtTANNo');
            }
        }
    }
    else {
        $("#btnAddNew").removeAttr('disabled');
        $('#txtTANNo').css('border-color', 'lightgrey');
        RemoveError('#txtTANNo');
    }
    return isValid;
}


