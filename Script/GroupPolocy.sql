CREATE TABLE [dbo].[tblEmployeeGroupPolicy](
	[GroupID] [varchar](3) NOT NULL,
	[GroupName] [varchar](50) NOT NULL,
	[SHIFT] [char](3) NULL,
	[SHIFTTYPE] [char](1) NULL,
	[SHIFTPATTERN] [char](11) NULL,
	[SHIFTREMAINDAYS] [int] NULL,
	[LASTSHIFTPERFORMED] [char](3) NULL,
	[INONLY] [char](1) NULL,
	[ISPUNCHALL] [char](1) NULL,
	[ISTIMELOSSALLOWED] [char](1) NULL,
	[ALTERNATE_OFF_DAYS] [char](10) NULL,
	[CDAYS] [float] NULL,
	[ISROUNDTHECLOCKWORK] [char](1) NULL,
	[ISOT] [char](1) NULL,
	[OTRATE] [char](6) NULL,
	[FIRSTOFFDAY] [char](3) NULL,
	[SECONDOFFTYPE] [char](1) NULL,
	[HALFDAYSHIFT] [char](3) NULL,
	[SECONDOFFDAY] [char](3) NULL,
	[PERMISLATEARRIVAL] [int] NULL,
	[PERMISEARLYDEPRT] [int] NULL,
	[ISAUTOSHIFT] [char](1) NULL,
	[ISOUTWORK] [char](1) NULL,
	[MAXDAYMIN] [float] NULL,
	[ISOS] [char](1) NULL,
	[AUTH_SHIFTS] [char](50) NULL,
	[TIME] [int] NULL,
	[SHORT] [int] NULL,
	[HALF] [int] NULL,
	[ISHALFDAY] [char](1) NULL,
	[ISSHORT] [char](1) NULL,
	[TWO] [char](1) NULL,
	[isReleaver] [char](1) NULL,
	[isWorker] [char](1) NULL,
	[isFlexi] [char](1) NULL,
	[SSN] [varchar](100) NOT NULL,
	[MIS] [char](1) NULL,
	[IsCOF] [char](1) NULL,
	[HLFAfter] [int] NULL,
	[HLFBefore] [int] NULL,
	[ResignedAfter] [int] NULL,
	[EnableAutoResign] [char](1) NULL,
	[S_END] [datetime] NULL,
	[S_OUT] [datetime] NULL,
	[AUTOSHIFT_LOW] [int] NULL,
	[AUTOSHIFT_UP] [int] NULL,
	[ISPRESENTONWOPRESENT] [char](1) NULL,
	[ISPRESENTONHLDPRESENT] [char](1) NULL,
	[NightShiftFourPunch] [char](1) NULL,
	[ISAUTOABSENT] [char](1) NULL,
	[ISAWA] [char](1) NULL,
	[ISWA] [char](1) NULL,
	[ISAW] [char](1) NULL,
	[ISPREWO] [char](1) NULL,
	[ISOTOUTMINUSSHIFTENDTIME] [char](1) NULL,
	[ISOTWRKGHRSMINUSSHIFTHRS] [char](1) NULL,
	[ISOTEARLYCOMEPLUSLATEDEP] [char](1) NULL,
	[DEDUCTHOLIDAYOT] [int] NULL,
	[DEDUCTWOOT] [int] NULL,
	[ISOTEARLYCOMING] [char](1) NULL,
	[OTMinus] [char](1) NULL,
	[OTROUND] [char](1) NULL,
	[OTEARLYDUR] [int] NULL,
	[OTLATECOMINGDUR] [int] NULL,
	[OTRESTRICTENDDUR] [int] NULL,
	[DUPLICATECHECKMIN] [int] NULL,
	[PREWO] [int] NULL,
	[CompanyCode] [varchar](4) NOT NULL,
CONSTRAINT [PK_tblEmployeeGroupPolicy] PRIMARY KEY CLUSTERED 
(
	[GroupID] ASC,
	[CompanyCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
DROP procedure tblEmployeeGroupPolicy_Add
GO
Create procedure tblEmployeeGroupPolicy_Add     
(
@GroupID varchar(50),@GroupName varchar(50),@SHIFT char(3),@SHIFTTYPE char(1),@SHIFTPATTERN char(11),@SHIFTREMAINDAYS int,@INONLY char(1),@ISPUNCHALL char(1),@ISTIMELOSSALLOWED char(1),@ALTERNATE_OFF_DAYS char(10),@CDAYS float(8),@ISROUNDTHECLOCKWORK char(1),@ISOT char(1),@OTRATE char(6),@FIRSTOFFDAY char(3),@SECONDOFFTYPE char(1),@HALFDAYSHIFT char(3),@SECONDOFFDAY char(3),@PERMISLATEARRIVAL int,@PERMISEARLYDEPRT int,@ISAUTOSHIFT char(1),@ISOUTWORK char(1),@MAXDAYMIN float(8),@ISOS char(1),@AUTH_SHIFTS char(50),@TIME int,@SHORT int,@HALF int,@ISHALFDAY char(1),@ISSHORT char(1),@TWO char(1),@isReleaver char(1),@isWorker char(1),@isFlexi char(1),@SSN varchar(100),@MIS char(1),@IsCOF char(1),@HLFAfter int,@HLFBefore int,@ResignedAfter int,@EnableAutoResign char(1),@S_END datetime,@S_OUT datetime,@AUTOSHIFT_LOW int,@AUTOSHIFT_UP int,@ISPRESENTONWOPRESENT char(1),@ISPRESENTONHLDPRESENT char(1),@NightShiftFourPunch char(1),@ISAUTOABSENT char(1),@ISAWA char(1),@ISWA char(1),@ISAW char(1),@ISPREWO char(1),@ISOTOUTMINUSSHIFTENDTIME char(1),@ISOTWRKGHRSMINUSSHIFTHRS char(1),@ISOTEARLYCOMEPLUSLATEDEP char(1),@DEDUCTHOLIDAYOT int,@DEDUCTWOOT int,@ISOTEARLYCOMING char(1),@OTMinus char(1),@OTROUND char(1),@OTEARLYDUR int,@OTLATECOMINGDUR int,@OTRESTRICTENDDUR int,@DUPLICATECHECKMIN int,@PREWO int,@CompanyCode varchar(4)
)    
as     
Begin     
    Insert into tblEmployeeGroupPolicy (GroupID,GroupName,SHIFT,SHIFTTYPE,SHIFTPATTERN,SHIFTREMAINDAYS,INONLY,ISPUNCHALL,ISTIMELOSSALLOWED,ALTERNATE_OFF_DAYS,CDAYS,ISROUNDTHECLOCKWORK,ISOT,OTRATE,FIRSTOFFDAY,SECONDOFFTYPE,HALFDAYSHIFT,SECONDOFFDAY,PERMISLATEARRIVAL,PERMISEARLYDEPRT,ISAUTOSHIFT,ISOUTWORK,MAXDAYMIN,ISOS,AUTH_SHIFTS,TIME,SHORT,HALF,ISHALFDAY,ISSHORT,TWO,isReleaver,isWorker,isFlexi,SSN,MIS,IsCOF,HLFAfter,HLFBefore,ResignedAfter,EnableAutoResign,S_END,S_OUT,AUTOSHIFT_LOW,AUTOSHIFT_UP,ISPRESENTONWOPRESENT,ISPRESENTONHLDPRESENT,NightShiftFourPunch,ISAUTOABSENT,ISAWA,ISWA,ISAW,ISPREWO,ISOTOUTMINUSSHIFTENDTIME,ISOTWRKGHRSMINUSSHIFTHRS,ISOTEARLYCOMEPLUSLATEDEP,DEDUCTHOLIDAYOT,DEDUCTWOOT,ISOTEARLYCOMING,OTMinus,OTROUND,OTEARLYDUR,OTLATECOMINGDUR,OTRESTRICTENDDUR,DUPLICATECHECKMIN,PREWO,CompanyCode)     
    Values (@GroupID,@GroupName,@SHIFT,@SHIFTTYPE,@SHIFTPATTERN,@SHIFTREMAINDAYS,@INONLY,@ISPUNCHALL,@ISTIMELOSSALLOWED,@ALTERNATE_OFF_DAYS,@CDAYS,@ISROUNDTHECLOCKWORK,@ISOT,@OTRATE,@FIRSTOFFDAY,@SECONDOFFTYPE,@HALFDAYSHIFT,@SECONDOFFDAY,@PERMISLATEARRIVAL,@PERMISEARLYDEPRT,@ISAUTOSHIFT,@ISOUTWORK,@MAXDAYMIN,@ISOS,@AUTH_SHIFTS,@TIME,@SHORT,@HALF,@ISHALFDAY,@ISSHORT,@TWO,@isReleaver,@isWorker,@isFlexi,@SSN,@MIS,@IsCOF,@HLFAfter,@HLFBefore,@ResignedAfter,@EnableAutoResign,@S_END,@S_OUT,@AUTOSHIFT_LOW,@AUTOSHIFT_UP,@ISPRESENTONWOPRESENT,@ISPRESENTONHLDPRESENT,@NightShiftFourPunch,@ISAUTOABSENT,@ISAWA,@ISWA,@ISAW,@ISPREWO,@ISOTOUTMINUSSHIFTENDTIME,@ISOTWRKGHRSMINUSSHIFTHRS,@ISOTEARLYCOMEPLUSLATEDEP,@DEDUCTHOLIDAYOT,@DEDUCTWOOT,@ISOTEARLYCOMING,@OTMinus,@OTROUND,@OTEARLYDUR,@OTLATECOMINGDUR,@OTRESTRICTENDDUR,@DUPLICATECHECKMIN,@PREWO,@CompanyCode)     
End
go
--Update Record Group Policy
Drop procedure tblEmployeeGroupPolicy_Upd
go
Create procedure tblEmployeeGroupPolicy_Upd      
(   
	@GroupID varchar(50),@GroupName varchar(50),@SHIFT char(3),@SHIFTTYPE char(1),@SHIFTPATTERN char(11),@SHIFTREMAINDAYS int,@INONLY char(1),@ISPUNCHALL char(1),@ISTIMELOSSALLOWED char(1),@ALTERNATE_OFF_DAYS char(10),@CDAYS float(8),@ISROUNDTHECLOCKWORK char(1),@ISOT char(1),@OTRATE char(6),@FIRSTOFFDAY char(3),@SECONDOFFTYPE char(1),@HALFDAYSHIFT char(3),@SECONDOFFDAY char(3),@PERMISLATEARRIVAL int,@PERMISEARLYDEPRT int,@ISAUTOSHIFT char(1),@ISOUTWORK char(1),@MAXDAYMIN float(8),@ISOS char(1),@AUTH_SHIFTS char(50),@TIME int,@SHORT int,@HALF int,@ISHALFDAY char(1),@ISSHORT char(1),@TWO char(1),@isReleaver char(1),@isWorker char(1),@isFlexi char(1),@SSN varchar(100),@MIS char(1),@IsCOF char(1),@HLFAfter int,@HLFBefore int,@ResignedAfter int,@EnableAutoResign char(1),@S_END datetime,@S_OUT datetime,@AUTOSHIFT_LOW int,@AUTOSHIFT_UP int,@ISPRESENTONWOPRESENT char(1),@ISPRESENTONHLDPRESENT char(1),@NightShiftFourPunch char(1),@ISAUTOABSENT char(1),@ISAWA char(1),@ISWA char(1),@ISAW char(1),@ISPREWO char(1),@ISOTOUTMINUSSHIFTENDTIME char(1),@ISOTWRKGHRSMINUSSHIFTHRS char(1),@ISOTEARLYCOMEPLUSLATEDEP char(1),@DEDUCTHOLIDAYOT int,@DEDUCTWOOT int,@ISOTEARLYCOMING char(1),@OTMinus char(1),@OTROUND char(1),@OTEARLYDUR int,@OTLATECOMINGDUR int,@OTRESTRICTENDDUR int,@DUPLICATECHECKMIN int,@PREWO int,@CompanyCode varchar(4)
)
as
begin      
   Update tblEmployeeGroupPolicy  set
 
GroupName=@GroupName,
SHIFT=@SHIFT,
SHIFTTYPE=@SHIFTTYPE,
SHIFTPATTERN=@SHIFTPATTERN,
SHIFTREMAINDAYS=@SHIFTREMAINDAYS,
INONLY=@INONLY,
ISPUNCHALL=@ISPUNCHALL,
ISTIMELOSSALLOWED=@ISTIMELOSSALLOWED,
ALTERNATE_OFF_DAYS=@ALTERNATE_OFF_DAYS,
CDAYS=@CDAYS,
ISROUNDTHECLOCKWORK=@ISROUNDTHECLOCKWORK,
ISOT=@ISOT,
OTRATE=@OTRATE,
FIRSTOFFDAY=@FIRSTOFFDAY,
SECONDOFFTYPE=@SECONDOFFTYPE,
HALFDAYSHIFT=@HALFDAYSHIFT,
SECONDOFFDAY=@SECONDOFFDAY,
PERMISLATEARRIVAL=@PERMISLATEARRIVAL,
PERMISEARLYDEPRT=@PERMISEARLYDEPRT,
ISAUTOSHIFT=@ISAUTOSHIFT,
ISOUTWORK=@ISOUTWORK,
MAXDAYMIN=@MAXDAYMIN,
ISOS=@ISOS,
AUTH_SHIFTS=@AUTH_SHIFTS,
TIME=@TIME,
SHORT=@SHORT,
HALF=@HALF,
ISHALFDAY=@ISHALFDAY,
ISSHORT=@ISSHORT,
TWO=@TWO,
isReleaver=@isReleaver,
isWorker=@isWorker,
isFlexi=@isFlexi,
SSN=@SSN,
MIS=@MIS,
IsCOF=@IsCOF,
HLFAfter=@HLFAfter,
HLFBefore=@HLFBefore,
ResignedAfter=@ResignedAfter,
EnableAutoResign=@EnableAutoResign,
S_END=@S_END,
S_OUT=@S_OUT,
AUTOSHIFT_LOW=@AUTOSHIFT_LOW,
AUTOSHIFT_UP=@AUTOSHIFT_UP,
ISPRESENTONWOPRESENT=@ISPRESENTONWOPRESENT,
ISPRESENTONHLDPRESENT=@ISPRESENTONHLDPRESENT,
NightShiftFourPunch=@NightShiftFourPunch,
ISAUTOABSENT=@ISAUTOABSENT,
ISAWA=@ISAWA,
ISWA=@ISWA,
ISAW=@ISAW,
ISPREWO=@ISPREWO,
ISOTOUTMINUSSHIFTENDTIME=@ISOTOUTMINUSSHIFTENDTIME,
ISOTWRKGHRSMINUSSHIFTHRS=@ISOTWRKGHRSMINUSSHIFTHRS,
ISOTEARLYCOMEPLUSLATEDEP=@ISOTEARLYCOMEPLUSLATEDEP,
DEDUCTHOLIDAYOT=@DEDUCTHOLIDAYOT,
DEDUCTWOOT=@DEDUCTWOOT,
ISOTEARLYCOMING=@ISOTEARLYCOMING,
OTMinus=@OTMinus,
OTROUND=@OTROUND,
OTEARLYDUR=@OTEARLYDUR,
OTLATECOMINGDUR=@OTLATECOMINGDUR,
OTRESTRICTENDDUR=@OTRESTRICTENDDUR,
DUPLICATECHECKMIN=@DUPLICATECHECKMIN,
PREWO=@PREWO,
CompanyCode=@CompanyCode
   where GroupID=@GroupID and CompanyCode=@CompanyCode
End
go
--Delete Record Shift Master
Drop procedure tblEmployeeGroupPolicy_Del
go
Create procedure tblEmployeeGroupPolicy_Del     
(      
   @CompanyCode char(4),
   @SHIFT char(4)  
)      
as       
begin      
   Delete from tblEmployeeGroupPolicy where SHIFT=@SHIFT and CompanyCode=@CompanyCode     
End
go
--Get all records Shift Master
Drop procedure tblEmployeeGroupPolicy_AllRec
go
Create procedure tblEmployeeGroupPolicy_AllRec   
as    
Begin    
--select ltrim(rtrim(CompanyCode)) as CompanyCode, ltrim(rtrim(SHIFT)) as SHIFT,STARTTIME,ENDTIME,LUNCHTIME,LUNCHDURATION,LUNCHENDTIME,OTSTARTAFTER,OTDEDUCTHRS,LUNCHDEDUCTION,SHIFTPOSITION,SHIFTDURATION,OTDEDUCTAFTER,CompanyCode,ShiftDiscription  FROM tblEmployeeGroupPolicy 

select GroupID,GroupName,SHIFT,SHIFTTYPE,SHIFTPATTERN,SHIFTREMAINDAYS,LASTSHIFTPERFORMED,INONLY,ISPUNCHALL,ISTIMELOSSALLOWED,ALTERNATE_OFF_DAYS,CDAYS,ISROUNDTHECLOCKWORK,ISOT,OTRATE,FIRSTOFFDAY,SECONDOFFTYPE,HALFDAYSHIFT,SECONDOFFDAY,PERMISLATEARRIVAL,PERMISEARLYDEPRT,ISAUTOSHIFT,ISOUTWORK,MAXDAYMIN,ISOS,AUTH_SHIFTS,TIME,SHORT,HALF,ISHALFDAY,ISSHORT,TWO,isReleaver,isWorker,isFlexi,SSN,MIS,IsCOF,HLFAfter,HLFBefore,ResignedAfter,EnableAutoResign,S_END,S_OUT,AUTOSHIFT_LOW,AUTOSHIFT_UP,ISPRESENTONWOPRESENT,ISPRESENTONHLDPRESENT,NightShiftFourPunch,ISAUTOABSENT,ISAWA,ISWA,ISAW,ISPREWO,ISOTOUTMINUSSHIFTENDTIME,ISOTWRKGHRSMINUSSHIFTHRS,ISOTEARLYCOMEPLUSLATEDEP,DEDUCTHOLIDAYOT,DEDUCTWOOT,ISOTEARLYCOMING,OTMinus,OTROUND,OTEARLYDUR,OTLATECOMINGDUR,OTRESTRICTENDDUR,DUPLICATECHECKMIN,PREWO,CompanyCode from tblEmployeeGroupPolicy
End
go
--Get all records Shift
Drop procedure tblEmployeeGroupPolicy_GetData
go
Create procedure tblEmployeeGroupPolicy_GetData 
(
@GroupID varchar(4)
--@CompanyCode varchar(4)
)  
as    
Begin    
   select ltrim(rtrim(CompanyCode)) as CompanyCode, ltrim(rtrim(GroupID)) as GroupID,GroupName,SHIFT,SHIFTTYPE,SHIFTPATTERN,SHIFTREMAINDAYS,LASTSHIFTPERFORMED,INONLY,ISPUNCHALL,ISTIMELOSSALLOWED,ALTERNATE_OFF_DAYS,CDAYS,ISROUNDTHECLOCKWORK,ISOT,OTRATE,FIRSTOFFDAY,SECONDOFFTYPE,HALFDAYSHIFT,SECONDOFFDAY,PERMISLATEARRIVAL,PERMISEARLYDEPRT,ISAUTOSHIFT,ISOUTWORK,MAXDAYMIN,ISOS,AUTH_SHIFTS,TIME,SHORT,HALF,ISHALFDAY,ISSHORT,TWO,isReleaver,isWorker,isFlexi,SSN,MIS,IsCOF,HLFAfter,HLFBefore,ResignedAfter,EnableAutoResign,convert(varchar(5),S_END,108)S_END,convert(varchar(5),S_OUT,108)S_OUT,AUTOSHIFT_LOW,AUTOSHIFT_UP,ISPRESENTONWOPRESENT,ISPRESENTONHLDPRESENT,NightShiftFourPunch,ISAUTOABSENT,ISAWA,ISWA,ISAW,ISPREWO,ISOTOUTMINUSSHIFTENDTIME,ISOTWRKGHRSMINUSSHIFTHRS,ISOTEARLYCOMEPLUSLATEDEP,DEDUCTHOLIDAYOT,DEDUCTWOOT,ISOTEARLYCOMING,OTMinus,OTROUND,OTEARLYDUR,OTLATECOMINGDUR,OTRESTRICTENDDUR,DUPLICATECHECKMIN,PREWO,CompanyCode  FROM tblEmployeeGroupPolicy WHERE GroupID=@GroupID --and CompanyCode= @CompanyCode  
End
GO
--CheckIsCompanyCode Company Master
drop procedure tblEmployeeGroupPolicy_CheckIsCode
GO
CREATE procedure tblEmployeeGroupPolicy_CheckIsCode
(
@Shift varchar(4),
@CompanyCode varchar(4)
)  
as      
begin
SELECT count(SHIFT) cnt  FROM tblEmployeeGroupPolicy where SHIFT=@SHIFT and CompanyCode=@CompanyCode  
end
GO
