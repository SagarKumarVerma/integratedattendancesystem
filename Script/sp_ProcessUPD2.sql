drop PROCEDURE [dbo].[ProcessUPD2]
go
create PROCEDURE [dbo].[ProcessUPD2]
 @SetupId VARCHAR(50),
 @ShiftAttended VARCHAR(3),
 @CompanyCode VARCHAR(10),
 @DepartmentCode VARCHAR(10),
 
 @SSN VARCHAR(100),
 @DateOfOffice DATETIME,
 @LeaveAmount FLOAT,
 @LeaveCode CHAR(3),
 @LeaveType CHAR(1),
 @LeaveType1 CHAR(1),
 @LeaveType2 CHAR(1),
 @FirstHalfLeavecode CHAR(3),
 @SecondHalfLeavecode CHAR(3),
 @MIn1 DATETIME,
 @MIn2 DATETIME,
 @MOut1 DATETIME,
 @MOut2 DATETIME,
 @IsOS Char(1),
 @IsOT Char(1),
 @InOnly Char(1),
 @IsPunchAll Char(1),
 @PermisLateArrival INT,
 @Time INT,
 @PermisEarlyDepart INT,
 @IsHalfDay CHAR(1),
 @Half INT,
 @IsShort CHAR(1), 
 @Short INT,
 @IsTimeLossAllowed CHAR(1),
 @OTRate FLOAT,
 @MIn1Manual CHAR(1),
 @MIn2Manual CHAR(1),
 @MOUT1Manual CHAR(1),
 @MOUT2Manual CHAR(1),
 @MIS CHAR(1),
 @ISOTONOFF CHAR(1)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @TempMUStatus CHAR(6)
	DECLARE @TempMUHoursworked INT
	DECLARE @TempMUExcLunchHours INT
	DECLARE @TempMUOtDuration INT
	DECLARE @TempMUOSDuration INT
	DECLARE @TempMUOtAmount FLOAT
	DECLARE @TempMUEarlyArrival INT
	DECLARE @TempMUEarlyDeparture INT
	DECLARE @TempMULateArrival INT
	DECLARE @TempMULunchEarlyDeparture INT
	DECLARE @TempMULunchLateArrival INT
	DECLARE @TempMUTotalLossHRS INT
	DECLARE @TempMULeaveValue FLOAT
	DECLARE @TempMUPresentValue FLOAT
	DECLARE @TempMUAbsentValue FLOAT
	DECLARE @TempMUHolidayValue FLOAT
	DECLARE @TempMUWoValue FLOAT
	DECLARE @TempMUOutWorkDuration INT
	DECLARE @TempMUFlag CHAR(4)

	DECLARE @TempMLate INT
	DECLARE @TempMEarly INT
	DECLARE @TempMLate1 INT
	DECLARE @TempMEarly1 INT
	DECLARE @TempMShiftTime INT
	DECLARE @TempMAbsHours INT
	
	DECLARE @TempMOTInHours FLOAT
	DECLARE @TempMOTFactor FLOAT
	DECLARE @TempMLDed INT
	DECLARE @TempMLDur INT
	DECLARE @TempMLunchDur INT
	DECLARE @TempMOT INT
	DECLARE @TempMMinutes INT

	DECLARE @TempMOtStartAfter INT
	DECLARE @TempMOtDeductAfter INT
	DECLARE @TempMOtDeductHours INT

	-- Required for Processing
	DECLARE @TempMUShiftStartTime DATETIME
	DECLARE @TempMUShiftEndTime DATETIME

	DECLARE @TempShiftStartTime DATETIME
	DECLARE @TempShiftEndTime DATETIME
	DECLARE @TempShiftName VARCHAR(3)

	DECLARE @TempStartTime TIME
	DECLARE @TempEndTime TIME
	DECLARE @TempDate DATE
	DECLARE @TempShiftStartDateTime DATETIME
	DECLARE @TempShiftEndDateTime DATETIME
	DECLARE @Minutes INT

	DECLARE @TempLunchStartTime DATETIME
	DECLARE @TempLunchStartDateTime DATETIME
	DECLARE @TempLunchEndTime DATETIME
	DECLARE @TempLunchEndDateTime DATETIME
	DECLARE @TempLunchDeduction INT
	DECLARE @TempShiftPosition CHAR(7)

	-- TBLSetup value 
	DECLARE @TempGIsPresentOnWOPresent CHAR(1)
	DECLARE @TempGIsPresentOnHLDPresent CHAR(1)	              
	DECLARE @TempGWoOT INT   -- [DEDUCTWOOT]
	DECLARE @TempGHldOT INT  -- [DEDUCTHOLIDAYOT]
	DECLARE @TempGISOTOUTMINUSSHIFTENDTIME CHAR(1)
	DECLARE @TempGISOTWRKGHRSMINUSSHIFTHRS CHAR (1)
	DECLARE @TempGOTFormulae INT -- [ISOTOUTMINUSSHIFTENDTIME]
	DECLARE @TempGOTEarly INT  -- [OTEARLYDUR]
	DECLARE @TempGOTLate INT -- [OTLATECOMINGDUR]
	DECLARE @TempGOTEnd INT -- [OTRESTRICTENDDUR]
	DECLARE @TempGIsOTMinus CHAR(1) -- [OwMinus] TODO : Check with Virendra Sir
	DECLARE @TempGOTRound CHAr(1)  -- [OTROUND]
	
	DECLARE @IsHoliday BIT
	DECLARE @HolidayCount INT
		
	SET @TempMUStatus = 'A'
	SET @TempMUHoursworked = 0
	SET @TempMUExcLunchHours = 0
	SET @TempMUOtDuration = 0
	SET @TempMUOSDuration = 0
	SET @TempMUOtAmount = 0
	SET @TempMUEarlyArrival = 0
	SET @TempMUEarlyDeparture = 0
	SET @TempMULateArrival = 0
	SET @TempMULunchEarlyDeparture = 0
	SET @TempMULunchLateArrival = 0
	SET @TempMUTotalLossHRS = 0
	SET @TempMULeaveValue = 0
	SET @TempMUPresentValue = 0
	SET @TempMUAbsentValue = 0
	SET @TempMUHolidayValue = 0
	SET @TempMUWoValue = 0
	SET @TempMUOutWorkDuration = 0
	SET @TempMUFlag = ''

	--For Multipal tbltimeregister 
	DECLARE @strTempMDate char(10)
	DECLARE @TableName  VARCHAR(max)
	declare @SQL as nvarchar(max)
	declare @SQL1 as nvarchar(max)
	declare @SQLUpdateShift as nvarchar(max)
	declare @SQLUpdatePunch as nvarchar(max)
	declare @SQLUpdateData as nvarchar(max)


	DECLARE @strMIn1  VARCHAR(max)
	DECLARE @strMIn2  VARCHAR(max)
	DECLARE @strMOut1  VARCHAR(max)
	DECLARE @strMOut2  VARCHAR(max)

	DECLARE @strTempShiftStartDateTime  VARCHAR(max)
	DECLARE @strTempShiftEndDateTime  VARCHAR(max)
	DECLARE @strTempLunchStartDateTime  VARCHAR(max)
	DECLARE @strTempLunchEndDateTime  VARCHAR(max)


	DECLARE @strTempMUHoursworked  VARCHAR(max)
	DECLARE @strTempMUExcLunchHours  VARCHAR(max)
	DECLARE @strTempMUOtDuration  VARCHAR(max)
	DECLARE @strTempMUOSDuration  VARCHAR(max)
	DECLARE @strTempMUOtAmount  VARCHAR(max)
	DECLARE @strTempMUEarlyArrival  VARCHAR(max)
	DECLARE @strTempMUEarlyDeparture  VARCHAR(max)
	DECLARE @strTempMULateArrival   VARCHAR(max)
	DECLARE @strTempMULunchEarlyDeparture  VARCHAR(max)
	DECLARE @strTempMULunchLateArrival  VARCHAR(max)
	DECLARE @strTempMUTotalLossHRS   VARCHAR(max)
	DECLARE @strTempMUStatus  VARCHAR(max)
	DECLARE @strShiftAttended  VARCHAR(max)
	DECLARE @strMIn1Manual  VARCHAR(max)
	DECLARE @strMIn2Manual  VARCHAR(max)
	DECLARE @strMOUT1Manual  VARCHAR(max)
	DECLARE @strMOUT2Manual  VARCHAR(max)
	DECLARE @strTempMULeaveValue  VARCHAR(max)
	DECLARE @strTempMUPresentValue  VARCHAR(max)
	DECLARE @strTempMUAbsentValue  VARCHAR(max)
	DECLARE @strTempMUHolidayValue  VARCHAR(max)
	DECLARE @strTempMUWoValue  VARCHAR(max)
	DECLARE @strTempMUOutWorkDuration  VARCHAR(max)
	
	SELECT 
		@TempGIsPresentOnWOPresent = ISPRESENTONWOPRESENT,
		@TempGIsPresentOnHLDPresent = ISPRESENTONHLDPRESENT,
		@TempGWoOT = DEDUCTWOOT,
		@TempGHldOT = DEDUCTHOLIDAYOT,
		@TempGISOTOUTMINUSSHIFTENDTIME = ISOTOUTMINUSSHIFTENDTIME,
		@TempGISOTWRKGHRSMINUSSHIFTHRS = ISOTWRKGHRSMINUSSHIFTHRS,
		@TempGOTLate = OTLATECOMINGDUR,
		@TempGOTEarly = OTEARLYDUR,
		@TempGOTEnd = OTRESTRICTENDDUR,
		@TempGOTRound = OTROUND,
		@TempGIsOTMinus = 'N'
    FROM TBLEMPLOYEESHIFTMASTER (nolock)
	WHERE SSN = @SetupId 

	IF @TempGISOTOUTMINUSSHIFTENDTIME = 'Y'
	BEGIN
		SET @TempGOTFormulae = 1
	END
	ELSE IF @TempGISOTWRKGHRSMINUSSHIFTHRS = 'Y'
	BEGIN
		SET @TempGOTFormulae = 2
	END
	ELSE
	BEGIN
		SET @TempGOTFormulae = 3
	END

	-- TODO : Check with Virendra Sir if it is needed. 
	IF @TempGOTFormulae != 3
	BEGIN
		SET @TempGOTLate = 0
		SET @TempGOTEarly = 0
		SET @TempGOTEnd = 0
	END

	SELECT @HolidayCount = COUNT(*) FROM Holiday(nolock) 
	WHERE DEPARTMENTCODE = @DepartmentCode 
	AND CompanyCode = @CompanyCode 
	AND  CAST(CONVERT(varchar(10), HDate, 101) AS DATE) = CAST(CONVERT(varchar(10), @DateOfOffice, 101) AS DATE)  
		
	IF @HolidayCount > 0
	BEGIN 
		SET @IsHoliday = 1
	END
	ELSE
	BEGIN 
		SET @IsHoliday = 0
	END

	-- SET Shift StartTime 
	IF @ShiftAttended != 'OFF' AND @ShiftAttended!= 'IGN'
	BEGIN
		SET @TempShiftName=''
		SELECT 
			@TempShiftName=[SHIFT],
			@TempShiftStartTime = StartTime,
			@TempShiftEndTime = ENDTIME,
			@TempLunchStartTime = LUNCHTIME,
			@TempLunchEndTime = LUNCHENDTIME,
			@TempLunchDeduction = LUNCHDEDUCTION,
			@TempShiftPosition = SHIFTPOSITION,
			@TempMOtStartAfter = OTSTARTAFTER,
			@TempMOtDeductAfter = OTDEDUCTAFTER,
			@TempMOtDeductHours = OTDEDUCTHRS
		FROM [dbo].[tblShiftMaster] (nolock)
		WHERE [SHIFT]=ltrim(rtrim(@ShiftAttended))
		AND CompanyCode = @CompanyCode


		IF @TempShiftName = '' 
		BEGIN
		-- Log message that shift is not a valid shift for 
			RETURN 
		END

		SET @TempDate = CAST(@DateOfOffice AS date)

		SET @TempShiftStartDateTime = CAST(@TempDate AS DATETIME)
		SET @TempShiftEndDateTime = CAST(@TempDate AS DATETIME)

		SET @Minutes = DATEDIFF(MINUTE, DATEADD(DAY, DATEDIFF(DAY, 0, @TempShiftStartTime), 0), @TempShiftStartTime)
		SET @TempShiftStartDateTime = DATEADD(HH, @Minutes/60, @TempShiftStartDateTime)
		SET @TempShiftStartDateTime = DATEADD(MINUTE, @Minutes%60,@TempShiftStartDateTime)

		SET @TempShiftEndDateTime = CAST(@TempDate AS DATETIME)

		SET @Minutes = DATEDIFF(MINUTE, DATEADD(DAY, DATEDIFF(DAY, 0, @TempShiftEndTime), 0), @TempShiftEndTime)
		SET @TempShiftEndDateTime = DATEADD(HH, @Minutes/60, @TempShiftEndDateTime)
		SET @TempShiftEndDateTime = DATEADD(MINUTE, @Minutes%60,@TempShiftEndDateTime)

		IF DATEDIFF(N, @TempShiftStartDateTime, @TempShiftEndDateTime) < 0
		BEGIN 
		    SET @TempDate = CAST (@TempShiftEndDateTime AS DATE)
			SET @TempDate = DATEADD(D, 1, @TempDate)
			
			SET @TempShiftEndDateTime = CAST(@TempDate AS DATETIME)
			SET @TempShiftEndDateTime = DATEADD(HH, @Minutes/60, @TempShiftEndDateTime)
			SET @TempShiftEndDateTime = DATEADD(MINUTE, @Minutes%60,@TempShiftEndDateTime)
		END

		SET @TempDate = CAST(@DateOfOffice AS date)
		SET @Minutes = DATEDIFF(MINUTE, DATEADD(DAY, DATEDIFF(DAY, 0, @TempLunchStartTime), 0), @TempLunchStartTime) 

		SET @TempLunchStartDateTime = CAST(@TempDate AS DATETIME)
		SET @TempLunchStartDateTime = DATEADD(HH, @Minutes/60, @TempLunchStartDateTime)
		SET @TempLunchStartDateTime = DATEADD(MINUTE, @Minutes%60,@TempLunchStartDateTime)

		IF DATEDIFF(N, @TempShiftStartDateTime, @TempLunchStartDateTime) < 0
		BEGIN
			SET @TempDate = CAST (@TempLunchStartDateTime AS DATE)
			SET @TempDate = DATEADD(D, 1, @TempDate)
			
			SET @TempLunchStartDateTime = CAST(@TempDate AS DATETIME)
			SET @TempLunchStartDateTime = DATEADD(HH, @Minutes/60, @TempLunchStartDateTime)
			SET @TempLunchStartDateTime = DATEADD(MINUTE, @Minutes%60,@TempLunchStartDateTime)
		END


		SET @TempDate = CAST(@DateOfOffice AS date)
		SET @Minutes = DATEDIFF(MINUTE, DATEADD(DAY, DATEDIFF(DAY, 0, @TempLunchEndTime), 0), @TempLunchEndTime) 

		SET @TempLunchEndDateTime = CAST(@TempDate AS DATETIME)
		SET @TempLunchEndDateTime = DATEADD(HH, @Minutes/60, @TempLunchEndDateTime)
		SET @TempLunchEndDateTime = DATEADD(MINUTE, @Minutes%60,@TempLunchEndDateTime)

		IF DATEDIFF(N, @TempShiftStartDateTime, @TempLunchEndDateTime) < 0
		BEGIN
			SET @TempDate = CAST (@TempLunchEndDateTime AS DATE)
			SET @TempDate = DATEADD(D, 1, @TempDate)
			
			SET @TempLunchEndDateTime = CAST(@TempDate AS DATETIME)
			SET @TempLunchEndDateTime = DATEADD(HH, @Minutes/60, @TempLunchEndDateTime)
			SET @TempLunchEndDateTime = DATEADD(MINUTE, @Minutes%60,@TempLunchEndDateTime)
		END

		SET @TempMLDur = DATEDIFF(N, @TempLunchStartDateTime, @TempLunchEndDateTime)
		SET @TempMLunchDur = @TempMLDur

		SET @TempMLDed = @TempLunchDeduction
		SET @TempMShiftTime = DATEDIFF(N, @TempShiftStartDateTime, @TempShiftEndDateTime) - @TempMLDur
	END


	IF @LeaveAmount > 0
	BEGIN 
		IF @LeaveAmount = 0.25 
		BEGIN 
			SET @TempMUStatus = 'Q_'
		END
		ELSE IF @LeaveAmount = 0.5 
		BEGIN 
			SET @TempMUStatus = 'H_'
		END
		ELSE IF @LeaveAmount = 0.75 
		BEGIN 
			SET @TempMUStatus = 'T_'
		END
		ELSE
		BEGIN
			SET @TempMUStatus = ''
		END

		SET @TempMUStatus = LTRIM(RTRIM(@TempMUStatus)) + @LeaveCode

		IF ISDATE(@MIn1) = 1
		BEGIN
			IF @ShiftAttended != 'IGN' AND @ShiftAttended != 'OFF' AND @IsHoliday=0
			BEGIN
				SET @TempMEarly = DATEDIFF(N, @MIn1, @TempShiftStartDateTime)
				IF @TempMEarly < 0
				BEGIN
					SET @TempMEarly = 0
				END
			END

			IF ISDATE(@MOut2) = 1
			BEGIN
				SET @TempMUHoursworked = DATEDIFF(N, @MIn1, @MOut2)
				--TODO : Check with Virendra sir for condition
				IF ISDATE(@MIn2) = 1
				BEGIN
					SET @TempMLDur = DATEDIFF(N, @MOut1, @MIn2)
					IF @TempMLDur < @TempMLDed 
					BEGIN
						SET @TempMLDur = @TempMLDed
					ENd
					SET @TempMUHoursworked = @TempMUHoursworked - @TempMLDur
				END 
				ELSE
				BEGIN
					SET @TempMUHoursworked = @TempMUHoursworked - @TempMLDed
				END
			END

			IF @LeaveType = 'P'
			BEGIN
				SET @TempMUPresentValue = 1
			END
			ELSE IF @LeaveType = 'L'
			BEGIN 
				SET @TempMULeaveValue = 0 + @LeaveAmount
				SET @TempMUPresentValue = 1 - @LeaveAmount
			END
			ELSE 
			BEGIN 
				SET @TempMUAbsentValue = 0 + @LeaveAmount
				SET @TempMUPresentValue = 1 - @LeaveAmount
			END
		END
		ELSE
		BEGIN
		    SET @LeaveType1 = ISNULL(@LeaveType1,'')
			SET @LeaveType2 = ISNULL(@LeaveType2,'')
			IF @LeaveType1 != '' AND @LeaveType2 != ''
			BEGIN
				SET @TempMUStatus = LTRIM(RTRIM(@FirstHalfLeavecode)) + LTRIM(RTRIM(@SecondHalfLeavecode))

				IF (@LeaveType1 = 'P' AND @LeaveType2 = 'A') OR (@LeaveType1 = 'A' AND @LeaveType2 = 'P')
				BEGIN
					SET @TempMUPresentValue = 0.5
					SET @TempMULeaveValue = 0
					SET @TempMUAbsentValue = 0.5
				END
				IF (@LeaveType1 = 'L' AND @LeaveType2 = 'P') OR (@LeaveType1 = 'P' AND @LeaveType2 = 'L')
				BEGIN
					SET @TempMUPresentValue = 0.5
					SET @TempMULeaveValue = 0.5
					SET @TempMUAbsentValue = 0
				END
				IF (@LeaveType1 = 'L' AND @LeaveType2 = 'A') OR (@LeaveType1 = 'A' AND @LeaveType2 = 'L')
				BEGIN
					SET @TempMUPresentValue = 0
					SET @TempMULeaveValue = 0.5
					SET @TempMUAbsentValue = 0.5
				END
				IF @LeaveType1 = 'L' AND @LeaveType2 = 'L'
				BEGIN
					SET @TempMUPresentValue = 0
					SET @TempMULeaveValue = 1
					SET @TempMUAbsentValue = 0					
				END
				IF @LeaveType1 = 'A' AND @LeaveType2 = 'A'
				BEGIN
					SET @TempMUPresentValue = 0
					SET @TempMULeaveValue = 0
					SET @TempMUAbsentValue = 1					
				END
				IF @LeaveType1 = 'P' AND @LeaveType2 = 'P'
				BEGIN
					SET @TempMUPresentValue = 1
					SET @TempMULeaveValue = 0
					SET @TempMUAbsentValue = 0					
				END
			END
			ELSE
			BEGIN
				IF @LeaveType = 'P'
				BEGIN
					SET @TempMUPresentValue = @LeaveAmount
				END
				ELSE IF @LeaveType = 'L'
				BEGIN
					SET @TempMULeaveValue = @LeaveAmount
				END
				ELSE IF @LeaveType = 'A' 
				BEGIN
					SET @TempMUAbsentValue = @LeaveAmount
				END

				IF @ShiftAttended = 'OFF'
				BEGIN 
					SET @TempMUWoValue = 1 - @LeaveAmount
				END
				ELSE IF @IsHoliday = 1
				BEGIN
					SET @TempMUHolidayValue = 1 - @LeaveAmount
				END
				ELSE IF SUBSTRING(@TempShiftPosition,1,1) = 'H'
				BEGIN
					SET @TempMUWoValue = 0.5
				END
				ELSE
				BEGIN
				    -- TODO : Check Bang Operator  mTime!LEAVETYPE <> "A" : Line NUmber 2188 in VB code in Module 1 
					IF @LeaveType != 'A' 
					BEGIN
						SET @TempMUAbsentValue = 1 - @LeaveAmount 
					END
				END
			END
		END
		GOTO Lastl
	END

	IF @ShiftAttended = 'OFF' AND @IsHoliday = 1
	BEGIN
		SET @TempMUHolidayValue = 1
		SET @TempMUStatus = 'WOH'

		IF ISDATE(@MIn1) = 1
		BEGIN
			SET @TempMUStatus = 'PWH'
			IF @TempGIsPresentOnWOPresent = 'Y' OR @TempGIsPresentOnHLDPresent = 'Y'
			BEGIN
				SET @TempMUPresentValue = 1
				SEt @TempMUHolidayValue = 0
			END

			IF ISDATE(@Mout2) = 1
			BEGIN
				SET @TempMUHoursworked = DATEDIFF(N, @MIn1, @MOut2)
				IF ISDATE(@MIn2) = 1
				BEGIN
					SET @TempMLDur = DATEDIFF(N, @MOut1, @MIn2)
					SET @TempMUHoursworked = @TempMUHoursworked - @TempMLDur
				END

				IF @IsOS = 'Y'
				BEGIN
					SET @TempMUOSDuration = @TempMUHoursworked
				END

				IF @IsOT = 'Y' 
				BEGIN
					SET @TempMUOtDuration = @TempMUHoursworked
				END


			END			
		END
		IF @ISOTONOFF='C'
		BEGIN
			SET @TempMUOtDuration =0
		END
			
		GOTO Lastl
	END

	IF @ShiftAttended = 'OFF'
	BEGIN
		SET @TempMUWoValue = 1
		SET @TempMUStatus = 'WO'

		IF ISDATE(@MIn1) = 1
		BEGIN
			SET @TempMUStatus = 'POW'
			IF @TempGIsPresentOnWOPresent = 'Y' 
			BEGIN
				SET @TempMUWoValue = 0
				SET @TempMUPresentValue = 1
			END

			IF ISDATE(@MOut2) = 1
			BEGIN
				SET @TempMUHoursworked = DATEDIFF(N, @MIn1, @MOut2)
				IF ISDATE(@MIn2) = 1
				BEGIN
					SET @TempMLDur = DATEDIFF(N, @MOut1, @MIn2)
					SET @TempMUHoursworked = @TempMUHoursworked - @TempMLDur
				END

				IF @IsOS = 'Y' 
				BEGIN
					SET @TempMUOSDuration = @TempMUHoursworked
				END

				IF @IsOT = 'Y'
				BEGIN
					SET @TempMUOtDuration = @TempMUHoursworked - @TempGWoOT
				END

			END
			IF @ISOTONOFF='C'
		BEGIN
			SET @TempMUOtDuration =0
		END
		END
		GOTO Lastl
	END

	IF @IsHoliday = 1
	BEGIN
		SET @TempMUHolidayValue = 1
		SET @TempMUStatus = 'HLD'

		IF ISDATE(@MIn1) = 1
		BEGIN
			SET @TempMUStatus = 'POH'
			IF @TempGIsPresentOnHLDPresent =  'Y'
			BEGIN
				SET @TempMUHolidayValue = 0
				SET @TempMUPresentValue = 1
			END
			IF ISDate(@MOut2) = 1
			BEGIN
				SET @TempMUHoursworked  = DATEDIFF(N, @MIn1, @MOut2)
				IF ISDATE(@MIn2) = 1
				BEGIN
					SET @TempMLDur = DATEDIFF(N, @MOut1, @MIn2)
					SET @TempMUHoursworked = @TempMUHoursworked - @TempMLDur
				END

				IF @IsOS = 'Y' 
				BEGIN
					SET @TempMUOSDuration = @TempMUHoursworked
				END

				IF @IsOT = 'Y'
				BEGIN
					SET @TempMUOtDuration = @TempMUHoursworked - @TempGHldOT
				END

			END
		END
		GOTO Lastl
	END
	--G4S 

	--IF ISDATE(@MIn1) = 1 AND @InOnly != 'N' 
	--BEGIN
	--    -- TODO : Check with Virendra sir. VB code.Line Number 2285, Module1.vb
	--	IF @InOnly != 'O' AND ISDATE(@Mout2) != 1
	--	BEGIN
	--	--	SET @MOut2 = @TempShiftEndDateTime
	--	END
	--END

	--IF @IsPunchAll = 'N' AND ISDATE(@MIn1) <> 1
	IF @IsPunchAll = 'N' AND ISDATE(@MIn1) = 0
	BEGIN
		SET @MIn1 = @TempShiftStartDateTime
		SET @MOut2 = @TempShiftEndDateTime
	END

	IF @IsPunchAll = 'N' AND ISDATE(@MIn1) = 1 AND ISDATE(@MOut2) != 1
	BEGIN
		IF DATEDIFF(N, @MIn1, @TempShiftEndDateTime) > 0
		BEGIN 
			SET @MOut2 = @TempShiftEndDateTime
		END
		ELSE
		BEGIN
			SET @MOut2 = @MIn1
			SET @MIn1 = @TempShiftStartDateTime
		END
	ENd

	IF (ISDATE(@MIn1) = 1 AND ISDATE(@MOut2) != 1) OR (ISDATE(@MIn1) != 1 AND ISDATE(@MOut2) = 1) 
	BEGIN
		IF @MIS='A'
		BEGIN
			SET @TempMUStatus = 'A'
			SET @TempMUPresentValue = 0
			SET @TempMUAbsentValue=1
		END
		ELSE IF  @MIS='H'
		BEGIN
			SET @TempMUStatus = 'HLF'
			SET @TempMUPresentValue =0.5
			SET @TempMUAbsentValue=0.5
		END
		ELSE
		BEGIN
			SET @TempMUStatus = 'MIS'
			SET @TempMUPresentValue =1
			SET @TempMUAbsentValue=0
		
		END


		IF SUBSTRING(@TempShiftPosition,1,1) = 'H'
		BEGIN
			IF @TempMULeaveValue < 0.5 
			BEGIN
				SET @TempMUPresentValue  = 0.5 - @TempMULeaveValue
			END
			SET @TempMUWoValue = 0.5
		END

		IF @ShiftAttended != 'AGN' 
		BEGIN
			IF ISDATE(@MIn1) = 1
			BEGIN
				SET @TempMEarly = DATEDIFF(N, @MIn1, @TempShiftStartDateTime)
				IF @TempMEarly > 0
				BEGIN
					SET @TempMUEarlyArrival = @TempMEarly
				END
				ELSE IF ABS(@TempMEarly) > @PermisLateArrival
				BEGIN
					SET @TempMULateArrival = ABS(@TempMEarly)
				END
			END
		END
		GOTO Lastl
	END

	IF SUBSTRING(@TempShiftPosition,1,1) = 'H' AND @TempMULeaveValue = 0
	BEGIN
		SET @TempMUWoValue = 0.5
		IF ISDATE(@MIn1) = 1 
		BEGIN 
			SET @TempMUPresentValue = 0.5
		END
		ELSE
		BEGIN
			SET @TempMUAbsentValue = 0.5
		END
	END

	IF SUBSTRING(@TempShiftPosition,1,1) = 'H' AND @TempMULeaveValue > 0
	BEGIN
		SET @TempMUWoValue = 0.5
		IF ISDATE(@MIn1) = 1 AND  @TempMULeaveValue < 0.5
		BEGIN 
			SET @TempMUPresentValue = 0.5 - @TempMULeaveValue
		END
		ELSE
		BEGIN
			IF  @TempMULeaveValue < 0.5
			BEGIN
				SET @TempMUAbsentValue = 0.5 - @TempMULeaveValue
			END			
		END
	END

	IF ISDATE(@MOut2) = 1 AND ISDATE(@MIn1) =1 
	BEGIN
		SET @TempMUHoursworked = DATEDIFF(N, @MIn1, @MOut2)
		IF ISDATE(@MIn2) = 1
		BEGIN
			SET @TempMLDur = DATEDIFF(N, @MOut1, @MIn2)
			IF @TempMLDur < @TempMLDed 
			BEGIN
				SET @TempMLDur = @TempMLDed
			END
			SET @TempMUHoursworked = @TempMUHoursworked - @TempMLDur
		END
		ELSE
		BEGIN
			SET @TempMUHoursworked = @TempMUHoursworked - @TempMLDed
		END

		IF @ShiftAttended != 'IGN'
		BEGIN
			SET @TempMUHoursworked = DATEDIFF(N, @MIn1, @MOut2)
			IF @TempMUHoursworked < @Time
			BEGIN 
				SET @TempMUHoursworked = 0
				SET @TempMUAbsentValue = 1
				GOTO Lastl
			END

			IF ISDATE(@MIn2) = 1 AND ISDATE(@TempLunchStartDateTime) = 1
			BEGIN
				SET @TempMEarly1 = DATEDIFF (N, @MOut1, @TempLunchStartDateTime)
				IF @TempMEarly1 > 0 
				BEGIN 
					SET @TempMULunchEarlyDeparture  = @TempMEarly1
				END

				SET @TempMLate1 = DATEDIFF(N, @TempLunchEndDateTime, @MIn2)
				IF @TempMLate1 > 0
				BEGIN
					SET @TempMULunchLateArrival = @TempMLate1				
				END

				IF @TempMLDur > @TempMLunchDur
				BEGIN
					SET @TempMUExcLunchHours = @TempMLDur - @TempMLunchDur
				END
			END

			IF @IsOS = 'Y' 
			BEGIN
				IF @TempMUStatus = 'HLF'
				BEGIN
					SET @TempMUOSDuration = @TempMUHoursworked - 240
				END
				ELSE
				BEGIN
					SET @TempMUOSDuration = @TempMUHoursworked - @TempMShiftTime
				END

				IF @TempMUOSDuration < 0 
				BEGIN
					SET @TempMUOSDuration = 0
				END
			END

			SET @TempMEarly = DATEDIFF(N, @MIn1, @TempShiftStartDateTime)
			IF @TempMEarly > 0
			BEGIN
				SET @TempMUEarlyArrival = @TempMEarly
			END
			ELSE IF ABS(@TempMEarly) > @PermisLateArrival
			BEGIN
				SET @TempMULateArrival = ABS(@TempMEarly)
			END

			SET @TempMLate = DATEDIFF(N, @TempShiftEndDateTime, @MOut2)
			IF @TempMLate < 0 AND ABS(@TempMLate) > @PermisEarlyDepart
			BEGIN 
				SET @TempMUEarlyDeparture = ABS(@TempMLate)
			END

			SET @TempMUStatus = 'P'

			IF SUBSTRING(@TempShiftPosition,1,1) = 'H'
			BEGIN
				IF @TempMULeaveValue  < 0.5 
				BEGIN
					SET @TempMUPresentValue = 0.5 - @TempMULeaveValue
				END
				SET @TempMUAbsentValue = 0				
			END
			ELSE
			BEGIN
				SET @TempMUPresentValue = 1
			END

			SET @TempMAbsHours = DATEDIFF(N, @TempShiftStartDateTime, @TempShiftEndDateTime) - @TempMUHoursworked
			IF @TempMAbsHours > 0
			BEGIN
				IF @IsHalfDay = 'Y' AND @TempMUHoursworked < @Half AND @TempMUHoursworked >= @Time
				BEGIN 
					SET @TempMUStatus = 'HLF'
					SET @TempMUPresentValue = 0.5
					SET @TempMUAbsentValue = 0.5 
					SET @TempMULateArrival = 0
					SET @TempMUEarlyDeparture = 0
					SET @TempMUTotalLossHRS = 0
				END
				ELSE IF @IsHalfDay = 'Y' AND @IsShort = 'Y' AND @TempMUHoursworked <= @Short AND @TempMUHoursworked > @Half
				BEGIN
					SET @TempMUStatus = 'SRT'
					SET @TempMUPresentValue = 1
				END
			END

			IF @IsOT = 'Y'
			BEGIN
				IF @TempGOTFormulae = 1 
				BEGIN 
					SET @TempMOT = DATEDIFF(N, @TempShiftEndDateTime, @MOut2)
				END
				ELSE IF @TempGOTFormulae = 2
				BEGIN 
					SET @TempMOT = @TempMUHoursworked - @TempMShiftTime
				END
				ELSE IF  @TempGOTFormulae = 3
				BEGIN
					SET @TempMEarly = DATEDIFF(N, @MIn1, @TempShiftStartDateTime)
					IF @TempMEarly <= @TempGOTEarly 
					BEGIN
						SET @TempMEarly = 0
					END

					SET @TempMLate = DATEDIFF(N, @TempShiftEndDateTime , @MOut2)
					IF @TempMLate <= @TempGOTLate
					BEGIN 
						SET @TempMLate = 0
					END

					SET @TempMOT = @TempMEarly + @TempMLate - @TempMUExcLunchHours 
				END

				IF @TempMOT < @TempMOtStartAfter
				BEGIN 
					SET @TempMOT = 0
				END
				ELSE IF @TempMOtDeductHours = 0 AND @TempMOT > @TempMOtDeductAfter
				BEGIN 
					SET @TempMOT = @TempMOtDeductAfter
				END
				ELSE IF @TempMOT >= (@TempMOtDeductAfter + @TempMOtDeductHours)
				BEGIN 
					SET @TempMOT = @TempMOT - @TempMOtDeductHours 
				END

				IF @TempGIsOTMinus = 'N' AND @TempMOT < 0
				BEGIN 
					SET @TempMOT = 0
				END

				SET @TempMUOtDuration = @TempMOT
			END
			GOTO Lastl
		END
		ELSE
		BEGIN 
			SET @TempMUPresentValue = 1
			SET @TempMUStatus = 'P'
		END

		
	END

	IF @TempMULeaveValue = 0 AND @TempMUPresentValue = 0 AND @TempMUHolidayValue = 0 AND @TempMUWoValue = 0
		BEGIN 
			SET @TempMUAbsentValue = 1
		END
	Lastl:

	IF @TempMUStatus = 'HLF' AND @IsOT = 'Y'
	BEGIN 
		SET @TempMUOtDuration = @TempMUHoursworked - 240
	END

	IF @TempGOTRound = 'Y'
	BEGIN 
		IF @TempMUOtDuration >= 0
		BEGIN
			SET @TempMOTInHours = (@TempMUOtDuration/60) + ((@TempMUOtDuration % 60)* 0.01)
			SET @TempMOTFactor = @TempMOTInHours - (@TempMUOtDuration/60)
		END
		ELSE
		BEGIN
			SET @TempMOTInHours = -((ABS(@TempMUOtDuration)/60) + ((ABS(@TempMUOtDuration) % 60)*0.01))
			SET @TempMOTFactor = @TempMOTInHours - (-(ABS(@TempMUOtDuration)/60))
		END
		
		IF @TempMOTFactor <= 0.15 
		BEGIN
			SET @TempMOTInHours =  (@TempMUOtDuration/60) 
			SET @TempMMinutes = 0
		END
		ELSE IF @TempMOTFactor >= 0.15  AND @TempMOTFactor < 0.45
		BEGIN
			SET @TempMOTInHours =  (@TempMUOtDuration/60) + 0.3
			SET @TempMMinutes =  0.3 * 60
		END
		ELSE IF  @TempMOTFactor >= 0.45 
		BEGIN
			SET @TempMOTInHours =  (@TempMUOtDuration/60) + 1
			SET @TempMMinutes = 1 * 60
		END

		--TODO : Check actual value of MUOUTDuration being stored in database
		SET @TempMUOtDuration =  ((@TempMUOtDuration/60)* 60) + @TempMMinutes
	END

	IF @IsTimeLossAllowed = 'Y'
	BEGIN 
		SET @TempMUTotalLossHRS = @TempMULateArrival + @TempMUEarlyDeparture + @TempMULunchLateArrival + @TempMULunchEarlyDeparture
	END
	ELSE
	BEGIN		
		SET @TempMULateArrival = 0
		SET @TempMUEarlyDeparture = 0
		SET @TempMUTotalLossHRS = 0
		SET @TempMULunchLateArrival = 0
		SET @TempMULunchEarlyDeparture = 0
	END

	IF @OTRate = 0
	BEGIN 
		SET @TempMUOtAmount = 0
	END
	ELSE
	BEGIN
		SET @TempMUOtAmount = @TempMUOtDuration  * (@OTRate / 60)
	END

	set @strTempMDate =convert(varchar,@DateOfOffice,23)				
	
	set @strTempShiftStartDateTime =convert(varchar,@TempShiftStartDateTime,21)
	set @strTempShiftEndDateTime =convert(varchar,@TempShiftEndDateTime,21)
	set @strTempLunchStartDateTime =convert(varchar,@TempLunchStartDateTime,21)
	set @strTempLunchEndDateTime =convert(varchar,@TempLunchEndDateTime,21)

	set @strTempMUHoursworked = @TempMUHoursworked
	set @strTempMUExcLunchHours = @TempMUExcLunchHours
	set @strTempMUOtDuration = @TempMUOtDuration
	set @strTempMUOSDuration = @TempMUOSDuration
	set @strTempMUOtAmount = @TempMUOtAmount
	set @strTempMUEarlyArrival = @TempMUEarlyArrival
	set @strTempMUEarlyDeparture = @TempMUEarlyDeparture
	set @strTempMULateArrival  = @TempMULateArrival 
	set @strTempMULunchEarlyDeparture = @TempMULunchEarlyDeparture
	set @strTempMULunchLateArrival = @TempMULunchLateArrival
	set @strTempMUTotalLossHRS  = @TempMUTotalLossHRS 
	set @strTempMUStatus = @TempMUStatus
	set @strShiftAttended = @ShiftAttended
	set @strMIn1Manual = @MIn1Manual
	set @strMIn2Manual = @MIn2Manual
	set @strMOUT1Manual = @MOUT1Manual
	set @strMOUT2Manual = @MOUT2Manual
	set @strTempMULeaveValue = @TempMULeaveValue
	set @strTempMUPresentValue = @TempMUPresentValue
	set @strTempMUAbsentValue = @TempMUAbsentValue
	set @strTempMUHolidayValue = @TempMUHolidayValue
	set @strTempMUWoValue = @TempMUWoValue
	set @strTempMUOutWorkDuration = @TempMUOutWorkDuration
	
	IF DATEPART(month, @DateOfOffice) < 10
	BEGIN
		set @TableName= '[TBLTIMEREGISTER_0'+CAST(DATEPART(month, @DateOfOffice) AS varchar)+CAST(DATEPART(year, @DateOfOffice) AS varchar)+']'

		--SET @SQL1='UPDATE '+ @TableName + 'set [SHIFTSTARTTIME] = '''+@strTempShiftStartDateTime+''', [SHIFTENDTIME] = '''+@strTempShiftEndDateTime+''', [LUNCHSTARTTIME] = '''+@strTempLunchStartDateTime+''', [LUNCHENDTIME] = '''+@strTempLunchEndDateTime+''' where SSN =''' + @SSN + ''' AND DateOFFICE ='''+@strTempMDate+'''';
		--SET @SQL='UPDATE '+ @TableName + 'set [in1] = '''+@strMIn1+''', [IN2] = '''+@strMIn2+''', [out1] = '''+@strMOut1+''', [out2] = '''+@strMOut2+''', [SHIFTSTARTTIME] = '''+@strTempShiftStartDateTime+''', [SHIFTENDTIME] = '''+@strTempShiftEndDateTime+''', [LUNCHSTARTTIME] = '''+@strTempLunchStartDateTime+''', [LUNCHENDTIME] = '''+@strTempLunchEndDateTime+''' where SSN =''' + @SSN + ''' AND DateOFFICE ='''+@strTempMDate+'''';
		
		IF ISDATE(@MIn1) = 1 AND ISDATE(@MIn2) = 1 AND ISDATE(@MOut1) = 1 AND ISDATE(@MOut2) = 1
		BEGIN
			set @strMIn1 =convert(varchar,@MIn1,21)
			set @strMIn2 =convert(varchar,@MIn2,21)
			set @strMOut1 =convert(varchar,@MOut1,21)
			set @strMOut2 =convert(varchar,@MOut2,21)

			--SET @SQL='UPDATE '+ @TableName + 'set [in1] = '''+@strMIn1+''', [IN2] = '''+@strMIn2+''', [out1] = '''+@strMOut1+''', [out2] = '''+@strMOut2+''' where SSN =''' + @SSN + ''' AND DateOFFICE ='''+@strTempMDate+'''';
			SET @SQLUpdatePunch='UPDATE '+ @TableName + 'set [in1] = '''+@strMIn1+''', [IN2] = '''+@strMIn2+''', [out1] = '''+@strMOut1+''', [out2] = '''+@strMOut2+''', [SHIFTSTARTTIME] = '''+@strTempShiftStartDateTime+''', [SHIFTENDTIME] = '''+@strTempShiftEndDateTime+''', [LUNCHSTARTTIME] = '''+@strTempLunchStartDateTime+''', [LUNCHENDTIME] = '''+@strTempLunchEndDateTime+''' where SSN =''' + @SSN + ''' AND DateOFFICE ='''+@strTempMDate+'''';
		END
		--ELSE IF @MIn1 <> null AND @MIn2 = null AND @MOUT1 = null AND @MOut2 <> null
		IF ISDATE(@MIn1) = 1 AND ISDATE(@MIn2) = 0 AND ISDATE(@MOut1) = 0 AND ISDATE(@MOut2) = 1
		BEGIN
			set @strMIn1 =convert(varchar,@MIn1,21)
			set @strMOut2 =convert(varchar,@MOut2,21)

			SET @SQLUpdatePunch='UPDATE '+ @TableName + 'set [in1] = '''+@strMIn1+''', [out2] = '''+@strMOut2+''', [SHIFTSTARTTIME] = '''+@strTempShiftStartDateTime+''', [SHIFTENDTIME] = '''+@strTempShiftEndDateTime+''', [LUNCHSTARTTIME] = '''+@strTempLunchStartDateTime+''', [LUNCHENDTIME] = '''+@strTempLunchEndDateTime+''' where SSN =''' + @SSN + ''' AND DateOFFICE ='''+@strTempMDate+'''';
		END
		--ELSE IF @MIn1 <> null AND @MIn2 = null AND @MOUT1 = null AND @MOut2 = null
		IF ISDATE(@MIn1) = 1 AND ISDATE(@MIn2) = 0 AND ISDATE(@MOut1) = 0 AND ISDATE(@MOut2) = 0
		BEGIN
			set @strMIn1 =convert(varchar,@MIn1,21)
			
			SET @SQLUpdatePunch='UPDATE '+ @TableName + 'set [in1] = '''+@strMIn1+''' where SSN =''' + @SSN + ''' AND DateOFFICE ='''+@strTempMDate+'''';
		END
		--ELSE IF @MIn1 <> null AND @MIn2 = null AND @MOUT1 <> null AND @MOut2 <> null
		IF ISDATE(@MIn1) = 1 AND ISDATE(@MIn2) = 0 AND ISDATE(@MOut1) = 1 AND ISDATE(@MOut2) = 1
		BEGIN
			set @strMIn1 =convert(varchar,@MIn1,21)
			set @strMOut1 =convert(varchar,@MOut1,21)
			set @strMOut2 =convert(varchar,@MOut2,21)

			SET @SQLUpdatePunch='UPDATE '+ @TableName + 'set [in1] = '''+@strMIn1+''', [out1] = '''+@strMOut1+''', [out2] = '''+@strMOut2+''' where SSN =''' + @SSN + ''' AND DateOFFICE ='''+@strTempMDate+'''';
		END
		--ELSE IF @MIn1 = null AND @MIn2 <> null AND @MOUT1 <> null AND @MOut2 <> null
		IF ISDATE(@MIn1) = 0 AND ISDATE(@MIn2) = 1 AND ISDATE(@MOut1) = 1 AND ISDATE(@MOut2) = 1
		BEGIN
			set @strMIn2 =convert(varchar,@MIn2,21)
			set @strMOut1 =convert(varchar,@MOut1,21)
			set @strMOut2 =convert(varchar,@MOut2,21)

			SET @SQLUpdatePunch='UPDATE '+ @TableName + 'set [in2] = '''+@strMIn2+''', [out1] = '''+@strMOut1+''', [out2] = '''+@strMOut2+''' where SSN =''' + @SSN + ''' AND DateOFFICE ='''+@strTempMDate+'''';
		END
		--ELSE IF @MIn1 = null AND @MIn2 <> null AND @MOUT1 <> null AND @MOut2 = null
		IF ISDATE(@MIn1) = 0 AND ISDATE(@MIn2) = 1 AND ISDATE(@MOut1) = 1 AND ISDATE(@MOut2) = 0
		BEGIN			
			set @strMIn2 =convert(varchar,@MIn2,21)
			set @strMOut1 =convert(varchar,@MOut1,21)

			SET @SQLUpdatePunch='UPDATE '+ @TableName + 'set [in2] = '''+@strMIn2+''', [out1] = '''+@strMOut1+''' where SSN =''' + @SSN + ''' AND DateOFFICE ='''+@strTempMDate+'''';
		END		
		
		SET @SQLUpdateShift='UPDATE '+ @TableName + 'set [SHIFTSTARTTIME] = '''+@strTempShiftStartDateTime+''', [SHIFTENDTIME] = '''+@strTempShiftEndDateTime+''', [LUNCHSTARTTIME] = '''+@strTempLunchStartDateTime+''', [LUNCHENDTIME] = '''+@strTempLunchEndDateTime+''' where SSN =''' + @SSN + ''' AND DateOFFICE ='''+@strTempMDate+'''';
		--SET @SQLUpdateData='UPDATE '+ @TableName + 'set [SHIFTSTARTTIME] = '''+@strTempShiftStartDateTime+''', [SHIFTENDTIME] = '''+@strTempShiftEndDateTime+''', [LUNCHSTARTTIME] = '''+@strTempLunchStartDateTime+''', [LUNCHENDTIME] = '''+@strTempLunchEndDateTime+''' where SSN =''' + @SSN + ''' AND DateOFFICE ='''+@strTempMDate+'''';
		
		SET @SQLUpdateData='UPDATE '+ @TableName + ' set 
		[HOURSWORKED] = '+@strTempMUHoursworked+', 
		[EXCLUNCHHOURS] = '+@strTempMUExcLunchHours+', 
		[OTDURATION] = '+@strTempMUOtDuration+',
		[OSDURATION] = '+@strTempMUOSDuration+', 
		[OTAMOUNT] = '''+@strTempMUOtAmount+''', 
		[EARLYARRIVAL] = '''+@strTempMUEarlyArrival+''', 
		[EARLYDEPARTURE] = '''+@strTempMUEarlyDeparture+''', 
		[LATEARRIVAL] = '''+@strTempMULateArrival+''', 
		[LUNCHEARLYDEPARTURE] = '''+@strTempMULunchEarlyDeparture+''', 
		[LUNCHLATEARRIVAL] = '''+@strTempMULunchLateArrival+''', 
		[TOTALLOSSHRS] = '''+@strTempMUTotalLossHRS+''', 
		[STATUS] = '''+@strTempMUStatus+''', 
		[SHIFTATTENDED] = '''+@strShiftAttended+''',
		[IN1MANNUAL] = '''+@strMIn1Manual+''',
		[IN2MANNUAL] = '''+@strMIn2Manual+''',
		[OUT1MANNUAL] = '''+@strMOUT1Manual+''',
		[OUT2MANNUAL] = '''+@strMOUT2Manual+''',
		[LEAVEVALUE] = '''+@strTempMULeaveValue+''',
		[PRESENTVALUE] = '''+@strTempMUPresentValue+''',
		[ABSENTVALUE] = '''+@strTempMUAbsentValue+''',
		[HOLIDAY_VALUE] = '''+@strTempMUHolidayValue+''',
		[WO_VALUE] = '''+@strTempMUWoValue+''',
		[OUTWORKDURATION] = '''+@strTempMUOutWorkDuration+''' 
	
		where SSN =''' + @SSN + ''' AND DateOFFICE ='''+@strTempMDate+'''';

		--SET @SQL1='UPDATE '+ @TableName + 'set [SHIFTSTARTTIME] = '''+@TempShiftStartDateTime+''', [SHIFTENDTIME] = '''+@TempShiftEndDateTime+''' where SSN =''' + @SSN + ''' AND DateOFFICE ='''+@strTempMDate+'''';
		
		print ('@SQLUpdateShift : --'+@SQLUpdateShift)
		print ('@SQLUpdateData : --'+@SQLUpdateData)
		print ('@SQLUpdatePunch : --'+@SQLUpdatePunch)

		execute(@SQLUpdateShift)
		execute(@SQLUpdateData)
		execute(@SQLUpdatePunch)
	END
	ELSE
	BEGIN
		set @TableName= '[TBLTIMEREGISTER_'+CAST(DATEPART(month, @DateOfOffice) AS varchar)+CAST(DATEPART(year, @DateOfOffice) AS varchar)+']'
		
		IF @MIn1 <> null AND @MIn2 <> null AND @MOUT1 <> null AND @MOut2 <> null
		BEGIN
			set @strMIn1 =convert(varchar,@MIn1,21)
			set @strMIn2 =convert(varchar,@MIn2,21)
			set @strMOut1 =convert(varchar,@MOut1,21)
			set @strMOut2 =convert(varchar,@MOut2,21)

			SET @SQL='UPDATE '+ @TableName + 'set [in1] = '''+@strMIn1+''', [IN2] = '''+@strMIn2+''', [out1] = '''+@strMOut1+''', [out2] = '''+@strMOut2+''' where SSN =''' + @SSN + ''' AND DateOFFICE ='''+@strTempMDate+'''';
		END
		ELSE IF @MIn1 <> null AND @MIn2 = null AND @MOUT1 = null AND @MOut2 <> null
		BEGIN
			set @strMIn1 =convert(varchar,@MIn1,21)
			set @strMOut2 =convert(varchar,@MOut2,21)

			SET @SQL='UPDATE '+ @TableName + 'set [in1] = '''+@strMIn1+''', [out2] = '''+@strMOut2+''' where SSN =''' + @SSN + ''' AND DateOFFICE ='''+@strTempMDate+'''';
		END
		ELSE IF @MIn1 <> null AND @MIn2 = null AND @MOUT1 = null AND @MOut2 = null
		BEGIN
			set @strMIn1 =convert(varchar,@MIn1,21)
			
			SET @SQL='UPDATE '+ @TableName + 'set [in1] = '''+@strMIn1+''' where SSN =''' + @SSN + ''' AND DateOFFICE ='''+@strTempMDate+'''';
		END
		ELSE IF @MIn1 <> null AND @MIn2 = null AND @MOUT1 <> null AND @MOut2 <> null
		BEGIN
			set @strMIn1 =convert(varchar,@MIn1,21)
			set @strMOut1 =convert(varchar,@MOut1,21)
			set @strMOut2 =convert(varchar,@MOut2,21)

			SET @SQL='UPDATE '+ @TableName + 'set [in1] = '''+@strMIn1+''', [out1] = '''+@strMOut1+''', [out2] = '''+@strMOut2+''' where SSN =''' + @SSN + ''' AND DateOFFICE ='''+@strTempMDate+'''';
		END
		ELSE IF @MIn1 = null AND @MIn2 <> null AND @MOUT1 <> null AND @MOut2 <> null
		BEGIN
			set @strMIn2 =convert(varchar,@MIn2,21)
			set @strMOut1 =convert(varchar,@MOut1,21)
			set @strMOut2 =convert(varchar,@MOut2,21)

			SET @SQL='UPDATE '+ @TableName + 'set [in2] = '''+@strMIn2+''', [out1] = '''+@strMOut1+''', [out2] = '''+@strMOut2+''' where SSN =''' + @SSN + ''' AND DateOFFICE ='''+@strTempMDate+'''';
		END
		ELSE IF @MIn1 = null AND @MIn2 <> null AND @MOUT1 <> null AND @MOut2 = null
		BEGIN			
			set @strMIn2 =convert(varchar,@MIn2,21)
			set @strMOut1 =convert(varchar,@MOut1,21)

			SET @SQL='UPDATE '+ @TableName + 'set [in2] = '''+@strMIn2+''', [out1] = '''+@strMOut1+''' where SSN =''' + @SSN + ''' AND DateOFFICE ='''+@strTempMDate+'''';
		END		

		SET @SQL1='UPDATE '+ @TableName + 'set [SHIFTSTARTTIME] = '''+@TempShiftStartDateTime+''', [SHIFTENDTIME] = '''+@TempShiftEndDateTime+''' where SSN =''' + @SSN + ''' AND DateOFFICE ='''+@strTempMDate+'''';
		print (@SQL)
		execute(@SQL)
	END
   
   --@TempShiftStartDateTime
   --@TempShiftEndDateTime
   --@TempLunchStartDateTime
   --@TempLunchEndDateTime

 --   UPDATE tbltimeregister
	--SET 		
	--	IN1MANNUAL = @MIn1Manual,
	--	IN2MANNUAL = @MIn2Manual,
	--	OUT1MANNUAL = @MOUT1Manual,
	--	OUT2MANNUAL = @MOUT2Manual,
	--	LEAVEVALUE = @TempMULeaveValue,
	--	PRESENTVALUE = @TempMUPresentValue,
	--	ABSENTVALUE = @TempMUAbsentValue,
	--	HOLIDAY_VALUE = @TempMUHolidayValue,
	--	WO_VALUE = @TempMUWoValue,
	--	OUTWORKDURATION = @TempMUOutWorkDuration
	--	HOURSWORKED = @TempMUHoursworked,
	--	EXCLUNCHHOURS = @TempMUExcLunchHours,
	--	OTDURATION = @TempMUOtDuration,
	--	OSDURATION = @TempMUOSDuration,
	--	OTAMOUNT = @TempMUOtAmount,
	--	EARLYARRIVAL = @TempMUEarlyArrival,
	--	EARLYDEPARTURE = @TempMUEarlyDeparture,
	--	LATEARRIVAL = @TempMULateArrival,
	--	LUNCHEARLYDEPARTURE = @TempMULunchEarlyDeparture,
	--	LUNCHLATEARRIVAL = @TempMULunchLateArrival,
	--	TOTALLOSSHRS = @TempMUTotalLossHRS,
	--	[STATUS] = @TempMUStatus,
	--	SHIFTATTENDED = @ShiftAttended,
	--	IN1 = @MIn1,
	--	IN2 = @MIn2,
	--	OUT1 = @MOut1,
	--	OUT2 = @MOut2,
	--	IN1MANNUAL = @MIn1Manual,
	--	IN2MANNUAL = @MIn2Manual,
	--	OUT1MANNUAL = @MOUT1Manual,
	--	OUT2MANNUAL = @MOUT2Manual,

	--	LEAVEVALUE = @TempMULeaveValue,
	--	PRESENTVALUE = @TempMUPresentValue,
	--	ABSENTVALUE = @TempMUAbsentValue,
	--	HOLIDAY_VALUE = @TempMUHolidayValue,
	--	WO_VALUE = @TempMUWoValue,
	--	OUTWORKDURATION = @TempMUOutWorkDuration
	--	WHERE SSN = @SSN 
	--	AND DateOFFICE = @DateOfOffice
END	





