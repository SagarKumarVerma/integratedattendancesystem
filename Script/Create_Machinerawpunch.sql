
CREATE TABLE [dbo].[MachineRawPunch](
	[CARDNO] [char](10) NOT NULL,
	[OFFICEPUNCH] [datetime] NOT NULL,
	[P_DAY] [char](1) NULL,
	[ISMANUAL] [char](1) NULL,
	[ReasonCode] [char](3) NULL,
	[MC_NO] [char](3) NULL,
	[INOUT] [char](1) NULL,
	[PAYCODE] [char](10) NULL,
	[Lcode] [char](4) NULL,
	[SSN] [varchar](100) NOT NULL,
	[CompanyCode] [varchar](4) NULL,
	[DeviceID] [varchar](50) NULL,
 CONSTRAINT [PK_MachineRawPunch] PRIMARY KEY CLUSTERED 
(
	[CARDNO] ASC,
	[OFFICEPUNCH] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO


