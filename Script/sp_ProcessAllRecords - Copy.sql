DROP PROCEDURE [dbo].[ProcessAllRecords]
GO
create PROCEDURE [dbo].[ProcessAllRecords]
	@FromDate DATETIME,
	@ToDate DATETIME,
	@CompanyCode VARCHAR(10),
	@PayCode VARCHAR(10),
	@SetupId Int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- tblSetup variables
	DECLARE @TempGNightShiftFourPunch CHAR(1)

	DECLARE @TempCompanyCode VARCHAR(10)
	DECLARE @TempDepartmentCode VARCHAR(10)
	DECLARE @TempDayHour FLOAT
	DECLARE @TempSSN VARCHAR(100)
	DECLARE @TempPayCode VARCHAR(10)
	DECLARE @TempIsOutWork CHAR(1)
	DECLARE @TempIsPunchAll CHAR(1)
	DECLARE @TempRSShiftAttended CHAR(3)

	-- RsEMP temp varaibles
	DECLARE @TempRsEmpTwo CHAR(1)
	DECLARE @TempRsEmpInOnly CHAR(1)
	
	-- For MachineRowPunch
	DECLARE @TempReasonCode VARCHAR(100)
	DECLARE @TempFirstPunchReasonCode VARCHAR(100)
	DECLARE @TempLastPunchReasonCode VARCHAR(100)
	DECLARE @TempRsTOut DATETIME
	
	
	DECLARE @TempRsPunchOFficePunch DATETIME
	DECLARE @TempRSPunchPDay CHAR(1)
	DECLARE @TempMPFlg CHAR(1)
	DECLARE @TempExecuteMachineRawUpdate BIT

	-- RsOut Temp Variables for INSERT
	DECLARE @TempRsOutDateOffice DATETIME
	DECLARE @TempRsOutPayCode VARCHAR(10)
	DECLARE @TempRsOutIN1 DATETIME 
	DECLARE @TempRsOutRIN1 CHAR(3) 
	DECLARE @TempRsOutOUT1 DATETIME 
	DECLARE @TempRsOutROUT1 CHAR(3) 
	DECLARE @TempRsOutIN2 DATETIME 
	DECLARE @TempRsOutRIN2 CHAR(3) 
	DECLARE @TempRsOutOUT2 DATETIME 
	DECLARE @TempRsOutROUT2 CHAR(3) 
	DECLARE @TempRsOutIN3 DATETIME 
	DECLARE @TempRsOutRIN3 CHAR(3) 
	DECLARE @TempRsOutOUT3 DATETIME 
	DECLARE @TempRsOutROUT3 CHAR(3) 
	DECLARE @TempRsOutIN4 DATETIME 
	DECLARE @TempRsOutRIN4 CHAR(3) 
	DECLARE @TempRsOutOUT4 DATETIME 
	DECLARE @TempRsOutROUT4 CHAR(3) 
	DECLARE @TempRsOutIN5 DATETIME 
	DECLARE @TempRsOutRIN5 CHAR(3) 
	DECLARE @TempRsOutOUT5 DATETIME 
	DECLARE @TempRsOutROUT5 CHAR(3) 
	DECLARE @TempRsOutIN6 DATETIME 
	DECLARE @TempRsOutRIN6 CHAR(3) 
	DECLARE @TempRsOutOUT6 DATETIME 
	DECLARE @TempRsOutROUT6 CHAR(3) 
	DECLARE @TempRsOutIN7 DATETIME 
	DECLARE @TempRsOutRIN7 CHAR(3) 
	DECLARE @TempRsOutOUT7 DATETIME 
	DECLARE @TempRsOutROUT7 CHAR(3) 
	DECLARE @TempRsOutIN8 DATETIME 
	DECLARE @TempRsOutRIN8 CHAR(3) 
	DECLARE @TempRsOutOUT8 DATETIME 
	DECLARE @TempRsOutROUT8 CHAR(3) 
	DECLARE @TempRsOutIN9 DATETIME 
	DECLARE @TempRsOutRIN9 CHAR(3) 
	DECLARE @TempRsOutOUT9 DATETIME 
	DECLARE @TempRsOutROUT9 CHAR(3) 
	DECLARE @TempRsOutIN10 DATETIME 
	DECLARE @TempRsOutRIN10 CHAR(3) 
	DECLARE @TempRsOutOUT10 DATETIME 
	DECLARE @TempRsOutROUT10 CHAR(3) 
	DECLARE @TempRsOutOutWorkDuration int
	DECLARE @TempRsOutReasonOutWork CHAR(1)
	DECLARE @TempRsOutSSN VARCHAR(100)	
	DECLARE @TempRsOutRecordCount INT

	DECLARE @IsPreviousDaySet BIT

		
	DECLARE @TempMReasonCode VARCHAR(100)
	DECLARE @TempMFirstPunchReasonCode VARCHAR(100)
	DECLARE @TempMLastPunchReasonCode VARCHAR(100)
	DECLARE @TempTimeRegsiterCount INT
	DECLARE @TempProcDate DATETIME
	DECLARE @TempMIn1 DATETIME
	DECLARE @TempMIn2 DATETIME
	DECLARE @TempMOut1 DATETIME
	DECLARE @TempMOut2 DATETIME
	DECLARE @TempMShiftStartTime DATETIME
	DECLARE @TempMShiftEndTime DATETIME
	DECLARE @TempMLunchStartTime DATETIME
	DECLARE @TempMLunchEndTime DATETIME
	DECLARE @TempMDate1 DATETIME
	DECLARE @TempMDate2 DATETIME
	DECLARE @TempMIn1Manual CHAR(1)
	DECLARE @TempMIn2Manual CHAR(1)
	DECLARE @TempMOut2Manual CHAR(1)
	DECLARE @TempMOut1Manual CHAR(1)
	DECLARE @TempMReasonOutWork CHAR(1)
	DECLARE @TempMRsPunchCount INT
	DECLARE @TempMCtr INT

	DECLARE @TempMRsShiftAttended CHAR(3)
	DECLARE @HolidayCount INT
	DECLARE @IsHoliday BIT

	-- RSTime Temp Variables
	DECLARE @TempRsTimePaycode VARCHAR(10)
	DECLARE @TempRsTimeDateOffice DATETIME

	-- RSPunch Temp Varaibles
	DECLARE @TempRsPunchRowNumber INT
	DECLARE @TempRsPunchRowCount INT
	DECLARE @TempRsPunchCurrentRowIndex INT
	DECLARE @TempRsPunchCurrentRowIndex2 INT
	DECLARE @TempRsPunchTimeOut DATETIME
	DECLARE @TempRsPunchReasonCode CHAR(3)

	DECLARE @HDate	DATETIME
	DECLARE @HOLIDAY VARCHAR(20)
	DECLARE @ADJUSTMENTHOLIDAY DATETIME
	DECLARE @OT_FACTOR FLOAT
	DECLARE @HD_COMPANYCODE VARCHAR(10)
	DECLARE @HD_DEPARTMENTCODE	VARCHAR(10)
	DECLARE @BRANCHCODE VARCHAR(10)
	DECLARE @Divisioncode	VARCHAR(10)	


	-- TempTable Loop Varialbes 
	DECLARE @TempRsEmpRowNumber INT
	DECLARE @TempRsEmpRecordCount INT

	DECLARE @TempRsTimeRowNumber INT
	DECLARE @TempRsTimeRecordCount INT


	-- RsEmp Variables
	DECLARE @TempRSEmpAuthShift CHAR(50)

	-- tbl SETUP
	DECLARE @TempTblSetupSOut DATETIME
	DECLARE @TempTblSetupSEnd DATETIME
	DECLARE @TempMinutes INT

	-- RsTime Variable
	DECLARE @TempRsTimeShift CHAR(3)
	DECLARE @AutoShift CHAR(3)

	DECLARE @TempMRsTimeLeaveAmount FLOAT
	DECLARE @TempMRsTimeLeaveType1 CHAR(1)
	DECLARE @TempMRsTimeLeaveType2 CHAR(1)
	DECLARE @TempMRsTimeLeaveCode CHAR(3)
	DECLARE @TempMRsTimeLeaveType CHAR(1)
	DECLARE @TempMRsTimeFirstHalfLeaveCode CHAR(3)
	DECLARE @TempMRsTimeSecondHalfLeaveCode CHAR(3)
	DECLARE @TempMRsEmpIsOS CHAR(1)
	DECLARE @TempMRsEmpIsOT CHAR(1)
	DECLARE @TempMRsEmpInOnly CHAR(1)
	DECLARE @TempMRsEmpIsPunchAll CHAR(1)
	DECLARE @TempMRsEmpPermisLateArrival INT
	DECLARE @TempMRsEmpTime INT
	DECLARE @TempMRsEmpPermisEarlyDepart INT
	DECLARE @TempMRsEmpIsHalfDay CHAR(1)
	DECLARE @TempMRsEmpHalf INT
	DECLARE @TempMRsEmpIsShort CHAR(1)
	DECLARE @TempMRsEmpShort INT
	DECLARE @TempMRsEmpIsTimeLossAllowed CHAR(1)
	DECLARE @TempMRsEmpOTRate FLOAT
	DECLARE @TempRsEmpIsRoundTheClock CHAR(1)
	DECLARE @MIS CHAR (1)
	DECLARE @ISOTONOFF CHAR(1)
	DECLARE @ResignedAfter INT
	DECLARE @ENABLEAUTORESIGN CHAR(1)

	-- Variables added FOR EMPLOYEE GROUP
	-- 08/08/2019

	DECLARE @S_END DATETIME
	DECLARE @S_OUT DATETIME
	DECLARE @AUTOSHIFT_LOW INT
	DECLARE @AUTOSHIFT_UP INT
	DECLARE @ISPRESENTONWOPRESENT CHAR(1)
	DECLARE @ISPRESENTONHLDPRESENT CHAR(1)
	DECLARE @NightShiftFourPunch CHAR(1)
	DECLARE @ISAUTOABSENT CHAR(1)
	DECLARE @ISAWA CHAR(1)
	DECLARE @ISWA CHAR(1)
	DECLARE @ISAW CHAR(1)
	DECLARE @ISPREWO CHAR(1)
	DECLARE @ISOTOUTMINUSSHIFTENDTIME CHAR(1)
	DECLARE @ISOTWRKGHRSMINUSSHIFTHRS CHAR(1)
	DECLARE @ISOTEARLYCOMEPLUSLATEDEP CHAR(1)
	DECLARE @DEDUCTHOLIDAYOT INT
	DECLARE @DEDUCTWOOT INT
	DECLARE @ISOTEARLYCOMING INT
	DECLARE @OTMinus INT
	DECLARE @OTROUND INT
	DECLARE @OTEARLYDUR INT
	DECLARE @OTLATECOMINGDUR INT
	DECLARE @OTRESTRICTENDDUR INT
	DECLARE @DUPLICATECHECKMIN INT
    DECLARE @PREWO INT
	 
	-- Variables added during teting with Ajitesh
	-- 02/01/2018
	DECLARE @TempOutDate2DayHour DATETIME
	

	DECLARE @strTempMDate CHAR(10)
	DECLARE @strVALUES varchar(max)	
	DECLARE @TableName  VARCHAR(max)
	declare @SQL as nvarchar(max)
	declare @SQL1 as nvarchar(max)
	declare @SQL2 as nvarchar(max)


	DECLARE  @Holidays TABLE
	(
		HDate	DATETIME,
		HOLIDAY VARCHAR(20),
		ADJUSTMENTHOLIDAY DATETIME,
		OT_FACTOR FLOAT,
		HD_COMPANYCODE VARCHAR(10),
		HD_DEPARTMENTCODE	VARCHAR(10),
		BRANCHCODE VARCHAR(10),
		Divisioncode	VARCHAR(10)	
	)


	DECLARE @TempRsEmp TABLE 
	(
	    RowNumber INT,
		PAYCODE  CHAR (10),
		CARDNO  CHAR (8),
		SHIFT  CHAR (3) ,
		SHIFTTYPE  CHAR (1) ,
		SHIFTPATTERN  CHAR (11) ,
		SHIFTREMAINDAYS  INT  ,
		LASTSHIFTPERFORMED  CHAR (3) ,
		INONLY  CHAR (1) ,
		ISPUNCHALL  CHAR (1) ,
		ISTIMELOSSALLOWED  CHAR (1) ,
		ALTERNATE_OFF_DAYS  CHAR (10) ,
		CDAYS  FLOAT  ,
		ISROUNDTHECLOCKWORK  CHAR (1) ,
		ISOT  CHAR (1) ,
		OTRATE  CHAR (6) ,
		FIRSTOFFDAY  CHAR (3) ,
		SECONDOFFTYPE  CHAR (1) ,
		HALFDAYSHIFT  CHAR (3) ,
		SECONDOFFDAY  CHAR (3) ,
		PERMISLATEARRIVAL  INT  ,
		PERMISEARLYDEPRT  INT  ,
		ISAUTOSHIFT  CHAR (1) ,
		ISOUTWORK  CHAR (1) ,
		MAXDAYMIN  FLOAT  ,
		ISOS  CHAR (1) ,
		AUTH_SHIFTS  CHAR (50) ,
		TIME  INT ,
		SHORT  INT,
		HALF  INT,
		ISHALFDAY  CHAR (1) ,
		ISSHORT  CHAR (1) ,
		TWO  CHAR (1) ,
		isReleaver  CHAR (1) ,
		isWorker  CHAR (1) ,
		isFlexi  CHAR (1) ,
		SSN  VARCHAR (100),
		MIS CHAR(1),
		IsCOF CHAR(1),
		HLFAfter INT,
		HLFBefore INT,
	    ResignedAfter INT,
		EnableAutoResign CHAR (1),
		S_END DATETIME,
		S_OUT DATETIME,
		AUTOSHIFT_LOW INT,
		AUTOSHIFT_UP INT,
		ISPRESENTONWOPRESENT CHAR(1),
		ISPRESENTONHLDPRESENT CHAR(1),
		NightShiftFourPunch CHAR(1),
		ISAUTOABSENT CHAR(1),
		ISAWA CHAR(1),
		ISWA CHAR(1),
		ISAW CHAR(1),
		ISPREWO CHAR(1),
		ISOTOUTMINUSSHIFTENDTIME CHAR(1),
		ISOTWRKGHRSMINUSSHIFTHRS CHAR(1),
		ISOTEARLYCOMEPLUSLATEDEP CHAR(1),
		DEDUCTHOLIDAYOT INT,
		DEDUCTWOOT INT,
		ISOTEARLYCOMING CHAR(1),
		OTMinus CHAR(1),
		OTROUND CHAR(1),
		OTEARLYDUR INT,
		OTLATECOMINGDUR INT,
		OTRESTRICTENDDUR INT,
		DUPLICATECHECKMIN INT,
		PREWO INT,
		CompanyCode VARCHAR(10),
		DepartmentCode VARCHAR(10)
	)

	DECLARE @TempRsTime TABLE 
	(	
	    RowNumber INT,
		PAYCODE CHAR(10) ,
		DateOFFICE DATETIME ,
		SHIFTSTARTTIME DATETIME ,
		SHIFTENDTIME DATETIME ,
		LUNCHSTARTTIME DATETIME ,
		LUNCHENDTIME DATETIME ,
		HOURSWORKED INT ,
		EXCLUNCHHOURS INT ,
		OTDURATION INT ,
		OSDURATION INT ,
		OTAMOUNT FLOAT ,
		EARLYARRIVAL INT ,
		EARLYDEPARTURE INT ,
		LATEARRIVAL INT ,
		LUNCHEARLYDEPARTURE INT ,
		LUNCHLATEARRIVAL INT ,
		TOTALLOSSHRS INT ,
		STATUS CHAR(6) ,
		LEAVETYPE1 CHAR(1) ,
		LEAVETYPE2 CHAR(1) ,
		FIRSTHALFLEAVECODE CHAR(3) ,
		SECONDHALFLEAVECODE CHAR(3) ,
		REASON VARCHAR(2000) ,
		SHIFT CHAR(3) ,
		SHIFTATTENDED CHAR(3) ,
		IN1 DATETIME ,
		IN2 DATETIME ,
		OUT1 DATETIME ,
		OUT2 DATETIME ,
		IN1MANNUAL CHAR(1) ,
		IN2MANNUAL CHAR(1) ,
		OUT1MANNUAL CHAR(1) ,
		OUT2MANNUAL CHAR(1) ,
		LEAVEVALUE FLOAT ,
		PRESENTVALUE FLOAT ,
		ABSENTVALUE FLOAT ,
		HOLIDAY_VALUE FLOAT ,
		WO_VALUE FLOAT ,
		OUTWORKDURATION INT ,
		LEAVETYPE CHAR(1) ,
		LEAVECODE CHAR(3) ,
		LEAVEAMOUNT FLOAT ,
		LEAVEAMOUNT1 FLOAT ,
		LEAVEAMOUNT2 FLOAT ,
		FLAG CHAR(4) ,
		LEAVEAPRDate DATETIME ,
		VOUCHER_NO CHAR(10) ,
		ReasonCode CHAR(3) ,
		ApprovedOT FLOAT ,
		WBR_Flag CHAR(1) ,
		Stage1OTApprovedBy CHAR(10) ,
		Stage2OTApprovedBy CHAR(10) ,
		Stage1ApprovalDate DATETIME ,
		Stage2ApprovalDate DATETIME ,
		AllowOT INT ,
		AbsentSMS CHAR(1) ,
		LateSMS CHAR(1) ,
		InSMS CHAR(1) ,
		OutSMS CHAR(1) ,
		SSN VARCHAR(100)	,
		COFUsed CHAR(1) 	
	)

	DECLARE @TempRsPunch TABLE
	(
		CARDNO char(10) ,
		OFFICEPUNCH datetime ,
		P_DAY char(1) ,
		ISMANUAL char(1) ,
		ReasonCode char(3) ,
		MC_NO char(3) ,
		INOUT char(1) ,
		PAYCODE char(10) ,
		Lcode char(4) ,
		SSN varchar(100) ,
		CompanyCode varchar(4),
		Remarks varchar(200),
		RowNumber INT
	)

	DECLARE @TempRsOutWork TABLE
	(
		PAYCODE char(10) ,
		DateOFFICE datetime ,
		IN1 datetime ,
		RIN1 char(3) ,
		OUT1 datetime ,
		ROUT1 char(3) ,
		IN2 datetime ,
		RIN2 char(3) ,
		OUT2 datetime ,
		ROUT2 char(3) ,
		IN3 datetime ,
		RIN3 char(3) ,
		OUT3 datetime ,
		ROUT3 char(3) ,
		IN4 datetime ,
		RIN4 char(3) ,
		OUT4 datetime ,
		ROUT4 char(3) ,
		IN5 datetime ,
		RIN5 char(3) ,
		OUT5 datetime ,
		ROUT5 char(3) ,
		IN6 datetime ,
		RIN6 char(3) ,
		OUT6 datetime ,
		ROUT6 char(3) ,
		IN7 datetime ,
		RIN7 char(3) ,
		OUT7 datetime ,
		ROUT7 char(3) ,
		IN8 datetime ,
		RIN8 char(3) ,
		OUT8 datetime ,
		ROUT8 char(3) ,
		IN9 datetime ,
		RIN9 char(3) ,
		OUT9 datetime ,
		ROUT9 char(3) ,
		IN10 datetime ,
		RIN10 char(3) ,
		OUT10 datetime ,
		ROUT10 char(3) ,
		OUTWORKDURATION int ,
		Reason_OutWork char(1) ,
		SSN varchar(100) 
	)

	SET @TempGNightShiftFourPunch =''
	SELECT @TempGNightShiftFourPunch = NightShiftFourPunch 
	FROM TBLEMPLOYEESHIFTMASTER (nolock)
	WHERE PAYCODE = @PayCode
	SET @TempGNightShiftFourPunch='N'
	IF @PayCode != ''
	BEGIN
	    INSERT INTO @TempRsEmp
		Select  ROW_NUMBER() OVER (ORDER BY Emp.PayCode) RowNumber, 
		    EmpShift.*,
			Emp.companycode,
			Emp.departmentcode 
			from tblemployeeshiftmaster(nolock) EmpShift
			INNER JOIN tblemployee(nolock)  Emp
			ON EmpShift.SSN = Emp.SSN 
			WHERE Emp.COMPANYCODE =@CompanyCode
			AND EmpShift.PAYCODE=@PayCode
			AND Emp.ACTIVE='Y'
	END
	ELSE
	BEGIN
		INSERT INTO @TempRsEmp
		Select 
		    ROW_NUMBER() OVER (ORDER BY Emp.PayCode) RowNumber, 
		    EmpShift.*,
			Emp.companycode,
			Emp.departmentcode 
			from tblemployeeshiftmaster(nolock) EmpShift
			INNER JOIN tblemployee(nolock)  Emp
			ON EmpShift.SSN = Emp.SSN 
			WHERE Emp.COMPANYCODE =@CompanyCode
			AND Emp.ACTIVE='Y'
	END


	SET @TempRsEmpRecordCount = 0
	SELECT @TempRsEmpRecordCount = COUNT(*) FROM @TempRsEmp 

	SET @TempRsEmpRowNumber = 1

	--print 'Processing Number of employee' & @TempRsEmpRowNumber
	IF @TempRsEmpRecordCount > 0 
	BEGIN
		While @TempRsEmpRowNumber <= @TempRsEmpRecordCount
		BEGIN
		        SET @TempSSN = ''
				SET @TempCompanyCode = ''
				SET @TempDepartmentCode = ''
				SET @TempDayHour =  0
				SET @TempPayCode = ''
				SET @TempIsOutWork = ''
				SET @TempIsPunchAll = ''
				SET @TempRsEmpTwo = ''
				SET @TempRsEmpInOnly = ''
				SET @TempRSEmpAuthShift = '' 
				SET @TempMRsEmpIsOS = ''
				SET @TempMRsEmpIsOT = ''
				SET @TempMRsEmpInOnly = ''
				SET @TempMRsEmpIsPunchAll = ''
				SET @TempMRsEmpPermisLateArrival = 0
				SET @TempMRsEmpTime = 0
				SET @TempMRsEmpPermisEarlyDepart = 0
				SET @TempMRsEmpIsHalfDay = ''
				SET @TempMRsEmpHalf = 0
				SET @TempMRsEmpIsShort = ''
				SET @TempMRsEmpShort = 0
				SET @TempMRsEmpIsTimeLossAllowed = ''
				SET @TempMRsEmpOTRate = 0.0
				SET @TempRsEmpIsRoundTheClock = ''
				SET @MIS=''
				SET @ISOTONOFF=''
				SET @ENABLEAUTORESIGN='N'
						
			SELECT 
				@TempSSN = SSN,
				@TempCompanyCode = CompanyCode,
				@TempDepartmentCode = DepartmentCode,
				@TempDayHour =  ISNULL(MaxDayMin,0),
				@TempPayCode = PAYCODE,
				@TempIsOutWork = IsOutWork,
				@TempIsPunchAll = ISPUNCHALL,
				@TempRsEmpTwo = TWO,
				@TempRsEmpInOnly = INONLY,
				@TempRSEmpAuthShift = AUTH_SHIFTS ,
				@TempMRsEmpIsOS = ISOS,
				@TempMRsEmpIsOT = ISOT,
				@TempMRsEmpInOnly = INONLY,
				@TempMRsEmpIsPunchAll = ISPUNCHALL,
				@TempMRsEmpPermisLateArrival = PERMISLATEARRIVAL,
				@TempMRsEmpTime = [Time],
				@TempMRsEmpPermisEarlyDepart = PERMISEARLYDEPRT,
				@TempMRsEmpIsHalfDay = ISHALFDAY,
				@TempMRsEmpHalf = HALF,
				@TempMRsEmpIsShort = ISSHORT,
				@TempMRsEmpShort = SHORT,
				@TempMRsEmpIsTimeLossAllowed = ISTIMELOSSALLOWED,
				@TempMRsEmpOTRate = OTRATE,
				@TempRsEmpIsRoundTheClock = ISROUNDTHECLOCKWORK	,
				@MIS=MIS,
				@ISOTONOFF=IsCOF,
				@S_END=S_END,
				@S_OUT=S_OUT,
				@AUTOSHIFT_LOW=AUTOSHIFT_LOW,
				@AUTOSHIFT_UP=AUTOSHIFT_UP,
				@ISPRESENTONWOPRESENT=ISPRESENTONWOPRESENT,
				@ISPRESENTONHLDPRESENT=ISPRESENTONHLDPRESENT,
				@NightShiftFourPunch=NightShiftFourPunch,
				@ISOTOUTMINUSSHIFTENDTIME=ISOTOUTMINUSSHIFTENDTIME,
				@ISOTWRKGHRSMINUSSHIFTHRS=ISOTWRKGHRSMINUSSHIFTHRS,
				@ISOTEARLYCOMEPLUSLATEDEP=ISOTEARLYCOMEPLUSLATEDEP
	     		FROM @TempRsEmp WHERE RowNumber = @TempRsEmpRowNumber


			DELETE FROM @Holidays
			-- Load Holidays
			-- TODO : Optimize . Load Holiday table once and filter based 
			-- on Department and company code
			INSERT INTO @Holidays 
				SELECT * FROM Holiday(nolock) 
			WHERE DEPARTMENTCODE = @TempDepartmentCode 
			AND CompanyCode = @TempCompanyCode
			
			-- TODO : Remove it after verification as we are already updating in ProcessBackData
			UPDATE MACHINERAWPUNCH 
			SET P_DAY='N' 
			WHERE PAYCODE=@TempPayCode 
			AND OFFICEPUNCH>=CAST(CONVERT(varchar(10), (DATEADD(DAY,1,@FromDate)), 101) AS DATE)

			
			DELETE FROM tblOutWorkRegister 
			WHERE PAYCODE=@PayCode
			AND DateOFFICE BETWEEN @FromDate AND  @ToDate
			--Ali
			DECLARE @RowCount1 INTEGER
			--End Ali
			
			---Change For multipal tbltimeregister
			declare @TempDateOffice1 DATETIME;
			set @TempDateOffice1=@FromDate;
			
			while @TempDateOffice1 <= @ToDate
			BEGIN
				declare @test as varchar(max); 
				declare @a char(10),  @b varchar(max) ,@c char(100)
				--set @a =@PayCode
				set @b =convert(varchar,@FromDate,23)
				set @strTempMDate =convert(varchar,@TempDateOffice1,23)
				
				--print(@strTempMWoVal)
								
				IF DATEPART(month, @TempDateOffice1) < 10
				BEGIN
				--set @strVALUES = ''''+@PayCode+''','''+@strTempMDate+''','''+@TempMShift+''','''+@TempMShift+''','''+@TempMSTAT+''',1,0,0,0,0,0,0,0,'''+@SSN+'''';
					set @TableName= '[TBLTIMEREGISTER_0'+CAST(DATEPART(month, @TempDateOffice1) AS varchar)+CAST(DATEPART(year, @TempDateOffice1) AS varchar)+']'
					
					SET @SQL1='UPDATE '+ @TableName + ' SET in1 = NULL, in2 = NULL, out1 = NULL,out2= NULL where SSN =''' + @TempSSN + ''' AND DateOFFICE ='''+@strTempMDate+'''';
					
					print (@SQL1)
					execute(@SQL1)	
				END
				ELSE
				BEGIN
					set @TableName= '[TBLTIMEREGISTER_'+CAST(DATEPART(month, @TempDateOffice1) AS varchar)+CAST(DATEPART(year, @TempDateOffice1) AS varchar)+']'
					
					SET @SQL1='UPDATE '+ @TableName + ' SET in1 = NULL, in2 = NULL, out1 = NULL,out2= NULL where SSN =''' + @TempSSN + ''' AND DateOFFICE ='''+@strTempMDate+'''';
					
					--print (@strVALUES)
					print (@SQL1)
					execute(@SQL1)
				END
				set @TempDateOffice1 =@TempDateOffice1+1;
			END
			---Change For multipal tbltimeregister			
			
			--UPDATE tbltimeregister 
			--SET in1=null,
			--	in2=null,
			--	out1=null,
			--	out2=null 
			--WHERE PAYCODE=@PayCode
			--AND DateOFFICE BETWEEN @FromDate AND  @ToDate

			--Ali
			SELECT @RowCount1 = @@ROWCOUNT
			--End Ali

			DELETE FROM @TempRsTime

			---Change For multipal tbltimeregister
			declare @TempDateOffice DATETIME;
			set @TempDateOffice=@FromDate;

			declare @FromMonth as int; 
			set @FromMonth=(DATEPART(month, @FromDate))

			declare @ToMonth as int; 
			set @ToMonth=(DATEPART(month, @FromDate))			
			

			while @TempDateOffice <= @ToDate
			BEGIN	
			set @strTempMDate =convert(varchar,@TempDateOffice,23)
				IF DATEPART(MONTH, @TempDateOffice) < 10
				BEGIN
					set @TableName= '[TBLTIMEREGISTER_0'+CAST(DATEPART(month, @TempDateOffice) AS varchar)+CAST(DATEPART(year, @TempDateOffice) AS varchar)+']'
					set @SQL2 = 'INSERT INTO TempRsTime SELECT * FROM '+@TableName +' WHERE SSN='''+ @TempSSN +''' AND DateOFFICE ='''+@strTempMDate+'''';
					print(@SQL2)
					execute(@SQL2)
				END
				ELSE
				BEGIN
					set @TableName= '[TBLTIMEREGISTER_'+CAST(DATEPART(month, @TempDateOffice) AS varchar)+CAST(DATEPART(year, @TempDateOffice) AS varchar)+']'
					set @SQL2 = 'INSERT INTO TempRsTime SELECT * FROM '+@TableName +' WHERE SSN='''+ @TempSSN +''' AND DateOFFICE ='''+@strTempMDate+'''';
					print(@SQL2)
					execute(@SQL2)
				END
				
				set @TempDateOffice=@TempDateOffice+1;
			END

			INSERT INTO @TempRsTime
			SELECT  ROW_NUMBER() OVER (ORDER BY SSN) RowNumber,  * FROM TempRsTime(nolock)
			WHERE SSN=@TempSSN
			AND DateOFFICE BETWEEN @FromDate AND @ToDate
			ORDER BY DateOFFICE
						
			DELETE FROM TempRsTime

			---Change For multipal tbltimeregister
					
			--INSERT INTO @TempRsTime
			--SELECT  ROW_NUMBER() OVER (ORDER BY SSN) RowNumber,  * FROM tbltimeregister(nolock)
			--WHERE PAYCODE=@PayCode
			--AND DateOFFICE BETWEEN @FromDate AND @ToDate
			--ORDER BY DateOFFICE
					

			SET @TempRsTimeRecordCount = 0
			SELECT @TempRsTimeRecordCount = COUNT(*) 
			FROM @TempRsTime

			SET @TempMReasonCode = ''
			SET @TempFirstPunchReasonCode = ''
			SET @TempLastPunchReasonCode = ''

			SET @TempRsTimeRowNumber = 1
			While @TempRsTimeRowNumber <= @TempRsTimeRecordCount
			BEGIN

				SET @TempMIn1 = NULL
				SET @TempMIn2 = NULL
				SET @TempMOut1 = NULL
				SET @TempMOut2 = NULL
				SET @TempMShiftStartTime = NULL
				SET @TempMShiftEndTime = NULL
				SET @TempMLunchStartTime = NULL
				SET @TempMLunchEndTime = NULL

				SET @TempMIn1Manual = 'N'
				SET @TempMIn2Manual = 'N'
				SET @TempMOut1Manual = 'N'
				SET @TempMOut2Manual = 'N'


				SET @TempProcDate=''
				SET @TempMDate1=''
				SET @TempRsTimeDateOffice = ''
				SET @TempRsTimePayCode = ''
				SET @TempRsShiftAttended = ''
				SET @TempRsTimeShift = ''
			
				SELECT 
					@TempProcDate=DateOFFICE,
					@TempMDate1=DateOFFICE,
					@TempRsTimeDateOffice = DateOFFICE,
					@TempRsTimePayCode = PayCode,
					@TempRsShiftAttended = SHIFTATTENDED,
					@TempRsTimeShift = [SHIFT]
				FROM @TempRsTime
				WHERE RowNumber = @TempRsTimeRowNumber

				SET @TempProcDate = CAST(CONVERT(varchar(10), @TempProcDate, 101) AS DATE)
				-- TODO :
				-- If it is NonRTC , Don't add the 1 day
				-- Get the value of NonRTC from EmployeeShift Master
				IF @TempRsEmpIsRoundTheClock = 'Y'
				BEGIN
					SET @TempMDate2 = DATEADD(DAY,1, @TempMDate1)

					SET @TempTblSetupSOut = ''
					SET @TempTblSetupSEnd = ''

					SELECT 
						@TempTblSetupSOut = S_OUT,
						@TempTblSetupSEnd = S_END
					FROM tblsetup (nolock)	  
					WHERE SETUPID = @SetupId
					
					SET @TempMinutes = DATEDIFF(MINUTE, DATEADD(DAY, DATEDIFF(DAY, 0, @TempTblSetupSOut), 0), @TempTblSetupSOut)
					SET @TempMDate2 = DATEADD(HH, @TempMinutes/60, @TempMDate2)
					SET @TempMDate2 = DATEADD(MINUTE, @TempMinutes%60,@TempMDate2)
				END
				ELSE
				BEGIN	
				   	SELECT 
						@TempTblSetupSOut = S_OUT,
						@TempTblSetupSEnd = S_END
					FROM tblsetup	  (nolock)
					WHERE SETUPID = @SetupId
									    
					SET @TempMDate2 = DATEADD(DAY,1, @TempMDate1)
					SET @TempMDate2 = CAST(CAST(@TempMDate2 AS VARCHAR(12)) AS DATETIME)
					SET @TempMDate2 = DATEADD(MINUTE,-1,@TempMDate2)
				END

				DELETE FROM @TempRsPunch
				-- 
				IF @TempIsOutWork = 'Y' 
				BEGIN

				    SET @TempOutDate2DayHour = NULL
					SET @TempOutDate2DayHour = DATEADD(DAY,1,@TempRsTimeDateOffice)						
					SET @TempMinutes = DATEDIFF(MINUTE, DATEADD(DAY, DATEDIFF(DAY, 0, @TempTblSetupSOut), 0), @TempTblSetupSOut)
					SET @TempOutDate2DayHour = DATEADD(HH, @TempMinutes/60, @TempOutDate2DayHour)
					SET @TempOutDate2DayHour = DATEADD(MINUTE, @TempMinutes%60,@TempOutDate2DayHour)

				  -- TODO : @TempDate2 take the g_o_end into consideration
					INSERT INTO @TempRsPunch
					SELECT *, ROW_NUMBER() OVER (ORDER BY OfficePunch) RowNumber FROM MachineRawPunch (nolock)
					WHERE (OFFICEPUNCH BETWEEN CONVERT(DATETIME, CONVERT(varchar(11),@TempMDate1, 111 ) + ' 00:00:00', 111)
									   AND @TempOutDate2DayHour)
					AND p_day<>'Y' 
					AND PAYCODE = @PayCode
					Order By OfficePunch
				END
				ELSE
				BEGIN
				  
					INSERT INTO @TempRsPunch
					SELECT *, ROW_NUMBER() OVER (ORDER BY OfficePunch) RowNumber FROM MachineRawPunch (nolock)
					WHERE (OFFICEPUNCH BETWEEN CONVERT(DATETIME, CONVERT(varchar(11),@TempMDate1, 111 ) + ' 00:00:00', 111)
									   AND CONVERT(DATETIME, CONVERT(varchar(11),@TempMDate2, 111 ) + ' 23:59:59', 111) )
					AND p_day<>'Y' 
					AND PAYCODE = @PayCode
					Order By OfficePunch
				END
				
				IF @TempIsPunchAll = 'N'
				BEGIN
					GOTO AsgIt
				END
				SET @TempMRsPunchCount = 0
				SELECT @TempMRsPunchCount = COUNT(*) FROM @TempRsPunch

				IF @TempMRsPunchCount > 0 AND @TempIsOutWork = 'Y'
				BEGIN
					SET @TempMIn1 = ''
					SET @TempMDate2 = ''

					SET @TempMIn1 = (SELECT TOP 1 OFFICEPUNCH FROM @TempRsPunch)
					SET @TempMDate2 = DATEADD(MINUTE, @TempDayHour, @TempMIn1)
									
					SET @TempOutDate2DayHour = NULL
					SET @TempOutDate2DayHour = DATEADD(DAY,1,@TempRsTimeDateOffice)						
					SET @TempMinutes = DATEDIFF(MINUTE, DATEADD(DAY, DATEDIFF(DAY, 0, @TempTblSetupSOut), 0), @TempTblSetupSOut)
					SET @TempOutDate2DayHour = DATEADD(HH, @TempMinutes/60, @TempOutDate2DayHour)
					SET @TempOutDate2DayHour = DATEADD(MINUTE, @TempMinutes%60,@TempOutDate2DayHour)


					IF @TempMDate2 > @TempOutDate2DayHour
					BEGIN
					    SET @TempMinutes = 0
						SET @TempMDate2 = DATEADD(DAY,1,@TempRsTimeDateOffice)
						
						SET @TempMinutes = DATEDIFF(MINUTE, DATEADD(DAY, DATEDIFF(DAY, 0, @TempTblSetupSOut), 0), @TempTblSetupSOut)
						SET @TempMDate2 = DATEADD(HH, @TempMinutes/60, @TempMDate2)
						SET @TempMDate2 = DATEADD(MINUTE, @TempMinutes%60,@TempMDate2)
					END

					-- DELETE Old records from RsMachinPunch
					DELETE FROM @TempRsPunch

					INSERT INTO @TempRsPunch
					SELECT *, ROW_NUMBER() OVER (ORDER BY OfficePunch) RowNumber FROM MachineRawPunch (nolock)
					WHERE (OFFICEPUNCH BETWEEN @TempMIn1 AND @TempMDate2)
					AND p_day<>'Y' 
					AND PAYCODE = @PayCode
					Order By OFFICEPUNCH
				END

				SET @TempMRsPunchCount = 0
				SELECT @TempMRsPunchCount = COUNT(*) FROM @TempRsPunch

				SET @TempMIn1Manual = ''
				SET @TempReasonCode = ''
				SET @TempRsPunchOFficePunch = ''

				SET @TempMIn1Manual = (SELECT TOP 1 ISMANUAL FROM @TempRsPunch)
				SET @TempReasonCode = (SELECT TOP 1 ISNULL(ReasonCode,'') FROM @TempRsPunch)
				SET @TempRsPunchOFficePunch = (SELECT TOP 1 OFFICEPUNCH FROM @TempRsPunch)
				--SET @TempMOut2Manual = (SELECT TOP 1 ISMANUAL FROM @TempRsPunch Order By OFFICEPUNCH DESC)

				IF @TempMRsPunchCount > 2  AND @TempIsOutWork = 'Y'
				BEGIN
				    
					

					SET @TempMIn1 = @TempRsPunchOFficePunch
					SET @TempMDate1 = DATEADD(DAY,1,@TempRsTimeDateOffice)

					SET @TempMinutes = DATEDIFF(MINUTE, DATEADD(DAY, DATEDIFF(DAY, 0, @TempTblSetupSEnd), 0), @TempTblSetupSEnd)
					SET @TempMDate1 = DATEADD(HH, @TempMinutes/60, @TempMDate1)
					SET @TempMDate1 = DATEADD(MINUTE, @TempMinutes%60,@TempMDate1)

					IF DATEDIFF(MINUTE,@TempRsPunchOFficePunch,@TempMDate1) < 0
					BEGIN
						SET @TempMIn1 = NULL
						GOTO AsgIt
					END

					IF LEN(@TempReasonCode) <> 0
					BEGIN
						SET @TempMReasonCode = @TempReasonCode
						SET @TempMFirstPunchReasonCode = @TempReasonCode
					END
					ELSE
					BEGIN
						SET @TempMFirstPunchReasonCode = ''
					END

					IF DATEDIFF(DAY, @TempRsTimeDateOffice, @TempRsPunchOFficePunch) > 0
					BEGIN
					   UPDATE MachineRawPunch
					   SET P_Day='Y'
					   WHERE PAYCODE=@PayCode 
					   AND OFFICEPUNCH = @TempMIn1
					END

					SET @TempMOut2 = ''
					SET @TempMOut2Manual = ''
					SET @TempReasonCode = ''
					SET @TempRsPunchOFficePunch = ''

					-- Move to last record. Order by Desc
					SET @TempMOut2 = (SELECT TOP 1 OFFICEPUNCH FROM @TempRsPunch Order By OFFICEPUNCH DESC)
					SET @TempMOut2Manual = (SELECT TOP 1 ISMANUAL FROM @TempRsPunch Order By OFFICEPUNCH DESC)
					SET @TempReasonCode = (SELECT TOP 1 ISNULL(ReasonCode,'') FROM @TempRsPunch Order By OFFICEPUNCH DESC)
					SET @TempRsPunchOFficePunch = (SELECT TOP 1 OFFICEPUNCH FROM @TempRsPunch Order By OFFICEPUNCH DESC)

					IF LEN(@TempReasonCode) <> 0
					BEGIN	
						SET @TempMReasonCode = @TempReasonCode
						SET @TempLastPunchReasonCode = @TempReasonCode
					END
					ELSE
					BEGIN
						SET @TempLastPunchReasonCode = ''
					END


					IF DATEDIFF(DAY, @TempRsTimeDateOffice, @TempRsPunchOFficePunch) > 0
					BEGIN
					   UPDATE MachineRawPunch
					   SET P_Day='Y'
					   WHERE SSN=@TempSSN 
					   AND OFFICEPUNCH = @TempMOut2
					END

					SET @TempRsTOut = ''
					SET @TempRsTOut = (SELECT TOP 1 OFFICEPUNCH FROM @TempRsPunch Order By OFFICEPUNCH DESC)

					INSERT INTO @TempRsOutWork
					SELECT * FROM tblOutWorkRegister (nolock)
					WHERE PAYCODE = @PayCode
					AND DateOFFICE = @TempMDate1

					SET @TempRsOutRecordCount = 0

					SELECT @TempRsOutRecordCount =COUNT(*) FROM @TempRsOutWork

					IF @TempRsOutRecordCount < 1
					BEGIN
						SET @TempRsOutPayCode = @TempRsTimePaycode
						INSERT INTO tblOutWorkRegister
							(
								PAYCODE,
								SSN,
								DateOFFICE
							)
						VALUES
							(
								@TempRsOutPayCode,
								@TempSSN,
								@TempRsTimeDateOffice
							)
					END

					SET @TempRsPunchRowCount = 0
					SET @TempRsPunchCurrentRowIndex = 2
					SET @TempRsOutOutWorkDuration = 0
					SET @TempMReasonOutWork = 'N'
					SET @TempMCtr = 1

					SELECT @TempRsPunchRowCount = Count(*) FROM @TempRsPunch
					
					SET @TempRsOutIN1=NULL
					SET @TempRsOutIN2=NULL
					SET @TempRsOutIN3=NULL
					SET @TempRsOutIN4=NULL
					SET @TempRsOutIN5=NULL
					SET @TempRsOutIN6=NULL
					SET @TempRsOutIN7=NULL
					SET @TempRsOutIN8=NULL
					SET @TempRsOutIN9=NULL
					SET @TempRsOutIN10=NULL

					SET @TempRsOutRIN1=NULL
					SET @TempRsOutRIN2=NULL
					SET @TempRsOutRIN3=NULL
					SET @TempRsOutRIN4=NULL
					SET @TempRsOutRIN5=NULL
					SET @TempRsOutRIN6=NULL
					SET @TempRsOutRIN7=NULL
					SET @TempRsOutRIN8=NULL
					SET @TempRsOutRIN9=NULL
					SET @TempRsOutRIN10=NULL

					SET @TempRsOutOUT1=NULL
					SET @TempRsOutOUT2=NULL
					SET @TempRsOutOUT3=NULL
					SET @TempRsOutOUT4=NULL
					SET @TempRsOutOUT5=NULL
					SET @TempRsOutOUT6=NULL
					SET @TempRsOutOUT7=NULL
					SET @TempRsOutOUT8=NULL
					SET @TempRsOutOUT9=NULL
					SET @TempRsOutOUT10=NULL

					SET @TempRsOutROUT1=NULL
					SET @TempRsOutROUT2=NULL
					SET @TempRsOutROUT3=NULL
					SET @TempRsOutROUT4=NULL
					SET @TempRsOutROUT5=NULL
					SET @TempRsOutROUT6=NULL
					SET @TempRsOutROUT7=NULL
					SET @TempRsOutROUT8=NULL
					SET @TempRsOutROUT9=NULL
					SET @TempRsOutROUT10=NULL

					SET @TempMReasonOutWork=''
					SET @TempRsOutOutWorkDuration=0
					-- STEP  : Find OUTTime from Mutliple Rows
					WHILE @TempRsPunchCurrentRowIndex <=  @TempRsPunchRowCount
					BEGIN
					    
						SET @TempRsPunchTimeOut = ''
						SET @TempRsPunchReasonCode = ''

						SELECT @TempRsPunchTimeOut = OFFICEPUNCH,
								@TempRsPunchReasonCode = RTRIM(LTRIM(ISNULL(ReasonCode, '')))
					    FROM @TempRsPunch Where RowNumber = @TempRsPunchCurrentRowIndex

						IF @TempRsTOut = @TempRsPunchTimeOut
						BEGIN
							BREAK
						END

						IF @TempMCtr = 1
						BEGIN 
							SET @TempRsOutIN1  =  @TempRsPunchTimeOut	
						END

						IF @TempMCtr = 2
						BEGIN 
							SET @TempRsOutIN2  =  @TempRsPunchTimeOut	
						END

						IF @TempMCtr = 3
						BEGIN 
							SET @TempRsOutIN3  =  @TempRsPunchTimeOut	
						END

						IF @TempMCtr = 4
						BEGIN 
							SET @TempRsOutIN4  =  @TempRsPunchTimeOut	
						END

						IF @TempMCtr = 5
						BEGIN 
							SET @TempRsOutIN5  =  @TempRsPunchTimeOut	
						END

						IF @TempMCtr = 6
						BEGIN 
							SET @TempRsOutIN6  =  @TempRsPunchTimeOut	
						END

						IF @TempMCtr = 7
						BEGIN 
							SET @TempRsOutIN7  =  @TempRsPunchTimeOut	
						END

						IF @TempMCtr = 8
						BEGIN 
							SET @TempRsOutIN8  =  @TempRsPunchTimeOut	
						END

						IF @TempMCtr = 9
						BEGIN 
							SET @TempRsOutIN9  =  @TempRsPunchTimeOut	
						END

						IF @TempMCtr = 10
						BEGIN 
							SET @TempRsOutIN10  =  @TempRsPunchTimeOut	
						END

						IF @TempRsPunchReasonCode <> ''
						BEGIN
							IF @TempMCtr = 1
							BEGIN 
								SET @TempRsOutRIN1 = @TempRsPunchReasonCode
							END

							IF @TempMCtr = 2
							BEGIN 
								SET @TempRsOutRIN2 = @TempRsPunchReasonCode
							END

							IF @TempMCtr = 3
							BEGIN 
								SET @TempRsOutRIN3 = @TempRsPunchReasonCode
							END

							IF @TempMCtr = 4
							BEGIN 
								SET @TempRsOutRIN4 = @TempRsPunchReasonCode
							END

							IF @TempMCtr = 5
							BEGIN 
								SET @TempRsOutRIN5 = @TempRsPunchReasonCode
							END

							IF @TempMCtr = 6
							BEGIN 
								SET @TempRsOutRIN6 = @TempRsPunchReasonCode
							END

							IF @TempMCtr = 7
							BEGIN 
								SET @TempRsOutRIN7 = @TempRsPunchReasonCode
							END

							IF @TempMCtr = 8
							BEGIN 
								SET @TempRsOutRIN8 = @TempRsPunchReasonCode
							END

							IF @TempMCtr = 9
							BEGIN 
								SET @TempRsOutRIN9 = @TempRsPunchReasonCode
							END

							IF @TempMCtr = 10
							BEGIN 
								SET @TempRsOutRIN10 = @TempRsPunchReasonCode
							END
							SET @TempMReasonCode = @TempRsPunchReasonCode

							IF(LEN(@TempRsPunchReasonCode) > 0)
							BEGIN
								SET @TempMReasonOutWork = 'Y'
							END
							ELSE
							BEGIN
								SET  @TempMReasonOutWork = 'N'
							END
						END	

						IF DATEDIFF(D,@TempRsTimeDateOffice, @TempRsPunchTimeOut) > 0
						BEGIN
							UPDATE MachineRawPunch SET P_DAY = 'Y'
							WHERE SSN = @TempSSN 
							AND OFFICEPUNCH = @TempRsPunchTimeOut							
						END

						SET @TempRsPunchCurrentRowIndex = @TempRsPunchCurrentRowIndex + 1

						IF @TempRsPunchCurrentRowIndex > @TempRsPunchRowCount
						BEGIN
							BREAK
						END

						IF @TempRsPunchCurrentRowIndex = @TempRsPunchRowCount
						BEGIN
							BREAK
						END

						SET @TempRsPunchTimeOut = ''
						SET @TempRsPunchReasonCode = ''

						SELECT @TempRsPunchTimeOut = OFFICEPUNCH,
							   @TempRsPunchReasonCode = RTRIM(LTRIM(ISNULL(ReasonCode, '')))
						FROM @TempRsPunch Where RowNumber = @TempRsPunchCurrentRowIndex
						
						IF @TempMCtr = 1
						BEGIN 
							SET @TempRsOutOUT1  =  @TempRsPunchTimeOut	
							SET @TempRsOutOutWorkDuration =  @TempRsOutOutWorkDuration + DATEDIFF(N, @TempRsOutIN1 , @TempRsOutOUT1)
						END

						IF @TempMCtr = 2
						BEGIN 
							SET @TempRsOutOUT2  =  @TempRsPunchTimeOut	
							SET @TempRsOutOutWorkDuration =  @TempRsOutOutWorkDuration + DATEDIFF(N, @TempRsOutIN2 , @TempRsOutOUT2)
						END

						IF @TempMCtr = 3
						BEGIN 
							SET @TempRsOutOUT3  =  @TempRsPunchTimeOut	
							SET @TempRsOutOutWorkDuration =  @TempRsOutOutWorkDuration + DATEDIFF(N, @TempRsOutIN3 , @TempRsOutOUT3)
						END

						IF @TempMCtr = 4
						BEGIN 
							SET @TempRsOutOUT4  =  @TempRsPunchTimeOut	
							SET @TempRsOutOutWorkDuration =  @TempRsOutOutWorkDuration + DATEDIFF(N, @TempRsOutIN4 , @TempRsOutOUT4)
						END

						IF @TempMCtr = 5
						BEGIN 
							SET @TempRsOutOUT5  =  @TempRsPunchTimeOut
							SET @TempRsOutOutWorkDuration =  @TempRsOutOutWorkDuration + DATEDIFF(N, @TempRsOutIN5 , @TempRsOutOUT5)	
						END

						IF @TempMCtr = 6
						BEGIN 
							SET @TempRsOutOUT6  =  @TempRsPunchTimeOut
							SET @TempRsOutOutWorkDuration =  @TempRsOutOutWorkDuration + DATEDIFF(N, @TempRsOutIN6, @TempRsOutOUT6)	
						END

						IF @TempMCtr = 7
						BEGIN 
							SET @TempRsOutOUT7  =  @TempRsPunchTimeOut
							SET @TempRsOutOutWorkDuration =  @TempRsOutOutWorkDuration + DATEDIFF(N, @TempRsOutIN7 , @TempRsOutOUT7)	
						END

						IF @TempMCtr = 8
						BEGIN 
							SET @TempRsOutOUT8  =  @TempRsPunchTimeOut	
							SET @TempRsOutOutWorkDuration =  @TempRsOutOutWorkDuration + DATEDIFF(N, @TempRsOutIN8 , @TempRsOutOUT8)
						END

						IF @TempMCtr = 9
						BEGIN 
							SET @TempRsOutOUT9  =  @TempRsPunchTimeOut	
							SET @TempRsOutOutWorkDuration =  @TempRsOutOutWorkDuration + DATEDIFF(N, @TempRsOutIN9, @TempRsOutOUT9)
						END

						IF @TempMCtr = 10
						BEGIN 
							SET @TempRsOutOUT10  =  @TempRsPunchTimeOut	
							SET @TempRsOutOutWorkDuration =  @TempRsOutOutWorkDuration + DATEDIFF(N, @TempRsOutIN10, @TempRsOutOUT10)
						END


						IF @TempRsPunchReasonCode <> ''
						BEGIN
							IF @TempMCtr = 1
							BEGIN 
								SET @TempRsOutROUT1 = @TempRsPunchReasonCode
							END

							IF @TempMCtr = 2
							BEGIN 
								SET @TempRsOutROUT2 = @TempRsPunchReasonCode
							END

							IF @TempMCtr = 3
							BEGIN 
								SET @TempRsOutROUT3 = @TempRsPunchReasonCode
							END

							IF @TempMCtr = 4
							BEGIN 
								SET @TempRsOutROUT4 = @TempRsPunchReasonCode
							END

							IF @TempMCtr = 5
							BEGIN 
								SET @TempRsOutROUT5 = @TempRsPunchReasonCode
							END

							IF @TempMCtr = 6
							BEGIN 
								SET @TempRsOutROUT6 = @TempRsPunchReasonCode
							END

							IF @TempMCtr = 7
							BEGIN 
								SET @TempRsOutROUT7 = @TempRsPunchReasonCode
							END

							IF @TempMCtr = 8
							BEGIN 
								SET @TempRsOutROUT8 = @TempRsPunchReasonCode
							END

							IF @TempMCtr = 9
							BEGIN 
								SET @TempRsOutROUT9 = @TempRsPunchReasonCode
							END

							IF @TempMCtr = 10
							BEGIN 
								SET @TempRsOutROUT10 = @TempRsPunchReasonCode
							END
							SET @TempMReasonCode = @TempRsPunchReasonCode

							IF(LEN(@TempRsPunchReasonCode) > 0)
							BEGIN
								SET @TempMReasonOutWork = 'Y'
							END
							ELSE
							BEGIN
								SET  @TempMReasonOutWork = 'N'
							END
						END	

						
						IF DATEDIFF(D,@TempRsTimeDateOffice, @TempRsPunchTimeOut) > 0
						BEGIN
							UPDATE MachineRawPunch SET P_DAY = 'Y'
							WHERE PAYCODE = @PayCode 
							AND OFFICEPUNCH = @TempRsPunchTimeOut							
						END

						SET @TempRsPunchCurrentRowIndex = @TempRsPunchCurrentRowIndex + 1
						SET @TempMCtr = @TempMCtr + 1

					END

					-- TODO
					-- INSERT this record first. Instead of UPDATE
					UPDATE tblOutWorkRegister
					SET 
						IN1 = @TempRsOutIN1,
						IN2 = @TempRsOutIN2,
						IN3 = @TempRsOutIN3,
						IN4 = @TempRsOutIN4,
						IN5 = @TempRsOutIN5,
						IN6 = @TempRsOutIN6,
						IN7 = @TempRsOutIN7,
						IN8 = @TempRsOutIN8,
						IN9 = @TempRsOutIN9,
						IN10 = @TempRsOutIN10,
						RIN1 = @TempRsOutRIN1,
						RIN2 = @TempRsOutRIN2,
						RIN3 = @TempRsOutRIN3,
						RIN4 = @TempRsOutRIN4,
						RIN5 = @TempRsOutRIN5,
						RIN6 = @TempRsOutRIN6,
						RIN7 = @TempRsOutRIN7,
						RIN8 = @TempRsOutRIN8,
						RIN9 = @TempRsOutRIN9,
						RIN10 = @TempRsOutRIN10,
						OUT1 = @TempRsOutOUT1,
						OUT2 = @TempRsOutOUT2,
						OUT3 = @TempRsOutOUT3,
						OUT4 = @TempRsOutOUT4,
						OUT5 = @TempRsOutOUT5,
						OUT6 = @TempRsOutOUT6,
						OUT7 = @TempRsOutOUT7,
						OUT8 = @TempRsOutOUT8,
						OUT9 = @TempRsOutOUT9,
						OUT10 = @TempRsOutOUT10,
						ROUT1 = @TempRsOutROUT1,
						ROUT2 = @TempRsOutROUT2,
						ROUT3 = @TempRsOutROUT3,
						ROUT4 = @TempRsOutROUT4,
						ROUT5 = @TempRsOutROUT5,
						ROUT6 = @TempRsOutROUT6,
						ROUT7 = @TempRsOutROUT7,
						ROUT8 = @TempRsOutROUT8,
						ROUT9 = @TempRsOutROUT9,
						ROUT10 = @TempRsOutROUT10,
						Reason_OutWork = @TempMReasonOutWork,
						OUTWORKDURATION = @TempRsOutOutWorkDuration,
						PAYCODE = @TempRsOutPayCode,
						DateOFFICE =  @TempRsTimeDateOffice
					WHERE PAYCODE = @PayCode
					AND DateOFFICE = @TempRsTimeDateOffice
					
					GOTO AsgIt
				END

				IF @TempIsOutWork = 'Y' and   @TempMRsPunchCount > 0					
				BEGIN
					SET @TempRsPunchCurrentRowIndex = 1
					SET @TempRsPunchCurrentRowIndex2 = 1
					SET @IsPreviousDaySet = 0
										
					WHILE @TempRsPunchCurrentRowIndex <= @TempMRsPunchCount
					BEGIN
					    SET @TempReasonCode = ''
						SET @TempRsPunchOFficePunch = ''
						SET @TempRSPunchPDay = ''

						SELECT 
							@TempReasonCode = ISNULL(ReasonCode,''),
							@TempRsPunchOFficePunch = OFFICEPUNCH,
							@TempRSPunchPDay = P_DAY
						FROM @TempRsPunch 
						WHERE RowNumber = @TempRsPunchCurrentRowIndex2

						IF @TempRSPunchPDay = 'Y' AND DATEDIFF(D, @TempRsPunchOFficePunch, @TempMDate1) = 0
						BEGIN
							
							IF @TempRsPunchCurrentRowIndex > @TempMRsPunchCount
							BEGIN
							    SET @TempRsPunchCurrentRowIndex = @TempRsPunchCurrentRowIndex + 1
								SET @IsPreviousDaySet = 1
								BREAK
							END
						END
						SET @TempRsPunchCurrentRowIndex = @TempRsPunchCurrentRowIndex + 1
						SET @TempRsPunchCurrentRowIndex2 = @TempRsPunchCurrentRowIndex2 + 1
					END

					--IF @TempRsPunchCurrentRowIndex > @TempMRsPunchCount
					--BEGIN
					--	GOTO AsgIt
					--END	
					

					IF @IsPreviousDaySet = 1
					BEGIN 
						GOTO AsgIt
					END
					SET @TempReasonCode = ''
					SET @TempRsPunchOFficePunch = ''
					SET @TempRSPunchPDay = ''

					SET @TempRsPunchCurrentRowIndex = 1
					SELECT 
							@TempReasonCode = ISNULL(ReasonCode,''),
							@TempRsPunchOFficePunch = OFFICEPUNCH,
							@TempRSPunchPDay = ISNULL(P_DAY,'')
					FROM @TempRsPunch 
					WHERE RowNumber = @TempRsPunchCurrentRowIndex

					SET @TempMDate1 = DATEADD(D, 1, @TempRsTimeDateOffice)
					
					SET @TempMinutes = DATEDIFF(MINUTE, DATEADD(DAY, DATEDIFF(DAY, 0, @TempTblSetupSEnd), 0), @TempTblSetupSEnd)
					SET @TempMDate1 = DATEADD(HH, @TempMinutes/60, @TempMDate1)
					SET @TempMDate1 = DATEADD(MINUTE, @TempMinutes%60,@TempMDate1)

					IF DATEDIFF(N, @TempRsPunchOFficePunch, @TempMDate1) < 0
					BEGIN
						SET @TempMIn1 = NULL
						GOTO AsgIt
					END

					IF DATEDIFF(N, @TempRsPunchOFficePunch, @TempMDate1) >= 0
					BEGIN
						SET @TempMIn1 = @TempRsPunchOFficePunch

						IF LEN(RTRIM(LTRIM(@TempReasonCode))) > 0
						BEGIN
							SET @TempMReasonCode = @TempReasonCode
							SET @TempFirstPunchReasonCode = @TempReasonCode
						END
						ELSE
						BEGIN
							SET @TempMReasonCode = ''
							SET @TempFirstPunchReasonCode = ''
						END

						IF DATEDIFF(D, @TempRsTimeDateOffice, @TempRsPunchOFficePunch) > 0
						BEGIN
							SET @TempMPFlg = 'Y'							
						END
						ELSE
						BEGIN
							SET @TempMPFlg = 'N'
						END

						IF RTRIM(LTRIM(@TempRSPunchPDay)) = ''
						BEGIN
							SET @TempExecuteMachineRawUpdate = 1
						END
						ELSE IF @TempMPFlg <> RTRIM(LTRIM(@TempRSPunchPDay)) 
						BEGIN
							SET @TempExecuteMachineRawUpdate = 1
						END

						IF @TempExecuteMachineRawUpdate = 1
						BEGIN
							UPDATE MachineRawPunch 
							Set P_Day=@TempMPFlg 
							Where PAYCODE=@PayCode
							AND OfficePunch = @TempMIn1
						END
					END
					ELSE
					BEGIN
						GOTO AsgIt
					END

					SET @TempRsPunchCurrentRowIndex = @TempRsPunchCurrentRowIndex + 1
					IF @TempRsPunchCurrentRowIndex > @TempMRsPunchCount
					BEGIN
						GOTO AsgIt
					END	

					SET @TempReasonCode = ''
					SET @TempRsPunchOFficePunch = ''
					SET @TempRSPunchPDay = ''

					SELECT 
						@TempReasonCode = ISNULL(ReasonCode,''),
						@TempRsPunchOFficePunch = OFFICEPUNCH,
						@TempRSPunchPDay = ISNULL(P_DAY,'')
					FROM @TempRsPunch 
					WHERE RowNumber = @TempRsPunchCurrentRowIndex

					SET @TempMOut1 = @TempRsPunchOFficePunch

					IF LEN(RTRIM(LTRIM(@TempReasonCode))) > 0
					BEGIN
						SET @TempMReasonCode = @TempReasonCode
						SET @TempFirstPunchReasonCode = @TempReasonCode
					END
					ELSE
					BEGIN
					    IF LEN(LTRIM(RTRIM(@TempMReasonCode))) <= 0
						BEGIN
							SET @TempMReasonCode = ''
							SET @TempFirstPunchReasonCode = ''
						END
					END

					IF DATEDIFF(D, @TempRsTimeDateOffice, @TempRsPunchOFficePunch) > 0
					BEGIN
						SET @TempMPFlg = 'Y'							
					END
					ELSE
					BEGIN
						SET @TempMPFlg = 'N'
					END

					IF RTRIM(LTRIM(@TempRSPunchPDay)) = ''
					BEGIN
						SET @TempExecuteMachineRawUpdate = 1
					END
					ELSE IF @TempMPFlg <> RTRIM(LTRIM(@TempRSPunchPDay)) 
					BEGIN
						SET @TempExecuteMachineRawUpdate = 1
					END

					IF @TempExecuteMachineRawUpdate = 1
					BEGIN
						UPDATE MachineRawPunch 
						Set P_Day=@TempMPFlg 
						Where PAYCODE=@PayCode
						AND OfficePunch = @TempMOut1
					END
					GoTo AsgIt	
				 END				

				IF   @TempMRsPunchCount > 0
				BEGIN
					SET @TempRsPunchCurrentRowIndex = 1
					SET @TempRsPunchCurrentRowIndex2 = 1
					SET @IsPreviousDaySet = 0					
					WHILE @TempRsPunchCurrentRowIndex <= @TempMRsPunchCount
					BEGIN
					    
						SET @TempReasonCode = ''
						SET @TempRsPunchOFficePunch = ''
						SET @TempRSPunchPDay = ''

						SELECT 
							@TempReasonCode = ISNULL(ReasonCode,''),
							@TempRsPunchOFficePunch = OFFICEPUNCH,
							@TempRSPunchPDay = P_DAY
						FROM @TempRsPunch 
						WHERE RowNumber = @TempRsPunchCurrentRowIndex2

						IF @TempRSPunchPDay = 'Y' AND DATEDIFF(D, @TempRsPunchOFficePunch, @TempMDate1) = 0
						BEGIN
							
							IF @TempRsPunchCurrentRowIndex > @TempMRsPunchCount
							BEGIN
							    SET @IsPreviousDaySet = 1
							    SET @TempRsPunchCurrentRowIndex = @TempRsPunchCurrentRowIndex + 1
								BREAK
							END
						END
						SET @TempRsPunchCurrentRowIndex = @TempRsPunchCurrentRowIndex + 1
						SET @TempRsPunchCurrentRowIndex2 = @TempRsPunchCurrentRowIndex2 + 1
					END

					--IF @TempRsPunchCurrentRowIndex > @TempMRsPunchCount
					--BEGIN
					--	GOTO AsgIt
					--END
					
					IF @IsPreviousDaySet = 1
					BEGIN 
						GOTO AsgIt
					END
					SET @TempReasonCode = ''
					SET @TempRsPunchOFficePunch = ''
					SET @TempRSPunchPDay = ''	
					
					SET @TempRsPunchCurrentRowIndex = 1
					SELECT 
							@TempReasonCode = ISNULL(ReasonCode,''),
							@TempRsPunchOFficePunch = OFFICEPUNCH,
							@TempRSPunchPDay = ISNULL(P_DAY,'')
					FROM @TempRsPunch 
					WHERE RowNumber = @TempRsPunchCurrentRowIndex

					SET @TempMDate1 = DATEADD(D, 1, @TempRsTimeDateOffice)
					
					SET @TempMinutes = DATEDIFF(MINUTE, DATEADD(DAY, DATEDIFF(DAY, 0, @TempTblSetupSEnd), 0), @TempTblSetupSEnd)
					SET @TempMDate1 = DATEADD(HH, @TempMinutes/60, @TempMDate1)
					SET @TempMDate1 = DATEADD(MINUTE, @TempMinutes%60, @TempMDate1)

					IF DATEDIFF(N, @TempRsPunchOFficePunch, @TempMDate1) < 0
					BEGIN
						SET @TempMIn1 = NULL
						GOTO AsgIt
					END

					IF DATEDIFF(N, @TempRsPunchOFficePunch, @TempMDate1) >= 0
					BEGIN
						SET @TempMIn1 = @TempRsPunchOFficePunch

						IF LEN(LTRIM(RTRIM(@TempReasonCode))) > 0
						BEGIN
							SET @TempMReasonCode = @TempReasonCode
							SET @TempFirstPunchReasonCode = @TempReasonCode
						END
						ELSE
						BEGIN
							SET @TempMReasonCode = ''
							SET @TempFirstPunchReasonCode = ''
						END

						IF DATEDIFF(D, @TempRsTimeDateOffice, @TempRsPunchOFficePunch) > 0
						BEGIN
							SET @TempMPFlg = 'Y'							
						END
						ELSE
						BEGIN
							SET @TempMPFlg = 'N'
						END

						IF RTRIM(LTRIM(@TempRSPunchPDay)) = ''
						BEGIN
							SET @TempExecuteMachineRawUpdate = 1
						END
						ELSE IF @TempMPFlg <> RTRIM(LTRIM(@TempRSPunchPDay)) 
						BEGIN
							SET @TempExecuteMachineRawUpdate = 1
						END

						IF @TempExecuteMachineRawUpdate = 1
						BEGIN
							UPDATE MachineRawPunch 
							Set P_Day=@TempMPFlg 
							Where PAYCODE=@PayCode
							AND OfficePunch = @TempMIn1
						END
					END
					ELSE
					BEGIN
						GOTO AsgIt
					END


					SET @TempMDate1 = DATEADD(N, @TempDayHour, @TempRsPunchOFficePunch)
					SET @TempRsPunchCurrentRowIndex = @TempRsPunchCurrentRowIndex + 1
					IF @TempRsPunchCurrentRowIndex > @TempMRsPunchCount
					BEGIN
						GOTO AsgIt
					END	

					SET @TempReasonCode = ''
					SET @TempRsPunchOFficePunch = ''
					SET @TempRSPunchPDay = ''

					SELECT 
						@TempReasonCode = ISNULL(ReasonCode,''),
						@TempRsPunchOFficePunch = OFFICEPUNCH,
						@TempRSPunchPDay = ISNULL(P_DAY,'')
					FROM @TempRsPunch 
					WHERE RowNumber = @TempRsPunchCurrentRowIndex

					IF DATEDIFF(N,@TempRsPunchOFficePunch,@TempMDate1) >=0 
					BEGIN
						SET @TempMOut1 = @TempRsPunchOFficePunch

						IF LEN(RTRIM(LTRIM(@TempReasonCode))) > 0
					BEGIN	
						SET @TempMReasonCode = @TempReasonCode
						SET @TempLastPunchReasonCode = @TempReasonCode
					END
					ELSE
					BEGIN
					    IF LEN(RTRIM(LTRIM(@TempMReasonCode))) <= 0
						BEGIN
							SET @TempMReasonCode = ''
							SET @TempLastPunchReasonCode = ''
						END
					END

						IF DATEDIFF(D, @TempRsTimeDateOffice, @TempRsPunchOFficePunch) > 0
					BEGIN
						SET @TempMPFlg = 'Y'							
					END
					ELSE
					BEGIN
						SET @TempMPFlg = 'N'
					END

						IF RTRIM(LTRIM(@TempRSPunchPDay)) = ''
					BEGIN
						SET @TempExecuteMachineRawUpdate = 1
					END
					ELSE IF @TempMPFlg <> RTRIM(LTRIM(@TempRSPunchPDay)) 
					BEGIN
						SET @TempExecuteMachineRawUpdate = 1
					END

						IF @TempExecuteMachineRawUpdate = 1
					BEGIN
						UPDATE MachineRawPunch 
						Set P_Day=@TempMPFlg 
						Where PAYCODE=@PayCode
						AND OfficePunch = @TempMOut1
					END								
				
				ELSE
				BEGIN
					GOTO AsgIt
				END
					END
					ELSE
					BEGIN
						GOTO AsgIT
					END

					IF @TempGNightShiftFourPunch = 'Y'
					BEGIN
						GOTO AsgIt
					END

					IF @TempRsPunchCurrentRowIndex <= @TempMRsPunchCount AND @TempRsEmpTwo <> 'Y' AND @TempRsEmpInOnly = 'N'
					BEGIN
						SET @TempRsPunchCurrentRowIndex = @TempRsPunchCurrentRowIndex + 1

						IF @TempRsPunchCurrentRowIndex > @TempMRsPunchCount
						BEGIN
							GOTO AsgIt
						END

						SET @TempReasonCode = ''
						SET @TempRsPunchOFficePunch = ''
						SET @TempRSPunchPDay = ''
						SELECT 
								@TempReasonCode = ISNULL(ReasonCode,''),
								@TempRsPunchOFficePunch = OFFICEPUNCH,
								@TempRSPunchPDay = ISNULL(P_DAY,'')
						FROM @TempRsPunch 
						WHERE RowNumber = @TempRsPunchCurrentRowIndex

						IF DATEDIFF(D, @TempRsTimeDateOffice, @TempRsPunchOFficePunch)  > 0 AND @TempGNightShiftFourPunch = 'N'
						BEGIN
							GOTO AsgIt
						END

						IF DATEDIFF(N, @TempRsPunchOFficePunch, @TempMDate1) >= 0
						BEGIN
							SET @TempMIn2 = @TempRsPunchOFficePunch

							IF LEN(LTRIM(RTRIM(@TempReasonCode))) > 0
							BEGIN
								IF LEN(RTRIM(LTRIM(@TempMReasonCode))) <= 0
								BEGIN
									SET @TempMReasonCode = @TempReasonCode								
								END
							END
							ELSE
							BEGIN
								IF LEN(RTRIM(LTRIM(@TempMReasonCode))) <= 0
								BEGIN
									SET @TempMReasonCode = ''
								END
							END

							IF DATEDIFF(D, @TempRsTimeDateOffice, @TempRsPunchOFficePunch) > 0
							BEGIN
								SET @TempMPFlg = 'Y'							
							END
							ELSE
							BEGIN
								SET @TempMPFlg = 'N'
							END

							IF RTRIM(LTRIM(@TempRSPunchPDay)) = ''
							BEGIN
								SET @TempExecuteMachineRawUpdate = 1
							END
							ELSE IF @TempMPFlg <> RTRIM(LTRIM(@TempRSPunchPDay)) 
							BEGIN
								SET @TempExecuteMachineRawUpdate = 1
							END

							IF @TempExecuteMachineRawUpdate = 1
							BEGIN
								UPDATE MachineRawPunch 
								Set P_Day=@TempMPFlg 
								Where SSN=@TempSSN
								AND OfficePunch = @TempMIn2
							END
						END
						ELSE
						BEGIN
							GOTO AsgIt
						END
					
						SET @TempRsPunchCurrentRowIndex = @TempRsPunchCurrentRowIndex + 1

						IF @TempRsPunchCurrentRowIndex > @TempMRsPunchCount
						BEGIN
							GOTO AsgIt
						END

						SET @TempReasonCode = ''
						SET @TempRsPunchOFficePunch = ''
						SET @TempRSPunchPDay = ''
						SELECT 
								@TempReasonCode = ISNULL(ReasonCode,''),
								@TempRsPunchOFficePunch = OFFICEPUNCH,
								@TempRSPunchPDay = ISNULL(P_DAY,'')
						FROM @TempRsPunch 
						WHERE RowNumber = @TempRsPunchCurrentRowIndex

						IF DATEDIFF(N, @TempRsPunchOFficePunch, @TempMDate1) >= 0
						BEGIN
							SET @TempMOut2 = @TempRsPunchOFficePunch

							IF LEN(LTRIM(RTRIM(@TempReasonCode))) > 0
							BEGIN
								IF LEN(RTRIM(LTRIM(@TempMReasonCode))) <= 0
								BEGIN
									SET @TempMReasonCode = @TempReasonCode
									SET @TempLastPunchReasonCode = @TempReasonCode	
								END
							END
							ELSE
							BEGIN
								IF LEN(RTRIM(LTRIM(@TempMReasonCode))) <= 0
								BEGIN
									SET @TempMReasonCode = ''
									SET @TempLastPunchReasonCode = ''	
								END
							END

							IF DATEDIFF(D, @TempRsTimeDateOffice, @TempRsPunchOFficePunch) > 0
							BEGIN
								SET @TempMPFlg = 'Y'							
							END
							ELSE
							BEGIN
								SET @TempMPFlg = 'N'
							END

							IF RTRIM(LTRIM(@TempRSPunchPDay)) = ''
							BEGIN
								SET @TempExecuteMachineRawUpdate = 1
							END
							ELSE IF @TempMPFlg <> RTRIM(LTRIM(@TempRSPunchPDay)) 
							BEGIN
								SET @TempExecuteMachineRawUpdate = 1
							END

							IF @TempExecuteMachineRawUpdate = 1
							BEGIN
								UPDATE MachineRawPunch 
								Set P_Day=@TempMPFlg 
								Where SSN=@TempSSN
								AND OfficePunch = @TempMOut2
							END
						END
				    END
				
				END
			AsgIt:
				

				IF @TempMOut2 IS NULL AND @TempMOut1 IS NOT NULL AND @TempMIn2 IS NULL 
				BEGIN
					SET @TempMOut2  = @TempMOut1
					SET @TempMOut1 = NULL
				END	

				SET @HolidayCount = 0

				SELECT @HolidayCount = COUNT(*) FROM Holiday(nolock) 
				WHERE DEPARTMENTCODE = @TempDepartmentCode 
				AND CompanyCode = @CompanyCode 
				AND  CAST(CONVERT(varchar(10), HDate, 101) AS DATE) = CAST(CONVERT(varchar(10), @TempRsTimeDateOffice, 101) AS DATE)  
		
				IF @HolidayCount > 0
				BEGIN 
					SET @IsHoliday = 1
				END
				ELSE
				BEGIN 
					SET @IsHoliday = 0
				END

				--SET @TempMRsShiftAttended = 

				---Change For multipal tbltimeregister

				EXEC [dbo].[ProcessDecideShift] @TempRsShiftAttended, @IsHoliday,@TempRSEmpAuthShift, @SetupId, @TempRsTimeDateOffice, @TempMIn1, @TempMIn2, @TempMOut1, @TempMOut2, @CompanyCode, @TempRsTimeShift, @AutoShift
				--EXEC [dbo].[ProcessPostLeaveThruReason] @TempMReasonCode, @TempMFirstPunchReasonCode, @TempLastPunchReasonCode, @TempPayCode, @TempRsTimeDateOffice
							
			set @TempDateOffice=@FromDate;
			set @FromMonth=(DATEPART(month, @FromDate))
			set @ToMonth=(DATEPART(month, @FromDate))	
						
			--DELETE FROM TempRsTime

			---Change For multipal tbltimeregister

				EXEC [dbo].[ProcessDecideShift] @TempRsShiftAttended, @IsHoliday,@TempRSEmpAuthShift, @SetupId, @TempRsTimeDateOffice, @TempMIn1, @TempMIn2, @TempMOut1, @TempMOut2, @CompanyCode, @TempRsTimeShift, @AutoShift
				--EXEC [dbo].[ProcessPostLeaveThruReason] @TempMReasonCode, @TempMFirstPunchReasonCode, @TempLastPunchReasonCode, @TempPayCode, @TempRsTimeDateOffice
				DELETE FROM TempLeaveTimeregister
				IF DATEPART(month, @TempDateOffice1) < 10
				BEGIN
					set @TableName= '[TBLTIMEREGISTER_0'+CAST(DATEPART(month, @TempRsTimeDateOffice) AS varchar)+CAST(DATEPART(year, @TempRsTimeDateOffice) AS varchar)+']'
					--set @SQL2 = 'INSERT INTO TempLeaveTimeregister SELECT PAYCODE,DateOFFICE,LEAVEAMOUNT,LEAVETYPE1,LEAVETYPE2,LeaveCode,LEAVETYPE,FIRSTHALFLEAVECODE,SECONDHALFLEAVECODE FROM '+@TableName +' WHERE SSN='''+ @TempSSN +''' AND DateOFFICE ='+@TempRsTimeDateOffice+'';
					set @SQL2 = 'INSERT INTO TempLeaveTimeregister SELECT PAYCODE,SSN,DateOFFICE,LEAVEAMOUNT,LEAVETYPE1,LEAVETYPE2,LeaveCode,LEAVETYPE,FIRSTHALFLEAVECODE,SECONDHALFLEAVECODE FROM '+@TableName +' WHERE SSN='''+ @TempSSN +''' AND DateOFFICE ='''+@strTempMDate+'''';
					print(@SQL2)
					execute(@SQL2)
				END
				ELSE
				BEGIN
					set @TableName= '[TBLTIMEREGISTER_'+CAST(DATEPART(month, @TempRsTimeDateOffice) AS varchar)+CAST(DATEPART(year, @TempRsTimeDateOffice) AS varchar)+']'
					--set @SQL2 = 'INSERT INTO TempLeaveTimeregister SELECT PAYCODE,DateOFFICE,LEAVEAMOUNT,LEAVETYPE1,LEAVETYPE2,LeaveCode,LEAVETYPE,FIRSTHALFLEAVECODE,SECONDHALFLEAVECODE FROM '+@TableName +' WHERE SSN='''+ @TempSSN +''' AND DateOFFICE ='+@TempRsTimeDateOffice+'';
					set @SQL2 = 'INSERT INTO TempLeaveTimeregister SELECT PAYCODE,SSN,DateOFFICE,LEAVEAMOUNT,LEAVETYPE1,LEAVETYPE2,LeaveCode,LEAVETYPE,FIRSTHALFLEAVECODE,SECONDHALFLEAVECODE FROM '+@TableName +' WHERE SSN='''+ @TempSSN +''' AND DateOFFICE ='''+@strTempMDate+'''';
					print(@SQL2)
					execute(@SQL2)
				END

				SET @TempMRsTimeLeaveAmount = 0.0
				SET @TempMRsTimeLeaveType1 = ''
				SET @TempMRsTimeLeaveType2 = ''
				SET @TempMRsTimeLeaveCode = ''
				SET @TempMRsTimeLeaveType = ''
				SET @TempMRsTimeFirstHalfLeaveCode = ''
				SET @TempMRsTimeSecondHalfLeaveCode = ''
					 
				SELECT 
					@TempMRsTimeLeaveAmount  = LEAVEAMOUNT,
					@TempMRsTimeLeaveType1 = LEAVETYPE1,
					@TempMRsTimeLeaveType2 = LEAVETYPE2,
					@TempMRsTimeLeaveCode = LeaveCode,
					@TempMRsTimeLeaveType = LEAVETYPE,
					@TempMRsTimeFirstHalfLeaveCode = FIRSTHALFLEAVECODE,
					@TempMRsTimeSecondHalfLeaveCode = SECONDHALFLEAVECODE
				FROM TempLeaveTimeregister
				WHERE SSN= @TempSSN
				AND DateOFFICE = @TempRsTimeDateOffice
						

				--delete from TempLeaveTimeregister WHERE SSN= @TempSSN AND DateOFFICE = @TempRsTimeDateOffice

				--EXEC [dbo].[ProcessUPD2] @SetupId, @AutoShift, @CompanyCode, @TempDepartmentCode, @TempSSN, @TempRsTimeDateOffice, @TempMRsTimeLeaveAmount, @TempMRsTimeLeaveCode, @TempMRsTimeLeaveType, @TempMRsTimeLeaveType1, @TempMRsTimeLeaveType2,@TempMRsTimeFirstHalfLeaveCode, @TempMRsTimeSecondHalfLeaveCode, @TempMIn1, @TempMIn2, @TempMOut1, @TempMOut2, @TempMRsEmpIsOS, @TempMRsEmpIsOT, @TempMRsEmpInOnly,@TempMRsEmpIsPunchAll,@TempMRsEmpPermisLateArrival,@TempMRsEmpTime,@TempMRsEmpPermisEarlyDepart,@TempMRsEmpIsHalfDay,@TempMRsEmpHalf,@TempMRsEmpIsShort,@TempMRsEmpShort,@TempMRsEmpIsTimeLossAllowed,@TempMRsEmpOTRate, @TempMIn1Manual, @TempMIn2Manual, @TempMOut1Manual, @TempMOut2Manual,@MIS,@ISOTONOFF
				EXEC [dbo].[ProcessUPD2] @TempSSN, @TempRsShiftAttended, @CompanyCode, @TempDepartmentCode, @TempSSN, @TempRsTimeDateOffice, @TempMRsTimeLeaveAmount, @TempMRsTimeLeaveCode, @TempMRsTimeLeaveType, @TempMRsTimeLeaveType1, @TempMRsTimeLeaveType2,@TempMRsTimeFirstHalfLeaveCode, @TempMRsTimeSecondHalfLeaveCode, @TempMIn1, @TempMIn2, @TempMOut1, @TempMOut2, @TempMRsEmpIsOS, @TempMRsEmpIsOT, @TempMRsEmpInOnly,@TempMRsEmpIsPunchAll,@TempMRsEmpPermisLateArrival,@TempMRsEmpTime,@TempMRsEmpPermisEarlyDepart,@TempMRsEmpIsHalfDay,@TempMRsEmpHalf,@TempMRsEmpIsShort,@TempMRsEmpShort,@TempMRsEmpIsTimeLossAllowed,@TempMRsEmpOTRate, @TempMIn1Manual, @TempMIn2Manual, @TempMOut1Manual, @TempMOut2Manual,@MIS,@ISOTONOFF
				--EXEC [dbo].[ProcessUPDReason] @TempPayCode, @TempRsTimeDateOffice, @TempSSN, @TempMReasonCode
							   
				SET @TempRsTimeRowNumber = @TempRsTimeRowNumber + 1				
			END
			SET @TempRsEmpRowNumber = @TempRsEmpRowNumber + 1
		END		
	END
	ELSE
	BEGIN
		PRINT 'Process All Records - No records to process'
	END
END