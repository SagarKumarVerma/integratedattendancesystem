
CREATE TABLE [dbo].[tblCalander](
	[MDate] [datetime] NOT NULL,
	[PROCess] [char](1) NULL,
	[NRTCPROC] [char](1) NULL,
	[AbsentMail] [char](1) NULL,
	[AutoEL] [char](1) NULL,
	[Autoleaveapproval] [char](1) NULL
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[tblCalander] ADD  DEFAULT ('N') FOR [AutoEL]
GO

