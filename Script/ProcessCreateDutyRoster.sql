DROP PROCEDURE [dbo].[ProcessCreateDutyRoster] 
GO
CREATE PROCEDURE [dbo].[ProcessCreateDutyRoster] 
	@PayCode CHAR(10),
	@FromDate DATETIME,
	@CompanyCode VARCHAR(10),
	@LocationCode VARCHAR(10)
AS
BEGIN
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	PRINT 'INSIDE ProcessCreateDutyRoster => Parameters are - '
	PRINT '@PayCode   :' +@PayCode
	PRINT '@FromDate    :' + CAST(@FromDate AS VARCHAR(50))
	PRINT convert(datetime,@FromDate,103)
	PRINT '@CompanyCode :' + @CompanyCode
	PRINT '@LocationCode :' + @LocationCode

	DECLARE @TempCount INT
	DECLARE @TableID INT
	DECLARE @TempSSN VARCHAR(100)
	DECLARE @MinDate DATETIME
	DECLARE @WOInclude VARCHAR(5)

	DECLARE @TempPayCode CHAR(10)
	DECLARE @DateOfJoin DateTime
	DECLARE @Shift VARCHAR(5)
	DECLARE @AlternateOffDays VARCHAR(10)
	DECLARE @FirstOffDay VARCHAR(5)
	DECLARE @SecondOffType CHAR(1)
	DECLARE @HalfDayShift VARCHAR(5)
	DECLARE @SecondOffDay VARCHAR(5)
	DECLARE @SSN VARCHAR(100)

	DECLARE @EmployeeShiftDetails Table 
	(
		PayCode CHAR(10),
		DateOfJoin DateTime,
		[Shift] VARCHAR(5),
		AlternateOffDays VARCHAR(10),
		FirstOffDay VARCHAR(5),
		SecondOffType CHAR(1),
		HalfDayShift VARCHAR(5),
		SecondOffDay VARCHAR(5),
		SSN VARCHAR(100)
	)

	IF (@PayCode = '')
	BEGIN
		INSERT INTO @EmployeeShiftDetails
		SELECT EmpShift.PAYCODE, 
			   Emp.DateOFJOIN,
			   EmpShift.[SHIFT],
			   EmpShift.ALTERNATE_OFF_DAYS,
			   EmpShift.FIRSTOFFDAY,
			   EmpShift.SECONDOFFTYPE,
			   EmpShift.HALFDAYSHIFT,
			   EmpShift.SECONDOFFDAY,
			   Emp.SSN
		FROM tblEmployeeShiftMaster(NOLOCK) EmpShift
		INNER JOIN tblEmployee(nolock) Emp ON Emp.SSN = EmpShift.SSN 
		WHERE Emp.active = 'Y' 
		AND Emp.Companycode=@CompanyCode  Order By EmpShift.Paycode
    END
	ELSE
	BEGIN
		INSERT INTO @EmployeeShiftDetails
		SELECT EmpShift.PAYCODE, 
			   Emp.DateOFJOIN,
			   EmpShift.[SHIFT],
			   EmpShift.ALTERNATE_OFF_DAYS,
			   EmpShift.FIRSTOFFDAY,
			   EmpShift.SECONDOFFTYPE,
			   EmpShift.HALFDAYSHIFT,
			   EmpShift.SECONDOFFDAY,
			   Emp.SSN
		FROM tblEmployeeShiftMaster(NOLOCK) EmpShift
		INNER JOIN tblEmployee(nolock) Emp ON Emp.SSN = EmpShift.SSN 
		WHERE Emp.active = 'Y' 
		AND Emp.Companycode=@CompanyCode 
		AND EmpShift.PAYCODE = @PayCode Order By EmpShift.Paycode
	END

	SELECT @TempCount = count(*) FROM @EmployeeShiftDetails
	IF ( @TempCount > 0)
	BEGIN
		WHILE  EXISTS(SELECT * FROM @EmployeeShiftDetails)
		BEGIN
		   -- GET First record to process
		   SELECT @TempSSN = (SELECT TOP 1 SSN FROM @EmployeeShiftDetails ORDER BY SSN ASC)
		   SELECT @TempPayCode = (SELECT TOP 1 PayCode FROM @EmployeeShiftDetails ORDER BY SSN ASC)
	       SELECT @DateOfJoin = DateOfJoin FROM @EmployeeShiftDetails WHERE SSN = @TempSSN

		   IF @DateOfJoin > @FromDate
		   BEGIN
				SET @MinDate = @DateOfJoin  
		   END
		   ELSE
		   BEGIN
				SET @MinDate = @FromDate
		   END
		   
		   SELECT @WOInclude = 'N' --WOInclude FROM tblEmployeeGroupPolicy (nolock) where CompanyCode=@CompanyCode

		   EXEC ProcessDutyRosterForCreate @TempPayCode, @MinDate, @WOInclude, @TempSSN

		   -- DELETE the record from temp table after processing
		   DELETE @EmployeeShiftDetails  where SSN = @TempSSN
		END
	END
	ELSE
	BEGIN
	 PRINT 'No record to process'
	END	
	--SELECT * FROM @EmployeeShiftDetails
END







