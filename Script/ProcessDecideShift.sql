drop PROCEDURE  [dbo].[ProcessDecideShift] 
go
create PROCEDURE  [dbo].[ProcessDecideShift] 
@ShiftAttended VARCHAR(3),
@IsHoliday BIT,	
@EmpAutoShifts VARCHAR(11),
@SetupID INT,
@DateOfOffice DATETIME,
@MIn1 DATETIME,
@MIn2 DATETIME,
@MOut1 DATETIME,
@MOut2 DATETIME,
@CompanyCode VARCHAR(10),
@MTimeShift VARCHAR(3),
@AutoShift CHAR(3) OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @TempAutoShift_Low INT
	DECLARE @TempAutoShift_Up INT
	DECLARE @TempMCtr INT
	DECLARE @TempMFlag BIT
	DECLARE @TempMShift VARCHAR(11)
	DECLARE @TempShift VARCHAR(3)
	DECLARE @TempDate DATE
	DECLARE @TempMShiftTime DATETIME
	DECLARE @TempMShiftStartTime DATETIME
	DECLARE @TempMShiftEndTime DATETIME
	DECLARE @TempMinutes INT

	DECLARE @TempShiftStartDateTime DATETIME
	DECLARE @TempMShiftStartDateTime DATETIME
	DECLARE @TempMShiftEndDateTime DATETIME
	
	DECLARE @TempInnerShift VARCHAR(3)

	SET @AutoShift = @ShiftAttended

	IF  @IsHoliday = 1 OR @ShiftAttended='OFF'
	BEGIN 
		--RETURN @AutoShift
			SELECT @AutoShift
			RETURN 0;
	END

	SET @EmpAutoShifts = ISNULL(@EmpAutoShifts,'')

	IF @EmpAutoShifts = '' AND LEN(LTRIM(RTRIM(@EmpAutoShifts))) = 0
	BEGIN 
		--RETURN @AutoShift
		SELECT @AutoShift
		RETURN 0;
	END

	SET @TempMShift = @EmpAutoShifts
	SELECT 
		@TempAutoShift_Low = AUTOSHIFT_LOW,
		@TempAutoshift_Up = AUTOSHIFT_UP
	FROM tblsetup 
	WHERE SETUPID = @SetupId 

	SET @TempMCtr = 1
	SET @TempMFlag = 0

	WHILE SUBSTRING(@TempMShift,@TempMCtr,3) != ''
	BEGIN
		SET @TempShift = SUBSTRING(@TempMShift,@TempMCtr,3)

		SET @TempInnerShift = ''
		SELECT 	
			@TempInnerShift = ISNULL([SHIFT],''),
			@TempMShiftStartTime = StartTime,
			@TempMShiftEndTime = ENDTIME			
		FROM [dbo].[tblShiftMaster]
		WHERE [SHIFT]=@TempShift
		--AND CompanyCode = @CompanyCode

		IF @TempInnerShift != ''
		BEGIN
			SET @TempDate = CAST(@DateOfOffice AS date)
			SET @TempShiftStartDateTime = CAST(@TempDate AS DATETIME)

			SET @TempMinutes = DATEDIFF(MINUTE, DATEADD(DAY, DATEDIFF(DAY, 0, @TempMShiftStartTime), 0), @TempMShiftStartTime)
			SET @TempShiftStartDateTime = DATEADD(HH, @TempMinutes/60, @TempShiftStartDateTime)
		    SET @TempShiftStartDateTime = DATEADD(MINUTE, @TempMinutes%60,@TempShiftStartDateTime)


			SET @TempMShiftStartDateTime= DATEADD(N,0-@TempAutoShift_Low, @TempShiftStartDateTime)
			SET @TempMShiftEndDateTime = DATEADD(N, @TempAutoShift_Low, @TempShiftStartDateTime)

			IF ISDATE(@MIn1) = 1
			BEGIN 
				IF (DATEDIFF(N, @TempMShiftStartDateTime, @MIn1) >= 0) AND (DATEDIFF(N, @MIn1, @TempMShiftEndDateTime) >= 0)
				BEGIN 
					SET @AutoShift = @TempShift
					SET @TempMFlag = 1
						SELECT @AutoShift
						RETURN 0;
				END
			END
		END
		SET @TempMCtr = @TempMCtr + 4	 
	END

	IF ISDATE(@MIn1) = 0 AND ISDATE(@MOut1) = 0 AND ISDATE(@MIn2) = 0 AND ISDATE(@MOut2) = 0
	BEGIN 
		SET @AutoShift = @MTimeShift
	END

	IF @TempMFlag = 0 
	BEGIN 
		SET @AutoShift = @MTimeShift
	END	

	SELECT @AutoShift
	--RETURN @AutoShift    
	RETURN 0;
END