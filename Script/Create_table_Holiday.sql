CREATE TABLE [dbo].[Holiday](
	[HDate] [datetime] NOT NULL,
	[HOLIDAY] [char](20) NULL,
	[ADJUSTMENTHOLIDAY] [datetime] NULL,
	[OT_FACTOR] [float] NULL,
	[COMPANYCODE] [char](4) NOT NULL,
	[DEPARTMENTCODE] [char](3) NOT NULL,
	[BranchCode] [char](10) NOT NULL CONSTRAINT [DF__Holiday__BranchC__200DB40D]  DEFAULT ('B1'),
	[Divisioncode] [char](10) NOT NULL CONSTRAINT [DF__Holiday__Divisio__5D3D1038]  DEFAULT ('B1')
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO


