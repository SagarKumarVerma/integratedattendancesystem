CREATE TABLE [dbo].[tblOutWorkRegister](
	[PAYCODE] [char](10) NOT NULL,
	[DateOFFICE] [datetime] NOT NULL,
	[IN1] [datetime] NULL,
	[RIN1] [char](3) NULL,
	[OUT1] [datetime] NULL,
	[ROUT1] [char](3) NULL,
	[IN2] [datetime] NULL,
	[RIN2] [char](3) NULL,
	[OUT2] [datetime] NULL,
	[ROUT2] [char](3) NULL,
	[IN3] [datetime] NULL,
	[RIN3] [char](3) NULL,
	[OUT3] [datetime] NULL,
	[ROUT3] [char](3) NULL,
	[IN4] [datetime] NULL,
	[RIN4] [char](3) NULL,
	[OUT4] [datetime] NULL,
	[ROUT4] [char](3) NULL,
	[IN5] [datetime] NULL,
	[RIN5] [char](3) NULL,
	[OUT5] [datetime] NULL,
	[ROUT5] [char](3) NULL,
	[IN6] [datetime] NULL,
	[RIN6] [char](3) NULL,
	[OUT6] [datetime] NULL,
	[ROUT6] [char](3) NULL,
	[IN7] [datetime] NULL,
	[RIN7] [char](3) NULL,
	[OUT7] [datetime] NULL,
	[ROUT7] [char](3) NULL,
	[IN8] [datetime] NULL,
	[RIN8] [char](3) NULL,
	[OUT8] [datetime] NULL,
	[ROUT8] [char](3) NULL,
	[IN9] [datetime] NULL,
	[RIN9] [char](3) NULL,
	[OUT9] [datetime] NULL,
	[ROUT9] [char](3) NULL,
	[IN10] [datetime] NULL,
	[RIN10] [char](3) NULL,
	[OUT10] [datetime] NULL,
	[ROUT10] [char](3) NULL,
	[OUTWORKDURATION] [int] NULL,
	[Reason_OutWork] [char](1) NULL,
	[SSN] [varchar](100) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


