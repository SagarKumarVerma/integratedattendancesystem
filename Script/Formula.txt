CREATE TABLE [dbo].[Pay_Formula](
    [Id] [bigint] IDENTITY(1,1) NOT NULL,
	[FormulaCode] [char](1) NULL,
	[Description] [nvarchar](100) NULL,
	[Formula] [nvarchar](355) NULL,
	[CreatedDate] [DATETIME] NULL,
    [LastModifiedDate] [DATETIME]  NULL,
	[LastModifiedBy] varchar(50)
	)
go
Drop procedure Payroll_TblFormulaSetupMaster_Add
go
Create procedure Payroll_TblFormulaSetupMaster_Add     
(   
    @FormulaCode char(1),
    @Description nvarchar(100),        
    @Formula nvarchar(355),        
    @LastModifiedBy Varchar(20)   
)    
as     
Begin     
    Insert into Pay_Formula (FormulaCode,[Description],Formula,CreatedDate,LastModifiedBy,LastModifiedDate)     
    Values (@FormulaCode,@Description,@Formula,GETDATE(),@LastModifiedBy,GETDATE())     
End
go
Drop procedure Payroll_TblFormulaSetupMaster_Upd
go
Create procedure Payroll_TblFormulaSetupMaster_Upd      
(      
    @FormulaCode char(1),
    @Description nvarchar(100),        
    @Formula nvarchar(355),        
    @LastModifiedBy Varchar(20) 
)      
as      
begin      
   Update Pay_Formula       
   set FormulaCode=@FormulaCode,          
   [Description]=@Description, 
   [Formula]  =@Formula, 
   LastModifiedBy=@LastModifiedBy,  
   LastModifiedDate=GETDATE()   
   where FormulaCode=@FormulaCode      
End
go
Drop procedure Payroll_TblFormulaSetupMaster_Del
go
Create procedure Payroll_TblFormulaSetupMaster_Del     
(      
    @FormulaCode char(1)    
)      
as       
begin      
   Delete from Pay_Formula where FormulaCode =@FormulaCode      
End

go
Drop procedure Payroll_TblFormulaSetupMaster_AllRec
go
Create procedure Payroll_TblFormulaSetupMaster_AllRec   
as    
Begin    
select FormulaCode,[Description],Formula,CreatedDate ,LastModifiedBy,LastModifiedDate from Pay_Formula     
End
go
Drop procedure Payroll_TblFormulaSetupMaster_GetFormulaSetupData 
go
Create procedure Payroll_TblFormulaSetupMaster_GetFormulaSetupData 
(
 @FormulaCode char(1)   
)  
as    
Begin    
   SELECT * FROM Pay_Formula where FormulaCode = @FormulaCode   
End

GO
Drop procedure Payroll_TblFormulaSetupMaster_CatNum 
go
Create procedure Payroll_TblFormulaSetupMaster_CatNum 
(
 @FormulaCode char(1)    
)  
as    
Begin    
     SELECT top 1 FormulaCode FROM Pay_Formula order by FormulaCode desc

End
GO

