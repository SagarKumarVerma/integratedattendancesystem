USE [IAS]
GO
/****** Object:  StoredProcedure [dbo].[ProcessDutyRosterForUpdate]    Script Date: 01/03/2021 16:18:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[ProcessDutyRosterForUpdate] 
	@PayCode CHAR(10),
	@FromDate DATETIME,
	@WO_Include VARCHAR(5),
	@SSN VARCHAR(100)
AS
BEGIN
	SET NOCOUNT ON;

	SET NOCOUNT ON;
	PRINT  ''
	PRINT 'INSIDE ProcessDutyRoster => Parameters are - '
	PRINT '@PayCode   :' +@PayCode
	PRINT '@FromDate    :' + CAST(@FromDate AS VARCHAR(50))
	PRINT '@WO_Include  : ' + @WO_Include
	PRINT '@SSN :' + @SSN

	DECLARE @TempCreateDate DATETIME
	DECLARE @TempMCount INT
	DECLARE @TempShiftType CHAR(1)
	DECLARE @TempPos INT
	DECLARE @TempMDate DATETIME
	DECLARE @TempMWDay CHAR(5)

	DECLARE @TempMRdays  INT
	DECLARE @TempMLShift  VARCHAR(5)
	DECLARE @TempMPat  VARCHAR(11)
	DECLARE @TempMShift  VARCHAR(5)
	DECLARE @TempMCnt  INT
	DECLARE @TempMAbsentVal  INT
	DECLARE @TempMWoVal  INT
	DECLARE @TempMSTAT VARCHAR(6)
	DECLARE @TempMSatCtr  INT

	
	DECLARE @TempPayCode CHAR(10)
	DECLARE @DateOfJoin DateTime
	DECLARE @Shift VARCHAR(5)
	DECLARE @AlternateOffDays VARCHAR(10)
	DECLARE @FirstOffDay VARCHAR(5)
	DECLARE @SecondOffType CHAR(1)
	DECLARE @HalfDayShift VARCHAR(5)
	DECLARE @SecondOffDay VARCHAR(5)
	DECLARE @EmpSSN VARCHAR(100)
	DECLARE @ShiftType CHAR(1)
	DECLARE @ShiftRemainDays INT
	DECLARE @SHIFTPATTERN VARCHAR(11)
	DECLARE @CDays FLOAT
	

	DECLARE @EmployeeShiftDetails Table 
	(
		PayCode CHAR(10),
		DateOfJoin DateTime,
		[Shift] VARCHAR(5),
		AlternateOffDays VARCHAR(10),
		FirstOffDay VARCHAR(5),
		SecondOffType CHAR(1),
		HalfDayShift VARCHAR(5),
		SecondOffDay VARCHAR(5),
		EmpSSN VARCHAR(100),
		ShiftType CHAR(1),
		ShiftRemainDays INT,
		SHIFTPATTERN VARCHAR(11),
		CDays FLOAT
	)	
	
	DECLARE @StringDateTime VARCHAR(50)
	IF NOT EXISTS(SELECT * FROM  tblcalander WHERE DatePart(yy,MDate) = DatePart(yy,@FromDate))
	BEGIN
	    SET  @StringDateTime = '1/1/'+ CAST(DatePart(yy,@FromDate) as varchar)
		PRINT @StringDateTime
		SET @TempCreateDate = CAST(@StringDateTime AS datetime)
		PRINT @TempCreateDate

		WHILE(DATEPART(yy, @TempCreateDate) = DatePart(yy,@FromDate))
		BEGIN
		  PRINT @TempCreateDate
		  INSERT INTO tblCalander (mDate) VALUES (@TempCreateDate)
		  SET @TempCreateDate = DateAdd(day, 1,@TempCreateDate)
		END
	END

	INSERT INTO @EmployeeShiftDetails
	SELECT 
		EmpShift.PAYCODE, 
		Emp.DateOFJOIN,
		EmpShift.[SHIFT],
		EmpShift.ALTERNATE_OFF_DAYS,
		EmpShift.FIRSTOFFDAY,
		EmpShift.SECONDOFFTYPE,
		EmpShift.HALFDAYSHIFT,
		EmpShift.SECONDOFFDAY,
		Emp.SSN,
		EmpShift.ShiftType,
		EmpShift.ShiftRemainDays,
		EmpShift.SHIFTPATTERN,
		EmpShift.CDays
	FROM tblEmployeeShiftMaster(NOLOCK) EmpShift
	INNER JOIN tblEmployee(nolock) Emp ON Emp.SSN = EmpShift.SSN 
	WHERE Emp.active = 'Y' AND Emp.SSN = @SSN

    SELECT 
		@TempPayCode = PayCode,
		@DateOfJoin = DateOfJoin,
		@Shift = [Shift],
		@AlternateOffDays = AlternateOffDays,
		@FirstOffDay = FirstOffDay,
		@SecondOffType = SecondOffType,
		@HalfDayShift = HalfDayShift,
		@SecondOffDay = SecondOffDay,
		@EmpSSN = EmpSSN,
		@ShiftType =  ShiftType,
		@ShiftRemainDays = ShiftRemainDays,
		@SHIFTPATTERN = SHIFTPATTERN,
		@CDays = CDays
	 FROM @EmployeeShiftDetails

	 SET @TempMSatCtr = 0
	-- SET TempMCount = 0 . This is not required but kept as it is used in vb code. 
	SET @TempMCount=0
	IF @TempMCount <= 0 
	BEGIN
		SET @TempMRdays = 7
		SET @TempMLShift = @Shift
		SET @TempShiftType = @ShiftType

		IF @TempShiftType = 'R'
		BEGIN
			SET  @TempMRdays = @ShiftRemainDays  
			SET  @TempMPat = @SHIFTPATTERN  
		END
		ELSE IF @TempShiftType = 'F'
		BEGIN
			IF len(@TempMLShift) = 1
			BEGIN 
				SET  @TempMPat = '  '+ @TempMLShift + ',' +'  '+  @TempMLShift + ',' + '  '+ @TempMLShift
			END
			ELSE IF len(@TempMLShift) = 2
			BEGIN
				SET @TempMPat = ' '+ @TempMLShift + ',' +' '+  @TempMLShift + ',' + ' '+ @TempMLShift
			END
			ELSE
			BEGIN
				SET @TempMPat = @TempMLShift + ',' + @TempMLShift + ',' +@TempMLShift
			END
		END
		ELSE
		BEGIN
			SET @TempMPat = 'IGN,IGN,IGN'
			SET @TempMLShift = 'IGN'
		END

		SET @TempPos = CHARINDEX(@TempMLShift,@TempMPat)

		IF @TempPos <= 0
		BEGIN
			SET @TempPos = 1
		ENd
		
		SET @TempMDate = @FromDate

		WHILE(DATEPART(yy, @TempMDate) = DatePart(yy,@FromDate))
		BEGIN
			WHILE( @TempMRdays > 0 AND DATEPART(yy, @TempMDate) = DatePart(yy,@FromDate))
			BEGIN
				IF(	DATEPART(DD, @TempMDate) = 1)
				BEGIN
					SET @TempMSatCtr = 0
				END

				SET @TempMWDay = UPPER(FORMAT(@TempMDate,'ddd'))
				IF @TempMWDay  = @FirstOffDay 
				BEGIN
					SET @TempMShift = 'OFF'
					IF @WO_Include != 'Y'
					BEGIN
						SET @TempMRdays = @TempMRdays + 1
					END
				END
				ELSE IF  @TempMWDay = @SecondOffDay
				BEGIN
					SET @TempMSatCtr= @TempMSatCtr + 1
					IF (CHARINDEX (CAST(@TempMSatCtr AS VARCHAR), @AlternateOffDays) > 0 )
					BEGIN
						IF @SecondOffType = 'H' 
						BEGIN
							SET @TempMShift  = @HalfDayShift
						END
						ELSE
						BEGIN
							SET @TempMShift  = 'OFF'
						END
				    
						IF @WO_Include != 'Y' 
						BEGIN
							SET @TempMRdays = @TempMRdays + 1
						END	                        
					END 
					ELSE
					BEGIN
						SET @TempMShift = SUBSTRING(@TempMPat, @TempPos,3)
					END 
				END
				ELSE
				BEGIN
					SET @TempMShift = SUBSTRING(@TempMPat, @TempPos,3)
				END      
			
			    IF @TempMShift = 'OFF' 
				BEGIN
					SET @TempMSTAT = 'WO'
					SET @TempMWoVal = 1
					SET @TempMAbsentVal = 0
				END
				ELSE
				BEGIN
					SET @TempMSTAT = 'A'
					SET @TempMWoVal = 0
					SET @TempMAbsentVal = 1				
				END

				UPDATE TBLTIMEREGISTER
				SET 
					[Shift]= @TempMShift,
					ShiftAttended=@TempMShift,
					[Status]=@TempMSTAT,
					WO_Value=@TempMWoVal,
					AbsentValue=@TempMAbsentVal,
					LEAVEVALUE=0,
					HOLIDAY_VALUE=0,
					HOURSWORKED=0,
					OtDuration=0,
					OTAmount=0
				WHERE SSN = @SSN and   Dateoffice = @TempMDate

				SET @TempMRdays = @TempMRdays - 1
				SET @TempMDate = DateAdd(day, 1,@TempMDate)				
			END

			SET @TempMRdays= @CDays
			IF @TempMRdays <= 0 
			BEGIN
				SET @TempMRdays = 7
			END

			SET @TempPos = @TempPos + 4

			IF (@TempPos > LEN(@TempMPat))
			BEGIN
				SET @TempPos = 1
			END

			IF SUBSTRING(@TempMPat, @TempPos, 3) = '   ' 
			BEGIN
				SET @TempPos = 1
			END                   
		END
	END  	
END