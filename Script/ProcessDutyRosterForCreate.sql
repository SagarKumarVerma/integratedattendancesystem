-- Batch submitted through debugger: SQLQuery1.sql|0|0|C:\Users\amail\AppData\Local\Temp\~vs8EFC.sql
drop PROCEDURE [dbo].[ProcessDutyRosterForCreate] 
go
CREATE PROCEDURE [dbo].[ProcessDutyRosterForCreate] 
	@PayCode CHAR(10),
	@FromDate DATETIME,
	@WO_Include VARCHAR(5),
	@SSN VARCHAR(100)
AS
BEGIN
	SET NOCOUNT ON;

	SET NOCOUNT ON;
	PRINT  ''
	PRINT 'INSIDE ProcessDutyRoster => Parameters are - '
	PRINT '@PayCode   :' +@PayCode
	PRINT '@FromDate    :' + CAST(@FromDate AS VARCHAR(50))
	PRINT '@WO_Include  : ' + @WO_Include
	PRINT '@SSN :' + @SSN

	DECLARE @TempCreateDate DATETIME
	DECLARE @TempMCount INT
	DECLARE @TempShiftType CHAR(1)
	DECLARE @TempPos INT
	DECLARE @TempMDate DATETIME
	DECLARE @TempMWDay CHAR(5)
	DECLARE @strTempMDate CHAR(10)
	DECLARE @strVALUES varchar(5000)

	DECLARE @TempMRdays  INT
	DECLARE @TempMLShift  VARCHAR(5)
	DECLARE @TempMPat  VARCHAR(11)
	DECLARE @TempMShift  VARCHAR(5)
	DECLARE @TempMCnt  INT
	DECLARE @TempMAbsentVal  INT
	DECLARE @TempMWoVal  INT
	DECLARE @TempMSTAT VARCHAR(6)
	DECLARE @TempMSatCtr  INT

	declare @SQL1 as nvarchar(max)
	declare @SQL2 as nvarchar(max)
	declare @SQL3 as nvarchar(max)
	declare @SQL4 as nvarchar(max)
	declare @SQL5 as nvarchar(max)
	declare @SQL6 datetime
	declare @SQL7 as nvarchar(max)
	declare @SQL8 as nvarchar(max)
	declare @SQL9 as nvarchar(max)
	declare @SQL10 as nvarchar(max)
	declare @SQL11 as nvarchar(max)

	DECLARE @Table1  VARCHAR(100)
	DECLARE @Table2  VARCHAR(100)
	DECLARE @Table3  VARCHAR(100)
	DECLARE @Table4  VARCHAR(100)
	DECLARE @Table5  VARCHAR(100)
	DECLARE @Table6  VARCHAR(100)
	DECLARE @Table7  VARCHAR(100)
	DECLARE @Table8  VARCHAR(100)
	DECLARE @Table9  VARCHAR(100)
	DECLARE @Table10  VARCHAR(100)
	DECLARE @Table11 VARCHAR(100)
	DECLARE @Table12  VARCHAR(100)


	
	DECLARE @TempPayCode CHAR(10)
	DECLARE @DateOfJoin DateTime
	DECLARE @Shift VARCHAR(5)
	DECLARE @AlternateOffDays VARCHAR(10)
	DECLARE @FirstOffDay VARCHAR(5)
	DECLARE @SecondOffType CHAR(1)
	DECLARE @HalfDayShift VARCHAR(5)
	DECLARE @SecondOffDay VARCHAR(5)
	DECLARE @EmpSSN VARCHAR(100)
	DECLARE @ShiftType CHAR(1)
	DECLARE @ShiftRemainDays INT
	DECLARE @SHIFTPATTERN VARCHAR(11)
	DECLARE @CDays FLOAT
	

	DECLARE @EmployeeShiftDetails Table 
	(
		PayCode CHAR(10),
		DateOfJoin DateTime,
		[Shift] VARCHAR(5),
		AlternateOffDays VARCHAR(10),
		FirstOffDay VARCHAR(5),
		SecondOffType CHAR(1),
		HalfDayShift VARCHAR(5),
		SecondOffDay VARCHAR(5),
		EmpSSN VARCHAR(100),
		ShiftType CHAR(1),
		ShiftRemainDays INT,
		SHIFTPATTERN VARCHAR(11),
		CDays FLOAT
	)	
	
	DECLARE @StringDateTime VARCHAR(50)
	IF NOT EXISTS(SELECT * FROM  tblcalander WHERE DatePart(yy,MDate) = DatePart(yy,@FromDate))
	BEGIN
	    SET  @StringDateTime = '1/1/'+ CAST(DatePart(yy,@FromDate) as varchar)
		--PRINT @StringDateTime
		SET @TempCreateDate = CAST(@StringDateTime AS datetime)
		--PRINT @TempCreateDate

		WHILE(DATEPART(yy, @TempCreateDate) = DatePart(yy,@FromDate))
		BEGIN
		  --PRINT @TempCreateDate
		  INSERT INTO tblCalander (mDate) VALUES (@TempCreateDate)
		  SET @TempCreateDate = DateAdd(day, 1,@TempCreateDate)
		END
	END

	INSERT INTO @EmployeeShiftDetails
	SELECT 
		EmpShift.PAYCODE, 
		Emp.DateOFJOIN,
		EmpShift.[SHIFT],
		EmpShift.ALTERNATE_OFF_DAYS,
		EmpShift.FIRSTOFFDAY,
		EmpShift.SECONDOFFTYPE,
		EmpShift.HALFDAYSHIFT,
		EmpShift.SECONDOFFDAY,
		Emp.SSN,
		EmpShift.ShiftType,
		EmpShift.ShiftRemainDays,
		EmpShift.SHIFTPATTERN,
		EmpShift.CDays
	FROM tblEmployeeShiftMaster(NOLOCK) EmpShift
	INNER JOIN tblEmployee(nolock) Emp ON Emp.SSN = EmpShift.SSN 
	WHERE Emp.active = 'Y' AND Emp.SSN = @SSN

    SELECT 
		@TempPayCode = PayCode,
		@DateOfJoin = DateOfJoin,
		@Shift = [Shift],
		@AlternateOffDays = AlternateOffDays,
		@FirstOffDay = FirstOffDay,
		@SecondOffType = SecondOffType,
		@HalfDayShift = HalfDayShift,
		@SecondOffDay = SecondOffDay,
		@EmpSSN = EmpSSN,
		@ShiftType =  ShiftType,
		@ShiftRemainDays = ShiftRemainDays,
		@SHIFTPATTERN = SHIFTPATTERN,
		@CDays = CDays
	 FROM @EmployeeShiftDetails

	 SET @TempMSatCtr = 0
	 --set @FromDate=@DateOfJoin
	-- SET TempMCount = 0 . This is not required but kept as it is used in vb code. 
	SET @TempMCount=0
	IF @TempMCount <= 0 
	BEGIN
		SET @TempMRdays = 7
		SET @TempMLShift = @Shift
		SET @TempShiftType = @ShiftType

		IF @TempShiftType = 'R'
		BEGIN
			SET  @TempMRdays = @ShiftRemainDays  
			SET  @TempMPat = @SHIFTPATTERN  
		END
		ELSE IF @TempShiftType = 'F'
		BEGIN
			IF len(@TempMLShift) = 1
			BEGIN 
				SET  @TempMPat = '  '+ @TempMLShift + ',' +'  '+  @TempMLShift + ',' + '  '+ @TempMLShift
			END
			ELSE IF len(@TempMLShift) = 2
			BEGIN
				SET @TempMPat = ' '+ @TempMLShift + ',' +' '+  @TempMLShift + ',' + ' '+ @TempMLShift
			END
			ELSE
			BEGIN
				SET @TempMPat = @TempMLShift + ',' + @TempMLShift + ',' +@TempMLShift
			END
		END
		ELSE
		BEGIN
			SET @TempMPat = 'IGN,IGN,IGN'
			SET @TempMLShift = 'IGN'
		END

		SET @TempPos = CHARINDEX(@TempMLShift,@TempMPat)

		IF @TempPos <= 0
		BEGIN
			SET @TempPos = 1
		ENd
		
		SET @TempMDate = @FromDate
		set @strTempMDate=convert(varchar(10),@TempMDate,103)

		WHILE(DATEPART(yy, @TempMDate) = DatePart(yy,@FromDate))
		BEGIN
			WHILE( @TempMRdays > 0 AND DATEPART(yy, @TempMDate) = DatePart(yy,@FromDate))
			BEGIN
				IF(	DATEPART(DD, @TempMDate) = 1)
				BEGIN
					SET @TempMSatCtr = 0
				END

				SET @TempMWDay = UPPER(FORMAT(@TempMDate,'ddd'))
				IF @TempMWDay  = @FirstOffDay 
				BEGIN
					SET @TempMShift = 'OFF'
					IF @WO_Include != 'Y'
					BEGIN
						SET @TempMRdays = @TempMRdays + 1
					END
				END
				ELSE IF  @TempMWDay = @SecondOffDay
				BEGIN
					SET @TempMSatCtr= @TempMSatCtr + 1
					IF (CHARINDEX (CAST(@TempMSatCtr AS VARCHAR), @AlternateOffDays) > 0 )
					BEGIN
						IF @SecondOffType = 'H' 
						BEGIN
							SET @TempMShift  = @HalfDayShift
						END
						ELSE
						BEGIN
							SET @TempMShift  = 'OFF'
						END
				    
						IF @WO_Include != 'Y' 
						BEGIN
							SET @TempMRdays = @TempMRdays + 1
						END	                        
					END 
					ELSE
					BEGIN
						SET @TempMShift = SUBSTRING(@TempMPat, @TempPos,3)
					END 
				END
				ELSE
				BEGIN
					SET @TempMShift = SUBSTRING(@TempMPat, @TempPos,3)
				END      
			
			    IF @TempMShift = 'OFF' 
				BEGIN
					SET @TempMSTAT = 'WO'
					SET @TempMWoVal = 1
					SET @TempMAbsentVal = 0
				END
				ELSE
				BEGIN
					SET @TempMSTAT = 'A'
					SET @TempMWoVal = 0
					SET @TempMAbsentVal = 1				
				END

				EXEC tablecreation

				BEGIN TRY
				
				declare @test as varchar(200); 
				declare @a char(10),  @b varchar(max) ,@c char(100)
				--set @a =@PayCode
				set @b =convert(varchar,@TempMDate,23)
				set @strTempMDate =convert(varchar,@TempMDate,23)
												
				IF DATEPART(month, @TempMDate) = 1
				BEGIN
				set @test = ''''+@PayCode+''','''+@b+''','''+@TempMShift+''','''+@TempMShift+''','''+	@TempMSTAT+''',1,0,0,0,0,0,0,0,'''+@SSN+'''';
					set @Table1= '[TBLTIMEREGISTER_0'+CAST(DATEPART(month, @TempMDate) AS varchar)+CAST(DATEPART(year, @TempMDate) AS varchar)+']'
					SET @SQL1='INSERT INTO'+ @Table1+ '(Paycode,DateOffice,Shift,ShiftAttended,Status,WO_Value,AbsentValue,presentvalue,leavevalue,HOLIDAY_VALUE,HOURSWORKED,OtDuration,OTAmount,SSN)
								VALUES('+@test+')'
								print (@SQL1)
				END

				ELSE IF DATEPART(month, @TempMDate) = 2
				BEGIN
				
				  set @test = ''''+@PayCode+''','''+@b+''','''+@TempMShift+''','''+@TempMShift+''','''+	@TempMSTAT+''',1,0,0,0,0,0,0,0,'''+@SSN+'''';
					set @Table1= '[TBLTIMEREGISTER_0'+CAST(DATEPART(month, @TempMDate) AS varchar)+CAST(DATEPART(year, @TempMDate) AS varchar)+']'
					SET @SQL1='INSERT INTO'+ @Table1+ '(Paycode,DateOffice,Shift,ShiftAttended,Status,WO_Value,AbsentValue,presentvalue,leavevalue,HOLIDAY_VALUE,HOURSWORKED,OtDuration,OTAmount,SSN)
								VALUES('+@test+')'
								print (@SQL1)
				END
				
				ELSE IF DATEPART(month, @TempMDate) = 3
				BEGIN
					set @test = ''''+@PayCode+''','''+@b+''','''+@TempMShift+''','''+@TempMShift+''','''+	@TempMSTAT+''',1,0,0,0,0,0,0,0,'''+@SSN+'''';
					set @Table1= '[TBLTIMEREGISTER_0'+CAST(DATEPART(month, @TempMDate) AS varchar)+CAST(DATEPART(year, @TempMDate) AS varchar)+']'
					SET @SQL1='INSERT INTO'+ @Table1+ '(Paycode,DateOffice,Shift,ShiftAttended,Status,WO_Value,AbsentValue,presentvalue,leavevalue,HOLIDAY_VALUE,HOURSWORKED,OtDuration,OTAmount,SSN)
								VALUES('+@test+')'
								print (@SQL1)
				END

				ELSE IF DATEPART(month, @TempMDate) = 4
				BEGIN
					set @test = ''''+@PayCode+''','''+@b+''','''+@TempMShift+''','''+@TempMShift+''','''+	@TempMSTAT+''',1,0,0,0,0,0,0,0,'''+@SSN+'''';
					set @Table1= '[TBLTIMEREGISTER_0'+CAST(DATEPART(month, @TempMDate) AS varchar)+CAST(DATEPART(year, @TempMDate) AS varchar)+']'
					SET @SQL1='INSERT INTO'+ @Table1+ '(Paycode,DateOffice,Shift,ShiftAttended,Status,WO_Value,AbsentValue,presentvalue,leavevalue,HOLIDAY_VALUE,HOURSWORKED,OtDuration,OTAmount,SSN)
								VALUES('+@test+')'
								print (@SQL1)
				END

				ELSE IF DATEPART(month, @TempMDate) = 5
				BEGIN
					set @test = ''''+@PayCode+''','''+@b+''','''+@TempMShift+''','''+@TempMShift+''','''+	@TempMSTAT+''',1,0,0,0,0,0,0,0,'''+@SSN+'''';
					set @Table1= '[TBLTIMEREGISTER_0'+CAST(DATEPART(month, @TempMDate) AS varchar)+CAST(DATEPART(year, @TempMDate) AS varchar)+']'
					SET @SQL1='INSERT INTO'+ @Table1+ '(Paycode,DateOffice,Shift,ShiftAttended,Status,WO_Value,AbsentValue,presentvalue,leavevalue,HOLIDAY_VALUE,HOURSWORKED,OtDuration,OTAmount,SSN)
								VALUES('+@test+')'
								print (@SQL1)
				END

				ELSE IF DATEPART(month, @TempMDate) = 6
				BEGIN
					set @test = ''''+@PayCode+''','''+@b+''','''+@TempMShift+''','''+@TempMShift+''','''+	@TempMSTAT+''',1,0,0,0,0,0,0,0,'''+@SSN+'''';
					set @Table1= '[TBLTIMEREGISTER_0'+CAST(DATEPART(month, @TempMDate) AS varchar)+CAST(DATEPART(year, @TempMDate) AS varchar)+']'
					SET @SQL1='INSERT INTO'+ @Table1+ '(Paycode,DateOffice,Shift,ShiftAttended,Status,WO_Value,AbsentValue,presentvalue,leavevalue,HOLIDAY_VALUE,HOURSWORKED,OtDuration,OTAmount,SSN)
								VALUES('+@test+')'
								print (@SQL1)
				END

				ELSE IF DATEPART(month, @TempMDate) = 7
				BEGIN
					set @test = ''''+@PayCode+''','''+@b+''','''+@TempMShift+''','''+@TempMShift+''','''+	@TempMSTAT+''',1,0,0,0,0,0,0,0,'''+@SSN+'''';
					set @Table1= '[TBLTIMEREGISTER_0'+CAST(DATEPART(month, @TempMDate) AS varchar)+CAST(DATEPART(year, @TempMDate) AS varchar)+']'
					SET @SQL1='INSERT INTO'+ @Table1+ '(Paycode,DateOffice,Shift,ShiftAttended,Status,WO_Value,AbsentValue,presentvalue,leavevalue,HOLIDAY_VALUE,HOURSWORKED,OtDuration,OTAmount,SSN)
								VALUES('+@test+')'
								print (@SQL1)
				END

				ELSE IF DATEPART(month, @TempMDate) = 8
				BEGIN
					set @test = ''''+@PayCode+''','''+@b+''','''+@TempMShift+''','''+@TempMShift+''','''+	@TempMSTAT+''',1,0,0,0,0,0,0,0,'''+@SSN+'''';
					set @Table1= '[TBLTIMEREGISTER_0'+CAST(DATEPART(month, @TempMDate) AS varchar)+CAST(DATEPART(year, @TempMDate) AS varchar)+']'
					SET @SQL1='INSERT INTO'+ @Table1+ '(Paycode,DateOffice,Shift,ShiftAttended,Status,WO_Value,AbsentValue,presentvalue,leavevalue,HOLIDAY_VALUE,HOURSWORKED,OtDuration,OTAmount,SSN)
								VALUES('+@test+')'
								print (@SQL1)
				END

				ELSE IF DATEPART(month, @TempMDate) = 9
				BEGIN
					set @test = ''''+@PayCode+''','''+@b+''','''+@TempMShift+''','''+@TempMShift+''','''+	@TempMSTAT+''',1,0,0,0,0,0,0,0,'''+@SSN+'''';
					set @Table1= '[TBLTIMEREGISTER_0'+CAST(DATEPART(month, @TempMDate) AS varchar)+CAST(DATEPART(year, @TempMDate) AS varchar)+']'
					SET @SQL1='INSERT INTO'+ @Table1+ '(Paycode,DateOffice,Shift,ShiftAttended,Status,WO_Value,AbsentValue,presentvalue,leavevalue,HOLIDAY_VALUE,HOURSWORKED,OtDuration,OTAmount,SSN)
								VALUES('+@test+')'
								print (@SQL1)
				END

				ELSE IF DATEPART(month, @TempMDate) = 10
				BEGIN
					set @test = ''''+@PayCode+''','''+@b+''','''+@TempMShift+''','''+@TempMShift+''','''+	@TempMSTAT+''',1,0,0,0,0,0,0,0,'''+@SSN+'''';
					set @Table1= '[TBLTIMEREGISTER_'+CAST(DATEPART(month, @TempMDate) AS varchar)+CAST(DATEPART(year, @TempMDate) AS varchar)+']'
					SET @SQL1='INSERT INTO'+ @Table1+ '(Paycode,DateOffice,Shift,ShiftAttended,Status,WO_Value,AbsentValue,presentvalue,leavevalue,HOLIDAY_VALUE,HOURSWORKED,OtDuration,OTAmount,SSN)
								VALUES('+@test+')'
								print (@SQL1)
				END

				ELSE IF DATEPART(month, @TempMDate) = 11
				BEGIN
					set @test = ''''+@PayCode+''','''+@b+''','''+@TempMShift+''','''+@TempMShift+''','''+	@TempMSTAT+''',1,0,0,0,0,0,0,0,'''+@SSN+'''';
					set @Table1= '[TBLTIMEREGISTER_'+CAST(DATEPART(month, @TempMDate) AS varchar)+CAST(DATEPART(year, @TempMDate) AS varchar)+']'
					SET @SQL1='INSERT INTO'+ @Table1+ '(Paycode,DateOffice,Shift,ShiftAttended,Status,WO_Value,AbsentValue,presentvalue,leavevalue,HOLIDAY_VALUE,HOURSWORKED,OtDuration,OTAmount,SSN)
								VALUES('+@test+')'
								print (@SQL1)
				END

				ELSE IF DATEPART(month, @TempMDate) = 12
				BEGIN
					set @test = ''''+@PayCode+''','''+@b+''','''+@TempMShift+''','''+@TempMShift+''','''+	@TempMSTAT+''',1,0,0,0,0,0,0,0,'''+@SSN+'''';
					set @Table1= '[TBLTIMEREGISTER_'+CAST(DATEPART(month, @TempMDate) AS varchar)+CAST(DATEPART(year, @TempMDate) AS varchar)+']'
					SET @SQL1='INSERT INTO'+ @Table1+ '(Paycode,DateOffice,Shift,ShiftAttended,Status,WO_Value,AbsentValue,presentvalue,leavevalue,HOLIDAY_VALUE,HOURSWORKED,OtDuration,OTAmount,SSN)
								VALUES('+@test+')'
								print (@SQL1)
				END

				
				--declare @SQLString as nvarchar(max)=@SQL1+@SQL2+@SQL3+@SQL4+@SQL5+','+@SQL6+','+@SQL7+','+@SQL8+','+@SQL9+','+@SQL10+','+@SQL11 + '0,0,0,0,0,0,'+@SSN+')'
				--print(@SQLString)
				--EXEC (@SQL1+@SQL2+@SQL3+@SQL4+@SQL5+','+@SQL6+','+@SQL7+','+@SQL8+','+@SQL9+','+@SQL10+','+@SQL11 + '0,0,0,0,0,0,'+@SSN+')')
				EXEC (@SQL1)
				END TRY
   BEGIN CATCH;   
   SELECT
    ERROR_NUMBER() AS ErrorNumber,
    ERROR_STATE() AS ErrorState,
    ERROR_SEVERITY() AS ErrorSeverity,
    ERROR_PROCEDURE() AS ErrorProcedure,
    ERROR_LINE() AS ErrorLine,
    ERROR_MESSAGE() AS ErrorMessage;
      
   END CATCH

					SET @TempMRdays = @TempMRdays - 1
					SET @TempMDate = DateAdd(day, 1,@TempMDate)				
			END

			SET @TempMRdays= @CDays
			IF @TempMRdays <= 0 
			BEGIN
				SET @TempMRdays = 7
			END

			SET @TempPos = @TempPos + 4

			IF (@TempPos > LEN(@TempMPat))
			BEGIN
				SET @TempPos = 1
			END

			IF SUBSTRING(@TempMPat, @TempPos, 3) = '   ' 
			BEGIN
				SET @TempPos = 1
			END                   
		END
	END  	
END