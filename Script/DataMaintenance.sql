Drop procedure [dbo].[tblEmployeeMaster_Details_DDL]  
GO
Create procedure [dbo].[tblEmployeeMaster_Details_DDL]   
as    
Begin    
  select * FROM TblEmployee 
End
GO
Drop procedure [dbo].[tblEmployeeMaster_Selected_Details]  
GO
Create procedure [dbo].[tblEmployeeMaster_Selected_Details] 
(
@EmpCode varchar(10)
)  
as    
Begin
Select tblEmployee.ACTIVE,tblEmployee.PresentCardNo,tblEmployee.Paycode,tblEmployee.EmpName,tblCategory.CategoryName,tblEmployee.SSN,
                tblcompany.companyname,tblDepartment.DepartmentName,tblemployeeshiftmaster.ISPRESENTONWOPRESENT from 
                tblEmployee ,tblCategory ,tblcompany , tblDepartment,tblemployeeshiftmaster
                where tblEmployee.Paycode=@EmpCode And 
                tblEmployee.companycode=tblcompany.companycode and tblEmployee.cat=tblCategory.CategoryCode and 
               tblEmployee.DepartmentCode=tblDepartment.DepartmentCode and tblemployee.SSN=tblEmployeeShiftMaster.SSN
End
go
Drop procedure [dbo].[tblTimeRegister_Select_Att]  
GO
Create procedure [dbo].[tblTimeRegister_Select_Att] 
(
@SSN varchar(10),
@DateOffice datetime
--@SSN varchar(100)
)  
as    
Begin
select PAYCODE,SSN,STATUS,SHIFT,SHIFTATTENDED,convert(varchar(5),IN1,108)IN1,convert(varchar(5),OUT2,108)OUT2,LATEARRIVAL,EARLYDEPARTURE,HOURSWORKED,convert(varchar(10),DateOFFICE,103)DateOFFICE from tbltimeregister_032021 where SSN=@SSN and DateOFFICE=CONVERT(datetime,@DateOffice,103) --datepart(month,DateOFFICE)=datepart(month,getdate()) and datepart(YEAR,DateOFFICE)=datepart(YEAR,getdate())
End
go
Drop procedure [dbo].[tblTimeRegister_Select_AllAtt]  
GO
Create procedure [dbo].[tblTimeRegister_Select_AllAtt] 
(
@SSN varchar(10),
@DateOffice datetime
--@SSN varchar(100)
)  
as    
Begin
select PAYCODE,SSN,STATUS,SHIFT,SHIFTATTENDED,convert(varchar(10),IN1,103)IN1,convert(varchar(10),OUT2,103)OUT2,LATEARRIVAL,EARLYDEPARTURE,HOURSWORKED,convert(varchar(10),DateOFFICE,103)DateOFFICE from tbltimeregister_032021 where SSN=@SSN and datepart(month,DateOFFICE)=datepart(month,getdate()) and datepart(YEAR,DateOFFICE)=datepart(YEAR,getdate())
End
go
Drop procedure MachineRawPunch_ManualPunch
go
Drop procedure MachineRawPunch_ManualPunch
go
Create procedure MachineRawPunch_ManualPunch     
(   

@CARDNO varchar(10),
@PAYCODE varchar(10),
@ISMANUAL varchar(1),
@P_Day varchar(1),
@OFFICEPUNCH datetime,
@CompanyCode varchar(4),
@SSN varchar(100)
    
)    
as     
Begin     
    Insert into MachineRawPunch (CARDNO,PAYCODE,ISMANUAL,P_Day,OFFICEPUNCH,CompanyCode,SSN)     
    Values (@CARDNO,@PAYCODE,@ISMANUAL,@P_Day,@OFFICEPUNCH,@CompanyCode,@SSN)     
End
go