CREATE TABLE [dbo].[tblCompany](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[IsActive] [varchar](1)NOT NULL,
	[CompanyCode] [varchar](4) NOT NULL,
	[CompanyName] [varchar](50) NOT NULL,
	[CompanyAddress] [varchar](200) NULL,
	[ShortName] [varchar](10) NULL,
	[IndustryNature] [varchar](100) NULL,
	[PhoneNo] [char](13) NULL,
	[EmailId] [varchar](100) NULL,
	[RegistationNo] [varchar](20) NULL,
	[PANNo] [varchar](25) NULL,
	[TANNo] [varchar](50) NULL,
	[TDSCircle] [varchar](25) NULL,
	[LCNo] [varchar](25) NULL,
	[PFNo] [varchar](12) NULL,
	[DCode] [varchar](10) NULL,
    [CreatedDate] [DATETIME] NULL,
    [LastModifiedDate] [DATETIME]  NULL,
	[LastModifiedBy] varchar(50),
 CONSTRAINT [PK_tblCompany] PRIMARY KEY CLUSTERED 
(
	[COMPANYCODE] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
go
--Add New record Company Master
Drop procedure TblCompanyMaster_Add
go
Create procedure TblCompanyMaster_Add     
(   
    @IsActive	 varchar(1),    
    @CompanyCode char(4),
    @CompanyName Varchar(50),        
    @ShortName Varchar(10),    
    @IndustryNature Varchar(100) ,
	@CompanyAddress Varchar(200) , 
    @PhoneNo char(13) , 
    @EmailId Varchar(100) ,    
    @RegistationNo Varchar(20) , 
	@PANNo Varchar(25)  ,
    @TANNo Varchar(50) , 
    @LCNo	 varchar(25) ,    
    @PFNo Varchar(12) ,    
    @LastModifiedBy Varchar(20)  

)    
as     
Begin     
    Insert into tblCompany (IsActive,CompanyCode,CompanyName,ShortName, IndustryNature,CompanyAddress,PhoneNo,EmailId,RegistationNo,PANNo,TANNo,LCNo,PFNo,CreatedDate,LastModifiedBy,LastModifiedDate)     
    Values (@IsActive,@CompanyCode,@CompanyName, @ShortName,@IndustryNature,@CompanyAddress,@PhoneNo,@EmailId,@RegistationNo ,@PANNo,@TANNo, @LCNo,@PFNo,GETDATE(),@LastModifiedBy,GETDATE())     
End
go
--Update Record Company Master
Drop procedure TblCompanyMaster_Upd
go
Create procedure TblCompanyMaster_Upd      
(      
    @IsActive	 varchar(1),    
    @CompanyCode char(4) ,
    @CompanyName Varchar(50),        
    @ShortName Varchar(10) ,    
    @IndustryNature Varchar(100) ,
	@CompanyAddress Varchar(200) , 
    @PhoneNo char(13) , 
    @EmailId Varchar(100) ,    
    @RegistationNo Varchar(20) , 
	@PANNo Varchar(25)  ,
    @TANNo Varchar(50) , 
    @LCNo	 varchar(25) ,    
    @PFNo Varchar(12) ,    
    @LastModifiedBy Varchar(20)  
)      
as      
begin      
   Update tblCompany       
   set IsActive=@IsActive, 
   CompanyName=@CompanyName,      
   ShortName=@ShortName,      
   IndustryNature=@IndustryNature,
   CompanyAddress=@CompanyAddress, 
   PhoneNo=@PhoneNo, 
   EmailId = @EmailId ,    
   RegistationNo=@RegistationNo,
   PANNo=@PANNo,
   TANNo=@TANNo,
   LCNo=@LCNo,
   PFNo=@PFNo, 
   LastModifiedBy=@LastModifiedBy,  
   LastModifiedDate=GETDATE()    
   where CompanyCode=@CompanyCode     
End
go
--Delete Record Company Master
Drop procedure TblCompanyMaster_Del
go
Create procedure TblCompanyMaster_Del     
(      
   @CompanyCode char(4)      
)      
as       
begin      
   Delete from tblCompany where CompanyCode=@CompanyCode     
End
go
--Get all records Company Master
Drop procedure TblCompanyMaster_AllRec
go
Create procedure TblCompanyMaster_AllRec   
as    
Begin    
select Id,ltrim(rtrim(CompanyCode)) as CompanyCode,case when IsActive='Y' then 'Active' else 'InActive' end  AS IsActive ,CompanyName,ShortName,IndustryNature,CompanyAddress,PhoneNo,EmailId,RegistationNo,PANNo,TANNo,PFNo,LCNo ,DCode,TDSCircle,CreatedDate,LastModifiedDate,LastModifiedBy  FROM tblcompany 
End
go
--CheckIsNumber Code Company Master
drop procedure TblCompanyMaster_CheckIsNumberCode
GO
CREATE procedure TblCompanyMaster_CheckIsNumberCode
as      
begin
SELECT count(CompanyCode) cnt  FROM tblCompany where isnumeric(CompanyCode)=0
end
GO
--Get all records Company
Drop procedure TblCompanyMaster_GetData
go
Create procedure TblCompanyMaster_GetData 
(
@CompanyCode varchar(4)
)  
as    
Begin    
   SELECT Id,ltrim(rtrim(CompanyCode)) as CompanyCode,case when IsActive='Y' then 'Y' else 'N' end  AS IsActive ,CompanyName,ShortName,IndustryNature,CompanyAddress,PhoneNo,EmailId,RegistationNo,PANNo,TANNo,PFNo,LCNo ,DCode,TDSCircle,CreatedDate,LastModifiedDate,LastModifiedBy  FROM tblCompany WHERE CompanyCode= @CompanyCode  
End
GO
--CheckIsCompanyCode Company Master
drop procedure TblCompanyMaster_CheckIsCode
GO
CREATE procedure TblCompanyMaster_CheckIsCode
(
@CompanyCode varchar(4)
)  
as      
begin
SELECT count(CompanyCode) cnt  FROM tblCompany where CompanyCode=@CompanyCode  
end
GO
