drop PROCEDURE [dbo].[ProcessPostLeaveThruReason]
go
create PROCEDURE [dbo].[ProcessPostLeaveThruReason]
@ReasonCode VARCHAR(100),
@StrFirstPunchReasonCode VARCHAR(100),
@StrLastPunchReasonCode VARCHAR(100),
@PayCode  VARCHAR(10),
@DateOffice DATETIME

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @TempMLeaveCode CHAR(3)
	DECLARE @TempMLeaveAmount FLOAT
	DECLARE @TempMLeaveType CHAR(1)
	DECLARE @TempMLeaveField CHAR(3)
	DECLARE @TempReasonCount INT
	DECLARE @TempLeaveCount INT
	DECLARE @SQLQuery VARCHAR(250)

	DECLARE @TempReasonCode CHAR(3)
	DECLARE @TempLeaveCode CHAR(3)
	DECLARE @TempLeaveAmount FLOAT
	DECLARE @TempReasonMaster TABLE
	(
		ReasonCode CHAR(3),
		[Description] CHAR(45),
		LeaveCode CHAR(3),
		LeaveValue FLOAT,
		Late CHAR(1),
		Early CHAR(1),
		ExcLunchHours CHAR(1),
		HoursWorked CHAR(1)
	)

	DECLARE @TempLeaveMaster TABLE
	(
		LeaveField CHAR(3),
		LeaveCode CHAR(3),		
		LeaveType CHAr(1)
	)

	INSERT INTO @TempReasonMaster 
		SELECT * FROM tblReasonMaster
		WHERE ReasonCode = @ReasonCode

    SELECT @TempReasonCount = COUNT(*) FROM @TempReasonMaster

	IF @TempReasonCount > 0
	BEGIN 
		SELECT TOP 1
			@TempReasonCode = ReasonCode,
			@TempLeaveAmount = LeaveValue
		FROM @TempReasonMaster

		IF (LEN(RTRIM(LTRIM(@TempReasonCode)))) <= 3 AND (LEN(RTRIM(LTRIM(@TempReasonCode)))) > 0
		BEGIN 
			INSERT INTO @TempLeaveMaster
				SELECT LEAVEFIELD, LEAVECODE, LEAVETYPE
				FROM tblleavemaster(NOLOCK) WHERE LEAVECODE = @ReasonCode

			SELECT  @TempLeaveCount =  COUNT(*) FROM @TempLeaveMaster

			IF @TempLeaveCount > 0
			BEGIN 
				SELECT TOP 1 
					@TempMLeaveCode = LeaveCode,
					@TempMLeaveAmount = @TempLeaveAmount,
					@TempMLeaveField = LeaveField				
				FROM @TempLeaveMaster
				
				-- TODO : Check with Virendra Sir

				SET @SQLQuery = 'Update tblLeaveLedger '+@TempMLeaveField + ' = ' + @TempMLeaveAmount  + ' WHERE PayCode ='+@PayCode 
				EXECUTE(@SQLQuery)	

				-- TODO : Check for RTC, NonRTC
				UPDATE tbltimeregister 
				SET [STATUS] = @TempMLeaveCode,
					LEAVETYPE = @TempMLeaveType,
					LEAVECODE = @TempMLeaveCode,
					LEAVEAMOUNT = @TempMLeaveAmount

				WHERE PAYCODE = @PayCode
				AND DateOFFICE = @DateOffice

			END
		END
	END

	IF @StrFirstPunchReasonCode != ''
	BEGIN
		DELETE FROM @TempReasonMaster
		INSERT INTO @TempReasonMaster 
		SELECT * FROM tblReasonMaster
		WHERE ReasonCode = @StrFirstPunchReasonCode

		SELECT @TempReasonCount = COUNT(*) FROM @TempReasonMaster

		IF @TempReasonCount = 1
		BEGIN 
			SELECT TOP 1
				@TempReasonCode = ReasonCode,
				@TempLeaveAmount = LeaveValue,
				@TempLeaveCode = ISNULL(LeaveCode,'')
			FROM @TempReasonMaster

			IF @TempLeaveCode != '' AND (LEN(LTRIM(RTRIM(@TempLeaveCode)))) > 0
			BEGIN
				UPDATE tbltimeregister 
				SET LATEARRIVAL = 0
				WHERE PAYCODE = @PayCode
				AND DateOFFICE = @DateOffice
			END 
		END
	END

	IF @StrLastPunchReasonCode != ''
	BEGIN
		DELETE FROM @TempReasonMaster
		INSERT INTO @TempReasonMaster 
		SELECT * FROM tblReasonMaster
		WHERE ReasonCode = @StrLastPunchReasonCode

		SELECT @TempReasonCount = COUNT(*) FROM @TempReasonMaster

		IF @TempReasonCount = 1
		BEGIN 
			SELECT TOP 1
				@TempReasonCode = ReasonCode,
				@TempLeaveAmount = LeaveValue,
				@TempLeaveCode = ISNULL(LeaveCode,'')
			FROM @TempReasonMaster

			IF @TempLeaveCode != '' AND (LEN(LTRIM(RTRIM(@TempLeaveCode)))) > 0
			BEGIN
				UPDATE tbltimeregister 
				SET EARLYDEPARTURE = 0
				WHERE PAYCODE = @PayCode
				AND DateOFFICE = @DateOffice
			END 
		END
	END
END








