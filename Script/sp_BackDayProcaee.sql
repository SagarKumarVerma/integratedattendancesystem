
ALTER PROCEDURE [dbo].[ProcessBackDate]
	@FromDate DATETIME,
	@ToDate DATETIME,
	@CompanyCode VARCHAR(4),
	@DepartmentCode CHAR(3),
	@PayCode VARCHAR(10),
	@SetupId INT
AS
BEGIN
	
	SET NOCOUNT ON;

	DECLARE @TempInOUT Char(1)	

    IF @PayCode != ''
	BEGIN
		UPDATE machinerawpunch 
		SET p_day='N' 
		WHERE companycode=@CompanyCode 
		AND officepunch >= @FromDate
		AND PAYCODE = @PayCode
	END
	ELSE
	BEGIN
		UPDATE machinerawpunch 
		SET p_day='N' 
		WHERE companycode=@CompanyCode and CAST(CONVERT(varchar(10), officepunch, 101) AS DATE)   >=  CAST(CONVERT(varchar(10), @FromDate, 101) AS DATE) 
		
	END
	
	SELECT @TempInOUT=INOUT FROM tblsetup (nolock)-- where CompanyCode=@CompanyCode
	
	SET @FromDate = CAST(CONVERT(varchar(10), @FromDate, 101) AS DATE)	
	IF @ToDate = ''
	BEGIN
		SET @ToDate = CONVERT(DATETIME, CONVERT(varchar(11),GETDATE(), 101 ) + ' 23:59:59', 101)
		--SET @ToDate =	CAST(CONVERT(varchar(10), GETDATE(), 101) AS DATE)		
	END
	ELSE
	BEGIN
		SET @ToDate = CONVERT(DATETIME, CONVERT(varchar(11),@ToDate, 101 ) + ' 23:59:59', 101)		
	END
	
	
	-- TODO - Use for loop, pass set up id
	EXEC ProcessAllRecords @FromDate,@ToDate, @CompanyCode, @PayCode, @SetupId

END







