CREATE TABLE [dbo].[tblGrade](
	[GradeCode] [char](3) NOT NULL,
	[GradeName] [char](45) NULL,
	[PERMISLATEARRIVAL] [int] NULL,
	[Grade] [int] NULL,
	[CompanyCode] [varchar](4) NOT NULL,
 CONSTRAINT [PK_tblGrade] PRIMARY KEY CLUSTERED 
(
	[GradeCode] ASC,
	[CompanyCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
Drop procedure TblGradeMaster_Add
go
Create procedure TblGradeMaster_Add     
(   
    @GradeCode char(5),
    @GradeName nvarchar(50),
	@CompanyCode Varchar(4),
    @IsActive Varchar(1),
    @LastModifiedBy Varchar(20)
)    
as     
Begin     
    Insert into tblGrade (GradeCode,GradeName,CompanyCode,IsActive,CreatedDate,LastModifiedBy,LastModifiedDate)     
    Values (@GradeCode,@GradeName,@CompanyCode,@IsActive,GETDATE(),@LastModifiedBy,GETDATE())     
End

go
Drop procedure TblGradeMaster_Upd
go
Create procedure TblGradeMaster_Upd      
(      
    @GradeCode char(5),
    @GradeName nvarchar(50),        
    @IsActive Varchar(1),    
	@CompanyCode Varchar(4),
    @LastModifiedBy Varchar(20) 
)      
as      
begin      
   Update tblGrade       
   set GradeName=@GradeName,          
   IsActive=@IsActive,    
   LastModifiedBy=@LastModifiedBy,  
   LastModifiedDate=GETDATE()   
   where GradeCode=@GradeCode      
End
go
Drop procedure TblGradeMaster_Del
go
Create procedure TblGradeMaster_Del     
(      
    @GradeCode char(5),
	@CompanyCode char(4)
)      
as       
begin      
   Delete from tblGrade where GradeCode =@GradeCode      
End

go
Drop procedure TblGradeMaster_AllRec
go
Create procedure TblGradeMaster_AllRec   
as    
Begin    
select ltrim(rtrim(GradeCode))GradeCode,GradeName,Companycode,case when IsActive='Y' then 'Active' else 'InActive' end  As IsActive,CreatedDate ,LastModifiedBy,LastModifiedDate from tblGrade     
End
go
Drop procedure TblGradeMaster_GetGradeData 
go
Create procedure TblGradeMaster_GetGradeData 
(
 @GradeCode char(5)   
)  
as    
Begin    
   SELECT * FROM tblGrade where GradeCode= @GradeCode   
End

GO
Drop procedure TblGradeMaster_CatNum 
go
Create procedure TblGradeMaster_CatNum 
(
 @GradeCode char(5)    
)  
as    
Begin    
     SELECT top 1 GradeCode FROM tblGrade order by GradeCode desc

End
GO
--CheckIsGradeCode Grade Master
drop procedure TblGradeMaster_CheckIsCode
GO
CREATE procedure TblGradeMaster_CheckIsCode
(
@GradeCode varchar(5)
)  
as      
begin
SELECT count(GradeCode) cnt  FROM tblGrade where GradeCode=@GradeCode 
end
GO