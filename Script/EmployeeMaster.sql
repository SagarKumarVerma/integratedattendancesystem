CREATE TABLE [dbo].[tblemployee](
	[ACTIVE] [char](1) NULL,
	[PAYCODE] [char](10) NOT NULL,
	[EMPNAME] [char](25) NULL,
	[GUARDIANNAME] [char](30) NULL,
	[DateOFBIRTH] [datetime] NULL,
	[DateOFJOIN] [datetime] NULL,
	[PRESENTCARDNO] [char](8) NOT NULL,
	[COMPANYCODE] [char](4) NULL,
	[DivisionCode] [char](3) NULL,
	[CAT] [char](3) NULL,
	[SEX] [char](1) NULL,
	[ISMARRIED] [char](1) NULL,
	[BUS] [char](10) NULL,
	[QUALIFICATION] [char](20) NULL,
	[EXPERIENCE] [char](20) NULL,
	[DESIGNATION] [char](50) NULL,
	[ADDRESS1] [char](100) NULL,
	[PINCODE1] [char](8) NULL,
	[TELEPHONE1] [char](20) NULL,
	[E_MAIL1] [char](50) NULL,
	[ADDRESS2] [char](100) NULL,
	[PINCODE2] [char](10) NULL,
	[TELEPHONE2] [char](20) NULL,
	[BLOODGROUP] [char](3) NULL,
	[EMPPHOTO] [char](100) NULL,
	[EMPSIGNATURE] [char](100) NULL,
	[DepartmentCode] [char](3) NULL,
	[GradeCode] [char](3) NULL,
	[Leavingdate] [datetime] NULL,
	[LeavingReason] [char](35) NULL,
	[VehicleNo] [char](15) NULL,
	[PFNO] [char](12) NULL,
	[PF_NO] [numeric](5, 0) NULL,
	[ESINO] [char](12) NULL,
	[AUTHORISEDMACHINE] [char](150) NULL,
	[EMPTYPE] [char](1) NULL,
	[BankAcc] [char](25) NULL,
	[bankCODE] [char](3) NULL,
	[Reporting1] [varchar](10) NULL,
	[Reporting2] [varchar](10) NULL,
	[BRANCHCODE] [char](3) NULL,
	[DESPANSARYCODE] [char](3) NULL,
	[headid] [char](10) NULL,
	[Email_CC] [varchar](100) NULL,
	[LeaveApprovalStages] [char](2) NULL,
	[IsFixedShift] [char](1) NULL,
	[involuntary] [nvarchar](2) NULL,
	[EmpBioData] [varchar](200) NULL,
	[dateofexp] [datetime] NULL,
	[Disable] [char](1) NULL,
	[Group_Index] [char](1) NULL,
	[OnRoll] [char](1) NULL,
	[teamid] [char](5) NULL,
	[PSCODE] [char](5) NULL,
	[SUBAREACODE] [char](10) NULL,
	[SECTIONID] [char](10) NULL,
	[POSITIONCODE] [char](10) NULL,
	[trainer] [char](1) NULL,
	[trainee] [char](1) NULL,
	[TraineeLevel] [char](1) NULL,
	[RockWellid] [char](20) NULL,
	[Template_Send] [char](1) NULL,
	[Grade] [int] NULL,
	[headid_2] [varchar](10) NULL,
	[head_id] [char](10) NULL,
	[HOD_Codd] [varchar](10) NULL,
	[HOD_Code] [varchar](10) NULL,
	[logid] [bigint] NULL,
	[GroupCode] [char](3) NULL,
	[ClassCode] [char](3) NULL,
	[ReligionCode] [char](3) NULL,
	[ValidityStartDate] [datetime] NULL,
	[ValidityEndDate] [datetime] NULL,
	[SSN] [varchar](100) NOT NULL,
	[DeviceGroupID] [int] NULL,
	[ClientID] [int] NULL,
	[GroupID] [varchar](3) NULL,
CONSTRAINT [PK_tblemployee] PRIMARY KEY CLUSTERED 
(
	[PRESENTCARDNO] ASC,
	[SSN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblemployee] ADD  CONSTRAINT [DF__tblemploy__Templ__638EB5B2]  DEFAULT ('N') FOR [Template_Send]
GO
ALTER TABLE [dbo].[tblemployee] ADD  CONSTRAINT [DF__tblemploy__Grade__6576FE24]  DEFAULT ((15)) FOR [Grade]
GO
Alter table  tblemployee alter column Empphoto nvarchar (max)
go
Alter table  tblemployee alter column Empsignature nvarchar (max)
go
Drop procedure tblemployee_Add
go
Create procedure tblemployee_Add
(  
	@ACTIVE char(1),
	@PAYCODE char(10),
	@EMPNAME char(25),
	@GUARDIANNAME char(30),
	@DateOFBIRTH datetime,
	@DateOFJOIN datetime ,
	@PRESENTCARDNO char(8),
	@COMPANYCODE char(4),
	@DivisionCode char(3),
	@CAT char(3),
	@SEX char(1),
	@ISMARRIED char(1),
	@QUALIFICATION char(20),
	@EXPERIENCE char(20),
	@DESIGNATION char(50),
	@ADDRESS1 char(100),
	@PINCODE1 char(8),
	@TELEPHONE1 char(20),
	@E_MAIL1 char(50),
	@ADDRESS2 char(100),
	@PINCODE2 char(10),
	@TELEPHONE2 char(20),
	@BLOODGROUP char(3),
	@EMPPHOTO nvarchar(max),
	@EMPSIGNATURE nvarchar(max),
	@DepartmentCode char(3),
	@GradeCode char(3),	
	@BankAcc char(25),
	@bankCODE char(5),	
	@headid char(10),	
	@SSN varchar(100),	
	@GroupID varchar(3),
	@UID char(12),
	@PANNO char(10)
)    
as     
Begin     
    Insert into tblemployee (ACTIVE,PAYCODE,EMPNAME,GUARDIANNAME,DateOFBIRTH,DateOFJOIN,PRESENTCARDNO,COMPANYCODE,DivisionCode,CAT,SEX,ISMARRIED,QUALIFICATION,EXPERIENCE,DESIGNATION,ADDRESS1,PINCODE1,TELEPHONE1,E_MAIL1,ADDRESS2,PINCODE2,TELEPHONE2,BLOODGROUP,EMPPHOTO,EMPSIGNATURE,DepartmentCode,GradeCode,BankAcc,bankCODE,headid,SSN,GroupID,UID,PANNO)     
    Values (@ACTIVE,@PAYCODE,@EMPNAME,@GUARDIANNAME,convert(datetime,@DateOFBIRTH,103),convert(datetime,@DateOFJOIN,103),@PRESENTCARDNO,@COMPANYCODE,@DivisionCode,@CAT,@SEX,@ISMARRIED,@QUALIFICATION,@EXPERIENCE,@DESIGNATION,@ADDRESS1,@PINCODE1,@TELEPHONE1,@E_MAIL1,@ADDRESS2,@PINCODE2,@TELEPHONE2,@BLOODGROUP,@EMPPHOTO,@EMPSIGNATURE,@DepartmentCode,@GradeCode,@BankAcc,@bankCODE,@headid,@SSN,@GroupID,,@UID,@PANNO)     
End
go
Drop procedure Employee_SingleList
go
CREATE procedure Employee_SingleList
(
@SSN varchar(100)
)  
as      
begin
select ACTIVE,PAYCODE,EMPNAME,GUARDIANNAME,convert(varchar(10),DateOFBIRTH,103)DateOFBIRTH,convert(varchar(10),DateOFJOIN,103)DateOFJOIN,PRESENTCARDNO,COMPANYCODE,DivisionCode,CAT,SEX,ISMARRIED,BUS,QUALIFICATION,EXPERIENCE,DESIGNATION,ADDRESS1,PINCODE1,TELEPHONE1,E_MAIL1,ADDRESS2,PINCODE2,TELEPHONE2,BLOODGROUP,EMPPHOTO,EMPSIGNATURE,DepartmentCode,GradeCode,isnull(Leavingdate,convert(varchar(10),GETDATE(),103)),LeavingReason,VehicleNo,PFNO,PF_NO,ESINO,EMPTYPE,BankAcc,bankCODE,BRANCHCODE,DESPANSARYCODE,headid,Email_CC,LeaveApprovalStages,RockWellid,Grade,head_id,ValidityStartDate,ValidityEndDate,SSN,DeviceGroupID,ClientID,GroupID,UID,PANNO,Leavingdate,LeavingReason from tblemployee where SSN=@SSN
end
go
drop procedure TblEmployeeMaster_CheckIsCode
GO
CREATE procedure TblEmployeeMaster_CheckIsCode
(
@Paycode varchar(4)
)  
as      
begin
SELECT count(paycode) cnt  FROM tblemployee where Paycode=@Paycode  
end
GO
Drop procedure TblEmployeeMaster_Del
go
Create procedure TblEmployeeMaster_Del     
(      
   @SSN varchar(100)  
)      
as       
begin      
   Delete from tblemployee where SSN=@SSN
End
go




































































--Delete Record Shift Master
Drop procedure tblemployee_Del
go
Create procedure tblemployee_Del     
(      
   @SSN char(10)
)      
as       
begin      
   Delete from tblemployee where SSN=@SSN   
End
go
--Get all records Shift Master
Drop procedure tblemployee_AllRec
go
Create procedure tblemployee_AllRec   
as    
Begin    
--select ltrim(rtrim(CompanyCode)) as CompanyCode, ltrim(rtrim(SHIFT)) as SHIFT,STARTTIME,ENDTIME,LUNCHTIME,LUNCHDURATION,LUNCHENDTIME,OTSTARTAFTER,OTDEDUCTHRS,LUNCHDEDUCTION,SHIFTPOSITION,SHIFTDURATION,OTDEDUCTAFTER,CompanyCode,ShiftDiscription  FROM tblemployee 

select ACTIVE,PAYCODE,EMPNAME,GUARDIANNAME,convert(varchar(10),DateOFBIRTH,103)DateOFBIRTH,convert(varchar(10),DateOFJOIN,103)DateOFJOIN,PRESENTCARDNO,ltrim(rtrim(CompanyCode))COMPANYCODE,ltrim(rtrim(DivisionCode))DivisionCode,ltrim(rtrim(CAT))CAT,SEX,ISMARRIED,BUS,QUALIFICATION,EXPERIENCE,ltrim(rtrim(DESIGNATION))DESIGNATION,ADDRESS1,PINCODE1,TELEPHONE1,E_MAIL1,ADDRESS2,PINCODE2,TELEPHONE2,BLOODGROUP,EMPPHOTO,EMPSIGNATURE,ltrim(rtrim(DepartmentCode))DepartmentCode,ltrim(rtrim(GradeCode))GradeCode,Leavingdate,LeavingReason,VehicleNo,PFNO,PF_NO,ESINO,EMPTYPE,BankAcc,bankCODE,ltrim(rtrim(BRANCHCODE))BRANCHCODE,ltrim(rtrim(DESPANSARYCODE))DESPANSARYCODE,headid,Email_CC,LeaveApprovalStages,RockWellid,Grade,head_id,ValidityStartDate,ValidityEndDate,SSN,DeviceGroupID,ClientID,GroupID,UID,PANNO from tblemployee
End
go

--CheckIsCompanyCode Company Master
drop procedure tblemployee_CheckIsCode
GO
CREATE procedure tblemployee_CheckIsCode
(
@Shift varchar(4),
@CompanyCode varchar(4)
)  
as      
begin
SELECT count(SHIFT) cnt  FROM tblemployee where SHIFT=@SHIFT and CompanyCode=@CompanyCode  
end
GO


