CREATE TABLE [dbo].[tblDivision](
	[Divisioncode] [char](3) NOT NULL,
	[DivisionName] [char](45) NULL,
	[CompanyCode] [varchar](4) NOT NULL,
	[CreatedDate] [DATETIME] NULL,
    [LastModifiedDate] [DATETIME]  NULL,
	[LastModifiedBy] varchar(50),
CONSTRAINT [PK_tblDivision] PRIMARY KEY CLUSTERED 
(
	[Divisioncode] ASC,
	[CompanyCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

Drop procedure TblDivisionMaster_Add
go
Create procedure TblDivisionMaster_Add     
(   
    @DivisionCode char(3),
    @DivisionName nvarchar(100),        
    @CompanyCode varchar(4),
    @LastModifiedBy Varchar(20)   
)    
as     
Begin     
    Insert into tblDivision (DivisionCode,DivisionName,CreatedDate,CompanyCode,LastModifiedBy,LastModifiedDate)     
    Values (@DivisionCode,@DivisionName,GETDATE(),@CompanyCode,@LastModifiedBy,GETDATE())     
End

go

go
Drop procedure TblDivisionMaster_Upd
go
Create procedure TblDivisionMaster_Upd      
(      
    @DivisionCode char(3),
    @DivisionName nvarchar(100),        
    @CompanyCode varchar(4),
    @LastModifiedBy Varchar(20)  
)      
as      
begin      
   Update tblDivision       
   set DivisionName=@DivisionName,          
   DivisionName=@DivisionName, 
   CompanyCode=@CompanyCode, 
   LastModifiedBy=@LastModifiedBy,  
   LastModifiedDate=GETDATE()   
   where DivisionCode=@DivisionCode      
End
go
Drop procedure TblDivisionMaster_Del
go
Create procedure TblDivisionMaster_Del     
(      
    @DivisionCode char(3)    
)      
as       
begin      
   Delete from tblDivision where DivisionCode =@DivisionCode      
End
go

