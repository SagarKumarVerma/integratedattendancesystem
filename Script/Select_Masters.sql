--Get all records CompanyMaster
Drop procedure TblCompanyMaster_SelectAll
go
Create procedure TblCompanyMaster_SelectAll 
as    
Begin    
   SELECT * FROM tblcompany 
End
go
--Get all records DepartmentMaster
Drop procedure TblDepartmentMaster_SelectAll
go
Create procedure TblDepartmentMaster_SelectAll 
as    
Begin    
   SELECT * FROM tbldepartment 
End
go
--Get all records GroupMaster
Drop procedure TblGroupMaster_SelectAll
go
Create procedure TblGroupMaster_SelectAll 
as    
Begin    
   SELECT * FROM tblGroup 
End
go
--Get all records DesignationMaster
Drop procedure TblDesignation_SelectAll
go
Create procedure TblDesignation_SelectAll 
as    
Begin    
   SELECT * FROM TblDesignation
End
go
--Get all records LocationMaster
Drop procedure TblLocation_SelectAll
go
Create procedure TblLocation_SelectAll 
as    
Begin    
   SELECT * FROM [tblDivision]
End
go

--Get all records CategoryMaster
Drop procedure TblCategory_SelectAll
go
Create procedure TblCategory_SelectAll
as
Begin
   SELECT * FROM tblCategory
End
go

--Get all records BankMaster
Drop procedure TblBank_SelectAll
go
Create procedure TblBank_SelectAll
as
Begin
   SELECT * FROM tblBank
End
go

--Get all records HODMaster
Drop procedure TblHOD_SelectAll
go
Create procedure TblHOD_SelectAll
as
Begin
   SELECT * FROM tblHOD
End
go

--Get all records Employee Group
Drop procedure TblEmployeeGroup_SelectAll
go
Create procedure TblEmployeeGroup_SelectAll
as
Begin
   SELECT * FROM [tblEmployeeGroupPolicy]
End
go

--Get all records Employee Group
Drop procedure tblShiftmaster_SelectAll
go
Create procedure tblShiftmaster_SelectAll
as
Begin
   SELECT shift+'-'+(select convert(varchar(5),STARTTIME,108))+'-'+(select convert(varchar(5),ENDTIME,108))shiftname,* FROM [tblShiftmaster]
End
go
Drop procedure TblGrade_SelectAll
go
Create procedure TblGrade_SelectAll
as
Begin
   SELECT * FROM tblGrade
End
go
Drop procedure TblEmployeeMaster_SelectAll
go
Create procedure TblEmployeeMaster_SelectAll
as
Begin
   SELECT LTRIM(RTRIM(PAYCODE))EmployeeCode,(LTRIM(RTRIM(PAYCODE))+' - '+LTRIM(RTRIM(EMPNAME)))EmployeeName FROM tblemployee 
End
go

