CREATE TABLE [dbo].[tblEmployeeShiftMaster](
	[PAYCODE] [char](10) NOT NULL,
	[CARDNO] [char](8) NOT NULL,
	[SHIFT] [char](3) NULL,
	[SHIFTTYPE] [char](1) NULL,
	[SHIFTPATTERN] [char](11) NULL,
	[SHIFTREMAINDAYS] [int] NULL,
	[LASTSHIFTPERFORMED] [char](3) NULL,
	[INONLY] [char](1) NULL,
	[ISPUNCHALL] [char](1) NULL,
	[ISTIMELOSSALLOWED] [char](1) NULL,
	[ALTERNATE_OFF_DAYS] [char](10) NULL,
	[CDAYS] [float] NULL,
	[ISROUNDTHECLOCKWORK] [char](1) NULL,
	[ISOT] [char](1) NULL,
	[OTRATE] [char](6) NULL,
	[FIRSTOFFDAY] [char](3) NULL,
	[SECONDOFFTYPE] [char](1) NULL,
	[HALFDAYSHIFT] [char](3) NULL,
	[SECONDOFFDAY] [char](3) NULL,
	[PERMISLATEARRIVAL] [int] NULL,
	[PERMISEARLYDEPRT] [int] NULL,
	[ISAUTOSHIFT] [char](1) NULL,
	[ISOUTWORK] [char](1) NULL,
	[MAXDAYMIN] [float] NULL,
	[ISOS] [char](1) NULL,
	[AUTH_SHIFTS] [char](50) NULL,
	[TIME] [int] NULL,
	[SHORT] [int] NULL,
	[HALF] [int] NULL,
	[ISHALFDAY] [char](1) NULL,
	[ISSHORT] [char](1) NULL,
	[TWO] [char](1) NULL,
	[isReleaver] [char](1) NULL,
	[isWorker] [char](1) NULL,
	[isFlexi] [char](1) NULL,
	[SSN] [varchar](100) NOT NULL,
	[MIS] [char](1) NULL,
	[IsCOF] [char](1) NULL,
	[HLFAfter] [int] NULL,
	[HLFBefore] [int] NULL,
	[ResignedAfter] [int] NULL,
	[EnableAutoResign] [char](1) NULL,
	[S_END] [datetime] NULL,
	[S_OUT] [datetime] NULL,
	[AUTOSHIFT_LOW] [int] NULL,
	[AUTOSHIFT_UP] [int] NULL,
	[ISPRESENTONWOPRESENT] [char](1) NULL,
	[ISPRESENTONHLDPRESENT] [char](1) NULL,
	[NightShiftFourPunch] [char](1) NULL,
	[ISAUTOABSENT] [char](1) NULL,
	[ISAWA] [char](1) NULL,
	[ISWA] [char](1) NULL,
	[ISAW] [char](1) NULL,
	[ISPREWO] [char](1) NULL,
	[ISOTOUTMINUSSHIFTENDTIME] [char](1) NULL,
	[ISOTWRKGHRSMINUSSHIFTHRS] [char](1) NULL,
	[ISOTEARLYCOMEPLUSLATEDEP] [char](1) NULL,
	[DEDUCTHOLIDAYOT] [int] NULL,
	[DEDUCTWOOT] [int] NULL,
	[ISOTEARLYCOMING] [char](1) NULL,
	[OTMinus] [char](1) NULL,
	[OTROUND] [char](1) NULL,
	[OTEARLYDUR] [int] NULL,
	[OTLATECOMINGDUR] [int] NULL,
	[OTRESTRICTENDDUR] [int] NULL,
	[DUPLICATECHECKMIN] [int] NULL,
	[PREWO] [int] NULL,
 CONSTRAINT [PK_tblEmployeeShiftMaster] PRIMARY KEY CLUSTERED 
(
	[CARDNO] ASC,
	[SSN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[tblEmployeeShiftMaster] ADD  CONSTRAINT [DF__tblEmploye__TIME__07020F21]  DEFAULT ((0)) FOR [TIME]
GO

ALTER TABLE [dbo].[tblEmployeeShiftMaster] ADD  CONSTRAINT [DF__tblEmploy__SHORT__07F6335A]  DEFAULT ((0)) FOR [SHORT]
GO

ALTER TABLE [dbo].[tblEmployeeShiftMaster] ADD  CONSTRAINT [DF__tblEmploye__HALF__08EA5793]  DEFAULT ((0)) FOR [HALF]
GO