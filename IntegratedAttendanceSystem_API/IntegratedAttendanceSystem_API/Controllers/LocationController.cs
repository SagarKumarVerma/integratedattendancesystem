﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Helpers;
using IntegratedAttendanceSystem_API.Models;
using IntegratedAttendanceSystem_API.DataAccess;

namespace IntegratedAttendanceSystem_API.Controllers
{
    public class LocationController : ApiController
    {
        LocationAccessModel objLocation = new LocationAccessModel();

        [Route("Location/GetSpecRec")]
        [HttpPost]
        public IHttpActionResult GetbyCode([FromBody]LocationModel LocationMst)
        {
            try
            {
                var LocationMstVar = objLocation.GetLocationData(LocationMst.LocationCode.Trim());
                return Ok(LocationMstVar);
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return BadRequest();
            }

        }

        [Route("Location/GetLoccode")]
        [HttpPost]
        public IHttpActionResult GetbyLoccode([FromBody]LocationModel LocationMst)
        {
            try
            {
                int result = 0;
                result = objLocation.GetLocationCode(LocationMst.LocationCode.Trim());
                if (result == 0)
                {
                    return Ok();
                }
                else if (result > 0)
                {
                    return Ok(1);
                }
                else
                {
                    return BadRequest();
                }
                //var CompMst = objCompany.GetCompanyCode(Company.Code.Trim());
                //return Ok(CompMst);
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return BadRequest();
            }
        }
        [Route("Location/GetAllList")]
        [HttpPost]
        public IHttpActionResult ListAllLocation([FromBody]LocationModel LocationMst)
        {
            try
            {
                return Ok(objLocation.GetAllLocation());
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return BadRequest();
            }

        }
        [Route("Location/InsertRecord")]
        [HttpPost]
        public IHttpActionResult Add([FromBody]LocationModel LocationMst)
        {
            try
            {
                int result = 0;
                result = objLocation.Add(LocationMst);
                if (result > 0)
                {
                    return Ok();
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);

                return BadRequest();
            }

        }
        [Route("Location/UpdateRecord")]
        [HttpPost]
        public IHttpActionResult Update([FromBody]LocationModel LocationMst)
        {
            try
            {
                int result = 0;
                result = objLocation.Update(LocationMst);
                if (result > 0)
                {
                    return Ok();
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return BadRequest();
            }
        }
        [Route("Location/DeleteRecord")]
        [HttpPost]
        public IHttpActionResult Delete([FromBody]LocationModel LocationMst)
        {
            int result = 0;
            try
            {
                result = objLocation.Delete(LocationMst);
                if (result > 0)
                {
                    return Ok();
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return BadRequest();
            }

        }
    }
}
