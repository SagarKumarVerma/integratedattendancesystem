﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Helpers;
using IntegratedAttendanceSystem_API.Models;
using IntegratedAttendanceSystem_API.DataAccess;

namespace IntegratedAttendanceSystem_API.Controllers
{
    public class DeviceController : ApiController
    {
        DeviceAccessModel DeptMst = new DeviceAccessModel();

        /// <summary>
        /// use this method to get specific record based on ID.
        /// </summary>
        /// <param name="ID">
        /// ID is like a key to get the data.
        /// </param>
        /// 


        [Route("Device/GetAllDevice")]
        [HttpPost]
        public IHttpActionResult GetAllDevice([FromBody]DeviceModel Dept)
        {
            try
            {
                return Ok(DeptMst.GetAllDevice());
            }
            catch (Exception ex)
            {
                ex.ToString();
                return BadRequest();
            }
        }


        [Route("Device/AddDevice")]
        [HttpPost]
        public IHttpActionResult Add([FromBody]DeviceModel Dept)
        {
            try
            {
                int result = 0;
                result = DeptMst.Add(Dept);
                if (result > 0)
                {
                    return Ok();
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                ex.ToString();

                return BadRequest();
            }
        }

        [Route("Device/UpdateDevice")]
        [HttpPost]
        public IHttpActionResult Update([FromBody]DeviceModel Dept)
        {
            try
            {
                int result = 0;
                result = DeptMst.Update(Dept);
                if (result > 0)
                {
                    return Ok();
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                return BadRequest();
            }
        }

        [Route("Device/DeleteDevice")]
        [HttpPost]
        public IHttpActionResult Delete([FromBody]DeviceModel Dept)
        {
            int result = 0;
            try
            {
                result = DeptMst.Delete(Dept);
                if (result > 0)
                {
                    return Ok();
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                return BadRequest();
            }
        }

        //================================

        [Route("Device/GetAutoID")]
        [HttpPost]
        public IHttpActionResult GetAutoGenerateID([FromBody]DeviceModel Dept)
        {
            try
            {
                DeviceModel objDeviceModel = new DeviceModel();
                var lastcode = DeptMst.GetAllDevice().ToList().OrderByDescending(m => m.DeviceCode).FirstOrDefault();
                if (lastcode == null)
                {
                    return Ok("00001");
                }
                else
                {
                    return Ok((Convert.ToInt32(lastcode.DeviceCode) + 1).ToString("D5").PadLeft(0));
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                return BadRequest();
            }
        }

        [Route("Device/GetSpecRec")]
        [HttpPost]
        public IHttpActionResult GetbyID([FromBody]DeviceModel Dept)
        {
            try
            {
                var DeptMstM = DeptMst.GetDeviceId(Dept.DeviceCode.Trim());
                return Ok(DeptMstM);
            }
            catch (Exception ex)
            {
                ex.ToString();
                return BadRequest();
            }
        }
        [Route("Device/GetDeptcode")]
        [HttpPost]
        public IHttpActionResult GetbyDeptcode([FromBody]DeviceModel Dept)
        {
            try
            {
                int result = 0;
                result = DeptMst.GetDeviceCode(Dept.DeviceCode.Trim());
                if (result == 0)
                {
                    return Ok();
                }
                else if (result > 0)
                {
                    return Ok(1);
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return BadRequest();
            }
        }
        //=================================
    }
}
