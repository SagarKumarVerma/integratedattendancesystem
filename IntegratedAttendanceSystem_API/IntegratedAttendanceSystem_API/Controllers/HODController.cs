﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Helpers;
using IntegratedAttendanceSystem_API.Models;
using IntegratedAttendanceSystem_API.DataAccess;

namespace IntegratedAttendanceSystem_API.Controllers
{
    public class HODController : ApiController
    {
        HODAccessModel objHOD = new HODAccessModel();

        [Route("HOD/GetSpecRec")]
        [HttpPost]
        public IHttpActionResult GetbyCode([FromBody]HODModel HODMst)
        {
            try
            {
                var HODMstVar = objHOD.GetHODData(HODMst.HODCode.Trim());
                return Ok(HODMstVar);
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return BadRequest();
            }

        }
        [Route("HOD/GetHODcode")]
        [HttpPost]
        public IHttpActionResult GetbyCompcode([FromBody]HODModel HODMst)
        {
            try
            {
                int result = 0;
                result = objHOD.GetHODCode(HODMst.HODCode.Trim());
                if (result == 0)
                {
                    return Ok();
                }
                else if (result > 0)
                {
                    return Ok(1);
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return BadRequest();
            }
        }
        [Route("HOD/GetAllList")]
        [HttpPost]
        public IHttpActionResult ListAllHOD([FromBody]HODModel HODMst)
        {
            try
            {
                return Ok(objHOD.GetAllHOD());
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return BadRequest();
            }
        }
        [Route("HOD/InsertRecord")]
        [HttpPost]
        public IHttpActionResult Add([FromBody]HODModel HODMst)
        {
            try
            {
                int result = 0;
                result = objHOD.Add(HODMst);
                if (result > 0)
                {
                    return Ok();
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);

                return BadRequest();
            }

        }       
        [Route("HOD/UpdateRecord")]
        [HttpPost]
        public IHttpActionResult Update([FromBody]HODModel HODMst)
        {
            try
            {
                int result = 0;
                result = objHOD.Update(HODMst);
                if (result > 0)
                {
                    return Ok();
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return BadRequest();
            }
        }
        [Route("HOD/DeleteRecord")]
        [HttpPost]
        public IHttpActionResult Delete([FromBody]HODModel HODMst)
        {
            int result = 0;
            try
            {
                result = objHOD.Delete(HODMst);
                if (result > 0)
                {
                    return Ok();
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return BadRequest();
            }
        }

        // POST api/values for Emp Details
        [Route("HOD/GetAllEmployeeCodeList")]
        [HttpPost]
        public IHttpActionResult GetAllEmployeeCodeList([FromBody] DataMaintenanceModel DataMntModal)
        {
            try
            {
                return Ok(objHOD.GetAllEmployeeCode());
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return BadRequest();
            }

        }

        [Route("HOD/GetbyHOD")]
        [HttpPost]
        public IHttpActionResult GetbyHOD([FromBody] HODModel HODMst)
        {
            try
            {
                int result = 0;
                result = objHOD.GetHODCode(HODMst.HODCode.Trim());
                if (result == 0)
                {
                    return Ok();
                }
                else if (result > 0)
                {
                    return Ok(1);
                }
                else
                {
                    return BadRequest();
                }
                //var CompMst = objCompany.GetCompanyCode(Company.Code.Trim());
                //return Ok(CompMst);
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return BadRequest();
            }
        }
    }
}
