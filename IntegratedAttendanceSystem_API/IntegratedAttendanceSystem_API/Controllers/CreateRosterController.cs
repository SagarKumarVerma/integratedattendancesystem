﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Helpers;
using IntegratedAttendanceSystem_API.Models;
using IntegratedAttendanceSystem_API.DataAccess;

namespace IntegratedAttendanceSystem_API.Controllers
{
    public class CreateRosterController : ApiController
    {
        RosterAccessModel objRoster = new RosterAccessModel();

        [Route("CreateRoster/GetEmployeeList")]
        [HttpPost]
        public IHttpActionResult ListAllEmployee([FromBody] Employee em)
        {
            try
            {
                EmployeeMasteModelforDDL ddlEMP = new EmployeeMasteModelforDDL();
                return Ok(ddlEMP.GetEmployeeList());
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return BadRequest();
            }
        }


        // POST api/values
        [Route("CreateRoster/CreateRoster")]
        [HttpPost]
        public IHttpActionResult CreateRoster(RosterModel C_Roster)
        {
            try
            {
                int result = 0;
                result = objRoster.CreateRoster(C_Roster);
                if (result != -1)
                {
                    return Ok();
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);

                return BadRequest();
            }
        }
        //public IHttpActionResult CreateTables(RosterModel C_Roster)
        //{
        //    try
        //    {
        //        int result = 0;
        //        result = objRoster.CreateTable(C_Roster);
        //        if (result != -1)
        //        {
        //            return Ok();
        //        }
        //        else
        //        {
        //            return BadRequest();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ExceptionLogging.SendErrorToText(ex);

        //        return BadRequest();
        //    }
        //}

        /// <summary>
        /// use this method to post your data into database.Most suitable for update operation.
        /// </summary>
        /// <param name="Company">
        /// you can change the value parameter with your model object according to requirement.
        /// </param>        

        [Route("CreateRoster/UpdateRoster")]
        [HttpPost]
        public IHttpActionResult Update([FromBody] RosterModel Roster)
        {
            try
            {
                int result = 0;
                result = objRoster.UpdateRoster(Roster);
                if (result > 0)
                {
                    return Ok();
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return BadRequest();
            }

        }
    }
}
