﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Helpers;
using IntegratedAttendanceSystem_API.Models;
using IntegratedAttendanceSystem_API.DataAccess;

namespace IntegratedAttendanceSystem_API.Controllers
{
    public class DesignationController : ApiController
    {
        DesignationAccessModel objDesignation = new DesignationAccessModel();

        [Route("Designation/GetSpecRec")]
        [HttpPost]
        public IHttpActionResult GetbyCode([FromBody]DesignationModel DesignationMst)
        {
            try
            {
                var DesignationMstVar = objDesignation.GetDesignationData(DesignationMst.DesignationCode.Trim());
                return Ok(DesignationMstVar);
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return BadRequest();
            }
        }

        [Route("Designation/GetLoccode")]
        [HttpPost]
        public IHttpActionResult GetbyLoccode([FromBody]DesignationModel DesignationMst)
        {
            try
            {
                int result = 0;
                result = objDesignation.GetDesignationCode(DesignationMst.DesignationCode.Trim());
                if (result == 0)
                {
                    return Ok();
                }
                else if (result > 0)
                {
                    return Ok(1);
                }
                else
                {
                    return BadRequest();
                }
                //var CompMst = objCompany.GetCompanyCode(Company.Code.Trim());
                //return Ok(CompMst);
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return BadRequest();
            }
        }
        [Route("Designation/GetAllDesignation")]
        [HttpPost]
        public IHttpActionResult ListAllDesignation([FromBody]DesignationModel DesignationMst)
        {
            try
            {
                return Ok(objDesignation.GetAllDesignation());
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return BadRequest();
            }

        }
        [Route("Designation/InsertRecord")]
        [HttpPost]
        public IHttpActionResult Add([FromBody]DesignationModel DesignationMst)
        {
            try
            {
                int result = 0;
                result = objDesignation.Add(DesignationMst);
                if (result > 0)
                {
                    return Ok();
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);

                return BadRequest();
            }

        }
        [Route("Designation/UpdateRecord")]
        [HttpPost]
        public IHttpActionResult Update([FromBody]DesignationModel DesignationMst)
        {
            try
            {
                int result = 0;
                result = objDesignation.Update(DesignationMst);
                if (result > 0)
                {
                    return Ok();
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return BadRequest();
            }
        }
        [Route("Designation/DeleteRecord")]
        [HttpPost]
        public IHttpActionResult Delete([FromBody]DesignationModel DesignationMst)
        {
            int result = 0;
            try
            {
                result = objDesignation.Delete(DesignationMst);
                if (result > 0)
                {
                    return Ok();
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return BadRequest();
            }

        }

        [Route("Designation/GetDesignation")]
        [HttpPost]
        public IHttpActionResult GetbyDesignation([FromBody] DesignationModel DesignationMst)
        {
            try
            {
                int result = 0;
                result = objDesignation.GetDesignationCode(DesignationMst.DesignationCode.Trim());
                if (result == 0)
                {
                    return Ok();
                }
                else if (result > 0)
                {
                    return Ok(1);
                }
                else
                {
                    return BadRequest();
                }
                //var CompMst = objCompany.GetCompanyCode(Company.Code.Trim());
                //return Ok(CompMst);
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return BadRequest();
            }
        }
    }
}
