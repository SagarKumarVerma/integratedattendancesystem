﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Helpers;
using IntegratedAttendanceSystem_API.Models;
using IntegratedAttendanceSystem_API.DataAccess;

namespace IntegratedAttendanceSystem_API.Controllers
{
    public class CategoryController : ApiController
    {
        CategoryAccessModel objCategory = new CategoryAccessModel();

        [Route("Category/GetSpecRec")]
        [HttpPost]
        public IHttpActionResult GetbyCode([FromBody]CategoryModel CateMst)
        {
            try
            {
                var CateMstVar = objCategory.GetCategoryData(CateMst.CategoryCode.Trim());
                return Ok(CateMstVar);
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return BadRequest();
            }

        }
        [Route("Category/GetCatecode")]
        [HttpPost]
        public IHttpActionResult GetbyCompcode([FromBody]CategoryModel CateMst)
        {
            try
            {
                int result = 0;
                result = objCategory.GetCategoryCode(CateMst.CategoryCode.Trim());
                if (result == 0)
                {
                    return Ok();
                }
                else if (result > 0)
                {
                    return Ok(1);
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return BadRequest();
            }
        }
        [Route("Category/GetAllList")]
        [HttpPost]
        public IHttpActionResult ListAllCategory([FromBody]CategoryModel CateMst)
        {
            try
            {
                return Ok(objCategory.GetAllCategory());
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return BadRequest();
            }

        }
        [Route("Category/InsertRecord")]
        [HttpPost]
        public IHttpActionResult Add([FromBody]CategoryModel CateMst)
        {
            try
            {
                int result = 0;
                result = objCategory.Add(CateMst);
                if (result > 0)
                {
                    return Ok();
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);

                return BadRequest();
            }

        }       
        [Route("Category/UpdateRecord")]
        [HttpPost]
        public IHttpActionResult Update([FromBody]CategoryModel CateMst)
        {
            try
            {
                int result = 0;
                result = objCategory.Update(CateMst);
                if (result > 0)
                {
                    return Ok();
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return BadRequest();
            }
        }
        [Route("Category/DeleteRecord")]
        [HttpPost]
        public IHttpActionResult Delete([FromBody]CategoryModel CateMst)
        {
            int result = 0;
            try
            {
                result = objCategory.Delete(CateMst);
                if (result > 0)
                {
                    return Ok();
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return BadRequest();
            }

        }
    }
}
