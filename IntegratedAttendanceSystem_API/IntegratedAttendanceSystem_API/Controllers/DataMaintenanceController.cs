﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Helpers;
using IntegratedAttendanceSystem_API.Models;
using IntegratedAttendanceSystem_API.DataAccess;

namespace IntegratedAttendanceSystem_API.Controllers
{
    public class DataMaintenanceController : ApiController
    {
        DataMaintenanceAccessModel objDataMaintenance = new DataMaintenanceAccessModel();

        [Route("DataMaintenance/GetAllEmployeeCodeList")]
        [HttpPost]
        public IHttpActionResult GetAllEmployeeCodeList([FromBody] DataMaintenanceModel DataMntModal)
        {
            try
            {
                return Ok(objDataMaintenance.GetAllEmployeeCode());
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return BadRequest();
            }

        }

        // POST api/values for Emp Details 
        [Route("DataMaintenance/SelectedEmployeecodeList")]
        [HttpPost]
        public IHttpActionResult SelectedEmployeecodeList([FromBody] DataMaintenanceModel DataMntModal)
        {
            try
            {
                return Ok(objDataMaintenance.SelectedEmployeecodeListDetl(DataMntModal.EmpCode.Trim()));
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return BadRequest();
            }
        }

        [Route("DataMaintenance/GetAttList")]
        [HttpPost]
        public IHttpActionResult ListAllDataMaintenance([FromBody] DataMaintenanceModel DataMaintenance)
        {
            try
            {
                return Ok(objDataMaintenance.GetAttList(DataMaintenance));
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return BadRequest();
            }
        }

        [Route("DataMaintenance/GetAllAtt")]
        [HttpPost]
        public IHttpActionResult ListAllAttData([FromBody] DataMaintenanceModel DataMaintenance)
        {
            try
            {
                return Ok(objDataMaintenance.GetAllAtt(DataMaintenance));
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return BadRequest();
            }

        }


        // POST api/values
        [Route("DataMaintenance/InsertManualPunch")]
        [HttpPost]
        public IHttpActionResult Add([FromBody] DataMaintenanceModel mPunch)
        {
            try
            {
                int result = 0;
                result = objDataMaintenance.Add(mPunch);
                if (result > 0)
                {
                    return Ok();
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);

                return BadRequest();
            }
        }


        // POST api/values
        [Route("DataMaintenance/CallBackDateProcess")]
        [HttpPost]
        public IHttpActionResult CallBackDay([FromBody] DataMaintenanceModel mPunch)
        {
            try
            {
                int result = 0;
                result = objDataMaintenance.CallBackDay(mPunch);
                if (result > 0)
                {
                    return Ok();
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);

                return BadRequest();
            }
        }

        

        //[Route("DataMaintenance/GetSpecRec")]
        //[HttpPost]
        //public IHttpActionResult GetbyCode([FromBody]DataMaintenanceModel DataMaintenance)
        //{
        //    try
        //    {
        //        var CompMst = objDataMaintenance.GetAttData(DataMaintenance.Code.Trim());
        //        return Ok(CompMst);
        //    }
        //    catch (Exception ex)
        //    {
        //        ExceptionLogging.SendErrorToText(ex);
        //        return BadRequest();
        //    }
        //}

        //[Route("DataMaintenance/GetCompcode")]
        //[HttpPost]
        //public IHttpActionResult GetbyCompcode([FromBody]DataMaintenanceModel DataMaintenance)
        //{
        //    try
        //    {
        //        int result = 0;
        //        result = objDataMaintenance.GetDataMaintenanceCode(DataMaintenance.Code.Trim());
        //        if (result == 0)
        //        {
        //            return Ok();
        //        }
        //        else if(result > 0)
        //        {
        //            return Ok(1);
        //        }
        //        else
        //        {
        //            return BadRequest();
        //        }
        //        //var CompMst = objDataMaintenance.GetDataMaintenanceCode(DataMaintenance.Code.Trim());
        //        //return Ok(CompMst);
        //    }
        //    catch (Exception ex)
        //    {
        //        ExceptionLogging.SendErrorToText(ex);
        //        return BadRequest();
        //    }
        //}

        //[Route("DataMaintenance/GetAllList")]
        //[HttpPost]
        //public IHttpActionResult ListAllDataMaintenance([FromBody]DataMaintenanceModel DataMaintenance)
        //{
        //    try
        //    {
        //        return Ok(objDataMaintenance.GetAllDataMaintenance());
        //    }
        //    catch (Exception ex)
        //    {
        //        ExceptionLogging.SendErrorToText(ex);
        //        return BadRequest();
        //    }

        //}

        // POST api/values
        //[Route("DataMaintenance/InsertRecord")]
        //[HttpPost]
        //public IHttpActionResult Add([FromBody]DataMaintenanceModel DataMaintenance)
        //{
        //    try
        //    {
        //        int result = 0;
        //        result = objDataMaintenance.Add(DataMaintenance);
        //        if (result > 0)
        //        {
        //            return Ok();
        //        }
        //        else
        //        {
        //            return BadRequest();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ExceptionLogging.SendErrorToText(ex);

        //        return BadRequest();
        //    }
        //}

        /// <summary>
        /// use this method to post your data into database.Most suitable for update operation.
        /// </summary>
        /// <param name="DataMaintenance">
        /// you can change the value parameter with your model object according to requirement.
        /// </param>        

        //[Route("DataMaintenance/UpdateRecord")]
        //[HttpPost]
        //public IHttpActionResult Update([FromBody]DataMaintenanceModel DataMaintenance)
        //{
        //    try
        //    {
        //        int result = 0;
        //        result = objDataMaintenance.Update(DataMaintenance);
        //        if (result > 0)
        //        {
        //            return Ok();
        //        }
        //        else
        //        {
        //            return BadRequest();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ExceptionLogging.SendErrorToText(ex);
        //        return BadRequest();
        //    }

        //}

        /// <summary>
        /// use this method to delete the specific Record.
        /// </summary>
        /// <param name="id">

        /// </param> 
        //[Route("DataMaintenance/DeleteRecord")]
        //[HttpPost]
        //public IHttpActionResult Delete([FromBody]DataMaintenanceModel DataMaintenance)
        //{
        //    int result = 0;
        //    try
        //    {
        //        result = objDataMaintenance.Delete(DataMaintenance);
        //        if (result > 0)
        //        {
        //            return Ok();
        //        }
        //        else
        //        {
        //            return BadRequest();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ExceptionLogging.SendErrorToText(ex);
        //        return BadRequest();
        //    }
        //}
    }
}
