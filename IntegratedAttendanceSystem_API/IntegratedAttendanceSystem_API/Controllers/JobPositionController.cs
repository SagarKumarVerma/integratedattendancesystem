﻿using IntegratedAttendanceSystem_API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace IntegratedAttendanceSystem_API.Controllers
{
    public class EmployeeController : ApiController
    {
        EmployeeAccessModel jpam = new EmployeeAccessModel();
        /// <summary>
        /// use this method for get all group list
        /// </summary>
        /// <returns></returns>
        [Route("JobPos/GetGroupList")]
        [HttpPost]
        public IHttpActionResult ListAlGroup([FromBody]Employee jpm)
        {
            try
            {
                GroupMasteModel gm = new GroupMasteModel();
                return Ok(gm.GetGroupList());
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return BadRequest();
            }

        }
        [Route("JobPos/GetCompanyList")]
        [HttpPost]
        public IHttpActionResult ListAllCompany([FromBody]Employee jpm)
        {
            try
            {
                CompanyMasteModel cm = new CompanyMasteModel();
                return Ok(cm.GetCompanyList());
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return BadRequest();
            }

        }
        [Route("JobPos/GetDepartmentList")]
        [HttpPost]
        public IHttpActionResult ListAllDepartment([FromBody]Employee jpm)
        {
            try
            {
                DepartmentMasteModel dm = new DepartmentMasteModel();
                return Ok(dm.GetDepartmentList());
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return BadRequest();
            }

        }
        [Route("JobPos/GetDesignationList")]
        [HttpPost]
        public IHttpActionResult ListDesignation([FromBody]Employee jpm)
        {
            try
            {
                DesignationMasterModel dm = new DesignationMasterModel();
                return Ok(dm.GetDesignationList());
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return BadRequest();
            }

        }
        [Route("JobPos/GetJDList")]
        [HttpPost]
        public IHttpActionResult ListJD([FromBody]Employee jpm)
        {
            try
            {
                return Ok(jpam.BindJD(jpm.CompCode,jpm.DepCode,jpm.IsInsert));
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return BadRequest();
            }

        }
        [Route("JobPos/GetAgeList")]
        [HttpPost]
        public IHttpActionResult ListAge([FromBody]Employee jpm)
        {
            try
            {
                return Ok(jpam.BindAge(jpm.JobId));
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return BadRequest();
            }

        }
        [Route("JobPos/GetJobCategory")]
        [HttpPost]
        public IHttpActionResult ListJobCategory([FromBody]Employee jpm)
        {
            try
            {
                return Ok(jpam.BindJobCat(jpm.IsInsert));
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return BadRequest();
            }

        }
        [Route("JobPos/GetInterviewMode")]
        [HttpPost]
        public IHttpActionResult ListInterviewMode([FromBody]Employee jpm)
        {
            try
            {
                return Ok(jpam.BindIM(jpm.IsInsert));
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return BadRequest();
            }

        }

        [Route("JobPos/GetQualification")]
        [HttpPost]
        public IHttpActionResult ListQualification([FromBody]Employee jpm)
        {
            try
            {
                return Ok(jpam.BindQualification(jpm.IsInsert));
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return BadRequest();
            }

        }

        [Route("JobPos/GetLanguage")]
        [HttpPost]
        public IHttpActionResult ListLanguage([FromBody]Employee jpm)
        {
            try
            {
                return Ok(jpam.BindLanguage(jpm.IsInsert));
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return BadRequest();
            }

        }

        [Route("JobPos/GetCertification")]
        [HttpPost]
        public IHttpActionResult ListCertification([FromBody]Employee jpm)
        {
            try
            {
                return Ok(jpam.BindCertificate(jpm.IsInsert));
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return BadRequest();
            }

        }
        [Route("JobPos/GetSpecializationField")]
        [HttpPost]
        public IHttpActionResult ListSpecializationField([FromBody]Employee jpm)
        {
            try
            {
                return Ok(jpam.BindSpecializationField(jpm.QID));
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return BadRequest();
            }

        }
        [Route("JobPos/GetPrimarySkill")]
        [HttpPost]
        public IHttpActionResult ListPrimarySkill([FromBody]Employee jpm)
        {
            try
            {
                return Ok(jpam.BindPrimarySkill(jpm.JobId));
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return BadRequest();
            }

        }
        [Route("JobPos/GetSecondarySkill")]
        [HttpPost]
        public IHttpActionResult ListSecondarySkill([FromBody]Employee jpm)
        {
            try
            {
                return Ok(jpam.BindSecondarySkill(jpm.JobId));
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return BadRequest();
            }

        }
        [Route("JobPos/GetTotalExp")]
        [HttpPost]
        public IHttpActionResult ListTotalExp([FromBody]Employee jpm)
        {
            try
            {
                return Ok(jpam.BindTotalExp(jpm.JobId));
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return BadRequest();
            }

        }
        [Route("JobPos/GetRelvExp")]
        [HttpPost]
        public IHttpActionResult ListRelvExp([FromBody]Employee jpm)
        {
            try
            {
                return Ok(jpam.BindRelvExp(jpm.JobId));
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return BadRequest();
            }

        }
        [Route("JobPos/GetAllJobPos")]
        [HttpPost]
        public IHttpActionResult ListAllJobPos([FromBody]Employee jpm)
        {
            try
            {
                return Ok(jpam.BindJobPos());
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return BadRequest();
            }

        }

        [Route("JobPos/InsertJobPos")]
        [HttpPost]
        public IHttpActionResult EmployeeCreate([FromBody]Employee jpm)
        {
            try
            {
                return Ok(jpam.JobPosCreate(jpm));
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return BadRequest();
            }

        }
        [Route("JobPos/UpdateJobPos")]
        [HttpPost]
        public IHttpActionResult EmployeeUpdate([FromBody]Employee jpm)
        {
            try
            {
                return Ok(jpam.JobPosUpdate(jpm));
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return BadRequest();
            }

        }
        [Route("JobPos/GetJobPos")]
        [HttpPost]
        public IHttpActionResult GetJobPos([FromBody]Employee jpm)
        {
            try
            {
                var jp = jpam.GetJobPos(jpm.JobPosCode);
                return Ok(jp);
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return BadRequest();
            }

        }

        [Route("JobPos/DeleteRecord")]
        [HttpPost]
        public IHttpActionResult Delete([FromBody]Employee jpm)
        {
            int result = 0;
            try
            {
                result = jpam.DeleteEmployee(jpm);
                if (result > 0)
                {
                    return Ok(result);
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return BadRequest();
            }

        }
        [Route("JobPos/GetAutoID")]
        [HttpPost]
        public IHttpActionResult GetAutoGenerateID([FromBody]Employee JPM)
        {
            try
            {
                var lastcode = jpam.GetAutoID(JPM);

                return Ok(lastcode);
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return BadRequest();
            }

        }
    }
}
