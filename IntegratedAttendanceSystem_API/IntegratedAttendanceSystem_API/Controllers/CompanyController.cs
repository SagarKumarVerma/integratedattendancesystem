﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Helpers;
using IntegratedAttendanceSystem_API.Models;
using IntegratedAttendanceSystem_API.DataAccess;

namespace IntegratedAttendanceSystem_API.Controllers
{
    public class CompanyController : ApiController
    {
        CompanyAccessModel objCompany = new CompanyAccessModel();

        [Route("Company/GetSpecRec")]
        [HttpPost]
        public IHttpActionResult GetbyCode([FromBody]CompanyModel Company)
        {
            try
            {
                var CompMst = objCompany.GetCompanyData(Company.Code.Trim());
                return Ok(CompMst);
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return BadRequest();
            }
        }

        [Route("Company/GetCompcode")]
        [HttpPost]
        public IHttpActionResult GetbyCompcode([FromBody]CompanyModel Company)
        {
            try
            {
                int result = 0;
                result = objCompany.GetCompanyCode(Company.Code.Trim());
                if (result == 0)
                {
                    return Ok();
                }
                else if(result > 0)
                {
                    return Ok(1);
                }
                else
                {
                    return BadRequest();
                }
                //var CompMst = objCompany.GetCompanyCode(Company.Code.Trim());
                //return Ok(CompMst);
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return BadRequest();
            }
        }

        [Route("Company/GetAllList")]
        [HttpPost]
        public IHttpActionResult ListAllCompany([FromBody]CompanyModel Company)
        {
            try
            {
                return Ok(objCompany.GetAllCompany());
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return BadRequest();
            }

        }

        // POST api/values
        [Route("Company/InsertRecord")]
        [HttpPost]
        public IHttpActionResult Add([FromBody]CompanyModel Company)
        {
            try
            {
                int result = 0;
                result = objCompany.Add(Company);
                if (result > 0)
                {
                    return Ok();
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);

                return BadRequest();
            }
        }

        /// <summary>
        /// use this method to post your data into database.Most suitable for update operation.
        /// </summary>
        /// <param name="Company">
        /// you can change the value parameter with your model object according to requirement.
        /// </param>        

        [Route("Company/UpdateRecord")]
        [HttpPost]
        public IHttpActionResult Update([FromBody]CompanyModel Company)
        {
            try
            {
                int result = 0;
                result = objCompany.Update(Company);
                if (result > 0)
                {
                    return Ok();
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return BadRequest();
            }

        }

        /// <summary>
        /// use this method to delete the specific Record.
        /// </summary>
        /// <param name="id">

        /// </param> 
        [Route("Company/DeleteRecord")]
        [HttpPost]
        public IHttpActionResult Delete([FromBody]CompanyModel Company)
        {
            int result = 0;
            try
            {
                result = objCompany.Delete(Company);
                if (result > 0)
                {
                    return Ok();
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return BadRequest();
            }
        }
    }
}
