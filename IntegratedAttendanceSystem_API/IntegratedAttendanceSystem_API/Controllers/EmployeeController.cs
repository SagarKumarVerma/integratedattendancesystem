﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Helpers;
using IntegratedAttendanceSystem_API.Models;
using IntegratedAttendanceSystem_API.DataAccess;

namespace IntegratedAttendanceSystem_API.Controllers
{
    public class EmployeeController : ApiController
    {
        EmployeeAccessModel Empm = new EmployeeAccessModel();
        /// <summary>
        /// use this method for get all group list
        /// </summary>
        /// <returns></returns>
        /// 

        [Route("Employee/GetGroupList")]
        [HttpPost]
        public IHttpActionResult ListAlGroup([FromBody] Employee em)
        {
            try
            {
                GroupMasteModel gm = new GroupMasteModel();
                return Ok(gm.GetGroupList());
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return BadRequest();
            }

        }

        [Route("Employee/GetCompanyList1")]
        [HttpPost]
        public IHttpActionResult ListAllCompany([FromBody] Employee em)
        {
            try
            {
                CompanyMasteModel cm = new CompanyMasteModel();
                return Ok(cm.GetCompanyList());
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return BadRequest();
            }

        }

        [Route("Employee/GetDepartmentList")]
        [HttpPost]
        public IHttpActionResult ListAllDepartment([FromBody] Employee em)
        {
            try
            {
                DepartmentMasteModel dm = new DepartmentMasteModel();
                return Ok(dm.GetDepartmentList());
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return BadRequest();
            }
        }

        [Route("Employee/GetDesignationList")]
        [HttpPost]
        public IHttpActionResult ListAllDesignation([FromBody] Employee em)
        {
            try
            {
                DesignationMasterModel dm = new DesignationMasterModel();
                return Ok(dm.GetDesignationList());
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return BadRequest();
            }
        }

        [Route("Employee/GetCategoryList")]
        [HttpPost]
        public IHttpActionResult ListAllCategory([FromBody] Employee cm)
        {
            try
            {
                CategoryMasterModel Catm = new CategoryMasterModel();
                return Ok(Catm.GetCategoryList());
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return BadRequest();
            }
        }

        [Route("Employee/GetLocationList")]  //Location and Division is same.
        [HttpPost]
        public IHttpActionResult ListAllLocation([FromBody] Employee cm)
        {
            try
            {
                LocationMasterModel Catm = new LocationMasterModel();
                return Ok(Catm.GetLocationList());
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return BadRequest();
            }
        }

        [Route("Employee/GetBankList")]
        [HttpPost]
        public IHttpActionResult ListAllBank([FromBody] Employee em)
        {
            try
            {
                BankMasterModel Catm = new BankMasterModel();
                return Ok(Catm.GetBankList());
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return BadRequest();
            }
        }

        [Route("Employee/GetHODList")]
        [HttpPost]
        public IHttpActionResult ListAllHOD([FromBody] Employee em)
        {
            try
            {
                HODMasterModel Catm = new HODMasterModel();
                return Ok(Catm.GetHODList());
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return BadRequest();
            }
        }

        [Route("Employee/GetEmployeeGroupList")]
        [HttpPost]
        public IHttpActionResult ListAllEmployeeGroup([FromBody] Employee em)
        {
            try
            {
                EmployeeGroupMasterModel eg = new EmployeeGroupMasterModel();
                return Ok(eg.GetEmployeeGroupList());
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return BadRequest();
            }
        }

        [Route("Employee/GetGradeList")]
        [HttpPost]
        public IHttpActionResult ListAllGrade([FromBody] Employee em)
        {
            try
            {
                GradeMasterModel grdm = new GradeMasterModel();
                return Ok(grdm.GetGradeList());
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return BadRequest();
            }
        }

        [Route("Employee/GetAllEmployee")]
        [HttpPost]
        public IHttpActionResult ListAllEmployee([FromBody] Employee em)
        {
            try
            {
                return Ok(Empm.BindEmployee());
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return BadRequest();
            }
        }

        // POST api/values
        [Route("Employee/AddEmployee")]
        [HttpPost]
        public IHttpActionResult Add([FromBody] Employee employee)
        {
            try
            {
                int result = 0;
                result = Empm.Add(employee);
                if (result > 0)
                {
                    return Ok();
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);

                return BadRequest();
            }
        }

        //Get Employee by paycode for update
        [Route("Employee/GetEmployee")]
        [HttpPost]
        public IHttpActionResult GetEmployee([FromBody] Employee emp)
        {
            try
            {
                var jp = Empm.GetEmployee(emp.sSSN);
                return Ok(jp);
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return BadRequest();
            }
        }


        [Route("Employee/Update")]
        [HttpPost]
        public IHttpActionResult EmployeeUpdate([FromBody] Employee emp)
        {
            try
            {
                return Ok(Empm.Update(emp));
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return BadRequest();
            }
        }

        [Route("Employee/GetbyEmployeecode")]
        [HttpPost]
        public IHttpActionResult GetbyEmployeecode([FromBody] Employee Employee)
        {
            try
            {
                int result = 0;
                result = Empm.GetEmployeeCode(Employee.sPAYCODE.Trim());
                if (result == 0)
                {
                    return Ok();
                }
                else if (result > 0)
                {
                    return Ok(1);
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return BadRequest();
            }
        }

        [Route("Employee/DeleteRecord")]
        [HttpPost]
        public IHttpActionResult Delete([FromBody] Employee empm)
        {
            int result = 0;
            try
            {
                result = Empm.DeleteEmployee(empm.sSSN);
                if (result > 0)
                {
                    return Ok(result);
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return BadRequest();
            }
        }
            //#########################################################################


            //[Route("Employee/GetJDList")]
            //[HttpPost]
            //public IHttpActionResult ListJD([FromBody] Employee jpm)
            //{
            //    try
            //    {
            //        return Ok(jpam.BindJD(jpm.CompCode, jpm.DepCode, jpm.IsInsert));
            //    }
            //    catch (Exception ex)
            //    {
            //        ExceptionLogging.SendErrorToText(ex);
            //        return BadRequest();
            //    }

            //}
            //[Route("Employee/GetAgeList")]
            //[HttpPost]
            //public IHttpActionResult ListAge([FromBody] Employee jpm)
            //{
            //    try
            //    {
            //        return Ok(jpam.BindAge(jpm.JobId));
            //    }
            //    catch (Exception ex)
            //    {
            //        ExceptionLogging.SendErrorToText(ex);
            //        return BadRequest();
            //    }

            //}

            //[Route("Employee/GetInterviewMode")]
            //[HttpPost]
            //public IHttpActionResult ListInterviewMode([FromBody] Employee jpm)
            //{
            //    try
            //    {
            //        return Ok(jpam.BindIM(jpm.IsInsert));
            //    }
            //    catch (Exception ex)
            //    {
            //        ExceptionLogging.SendErrorToText(ex);
            //        return BadRequest();
            //    }

            //}

            //[Route("Employee/GetQualification")]
            //[HttpPost]
            //public IHttpActionResult ListQualification([FromBody] Employee jpm)
            //{
            //    try
            //    {
            //        return Ok(jpam.BindQualification(jpm.IsInsert));
            //    }
            //    catch (Exception ex)
            //    {
            //        ExceptionLogging.SendErrorToText(ex);
            //        return BadRequest();
            //    }

            //}

            //[Route("Employee/GetLanguage")]
            //[HttpPost]
            //public IHttpActionResult ListLanguage([FromBody] Employee jpm)
            //{
            //    try
            //    {
            //        return Ok(jpam.BindLanguage(jpm.IsInsert));
            //    }
            //    catch (Exception ex)
            //    {
            //        ExceptionLogging.SendErrorToText(ex);
            //        return BadRequest();
            //    }

            //}

            //[Route("Employee/GetCertification")]
            //[HttpPost]
            //public IHttpActionResult ListCertification([FromBody] Employee jpm)
            //{
            //    try
            //    {
            //        return Ok(jpam.BindCertificate(jpm.IsInsert));
            //    }
            //    catch (Exception ex)
            //    {
            //        ExceptionLogging.SendErrorToText(ex);
            //        return BadRequest();
            //    }

            //}
            //[Route("Employee/GetSpecializationField")]
            //[HttpPost]
            //public IHttpActionResult ListSpecializationField([FromBody] Employee jpm)
            //{
            //    try
            //    {
            //        return Ok(jpam.BindSpecializationField(jpm.QID));
            //    }
            //    catch (Exception ex)
            //    {
            //        ExceptionLogging.SendErrorToText(ex);
            //        return BadRequest();
            //    }

            //}
            //[Route("Employee/GetPrimarySkill")]
            //[HttpPost]
            //public IHttpActionResult ListPrimarySkill([FromBody] Employee jpm)
            //{
            //    try
            //    {
            //        return Ok(jpam.BindPrimarySkill(jpm.JobId));
            //    }
            //    catch (Exception ex)
            //    {
            //        ExceptionLogging.SendErrorToText(ex);
            //        return BadRequest();
            //    }

            //}
            //[Route("Employee/GetSecondarySkill")]
            //[HttpPost]
            //public IHttpActionResult ListSecondarySkill([FromBody] Employee jpm)
            //{
            //    try
            //    {
            //        return Ok(jpam.BindSecondarySkill(jpm.JobId));
            //    }
            //    catch (Exception ex)
            //    {
            //        ExceptionLogging.SendErrorToText(ex);
            //        return BadRequest();
            //    }

            //}
            //[Route("Employee/GetTotalExp")]
            //[HttpPost]
            //public IHttpActionResult ListTotalExp([FromBody] Employee jpm)
            //{
            //    try
            //    {
            //        return Ok(jpam.BindTotalExp(jpm.JobId));
            //    }
            //    catch (Exception ex)
            //    {
            //        ExceptionLogging.SendErrorToText(ex);
            //        return BadRequest();
            //    }

            //}
            //[Route("Employee/GetRelvExp")]
            //[HttpPost]
            //public IHttpActionResult ListRelvExp([FromBody] Employee jpm)
            //{
            //    try
            //    {
            //        return Ok(jpam.BindRelvExp(jpm.JobId));
            //    }
            //    catch (Exception ex)
            //    {
            //        ExceptionLogging.SendErrorToText(ex);
            //        return BadRequest();
            //    }

            //}
            ////[Route("Employee/GetAllJobPos")]
            ////[HttpPost]
            ////public IHttpActionResult ListAllJobPos([FromBody] Employee jpm)
            ////{
            ////    try
            ////    {
            ////        return Ok(jpam.BindJobPos());
            ////    }
            ////    catch (Exception ex)
            ////    {
            ////        ExceptionLogging.SendErrorToText(ex);
            ////        return BadRequest();
            ////    }

            ////}

            //[Route("Employee/InsertJobPos")]
            //[HttpPost]
            //public IHttpActionResult EmployeeCreate([FromBody] Employee jpm)
            //{
            //    try
            //    {
            //        return Ok(jpam.JobPosCreate(jpm));
            //    }
            //    catch (Exception ex)
            //    {
            //        ExceptionLogging.SendErrorToText(ex);
            //        return BadRequest();
            //    }

            //}
            //[Route("Employee/UpdateJobPos")]
            //[HttpPost]
            //public IHttpActionResult EmployeeUpdate([FromBody] Employee jpm)
            //{
            //    try
            //    {
            //        return Ok(jpam.JobPosUpdate(jpm));
            //    }
            //    catch (Exception ex)
            //    {
            //        ExceptionLogging.SendErrorToText(ex);
            //        return BadRequest();
            //    }

            //}
            //[Route("Employee/GetJobPos")]
            //[HttpPost]
            //public IHttpActionResult GetJobPos([FromBody] Employee jpm)
            //{
            //    try
            //    {
            //        var jp = jpam.GetJobPos(jpm.JobPosCode);
            //        return Ok(jp);
            //    }
            //    catch (Exception ex)
            //    {
            //        ExceptionLogging.SendErrorToText(ex);
            //        return BadRequest();
            //    }

            //}

            //[Route("Employee/DeleteRecord")]
            //[HttpPost]
            //public IHttpActionResult Delete([FromBody] Employee jpm)
            //{
            //    int result = 0;
            //    try
            //    {
            //        result = jpam.DeleteEmployee(jpm);
            //        if (result > 0)
            //        {
            //            return Ok(result);
            //        }
            //        else
            //        {
            //            return BadRequest();
            //        }
            //    }
            //    catch (Exception ex)
            //    {
            //        ExceptionLogging.SendErrorToText(ex);
            //        return BadRequest();
            //    }

            //}
            //[Route("Employee/GetAutoID")]
            //[HttpPost]
            //public IHttpActionResult GetAutoGenerateID([FromBody] Employee JPM)
            //{
            //    try
            //    {
            //        var lastcode = jpam.GetAutoID(JPM);

            //        return Ok(lastcode);
            //    }
            //    catch (Exception ex)
            //    {
            //        ExceptionLogging.SendErrorToText(ex);
            //        return BadRequest();
            //    }

            //}
        }
}
