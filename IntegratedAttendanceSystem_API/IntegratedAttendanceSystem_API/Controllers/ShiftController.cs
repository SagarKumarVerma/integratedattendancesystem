﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Helpers;
using IntegratedAttendanceSystem_API.Models;
using IntegratedAttendanceSystem_API.DataAccess;

namespace IntegratedAttendanceSystem_API.Controllers
{
    public class ShiftController : ApiController
    {
        ShiftAccessModel objShift = new ShiftAccessModel();

        [Route("Shift/GetSpecRec")]
        [HttpPost]
        public IHttpActionResult GetbyCode([FromBody]ShiftModel Shift)
        {
            try
            {
                var CompMst = objShift.GetShiftData(Shift.SHIFT.Trim(),Shift.CompanyCode.Trim());
                
                return Ok(CompMst);
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return BadRequest();
            }
        }

        [Route("Shift/GetCompcode")]
        [HttpPost]
        public IHttpActionResult GetbyCompcode([FromBody]ShiftModel Shift)
        {
            try
            {
                int result = 0;
                result = objShift.GetShiftCode(Shift.SHIFT.Trim(), Shift.CompanyCode);
                if (result == 0)
                {
                    return Ok();
                }
                else if(result > 0)
                {
                    return Ok(1);
                }
                else
                {
                    return BadRequest();
                }
                //var CompMst = objShift.GetShiftCode(Shift.Code.Trim());
                //return Ok(CompMst);
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return BadRequest();
            }
        }

        [Route("Shift/GetAllList")]
        [HttpPost]
        public IHttpActionResult ListAllShift([FromBody]ShiftModel Shift)
        {
            try
            {
                return Ok(objShift.GetAllShift(Shift.CompanyCode, Shift.UserType));
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return BadRequest();
            }
        }

        // POST api/values
        [Route("Shift/InsertRecord")]
        [HttpPost]
        public IHttpActionResult Add([FromBody]ShiftModel Shift)
        {
            try
            {
                int result = 0;
                result = objShift.Add(Shift);
                if (result > 0)
                {
                    return Ok();
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);

                return BadRequest();
            }

        }

        /// <summary>
        /// use this method to post your data into database.Most suitable for update operation.
        /// </summary>
        /// <param name="Shift">
        /// you can change the value parameter with your model object according to requirement.
        /// </param>        

        [Route("Shift/UpdateRecord")]
        [HttpPost]
        public IHttpActionResult Update([FromBody]ShiftModel Shift)
        {
            try
            {
                int result = 0;
                result = objShift.Update(Shift);
                if (result > 0)
                {
                    return Ok();
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return BadRequest();
            }

        }

        [Route("Shift/GetCompanyList1")]
        [HttpPost]
        public IHttpActionResult ListAllCompany([FromBody] Employee em)
        {
            try
            {
                CompanyMasteModel cm = new CompanyMasteModel();
                return Ok(cm.GetCompanyList());
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return BadRequest();
            }

        }

        /// <summary>
        /// use this method to delete the specific Record.
        /// </summary>
        /// <param name="id">

        /// </param> 
        [Route("Shift/DeleteRecord")]
        [HttpPost]
        public IHttpActionResult Delete([FromBody] ShiftModel Shift)
        {
            int result = 0;
            try
            {
                result = objShift.DeleteShif(Shift.SHIFT,Shift.CompanyCode);
                if (result > 0)
                {
                    return Ok();
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return BadRequest();
            }
        }
    }
}
