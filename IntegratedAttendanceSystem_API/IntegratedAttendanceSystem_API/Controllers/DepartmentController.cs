﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Helpers;
using IntegratedAttendanceSystem_API.Models;
using IntegratedAttendanceSystem_API.DataAccess;

namespace IntegratedAttendanceSystem_API.Controllers
{
    public class DepartmentController : ApiController
    {
        DepartmentAccessModel DeptMst = new DepartmentAccessModel();

        /// <summary>
        /// use this method to get specific record based on ID.
        /// </summary>
        /// <param name="ID">
        /// ID is like a key to get the data.
        /// </param>
        /// 


        [Route("Department/GetAllDepartment")]
        [HttpPost]
        public IHttpActionResult GetAllDepartment([FromBody]DepartmentModel Dept)
        {
            try
            {
                return Ok(DeptMst.GetAllDepartment());
            }
            catch (Exception ex)
            {
                ex.ToString();
                return BadRequest();
            }
        }


        [Route("Department/AddDepartment")]
        [HttpPost]
        public IHttpActionResult Add([FromBody]DepartmentModel Dept)
        {
            try
            {
                int result = 0;
                result = DeptMst.Add(Dept);
                if (result > 0)
                {
                    return Ok();
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                ex.ToString();

                return BadRequest();
            }
        }

        [Route("Department/UpdateDepartment")]
        [HttpPost]
        public IHttpActionResult Update([FromBody]DepartmentModel Dept)
        {
            try
            {
                int result = 0;
                result = DeptMst.Update(Dept);
                if (result > 0)
                {
                    return Ok();
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                return BadRequest();
            }
        }

        [Route("Department/DeleteDepartment")]
        [HttpPost]
        public IHttpActionResult Delete([FromBody]DepartmentModel Dept)
        {
            int result = 0;
            try
            {
                result = DeptMst.Delete(Dept);
                if (result > 0)
                {
                    return Ok();
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                return BadRequest();
            }
        }

        //================================

        [Route("Department/GetAutoID")]
        [HttpPost]
        public IHttpActionResult GetAutoGenerateID([FromBody]DepartmentModel Dept)
        {
            try
            {
                DepartmentModel objDepartmentModel = new DepartmentModel();
                var lastcode = DeptMst.GetAllDepartment().ToList().OrderByDescending(m => m.DepartmentCode).FirstOrDefault();
                if (lastcode == null)
                {
                    return Ok("00001");
                }
                else
                {
                    return Ok((Convert.ToInt32(lastcode.DepartmentCode) + 1).ToString("D5").PadLeft(0));
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                return BadRequest();
            }
        }

        [Route("Department/GetSpecRec")]
        [HttpPost]
        public IHttpActionResult GetbyID([FromBody]DepartmentModel Dept)
        {
            try
            {
                var DeptMstM = DeptMst.GetDepartmentId(Dept.DepartmentCode.Trim());
                return Ok(DeptMstM);
            }
            catch (Exception ex)
            {
                ex.ToString();
                return BadRequest();
            }
        }
        [Route("Department/GetDeptcode")]
        [HttpPost]
        public IHttpActionResult GetbyDeptcode([FromBody]DepartmentModel Dept)
        {
            try
            {
                int result = 0;
                result = DeptMst.GetDepartmentCode(Dept.DepartmentCode.Trim());
                if (result == 0)
                {
                    return Ok();
                }
                else if (result > 0)
                {
                    return Ok(1);
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return BadRequest();
            }
        }
        //=================================
    }
}
