﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Helpers;
using IntegratedAttendanceSystem_API.Models;
using IntegratedAttendanceSystem_API.DataAccess;

namespace IntegratedAttendanceSystem_API.Controllers
{
    public class GradeController : ApiController
    {
        GradeAccessModel objGrade = new GradeAccessModel();

        [Route("Grade/GetSpecRec")]
        [HttpPost]
        public IHttpActionResult GetbyCode([FromBody]GradeModel GradeMst)
        {
            try
            {
                var GradeMstVar = objGrade.GetGradeData(GradeMst.GradeCode.Trim());
                return Ok(GradeMstVar);
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return BadRequest();
            }

        }
        [Route("Grade/GetbyGradecode")]
        [HttpPost]
        public IHttpActionResult GetbyCompcode([FromBody]GradeModel GradeMst)
        {
            try
            {
                int result = 0;
                result = objGrade.GetGradeCode(GradeMst.GradeCode.Trim());
                if (result == 0)
                {
                    return Ok();
                }
                else if (result > 0)
                {
                    return Ok(1);
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return BadRequest();
            }
        }
        [Route("Grade/GetAllList")]
        [HttpPost]
        public IHttpActionResult ListAllGrade([FromBody]GradeModel GradeMst)
        {
            try
            {
                return Ok(objGrade.GetAllGrade());
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return BadRequest();
            }

        }
        [Route("Grade/InsertRecord")]
        [HttpPost]
        public IHttpActionResult Add([FromBody]GradeModel GradeMst)
        {
            try
            {
                int result = 0;
                result = objGrade.Add(GradeMst);
                if (result > 0)
                {
                    return Ok();
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);

                return BadRequest();
            }
        }       
        [Route("Grade/UpdateRecord")]
        [HttpPost]
        public IHttpActionResult Update([FromBody]GradeModel GradeMst)
        {
            try
            {
                int result = 0;
                result = objGrade.Update(GradeMst);
                if (result > 0)
                {
                    return Ok();
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return BadRequest();
            }
        }
        [Route("Grade/DeleteRecord")]
        [HttpPost]
        public IHttpActionResult Delete([FromBody]GradeModel GradeMst)
        {
            int result = 0;
            try
            {
                result = objGrade.Delete(GradeMst);
                if (result > 0)
                {
                    return Ok();
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return BadRequest();
            }

        }
    }
}
