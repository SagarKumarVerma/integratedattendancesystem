﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IntegratedAttendanceSystem_API.Models
{
    public class CompanyCloneModel
    {
        public string IsActive { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string ShortName { get; set; }
        public string IndustryNature { get; set; }
        public string Address { get; set; }
        public string PhoneNo { get; set; }
        public string EmailId { get; set; }
        public string RegistationNo { get; set; }
        public string PANNo { get; set; }
        public string TANNo { get; set; }
        public string PFNo { get; set; }
        public string LCNo { get; set; }
    }
}