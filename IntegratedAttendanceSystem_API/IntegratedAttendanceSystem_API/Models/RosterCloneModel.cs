﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IntegratedAttendanceSystem_API.Models
{
    public class RosterCloneModel
    {
        public string sSelectionMode { get; set; }
        public DateTime sFromDate { get; set; }
        public string sCompanyCode { get; set; }
        public string sCompanyCodeGlobel { get; set; }
        public string sEmployeeCode { get; set; }
        public string sEmployeeName { get; set; }
        public string sLocationCode { get; set; }
        public string sUserName { get; set; }
        public string LoginTerminalNameIP { get; set; }
        public string LastModifiedBy { get; set; }
        public List<Company_list> Company { get; set; }
        public List<Employee_list> sDepartment { get; set; }
        public List<Division_list> sDivision { get; set; }

        public string sProcessDate { get; set; }
    }
}