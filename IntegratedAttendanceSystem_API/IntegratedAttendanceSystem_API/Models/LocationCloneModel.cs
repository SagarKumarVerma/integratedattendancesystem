﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IntegratedAttendanceSystem_API.Models
{
    public class LocationCloneModel
    {
        public string LocationCode { get; set; }
        public string LocationName { get; set; }
        public string LocationCity { get; set; }
        public string LocationState { get; set; }
        public string LocationPostalCode { get; set; }
        public string LocationCountry { get; set; }
        public string LocationAddress1 { get; set; }
        public string LocationAddress2 { get; set; }
        public string LocationContactEmailID { get; set; }
    }
}