﻿using IntegratedAttendanceSystem_API.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace IntegratedAttendanceSystem_API.Models
{
    public class EmployeeGroupPolicyAccessModel
    {
        /// <summary>
        /// code for bind jd dropdown
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        //public IEnumerable<JobPosJDMaster_list> BindJD(string compcode, string depcode, string Action)
        //{
        //    Employee jpm = new Employee();
        //    List<JobPosJDMaster_list> lst = new List<JobPosJDMaster_list>();

        //    DataTable dt = DataOperation.GetDataTableWithParameter("UspJobPos_BindJD",
        //                                                            new SqlParameter("CompanyCode", compcode.Trim()),
        //                                                            new SqlParameter("DepartmentCode", depcode.Trim()),
        //                                                              new SqlParameter("Action", Action.Trim()));
        //    if (dt.Rows.Count > 0)
        //    {

        //        for (int index = 0; index < dt.Rows.Count; index++)
        //        {
        //            lst.Add(new JobPosJDMaster_list
        //            {
        //                JobMasterId = Convert.ToString(dt.Rows[index]["JobId"]).Trim(),
        //                JobMasterName = Convert.ToString(dt.Rows[index]["JobName"]).Trim(),
        //            });
        //        }

        //    }
        //    return lst;
        //}
        /// <summary>
        /// code bind age as per the selection of JD
        /// </summary>
        /// <param name="compcode"></param>
        /// <param name="depcode"></param>
        /// <returns></returns>
        //public IEnumerable<JobPosAgeMaster_list> BindAge(string JobID)
        //{
        //    Employee jpm = new Employee();
        //    List<JobPosAgeMaster_list> lst = new List<JobPosAgeMaster_list>();

        //    DataTable dt = DataOperation.GetDataTableWithParameter("UspJobPosAgeSelect",
        //                                                            new SqlParameter("JobId", JobID.Trim()));
        //    if (dt.Rows.Count > 0)
        //    {

        //        for (int index = 0; index < dt.Rows.Count; index++)
        //        {
        //            lst.Add(new JobPosAgeMaster_list
        //            {
        //                AgeId = Convert.ToString(dt.Rows[index]["AgeId"]).Trim(),
        //                AgeDesc = Convert.ToString(dt.Rows[index]["AgeDesc"]).Trim(),
        //            });
        //        }

        //    }
        //    return lst;
        //}

        //public IEnumerable<CatMaster_list> BindJobCat(string isInsert)
        //{
        //    Employee jpm = new Employee();
        //    List<CatMaster_list> lst = new List<CatMaster_list>();

        //    DataTable dt = DataOperation.GetDataTableWithParameter("TblCategory_SelectAll",
        //                                                            new SqlParameter("Action", isInsert.Trim()));
        //    if (dt.Rows.Count > 0)
        //    {

        //        for (int index = 0; index < dt.Rows.Count; index++)
        //        {
        //            lst.Add(new CatMaster_list
        //            {
        //                CategoryCode = Convert.ToString(dt.Rows[index]["CategoryCode"]).Trim(),
        //                CategoryName  = Convert.ToString(dt.Rows[index]["CategoryName"]).Trim(),
        //            });
        //        }

        //    }
        //    return lst;
        //}

        //public IEnumerable<JobPosIMMaster_list> BindIM(string isInsert)
        //{
        //    Employee jpm = new Employee();
        //    List<JobPosIMMaster_list> lst = new List<JobPosIMMaster_list>();

        //    DataTable dt = DataOperation.GetDataTableWithParameter("UspJobPos_BindIM",
        //                                                            new SqlParameter("Action", isInsert.Trim()));
        //    if (dt.Rows.Count > 0)
        //    {

        //        for (int index = 0; index < dt.Rows.Count; index++)
        //        {
        //            lst.Add(new JobPosIMMaster_list
        //            {
        //                IMCode = Convert.ToString(dt.Rows[index]["InterModeCode"]).Trim(),
        //                IMDesc = Convert.ToString(dt.Rows[index]["InterModeDesc"]).Trim(),
        //            });
        //        }

        //    }
        //    return lst;
        //}

        //public IEnumerable<JobPosQuaification_list> BindQualification(string isInsert)
        //{
        //    Employee jpm = new Employee();
        //    List<JobPosQuaification_list> lst = new List<JobPosQuaification_list>();

        //    DataTable dt = DataOperation.GetDataTableWithParameter("UspJobPos_BindQualification",
        //                                                            new SqlParameter("Action", isInsert.Trim()));
        //    if (dt.Rows.Count > 0)
        //    {

        //        for (int index = 0; index < dt.Rows.Count; index++)
        //        {
        //            lst.Add(new JobPosQuaification_list
        //            {
        //                QID = Convert.ToString(dt.Rows[index]["QualId"]).Trim(),
        //                QDescc = Convert.ToString(dt.Rows[index]["QualDesc"]).Trim(),
        //            });
        //        }

        //    }
        //    return lst;
        //}

        //public IEnumerable<JobPosLanguage_list> BindLanguage(string isInsert)
        //{
        //    Employee jpm = new Employee();
        //    List<JobPosLanguage_list> lst = new List<JobPosLanguage_list>();

        //    DataTable dt = DataOperation.GetDataTableWithParameter("UspJobPos_BindLanguage",
        //                                                            new SqlParameter("Action", isInsert.Trim()));
        //    if (dt.Rows.Count > 0)
        //    {

        //        for (int index = 0; index < dt.Rows.Count; index++)
        //        {
        //            lst.Add(new JobPosLanguage_list
        //            {
        //                LangCode = Convert.ToString(dt.Rows[index]["LanguageCode"]).Trim(),
        //                LangDesc = Convert.ToString(dt.Rows[index]["LanguageDesc"]).Trim(),
        //            });
        //        }

        //    }
        //    return lst;
        //}

        //public IEnumerable<JobPosCertificate_list> BindCertificate(string isInsert)
        //{
        //    Employee jpm = new Employee();
        //    List<JobPosCertificate_list> lst = new List<JobPosCertificate_list>();

        //    DataTable dt = DataOperation.GetDataTableWithParameter("UspJobPos_BindCertification",
        //                                                            new SqlParameter("Action", isInsert.Trim()));
        //    if (dt.Rows.Count > 0)
        //    {

        //        for (int index = 0; index < dt.Rows.Count; index++)
        //        {
        //            lst.Add(new JobPosCertificate_list
        //            {
        //                CertCode = Convert.ToString(dt.Rows[index]["CertCode"]).Trim(),
        //                CertName = Convert.ToString(dt.Rows[index]["CertName"]).Trim(),
        //            });
        //        }

        //    }
        //    return lst;
        //}

        //public IEnumerable<JobPosSpeclzn_list> BindSpecializationField(string qID)
        //{
        //    Employee jpm = new Employee();
        //    List<JobPosSpeclzn_list> lst = new List<JobPosSpeclzn_list>();

        //    DataTable dt = DataOperation.GetDataTableWithParameter("UspJobPos_SpecializationFiled",
        //                                                            new SqlParameter("QID", qID.Trim()));
        //    if (dt.Rows.Count > 0)
        //    {

        //        for (int index = 0; index < dt.Rows.Count; index++)
        //        {
        //            lst.Add(new JobPosSpeclzn_list
        //            {
        //                SpecLznID = Convert.ToString(dt.Rows[index]["MqualId"]).Trim(),
        //                SpecLznDesc = Convert.ToString(dt.Rows[index]["MqualDesc"]).Trim(),
        //            });
        //        }

        //    }
        //    return lst;
        //}

        public IEnumerable<EmployeeGroupPolicy> BindEmployeeGroupPolicy()
        {
            try
            {
                List<EmployeeGroupPolicy> lst = new List<EmployeeGroupPolicy>();
                DataTable dt = DataOperation.GetDataTableWithoutParameter("tblEmployeeGroupPolicy_AllRec");
                if (dt.Rows.Count > 0)
                {
                    for (int index = 0; index < dt.Rows.Count; index++)
                    {
                        EmployeeGroupPolicy egp = new EmployeeGroupPolicy();

                        egp.sGroupID = dt.Rows[index]["GroupID"].ToString();
                        egp.sGroupName = dt.Rows[index]["GroupName"].ToString();
                        egp.sCOMPANYCODE = dt.Rows[index]["CompanyCode"].ToString();
                        egp.sISROUNDTHECLOCKWORK = dt.Rows[index]["ISROUNDTHECLOCKWORK"].ToString();
                        egp.sPERMISLATEARRIVAL = dt.Rows[index]["PERMISLATEARRIVAL"].ToString();//Convert.ToInt32(dt.Rows[index]["PERMISLATEARRIVAL"].ToString());
                        egp.sPERMISEARLYDEPRT = dt.Rows[index]["PERMISEARLYDEPRT"].ToString();//Convert.ToInt32(dt.Rows[index]["PERMISEARLYDEPRT"].ToString());
                        egp.sMAXDAYMIN = dt.Rows[index]["MAXDAYMIN"].ToString();
                        egp.sISOUTWORK = dt.Rows[index]["ISOUTWORK"].ToString();
                        egp.sISTIMELOSSALLOWED = dt.Rows[index]["ISTIMELOSSALLOWED"].ToString();
                        egp.sISHALFDAY = dt.Rows[index]["ISHALFDAY"].ToString();
                        egp.sISSHORT = dt.Rows[index]["ISSHORT"].ToString();
                        egp.sTIME = dt.Rows[index]["TIME"].ToString(); // Convert.ToInt32(dt.Rows[index]["TIME"].ToString());
                        egp.sSHORT = dt.Rows[index]["SHORT"].ToString(); // Convert.ToInt32(dt.Rows[index]["SHORT"].ToString());
                        egp.sHALF = dt.Rows[index]["HALF"].ToString(); // Convert.ToInt32(dt.Rows[index]["HALF"].ToString());
                        egp.sHLFAfter = dt.Rows[index]["HLFAfter"].ToString(); // Convert.ToInt32(dt.Rows[index]["HLFAfter"].ToString());
                        egp.sHLFBefore = dt.Rows[index]["HLFBefore"].ToString(); // Convert.ToInt32(dt.Rows[index]["HLFBefore"].ToString());
                        egp.sCOMPANYCODE = dt.Rows[index]["CompanyCode"].ToString();

                        lst.Add(egp);
                    }
                }
                return lst;
            }
            catch (Exception ex)
            {
                return null;
            }

        }
        public Employee GetEmployee(string sPAYCODE)
        {
            Employee em = new Employee();
            DataTable dt = DataOperation.GetDataTableWithParameter("Employee_SingleList", new SqlParameter("PAYCODE", sPAYCODE.Trim()));
            if (dt.Rows.Count > 0)
            {
                for (int index = 0; index < dt.Rows.Count; index++)
                {
                    em.sACTIVE = dt.Rows[index]["ACTIVE"].ToString();
                    em.sPAYCODE = dt.Rows[index]["PAYCODE"].ToString();
                    em.sEMPNAME = dt.Rows[index]["EMPNAME"].ToString();
                    em.sGUARDIANNAME = dt.Rows[index]["GUARDIANNAME"].ToString();
                    em.sDateOFBIRTH = dt.Rows[index]["DateOFBIRTH"].ToString();
                    em.sDateOFJOIN = dt.Rows[index]["DateOFJOIN"].ToString();
                    em.sPRESENTCARDNO = dt.Rows[index]["PRESENTCARDNO"].ToString();
                    //em.sCOMPANYCODE = dt.Rows[index]["COMPANYCODE"].ToString();
                    //em.sDivisionCode = dt.Rows[index]["DivisionCode"].ToString();
                    //em.sCAT = dt.Rows[index]["CAT"].ToString();
                    em.sSEX = dt.Rows[index]["SEX"].ToString();
                    em.sISMARRIED = dt.Rows[index]["ISMARRIED"].ToString();
                    //em.sBUS = dt.Rows[index]["BUS"].ToString();
                    em.sQUALIFICATION = dt.Rows[index]["QUALIFICATION"].ToString();
                    em.sEXPERIENCE = dt.Rows[index]["EXPERIENCE"].ToString();
                    //em.sDESIGNATION = dt.Rows[index]["DESIGNATION"].ToString();
                    em.sADDRESS1 = dt.Rows[index]["ADDRESS1"].ToString();
                    em.sPINCODE1 = dt.Rows[index]["PINCODE1"].ToString();
                    em.sTELEPHONE1 = dt.Rows[index]["TELEPHONE1"].ToString();
                    em.sE_MAIL1 = dt.Rows[index]["E_MAIL1"].ToString();
                    em.sADDRESS2 = dt.Rows[index]["ADDRESS2"].ToString();
                    em.sPINCODE2 = dt.Rows[index]["PINCODE2"].ToString();
                    em.sTELEPHONE2 = dt.Rows[index]["TELEPHONE2"].ToString();
                    em.sBLOODGROUP = dt.Rows[index]["BLOODGROUP"].ToString();
                    em.sEMPPHOTO = dt.Rows[index]["EMPPHOTO"].ToString();
                    em.sEMPSIGNATURE = dt.Rows[index]["EMPSIGNATURE"].ToString();
                    //em.sDepartmentCode = dt.Rows[index]["DepartmentCode"].ToString();
                    //em.sGradeCode = dt.Rows[index]["GradeCode"].ToString();
                    em.sLeavingdate = dt.Rows[index]["Leavingdate"].ToString();
                    em.sLeavingReason = dt.Rows[index]["LeavingReason"].ToString();
                    em.sVehicleNo = dt.Rows[index]["VehicleNo"].ToString();
                    em.sPFNO = dt.Rows[index]["PFNO"].ToString();
                    em.sPF_NO = dt.Rows[index]["PF_NO"].ToString();
                    em.sESINO = dt.Rows[index]["ESINO"].ToString();
                    em.sEMPTYPE = dt.Rows[index]["EMPTYPE"].ToString();
                    em.sBankAcc = dt.Rows[index]["BankAcc"].ToString();
                    //em.sbankCODE = dt.Rows[index]["bankCODE"].ToString();
                    em.sBRANCHCODE = dt.Rows[index]["BRANCHCODE"].ToString();
                    em.sDESPANSARYCODE = dt.Rows[index]["DESPANSARYCODE"].ToString();
                    em.sheadid = dt.Rows[index]["headid"].ToString();
                    em.sEmail_CC = dt.Rows[index]["Email_CC"].ToString();
                    em.sLeaveApprovalStages = dt.Rows[index]["LeaveApprovalStages"].ToString();
                    em.sRockWellid = dt.Rows[index]["RockWellid"].ToString();
                    //em.sGrade = dt.Rows[index]["Grade"].ToString();
                    em.sheadid_2 = dt.Rows[index]["headid_2"].ToString();
                    em.shead_id = dt.Rows[index]["head_id"].ToString();
                    em.sValidityStartDate = dt.Rows[index]["ValidityStartDate"].ToString();
                    em.sValidityEndDate = dt.Rows[index]["ValidityEndDate"].ToString();
                    em.sSSN = dt.Rows[index]["SSN"].ToString();
                    em.sDeviceGroupID = dt.Rows[index]["DeviceGroupID"].ToString();
                    em.sClientID = dt.Rows[index]["ClientID"].ToString();
                    em.sGroupID = dt.Rows[index]["GroupID"].ToString();
                }
            }
            return em;
        }

        //public string JobPosCreate(Employee em)
        //{
        //    string result = "";
        //    string IMCode = JPM.IMCode.Remove(JPM.IMCode.Length - 1, 1);
        //    string QID = JPM.QID.Remove(JPM.QID.Length - 1, 1);
        //    string Splzfld = JPM.SpecLznID.Remove(JPM.SpecLznID.Length - 1, 1);
        //    string lang = JPM.LangCode.Remove(JPM.LangCode.Length - 1, 1);
        //    string pss = JPM.pskillID.Remove(JPM.pskillID.Length - 1, 1);
        //    string sss = JPM.sskillID.Remove(JPM.sskillID.Length - 1, 1);
        //    string exp = JPM.ExpId.Remove(JPM.ExpId.Length - 1, 1);
        //    string relvexp = JPM.RelvExpId.Remove(JPM.RelvExpId.Length - 1, 1);
        //    string certcode = JPM.CertCode.Remove(JPM.CertCode.Length - 1, 1);
        //    JPM.JobPosCode = GetAutoID(JPM);
        //    int res = DataOperation.InsUpdDel("UspJobPos_Create", new SqlParameter("IsActive", JPM.IsActive), new SqlParameter("JobPosCode", JPM.JobPosCode.Trim()),
        //        new SqlParameter("JobPosName", JPM.JobPosName.Trim()),
        //        new SqlParameter("JobPosDesc", JPM.JobPosDesc.Trim()),
        //        new SqlParameter("Gender", JPM.Gender.Trim()),
        //        new SqlParameter("DesignationCode", JPM.DesignCode.Trim()),
        //        new SqlParameter("JobId", JPM.JobId.Trim()),
        //        new SqlParameter("JobCatCode", JPM.CatCode.Trim()),
        //        new SqlParameter("AgeId", JPM.AgeId.Trim()),
        //        new SqlParameter("InterModeCode", IMCode.Trim()),
        //        new SqlParameter("MultiQID", QID.Trim()),
        //        new SqlParameter("SpecLznID", Splzfld.Trim()),
        //        new SqlParameter("LangCode", lang.Trim()),
        //        new SqlParameter("pskillID", pss.Trim()),
        //        new SqlParameter("sskillID", sss.Trim()),
        //        new SqlParameter("ExpId", exp.Trim()),
        //        new SqlParameter("RelvExpId", relvexp.Trim()),
        //        new SqlParameter("CertCode", certcode.Trim()),
        //        new SqlParameter("LastModifiedBy", JPM.LastModifiedBy.Trim()));
        //    result = res.ToString();
        //    if (res == 1)
        //    {
        //        Employee oldobject = GetJobPos(JPM.JobPosCode);
        //        AuditLog.CreateAuditTrail(Employee.EMPLOYEEMASTER, Employee.PAYCODE, JPM.JobPosCode, AppConstant.CREATED, JPM.LastModifiedBy, JPM.LoginTerminalNameIP, oldobject, JPM);
        //        result = JPM.JobPosCode;
        //    }
        //    return result;
        //}

        //public int JobPosUpdate(Employee JPM)
        //{
        //    string IMCode = JPM.IMCode.Remove(JPM.IMCode.Length - 1, 1);
        //    string QID = JPM.QID.Remove(JPM.QID.Length - 1, 1);
        //    string Splzfld = JPM.SpecLznID.Remove(JPM.SpecLznID.Length - 1, 1);
        //    string lang = JPM.LangCode.Remove(JPM.LangCode.Length - 1, 1);
        //    string pss = JPM.pskillID.Remove(JPM.pskillID.Length - 1, 1);
        //    string sss = JPM.sskillID.Remove(JPM.sskillID.Length - 1, 1);
        //    string exp = JPM.ExpId.Remove(JPM.ExpId.Length - 1, 1);
        //    string relvexp = JPM.RelvExpId.Remove(JPM.RelvExpId.Length - 1, 1);
        //    string certcode = JPM.CertCode.Remove(JPM.CertCode.Length - 1, 1);
        //    Employee objjpm = GetJobPos(JPM.JobPosCode);
        //    int res = DataOperation.InsUpdDel("UspJobPos_Update", new SqlParameter("IsActive", JPM.IsActive), new SqlParameter("JobPosCode", JPM.JobPosCode),
        //        new SqlParameter("JobPosName", JPM.JobPosName.Trim()),
        //        new SqlParameter("JobPosDesc", JPM.JobPosDesc.Trim()),
        //        new SqlParameter("Gender", JPM.Gender.Trim()),
        //        new SqlParameter("DesignationCode", JPM.DesignCode.Trim()),
        //        new SqlParameter("JobId", JPM.JobId.Trim()),
        //        new SqlParameter("JobCatCode", JPM.CatCode.Trim()),
        //        new SqlParameter("AgeId", JPM.AgeId.Trim()),
        //        new SqlParameter("InterModeCode",IMCode.Trim()),
        //        new SqlParameter("MultiQID", QID.Trim()),
        //        new SqlParameter("SpecLznID", Splzfld.Trim()),
        //        new SqlParameter("LangCode", lang.Trim()),
        //        new SqlParameter("pskillID", pss.Trim()),
        //        new SqlParameter("sskillID", sss.Trim()),
        //        new SqlParameter("ExpId", exp.Trim()),
        //        new SqlParameter("RelvExpId", relvexp.Trim()),
        //        new SqlParameter("CertCode", certcode.Trim()),
        //        new SqlParameter("LastModifiedBy", JPM.LastModifiedBy.Trim()));
        //    if (res == 1)
        //    {
        //        EmployeeClone oldobject = new EmployeeClone();
        //        //code for clone copy create for old object
        //        oldobject.IsActive = objjpm.IsActive;
        //        oldobject.JobPosCode = objjpm.JobPosCode;
        //        oldobject.JobPosName = objjpm.JobPosName;
        //        oldobject.JobPosDesc = objjpm.JobPosDesc;
        //        oldobject.Gender = objjpm.Gender;
        //        oldobject.CompCode = objjpm.CompCode;
        //        oldobject.DepCode = objjpm.DepCode;
        //        oldobject.DesignCode = objjpm.DesignCode;
        //        oldobject.JobId = objjpm.JobId;
        //        oldobject.AgeId = objjpm.AgeId;
        //        oldobject.CatCode = objjpm.CatCode;
        //        oldobject.IMCode = objjpm.IMCode;
        //        oldobject.QID = objjpm.QID;
        //        oldobject.LangCode = objjpm.LangCode;
        //        oldobject.SpecLznID = objjpm.SpecLznID;
        //        oldobject.pskillID = objjpm.pskillID;
        //        oldobject.sskillID = objjpm.sskillID;
        //        oldobject.ExpId = objjpm.ExpId;
        //        oldobject.RelvExpId = objjpm.RelvExpId;
        //        oldobject.CertCode = objjpm.CertCode;
        //        EmployeeClone newobject = new EmployeeClone();
        //        //code for clone copy create for new object
        //        newobject.IsActive = JPM.IsActive;
        //        newobject.JobPosCode = JPM.JobPosCode;
        //        newobject.JobPosName = JPM.JobPosName;
        //        newobject.JobPosDesc = JPM.JobPosDesc;
        //        newobject.Gender = JPM.Gender;
        //        newobject.CompCode = JPM.CompCode;
        //        newobject.DepCode = JPM.DepCode;
        //        newobject.DesignCode = JPM.DesignCode;
        //        newobject.JobId = JPM.JobId;
        //        newobject.AgeId = JPM.AgeId;
        //        newobject.CatCode = JPM.CatCode;
        //        newobject.IMCode = JPM.IMCode;
        //        newobject.QID = QID;
        //        newobject.LangCode = lang;
        //        newobject.SpecLznID = Splzfld;
        //        newobject.pskillID = pss;
        //        newobject.sskillID = sss;
        //        newobject.ExpId = exp;
        //        newobject.RelvExpId = relvexp;
        //        newobject.CertCode = certcode;
        //        //code for compare IM code between old and new
        //        string[] arrIMold = objjpm.IMCode.Split(',');
        //        string[] arrIMnew = IMCode.Split(',');
        //        bool isSameIM = ArrayCompare(arrIMold, arrIMnew);
        //        if (isSameIM == true)
        //        {
        //            oldobject.IMCode = "";
        //            newobject.IMCode = "";
        //        }
        //        //code for comapre QID betwenn old and new
        //        string[] arrQIDold = objjpm.QID.Split(',');
        //        string[] arrQIDnew = QID.Split(',');
        //        bool isSameQID = ArrayCompare(arrQIDold, arrQIDnew);
        //        if (isSameQID == true)
        //        {
        //            oldobject.QID = "";
        //            newobject.QID = "";
        //        }
        //        //code for compare language betweenn old and new
        //        string[] arrLangold = objjpm.LangCode.Split(',');
        //        string[] arrLangnew = lang.Split(',');
        //        bool isSameLang = ArrayCompare(arrLangold, arrLangnew);
        //        if (isSameLang == true)
        //        {
        //            oldobject.LangCode = "";
        //            newobject.LangCode = "";
        //        }
        //        //code for compare Specialiazation betweenn old and new
        //        string[] arrsplznold = objjpm.SpecLznID.Split(',');
        //        string[] arrsplznnew = Splzfld.Split(',');
        //        bool isSplzfld = ArrayCompare(arrsplznold, arrsplznnew);
        //        if (isSplzfld == true)
        //        {
        //            oldobject.SpecLznID = "";
        //            newobject.SpecLznID = "";
        //        }
        //        //code for compare primary skill sets betweenn old and new
        //        string[] arrPSSold = objjpm.pskillID.Split(',');
        //        string[] arrPSSnew = pss.Split(',');
        //        bool isPSS = ArrayCompare(arrPSSold, arrPSSnew);
        //        if (isPSS == true)
        //        {
        //            oldobject.pskillID = "";
        //            newobject.pskillID = "";
        //        }
        //        //code for compare secondary  skill sets betweenn old and new
        //        string[] arrSSSold = objjpm.pskillID.Split(',');
        //        string[] arrSSSnew = pss.Split(',');
        //        bool isSSS = ArrayCompare(arrSSSold, arrSSSnew);
        //        if (isSSS == true)
        //        {
        //            oldobject.sskillID = "";
        //            newobject.sskillID = "";
        //        }
        //        //code for compare total experiencce betweenn old and new
        //        string[] arrEXPold = objjpm.ExpId.Split(',');
        //        string[] arrEXPnew = exp.Split(',');
        //        bool isEXP = ArrayCompare(arrEXPold, arrEXPnew);
        //        if (isEXP == true)
        //        {
        //            oldobject.ExpId = "";
        //            newobject.ExpId = "";
        //        }
        //        //code for compare rrelevant experiencce betweenn old and new
        //        string[] arrREXPold = objjpm.RelvExpId.Split(',');
        //        string[] arrREXPnew = relvexp.Split(',');
        //        bool isREXP = ArrayCompare(arrREXPold, arrREXPnew);
        //        if (isREXP == true)
        //        {
        //            oldobject.RelvExpId = "";
        //            newobject.RelvExpId = "";
        //        }
        //        //code for compare certificate code   betweenn old and new
        //        string[] arrCertold = objjpm.CertCode.Split(',');
        //        string[] arrCertnew = certcode.Split(',');
        //        bool isCert = ArrayCompare(arrCertold, arrCertnew);
        //        if (isCert == true)
        //        {
        //            oldobject.CertCode = "";
        //            newobject.CertCode = "";
        //        }
        //        AuditLog.CreateAuditTrail(Employee.EMPLOYEEMASTER, Employee.PAYCODE, JPM.JobPosCode, AppConstant.MODIFIED, JPM.LastModifiedBy, JPM.LoginTerminalNameIP, oldobject, newobject);
        //    }
        //    return res;
        //}

        public bool ArrayCompare(string[] arrQIDold, string[] arrQIDnew)
        {
            bool hasDuplicate = false;
            foreach (var numberA in arrQIDold)
            {
                foreach (var numberB in arrQIDnew)
                {
                    if (numberA == numberB)
                    {
                        hasDuplicate = true;
                        break;
                    }
                    else
                    {
                        hasDuplicate = false;
                    }
                }
                if (hasDuplicate == false)
                {
                    break;
                }
            }
            return hasDuplicate;
        }

        //public IEnumerable<JobPosPrimarySkill_list> BindPrimarySkill(string jobId)
        //{

        //    Employee jpm = new Employee();
        //    List<JobPosPrimarySkill_list> lst = new List<JobPosPrimarySkill_list>();
        //    DataTable dt = DataOperation.GetDataTableWithParameter("UspJobPos_PrimarySkillset",
        //                                                            new SqlParameter("JobID", jobId.Trim()));
        //    if (dt.Rows.Count > 0)
        //    {

        //        for (int index = 0; index < dt.Rows.Count; index++)
        //        {
        //            lst.Add(new JobPosPrimarySkill_list
        //            {
        //                pskillID = Convert.ToString(dt.Rows[index]["SkillId"]).Trim(),
        //                pskillName = Convert.ToString(dt.Rows[index]["SkillName"]).Trim(),

        //            });
        //        }

        //    }
        //    return lst;

        //}
        //public IEnumerable<JobPosSecondarySkill_list> BindSecondarySkill(string jobId)
        //{
        //    Employee jpm = new Employee();
        //    List<JobPosSecondarySkill_list> lst = new List<JobPosSecondarySkill_list>();
        //    DataTable dt = DataOperation.GetDataTableWithParameter("UspJobPos_SecondarySkillset",
        //                                                            new SqlParameter("JobID", jobId.Trim()));
        //    if (dt.Rows.Count > 0)
        //    {

        //        for (int index = 0; index < dt.Rows.Count; index++)
        //        {
        //            lst.Add(new JobPosSecondarySkill_list
        //            {
        //                sskillID = Convert.ToString(dt.Rows[index]["SkillId"]).Trim(),
        //                sskillName = Convert.ToString(dt.Rows[index]["SkillName"]).Trim(),

        //            });
        //        }

        //    }
        //    return lst;
        //}

        //public List<JobPosTotalExp_list> BindTotalExp(string jobId)
        //{
        //    Employee jpm = new Employee();
        //    List<JobPosTotalExp_list> lst = new List<JobPosTotalExp_list>();

        //    DataTable dt = DataOperation.GetDataTableWithParameter("UspJobPos_TotalExp",
        //                                                            new SqlParameter("JobID", jobId.Trim()));
        //    if (dt.Rows.Count > 0)
        //    {

        //        for (int index = 0; index < dt.Rows.Count; index++)
        //        {
        //            lst.Add(new JobPosTotalExp_list
        //            {
        //                ExpId = Convert.ToString(dt.Rows[index]["ExpId"]).Trim(),
        //                ExpDesc = Convert.ToString(dt.Rows[index]["ExpDesc"]).Trim(),
        //            });
        //        }

        //    }
        //    return lst;
        //}
        //public List<JobPosRelv_list> BindRelvExp(string jobId)
        //{
        //    Employee jpm = new Employee();
        //    List<JobPosRelv_list> lst = new List<JobPosRelv_list>();

        //    DataTable dt = DataOperation.GetDataTableWithParameter("UspJobPos_RelvExp",
        //                                                            new SqlParameter("JobID", jobId.Trim()));
        //    if (dt.Rows.Count > 0)
        //    {

        //        for (int index = 0; index < dt.Rows.Count; index++)
        //        {
        //            lst.Add(new JobPosRelv_list
        //            {
        //                RelvExpId = Convert.ToString(dt.Rows[index]["RelvExpid"]).Trim(),
        //                RelvExpDesc = Convert.ToString(dt.Rows[index]["RelvExpDesc"]).Trim(),
        //            });
        //        }

        //    }
        //    return lst;
        //}

        //public int DeleteEmployee(Employee JPM)
        //{
        //    int res = -1;
        //    Employee objjpm = GetJobPos(JPM.JobPosCode);
        //    EmployeeClone auditoldobject = new EmployeeClone();
        //    //code for clone copy create for old object
        //    auditoldobject.IsActive = objjpm.IsActive;
        //    auditoldobject.JobPosCode = objjpm.JobPosCode;
        //    auditoldobject.JobPosName = objjpm.JobPosName;
        //    auditoldobject.JobPosDesc = objjpm.JobPosDesc;
        //    auditoldobject.Gender = objjpm.Gender;
        //    auditoldobject.CompCode = objjpm.CompCode;
        //    auditoldobject.DepCode = objjpm.DepCode;
        //    auditoldobject.DesignCode = objjpm.DesignCode;
        //    auditoldobject.JobId = objjpm.JobId;
        //    auditoldobject.AgeId = objjpm.AgeId;
        //    auditoldobject.CatCode = objjpm.CatCode;
        //    auditoldobject.IMCode = objjpm.IMCode;
        //    auditoldobject.QID = objjpm.QID;
        //    auditoldobject.LangCode = objjpm.LangCode;
        //    auditoldobject.SpecLznID = objjpm.SpecLznID;
        //    auditoldobject.pskillID = objjpm.pskillID;
        //    auditoldobject.sskillID = objjpm.sskillID;
        //    auditoldobject.ExpId = objjpm.ExpId;
        //    auditoldobject.RelvExpId = objjpm.RelvExpId;
        //    auditoldobject.CertCode = objjpm.CertCode;
        //    EmployeeClone auditnewobject = new EmployeeClone();
        //    //code for clone copy create for new object
        //    auditnewobject.IsActive = "";
        //    auditnewobject.JobPosCode = "";
        //    auditnewobject.JobPosName = "";
        //    auditnewobject.JobPosDesc = "";
        //    auditnewobject.Gender = "";
        //    auditnewobject.CompCode = "";
        //    auditnewobject.DepCode = "";
        //    auditnewobject.DesignCode = "";
        //    auditnewobject.JobId = "";
        //    auditnewobject.AgeId = "";
        //    auditnewobject.CatCode = "";
        //    auditnewobject.IMCode = "";
        //    auditnewobject.QID = "";
        //    auditnewobject.LangCode = "";
        //    auditnewobject.SpecLznID = "";
        //    auditnewobject.pskillID = "";
        //    auditnewobject.sskillID = "";
        //    auditnewobject.ExpId = "";
        //    auditnewobject.RelvExpId = "";
        //    auditnewobject.CertCode = "";

        //    DataTable dt = DataOperation.GetDataTableWithParameter("Usp_CheckJobPosRefUse", new SqlParameter("JobPosCode", JPM.JobPosCode.Trim()));
        //    if (Convert.ToInt32(dt.Rows[0]["Column1"]) > 0)
        //    {
        //        res = 2;
        //    }
        //    else
        //    {
        //        res = DataOperation.InsUpdDel("Employee_Delete", new SqlParameter("JobPosCode", JPM.JobPosCode.Trim()));
        //        if (res == 1)
        //        {
        //            AuditLog.CreateAuditTrail(Employee.EMPLOYEEMASTER, Employee.PAYCODE, JPM.JobPosCode.Trim(), AppConstant.DELETED, JPM.LastModifiedBy, JPM.LoginTerminalNameIP, auditoldobject, auditnewobject);
        //        }
        //    }
        //    return res;
        //}

        public string GetAutoID(Employee JPM)
        {
            string res = "";
            DataTable dt = DataOperation.GetDataTableWithoutParameter("JobPosGenerateAutoCode");

            res = dt.Rows[0]["AutoID"].ToString();
            return res;

        }

        //To Add new Company record    
        public int Add(EmployeeGroupPolicy EMPGP)
        {
            string inonly = "N";
            string isPunchAll = "N";
            string Twopunch = "N";
            string IsOutWork = "N";

            EMPGP.sGroupID = (String.IsNullOrEmpty(EMPGP.sGroupID) ? "" : EMPGP.sGroupID);
            EMPGP.sGroupName = (String.IsNullOrEmpty(EMPGP.sGroupName) ? "" : EMPGP.sGroupName);
            EMPGP.sSHIFT = (String.IsNullOrEmpty(EMPGP.sSHIFT) ? "" : EMPGP.sSHIFT);
            EMPGP.sSHIFTTYPE = (String.IsNullOrEmpty(EMPGP.sSHIFTTYPE) ? "" : EMPGP.sSHIFTTYPE);
            EMPGP.sSHIFTPATTERN = (String.IsNullOrEmpty(EMPGP.sSHIFTPATTERN) ? "" : EMPGP.sSHIFTPATTERN);
            //EMPGP.sSHIFTREMAINDAYS = (String.IsNullOrEmpty(EMPGP.sSHIFTREMAINDAYS) ? "" : EMPGP.sSHIFTREMAINDAYS);
            EMPGP.sLASTSHIFTPERFORMED = (String.IsNullOrEmpty(EMPGP.sLASTSHIFTPERFORMED) ? "" : EMPGP.sLASTSHIFTPERFORMED);
            EMPGP.sINONLY = (String.IsNullOrEmpty(EMPGP.sINONLY) ? "" : EMPGP.sINONLY);
            EMPGP.sISPUNCHALL = (String.IsNullOrEmpty(EMPGP.sISPUNCHALL) ? "" : EMPGP.sISPUNCHALL);
            EMPGP.sISTIMELOSSALLOWED = (String.IsNullOrEmpty(EMPGP.sISTIMELOSSALLOWED) ? "" : EMPGP.sISTIMELOSSALLOWED);
            EMPGP.sALTERNATE_OFF_DAYS = (String.IsNullOrEmpty(EMPGP.sALTERNATE_OFF_DAYS) ? "" : EMPGP.sALTERNATE_OFF_DAYS);
            EMPGP.sCDAYS = (String.IsNullOrEmpty(EMPGP.sCDAYS) ? "" : EMPGP.sCDAYS);
            EMPGP.sISROUNDTHECLOCKWORK = (String.IsNullOrEmpty(EMPGP.sISROUNDTHECLOCKWORK) ? "" : EMPGP.sISROUNDTHECLOCKWORK);
            EMPGP.sISOT = (String.IsNullOrEmpty(EMPGP.sISOT) ? "" : EMPGP.sISOT);
            EMPGP.sOTRATE = (String.IsNullOrEmpty(EMPGP.sOTRATE) ? "" : EMPGP.sOTRATE);
            EMPGP.sFIRSTOFFDAY = (String.IsNullOrEmpty(EMPGP.sFIRSTOFFDAY) ? "" : EMPGP.sFIRSTOFFDAY);
            EMPGP.sSECONDOFFTYPE = (String.IsNullOrEmpty(EMPGP.sSECONDOFFTYPE) ? "" : EMPGP.sSECONDOFFTYPE);
            EMPGP.sHALFDAYSHIFT = (String.IsNullOrEmpty(EMPGP.sHALFDAYSHIFT) ? "" : EMPGP.sHALFDAYSHIFT);
            EMPGP.sSECONDOFFDAY = (String.IsNullOrEmpty(EMPGP.sSECONDOFFDAY) ? "" : EMPGP.sSECONDOFFDAY);
            //EMPGP.sPERMISLATEARRIVAL = (String.IsNullOrEmpty(EMPGP.sPERMISLATEARRIVAL) ? "" : EMPGP.sPERMISLATEARRIVAL);
            //EMPGP.sPERMISEARLYDEPRT = (String.IsNullOrEmpty(EMPGP.sPERMISEARLYDEPRT) ? "" : EMPGP.sPERMISEARLYDEPRT);
            EMPGP.sISAUTOSHIFT = (String.IsNullOrEmpty(EMPGP.sISAUTOSHIFT) ? "" : EMPGP.sISAUTOSHIFT);
            EMPGP.sISOUTWORK = (String.IsNullOrEmpty(EMPGP.sISOUTWORK) ? "" : EMPGP.sISOUTWORK);
            EMPGP.sMAXDAYMIN = (String.IsNullOrEmpty(EMPGP.sMAXDAYMIN) ? "" : EMPGP.sMAXDAYMIN);
            EMPGP.sISOS = (String.IsNullOrEmpty(EMPGP.sISOS) ? "" : EMPGP.sISOS);
            EMPGP.sAUTH_SHIFTS = (String.IsNullOrEmpty(EMPGP.sAUTH_SHIFTS) ? "" : EMPGP.sAUTH_SHIFTS);
            //EMPGP.sTIME = (String.IsNullOrEmpty(EMPGP.sTIME) ? "" : EMPGP.sTIME);
            //EMPGP.sSHORT = (String.IsNullOrEmpty(EMPGP.sSHORT) ? "" : EMPGP.sSHORT);
            //EMPGP.sHALF = (String.IsNullOrEmpty(EMPGP.sHALF) ? "" : EMPGP.sHALF);
            EMPGP.sISHALFDAY = (String.IsNullOrEmpty(EMPGP.sISHALFDAY) ? "" : EMPGP.sISHALFDAY);
            EMPGP.sISSHORT = (String.IsNullOrEmpty(EMPGP.sISSHORT) ? "" : EMPGP.sISSHORT);
            EMPGP.sTWO = (String.IsNullOrEmpty(EMPGP.sTWO) ? "" : EMPGP.sTWO);
            EMPGP.sisReleaver = (String.IsNullOrEmpty(EMPGP.sisReleaver) ? "" : EMPGP.sisReleaver);
            EMPGP.sisWorker = (String.IsNullOrEmpty(EMPGP.sisWorker) ? "" : EMPGP.sisWorker);
            EMPGP.sisFlexi = (String.IsNullOrEmpty(EMPGP.sisFlexi) ? "" : EMPGP.sisFlexi);
            EMPGP.sSSN = (String.IsNullOrEmpty(EMPGP.sSSN) ? "" : EMPGP.sSSN);
            EMPGP.sMIS = (String.IsNullOrEmpty(EMPGP.sMIS) ? "" : EMPGP.sMIS);
            EMPGP.sIsCOF = (String.IsNullOrEmpty(EMPGP.sIsCOF) ? "" : EMPGP.sIsCOF);
            //EMPGP.sHLFAfter = (String.IsNullOrEmpty(EMPGP.sHLFAfter) ? "" : EMPGP.sHLFAfter);
            //EMPGP.sHLFBefore = (String.IsNullOrEmpty(EMPGP.sHLFBefore) ? "" : EMPGP.sHLFBefore);
            EMPGP.sResignedAfter = (String.IsNullOrEmpty(EMPGP.sResignedAfter) ? "" : EMPGP.sResignedAfter);
            EMPGP.sEnableAutoResign = (String.IsNullOrEmpty(EMPGP.sEnableAutoResign) ? "" : EMPGP.sEnableAutoResign);
            EMPGP.sS_END = (String.IsNullOrEmpty(EMPGP.sS_END) ? "" : EMPGP.sS_END);
            EMPGP.sS_OUT = (String.IsNullOrEmpty(EMPGP.sS_OUT) ? "" : EMPGP.sS_OUT);
            //EMPGP.sAUTOSHIFT_LOW = (String.IsNullOrEmpty(EMPGP.sAUTOSHIFT_LOW) ? "" : EMPGP.sAUTOSHIFT_LOW);
            //EMPGP.sAUTOSHIFT_UP = (String.IsNullOrEmpty(EMPGP.sAUTOSHIFT_UP) ? "" : EMPGP.sAUTOSHIFT_UP);
            EMPGP.sISPRESENTONWOPRESENT = (String.IsNullOrEmpty(EMPGP.sISPRESENTONWOPRESENT) ? "" : EMPGP.sISPRESENTONWOPRESENT);
            EMPGP.sISPRESENTONHLDPRESENT = (String.IsNullOrEmpty(EMPGP.sISPRESENTONHLDPRESENT) ? "" : EMPGP.sISPRESENTONHLDPRESENT);
            EMPGP.sNightShiftFourPunch = (String.IsNullOrEmpty(EMPGP.sNightShiftFourPunch) ? "" : EMPGP.sNightShiftFourPunch);
            EMPGP.sISAUTOABSENT = (String.IsNullOrEmpty(EMPGP.sISAUTOABSENT) ? "" : EMPGP.sISAUTOABSENT);
            EMPGP.sISAWA = (String.IsNullOrEmpty(EMPGP.sISAWA) ? "" : EMPGP.sISAWA);
            EMPGP.sISWA = (String.IsNullOrEmpty(EMPGP.sISWA) ? "" : EMPGP.sISWA);
            EMPGP.sISAW = (String.IsNullOrEmpty(EMPGP.sISAW) ? "" : EMPGP.sISAW);
            EMPGP.sISPREWO = (String.IsNullOrEmpty(EMPGP.sISPREWO) ? "" : EMPGP.sISPREWO);
            EMPGP.sISOTOUTMINUSSHIFTENDTIME = (String.IsNullOrEmpty(EMPGP.sISOTOUTMINUSSHIFTENDTIME) ? "" : EMPGP.sISOTOUTMINUSSHIFTENDTIME);
            EMPGP.sISOTWRKGHRSMINUSSHIFTHRS = (String.IsNullOrEmpty(EMPGP.sISOTWRKGHRSMINUSSHIFTHRS) ? "" : EMPGP.sISOTWRKGHRSMINUSSHIFTHRS);
            EMPGP.sISOTEARLYCOMEPLUSLATEDEP = (String.IsNullOrEmpty(EMPGP.sISOTEARLYCOMEPLUSLATEDEP) ? "" : EMPGP.sISOTEARLYCOMEPLUSLATEDEP);

            EMPGP.sDEDUCTHOLIDAYOT = (String.IsNullOrEmpty(EMPGP.sDEDUCTHOLIDAYOT) ? "0" : EMPGP.sDEDUCTHOLIDAYOT);
            EMPGP.sDEDUCTWOOT = (String.IsNullOrEmpty(EMPGP.sDEDUCTWOOT) ? "0" : EMPGP.sDEDUCTWOOT);

            EMPGP.sISOTEARLYCOMING = (String.IsNullOrEmpty(EMPGP.sISOTEARLYCOMING) ? "" : EMPGP.sISOTEARLYCOMING);
            EMPGP.sOTMinus = (String.IsNullOrEmpty(EMPGP.sOTMinus) ? "" : EMPGP.sOTMinus);
            EMPGP.sOTROUND = (String.IsNullOrEmpty(EMPGP.sOTROUND) ? "" : EMPGP.sOTROUND);

            EMPGP.sOTEARLYDUR = (String.IsNullOrEmpty(EMPGP.sOTEARLYDUR) ? "0" : EMPGP.sOTEARLYDUR);
            EMPGP.sOTLATECOMINGDUR = (String.IsNullOrEmpty(EMPGP.sOTLATECOMINGDUR) ? "0" : EMPGP.sOTLATECOMINGDUR);
            EMPGP.sOTRESTRICTENDDUR = (String.IsNullOrEmpty(EMPGP.sOTRESTRICTENDDUR) ? "0" : EMPGP.sOTRESTRICTENDDUR);
            EMPGP.sDUPLICATECHECKMIN = (String.IsNullOrEmpty(EMPGP.sDUPLICATECHECKMIN) ? "0" : EMPGP.sDUPLICATECHECKMIN);
            EMPGP.sPREWO = (String.IsNullOrEmpty(EMPGP.sPREWO) ? "0" : EMPGP.sPREWO);

            EMPGP.sCOMPANYCODE = (String.IsNullOrEmpty(EMPGP.sCOMPANYCODE) ? "" : EMPGP.sCOMPANYCODE);

            if (EMPGP.sIsCOF.ToUpper().Trim() == "ON")
                EMPGP.sIsCOF = "Y";
            else
                EMPGP.sIsCOF = "N";

            if (EMPGP.sISPRESENTONWOPRESENT.ToUpper().Trim() == "ON")
                EMPGP.sISPRESENTONWOPRESENT = "Y";
            else
                EMPGP.sISPRESENTONWOPRESENT = "N";

            if (EMPGP.sISPRESENTONHLDPRESENT.ToUpper().Trim() == "ON")
                EMPGP.sISPRESENTONHLDPRESENT = "Y";
            else
                EMPGP.sISPRESENTONHLDPRESENT = "N";

            if (EMPGP.sNightShiftFourPunch.ToUpper().Trim() == "ON")
                EMPGP.sNightShiftFourPunch = "Y";
            else
                EMPGP.sNightShiftFourPunch = "N";

            if (EMPGP.sISAUTOABSENT.ToUpper().Trim() == "ON")
                EMPGP.sISAUTOABSENT = "Y";
            else
                EMPGP.sISAUTOABSENT = "N";

            if (EMPGP.sISAWA.ToUpper().Trim() == "ON")
                EMPGP.sISAWA = "Y";
            else
                EMPGP.sISAWA = "N";

            if (EMPGP.sISWA.ToUpper().Trim() == "ON")
                EMPGP.sISWA = "Y";
            else
                EMPGP.sISWA = "N";

            if (EMPGP.sISPREWO.ToUpper().Trim() == "ON")
                EMPGP.sISPREWO = "Y";
            else
                EMPGP.sISPREWO = "N";

            if (EMPGP.sISOTWRKGHRSMINUSSHIFTHRS.ToUpper().Trim() == "ON")
                EMPGP.sISOTWRKGHRSMINUSSHIFTHRS = "Y";
            else
                EMPGP.sISOTWRKGHRSMINUSSHIFTHRS = "N";

            if (EMPGP.sISOTEARLYCOMEPLUSLATEDEP.ToUpper().Trim() == "ON")
                EMPGP.sISOTEARLYCOMEPLUSLATEDEP = "Y";
            else
                EMPGP.sISOTEARLYCOMEPLUSLATEDEP = "N";


            //------------OT Option----------------
            if (EMPGP.sISOTOUTMINUSSHIFTENDTIME.ToString().Trim().ToUpper() == "ON")
            {
                EMPGP.sISOTOUTMINUSSHIFTENDTIME = "Y";
            }
            else if (EMPGP.sISOTWRKGHRSMINUSSHIFTHRS.ToString().Trim().ToUpper() == "ON")
            {
                EMPGP.sISOTWRKGHRSMINUSSHIFTHRS = "Y";
            }
            else if (EMPGP.sISOTEARLYCOMEPLUSLATEDEP.ToString().Trim().ToUpper() == "ON")
            {
                EMPGP.sISOTEARLYCOMEPLUSLATEDEP = "Y";
            }

            //------------Punches combinations----------------
            if (EMPGP.sISPUNCHALL.ToString().Trim() == "NoPunch")
            {
                inonly = "N";
                isPunchAll = "N";
                Twopunch = "N";
                IsOutWork = "N";
            }
            else if (EMPGP.sISPUNCHALL.ToString().Trim() == "SinglePunchOnly")
            {
                if (EMPGP.sISPUNCHALL.ToString().Trim() == "O")
                    inonly = "O";
                else
                    inonly = "Y";

                isPunchAll = "Y";
                Twopunch = "N";
                IsOutWork = "N";
            }
            else if (EMPGP.sISPUNCHALL.ToString().Trim() == "TwoPunches")
            {
                inonly = "N";
                isPunchAll = "Y";
                Twopunch = "Y";
                IsOutWork = "N";
            }
            else if (EMPGP.sISPUNCHALL.ToString().Trim() == "FourPunch")
            {
                inonly = "N";
                isPunchAll = "Y";
                Twopunch = "N";
                IsOutWork = "N";
            }
            else if (EMPGP.sISPUNCHALL.ToString().Trim() == "MultiplePunch")
            {
                inonly = "N";
                isPunchAll = "Y";
                Twopunch = "N";
                IsOutWork = "Y";
            }

            //------------Second Week Days----------------           
            string days = "";
            if (EMPGP.sALTERNATE_OFF_DAYS_1.ToString().Trim() == "" || EMPGP.sALTERNATE_OFF_DAYS_2.ToString().Trim() == "" || EMPGP.sALTERNATE_OFF_DAYS_3.ToString().Trim() == "" || EMPGP.sALTERNATE_OFF_DAYS_4.ToString().Trim() == "" || EMPGP.sALTERNATE_OFF_DAYS_5.ToString().Trim() == "")
            {
                days = "";
            }
            if (EMPGP.sALTERNATE_OFF_DAYS_1.ToString().Trim() == "on")
            {
                days += 1;
            }
            if (EMPGP.sALTERNATE_OFF_DAYS_2.ToString().Trim() == "on")
            {
                days += 2;
            }
            if (EMPGP.sALTERNATE_OFF_DAYS_3.ToString().Trim() == "on")
            {
                days += 3;
            }
            if (EMPGP.sALTERNATE_OFF_DAYS_4.ToString().Trim() == "on")
            {
                days += 4;
            }
            if (EMPGP.sALTERNATE_OFF_DAYS_5.ToString().Trim() == "on")
            {
                days += 5;
            }

            int res = DataOperation.InsUpdDel("tblEmployeeGroupPolicy_Add",
                new SqlParameter("GroupID", EMPGP.sGroupID),
                new SqlParameter("GroupName", EMPGP.sGroupName),
                new SqlParameter("SHIFT", EMPGP.sSHIFT),
                new SqlParameter("SHIFTTYPE", EMPGP.sSHIFTTYPE),
                new SqlParameter("SHIFTPATTERN", EMPGP.sSHIFTPATTERN),
                new SqlParameter("SHIFTREMAINDAYS", EMPGP.sSHIFTREMAINDAYS),
                //new SqlParameter("LASTSHIFTPERFORMED", EMPGP.sLASTSHIFTPERFORMED),
                new SqlParameter("INONLY", inonly),
                new SqlParameter("ISPUNCHALL", isPunchAll),
                new SqlParameter("ISTIMELOSSALLOWED", IsOutWork),
                new SqlParameter("ALTERNATE_OFF_DAYS", EMPGP.sALTERNATE_OFF_DAYS),
                new SqlParameter("CDAYS", EMPGP.sCDAYS),
                new SqlParameter("ISROUNDTHECLOCKWORK", EMPGP.sISROUNDTHECLOCKWORK),
                new SqlParameter("ISOT", EMPGP.sISOT),
                new SqlParameter("OTRATE", EMPGP.sOTRATE),
                new SqlParameter("FIRSTOFFDAY", EMPGP.sFIRSTOFFDAY),
                new SqlParameter("SECONDOFFTYPE", EMPGP.sSECONDOFFTYPE),
                new SqlParameter("HALFDAYSHIFT", EMPGP.sHALFDAYSHIFT),
                new SqlParameter("SECONDOFFDAY", EMPGP.sSECONDOFFDAY),
                new SqlParameter("PERMISLATEARRIVAL", Convert.ToInt32(EMPGP.sPERMISLATEARRIVAL)),
                new SqlParameter("PERMISEARLYDEPRT", Convert.ToInt32(EMPGP.sPERMISEARLYDEPRT)),
                new SqlParameter("ISAUTOSHIFT", EMPGP.sISAUTOSHIFT),
                new SqlParameter("ISOUTWORK", EMPGP.sISOUTWORK),
                new SqlParameter("MAXDAYMIN", EMPGP.sMAXDAYMIN),
                new SqlParameter("ISOS", EMPGP.sISOS),
                new SqlParameter("AUTH_SHIFTS", EMPGP.sAUTH_SHIFTS),
                new SqlParameter("TIME", Convert.ToInt32(EMPGP.sTIME)),
                new SqlParameter("SHORT", Convert.ToInt32(EMPGP.sSHORT)),
                new SqlParameter("HALF", Convert.ToInt32(EMPGP.sHALF)),
                new SqlParameter("ISHALFDAY", EMPGP.sISHALFDAY),
                new SqlParameter("ISSHORT", EMPGP.sISSHORT),
                new SqlParameter("TWO", Twopunch),
                new SqlParameter("isReleaver", EMPGP.sisReleaver),
                new SqlParameter("isWorker", EMPGP.sisWorker),
                new SqlParameter("isFlexi", EMPGP.sisFlexi),
                new SqlParameter("SSN", EMPGP.sSSN),
                new SqlParameter("MIS", EMPGP.sMIS),
                new SqlParameter("IsCOF", EMPGP.sIsCOF),
                new SqlParameter("HLFAfter", Convert.ToInt32(EMPGP.sHLFAfter)),
                new SqlParameter("HLFBefore", Convert.ToInt32(EMPGP.sHLFBefore)),
                new SqlParameter("ResignedAfter", EMPGP.sResignedAfter),
                new SqlParameter("EnableAutoResign", EMPGP.sEnableAutoResign),
                new SqlParameter("S_END", EMPGP.sS_END),
                new SqlParameter("S_OUT", EMPGP.sS_OUT),
                new SqlParameter("AUTOSHIFT_LOW", Convert.ToInt32(EMPGP.sAUTOSHIFT_LOW)),
                new SqlParameter("AUTOSHIFT_UP", Convert.ToInt32(EMPGP.sAUTOSHIFT_UP)),
                new SqlParameter("ISPRESENTONWOPRESENT", EMPGP.sISPRESENTONWOPRESENT),
                new SqlParameter("ISPRESENTONHLDPRESENT", EMPGP.sISPRESENTONHLDPRESENT),
                new SqlParameter("NightShiftFourPunch", EMPGP.sNightShiftFourPunch),
                new SqlParameter("ISAUTOABSENT", EMPGP.sISAUTOABSENT),
                new SqlParameter("ISAWA", EMPGP.sISAWA),
                new SqlParameter("ISWA", EMPGP.sISWA),
                new SqlParameter("ISAW", EMPGP.sISAW),
                new SqlParameter("ISPREWO", EMPGP.sISPREWO),
                new SqlParameter("ISOTOUTMINUSSHIFTENDTIME", EMPGP.sISOTOUTMINUSSHIFTENDTIME),
                new SqlParameter("ISOTWRKGHRSMINUSSHIFTHRS", EMPGP.sISOTWRKGHRSMINUSSHIFTHRS),
                new SqlParameter("ISOTEARLYCOMEPLUSLATEDEP", EMPGP.sISOTEARLYCOMEPLUSLATEDEP),
                new SqlParameter("DEDUCTHOLIDAYOT", EMPGP.sDEDUCTHOLIDAYOT),
                new SqlParameter("DEDUCTWOOT", EMPGP.sDEDUCTWOOT),
                new SqlParameter("ISOTEARLYCOMING", EMPGP.sISOTEARLYCOMING),
                new SqlParameter("OTMinus", EMPGP.sOTMinus),
                new SqlParameter("OTROUND", EMPGP.sOTROUND),
                new SqlParameter("OTEARLYDUR", EMPGP.sOTEARLYDUR),
                new SqlParameter("OTLATECOMINGDUR", EMPGP.sOTLATECOMINGDUR),
                new SqlParameter("OTRESTRICTENDDUR", EMPGP.sOTRESTRICTENDDUR),
                new SqlParameter("DUPLICATECHECKMIN", EMPGP.sDUPLICATECHECKMIN),
                new SqlParameter("PREWO", EMPGP.sPREWO),
                new SqlParameter("CompanyCode", EMPGP.sCOMPANYCODE));
            if (res == 1)
            {
                //int resEmpShiftMat = DataOperation.InsUpdDel("UpdateEmployeeShiftMaster",
                //         new SqlParameter("SHIFT", EMPGP.sSHIFT),
                //         new SqlParameter("SHIFTTYPE", EMPGP.sSHIFTTYPE),
                //         new SqlParameter("SHIFTPATTERN", EMPGP.sSHIFTPATTERN),
                //         new SqlParameter("SHIFTREMAINDAYS", EMPGP.sSHIFTREMAINDAYS),
                //         new SqlParameter("INONLY", EMPGP.sINONLY),
                //         new SqlParameter("ISPUNCHALL", EMPGP.sISPUNCHALL),
                //         new SqlParameter("ISTIMELOSSALLOWED", EMPGP.sISTIMELOSSALLOWED),
                //         new SqlParameter("ALTERNATE_OFF_DAYS", EMPGP.sALTERNATE_OFF_DAYS),
                //         new SqlParameter("CDAYS", EMPGP.sCDAYS),
                //         new SqlParameter("ISROUNDTHECLOCKWORK", EMPGP.sISROUNDTHECLOCKWORK),
                //         new SqlParameter("ISOT", EMPGP.sISOT),
                //         new SqlParameter("OTRATE", EMPGP.sOTRATE),
                //         new SqlParameter("FIRSTOFFDAY", EMPGP.sFIRSTOFFDAY),
                //         new SqlParameter("SECONDOFFTYPE", EMPGP.sSECONDOFFTYPE),
                //         new SqlParameter("HALFDAYSHIFT", EMPGP.sHALFDAYSHIFT),
                //         new SqlParameter("SECONDOFFDAY", EMPGP.sSECONDOFFDAY),
                //         new SqlParameter("PERMISLATEARRIVAL", EMPGP.sPERMISLATEARRIVAL),
                //         new SqlParameter("PERMISEARLYDEPRT", EMPGP.sPERMISEARLYDEPRT),
                //         new SqlParameter("ISAUTOSHIFT", EMPGP.sISAUTOSHIFT),
                //         new SqlParameter("IsOutWork", EMPGP.sISOUTWORK),
                //         new SqlParameter("MAXDAYMIN", EMPGP.sMAXDAYMIN),
                //         new SqlParameter("ISOS", EMPGP.sISOS),
                //         new SqlParameter("AUTH_SHIFTS", EMPGP.sAUTH_SHIFTS),
                //         new SqlParameter("[TIME]", EMPGP.sTIME),
                //         new SqlParameter("Short", EMPGP.sSHORT),
                //         new SqlParameter("Half", EMPGP.sHALF),
                //         new SqlParameter("ISHALFDAY", EMPGP.sISHALFDAY),
                //         new SqlParameter("ISSHORT", EMPGP.sISSHORT),
                //         new SqlParameter("TWO", EMPGP.sTWO),
                //         new SqlParameter("isWorker", EMPGP.sisWorker),
                //         new SqlParameter("isFlexi", EMPGP.sisFlexi),
                //         new SqlParameter("MIS", EMPGP.sMIS),
                //         new SqlParameter("IsCOF", EMPGP.sIsCOF),
                //         new SqlParameter("HLFAfter", EMPGP.sHLFAfter),
                //         new SqlParameter("HLFBefore", EMPGP.sHLFBefore),
                //         new SqlParameter("ResignedAfter", EMPGP.sResignedAfter),
                //         new SqlParameter("EnableAutoResign", EMPGP.sEnableAutoResign),
                //         new SqlParameter("[S_END]", EMPGP.sS_END),
                //         new SqlParameter("[S_OUT]", EMPGP.sS_OUT),
                //         new SqlParameter("[AUTOSHIFT_LOW]", EMPGP.sAUTOSHIFT_LOW),
                //         new SqlParameter("[AUTOSHIFT_UP]", EMPGP.sAUTOSHIFT_UP),
                //         new SqlParameter("[ISPRESENTONWOPRESENT]", EMPGP.sISPRESENTONWOPRESENT),
                //         new SqlParameter("[ISPRESENTONHLDPRESENT]", EMPGP.sISPRESENTONHLDPRESENT),
                //         new SqlParameter("[NightShiftFourPunch]", EMPGP.sNightShiftFourPunch),
                //         new SqlParameter("[ISAUTOABSENT]", EMPGP.sISAUTOABSENT),
                //         new SqlParameter("[ISAWA]", EMPGP.sISAWA),
                //         new SqlParameter("[ISWA]", EMPGP.sISWA),
                //         new SqlParameter("[ISAW]", EMPGP.sISAW),
                //         new SqlParameter("[ISPREWO]", EMPGP.sISPREWO),
                //         new SqlParameter("[ISOTOUTMINUSSHIFTENDTIME]", EMPGP.sISOTOUTMINUSSHIFTENDTIME),
                //         new SqlParameter("[ISOTWRKGHRSMINUSSHIFTHRS]", EMPGP.sISOTWRKGHRSMINUSSHIFTHRS),
                //         new SqlParameter("[ISOTEARLYCOMEPLUSLATEDEP]", EMPGP.sISOTEARLYCOMEPLUSLATEDEP),
                //         new SqlParameter("[DEDUCTHOLIDAYOT]", EMPGP.sDEDUCTHOLIDAYOT),
                //         new SqlParameter("[DEDUCTWOOT]", EMPGP.sDEDUCTWOOT),
                //         new SqlParameter("[ISOTEARLYCOMING]", EMPGP.sISOTEARLYCOMING),
                //         new SqlParameter("[OTMinus]", EMPGP.sOTMinus),
                //         new SqlParameter("[OTROUND]", EMPGP.sOTROUND),
                //         new SqlParameter("[OTEARLYDUR]", EMPGP.sOTEARLYDUR),
                //         new SqlParameter("[OTLATECOMINGDUR]", EMPGP.sOTLATECOMINGDUR),
                //         new SqlParameter("[OTRESTRICTENDDUR]", EMPGP.sOTRESTRICTENDDUR),
                //         new SqlParameter("[DUPLICATECHECKMIN]", EMPGP.sDUPLICATECHECKMIN),
                //         new SqlParameter("[PREWO]", EMPGP.sPREWO));
            }
            return res;
        }

        public EmployeeGroupPolicy GetGetEmpGroup(string ID)
        {
            EmployeeGroupPolicy EmpGpPlc = new EmployeeGroupPolicy();

            DataTable dt = DataOperation.GetDataTableWithParameter("tblEmployeeGroupPolicy_GetData", new SqlParameter("GroupID", ID.Trim()));
            if (dt.Rows.Count > 0)
            {
                for (int index = 0; index < dt.Rows.Count; index++)
                {
                    EmpGpPlc.sGroupID = dt.Rows[index]["GroupID"].ToString().Trim();
                    EmpGpPlc.sGroupName = dt.Rows[index]["GroupName"].ToString().Trim();
                    EmpGpPlc.sSHIFT = dt.Rows[index]["SHIFT"].ToString().Trim();
                    EmpGpPlc.sSHIFTTYPE = dt.Rows[index]["SHIFTTYPE"].ToString().Trim();
                    EmpGpPlc.sSHIFTPATTERN = dt.Rows[index]["SHIFTPATTERN"].ToString().Trim();
                    EmpGpPlc.sSHIFTREMAINDAYS = dt.Rows[index]["SHIFTREMAINDAYS"].ToString().Trim();
                    EmpGpPlc.sLASTSHIFTPERFORMED = dt.Rows[index]["LASTSHIFTPERFORMED"].ToString().Trim();
                    EmpGpPlc.sINONLY = dt.Rows[index]["INONLY"].ToString().Trim();
                    EmpGpPlc.sISPUNCHALL = dt.Rows[index]["ISPUNCHALL"].ToString().Trim();
                    EmpGpPlc.sISTIMELOSSALLOWED = dt.Rows[index]["ISTIMELOSSALLOWED"].ToString().Trim();
                    EmpGpPlc.sALTERNATE_OFF_DAYS = dt.Rows[index]["ALTERNATE_OFF_DAYS"].ToString().Trim();
                    EmpGpPlc.sCDAYS = dt.Rows[index]["CDAYS"].ToString().Trim();
                    EmpGpPlc.sISROUNDTHECLOCKWORK = dt.Rows[index]["ISROUNDTHECLOCKWORK"].ToString().Trim();
                    EmpGpPlc.sISOT = dt.Rows[index]["ISOT"].ToString().Trim();
                    EmpGpPlc.sOTRATE = dt.Rows[index]["OTRATE"].ToString().Trim();
                    EmpGpPlc.sFIRSTOFFDAY = dt.Rows[index]["FIRSTOFFDAY"].ToString().Trim();
                    EmpGpPlc.sSECONDOFFTYPE = dt.Rows[index]["SECONDOFFTYPE"].ToString().Trim();
                    EmpGpPlc.sHALFDAYSHIFT = dt.Rows[index]["HALFDAYSHIFT"].ToString().Trim();
                    EmpGpPlc.sSECONDOFFDAY = dt.Rows[index]["SECONDOFFDAY"].ToString().Trim();
                    //EmpGpPlc.sPERMISLATEARRIVAL = Convert.ToInt32(dt.Rows[index]["PERMISLATEARRIVAL"].ToString().Trim());

                    EmpGpPlc.tsPERMISLATEARRIVAL = TimeSpan.FromMinutes(Convert.ToInt32(dt.Rows[index]["PERMISLATEARRIVAL"].ToString().Trim()));
                    //int hh = timeSpan.Hours;
                    //int mm = timeSpan.Minutes;
                    //int ss = timeSpan.Seconds;

                    //EmpGpPlc.sPERMISEARLYDEPRT = Convert.ToInt32(dt.Rows[index]["PERMISEARLYDEPRT"].ToString().Trim());

                    EmpGpPlc.tsPERMISEARLYDEPRT = TimeSpan.FromMinutes(Convert.ToInt32(dt.Rows[index]["PERMISEARLYDEPRT"].ToString().Trim()));

                    EmpGpPlc.sISAUTOSHIFT = dt.Rows[index]["ISAUTOSHIFT"].ToString().Trim();
                    EmpGpPlc.sISOUTWORK = dt.Rows[index]["ISOUTWORK"].ToString().Trim();
                    EmpGpPlc.sMAXDAYMIN = dt.Rows[index]["MAXDAYMIN"].ToString().Trim();
                    EmpGpPlc.sISOS = dt.Rows[index]["ISOS"].ToString().Trim();
                    EmpGpPlc.sAUTH_SHIFTS = dt.Rows[index]["AUTH_SHIFTS"].ToString().Trim();

                    //EmpGpPlc.sTIME = Convert.ToInt32(dt.Rows[index]["TIME"].ToString().Trim());
                    EmpGpPlc.tsTIME = TimeSpan.FromMinutes(Convert.ToInt32(dt.Rows[index]["TIME"].ToString().Trim()));

                    //EmpGpPlc.sSHORT = Convert.ToInt32(dt.Rows[index]["SHORT"].ToString().Trim());
                    EmpGpPlc.tsSHORT = TimeSpan.FromMinutes(Convert.ToInt32(dt.Rows[index]["SHORT"].ToString().Trim()));

                    //EmpGpPlc.sHALF = Convert.ToInt32(dt.Rows[index]["HALF"].ToString().Trim());
                    EmpGpPlc.tsHALF = TimeSpan.FromMinutes(Convert.ToInt32(dt.Rows[index]["HALF"].ToString().Trim()));

                    EmpGpPlc.sISHALFDAY = dt.Rows[index]["ISHALFDAY"].ToString().Trim();
                    EmpGpPlc.sISSHORT = dt.Rows[index]["ISSHORT"].ToString().Trim();
                    EmpGpPlc.sTWO = dt.Rows[index]["TWO"].ToString().Trim();
                    EmpGpPlc.sisReleaver = dt.Rows[index]["isReleaver"].ToString().Trim();
                    EmpGpPlc.sisWorker = dt.Rows[index]["isWorker"].ToString().Trim();
                    EmpGpPlc.sisFlexi = dt.Rows[index]["isFlexi"].ToString().Trim();
                    EmpGpPlc.sSSN = dt.Rows[index]["SSN"].ToString().Trim();
                    EmpGpPlc.sMIS = dt.Rows[index]["MIS"].ToString().Trim();
                    EmpGpPlc.sIsCOF = dt.Rows[index]["IsCOF"].ToString().Trim();

                    //EmpGpPlc.sHLFAfter = Convert.ToInt32(dt.Rows[index]["HLFAfter"].ToString().Trim());
                    EmpGpPlc.tsHLFAfter = TimeSpan.FromMinutes(Convert.ToInt32(dt.Rows[index]["HLFAfter"].ToString().Trim()));

                    //EmpGpPlc.sHLFBefore = Convert.ToInt32(dt.Rows[index]["HLFBefore"].ToString().Trim());
                    EmpGpPlc.tsHLFBefore = TimeSpan.FromMinutes(Convert.ToInt32(dt.Rows[index]["HLFBefore"].ToString().Trim()));

                    EmpGpPlc.sResignedAfter = dt.Rows[index]["ResignedAfter"].ToString().Trim();
                    EmpGpPlc.sEnableAutoResign = dt.Rows[index]["EnableAutoResign"].ToString().Trim();
                    EmpGpPlc.sS_END = dt.Rows[index]["S_END"].ToString().Trim();
                    EmpGpPlc.sS_OUT = dt.Rows[index]["S_OUT"].ToString().Trim();

                    //EmpGpPlc.sAUTOSHIFT_LOW = Convert.ToInt32(dt.Rows[index]["AUTOSHIFT_LOW"].ToString().Trim());
                    EmpGpPlc.tsAUTOSHIFT_LOW = TimeSpan.FromMinutes(Convert.ToInt32(dt.Rows[index]["AUTOSHIFT_LOW"].ToString().Trim()));

                    //EmpGpPlc.sAUTOSHIFT_UP = Convert.ToInt32(dt.Rows[index]["AUTOSHIFT_UP"].ToString().Trim());
                    EmpGpPlc.tsAUTOSHIFT_UP = TimeSpan.FromMinutes(Convert.ToInt32(dt.Rows[index]["AUTOSHIFT_UP"].ToString().Trim()));

                    EmpGpPlc.sISPRESENTONWOPRESENT = dt.Rows[index]["ISPRESENTONWOPRESENT"].ToString().Trim();
                    EmpGpPlc.sISPRESENTONHLDPRESENT = dt.Rows[index]["ISPRESENTONHLDPRESENT"].ToString().Trim();
                    EmpGpPlc.sNightShiftFourPunch = dt.Rows[index]["NightShiftFourPunch"].ToString().Trim();
                    EmpGpPlc.sISAUTOABSENT = dt.Rows[index]["ISAUTOABSENT"].ToString().Trim();
                    EmpGpPlc.sISAWA = dt.Rows[index]["ISAWA"].ToString().Trim();
                    EmpGpPlc.sISWA = dt.Rows[index]["ISWA"].ToString().Trim();
                    EmpGpPlc.sISAW = dt.Rows[index]["ISAW"].ToString().Trim();
                    EmpGpPlc.sISPREWO = dt.Rows[index]["ISPREWO"].ToString().Trim();
                    EmpGpPlc.sISOTOUTMINUSSHIFTENDTIME = dt.Rows[index]["ISOTOUTMINUSSHIFTENDTIME"].ToString().Trim();
                    EmpGpPlc.sISOTWRKGHRSMINUSSHIFTHRS = dt.Rows[index]["ISOTWRKGHRSMINUSSHIFTHRS"].ToString().Trim();
                    EmpGpPlc.sISOTEARLYCOMEPLUSLATEDEP = dt.Rows[index]["ISOTEARLYCOMEPLUSLATEDEP"].ToString().Trim();

                    //EmpGpPlc.sDEDUCTHOLIDAYOT = Convert.ToInt32(dt.Rows[index]["DEDUCTHOLIDAYOT"].ToString().Trim());
                    EmpGpPlc.tsDEDUCTHOLIDAYOT = TimeSpan.FromMinutes(Convert.ToInt32(dt.Rows[index]["DEDUCTHOLIDAYOT"].ToString().Trim()));

                    //EmpGpPlc.sDEDUCTWOOT = Convert.ToInt32(dt.Rows[index]["DEDUCTWOOT"].ToString().Trim());
                    EmpGpPlc.tsDEDUCTWOOT = TimeSpan.FromMinutes(Convert.ToInt32(dt.Rows[index]["DEDUCTWOOT"].ToString().Trim()));

                    EmpGpPlc.sISOTEARLYCOMING = dt.Rows[index]["ISOTEARLYCOMING"].ToString().Trim();
                    EmpGpPlc.sOTMinus = dt.Rows[index]["OTMinus"].ToString().Trim();
                    EmpGpPlc.sOTROUND = dt.Rows[index]["OTROUND"].ToString().Trim();

                    //EmpGpPlc.sOTEARLYDUR = Convert.ToInt32(dt.Rows[index]["OTEARLYDUR"].ToString().Trim());
                    EmpGpPlc.tsOTEARLYDUR = TimeSpan.FromMinutes(Convert.ToInt32(dt.Rows[index]["OTEARLYDUR"].ToString().Trim()));

                    //EmpGpPlc.sOTLATECOMINGDUR = Convert.ToInt32(dt.Rows[index]["OTLATECOMINGDUR"].ToString().Trim());
                    EmpGpPlc.tsOTLATECOMINGDUR = TimeSpan.FromMinutes(Convert.ToInt32(dt.Rows[index]["OTLATECOMINGDUR"].ToString().Trim()));

                    //EmpGpPlc.sOTRESTRICTENDDUR = Convert.ToInt32(dt.Rows[index]["OTRESTRICTENDDUR"].ToString().Trim());
                    EmpGpPlc.tsOTRESTRICTENDDUR = TimeSpan.FromMinutes(Convert.ToInt32(dt.Rows[index]["OTRESTRICTENDDUR"].ToString().Trim()));

                    //EmpGpPlc.sDUPLICATECHECKMIN = Convert.ToInt32(dt.Rows[index]["DUPLICATECHECKMIN"].ToString().Trim());
                    EmpGpPlc.tsDUPLICATECHECKMIN = TimeSpan.FromMinutes(Convert.ToInt32(dt.Rows[index]["DUPLICATECHECKMIN"].ToString().Trim()));

                    //EmpGpPlc.sPREWO = Convert.ToInt32(dt.Rows[index]["PREWO"].ToString().Trim());
                    EmpGpPlc.tsPREWO = TimeSpan.FromMinutes(Convert.ToInt32(dt.Rows[index]["PREWO"].ToString().Trim()));

                    EmpGpPlc.sCOMPANYCODE = dt.Rows[index]["CompanyCode"].ToString().Trim();
                }
            }
            return EmpGpPlc;
        }

        public int EmployeeGroupPolicyUpdate(EmployeeGroupPolicy EMPGP)
        {
            string inonly = "N";
            string isPunchAll = "N";
            string Twopunch = "N";
            string IsOutWork = "N";

            DateTime Endtime_IN1 = System.DateTime.MinValue;
            DateTime EndTime_RTCEmp1 = System.DateTime.MinValue;

            EMPGP.sGroupID = (String.IsNullOrEmpty(EMPGP.sGroupID) ? "" : EMPGP.sGroupID);
            EMPGP.sGroupName = (String.IsNullOrEmpty(EMPGP.sGroupName) ? "" : EMPGP.sGroupName);
            EMPGP.sSHIFT = (String.IsNullOrEmpty(EMPGP.sSHIFT) ? "" : EMPGP.sSHIFT);
            EMPGP.sSHIFTTYPE = (String.IsNullOrEmpty(EMPGP.sSHIFTTYPE) ? "" : EMPGP.sSHIFTTYPE);
            EMPGP.sSHIFTPATTERN = (String.IsNullOrEmpty(EMPGP.sSHIFTPATTERN) ? "" : EMPGP.sSHIFTPATTERN);
            //EMPGP.sSHIFTREMAINDAYS = (String.IsNullOrEmpty(EMPGP.sSHIFTREMAINDAYS) ? "" : EMPGP.sSHIFTREMAINDAYS);
            EMPGP.sLASTSHIFTPERFORMED = (String.IsNullOrEmpty(EMPGP.sLASTSHIFTPERFORMED) ? "" : EMPGP.sLASTSHIFTPERFORMED);
            EMPGP.sINONLY = (String.IsNullOrEmpty(EMPGP.sINONLY) ? "" : EMPGP.sINONLY);
            EMPGP.sISPUNCHALL = (String.IsNullOrEmpty(EMPGP.sISPUNCHALL) ? "" : EMPGP.sISPUNCHALL);
            EMPGP.sISTIMELOSSALLOWED = (String.IsNullOrEmpty(EMPGP.sISTIMELOSSALLOWED) ? "" : EMPGP.sISTIMELOSSALLOWED);
            EMPGP.sALTERNATE_OFF_DAYS = (String.IsNullOrEmpty(EMPGP.sALTERNATE_OFF_DAYS) ? "" : EMPGP.sALTERNATE_OFF_DAYS);
            EMPGP.sALTERNATE_OFF_DAYS_1 = (String.IsNullOrEmpty(EMPGP.sALTERNATE_OFF_DAYS_1) ? "" : EMPGP.sALTERNATE_OFF_DAYS_1);
            EMPGP.sALTERNATE_OFF_DAYS_2 = (String.IsNullOrEmpty(EMPGP.sALTERNATE_OFF_DAYS_2) ? "" : EMPGP.sALTERNATE_OFF_DAYS_2);
            EMPGP.sALTERNATE_OFF_DAYS_3 = (String.IsNullOrEmpty(EMPGP.sALTERNATE_OFF_DAYS_3) ? "" : EMPGP.sALTERNATE_OFF_DAYS_3);
            EMPGP.sALTERNATE_OFF_DAYS_4 = (String.IsNullOrEmpty(EMPGP.sALTERNATE_OFF_DAYS_4) ? "" : EMPGP.sALTERNATE_OFF_DAYS_4);
            EMPGP.sALTERNATE_OFF_DAYS_5 = (String.IsNullOrEmpty(EMPGP.sALTERNATE_OFF_DAYS_5) ? "" : EMPGP.sALTERNATE_OFF_DAYS_5);
            EMPGP.sCDAYS = (String.IsNullOrEmpty(EMPGP.sCDAYS) ? "" : EMPGP.sCDAYS);
            EMPGP.sISROUNDTHECLOCKWORK = (String.IsNullOrEmpty(EMPGP.sISROUNDTHECLOCKWORK) ? "" : EMPGP.sISROUNDTHECLOCKWORK);
            EMPGP.sISOT = (String.IsNullOrEmpty(EMPGP.sISOT) ? "" : EMPGP.sISOT);
            EMPGP.sOTRATE = (String.IsNullOrEmpty(EMPGP.sOTRATE) ? "" : EMPGP.sOTRATE);
            EMPGP.sFIRSTOFFDAY = (String.IsNullOrEmpty(EMPGP.sFIRSTOFFDAY) ? "" : EMPGP.sFIRSTOFFDAY);
            EMPGP.sSECONDOFFTYPE = (String.IsNullOrEmpty(EMPGP.sSECONDOFFTYPE) ? "" : EMPGP.sSECONDOFFTYPE);
            EMPGP.sHALFDAYSHIFT = (String.IsNullOrEmpty(EMPGP.sHALFDAYSHIFT) ? "" : EMPGP.sHALFDAYSHIFT);
            EMPGP.sSECONDOFFDAY = (String.IsNullOrEmpty(EMPGP.sSECONDOFFDAY) ? "" : EMPGP.sSECONDOFFDAY);
            //EMPGP.sPERMISLATEARRIVAL = (String.IsNullOrEmpty(EMPGP.sPERMISLATEARRIVAL) ? "" : EMPGP.sPERMISLATEARRIVAL);
            //EMPGP.sPERMISEARLYDEPRT = (String.IsNullOrEmpty(EMPGP.sPERMISEARLYDEPRT) ? "" : EMPGP.sPERMISEARLYDEPRT);
            EMPGP.sISAUTOSHIFT = (String.IsNullOrEmpty(EMPGP.sISAUTOSHIFT) ? "" : EMPGP.sISAUTOSHIFT);
            EMPGP.sISOUTWORK = (String.IsNullOrEmpty(EMPGP.sISOUTWORK) ? "" : EMPGP.sISOUTWORK);
            EMPGP.sMAXDAYMIN = (String.IsNullOrEmpty(EMPGP.sMAXDAYMIN) ? "" : EMPGP.sMAXDAYMIN);
            EMPGP.sISOS = (String.IsNullOrEmpty(EMPGP.sISOS) ? "" : EMPGP.sISOS);
            EMPGP.sAUTH_SHIFTS = (String.IsNullOrEmpty(EMPGP.sAUTH_SHIFTS) ? "" : EMPGP.sAUTH_SHIFTS);
            //EMPGP.sTIME = (String.IsNullOrEmpty(EMPGP.sTIME) ? "" : EMPGP.sTIME);
            //EMPGP.sSHORT = (String.IsNullOrEmpty(EMPGP.sSHORT) ? "" : EMPGP.sSHORT);
            //EMPGP.sHALF = (String.IsNullOrEmpty(EMPGP.sHALF) ? "" : EMPGP.sHALF);
            EMPGP.sISHALFDAY = (String.IsNullOrEmpty(EMPGP.sISHALFDAY) ? "" : EMPGP.sISHALFDAY);
            EMPGP.sISSHORT = (String.IsNullOrEmpty(EMPGP.sISSHORT) ? "" : EMPGP.sISSHORT);
            EMPGP.sTWO = (String.IsNullOrEmpty(EMPGP.sTWO) ? "" : EMPGP.sTWO);
            EMPGP.sisReleaver = (String.IsNullOrEmpty(EMPGP.sisReleaver) ? "" : EMPGP.sisReleaver);
            EMPGP.sisWorker = (String.IsNullOrEmpty(EMPGP.sisWorker) ? "" : EMPGP.sisWorker);
            EMPGP.sisFlexi = (String.IsNullOrEmpty(EMPGP.sisFlexi) ? "" : EMPGP.sisFlexi);
            EMPGP.sSSN = (String.IsNullOrEmpty(EMPGP.sSSN) ? "" : EMPGP.sSSN);
            EMPGP.sMIS = (String.IsNullOrEmpty(EMPGP.sMIS) ? "" : EMPGP.sMIS);
            EMPGP.sIsCOF = (String.IsNullOrEmpty(EMPGP.sIsCOF) ? "" : EMPGP.sIsCOF);
            //EMPGP.sHLFAfter = (String.IsNullOrEmpty(EMPGP.sHLFAfter) ? "" : EMPGP.sHLFAfter);
            //EMPGP.sHLFBefore = (String.IsNullOrEmpty(EMPGP.sHLFBefore) ? "" : EMPGP.sHLFBefore);
            EMPGP.sResignedAfter = (String.IsNullOrEmpty(EMPGP.sResignedAfter) ? "" : EMPGP.sResignedAfter);
            EMPGP.sEnableAutoResign = (String.IsNullOrEmpty(EMPGP.sEnableAutoResign) ? "" : EMPGP.sEnableAutoResign);
            EMPGP.sS_END = (String.IsNullOrEmpty(EMPGP.sS_END) ? "" : EMPGP.sS_END);
            EMPGP.sS_OUT = (String.IsNullOrEmpty(EMPGP.sS_OUT) ? "" : EMPGP.sS_OUT);
            //EMPGP.sAUTOSHIFT_LOW = (String.IsNullOrEmpty(EMPGP.sAUTOSHIFT_LOW) ? "" : EMPGP.sAUTOSHIFT_LOW);
            //EMPGP.sAUTOSHIFT_UP = (String.IsNullOrEmpty(EMPGP.sAUTOSHIFT_UP) ? "" : EMPGP.sAUTOSHIFT_UP);
            EMPGP.sISPRESENTONWOPRESENT = (String.IsNullOrEmpty(EMPGP.sISPRESENTONWOPRESENT) ? "" : EMPGP.sISPRESENTONWOPRESENT);
            EMPGP.sISPRESENTONHLDPRESENT = (String.IsNullOrEmpty(EMPGP.sISPRESENTONHLDPRESENT) ? "" : EMPGP.sISPRESENTONHLDPRESENT);
            EMPGP.sNightShiftFourPunch = (String.IsNullOrEmpty(EMPGP.sNightShiftFourPunch) ? "" : EMPGP.sNightShiftFourPunch);
            EMPGP.sISAUTOABSENT = (String.IsNullOrEmpty(EMPGP.sISAUTOABSENT) ? "" : EMPGP.sISAUTOABSENT);
            EMPGP.sISAWA = (String.IsNullOrEmpty(EMPGP.sISAWA) ? "" : EMPGP.sISAWA);
            EMPGP.sISWA = (String.IsNullOrEmpty(EMPGP.sISWA) ? "" : EMPGP.sISWA);
            EMPGP.sISAW = (String.IsNullOrEmpty(EMPGP.sISAW) ? "" : EMPGP.sISAW);
            EMPGP.sISPREWO = (String.IsNullOrEmpty(EMPGP.sISPREWO) ? "" : EMPGP.sISPREWO);
            EMPGP.sISOTOUTMINUSSHIFTENDTIME = (String.IsNullOrEmpty(EMPGP.sISOTOUTMINUSSHIFTENDTIME) ? "" : EMPGP.sISOTOUTMINUSSHIFTENDTIME);
            EMPGP.sISOTWRKGHRSMINUSSHIFTHRS = (String.IsNullOrEmpty(EMPGP.sISOTWRKGHRSMINUSSHIFTHRS) ? "" : EMPGP.sISOTWRKGHRSMINUSSHIFTHRS);
            EMPGP.sISOTEARLYCOMEPLUSLATEDEP = (String.IsNullOrEmpty(EMPGP.sISOTEARLYCOMEPLUSLATEDEP) ? "" : EMPGP.sISOTEARLYCOMEPLUSLATEDEP);
            
            EMPGP.sDEDUCTHOLIDAYOT = (String.IsNullOrEmpty(EMPGP.sDEDUCTHOLIDAYOT) ? "0" : EMPGP.sDEDUCTHOLIDAYOT);
            EMPGP.sDEDUCTWOOT = (String.IsNullOrEmpty(EMPGP.sDEDUCTWOOT) ? "0" : EMPGP.sDEDUCTWOOT);
            
            EMPGP.sISOTEARLYCOMING = (String.IsNullOrEmpty(EMPGP.sISOTEARLYCOMING) ? "" : EMPGP.sISOTEARLYCOMING);
            EMPGP.sOTMinus = (String.IsNullOrEmpty(EMPGP.sOTMinus) ? "" : EMPGP.sOTMinus);
            EMPGP.sOTROUND = (String.IsNullOrEmpty(EMPGP.sOTROUND) ? "" : EMPGP.sOTROUND);
            
            EMPGP.sOTEARLYDUR = (String.IsNullOrEmpty(EMPGP.sOTEARLYDUR) ? "0" : EMPGP.sOTEARLYDUR);
            EMPGP.sOTLATECOMINGDUR = (String.IsNullOrEmpty(EMPGP.sOTLATECOMINGDUR) ? "0" : EMPGP.sOTLATECOMINGDUR);
            EMPGP.sOTRESTRICTENDDUR = (String.IsNullOrEmpty(EMPGP.sOTRESTRICTENDDUR) ? "0" : EMPGP.sOTRESTRICTENDDUR);
            EMPGP.sDUPLICATECHECKMIN = (String.IsNullOrEmpty(EMPGP.sDUPLICATECHECKMIN) ? "0" : EMPGP.sDUPLICATECHECKMIN);
            EMPGP.sPREWO = (String.IsNullOrEmpty(EMPGP.sPREWO) ? "0" : EMPGP.sPREWO);
            
            EMPGP.sCOMPANYCODE = (String.IsNullOrEmpty(EMPGP.sCOMPANYCODE) ? "" : EMPGP.sCOMPANYCODE);

            if (EMPGP.sS_END != "")
            {
                string strS_END = EMPGP.sS_END;
                string strS_OUT = EMPGP.sS_OUT;

                Endtime_IN1 = Convert.ToDateTime(strS_END); //(String.IsNullOrEmpty(EMPGP.sS_END) ? "" : EMPGP.sS_END);
                EndTime_RTCEmp1 = Convert.ToDateTime(strS_OUT); //(String.IsNullOrEmpty(EMPGP.sS_END) ? "" : EMPGP.sS_END);
            }

            if (EMPGP.sIsCOF.ToUpper().Trim() == "Y")
                EMPGP.sIsCOF = "Y";
            else
                EMPGP.sIsCOF = "N";

            if (EMPGP.sISPRESENTONWOPRESENT.ToUpper().Trim() == "Y")
                EMPGP.sISPRESENTONWOPRESENT = "Y";
            else
                EMPGP.sISPRESENTONWOPRESENT = "N";

            if (EMPGP.sISPRESENTONHLDPRESENT.ToUpper().Trim() == "Y")
                EMPGP.sISPRESENTONHLDPRESENT = "Y";
            else
                EMPGP.sISPRESENTONHLDPRESENT = "N";

            if (EMPGP.sNightShiftFourPunch.ToUpper().Trim() == "Y")
                EMPGP.sNightShiftFourPunch = "Y";
            else
                EMPGP.sNightShiftFourPunch = "N";

            if (EMPGP.sISAUTOABSENT.ToUpper().Trim() == "Y")
                EMPGP.sISAUTOABSENT = "Y";
            else
                EMPGP.sISAUTOABSENT = "N";

            if (EMPGP.sISAWA.ToUpper().Trim() == "Y")
                EMPGP.sISAWA = "Y";
            else
                EMPGP.sISAWA = "N";

            if (EMPGP.sISWA.ToUpper().Trim() == "Y")
                EMPGP.sISWA = "Y";
            else
                EMPGP.sISWA = "N";

            if (EMPGP.sISPREWO.ToUpper().Trim() == "Y")
                EMPGP.sISPREWO = "Y";
            else
                EMPGP.sISPREWO = "N";

            if (EMPGP.sISOTWRKGHRSMINUSSHIFTHRS.ToUpper().Trim() == "Y")
                EMPGP.sISOTWRKGHRSMINUSSHIFTHRS = "Y";
            else
                EMPGP.sISOTWRKGHRSMINUSSHIFTHRS = "N";

            if (EMPGP.sISOTEARLYCOMEPLUSLATEDEP.ToUpper().Trim() == "Y")
                EMPGP.sISOTEARLYCOMEPLUSLATEDEP = "Y";
            else
                EMPGP.sISOTEARLYCOMEPLUSLATEDEP = "N";


            //------------OT Option----------------
            if (EMPGP.sISOTOUTMINUSSHIFTENDTIME.ToString().Trim().ToUpper() == "Y")
            {
                EMPGP.sISOTOUTMINUSSHIFTENDTIME = "Y";
            }
            else if (EMPGP.sISOTWRKGHRSMINUSSHIFTHRS.ToString().Trim().ToUpper() == "Y")
            {
                EMPGP.sISOTWRKGHRSMINUSSHIFTHRS = "Y";
            }
            else if (EMPGP.sISOTEARLYCOMEPLUSLATEDEP.ToString().Trim().ToUpper() == "Y")
            {
                EMPGP.sISOTEARLYCOMEPLUSLATEDEP = "Y";
            }

            //------------Punches combinations----------------
            if (EMPGP.sISPUNCHALL.ToString().Trim() == "NoPunch")
            {
                inonly = "N";
                isPunchAll = "N";
                Twopunch = "N";
                IsOutWork = "N";
            }
            else if (EMPGP.sISPUNCHALL.ToString().Trim() == "SinglePunchOnly")
            {
                if (EMPGP.sISPUNCHALL.ToString().Trim() == "O")
                    inonly = "O";
                else
                    inonly = "Y";

                isPunchAll = "Y";
                Twopunch = "N";
                IsOutWork = "N";
            }
            else if (EMPGP.sISPUNCHALL.ToString().Trim() == "TwoPunches")
            {
                inonly = "N";
                isPunchAll = "Y";
                Twopunch = "Y";
                IsOutWork = "N";
            }
            else if (EMPGP.sISPUNCHALL.ToString().Trim() == "FourPunches")
            {
                inonly = "N";
                isPunchAll = "Y";
                Twopunch = "N";
                IsOutWork = "N";
            }
            else if (EMPGP.sISPUNCHALL.ToString().Trim() == "MultiplePunch")
            {
                inonly = "N";
                isPunchAll = "Y";
                Twopunch = "N";
                IsOutWork = "Y";
            }

            //------------Second Week Days----------------           
            string days = "";
            if (EMPGP.sALTERNATE_OFF_DAYS_1.ToString().Trim() == "" || EMPGP.sALTERNATE_OFF_DAYS_2.ToString().Trim() == "" || EMPGP.sALTERNATE_OFF_DAYS_3.ToString().Trim() == "" || EMPGP.sALTERNATE_OFF_DAYS_4.ToString().Trim() == "" || EMPGP.sALTERNATE_OFF_DAYS_5.ToString().Trim() == "")
            {
                days = "";
            }
            if (EMPGP.sALTERNATE_OFF_DAYS_1.ToString().ToUpper().Trim() == "ON")
            {
                days += 1;
            }
            if (EMPGP.sALTERNATE_OFF_DAYS_2.ToString().ToUpper().Trim() == "ON")
            {
                days += 2;
            }
            if (EMPGP.sALTERNATE_OFF_DAYS_3.ToString().ToUpper().Trim() == "ON")
            {
                days += 3;
            }
            if (EMPGP.sALTERNATE_OFF_DAYS_4.ToString().ToUpper().Trim() == "ON")
            {
                days += 4;
            }
            if (EMPGP.sALTERNATE_OFF_DAYS_5.ToString().ToUpper().Trim() == "ON")
            {
                days += 5;
            }

            int res = DataOperation.InsUpdDel("tblEmployeeGroupPolicy_Upd",
                new SqlParameter("GroupID", EMPGP.sGroupID),
                new SqlParameter("GroupName", EMPGP.sGroupName),
                new SqlParameter("SHIFT", EMPGP.sSHIFT),
                new SqlParameter("SHIFTTYPE", EMPGP.sSHIFTTYPE),
                new SqlParameter("SHIFTPATTERN", EMPGP.sSHIFTPATTERN),
                new SqlParameter("SHIFTREMAINDAYS", EMPGP.sSHIFTREMAINDAYS),
                //new SqlParameter("LASTSHIFTPERFORMED", EMPGP.sLASTSHIFTPERFORMED),
                new SqlParameter("INONLY", inonly),
                new SqlParameter("ISPUNCHALL", isPunchAll),
                new SqlParameter("ISTIMELOSSALLOWED", IsOutWork),
                new SqlParameter("ALTERNATE_OFF_DAYS", EMPGP.sALTERNATE_OFF_DAYS),
                new SqlParameter("CDAYS", EMPGP.sCDAYS),
                new SqlParameter("ISROUNDTHECLOCKWORK", EMPGP.sISROUNDTHECLOCKWORK),
                new SqlParameter("ISOT", EMPGP.sISOT),
                new SqlParameter("OTRATE", EMPGP.sOTRATE),
                new SqlParameter("FIRSTOFFDAY", EMPGP.sFIRSTOFFDAY),
                new SqlParameter("SECONDOFFTYPE", EMPGP.sSECONDOFFTYPE),
                new SqlParameter("HALFDAYSHIFT", EMPGP.sHALFDAYSHIFT),
                new SqlParameter("SECONDOFFDAY", EMPGP.sSECONDOFFDAY),
                //new SqlParameter("PERMISLATEARRIVAL", EMPGP.sPERMISLATEARRIVAL),
                //new SqlParameter("PERMISEARLYDEPRT", EMPGP.sPERMISEARLYDEPRT),
                new SqlParameter("PERMISLATEARRIVAL", Convert.ToInt32(EMPGP.sPERMISLATEARRIVAL)),
                new SqlParameter("PERMISEARLYDEPRT", Convert.ToInt32(EMPGP.sPERMISEARLYDEPRT)),
                new SqlParameter("ISAUTOSHIFT", EMPGP.sISAUTOSHIFT),
                new SqlParameter("ISOUTWORK", EMPGP.sISOUTWORK),
                new SqlParameter("MAXDAYMIN", EMPGP.sMAXDAYMIN),
                new SqlParameter("ISOS", EMPGP.sISOS),
                new SqlParameter("AUTH_SHIFTS", EMPGP.sAUTH_SHIFTS),
                //new SqlParameter("TIME", EMPGP.sTIME),
                //new SqlParameter("SHORT", EMPGP.sSHORT),
                //new SqlParameter("HALF", EMPGP.sHALF),
                new SqlParameter("TIME", Convert.ToInt32(EMPGP.sTIME)),
                new SqlParameter("SHORT", Convert.ToInt32(EMPGP.sSHORT)),
                new SqlParameter("HALF", Convert.ToInt32(EMPGP.sHALF)),
                new SqlParameter("ISHALFDAY", EMPGP.sISHALFDAY),
                new SqlParameter("ISSHORT", EMPGP.sISSHORT),
                new SqlParameter("TWO", Twopunch),
                new SqlParameter("isReleaver", EMPGP.sisReleaver),
                new SqlParameter("isWorker", EMPGP.sisWorker),
                new SqlParameter("isFlexi", EMPGP.sisFlexi),
                new SqlParameter("SSN", EMPGP.sSSN),
                new SqlParameter("MIS", EMPGP.sMIS),
                new SqlParameter("IsCOF", EMPGP.sIsCOF),
                new SqlParameter("HLFAfter", EMPGP.sHLFAfter),
                new SqlParameter("HLFBefore", EMPGP.sHLFBefore),
                new SqlParameter("ResignedAfter", EMPGP.sResignedAfter),
                new SqlParameter("EnableAutoResign", EMPGP.sEnableAutoResign),
                new SqlParameter("S_END", Endtime_IN1),
                new SqlParameter("S_OUT", EndTime_RTCEmp1),
                new SqlParameter("AUTOSHIFT_LOW", EMPGP.sAUTOSHIFT_LOW),
                new SqlParameter("AUTOSHIFT_UP", EMPGP.sAUTOSHIFT_UP),
                new SqlParameter("ISPRESENTONWOPRESENT", EMPGP.sISPRESENTONWOPRESENT),
                new SqlParameter("ISPRESENTONHLDPRESENT", EMPGP.sISPRESENTONHLDPRESENT),
                new SqlParameter("NightShiftFourPunch", EMPGP.sNightShiftFourPunch),
                new SqlParameter("ISAUTOABSENT", EMPGP.sISAUTOABSENT),
                new SqlParameter("ISAWA", EMPGP.sISAWA),
                new SqlParameter("ISWA", EMPGP.sISWA),
                new SqlParameter("ISAW", EMPGP.sISAW),
                new SqlParameter("ISPREWO", EMPGP.sISPREWO),
                new SqlParameter("ISOTOUTMINUSSHIFTENDTIME", EMPGP.sISOTOUTMINUSSHIFTENDTIME),
                new SqlParameter("ISOTWRKGHRSMINUSSHIFTHRS", EMPGP.sISOTWRKGHRSMINUSSHIFTHRS),
                new SqlParameter("ISOTEARLYCOMEPLUSLATEDEP", EMPGP.sISOTEARLYCOMEPLUSLATEDEP),
                new SqlParameter("DEDUCTHOLIDAYOT", EMPGP.sDEDUCTHOLIDAYOT),
                new SqlParameter("DEDUCTWOOT", EMPGP.sDEDUCTWOOT),
                new SqlParameter("ISOTEARLYCOMING", EMPGP.sISOTEARLYCOMING),
                new SqlParameter("OTMinus", EMPGP.sOTMinus),
                new SqlParameter("OTROUND", EMPGP.sOTROUND),
                new SqlParameter("OTEARLYDUR", EMPGP.sOTEARLYDUR),
                new SqlParameter("OTLATECOMINGDUR", EMPGP.sOTLATECOMINGDUR),
                new SqlParameter("OTRESTRICTENDDUR", EMPGP.sOTRESTRICTENDDUR),
                new SqlParameter("DUPLICATECHECKMIN", EMPGP.sDUPLICATECHECKMIN),
                new SqlParameter("PREWO", EMPGP.sPREWO),
                new SqlParameter("CompanyCode", EMPGP.sCOMPANYCODE));
            if (res == 1)
            {
                int resEmpShiftMat = DataOperation.InsUpdDel("UpdateEmployeeShiftMaster", new SqlParameter("GroupID", EMPGP.sGroupID)
                         //new SqlParameter("SHIFT", EMPGP.sSHIFT),
                         //new SqlParameter("SHIFTTYPE", EMPGP.sSHIFTTYPE),
                         //new SqlParameter("SHIFTPATTERN", EMPGP.sSHIFTPATTERN),
                         //new SqlParameter("SHIFTREMAINDAYS", EMPGP.sSHIFTREMAINDAYS),
                         //new SqlParameter("INONLY", EMPGP.sINONLY),
                         //new SqlParameter("ISPUNCHALL", EMPGP.sISPUNCHALL),
                         //new SqlParameter("ISTIMELOSSALLOWED", EMPGP.sISTIMELOSSALLOWED),
                         //new SqlParameter("ALTERNATE_OFF_DAYS", EMPGP.sALTERNATE_OFF_DAYS),
                         //new SqlParameter("CDAYS", EMPGP.sCDAYS),
                         //new SqlParameter("ISROUNDTHECLOCKWORK", EMPGP.sISROUNDTHECLOCKWORK),
                         //new SqlParameter("ISOT", EMPGP.sISOT),
                         //new SqlParameter("OTRATE", EMPGP.sOTRATE),
                         //new SqlParameter("FIRSTOFFDAY", EMPGP.sFIRSTOFFDAY),
                         //new SqlParameter("SECONDOFFTYPE", EMPGP.sSECONDOFFTYPE),
                         //new SqlParameter("HALFDAYSHIFT", EMPGP.sHALFDAYSHIFT),
                         //new SqlParameter("SECONDOFFDAY", EMPGP.sSECONDOFFDAY),
                         //new SqlParameter("PERMISLATEARRIVAL", EMPGP.sPERMISLATEARRIVAL),
                         //new SqlParameter("PERMISEARLYDEPRT", EMPGP.sPERMISEARLYDEPRT),
                         //new SqlParameter("ISAUTOSHIFT", EMPGP.sISAUTOSHIFT),
                         //new SqlParameter("IsOutWork", EMPGP.sISOUTWORK),
                         //new SqlParameter("MAXDAYMIN", EMPGP.sMAXDAYMIN),
                         //new SqlParameter("ISOS", EMPGP.sISOS),
                         //new SqlParameter("AUTH_SHIFTS", EMPGP.sAUTH_SHIFTS),
                         //new SqlParameter("TIME", EMPGP.sTIME),
                         //new SqlParameter("Short", EMPGP.sSHORT),
                         //new SqlParameter("Half", EMPGP.sHALF),
                         //new SqlParameter("ISHALFDAY", EMPGP.sISHALFDAY),
                         //new SqlParameter("ISSHORT", EMPGP.sISSHORT),
                         //new SqlParameter("TWO", EMPGP.sTWO),
                         ////new SqlParameter("isWorker", EMPGP.sisWorker),
                         ////new SqlParameter("isFlexi", EMPGP.sisFlexi),
                         //new SqlParameter("MIS", EMPGP.sMIS),
                         //new SqlParameter("IsCOF", EMPGP.sIsCOF),
                         //new SqlParameter("HLFAfter", EMPGP.sHLFAfter),
                         //new SqlParameter("HLFBefore", EMPGP.sHLFBefore),
                         //new SqlParameter("ResignedAfter", EMPGP.sResignedAfter),
                         //new SqlParameter("EnableAutoResign", EMPGP.sEnableAutoResign),
                         //new SqlParameter("S_END", EMPGP.sS_END),
                         //new SqlParameter("S_OUT", EMPGP.sS_OUT),
                         //new SqlParameter("AUTOSHIFT_LOW", EMPGP.sAUTOSHIFT_LOW),
                         //new SqlParameter("AUTOSHIFT_UP", EMPGP.sAUTOSHIFT_UP),
                         //new SqlParameter("ISPRESENTONWOPRESENT", EMPGP.sISPRESENTONWOPRESENT),
                         //new SqlParameter("ISPRESENTONHLDPRESENT", EMPGP.sISPRESENTONHLDPRESENT),
                         //new SqlParameter("NightShiftFourPunch", EMPGP.sNightShiftFourPunch),
                         //new SqlParameter("ISAUTOABSENT", EMPGP.sISAUTOABSENT),
                         ////new SqlParameter("ISAWA", EMPGP.sISAWA),
                         //new SqlParameter("ISWA", EMPGP.sISWA),
                         //new SqlParameter("ISAW", EMPGP.sISAW),
                         //new SqlParameter("ISPREWO", EMPGP.sISPREWO),
                         //new SqlParameter("ISOTOUTMINUSSHIFTENDTIME", EMPGP.sISOTOUTMINUSSHIFTENDTIME),
                         //new SqlParameter("ISOTWRKGHRSMINUSSHIFTHRS", EMPGP.sISOTWRKGHRSMINUSSHIFTHRS),
                         //new SqlParameter("ISOTEARLYCOMEPLUSLATEDEP", EMPGP.sISOTEARLYCOMEPLUSLATEDEP),
                         //new SqlParameter("DEDUCTHOLIDAYOT", EMPGP.sDEDUCTHOLIDAYOT),
                         //new SqlParameter("DEDUCTWOOT", EMPGP.sDEDUCTWOOT),
                         //new SqlParameter("ISOTEARLYCOMING", EMPGP.sISOTEARLYCOMING),
                         //new SqlParameter("OTMinus", EMPGP.sOTMinus),
                         //new SqlParameter("OTROUND", EMPGP.sOTROUND),
                         //new SqlParameter("OTEARLYDUR", EMPGP.sOTEARLYDUR),
                         //new SqlParameter("OTLATECOMINGDUR", EMPGP.sOTLATECOMINGDUR),
                         //new SqlParameter("OTRESTRICTENDDUR", EMPGP.sOTRESTRICTENDDUR)
                         //new SqlParameter("DUPLICATECHECKMIN", EMPGP.sDUPLICATECHECKMIN),
                         //new SqlParameter("PREWO", EMPGP.sPREWO)
                         );
            }
            return res;
        }
    }
}