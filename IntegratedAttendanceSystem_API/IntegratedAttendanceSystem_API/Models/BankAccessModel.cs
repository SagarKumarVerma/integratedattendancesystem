﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;
using IntegratedAttendanceSystem_API.DataAccess;

namespace IntegratedAttendanceSystem_API.Models
{
    public class BankAccessModel
    {
        public int IsCheckIsNumberExpID()
        {
            int res = 0;
            DataTable dt = DataOperation.GetDataTableWithoutParameter("TblBankMaster_CheckIsNumberExpID");
            if (dt.Rows.Count > 0)
            {
                for (int index = 0; index < dt.Rows.Count; index++)
                {
                    res = Convert.ToInt32(dt.Rows[index]["cnt"].ToString());
                }
            }
            return res;
        }
        //To View all Bank details    
        public IEnumerable<BankModel> GetAllBank()
        {
            DateTime date = System.DateTime.Now;
            List<BankModel> lstBank = new List<BankModel>();
            DataTable dt = DataOperation.GetDataTableWithoutParameter("TblBankMaster_AllRec");
            if (dt.Rows.Count > 0)
            {
                for (int index = 0; index < dt.Rows.Count; index++)
                {
                    BankModel BankMdl = new BankModel();
                    BankMdl.BankCode = dt.Rows[index]["BankCode"].ToString();
                    BankMdl.BankName = dt.Rows[index]["BankName"].ToString();
                    BankMdl.IFSCCode = dt.Rows[index]["IFSCCode"].ToString();
                    BankMdl.Address = dt.Rows[index]["Address"].ToString();
                    BankMdl.ShortName = dt.Rows[index]["ShortName"].ToString();
                    BankMdl.CreatedDate = Convert.ToDateTime(dt.Rows[index]["CreatedDate"].ToString());
                    BankMdl.LastModifiedBy = dt.Rows[index]["LastModifiedBy"].ToString();
                    BankMdl.LastModifiedDate = Convert.ToDateTime(dt.Rows[index]["LastModifiedDate"].ToString());
                    lstBank.Add(BankMdl);
                }
            }
            return lstBank;
        }
        //To Add new Bank record    
        public int Add(BankModel BankMst)
        {
            BankMst.IFSCCode = (String.IsNullOrEmpty(BankMst.IFSCCode) ? "" : BankMst.IFSCCode);
            BankMst.Address = (String.IsNullOrEmpty(BankMst.Address) ? "" : BankMst.Address);
            BankMst.ShortName = (String.IsNullOrEmpty(BankMst.ShortName) ? "" : BankMst.ShortName);

            int res = DataOperation.InsUpdDel("TblBankMaster_Add",
                new SqlParameter("BankCode", BankMst.BankCode),
                new SqlParameter("BankName", BankMst.BankName),
                new SqlParameter("IFSCCode", BankMst.IFSCCode),
                new SqlParameter("Address", BankMst.Address),
                new SqlParameter("ShortName", BankMst.ShortName),
                new SqlParameter("LastModifiedBy", BankMst.LastModifiedBy.Trim()));
            if (res == 1)
            {
                BankModel oldobject = GetBankData(BankMst.BankCode);
                AuditLog.CreateAuditTrail(BankModel.BANKMASTER, BankModel.BANKCODE, BankMst.BankCode, AppConstant.CREATED, BankMst.LastModifiedBy, BankMst.LoginTerminalNameIP, oldobject, BankMst);
            }
            return res;
        }
        //To Update the records of a particluar Bank 
        public int Update(BankModel BankMst)
        {
            BankMst.IFSCCode = (String.IsNullOrEmpty(BankMst.IFSCCode) ? "" : BankMst.IFSCCode);
            BankMst.Address = (String.IsNullOrEmpty(BankMst.Address) ? "" : BankMst.Address);
            BankMst.ShortName = (String.IsNullOrEmpty(BankMst.ShortName) ? "" : BankMst.ShortName);

            BankModel objgetexpmast = GetBankData(BankMst.BankCode);
            int res = DataOperation.InsUpdDel("TblBankMaster_Upd",
               new SqlParameter("BankCode", BankMst.BankCode),
                new SqlParameter("BankName", BankMst.BankName),
                new SqlParameter("IFSCCode", BankMst.IFSCCode),
                new SqlParameter("Address", BankMst.Address),
                new SqlParameter("ShortName", BankMst.ShortName),
                new SqlParameter("LastModifiedBy", BankMst.LastModifiedBy.Trim()));
            if (res == 1)
            {
                BankCloneModel objAuditold = new BankCloneModel();
                BankCloneModel objAuditnew = new BankCloneModel();
                //code for clone copy create for old object
                objAuditold.BankCode = objgetexpmast.BankCode;
                objAuditold.BankName = objgetexpmast.BankName;
                objAuditold.IFSCCode = objgetexpmast.IFSCCode;
                objAuditold.Address = objgetexpmast.Address;
                objAuditold.ShortName = objgetexpmast.ShortName;
                //code for clone copy create for new  object
                objAuditnew.BankCode = BankMst.BankCode;
                objAuditnew.BankName = BankMst.BankName;
                objAuditnew.IFSCCode = BankMst.IFSCCode;
                objAuditnew.Address = BankMst.Address;
                objAuditnew.ShortName = BankMst.ShortName;

                //objAuditnew.LastModifiedBy = Convert.ToString(HttpContext.Current.Session["UserName"]);
                //objAuditnew.LastModifiedDate = DateTime.Now;
                AuditLog.CreateAuditTrail(BankModel.BANKMASTER, BankModel.BANKCODE, BankMst.BankCode, AppConstant.MODIFIED, BankMst.LastModifiedBy, BankMst.LoginTerminalNameIP, objAuditold, objAuditnew);
            }
            return res;
        }
        //Get the details of a particular Bank Data  
        public BankModel GetBankData(string BankCode)
        {

            BankModel BankMstMod = new BankModel();
            DataTable dt = DataOperation.GetDataTableWithParameter("TblBankMaster_GetBankData", new SqlParameter("BankCode", BankCode.Trim()));
            if (dt.Rows.Count > 0)
            {
                for (int index = 0; index < dt.Rows.Count; index++)
                {
                    BankMstMod.BankCode = dt.Rows[index]["BankCode"].ToString();
                    BankMstMod.BankName = dt.Rows[index]["BankName"].ToString();
                    BankMstMod.IFSCCode = dt.Rows[index]["IFSCCode"].ToString();
                    BankMstMod.Address = dt.Rows[index]["Address"].ToString();
                    BankMstMod.ShortName = dt.Rows[index]["ShortName"].ToString();
                    BankMstMod.CreatedDate = Convert.ToDateTime(dt.Rows[index]["CreatedDate"].ToString());
                    BankMstMod.LastModifiedBy = dt.Rows[index]["LastModifiedBy"].ToString();
                    BankMstMod.LastModifiedDate = Convert.ToDateTime(dt.Rows[index]["LastModifiedDate"].ToString());
                }
            }
            return BankMstMod;
        }
        //Get the details of a particular  Bank Code  
        public int GetBankCode(string Code)
        {
            int res = 0;
            DataTable dt = DataOperation.GetDataTableWithParameter("TblBankMaster_CheckIsCode", new SqlParameter("BankCode", Code.Trim()));
            if (dt.Rows.Count > 0)
            {
                for (int index = 0; index < dt.Rows.Count; index++)
                {
                    res = Convert.ToInt32(dt.Rows[index]["cnt"].ToString());
                }
            }
            return res;
        }
        //To Delete the record on a particular  Bank  
        public int Deleteold(string BankCode)
        {
            string oldCode = BankCode;
            BankModel oldobject = GetBankData(oldCode.Trim());
            BankCloneModel objAuditold = new BankCloneModel();
            BankCloneModel objAuditnew = new BankCloneModel();
            int res = DataOperation.InsUpdDel("TblBankMaster_Del", new SqlParameter("BankCode", BankCode.Trim()));
            return res;
        }
        public int Delete(BankModel CateMst)
        {
            int res = -1;
            BankCloneModel objAuditold = new BankCloneModel();
            BankCloneModel objAuditnew = new BankCloneModel();
            BankModel objgetexpmast = GetBankData(CateMst.BankCode.Trim());
            //code for clone copy create for old object
            objAuditold.BankCode = objgetexpmast.BankCode;
            objAuditold.BankName = objgetexpmast.BankName;
            objAuditold.IFSCCode = objgetexpmast.IFSCCode;
            objAuditold.Address = objgetexpmast.Address;
            objAuditold.ShortName = objgetexpmast.ShortName;
            
            //code for clone copy create for new  object
            objAuditnew.BankCode = "";
            objAuditnew.BankName = "";
            objAuditnew.IFSCCode = "";
            objAuditnew.Address = "";
            objAuditnew.ShortName = "";

            res = DataOperation.InsUpdDel("TblBankMaster_Del", new SqlParameter("BankCode", CateMst.BankCode.Trim()));
            if (res == 1)
            {
                AuditLog.CreateAuditTrail(BankModel.BANKMASTER, BankModel.BANKCODE, CateMst.BankCode.Trim(), AppConstant.DELETED, CateMst.LastModifiedBy, CateMst.LoginTerminalNameIP, objAuditold, objAuditnew);
            }
            // }
            return res;
        }
    }
}