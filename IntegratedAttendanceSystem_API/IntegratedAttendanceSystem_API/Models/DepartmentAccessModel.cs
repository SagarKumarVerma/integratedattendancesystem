﻿using IntegratedAttendanceSystem_API.DataAccess;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace IntegratedAttendanceSystem_API.Models
{
    public class DepartmentAccessModel
    {

        public List<DepartmentModel> GetAllDepartment()
        {
            List<DepartmentModel> lstDept = new List<DepartmentModel>();
            DataTable dt = DataOperation.GetDataTableWithoutParameter("TblDepartmentMaster_AllRec");
            if (dt.Rows.Count > 0)
            {

                for (int index = 0; index < dt.Rows.Count; index++)
                {
                    DepartmentModel DeptMast = new DepartmentModel();
                    DeptMast.DepartmentCode = dt.Rows[index]["DepartmentCode"].ToString();
                    DeptMast.DepartmentName = dt.Rows[index]["DepartmentName"].ToString();
                    DeptMast.IsActive = dt.Rows[index]["IsActive"].ToString().Trim();
                    //#
                    DeptMast.LastModifiedBy = dt.Rows[index]["LastModifiedBy"].ToString();
                    DeptMast.LastModifiedDate = Convert.ToDateTime(dt.Rows[index]["LastModifiedDate"].ToString());
                    //
                    lstDept.Add(DeptMast);
                }
            }
            return lstDept;
        }
        public int Add(DepartmentModel DeptMast)
        {
            int res = DataOperation.InsUpdDel("TblDepartmentMaster_Add",
                new SqlParameter("DepartmentCode", DeptMast.DepartmentCode),
                new SqlParameter("DepartmentName", DeptMast.DepartmentName),
                new SqlParameter("CompanyCode", "C01"),
                new SqlParameter("IsActive", DeptMast.IsActive),
                new SqlParameter("LastModifiedBy", DeptMast.LastModifiedBy.ToString()));
            if (res == 1)
            {
                DepartmentModel oldobject = GetDepartmentId(DeptMast.DepartmentCode);
                AuditLog.CreateAuditTrail(DepartmentModel.DEPARTMENTMASTER, DepartmentModel.DEPARTMENTCODE, DeptMast.DepartmentCode, AppConstant.CREATED, DeptMast.LastModifiedBy, DeptMast.LoginTerminalNameIP, oldobject, DeptMast);
            }
            return res;

        }
        public int Update(DepartmentModel DeptMast)
        {


            DepartmentModel objgetDeptMast = GetDepartmentId(DeptMast.DepartmentCode);
            ////int res = -1;
            //DataTable dt = DataOperation.GetDataTableWithParameter("SP_CheckDeptRefUse", new SqlParameter("DepartmentCode", DeptMast.DepartmentCode.Trim()));
            //if (Convert.ToInt32(dt.Rows[0]["Column1"]) > 0)
            //{
            //    res = 2;
            //}
            //else
            //{
            int res = DataOperation.InsUpdDel("TblDepartmentMaster_Upd",
                new SqlParameter("DepartmentCode", DeptMast.DepartmentCode),
                new SqlParameter("DepartmentName", DeptMast.DepartmentName),
                new SqlParameter("IsActive", DeptMast.IsActive),
                 new SqlParameter("LastModifiedBy", DeptMast.LastModifiedBy.ToString()));
                if (res == 1)
                {
                    DepartmentCloneModel objDeptMstCloOld = new DepartmentCloneModel();
                    DepartmentCloneModel objDeptMstCloNew = new DepartmentCloneModel();

                    //code for clone copy created for old obj
                    //#
                    if (objgetDeptMast.IsActive == "True")
                    {
                        objDeptMstCloOld.IsActive = "Y";
                    }
                    else
                    {
                        objDeptMstCloOld.IsActive = "N";
                    }
                    //#
                    objDeptMstCloOld.DepartmentCode = objgetDeptMast.DepartmentCode;
                    objDeptMstCloOld.DepartmentName = objgetDeptMast.DepartmentName;
                    objDeptMstCloOld.LastModifiedBy = objgetDeptMast.LastModifiedBy;
                    objDeptMstCloOld.LastModifiedDate = objgetDeptMast.LastModifiedDate;
                    //code for clone copy create for new  object
                    objDeptMstCloNew.IsActive = DeptMast.IsActive;
                    objDeptMstCloNew.DepartmentCode = DeptMast.DepartmentCode;
                    objDeptMstCloNew.DepartmentName = DeptMast.DepartmentName;
                    objDeptMstCloNew.LastModifiedBy = DeptMast.LastModifiedBy; // Convert.ToString(HttpContext.Current.Session["UserName"]);
                    objDeptMstCloNew.LastModifiedDate = DateTime.Now;
                    AuditLog.CreateAuditTrail(DepartmentModel.DEPARTMENTMASTER, DepartmentModel.DEPARTMENTCODE, DeptMast.DepartmentCode, AppConstant.MODIFIED, DeptMast.LastModifiedBy, DeptMast.LoginTerminalNameIP, objDeptMstCloOld, objDeptMstCloNew);
                }

            //}
            return res;
        }
        public int Delete(DepartmentModel DeptMast)
        {
            int res = -1;
            string oldDepartmentCode = DeptMast.DepartmentCode; // id;
            DepartmentModel oldobject = GetDepartmentId(oldDepartmentCode.Trim());
            DepartmentCloneModel objAuditold = new DepartmentCloneModel();
            DepartmentCloneModel objAuditnew = new DepartmentCloneModel();
            objAuditold.DepartmentCode = oldobject.DepartmentCode;
            objAuditold.DepartmentName = oldobject.DepartmentName;
            objAuditold.IsActive = oldobject.IsActive;
            //code for clone copy create for new  object
            objAuditnew.DepartmentCode = "";
            objAuditnew.DepartmentName = "";
            objAuditnew.IsActive = "";
            //DataTable dt = DataOperation.GetDataTableWithParameter("SP_CheckLangRefUse", new SqlParameter("DepartmentCode", DeptMast.DepartmentCode.Trim()));
            //if (Convert.ToInt32(dt.Rows[0]["Column1"]) > 0)
            //{
            //    res = 2;
            //}
            //else
            //{
                res = DataOperation.InsUpdDel("TblDepartmentMaster_Del", new SqlParameter("DepartmentCode", DeptMast.DepartmentCode.Trim()));
                if (res == 1)
                {
                    AuditLog.CreateAuditTrail(DepartmentModel.DEPARTMENTMASTER, DepartmentModel.DEPARTMENTCODE, oldDepartmentCode.Trim(), AppConstant.DELETED, DeptMast.LastModifiedBy, DeptMast.LoginTerminalNameIP, objAuditold, objAuditnew);
                }
            //}
            return res;
        }
        // Get the details of a particular Department
        public List<DepartmentModel> ListAllDepartment()
        {
            List<DepartmentModel> lstDeptMast = new List<DepartmentModel>();
            DataTable dt = DataOperation.GetDataTableWithoutParameter("TblDepartmentMaster_AllRec");
            if (dt.Rows.Count > 0)
            {

                for (int index = 0; index < dt.Rows.Count; index++)
                {
                    DepartmentModel DeptMast = new DepartmentModel();
                    DeptMast.DepartmentCode = dt.Rows[index]["DepartmentCode"].ToString();
                    DeptMast.DepartmentName = dt.Rows[index]["DepartmentName"].ToString();
                    DeptMast.IsActive = dt.Rows[index]["IsActive"].ToString();
                    DeptMast.LastModifiedBy = dt.Rows[index]["LastModifiedBy"].ToString();
                    DeptMast.LastModifiedDate = Convert.ToDateTime(dt.Rows[index]["LastModifiedDate"].ToString());
                    lstDeptMast.Add(DeptMast);
                }

            }
            return lstDeptMast;
        }
        public DepartmentModel GetDepartmentId(string Code)
        {

            DepartmentModel DeptMast = new DepartmentModel();
            DataTable dt = DataOperation.GetDataTableWithParameter("TblDepartmentMaster_GetDepartmentData", new SqlParameter("DepartmentCode", Code.Trim()));
            if (dt.Rows.Count > 0)
            {

                for (int index = 0; index < dt.Rows.Count; index++)
                {
                    DeptMast.DepartmentCode = dt.Rows[index]["DepartmentCode"].ToString();
                    DeptMast.DepartmentName = dt.Rows[index]["DepartmentName"].ToString();
                    if (dt.Rows[index]["IsActive"].ToString() == "Y")
                        DeptMast.IsActive = "True";
                    else
                        DeptMast.IsActive = "False";

                    //#
                    DeptMast.LastModifiedBy = dt.Rows[index]["LastModifiedBy"].ToString();
                    DeptMast.LastModifiedDate = Convert.ToDateTime(dt.Rows[index]["LastModifiedDate"].ToString());
                    //
                }
            }
            return DeptMast;
        }
        //Get the details of a particular  Department Code  
        public int GetDepartmentCode(string Code)
        {
            int res = 0;
            DataTable dt = DataOperation.GetDataTableWithParameter("TblDepartmentMaster_CheckIsCode", new SqlParameter("DepartmentCode", Code.Trim()));
            if (dt.Rows.Count > 0)
            {
                for (int index = 0; index < dt.Rows.Count; index++)
                {
                    res = Convert.ToInt32(dt.Rows[index]["cnt"].ToString());
                }
            }
            return res;
        }
    }
}