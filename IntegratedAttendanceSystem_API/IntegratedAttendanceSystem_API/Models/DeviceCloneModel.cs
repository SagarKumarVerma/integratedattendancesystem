﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IntegratedAttendanceSystem_API.Models
{
    public class DeviceCloneModel
    {
        public string IsActive { get; set; }
        public string DeviceCode { get; set; }
        public string DeviceName { get; set; }
        //public string DeviceHODID { get; set; }
        //public string HODEmailID { get; set; }
        public DateTime CreatedDate { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime LastModifiedDate { get; set; }
        //#
        public string LoginTerminalNameIP { get; set; }
    }
}