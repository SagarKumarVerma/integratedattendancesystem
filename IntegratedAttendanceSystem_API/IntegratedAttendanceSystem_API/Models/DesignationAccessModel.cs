﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;
using IntegratedAttendanceSystem_API.DataAccess;

namespace IntegratedAttendanceSystem_API.Models
{
    public class DesignationAccessModel
    {
        public int IsCheckIsNumberExpID()
        {
            int res = 0;
            DataTable dt = DataOperation.GetDataTableWithoutParameter("TblDesignationMaster_CheckIsNumberExpID");
            if (dt.Rows.Count > 0)
            {
                for (int index = 0; index < dt.Rows.Count; index++)
                {
                    res = Convert.ToInt32(dt.Rows[index]["cnt"].ToString());
                }
            }
            return res;
        }
        //To View all Designation details    
        public IEnumerable<DesignationModel> GetAllDesignation()
        {
            DateTime date = System.DateTime.Now;
            List<DesignationModel> lstDesignation = new List<DesignationModel>();
            DataTable dt = DataOperation.GetDataTableWithoutParameter("tblDesignation_AllRec");
            if (dt.Rows.Count > 0)
            {
                for (int index = 0; index < dt.Rows.Count; index++)
                {
                    DesignationModel DesignationMdl = new DesignationModel();
                    DesignationMdl.DesignationCode = dt.Rows[index]["DesignationCode"].ToString().Trim();
                    DesignationMdl.DesignationName = dt.Rows[index]["DesignationName"].ToString().Trim();
                    DesignationMdl.LastModifiedBy = dt.Rows[index]["LastModifiedBy"].ToString().Trim();
                    DesignationMdl.LastModifiedDate = dt.Rows[index]["LastModifiedDate"].ToString().Trim(); // Convert.ToDateTime(dt.Rows[index]["LastModifiedDate"].ToString());
                    lstDesignation.Add(DesignationMdl);
                }
            }
            return lstDesignation;
        }
        //To Add new Designation record    
        public int Add(DesignationModel DesignationMst)
        {
            //DesignationMst.DesignationCity = (String.IsNullOrEmpty(DesignationMst.DesignationCity) ? "" : DesignationMst.DesignationCity);
            //DesignationMst.DesignationState = (String.IsNullOrEmpty(DesignationMst.DesignationState) ? "" : DesignationMst.DesignationState);
            //DesignationMst.DesignationPostalCode = (String.IsNullOrEmpty(DesignationMst.DesignationPostalCode) ? "" : DesignationMst.DesignationPostalCode);
            //DesignationMst.DesignationCountry = (String.IsNullOrEmpty(DesignationMst.DesignationCountry) ? "" : DesignationMst.DesignationCountry);
            //DesignationMst.DesignationAddress1 = (String.IsNullOrEmpty(DesignationMst.DesignationAddress1) ? "" : DesignationMst.DesignationAddress1);
            //DesignationMst.DesignationAddress2 = (String.IsNullOrEmpty(DesignationMst.DesignationAddress2) ? "" : DesignationMst.DesignationAddress2);
            //DesignationMst.DesignationContactEmailID = (String.IsNullOrEmpty(DesignationMst.DesignationContactEmailID) ? "" : DesignationMst.DesignationContactEmailID);

            int res = DataOperation.InsUpdDel("TblDesignation_Add",
                new SqlParameter("DesignationCode", DesignationMst.DesignationCode),
                new SqlParameter("DesignationName", DesignationMst.DesignationName),              
                new SqlParameter("LastModifiedBy", DesignationMst.LastModifiedBy.Trim()));
            if (res == 1)
            {
                DesignationModel oldobject = GetDesignationData(DesignationMst.DesignationCode);
                AuditLog.CreateAuditTrail(DesignationModel.DESIGNATIONMASTER, DesignationModel.DESIGNATIONCODE, DesignationMst.DesignationCode, AppConstant.CREATED, DesignationMst.LastModifiedBy, DesignationMst.LoginTerminalNameIP, oldobject, DesignationMst);
            }
            return res;
        }
        //To Update the records of a particluar Designation 
        public int Update(DesignationModel DesignationMst)
        {
            //DesignationMst.DesignationCity = (String.IsNullOrEmpty(DesignationMst.DesignationCity) ? "" : DesignationMst.DesignationCity);
            //DesignationMst.DesignationState = (String.IsNullOrEmpty(DesignationMst.DesignationState) ? "" : DesignationMst.DesignationState);
            //DesignationMst.DesignationPostalCode = (String.IsNullOrEmpty(DesignationMst.DesignationPostalCode) ? "" : DesignationMst.DesignationPostalCode);
            //DesignationMst.DesignationCountry = (String.IsNullOrEmpty(DesignationMst.DesignationCountry) ? "" : DesignationMst.DesignationCountry);
            //DesignationMst.DesignationAddress1 = (String.IsNullOrEmpty(DesignationMst.DesignationAddress1) ? "" : DesignationMst.DesignationAddress1);
            //DesignationMst.DesignationAddress2 = (String.IsNullOrEmpty(DesignationMst.DesignationAddress2) ? "" : DesignationMst.DesignationAddress2);
            //DesignationMst.DesignationContactEmailID = (String.IsNullOrEmpty(DesignationMst.DesignationContactEmailID) ? "" : DesignationMst.DesignationContactEmailID);

            DesignationModel objgetexpmast = GetDesignationData(DesignationMst.DesignationCode);
            int res = DataOperation.InsUpdDel("TblDesignation_Upd",
                new SqlParameter("DesignationCode", DesignationMst.DesignationCode),
                new SqlParameter("DesignationName", DesignationMst.DesignationName),
                new SqlParameter("LastModifiedBy", DesignationMst.LastModifiedBy.Trim()));
            if (res == 1)
            {
                DesignationCloneModel objAuditold = new DesignationCloneModel();
                DesignationCloneModel objAuditnew = new DesignationCloneModel();
                //code for clone copy create for old object
                objAuditold.DesignationCode = objgetexpmast.DesignationCode;
                objAuditold.DesignationName = objgetexpmast.DesignationName;
                //code for clone copy create for new  object
                objAuditnew.DesignationCode = DesignationMst.DesignationCode;
                objAuditnew.DesignationName = DesignationMst.DesignationName;

                //objAuditnew.LastModifiedBy = Convert.ToString(HttpContext.Current.Session["UserName"]);
                //objAuditnew.LastModifiedDate = DateTime.Now;
                AuditLog.CreateAuditTrail(DesignationModel.DESIGNATIONMASTER, DesignationModel.DESIGNATIONCODE, DesignationMst.DesignationCode, AppConstant.MODIFIED, DesignationMst.LastModifiedBy, DesignationMst.LoginTerminalNameIP, objAuditold, objAuditnew);
            }
            return res;
        }
        //Get the details of a particular Designation Data  
        public DesignationModel GetDesignationData(string DesignationCode)
        {

            DesignationModel DesignationMstMod = new DesignationModel();
            DataTable dt = DataOperation.GetDataTableWithParameter("TblDesignation_GetDesignationData", new SqlParameter("DesignationCode", DesignationCode.Trim()));
            if (dt.Rows.Count > 0)
            {
                for (int index = 0; index < dt.Rows.Count; index++)
                {
                    DesignationMstMod.DesignationCode = dt.Rows[index]["DesignationCode"].ToString();
                    DesignationMstMod.DesignationName = dt.Rows[index]["DesignationName"].ToString();
                    DesignationMstMod.LastModifiedBy = dt.Rows[index]["LastModifiedBy"].ToString();
                    DesignationMstMod.LastModifiedDate = dt.Rows[index]["LastModifiedDate"].ToString(); // Convert.ToDateTime(dt.Rows[index]["LastModifiedDate"].ToString());
                }
            }
            return DesignationMstMod;
        }
        //Get the details of a particular  Company Code  
        public int GetDesignationCode(string Code)
        {
            int res = 0;
            DataTable dt = DataOperation.GetDataTableWithParameter("tblDesignation_CheckIsCode", new SqlParameter("DesignationCode", Code.Trim()));
            if (dt.Rows.Count > 0)
            {
                for (int index = 0; index < dt.Rows.Count; index++)
                {
                    res = Convert.ToInt32(dt.Rows[index]["cnt"].ToString());
                }
            }
            return res;
        }
        //To Delete the record on a particular  Designation 
        public int Deleteold(string DesignationCode)
        {
            string oldCode = DesignationCode;
            DesignationModel oldobject = GetDesignationData(oldCode.Trim());
            DesignationCloneModel objAuditold = new DesignationCloneModel();
            DesignationCloneModel objAuditnew = new DesignationCloneModel();
            int res = DataOperation.InsUpdDel("TblDesignationMaster_Del", new SqlParameter("DesignationCode", DesignationCode.Trim()));
            return res;
        }
        public int Delete(DesignationModel DesignationMst)
        {
            int res = -1;
            DesignationCloneModel objAuditold = new DesignationCloneModel();
            DesignationCloneModel objAuditnew = new DesignationCloneModel();
            DesignationModel objgetexpmast = GetDesignationData(DesignationMst.DesignationCode.Trim());
            //code for clone copy create for old object
            objAuditold.DesignationCode = objgetexpmast.DesignationCode;
            objAuditold.DesignationName = objgetexpmast.DesignationName;

            //code for clone copy create for new  object
            objAuditnew.DesignationCode = "";
            objAuditnew.DesignationName = "";

            res = DataOperation.InsUpdDel("TblDesignation_Del", new SqlParameter("DesignationCode", DesignationMst.DesignationCode.Trim()));
            if (res == 1)
            {
                AuditLog.CreateAuditTrail(DesignationModel.DESIGNATIONMASTER, DesignationModel.DESIGNATIONCODE, DesignationMst.DesignationCode.Trim(), AppConstant.DELETED, DesignationMst.LastModifiedBy, DesignationMst.LoginTerminalNameIP, objAuditold, objAuditnew);
            }
            // }
            return res;
        }

        public List<DesignationModel> ListAllDesignation()
        {
            DateTime date = System.DateTime.Now;
            List<DesignationModel> lstDesg = new List<DesignationModel>();
            DataTable dt = DataOperation.GetDataTableWithoutParameter("tblDesignation_AllRec");
            if (dt.Rows.Count > 0)
            {
                for (int index = 0; index < dt.Rows.Count; index++)
                {
                    DesignationModel DesignationMdl = new DesignationModel();
                    DesignationMdl.DesignationCode = dt.Rows[index]["DesignationCode"].ToString();
                    DesignationMdl.DesignationName = dt.Rows[index]["DesignationName"].ToString();
                    DesignationMdl.LastModifiedBy = dt.Rows[index]["LastModifiedBy"].ToString();
                    DesignationMdl.LastModifiedDate = dt.Rows[index]["LastModifiedDate"].ToString(); // Convert.ToDateTime(dt.Rows[index]["LastModifiedDate"].ToString());
                    lstDesg.Add(DesignationMdl);
                }

            }
            return lstDesg;
        }
    }
}