﻿using IntegratedAttendanceSystem_API.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IntegratedAttendanceSystem_API.Models
{
    public class DesignationMasterModel
    {
        public List<Designation_list> GetDesignationList()
        {
            List<Designation_list> desig = new List<Designation_list>();
            DataTable dt = DataOperation.GetDataTableWithoutParameter("TblDesignation_SelectAll");
            if (dt.Rows.Count > 0)
            {
                for (int index = 0; index < dt.Rows.Count; index++)
                {
                    desig.Add(new Designation_list
                    {
                        DesignationCode = Convert.ToString(dt.Rows[index]["DesignationCode"]).Trim(),
                        DesignationName = Convert.ToString(dt.Rows[index]["DesignationName"]).Trim(),
                    });
                }
            }
            return desig;
        }
    }
}