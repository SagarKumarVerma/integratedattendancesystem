﻿using IntegratedAttendanceSystem_API.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IntegratedAttendanceSystem_API.Models
{
    public class GroupMasteModel
    {
        public List<Group_list> GetGroupList()
        {
            List<Group_list> group = new List<Group_list>();
            DataTable dt = DataOperation.GetDataTableWithoutParameter("TblGroupMaster_SelectAll");
            if (dt.Rows.Count > 0)
            {
                for (int index = 0; index < dt.Rows.Count; index++)
                {
                    group.Add(new Group_list
                    {
                        GroupCode = Convert.ToString(dt.Rows[index]["GroupCode"]).Trim(),
                        GroupName = Convert.ToString(dt.Rows[index]["GroupName"]).Trim(),
                    });
                }
            }
            return group;
        }
    }
}