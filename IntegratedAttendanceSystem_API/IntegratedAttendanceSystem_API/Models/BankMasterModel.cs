﻿using IntegratedAttendanceSystem_API.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IntegratedAttendanceSystem_API.Models
{
    public class BankMasterModel
    {
        public List<Bank_list> GetBankList()
        {
            List<Bank_list> bnk = new List<Bank_list>();
            DataTable dt = DataOperation.GetDataTableWithoutParameter("TblBank_SelectAll");
            if (dt.Rows.Count > 0)
            {
                for (int index = 0; index < dt.Rows.Count; index++)
                {
                    bnk.Add(new Bank_list
                    {
                        BankCode = Convert.ToString(dt.Rows[index]["BankCode"]).Trim(),
                        BankName = Convert.ToString(dt.Rows[index]["BankName"]).Trim(),
                    });
                }
            }
            return bnk;
        }
    }
}