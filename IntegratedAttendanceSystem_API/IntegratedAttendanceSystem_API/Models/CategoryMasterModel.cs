﻿using IntegratedAttendanceSystem_API.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IntegratedAttendanceSystem_API.Models
{
    public class CategoryMasterModel
    {
        public List<CatMaster_list> GetCategoryList()
        {
            List<CatMaster_list> cat = new List<CatMaster_list>();
            DataTable dt = DataOperation.GetDataTableWithoutParameter("TblCategory_SelectAll");
            if (dt.Rows.Count > 0)
            {
                for (int index = 0; index < dt.Rows.Count; index++)
                {
                    cat.Add(new CatMaster_list
                    {
                        CategoryCode = Convert.ToString(dt.Rows[index]["CategoryCode"]).Trim(),
                        CategoryName = Convert.ToString(dt.Rows[index]["CategoryName"]).Trim(),
                    });
                }
            }
            return cat;
        }
    }
}