﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;
using IntegratedAttendanceSystem_API.DataAccess;

namespace IntegratedAttendanceSystem_API.Models
{
    public class GradeAccessModel
    {
        public int IsCheckIsNumberExpID()
        {
            int res = 0;
            DataTable dt = DataOperation.GetDataTableWithoutParameter("TblGradeMaster_CheckIsNumberExpID");
            if (dt.Rows.Count > 0)
            {
                for (int index = 0; index < dt.Rows.Count; index++)
                {
                    res = Convert.ToInt32(dt.Rows[index]["cnt"].ToString());
                }
            }
            return res;
        }
        //To View all Grade details    
        public IEnumerable<GradeModel> GetAllGrade()
        {
            DateTime date = System.DateTime.Now;
            List<GradeModel> lstGrade = new List<GradeModel>();
            DataTable dt = DataOperation.GetDataTableWithoutParameter("TblGradeMaster_AllRec");
            if (dt.Rows.Count > 0)
            {
                for (int index = 0; index < dt.Rows.Count; index++)
                {
                    GradeModel GrdModel = new GradeModel();
                    GrdModel.IsActive = dt.Rows[index]["IsActive"].ToString().Trim();
                    GrdModel.GradeCode = dt.Rows[index]["GradeCode"].ToString().Trim();
                    GrdModel.GradeName = dt.Rows[index]["GradeName"].ToString().Trim();
                    GrdModel.CompanyCode = dt.Rows[index]["Companycode"].ToString().Trim();
                    GrdModel.CreatedDate = dt.Rows[index]["CreatedDate"].ToString().Trim(); // Convert.ToDateTime(dt.Rows[index]["CreatedDate"].ToString());
                    GrdModel.LastModifiedBy = dt.Rows[index]["LastModifiedBy"].ToString().Trim();
                    GrdModel.LastModifiedDate = dt.Rows[index]["LastModifiedDate"].ToString().Trim(); // Convert.ToDateTime(dt.Rows[index]["LastModifiedDate"].ToString());
                    lstGrade.Add(GrdModel);
                }
            }
            return lstGrade;
        }
        //To Add new Grade record    
        public int Add(GradeModel GrdMst)
        {
            int res = DataOperation.InsUpdDel("TblGradeMaster_Add",
                new SqlParameter("IsActive", GrdMst.IsActive),
                new SqlParameter("GradeCode", GrdMst.GradeCode.Trim()),
                new SqlParameter("GradeName", GrdMst.GradeName.Trim()),
                new SqlParameter("CompanyCode", GrdMst.CompanyCode.Trim()),
                new SqlParameter("LastModifiedBy", GrdMst.LastModifiedBy.Trim()));
            if (res == 1)
            {
                GradeModel oldobject = GetGradeData(GrdMst.GradeCode);
                AuditLog.CreateAuditTrail(GradeModel.GradeMASTER, GradeModel.GradeCODE, GrdMst.GradeCode, AppConstant.CREATED, GrdMst.LastModifiedBy, GrdMst.LoginTerminalNameIP, oldobject, GrdMst);
            }
            return res;
        }
        //To Update the records of a particluar Grade 
        public int Update(GradeModel GrdMst)
        {
            GradeModel objgetexpmast = GetGradeData(GrdMst.GradeCode);
            int res = DataOperation.InsUpdDel("TblGradeMaster_Upd",
                new SqlParameter("IsActive", GrdMst.IsActive),
                new SqlParameter("GradeCode", GrdMst.GradeCode.Trim()),
                new SqlParameter("GradeName", GrdMst.GradeName.Trim()),
                new SqlParameter("CompanyCode", GrdMst.CompanyCode.Trim()),
                new SqlParameter("LastModifiedBy", GrdMst.LastModifiedBy.Trim()));
            if (res == 1)
            {
                GradeCloneModel objAuditold = new GradeCloneModel();
                GradeCloneModel objAuditnew = new GradeCloneModel();
                //code for clone copy create for old object
                objAuditold.IsActive = objgetexpmast.IsActive;
                objAuditold.GradeCode = objgetexpmast.GradeCode;
                objAuditold.GradeName = objgetexpmast.GradeName;
                //code for clone copy create for new  object
                objAuditnew.IsActive = GrdMst.IsActive;
                objAuditnew.GradeCode = GrdMst.GradeCode;
                objAuditnew.GradeName = GrdMst.GradeName;
               
                //objAuditnew.LastModifiedBy = Convert.ToString(HttpContext.Current.Session["UserName"]);
                //objAuditnew.LastModifiedDate = DateTime.Now;
                AuditLog.CreateAuditTrail(GradeModel.GradeMASTER, GradeModel.GradeCODE, GrdMst.GradeCode, AppConstant.MODIFIED, GrdMst.LastModifiedBy, GrdMst.LoginTerminalNameIP, objAuditold, objAuditnew);
            }
            return res;
        }
        //Get the details of a particular Grade Data  
        public GradeModel GetGradeData(string GradeCode)
        {
            GradeModel GrdMstMod = new GradeModel();
            DataTable dt = DataOperation.GetDataTableWithParameter("TblGradeMaster_GetGradeData", new SqlParameter("GradeCode", GradeCode.Trim()));
            if (dt.Rows.Count > 0)
            {
                for (int index = 0; index < dt.Rows.Count; index++)
                {
                    GrdMstMod.IsActive = dt.Rows[index]["IsActive"].ToString();
                    GrdMstMod.GradeCode = dt.Rows[index]["GradeCode"].ToString().Trim();
                    GrdMstMod.GradeName = dt.Rows[index]["GradeName"].ToString().Trim();
                    GrdMstMod.CompanyCode = dt.Rows[index]["CompanyCode"].ToString().Trim();
                    GrdMstMod.CreatedDate = dt.Rows[index]["CreatedDate"].ToString().Trim(); // Convert.ToDateTime(dt.Rows[index]["CreatedDate"].ToString());
                    GrdMstMod.LastModifiedBy = dt.Rows[index]["LastModifiedBy"].ToString().Trim();
                    GrdMstMod.LastModifiedDate = dt.Rows[index]["LastModifiedDate"].ToString().Trim(); // Convert.ToDateTime(dt.Rows[index]["LastModifiedDate"].ToString());
                }
            }
            return GrdMstMod;
        }
        //To Delete the record on a particular  Grade  
        public int Deleteold(string GradeCode)
        {
            string oldCode = GradeCode;
            GradeModel oldobject = GetGradeData(oldCode.Trim());
            GradeCloneModel objAuditold = new GradeCloneModel();
            GradeCloneModel objAuditnew = new GradeCloneModel();
            int res = DataOperation.InsUpdDel("TblGradeMaster_Del", new SqlParameter("GradeCode", GradeCode.Trim()));
            return res;
        }
        public int Delete(GradeModel GrdMst)
        {
            int res = -1;
            GradeCloneModel objAuditold = new GradeCloneModel();
            GradeCloneModel objAuditnew = new GradeCloneModel();
            GradeModel objgetexpmast = GetGradeData(GrdMst.GradeCode.Trim());
            //code for clone copy create for old object
            objAuditold.IsActive = objgetexpmast.IsActive;
            objAuditold.GradeCode = objgetexpmast.GradeCode;
            objAuditold.GradeName = objgetexpmast.GradeName;
            
            //code for clone copy create for new  object
            objAuditnew.IsActive = "";
            objAuditnew.GradeCode = "";
            objAuditnew.GradeName = "";
           
            res = DataOperation.InsUpdDel("TblGradeMaster_Del", new SqlParameter("GradeCode", GrdMst.GradeCode.Trim()), new SqlParameter("CompanyCode", GrdMst.CompanyCode.Trim()));
            if (res == 1)
            {
                AuditLog.CreateAuditTrail(GradeModel.GradeMASTER, GradeModel.GradeCODE, GrdMst.GradeCode.Trim(), AppConstant.DELETED, GrdMst.LastModifiedBy, GrdMst.LoginTerminalNameIP, objAuditold, objAuditnew);
            }
            // }
            return res;
        }
        //Get the details of a particular  Grade Code  
        public int GetGradeCode(string Code)
        {

            int res = 0;
            DataTable dt = DataOperation.GetDataTableWithParameter("TblGradeMaster_CheckIsCode", new SqlParameter("GradeCode", Code.Trim()));
            if (dt.Rows.Count > 0)
            {
                for (int index = 0; index < dt.Rows.Count; index++)
                {
                    res = Convert.ToInt32(dt.Rows[index]["cnt"].ToString());
                }
            }
            return res;
        }
    }
}