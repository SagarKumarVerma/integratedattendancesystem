﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;
using IntegratedAttendanceSystem_API.DataAccess;

namespace IntegratedAttendanceSystem_API.Models
{
    public class LocationAccessModel
    {
        public int IsCheckIsNumberExpID()
        {
            int res = 0;
            DataTable dt = DataOperation.GetDataTableWithoutParameter("TblLocationMaster_CheckIsNumberExpID");
            if (dt.Rows.Count > 0)
            {
                for (int index = 0; index < dt.Rows.Count; index++)
                {
                    res = Convert.ToInt32(dt.Rows[index]["cnt"].ToString());
                }
            }
            return res;
        }
        //To View all Location details    
        public IEnumerable<LocationModel> GetAllLocation()
        {
            DateTime date = System.DateTime.Now;
            List<LocationModel> lstLocation = new List<LocationModel>();
            DataTable dt = DataOperation.GetDataTableWithoutParameter("TblLocationMaster_AllRec");
            if (dt.Rows.Count > 0)
            {
                for (int index = 0; index < dt.Rows.Count; index++)
                {
                    LocationModel LocationMdl = new LocationModel();
                    LocationMdl.LocationCode = dt.Rows[index]["LocationCode"].ToString();
                    LocationMdl.LocationName = dt.Rows[index]["LocationName"].ToString();
                    LocationMdl.LocationCity = dt.Rows[index]["City"].ToString();
                    LocationMdl.LocationState = dt.Rows[index]["State"].ToString();
                    LocationMdl.LocationPostalCode = dt.Rows[index]["PostalCode"].ToString();
                    LocationMdl.LocationCountry = dt.Rows[index]["Country"].ToString();
                    LocationMdl.LocationAddress1 = dt.Rows[index]["Address1"].ToString();
                    LocationMdl.LocationAddress2 = dt.Rows[index]["Address2"].ToString();
                    LocationMdl.LocationContactEmailID = dt.Rows[index]["ContactEmailID"].ToString();
                    LocationMdl.CreatedDate = Convert.ToDateTime(dt.Rows[index]["CreatedDate"].ToString());
                    LocationMdl.LastModifiedBy = dt.Rows[index]["LastModifiedBy"].ToString();
                    LocationMdl.LastModifiedDate = Convert.ToDateTime(dt.Rows[index]["LastModifiedDate"].ToString());
                    lstLocation.Add(LocationMdl);
                }
            }
            return lstLocation;
        }
        //To Add new Location record    
        public int Add(LocationModel LocationMst)
        {
            LocationMst.LocationCity = (String.IsNullOrEmpty(LocationMst.LocationCity) ? "" : LocationMst.LocationCity);
            LocationMst.LocationState = (String.IsNullOrEmpty(LocationMst.LocationState) ? "" : LocationMst.LocationState);
            LocationMst.LocationPostalCode = (String.IsNullOrEmpty(LocationMst.LocationPostalCode) ? "" : LocationMst.LocationPostalCode);
            LocationMst.LocationCountry = (String.IsNullOrEmpty(LocationMst.LocationCountry) ? "" : LocationMst.LocationCountry);
            LocationMst.LocationAddress1 = (String.IsNullOrEmpty(LocationMst.LocationAddress1) ? "" : LocationMst.LocationAddress1);
            LocationMst.LocationAddress2 = (String.IsNullOrEmpty(LocationMst.LocationAddress2) ? "" : LocationMst.LocationAddress2);
            LocationMst.LocationContactEmailID = (String.IsNullOrEmpty(LocationMst.LocationContactEmailID) ? "" : LocationMst.LocationContactEmailID);

            int res = DataOperation.InsUpdDel("TblLocationMaster_Add",
                new SqlParameter("LocationCode", LocationMst.LocationCode),
                new SqlParameter("LocationName", LocationMst.LocationName),
                new SqlParameter("City", LocationMst.LocationCity),
                new SqlParameter("State", LocationMst.LocationState),
                new SqlParameter("PostalCode", LocationMst.LocationPostalCode),
                new SqlParameter("Country", LocationMst.LocationCountry),
                new SqlParameter("Address1", LocationMst.LocationAddress1),
                new SqlParameter("Address2", LocationMst.LocationAddress2),
                new SqlParameter("ContactEmailID", LocationMst.LocationContactEmailID),
                new SqlParameter("LastModifiedBy", LocationMst.LastModifiedBy.Trim()));
            if (res == 1)
            {
                LocationModel oldobject = GetLocationData(LocationMst.LocationCode);
                AuditLog.CreateAuditTrail(LocationModel.LOCATIONMASTER, LocationModel.LOCATIONCODE, LocationMst.LocationCode, AppConstant.CREATED, LocationMst.LastModifiedBy, LocationMst.LoginTerminalNameIP, oldobject, LocationMst);
            }
            return res;
        }
        //To Update the records of a particluar Location 
        public int Update(LocationModel LocationMst)
        {
            LocationMst.LocationCity = (String.IsNullOrEmpty(LocationMst.LocationCity) ? "" : LocationMst.LocationCity);
            LocationMst.LocationState = (String.IsNullOrEmpty(LocationMst.LocationState) ? "" : LocationMst.LocationState);
            LocationMst.LocationPostalCode = (String.IsNullOrEmpty(LocationMst.LocationPostalCode) ? "" : LocationMst.LocationPostalCode);
            LocationMst.LocationCountry = (String.IsNullOrEmpty(LocationMst.LocationCountry) ? "" : LocationMst.LocationCountry);
            LocationMst.LocationAddress1 = (String.IsNullOrEmpty(LocationMst.LocationAddress1) ? "" : LocationMst.LocationAddress1);
            LocationMst.LocationAddress2 = (String.IsNullOrEmpty(LocationMst.LocationAddress2) ? "" : LocationMst.LocationAddress2);
            LocationMst.LocationContactEmailID = (String.IsNullOrEmpty(LocationMst.LocationContactEmailID) ? "" : LocationMst.LocationContactEmailID);

            LocationModel objgetexpmast = GetLocationData(LocationMst.LocationCode);
            int res = DataOperation.InsUpdDel("TblLocationMaster_Upd",
                new SqlParameter("LocationCode", LocationMst.LocationCode),
                new SqlParameter("LocationName", LocationMst.LocationName),
                new SqlParameter("City", LocationMst.LocationCity),
                new SqlParameter("State", LocationMst.LocationState),
                new SqlParameter("PostalCode", LocationMst.LocationPostalCode),
                new SqlParameter("Country", LocationMst.LocationCountry),
                new SqlParameter("Address1", LocationMst.LocationAddress1),
                new SqlParameter("Address2", LocationMst.LocationAddress2),
                new SqlParameter("ContactEmailID", LocationMst.LocationContactEmailID),
                new SqlParameter("LastModifiedBy", LocationMst.LastModifiedBy.Trim()));
            if (res == 1)
            {
                LocationCloneModel objAuditold = new LocationCloneModel();
                LocationCloneModel objAuditnew = new LocationCloneModel();
                //code for clone copy create for old object
                objAuditold.LocationCode = objgetexpmast.LocationCode;
                objAuditold.LocationName = objgetexpmast.LocationName;
                objAuditold.LocationCity = objgetexpmast.LocationCity;
                objAuditold.LocationState = objgetexpmast.LocationState;
                objAuditold.LocationPostalCode = objgetexpmast.LocationPostalCode;
                objAuditold.LocationCountry = objgetexpmast.LocationCountry;
                objAuditold.LocationAddress1 = objgetexpmast.LocationAddress1;
                objAuditold.LocationAddress2 = objgetexpmast.LocationAddress2;
                objAuditold.LocationContactEmailID = objgetexpmast.LocationContactEmailID;
                //code for clone copy create for new  object
                objAuditnew.LocationCode = LocationMst.LocationCode;
                objAuditnew.LocationName = LocationMst.LocationName;
                objAuditnew.LocationCity = LocationMst.LocationCity;
                objAuditnew.LocationState = LocationMst.LocationState;
                objAuditnew.LocationPostalCode = LocationMst.LocationPostalCode;
                objAuditnew.LocationCountry = LocationMst.LocationCountry;
                objAuditnew.LocationAddress1 = LocationMst.LocationAddress1;
                objAuditnew.LocationAddress2 = LocationMst.LocationAddress2;
                objAuditnew.LocationContactEmailID = LocationMst.LocationContactEmailID;

                //objAuditnew.LastModifiedBy = Convert.ToString(HttpContext.Current.Session["UserName"]);
                //objAuditnew.LastModifiedDate = DateTime.Now;
                AuditLog.CreateAuditTrail(LocationModel.LOCATIONMASTER, LocationModel.LOCATIONCODE, LocationMst.LocationCode, AppConstant.MODIFIED, LocationMst.LastModifiedBy, LocationMst.LoginTerminalNameIP, objAuditold, objAuditnew);
            }
            return res;
        }
        //Get the details of a particular Location Data  
        public LocationModel GetLocationData(string LocationCode)
        {

            LocationModel LocationMstMod = new LocationModel();
            DataTable dt = DataOperation.GetDataTableWithParameter("TblLocationMaster_GetLocationData", new SqlParameter("LocationCode", LocationCode.Trim()));
            if (dt.Rows.Count > 0)
            {
                for (int index = 0; index < dt.Rows.Count; index++)
                {
                    LocationMstMod.LocationCode = dt.Rows[index]["LocationCode"].ToString();
                    LocationMstMod.LocationName = dt.Rows[index]["LocationName"].ToString();
                    LocationMstMod.LocationCity = dt.Rows[index]["City"].ToString();
                    LocationMstMod.LocationState = dt.Rows[index]["State"].ToString();
                    LocationMstMod.LocationPostalCode = dt.Rows[index]["PostalCode"].ToString();
                    LocationMstMod.LocationCountry = dt.Rows[index]["Country"].ToString();
                    LocationMstMod.LocationAddress1 = dt.Rows[index]["Address1"].ToString();
                    LocationMstMod.LocationAddress2 = dt.Rows[index]["Address2"].ToString();
                    LocationMstMod.LocationContactEmailID = dt.Rows[index]["ContactEmailID"].ToString();
                    LocationMstMod.CreatedDate = Convert.ToDateTime(dt.Rows[index]["CreatedDate"].ToString());
                    LocationMstMod.LastModifiedBy = dt.Rows[index]["LastModifiedBy"].ToString();
                    LocationMstMod.LastModifiedDate = Convert.ToDateTime(dt.Rows[index]["LastModifiedDate"].ToString());
                }
            }
            return LocationMstMod;
        }
        //Get the details of a particular  Company Code  
        public int GetLocationCode(string Code)
        {
            int res = 0;
            DataTable dt = DataOperation.GetDataTableWithParameter("TblLocationMaster_CheckIsCode", new SqlParameter("LocationCode", Code.Trim()));
            if (dt.Rows.Count > 0)
            {
                for (int index = 0; index < dt.Rows.Count; index++)
                {
                    res = Convert.ToInt32(dt.Rows[index]["cnt"].ToString());
                }
            }
            return res;
        }
        //To Delete the record on a particular  Location 
        public int Deleteold(string LocationCode)
        {
            string oldCode = LocationCode;
            LocationModel oldobject = GetLocationData(oldCode.Trim());
            LocationCloneModel objAuditold = new LocationCloneModel();
            LocationCloneModel objAuditnew = new LocationCloneModel();
            int res = DataOperation.InsUpdDel("TblLocationMaster_Del", new SqlParameter("LocationCode", LocationCode.Trim()));
            return res;
        }
        public int Delete(LocationModel LocationMst)
        {
            int res = -1;
            LocationCloneModel objAuditold = new LocationCloneModel();
            LocationCloneModel objAuditnew = new LocationCloneModel();
            LocationModel objgetexpmast = GetLocationData(LocationMst.LocationCode.Trim());
            //code for clone copy create for old object
            objAuditold.LocationCode = objgetexpmast.LocationCode;
            objAuditold.LocationName = objgetexpmast.LocationName;
            objAuditold.LocationCity = objgetexpmast.LocationCity;
            objAuditold.LocationState = objgetexpmast.LocationState;
            objAuditold.LocationPostalCode = objgetexpmast.LocationPostalCode;
            objAuditold.LocationCountry = objgetexpmast.LocationCountry;
            objAuditold.LocationAddress1 = objgetexpmast.LocationAddress1;
            objAuditold.LocationAddress2 = objgetexpmast.LocationAddress2;
            objAuditold.LocationContactEmailID = objgetexpmast.LocationContactEmailID;

            //code for clone copy create for new  object
            objAuditnew.LocationCode = "";
            objAuditnew.LocationName = "";
            objAuditnew.LocationCity = "";
            objAuditnew.LocationState = "";
            objAuditnew.LocationPostalCode = "";
            objAuditnew.LocationCountry = "";
            objAuditnew.LocationAddress1 = "";
            objAuditnew.LocationAddress2 = "";
            objAuditnew.LocationContactEmailID = "";

            res = DataOperation.InsUpdDel("TblLocationMaster_Del", new SqlParameter("LocationCode", LocationMst.LocationCode.Trim()));
            if (res == 1)
            {
                AuditLog.CreateAuditTrail(LocationModel.LOCATIONMASTER, LocationModel.LOCATIONCODE, LocationMst.LocationCode.Trim(), AppConstant.DELETED, LocationMst.LastModifiedBy, LocationMst.LoginTerminalNameIP, objAuditold, objAuditnew);
            }
            // }
            return res;
        }


    }
}