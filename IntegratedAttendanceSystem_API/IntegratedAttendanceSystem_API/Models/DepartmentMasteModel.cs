﻿using IntegratedAttendanceSystem_API.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IntegratedAttendanceSystem_API.Models
{
    public class DepartmentMasteModel
    {
        public List<Department_list> GetDepartmentList()
        {
            List<Department_list> department = new List<Department_list>();
            DataTable dt = DataOperation.GetDataTableWithoutParameter("TblDepartmentMaster_SelectAll");
            if (dt.Rows.Count > 0)
            {
                for (int index = 0; index < dt.Rows.Count; index++)
                {
                    department.Add(new Department_list
                    {
                        DepartmentCode = Convert.ToString(dt.Rows[index]["DepartmentCode"]).Trim(),
                        DepartmentName = Convert.ToString(dt.Rows[index]["DepartmentName"]).Trim(),
                    });
                }
            }
            return department;
        }
    }
}