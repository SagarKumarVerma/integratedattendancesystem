﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;
using IntegratedAttendanceSystem_API.DataAccess;


namespace IntegratedAttendanceSystem_API.Models
{
    public class CompanyAccessModel
    {
        public int IsCheckIsNumberExpID()
        {
            int res = 0;
            DataTable dt = DataOperation.GetDataTableWithoutParameter("TblCompanyMaster_CheckIsNumberExpID");
            if (dt.Rows.Count > 0)
            {
                for (int index = 0; index < dt.Rows.Count; index++)
                {
                    res = Convert.ToInt32(dt.Rows[index]["cnt"].ToString());
                }
            }
            return res;
        }
        //To View all Company details    
        public IEnumerable<CompanyModel> GetAllCompany()
        {
            DateTime date = System.DateTime.Now;
            List<CompanyModel> lstCompany = new List<CompanyModel>();
            DataTable dt = DataOperation.GetDataTableWithoutParameter("TblCompanyMaster_AllRec");
            if (dt.Rows.Count > 0)
            {
                for (int index = 0; index < dt.Rows.Count; index++)
                {

                    CompanyModel Company = new CompanyModel();
                    Company.IsActive = dt.Rows[index]["IsActive"].ToString();
                    Company.Code = dt.Rows[index]["CompanyCode"].ToString();
                    Company.Name = dt.Rows[index]["CompanyName"].ToString();
                    Company.ShortName = dt.Rows[index]["ShortName"].ToString();
                    Company.IndustryNature = dt.Rows[index]["IndustryNature"].ToString();
                    Company.Address = dt.Rows[index]["CompanyAddress"].ToString();
                    Company.PhoneNo = dt.Rows[index]["PhoneNo"].ToString();
                    Company.EmailId = dt.Rows[index]["EmailId"].ToString();
                    Company.RegistationNo = dt.Rows[index]["RegistationNo"].ToString();
                    Company.PANNo = dt.Rows[index]["PANNo"].ToString();
                    Company.TANNo = dt.Rows[index]["TANNo"].ToString();
                    Company.PFNo = dt.Rows[index]["PFNo"].ToString();
                    Company.LCNo = dt.Rows[index]["LCNo"].ToString();
                    Company.CreatedDate = Convert.ToDateTime(dt.Rows[index]["CreatedDate"].ToString());
                    Company.LastModifiedBy = dt.Rows[index]["LastModifiedBy"].ToString();
                    Company.LastModifiedDate = Convert.ToDateTime(dt.Rows[index]["LastModifiedDate"].ToString());
                    lstCompany.Add(Company);
                }
            }
            return lstCompany;
        }
        //To Add new Company record    
        public int Add(CompanyModel Company)
        {
            Company.ShortName = (String.IsNullOrEmpty(Company.ShortName) ? "" : Company.ShortName);
            Company.IndustryNature = (String.IsNullOrEmpty(Company.IndustryNature) ? "" : Company.IndustryNature);
            Company.Address = (String.IsNullOrEmpty(Company.Address) ? "" : Company.Address);
            Company.PhoneNo = (String.IsNullOrEmpty(Company.PhoneNo) ? "" : Company.PhoneNo);
            Company.EmailId = (String.IsNullOrEmpty(Company.EmailId) ? "" : Company.EmailId);
            Company.RegistationNo = (String.IsNullOrEmpty(Company.RegistationNo) ? "" : Company.RegistationNo);
            Company.PANNo = (String.IsNullOrEmpty(Company.PANNo) ? "" : Company.PANNo);
            Company.TANNo = (String.IsNullOrEmpty(Company.TANNo) ? "" : Company.TANNo);
            Company.PFNo = (String.IsNullOrEmpty(Company.PFNo) ? "" : Company.PFNo);
            Company.LCNo = (String.IsNullOrEmpty(Company.PFNo) ? "" : Company.LCNo);

            int res = DataOperation.InsUpdDel("TblCompanyMaster_Add",
                new SqlParameter("IsActive", Company.IsActive),
                new SqlParameter("CompanyCode", Company.Code),
                new SqlParameter("CompanyName", Company.Name),
                new SqlParameter("ShortName", Company.ShortName),
                new SqlParameter("IndustryNature", Company.IndustryNature),
                new SqlParameter("CompanyAddress", Company.Address),
                new SqlParameter("PhoneNo", Company.PhoneNo),
                new SqlParameter("EmailId", Company.EmailId),
                new SqlParameter("RegistationNo", Company.RegistationNo),
                new SqlParameter("PANNo", Company.PANNo),
                new SqlParameter("TANNo", Company.TANNo),
                new SqlParameter("PFNo", Company.PFNo),
                new SqlParameter("LCNo", Company.LCNo),
                new SqlParameter("LastModifiedBy", Company.LastModifiedBy.Trim()));
            if (res == 1)
            {
                CompanyModel oldobject = GetCompanyData(Company.Code);
                AuditLog.CreateAuditTrail(CompanyModel.COMPANYMASTER, CompanyModel.COMPANYCODE, Company.Code, AppConstant.CREATED, Company.LastModifiedBy, Company.LoginTerminalNameIP, oldobject, Company);
            }
            return res;
        }
        //To Update the records of a particluar  Company  
        public int Update(CompanyModel Company)
        {
            Company.ShortName = (String.IsNullOrEmpty(Company.ShortName) ? "" : Company.ShortName);
            Company.IndustryNature = (String.IsNullOrEmpty(Company.IndustryNature) ? "" : Company.IndustryNature);
            Company.Address = (String.IsNullOrEmpty(Company.Address) ? "" : Company.Address);
            Company.PhoneNo = (String.IsNullOrEmpty(Company.PhoneNo) ? "" : Company.PhoneNo);
            Company.EmailId = (String.IsNullOrEmpty(Company.EmailId) ? "" : Company.EmailId);
            Company.RegistationNo = (String.IsNullOrEmpty(Company.RegistationNo) ? "" : Company.RegistationNo);
            Company.PANNo = (String.IsNullOrEmpty(Company.PANNo) ? "" : Company.PANNo);
            Company.TANNo = (String.IsNullOrEmpty(Company.TANNo) ? "" : Company.TANNo);
            Company.PFNo = (String.IsNullOrEmpty(Company.PFNo) ? "" : Company.PFNo);
            Company.LCNo = (String.IsNullOrEmpty(Company.PFNo) ? "" : Company.LCNo);
            CompanyModel objgetexpmast = GetCompanyData(Company.Code);
            int res = DataOperation.InsUpdDel("TblCompanyMaster_Upd",
                new SqlParameter("IsActive", Company.IsActive),
                new SqlParameter("CompanyCode", Company.Code),
                new SqlParameter("CompanyName", Company.Name),
                new SqlParameter("ShortName", Company.ShortName),
                new SqlParameter("IndustryNature", Company.IndustryNature),
                new SqlParameter("CompanyAddress", Company.Address),
                new SqlParameter("PhoneNo", Company.PhoneNo),
                new SqlParameter("EmailId", Company.EmailId),
                new SqlParameter("RegistationNo", Company.RegistationNo),
                new SqlParameter("PANNo", Company.PANNo),
                new SqlParameter("TANNo", Company.TANNo),
                new SqlParameter("PFNo", Company.PFNo),
                new SqlParameter("LCNo", Company.LCNo),
                new SqlParameter("LastModifiedBy", Company.LastModifiedBy.Trim()));
            if (res == 1)
            {
                CompanyCloneModel objAuditold = new CompanyCloneModel();
                CompanyCloneModel objAuditnew = new CompanyCloneModel();

                //code for clone copy create for old object
                if (objAuditold.IsActive == "True")
                {
                    objAuditold.IsActive = "Y";
                }
                else
                {
                    objAuditold.IsActive = "N";
                }
                objAuditold.Code = objgetexpmast.Code;
                objAuditold.Name = objgetexpmast.Name;
                objAuditold.ShortName = objgetexpmast.ShortName;
                objAuditold.IndustryNature = objgetexpmast.IndustryNature;
                objAuditold.Address = objgetexpmast.Address;
                objAuditold.PhoneNo = objgetexpmast.PhoneNo;
                objAuditold.EmailId = objgetexpmast.EmailId;
                objAuditold.RegistationNo = objgetexpmast.RegistationNo;
                objAuditold.PANNo = objgetexpmast.PANNo;
                objAuditold.TANNo = objgetexpmast.TANNo;
                objAuditold.PFNo = objgetexpmast.PFNo;
                objAuditold.LCNo = objgetexpmast.LCNo;
                //code for clone copy create for new  object
                objAuditnew.IsActive = Company.IsActive;
                objAuditnew.Code = Company.Code;
                objAuditnew.Name = Company.Name;
                objAuditnew.ShortName = Company.ShortName;
                objAuditnew.IndustryNature = Company.IndustryNature;
                objAuditnew.Address = Company.Address;
                objAuditnew.PhoneNo = Company.PhoneNo;
                objAuditnew.EmailId = Company.EmailId;
                objAuditnew.RegistationNo = Company.RegistationNo;
                objAuditnew.PANNo = Company.PANNo;
                objAuditnew.TANNo = Company.TANNo;
                objAuditnew.PFNo = Company.PFNo;
                objAuditnew.LCNo = Company.LCNo;
                //objAuditnew.LastModifiedBy = Convert.ToString(HttpContext.Current.Session["UserName"]);
                //objAuditnew.LastModifiedDate = DateTime.Now;
                AuditLog.CreateAuditTrail(CompanyModel.COMPANYMASTER, CompanyModel.COMPANYCODE, Company.Code, AppConstant.MODIFIED, Company.LastModifiedBy, Company.LoginTerminalNameIP, objAuditold, objAuditnew);
            }
            return res;
        }
        //Get the details of a particular  Company Data  
        public CompanyModel GetCompanyData(string Code)
        {
            CompanyModel Company = new CompanyModel();
            DataTable dt = DataOperation.GetDataTableWithParameter("TblCompanyMaster_GetData", new SqlParameter("CompanyCode", Code.Trim()));
            if (dt.Rows.Count > 0)
            {
                for (int index = 0; index < dt.Rows.Count; index++)
                {
                    if (dt.Rows[index]["IsActive"].ToString() == "Y")
                    {
                        Company.IsActive = "True";
                    }
                    else
                    {
                        Company.IsActive = "False";
                    }
                    Company.Code = dt.Rows[index]["CompanyCode"].ToString();
                    Company.Name = dt.Rows[index]["CompanyName"].ToString();
                    Company.ShortName = dt.Rows[index]["ShortName"].ToString();
                    Company.IndustryNature = dt.Rows[index]["IndustryNature"].ToString();
                    Company.Address = dt.Rows[index]["CompanyAddress"].ToString();
                    Company.PhoneNo = dt.Rows[index]["PhoneNo"].ToString();
                    Company.EmailId = dt.Rows[index]["EmailId"].ToString();
                    Company.RegistationNo = dt.Rows[index]["RegistationNo"].ToString();
                    Company.PANNo = dt.Rows[index]["PANNo"].ToString();
                    Company.TANNo = dt.Rows[index]["TANNo"].ToString();
                    Company.PFNo = dt.Rows[index]["PFNo"].ToString();
                    Company.LCNo = dt.Rows[index]["LCNo"].ToString();
                    Company.CreatedDate = Convert.ToDateTime(dt.Rows[index]["CreatedDate"].ToString());
                    Company.LastModifiedBy = dt.Rows[index]["LastModifiedBy"].ToString();
                    Company.LastModifiedDate = Convert.ToDateTime(dt.Rows[index]["LastModifiedDate"].ToString());
                }
            }
            return Company;
        }
        //Get the details of a particular  Company Code  
        public int GetCompanyCode(string Code)
        {
            
            int res = 0;
            DataTable dt = DataOperation.GetDataTableWithParameter("TblCompanyMaster_CheckIsCode", new SqlParameter("CompanyCode", Code.Trim()));
            if (dt.Rows.Count > 0)
            {
                for (int index = 0; index < dt.Rows.Count; index++)
                {
                    res = Convert.ToInt32(dt.Rows[index]["cnt"].ToString());
                }
            }
            return res;
        }
        //To Delete the record on a particular Copmpany  
        public int DeleteCompanyold(string Code)
        {
            string oldCode = Code;
            CompanyModel oldobject = GetCompanyData(oldCode.Trim());
            CompanyCloneModel objAuditold = new CompanyCloneModel();
            CompanyCloneModel objAuditnew = new CompanyCloneModel();
            int res = DataOperation.InsUpdDel("TblCompanyMaster_Del", new SqlParameter("CompanyCode", Code.Trim()));
            return res;
        }
        public int Delete(CompanyModel Company)
        {
            int res = -1;
            CompanyCloneModel objAuditold = new CompanyCloneModel();
            CompanyCloneModel objAuditnew = new CompanyCloneModel();
            CompanyModel objgetexpmast = GetCompanyData(Company.Code.Trim());
            //code for clone copy create for old object
            objAuditold.IsActive = objgetexpmast.IsActive;
            objAuditold.Code = objgetexpmast.Code;
            objAuditold.Name = objgetexpmast.Name;
            objAuditold.ShortName = objgetexpmast.ShortName;
            objAuditold.IndustryNature = objgetexpmast.IndustryNature;
            objAuditold.Address = objgetexpmast.Address;
            objAuditold.PhoneNo = objgetexpmast.PhoneNo;
            objAuditold.EmailId = objgetexpmast.EmailId;
            objAuditold.RegistationNo = objgetexpmast.RegistationNo;
            objAuditold.PANNo = objgetexpmast.PANNo;
            objAuditold.TANNo = objgetexpmast.TANNo;
            objAuditold.PFNo = objgetexpmast.PFNo;
            objAuditold.LCNo = objgetexpmast.LCNo;
            //code for clone copy create for new  object
            objAuditnew.IsActive = "";
            objAuditnew.Code = "";
            objAuditnew.Name = "";
            objAuditnew.ShortName = "";
            objAuditnew.IndustryNature = "";
            objAuditnew.Address = "";
            objAuditnew.PhoneNo = "";
            objAuditnew.EmailId = "";
            objAuditnew.RegistationNo = "";
            objAuditnew.PANNo = "";
            objAuditnew.TANNo = "";
            objAuditnew.PFNo = "";
            objAuditnew.LCNo = "";

            res = DataOperation.InsUpdDel("TblCompanyMaster_Del", new SqlParameter("CompanyCode", Company.Code.Trim()));
            if (res == 1)
            {
                AuditLog.CreateAuditTrail(CompanyModel.COMPANYMASTER, CompanyModel.COMPANYCODE, Company.Code.Trim(), AppConstant.DELETED, Company.LastModifiedBy, Company.LoginTerminalNameIP, objAuditold, objAuditnew);
            }
            // }
            return res;
        }
    }
}