﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IntegratedAttendanceSystem_API.Models
{
    public class EmployeeClone
    {
        public const string EmployeeMaster = "Employee Master";
        public const string PayCode = "PayCode";

        public string IsActive { get; set; }
        public string JobPosCode { get; set; }
        public string JobPosName { get; set; }
        public string JobPosDesc { get; set; }
        public string Gender { get; set; }
        public string CompCode { get; set; }
        public string DepCode { get; set; }
      //  public string GroupCode { get; set; }
        public string DesignCode { get; set; }
        public string JobId { get; set; }
        public string AgeId { get; set; }
        public string CatCode { get; set; }
        public string IMCode { get; set; }
        public string QID { get; set; }
        public string LangCode { get; set; }
        public string CertCode { get; set; }
        public string SpecLznID { get; set; }
        public string pskillID { get; set; }
        public string sskillID { get; set; }
        public string ExpId { get; set; }
        public string RelvExpId { get; set; }


    }



 
}