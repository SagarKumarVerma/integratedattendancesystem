﻿using IntegratedAttendanceSystem_API.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IntegratedAttendanceSystem_API.Models
{
    public class CompanyMasteModel
    {
        public List<Company_list> GetCompanyList()
        {
            List<Company_list> company = new List<Company_list>();
            DataTable dt = DataOperation.GetDataTableWithoutParameter("TblCompanyMaster_SelectAll");
            if (dt.Rows.Count > 0)
            {
                for (int index = 0; index < dt.Rows.Count; index++)
                {
                    company.Add(new Company_list
                    {
                        CompanyCode = Convert.ToString(dt.Rows[index]["CompanyCode"]).Trim(),
                        CompanyName = Convert.ToString(dt.Rows[index]["CompanyName"]).Trim(),
                    });
                }
            }
            return company;
        }
    }
}