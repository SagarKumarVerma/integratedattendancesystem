﻿using IntegratedAttendanceSystem_API.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IntegratedAttendanceSystem_API.Models
{
    public class GradeMasterModel
    {
        public List<Grade_list> GetGradeList()
        {
            List<Grade_list> Grade = new List<Grade_list>();
            DataTable dt = DataOperation.GetDataTableWithoutParameter("TblGrade_SelectAll");
            if (dt.Rows.Count > 0)
            {
                for (int index = 0; index < dt.Rows.Count; index++)
                {
                    Grade.Add(new Grade_list
                    {
                        GradeCode = Convert.ToString(dt.Rows[index]["GradeCode"]).Trim(),
                        GradeName = Convert.ToString(dt.Rows[index]["GradeCode"]).Trim(),
                    });
                }
            }
            return Grade;
        }
    }
}