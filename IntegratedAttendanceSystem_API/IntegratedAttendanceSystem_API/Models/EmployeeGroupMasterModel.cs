﻿using IntegratedAttendanceSystem_API.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IntegratedAttendanceSystem_API.Models
{
    public class EmployeeGroupMasterModel
    {
        public List<EmployeeGroup_list> GetEmployeeGroupList()
        {
            List<EmployeeGroup_list> EmployeeGroup = new List<EmployeeGroup_list>();
            DataTable dt = DataOperation.GetDataTableWithoutParameter("TblEmployeeGroup_SelectAll");
            if (dt.Rows.Count > 0)
            {
                for (int index = 0; index < dt.Rows.Count; index++)
                {
                    EmployeeGroup.Add(new EmployeeGroup_list
                    {
                        GroupID = Convert.ToString(dt.Rows[index]["GroupID"]).Trim(),
                        GroupName = Convert.ToString(dt.Rows[index]["GroupName"]).Trim(),
                    });
                }
            }
            return EmployeeGroup;
        }
    }
}