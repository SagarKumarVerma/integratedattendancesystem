﻿using IntegratedAttendanceSystem_API.DataAccess;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace IntegratedAttendanceSystem_API.Models
{
    public class DeviceAccessModel
    {
        public List<DeviceModel> GetAllDevice()
        {
            List<DeviceModel> lstDept = new List<DeviceModel>();
            DataTable dt = DataOperation.GetDataTableWithoutParameter("TblDeviceMaster_AllRec");
            if (dt.Rows.Count > 0)
            {

                for (int index = 0; index < dt.Rows.Count; index++)
                {
                    DeviceModel DeptMast = new DeviceModel();
                    DeptMast.DeviceCode = dt.Rows[index]["DeviceCode"].ToString();
                    DeptMast.DeviceName = dt.Rows[index]["DeviceName"].ToString();
                    DeptMast.IsActive = dt.Rows[index]["IsActive"].ToString().Trim();
                    //#
                    DeptMast.LastModifiedBy = dt.Rows[index]["LastModifiedBy"].ToString();
                    DeptMast.LastModifiedDate = Convert.ToDateTime(dt.Rows[index]["LastModifiedDate"].ToString());
                    //
                    lstDept.Add(DeptMast);
                }
            }
            return lstDept;
        }
        public int Add(DeviceModel DeptMast)
        {
            int res = DataOperation.InsUpdDel("TblDeviceMaster_Add",
                new SqlParameter("DeviceCode", DeptMast.DeviceCode),
                new SqlParameter("DeviceName", DeptMast.DeviceName),
                new SqlParameter("IsActive", DeptMast.IsActive),
                new SqlParameter("LastModifiedBy", DeptMast.LastModifiedBy.ToString()));
            if (res == 1)
            {
                DeviceModel oldobject = GetDeviceId(DeptMast.DeviceCode);
                AuditLog.CreateAuditTrail(DeviceModel.DeviceMASTER, DeviceModel.DeviceCODE, DeptMast.DeviceCode, AppConstant.CREATED, DeptMast.LastModifiedBy, DeptMast.LoginTerminalNameIP, oldobject, DeptMast);
            }
            return res;

        }
        public int Update(DeviceModel DeptMast)
        {


            DeviceModel objgetDeptMast = GetDeviceId(DeptMast.DeviceCode);
            ////int res = -1;
            //DataTable dt = DataOperation.GetDataTableWithParameter("SP_CheckDeptRefUse", new SqlParameter("DeviceCode", DeptMast.DeviceCode.Trim()));
            //if (Convert.ToInt32(dt.Rows[0]["Column1"]) > 0)
            //{
            //    res = 2;
            //}
            //else
            //{
            int res = DataOperation.InsUpdDel("TblDeviceMaster_Upd",
                new SqlParameter("DeviceCode", DeptMast.DeviceCode),
                new SqlParameter("DeviceName", DeptMast.DeviceName),
                new SqlParameter("IsActive", DeptMast.IsActive),
                 new SqlParameter("LastModifiedBy", DeptMast.LastModifiedBy.ToString()));
                if (res == 1)
                {
                    DeviceCloneModel objDeptMstCloOld = new DeviceCloneModel();
                    DeviceCloneModel objDeptMstCloNew = new DeviceCloneModel();

                    //code for clone copy created for old obj
                    //#
                    if (objgetDeptMast.IsActive == "True")
                    {
                        objDeptMstCloOld.IsActive = "Y";
                    }
                    else
                    {
                        objDeptMstCloOld.IsActive = "N";
                    }
                    //#
                    objDeptMstCloOld.DeviceCode = objgetDeptMast.DeviceCode;
                    objDeptMstCloOld.DeviceName = objgetDeptMast.DeviceName;
                    objDeptMstCloOld.LastModifiedBy = objgetDeptMast.LastModifiedBy;
                    objDeptMstCloOld.LastModifiedDate = objgetDeptMast.LastModifiedDate;
                    //code for clone copy create for new  object
                    objDeptMstCloNew.IsActive = DeptMast.IsActive;
                    objDeptMstCloNew.DeviceCode = DeptMast.DeviceCode;
                    objDeptMstCloNew.DeviceName = DeptMast.DeviceName;
                    objDeptMstCloNew.LastModifiedBy = DeptMast.LastModifiedBy; // Convert.ToString(HttpContext.Current.Session["UserName"]);
                    objDeptMstCloNew.LastModifiedDate = DateTime.Now;
                    AuditLog.CreateAuditTrail(DeviceModel.DeviceMASTER, DeviceModel.DeviceCODE, DeptMast.DeviceCode, AppConstant.MODIFIED, DeptMast.LastModifiedBy, DeptMast.LoginTerminalNameIP, objDeptMstCloOld, objDeptMstCloNew);
                }

            //}
            return res;
        }
        public int Delete(DeviceModel DeptMast)
        {
            int res = -1;
            string oldDeviceCode = DeptMast.DeviceCode; // id;
            DeviceModel oldobject = GetDeviceId(oldDeviceCode.Trim());
            DeviceCloneModel objAuditold = new DeviceCloneModel();
            DeviceCloneModel objAuditnew = new DeviceCloneModel();
            objAuditold.DeviceCode = oldobject.DeviceCode;
            objAuditold.DeviceName = oldobject.DeviceName;
            objAuditold.IsActive = oldobject.IsActive;
            //code for clone copy create for new  object
            objAuditnew.DeviceCode = "";
            objAuditnew.DeviceName = "";
            objAuditnew.IsActive = "";
            //DataTable dt = DataOperation.GetDataTableWithParameter("SP_CheckLangRefUse", new SqlParameter("DeviceCode", DeptMast.DeviceCode.Trim()));
            //if (Convert.ToInt32(dt.Rows[0]["Column1"]) > 0)
            //{
            //    res = 2;
            //}
            //else
            //{
                res = DataOperation.InsUpdDel("TblDeviceMaster_Del", new SqlParameter("DeviceCode", DeptMast.DeviceCode.Trim()));
                if (res == 1)
                {
                    AuditLog.CreateAuditTrail(DeviceModel.DeviceMASTER, DeviceModel.DeviceCODE, oldDeviceCode.Trim(), AppConstant.DELETED, DeptMast.LastModifiedBy, DeptMast.LoginTerminalNameIP, objAuditold, objAuditnew);
                }
            //}
            return res;
        }
        // Get the details of a particular Device
        public List<DeviceModel> ListAllDevice()
        {
            List<DeviceModel> lstDeptMast = new List<DeviceModel>();
            DataTable dt = DataOperation.GetDataTableWithoutParameter("TblDeviceMaster_AllRec");
            if (dt.Rows.Count > 0)
            {

                for (int index = 0; index < dt.Rows.Count; index++)
                {
                    DeviceModel DeptMast = new DeviceModel();
                    DeptMast.DeviceCode = dt.Rows[index]["DeviceCode"].ToString();
                    DeptMast.DeviceName = dt.Rows[index]["DeviceName"].ToString();
                    DeptMast.IsActive = dt.Rows[index]["IsActive"].ToString();
                    DeptMast.LastModifiedBy = dt.Rows[index]["LastModifiedBy"].ToString();
                    DeptMast.LastModifiedDate = Convert.ToDateTime(dt.Rows[index]["LastModifiedDate"].ToString());
                    lstDeptMast.Add(DeptMast);
                }

            }
            return lstDeptMast;
        }
        public DeviceModel GetDeviceId(string Code)
        {

            DeviceModel DeptMast = new DeviceModel();
            DataTable dt = DataOperation.GetDataTableWithParameter("TblDeviceMaster_GetDeviceData", new SqlParameter("DeviceCode", Code.Trim()));
            if (dt.Rows.Count > 0)
            {

                for (int index = 0; index < dt.Rows.Count; index++)
                {
                    DeptMast.DeviceCode = dt.Rows[index]["DeviceCode"].ToString();
                    DeptMast.DeviceName = dt.Rows[index]["DeviceName"].ToString();
                    if (dt.Rows[index]["IsActive"].ToString() == "Y")
                        DeptMast.IsActive = "True";
                    else
                        DeptMast.IsActive = "False";

                    //#
                    DeptMast.LastModifiedBy = dt.Rows[index]["LastModifiedBy"].ToString();
                    DeptMast.LastModifiedDate = Convert.ToDateTime(dt.Rows[index]["LastModifiedDate"].ToString());
                    //
                }
            }
            return DeptMast;
        }
        //Get the details of a particular  Device Code  
        public int GetDeviceCode(string Code)
        {
            int res = 0;
            DataTable dt = DataOperation.GetDataTableWithParameter("TblDeviceMaster_CheckIsCode", new SqlParameter("DeviceCode", Code.Trim()));
            if (dt.Rows.Count > 0)
            {
                for (int index = 0; index < dt.Rows.Count; index++)
                {
                    res = Convert.ToInt32(dt.Rows[index]["cnt"].ToString());
                }
            }
            return res;
        }
    }
}