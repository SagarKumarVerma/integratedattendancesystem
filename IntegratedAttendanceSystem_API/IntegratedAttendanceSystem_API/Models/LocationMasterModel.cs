﻿using IntegratedAttendanceSystem_API.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IntegratedAttendanceSystem_API.Models
{
    public class LocationMasterModel
    {
        public List<Division_list> GetLocationList()
        {
            List<Division_list> div = new List<Division_list>();
            DataTable dt = DataOperation.GetDataTableWithoutParameter("TblLocation_SelectAll");
            if (dt.Rows.Count > 0)
            {
                for (int index = 0; index < dt.Rows.Count; index++)
                {
                    div.Add(new Division_list
                    {
                        DivisionCode = Convert.ToString(dt.Rows[index]["DivisionCode"]).Trim(),
                        DivisionName = Convert.ToString(dt.Rows[index]["DivisionName"]).Trim(),
                    });
                }
            }
            return div;
        }
    }
}