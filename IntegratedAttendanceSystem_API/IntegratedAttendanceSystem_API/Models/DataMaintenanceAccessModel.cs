﻿using System;
using Microsoft.VisualBasic;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;
using IntegratedAttendanceSystem_API.DataAccess;

namespace IntegratedAttendanceSystem_API.Models
{
    public class DataMaintenanceAccessModel
    {
        //int i = 0, intNumberOfInstallment, intTempNumberOfInstallment, intNumberOfInstallmentLoan, intTempNumberOfInstallmentLoan;
        //double dblAdvanceAmount, dblTempAdvanceAmount, dblInstallmentAmount, dblLoanAmount, dblTempLoanAmount, dblInstallmentAmountLoan, dblActualAmount , dblIntrestRate , dblCalculatedInstallmentAmountLoan;
        //string strTransactionMonth, strTransactionYear, strTransactionFrom, strTransactionLoanMonth, strTransactionLoanYear, strTransactionLoanFrom;
        //DateTime dtTransactionFromMonth , dtTempTransactionFromMonth, dtTransactionLoanFromMonth, dtTempTransactionLoanFromMonth;
        //To View all Advance Loan Setup details    
        public IEnumerable<DataMaintenanceModel.EmployeeCode_list> GetAllEmployeeCode()
        {
            List<DataMaintenanceModel.EmployeeCode_list> lstEmployeeCode = new List<DataMaintenanceModel.EmployeeCode_list>();
            DataTable dt = DataOperation.GetDataTableWithoutParameter("tblEmployeeMaster_Details_DDL");
            if (dt.Rows.Count > 0)
            {
                for (int index = 0; index < dt.Rows.Count; index++)
                {
                    lstEmployeeCode.Add(new DataMaintenanceModel.EmployeeCode_list
                    {
                        EmployeeCode = Convert.ToString(dt.Rows[index]["Paycode"]).Trim(),
                        EmployeeName = Convert.ToString(dt.Rows[index]["EmpName"]).Trim()
                    });
                }
            }
            return lstEmployeeCode;
        }
        public DataMaintenanceModel SelectedEmployeecodeListDetl(string EmpCode)
        {
            DataMaintenanceModel AdvLonStpMdl = new DataMaintenanceModel();
            DataTable dt = DataOperation.GetDataTableWithParameter("tblEmployeeMaster_Selected_Details", new SqlParameter("EmpCode", EmpCode.Trim()));
            if (dt.Rows.Count > 0)
            {
                for (int index = 0; index < dt.Rows.Count; index++)
                {
                    AdvLonStpMdl.EmpCode = dt.Rows[index]["Paycode"].ToString();
                    AdvLonStpMdl.EnrollmentCode = dt.Rows[index]["PresentCardNo"].ToString();
                    AdvLonStpMdl.EmpName = dt.Rows[index]["EmpName"].ToString();
                    AdvLonStpMdl.EmpStatus = dt.Rows[index]["ACTIVE"].ToString();
                    AdvLonStpMdl.Company = dt.Rows[index]["companyname"].ToString();
                    AdvLonStpMdl.Department = dt.Rows[index]["DepartmentName"].ToString();
                    AdvLonStpMdl.Category = dt.Rows[index]["CategoryName"].ToString();
                    AdvLonStpMdl.ISPRESENTONWOPRESENT = dt.Rows[index]["ISPRESENTONWOPRESENT"].ToString();
                    AdvLonStpMdl.SSN = dt.Rows[index]["SSN"].ToString();

                    //AdvLonStpMdl.CreatedDate = Convert.ToDateTime(dt.Rows[index]["CreatedDate"].ToString());
                    //AdvLonStpMdl.LastModifiedBy = dt.Rows[index]["LastModifiedBy"].ToString();
                    //AdvLonStpMdl.LastModifiedDate = Convert.ToDateTime(dt.Rows[index]["LastModifiedDate"].ToString());
                }
            }
            return AdvLonStpMdl;
        }

        public DataMaintenanceModel GetAttList(DataMaintenanceModel AttList)
        {
            //DataMaintenanceModel AttList = new DataMaintenanceModel();

            DataTable dt = DataOperation.GetDataTableWithParameter("tblTimeRegister_Select_Att", new SqlParameter("SSN", AttList.SSN.Trim()), new SqlParameter("Dateoffice", AttList.DateOffice));
            if (dt.Rows.Count > 0)
            {
                for (int index = 0; index < dt.Rows.Count; index++)
                {
                    AttList.EmpCode = dt.Rows[index]["Paycode"].ToString();
                    AttList.SSN = dt.Rows[index]["SSN"].ToString();
                    AttList.DateOffice = Convert.ToDateTime(dt.Rows[index]["DateOffice"].ToString());
                    AttList.In1 = dt.Rows[index]["in1"].ToString();
                    AttList.Out2 = dt.Rows[index]["out2"].ToString();
                    AttList.AttStatus = dt.Rows[index]["Status"].ToString();
                    //AttList.Company = dt.Rows[index]["companyname"].ToString();
                    //AttList.Department = dt.Rows[index]["DepartmentName"].ToString();
                    //AttList.Category = dt.Rows[index]["CategoryName"].ToString();

                    //AdvLonStpMdl.CreatedDate = Convert.ToDateTime(dt.Rows[index]["CreatedDate"].ToString());
                    //AdvLonStpMdl.LastModifiedBy = dt.Rows[index]["LastModifiedBy"].ToString();
                    //AdvLonStpMdl.LastModifiedDate = Convert.ToDateTime(dt.Rows[index]["LastModifiedDate"].ToString());
                }
            }
            return AttList;
        }

        public DataMaintenanceModel GetAllAtt(DataMaintenanceModel AttList)
        {
            //DataMaintenanceModel AttList = new DataMaintenanceModel();

            DataTable dt = DataOperation.GetDataTableWithParameter("[tblTimeRegister_Select_AllAtt]", new SqlParameter("SSN", AttList.SSN.Trim()), new SqlParameter("Dateoffice", AttList.DateOffice));
            if (dt.Rows.Count > 0)
            {
                for (int index = 0; index < dt.Rows.Count; index++)
                {
                    AttList.EmpCode = dt.Rows[index]["Paycode"].ToString();
                    AttList.SSN = dt.Rows[index]["SSN"].ToString();
                    AttList.DateOffice = Convert.ToDateTime(dt.Rows[index]["DateOffice"].ToString());
                    AttList.In1 = dt.Rows[index]["in1"].ToString();
                    AttList.Out2 = dt.Rows[index]["out2"].ToString();
                    AttList.AttStatus = dt.Rows[index]["Status"].ToString();
                    //AttList.Company = dt.Rows[index]["companyname"].ToString();
                    //AttList.Department = dt.Rows[index]["DepartmentName"].ToString();
                    //AttList.Category = dt.Rows[index]["CategoryName"].ToString();

                    //AdvLonStpMdl.CreatedDate = Convert.ToDateTime(dt.Rows[index]["CreatedDate"].ToString());
                    //AdvLonStpMdl.LastModifiedBy = dt.Rows[index]["LastModifiedBy"].ToString();
                    //AdvLonStpMdl.LastModifiedDate = Convert.ToDateTime(dt.Rows[index]["LastModifiedDate"].ToString());
                }
            }
            return AttList;
        }
        //To Add new Company record    
        public int Add(DataMaintenanceModel mPunch)
        {
            mPunch.EnrollmentCode = (String.IsNullOrEmpty(mPunch.EnrollmentCode) ? "" : mPunch.EnrollmentCode);
            mPunch.EmpCode = (String.IsNullOrEmpty(mPunch.EmpCode) ? "" : mPunch.EmpCode);
            string strDateOffice = mPunch.DateOffice.ToString("yyyy-MM-dd") + " " + mPunch.PunchTime;
            mPunch.Company = (String.IsNullOrEmpty(mPunch.Company) ? "" : mPunch.Company);
            mPunch.SSN = (String.IsNullOrEmpty(mPunch.SSN) ? "" : mPunch.SSN);

            int res = DataOperation.InsUpdDel("MachineRawPunch_ManualPunch",
                new SqlParameter("CARDNO", mPunch.EnrollmentCode),
                new SqlParameter("PAYCODE", mPunch.EmpCode),
                //new SqlParameter("ISMANUAL", mPunch.Name),
                //new SqlParameter("P_Day", mPunch.ShortName),
                new SqlParameter("OFFICEPUNCH", strDateOffice),
                new SqlParameter("CompanyCode", mPunch.SSN.Split('_')[0]),
                new SqlParameter("SSN", mPunch.SSN));
            if (res == 1)
            {
                //CompanyModel oldobject = GetCompanyData(Company.Code);
                //AuditLog.CreateAuditTrail(CompanyModel.COMPANYMASTER, CompanyModel.COMPANYCODE, mPunch.Company, AppConstant.CREATED, mPunch.LastModifiedBy, mPunch.LoginTerminalNameIP, oldobject, mPunch);

                //int CallBackDay = DataOperation.InsUpdDel("ProcessAllRecords",
                //new SqlParameter("FromDate", mPunch.DateOffice.ToString("yyyy-MM-dd")),
                //new SqlParameter("ToDate", mPunch.DateOffice.ToString("yyyy-MM-dd")),
                ////new SqlParameter("ISMANUAL", mPunch.Name),
                ////new SqlParameter("P_Day", mPunch.ShortName),
                ////new SqlParameter("OFFICEPUNCH", strDateOffice),
                //new SqlParameter("CompanyCode", mPunch.SSN.Split('_')[0]),
                //new SqlParameter("PayCode", mPunch.EmpCode),
                //new SqlParameter("SetupId", 2));

                //CallBackDay(mPunch.DateOffice.ToString("yyyy-MM-dd"), mPunch.DateOffice.ToString("yyyy-MM-dd"), mPunch.SSN.Split('_')[0], mPunch.EmpCode, 2);
            }
            return res;
        }


        public int CallBackDay(DataMaintenanceModel mPunch)
        {
            mPunch.EnrollmentCode = (String.IsNullOrEmpty(mPunch.EnrollmentCode) ? "" : mPunch.EnrollmentCode);
            mPunch.EmpCode = (String.IsNullOrEmpty(mPunch.EmpCode) ? "" : mPunch.EmpCode);
            string strDateOffice = mPunch.DateOffice.ToString("yyyy-MM-dd") + " " + mPunch.PunchTime;
            mPunch.Company = (String.IsNullOrEmpty(mPunch.Company) ? "" : mPunch.Company);
            mPunch.SSN = (String.IsNullOrEmpty(mPunch.SSN) ? "" : mPunch.SSN);

            int CallBackDay = DataOperation.InsUpdDel("ProcessAllRecords",
            new SqlParameter("FromDate", mPunch.DateOffice.ToString("yyyy-MM-dd")),
            new SqlParameter("ToDate", mPunch.DateOffice.ToString("yyyy-MM-dd")),
            new SqlParameter("CompanyCode", mPunch.SSN.Split('_')[0]),
            new SqlParameter("PayCode", mPunch.EmpCode),
            new SqlParameter("SetupId", 2));

            if (CallBackDay == 1)
            {
                //CompanyModel oldobject = GetCompanyData(Company.Code);
                //AuditLog.CreateAuditTrail(CompanyModel.COMPANYMASTER, CompanyModel.COMPANYCODE, mPunch.Company, AppConstant.CREATED, mPunch.LastModifiedBy, mPunch.LoginTerminalNameIP, oldobject, mPunch);

                //CallBackDay(mPunch.DateOffice.ToString("yyyy-MM-dd"), mPunch.DateOffice.ToString("yyyy-MM-dd"), mPunch.SSN.Split('_')[0], mPunch.EmpCode, 2);
            }
            return CallBackDay;
        }
        public void CallBackDay1(string sFromDate, string sToDate, string sCompCode, string sPayCode, int sSetupId)
        {
            DataOperation.InsUpdDel("ProcessAllRecords",
                new SqlParameter("FromDate", sFromDate),
                new SqlParameter("ToDate", sFromDate),
                new SqlParameter("CompanyCode", sCompCode),
                new SqlParameter("PayCode", sPayCode),
                new SqlParameter("SetupId", sSetupId));
        }

        //To Add new AdvanceLoan record    
        //public int Add(DataMaintenanceModel AdvanceLoanSetup)
        //{
        //    AdvanceLoanSetup.Reason = (String.IsNullOrEmpty(AdvanceLoanSetup.Reason) ? "" : AdvanceLoanSetup.Reason);
        //    int res = 0;
        //    if (AdvanceLoanSetup.AdvanceLoanType == "A")
        //    {
        //        res = DataOperation.InsUpdDel("Payroll_TBLADVANCE_Add",
        //        new SqlParameter("PAYCODE", AdvanceLoanSetup.EmpCode),
        //        new SqlParameter("A_L", AdvanceLoanSetup.AdvanceLoanType),
        //        new SqlParameter("IDNO", AdvanceLoanSetup.IdentityNumber),
        //        new SqlParameter("MON_YEAR", AdvanceLoanSetup.AdvanceMonthDate),
        //        new SqlParameter("Tran_Month", AdvanceLoanSetup.TransactionFromMonth),
        //        new SqlParameter("ADV_AMT", AdvanceLoanSetup.AdvanceAmount),
        //        new SqlParameter("INST_AMT", AdvanceLoanSetup.InstallmentAmount),
        //        new SqlParameter("INSTNO", AdvanceLoanSetup.NumberOfInstallment),
        //        new SqlParameter("INTREST_RATE", AdvanceLoanSetup.IntrestRate),
        //        new SqlParameter("Reason", AdvanceLoanSetup.Reason),
        //        new SqlParameter("IsDeductFromSalary", AdvanceLoanSetup.DeductFromSalary),
        //        new SqlParameter("LastModifiedBy", AdvanceLoanSetup.LastModifiedBy.Trim()));

        //        dblAdvanceAmount = Convert.ToDouble(AdvanceLoanSetup.AdvanceAmount);
        //        dblTempAdvanceAmount = Convert.ToDouble(AdvanceLoanSetup.AdvanceAmount);
        //        intNumberOfInstallment = Convert.ToInt32(AdvanceLoanSetup.NumberOfInstallment);
        //        intTempNumberOfInstallment = Convert.ToInt32(AdvanceLoanSetup.NumberOfInstallment);
        //        dtTransactionFromMonth = Convert.ToDateTime(AdvanceLoanSetup.TransactionFromMonth);
        //        dtTempTransactionFromMonth = Convert.ToDateTime(AdvanceLoanSetup.TransactionFromMonth);
        //        dblInstallmentAmount = Convert.ToDouble(AdvanceLoanSetup.InstallmentAmount);
        //        for (i = 1; i <= intNumberOfInstallment; i++)
        //        {

        //            dblAdvanceAmount = dblAdvanceAmount - dblInstallmentAmount;
        //            strTransactionMonth = dtTransactionFromMonth.Month.ToString();
        //            strTransactionYear = dtTransactionFromMonth.Year.ToString();
        //            strTransactionFrom = "01/" + strTransactionMonth + "/" + strTransactionYear;
        //            res = DataOperation.InsUpdDel("Payroll_TBLADVANCEDATA_Add",
        //            new SqlParameter("PAYCODE", AdvanceLoanSetup.EmpCode),
        //            new SqlParameter("A_L", AdvanceLoanSetup.AdvanceLoanType),
        //            new SqlParameter("IDNO", AdvanceLoanSetup.IdentityNumber),
        //            new SqlParameter("MON_YEAR", strTransactionFrom),
        //            new SqlParameter("CASH_AMT", "0"),
        //            new SqlParameter("PAMT", "0"),
        //            new SqlParameter("INST_AMT", AdvanceLoanSetup.InstallmentAmount),
        //            new SqlParameter("BALANCE_AMT", dblAdvanceAmount),
        //            new SqlParameter("INSTNO", intTempNumberOfInstallment),
        //            new SqlParameter("IAMT", AdvanceLoanSetup.IntrestRate),
        //            new SqlParameter("Reason", AdvanceLoanSetup.Reason),
        //            new SqlParameter("IsDeductFromSalary", AdvanceLoanSetup.DeductFromSalary),
        //            new SqlParameter("LastModifiedBy", AdvanceLoanSetup.LastModifiedBy.Trim()));
        //            dtTransactionFromMonth = dtTempTransactionFromMonth.AddMonths(i);
        //            dblTempAdvanceAmount = dblAdvanceAmount;
        //            intTempNumberOfInstallment = intTempNumberOfInstallment - 1;
        //        }
        //    }
        //    else
        //    {
        //        res = DataOperation.InsUpdDel("Payroll_TBLADVANCE_Add",
        //       new SqlParameter("PAYCODE", AdvanceLoanSetup.EmpCode),
        //       new SqlParameter("A_L", AdvanceLoanSetup.AdvanceLoanType),
        //       new SqlParameter("IDNO", AdvanceLoanSetup.IdentityNumber),
        //       new SqlParameter("MON_YEAR", AdvanceLoanSetup.LoanMonthDate),
        //       new SqlParameter("Tran_Month", AdvanceLoanSetup.TransactionFromMonthLoan),
        //       new SqlParameter("ADV_AMT", AdvanceLoanSetup.LoanAmount),
        //       new SqlParameter("INST_AMT", AdvanceLoanSetup.InstallmentAmountLoan),
        //       new SqlParameter("INSTNO", AdvanceLoanSetup.NumberOfInstallmentLoan),
        //       new SqlParameter("INTREST_RATE", AdvanceLoanSetup.IntrestRate),
        //       new SqlParameter("Reason", AdvanceLoanSetup.Reason),
        //       new SqlParameter("IsDeductFromSalary", AdvanceLoanSetup.DeductFromSalary),
        //       new SqlParameter("LastModifiedBy", AdvanceLoanSetup.LastModifiedBy.Trim()));
        //        dblLoanAmount = Convert.ToDouble(AdvanceLoanSetup.LoanAmount);
        //        dblTempLoanAmount = Convert.ToDouble(AdvanceLoanSetup.LoanAmount);
        //        intNumberOfInstallmentLoan = Convert.ToInt32(AdvanceLoanSetup.NumberOfInstallmentLoan);
        //        intTempNumberOfInstallmentLoan = Convert.ToInt32(AdvanceLoanSetup.NumberOfInstallmentLoan);
        //        dtTransactionLoanFromMonth = Convert.ToDateTime(AdvanceLoanSetup.TransactionFromMonthLoan);
        //        dtTempTransactionLoanFromMonth = Convert.ToDateTime(AdvanceLoanSetup.TransactionFromMonthLoan);
        //        dblInstallmentAmountLoan = Convert.ToDouble(AdvanceLoanSetup.InstallmentAmountLoan);
        //        dblIntrestRate = Convert.ToDouble(AdvanceLoanSetup.IntrestRate);
        //        for (i = 1; i <= intNumberOfInstallmentLoan; i++)
        //        {
        //            //dblLoanAmount = Math.Round(dblLoanAmount - (Convert.ToDouble(dblInstallmentAmountLoan) - Math.Round(Microsoft.VisualBasic.Financial.IPmt((SimulateVal.Val(dblIntrestRate) / 1200), i, SimulateVal.Val(intNumberOfInstallmentLoan), -SimulateVal.Val(AdvanceLoanSetup.LoanAmount), 0, Microsoft.VisualBasic.DueDate.EndOfPeriod), 2)), 2);
        //            //dblCalculatedInstallmentAmountLoan = (Convert.ToDouble(dblInstallmentAmountLoan) - Math.Round(Microsoft.VisualBasic.Financial.IPmt((SimulateVal.Val(dblIntrestRate) / 1200), i, SimulateVal.Val(intNumberOfInstallmentLoan), -SimulateVal.Val(AdvanceLoanSetup.LoanAmount), 0, Microsoft.VisualBasic.DueDate.EndOfPeriod), 2));
        //            //dblActualAmount = Math.Round(Microsoft.VisualBasic.Financial.IPmt((SimulateVal.Val(dblIntrestRate) / 1200), i, SimulateVal.Val(intNumberOfInstallmentLoan), -SimulateVal.Val(AdvanceLoanSetup.LoanAmount), 0, Microsoft.VisualBasic.DueDate.EndOfPeriod), 2);
        //            strTransactionLoanMonth = dtTransactionLoanFromMonth.Month.ToString();
        //            strTransactionLoanYear = dtTransactionLoanFromMonth.Year.ToString();
        //            strTransactionLoanFrom = "01/" + strTransactionLoanMonth + "/" + strTransactionLoanYear;
        //            res = DataOperation.InsUpdDel("Payroll_TBLADVANCEDATA_Add",
        //            new SqlParameter("PAYCODE", AdvanceLoanSetup.EmpCode),
        //            new SqlParameter("A_L", AdvanceLoanSetup.AdvanceLoanType),
        //            new SqlParameter("IDNO", AdvanceLoanSetup.IdentityNumber),
        //            new SqlParameter("MON_YEAR", strTransactionLoanFrom),
        //            new SqlParameter("CASH_AMT", "0"),
        //            new SqlParameter("PAMT", dblCalculatedInstallmentAmountLoan),
        //            new SqlParameter("INST_AMT", dblInstallmentAmountLoan),
        //            new SqlParameter("BALANCE_AMT", dblLoanAmount),
        //            new SqlParameter("INSTNO", intTempNumberOfInstallmentLoan),
        //            new SqlParameter("IAMT", dblActualAmount),
        //            new SqlParameter("Reason", AdvanceLoanSetup.Reason),
        //            new SqlParameter("IsDeductFromSalary", AdvanceLoanSetup.DeductFromSalary),
        //            new SqlParameter("LastModifiedBy", AdvanceLoanSetup.LastModifiedBy.Trim()));
        //            dtTransactionLoanFromMonth = dtTempTransactionLoanFromMonth.AddMonths(i);
        //            dblTempLoanAmount = dblLoanAmount;
        //            intTempNumberOfInstallmentLoan = intTempNumberOfInstallmentLoan - 1;
        //        }
        //    }

        //    if (res == 1)
        //    {
        //        //DataMaintenanceModel oldobject = GetDataMaintenanceModelData(AdvanceLoanSetup.Code);
        //        AuditLog.CreateAuditTrail(DataMaintenanceModel.PAYROLLADVANCELOANSETUP, DataMaintenanceModel.PAYROLLADVANCELOANSETUPCODE, "", AppConstant.CREATED, AdvanceLoanSetup.LastModifiedBy, AdvanceLoanSetup.LoginTerminalNameIP, "", AdvanceLoanSetup);
        //    }
        //    return res;
        //}

        //To View all  details    
        //public DataMaintenanceModel GetEmpIdentityNumber(string EmpCode, string MonYear)
        //{
        //    //DateTime date = System.DateTime.Now;
        //    DataMaintenanceModel lstIdentityNumber = new DataMaintenanceModel();
        //    DataTable dt = DataOperation.GetDataTableWithParameter("Payroll_tblAdvanceLoan_Maximum_ID", new SqlParameter("PayCode", EmpCode.Trim()), new SqlParameter("MON_YEAR", MonYear.Trim()));
        //    if (dt.Rows.Count > 0)
        //    {
        //        lstIdentityNumber.IdentityNumber = Convert.ToString(dt.Rows[0]["MIDNO"]).Trim();
        //    }
        //    return lstIdentityNumber;
        //}

        //To View all Advance Loan details    
        //public IEnumerable<DataMaintenanceModel> BindEmployeeAdvanceLoan(string EmpCode)
        //{
        //    string AdvanceLoanTypeValues = "", DeductFromSalary = "";
        //    DateTime date = System.DateTime.Now;
        //    List<DataMaintenanceModel> lstBindEmployeeAdvance = new List<DataMaintenanceModel>();
        //    DataTable dt = DataOperation.GetDataTableWithParameter("Payroll_tblAdvanceLoan_Employee_Wise_Record", new SqlParameter("PayCode", EmpCode.Trim()));
        //    if (dt.Rows.Count > 0)
        //    {
        //        for (int index = 0; index < dt.Rows.Count; index++)
        //        {
        //            DataMaintenanceModel AdvLonStpMdl = new DataMaintenanceModel();
        //            AdvLonStpMdl.IdentityNumber = dt.Rows[index]["IDNO"].ToString();
        //            if (dt.Rows[index]["A_L"].ToString() == "A")
        //            {
        //                AdvanceLoanTypeValues = "Advance";
        //            }
        //            else if (dt.Rows[index]["A_L"].ToString() == "P")
        //            {
        //                AdvanceLoanTypeValues = "Personal Loan";
        //            }
        //            else if (dt.Rows[index]["A_L"].ToString() == "H")
        //            {
        //                AdvanceLoanTypeValues = "Home Loan";
        //            }
        //            else if (dt.Rows[index]["A_L"].ToString() == "V")
        //            {
        //                AdvanceLoanTypeValues = "Vehicle Loan";
        //            }
        //            else if (dt.Rows[index]["A_L"].ToString() == "O")
        //            {
        //                AdvanceLoanTypeValues = "Other Loan";
        //            }
        //            else
        //            {
        //                AdvanceLoanTypeValues = "selected";
        //            }
        //            AdvLonStpMdl.AdvanceLoanType = AdvanceLoanTypeValues;
        //            AdvLonStpMdl.AdvanceMonthDate = dt.Rows[index]["MON_YEAR"].ToString();
        //            AdvLonStpMdl.RequestDate = dt.Rows[index]["Entry_Date"].ToString();
        //            AdvLonStpMdl.TransactionFromMonth = dt.Rows[index]["Tran_Month"].ToString();
        //            AdvLonStpMdl.AdvanceAmount = dt.Rows[index]["ADV_AMT"].ToString();
        //            AdvLonStpMdl.IntrestRate = dt.Rows[index]["INTREST_RATE"].ToString();
        //            AdvLonStpMdl.InstallmentAmount = dt.Rows[index]["INST_AMT"].ToString();
        //            AdvLonStpMdl.NumberOfInstallment = dt.Rows[index]["INSTNO"].ToString();
        //            if (dt.Rows[index]["IsDeductFromSalary"].ToString() == "Y")
        //            {
        //                DeductFromSalary = "True";
        //            }
        //            else
        //            {
        //                DeductFromSalary = "False";
        //            }
        //            AdvLonStpMdl.DeductFromSalary = DeductFromSalary;
        //            AdvLonStpMdl.CreatedDate = Convert.ToDateTime(dt.Rows[index]["CreatedDate"].ToString());
        //            AdvLonStpMdl.LastModifiedBy = dt.Rows[index]["LastModifiedBy"].ToString();
        //            AdvLonStpMdl.LastModifiedDate = Convert.ToDateTime(dt.Rows[index]["LastModifiedDate"].ToString());
        //            lstBindEmployeeAdvance.Add(AdvLonStpMdl);
        //        }
        //    }
        //    return lstBindEmployeeAdvance;
        //}

        //public int Delete(DataMaintenanceModel EmployeeAdvanceLoanDelete)
        //{
        //    int res = -1;
        //    DataMaintenanceCloneModel objAdvLonStpClnMdlOld = new DataMaintenanceCloneModel();
        //    DataMaintenanceCloneModel objAdvLonStpClnMdlNew = new DataMaintenanceCloneModel();
        //    DataMaintenanceModel objAdvLonStpClnMdl = GetAdvanceLoanSetupData(EmployeeAdvanceLoanDelete.EmpCode.Trim(), EmployeeAdvanceLoanDelete.IdentityNumber.Trim());
        //    //code for clone copy create for old object
        //    objAdvLonStpClnMdlOld.IdentityNumber = objAdvLonStpClnMdl.IdentityNumber;
        //    objAdvLonStpClnMdlOld.AdvanceLoanType = objAdvLonStpClnMdl.AdvanceLoanType;
        //    objAdvLonStpClnMdlOld.AdvanceMonthDate = objAdvLonStpClnMdl.AdvanceMonthDate;
        //    objAdvLonStpClnMdlOld.RequestDate = objAdvLonStpClnMdl.RequestDate;
        //    objAdvLonStpClnMdlOld.TransactionFromMonth = objAdvLonStpClnMdl.TransactionFromMonth;
        //    objAdvLonStpClnMdlOld.AdvanceAmount = objAdvLonStpClnMdl.AdvanceAmount;
        //    objAdvLonStpClnMdlOld.IntrestRate = objAdvLonStpClnMdl.IntrestRate;
        //    objAdvLonStpClnMdlOld.InstallmentAmount = objAdvLonStpClnMdl.InstallmentAmount;
        //    objAdvLonStpClnMdlOld.NumberOfInstallment = objAdvLonStpClnMdl.NumberOfInstallment;
        //    objAdvLonStpClnMdlOld.DeductFromSalary = objAdvLonStpClnMdl.DeductFromSalary;

        //    //code for clone copy create for new  object
        //    objAdvLonStpClnMdlNew.IdentityNumber = "";
        //    objAdvLonStpClnMdlNew.AdvanceLoanType = "";
        //    objAdvLonStpClnMdlNew.AdvanceMonthDate = "";
        //    objAdvLonStpClnMdlNew.RequestDate = "";
        //    objAdvLonStpClnMdlNew.TransactionFromMonth = "";
        //    objAdvLonStpClnMdlNew.AdvanceAmount = "";
        //    objAdvLonStpClnMdlNew.IntrestRate = "";
        //    objAdvLonStpClnMdlNew.InstallmentAmount = "";
        //    objAdvLonStpClnMdlNew.NumberOfInstallment = "";
        //    objAdvLonStpClnMdlNew.DeductFromSalary = "";


        //    res = DataOperation.InsUpdDel("Payroll_tblAdvanceLoan_Delete", new SqlParameter("PayCode", EmployeeAdvanceLoanDelete.EmpCode.Trim()), new SqlParameter("IDNO", EmployeeAdvanceLoanDelete.IdentityNumber.Trim()));
        //    if (res == 1)
        //    {
        //        AuditLog.CreateAuditTrail(DataMaintenanceModel.PAYROLLADVANCELOANSETUP, DataMaintenanceModel.PAYROLLADVANCELOANSETUPCODE, EmployeeAdvanceLoanDelete.IdentityNumber.Trim(), AppConstant.DELETED, EmployeeAdvanceLoanDelete.LastModifiedBy, EmployeeAdvanceLoanDelete.LoginTerminalNameIP, objAdvLonStpClnMdlOld, objAdvLonStpClnMdlNew);
        //    }
        //    // }
        //    return res;
        //}
        //public DataMaintenanceModel GetAdvanceLoanSetupData(string EmpCode, string IdentityNumber)
        //{
        //    string AdvanceLoanTypeValues = "", DeductFromSalary = "";
        //    DataMaintenanceModel AdvanceLoanSetup = new DataMaintenanceModel();
        //    DataTable dt = DataOperation.GetDataTableWithParameter("Payroll_tblAdvanceLoan_Employee_IdentityNumber_Record", new SqlParameter("PayCode", EmpCode.Trim()), new SqlParameter("IDNO", IdentityNumber.Trim()));
        //    if (dt.Rows.Count > 0)
        //    {
        //        for (int index = 0; index < dt.Rows.Count; index++)
        //        {
        //            DataMaintenanceModel AdvLonStpMdl = new DataMaintenanceModel();
        //            AdvLonStpMdl.IdentityNumber = dt.Rows[index]["IDNO"].ToString();
        //            if (dt.Rows[index]["A_L"].ToString() == "A")
        //            {
        //                AdvanceLoanTypeValues = "Advance";
        //            }
        //            else if (dt.Rows[index]["A_L"].ToString() == "P")
        //            {
        //                AdvanceLoanTypeValues = "Personal Loan";
        //            }
        //            else if (dt.Rows[index]["A_L"].ToString() == "H")
        //            {
        //                AdvanceLoanTypeValues = "Home Loan";
        //            }
        //            else if (dt.Rows[index]["A_L"].ToString() == "V")
        //            {
        //                AdvanceLoanTypeValues = "Vehicle Loan";
        //            }
        //            else if (dt.Rows[index]["A_L"].ToString() == "O")
        //            {
        //                AdvanceLoanTypeValues = "Other Loan";
        //            }
        //            else
        //            {
        //                AdvanceLoanTypeValues = "selected";
        //            }
        //            AdvLonStpMdl.AdvanceLoanType = AdvanceLoanTypeValues;
        //            AdvLonStpMdl.AdvanceMonthDate = dt.Rows[index]["MON_YEAR"].ToString();
        //            AdvLonStpMdl.RequestDate = dt.Rows[index]["Entry_Date"].ToString();
        //            AdvLonStpMdl.TransactionFromMonth = dt.Rows[index]["Tran_Month"].ToString();
        //            AdvLonStpMdl.AdvanceAmount = dt.Rows[index]["ADV_AMT"].ToString();
        //            AdvLonStpMdl.IntrestRate = dt.Rows[index]["INTREST_RATE"].ToString();
        //            AdvLonStpMdl.InstallmentAmount = dt.Rows[index]["INST_AMT"].ToString();
        //            AdvLonStpMdl.NumberOfInstallment = dt.Rows[index]["INSTNO"].ToString();
        //            if (dt.Rows[index]["IsDeductFromSalary"].ToString() == "Y")
        //            {
        //                DeductFromSalary = "True";
        //            }
        //            else
        //            {
        //                DeductFromSalary = "False";
        //            }
        //            AdvLonStpMdl.DeductFromSalary = DeductFromSalary;
        //            AdvLonStpMdl.CreatedDate = Convert.ToDateTime(dt.Rows[index]["CreatedDate"].ToString());
        //            AdvLonStpMdl.LastModifiedBy = dt.Rows[index]["LastModifiedBy"].ToString();
        //            AdvLonStpMdl.LastModifiedDate = Convert.ToDateTime(dt.Rows[index]["LastModifiedDate"].ToString());
        //        }
        //    }
        //    return AdvanceLoanSetup;
        //}

        //To View Employee and ID Wise Advance Loan details    
        //public IEnumerable<DataMaintenanceModel> BindEmployeeAdvanceLoanListIDWise(string EmpCode , string IdentityNumber)
        //{
        //    string DeductFromSalary = "";
        //    DateTime date = System.DateTime.Now;
        //    List<DataMaintenanceModel> lstBindEmployeeAdvance = new List<DataMaintenanceModel>();
        //    DataTable dt = DataOperation.GetDataTableWithParameter("Payroll_tblAdvanceLoanData_Employee_IdentityNumber_Record", new SqlParameter("PayCode", EmpCode.Trim()), new SqlParameter("IDNO", IdentityNumber.Trim()));
        //    if (dt.Rows.Count > 0)
        //    {
        //        for (int index = 0; index < dt.Rows.Count; index++)
        //        {
        //            DataMaintenanceModel AdvLonStpMdl = new DataMaintenanceModel();
        //            AdvLonStpMdl.AdvanceMonthDate = dt.Rows[index]["MON_YEAR"].ToString();
        //            AdvLonStpMdl.NumberOfInstallment = dt.Rows[index]["INSTNO"].ToString();
        //            AdvLonStpMdl.InstallmentAmount = dt.Rows[index]["INST_AMT"].ToString();
        //            AdvLonStpMdl.PaidAmountPerMonth = dt.Rows[index]["PAMT"].ToString();
        //            AdvLonStpMdl.IntrestRate = dt.Rows[index]["IAMT"].ToString();
        //            AdvLonStpMdl.BalanceAmount = dt.Rows[index]["BALANCE_AMT"].ToString();
        //            if (dt.Rows[index]["IsDeductFromSalary"].ToString() == "Y")
        //            {
        //                DeductFromSalary = "True";
        //            }
        //            else
        //            {
        //                DeductFromSalary = "False";
        //            }
        //            AdvLonStpMdl.DeductFromSalary = DeductFromSalary;
        //            AdvLonStpMdl.CreatedDate = Convert.ToDateTime(dt.Rows[index]["CreatedDate"].ToString());
        //            AdvLonStpMdl.LastModifiedBy = dt.Rows[index]["LastModifiedBy"].ToString();
        //            AdvLonStpMdl.LastModifiedDate = Convert.ToDateTime(dt.Rows[index]["LastModifiedDate"].ToString());
        //            lstBindEmployeeAdvance.Add(AdvLonStpMdl);
        //        }
        //    }
        //    return lstBindEmployeeAdvance;
        //}

        internal static class SimulateVal
        {
            internal static double Val(string expression)
            {
                if (expression == null)
                    return 0;

                for (int size = expression.Length; size > 0; size--)
                {
                    double testDouble;
                    if (double.TryParse(expression.Substring(0, size), out testDouble))
                        return testDouble;
                }
                return 0;
            }
            internal static double Val(object expression)
            {
                if (expression == null)
                    return 0;

                double testDouble;
                if (double.TryParse(expression.ToString(), out testDouble))
                    return testDouble;

                bool testBool;
                if (bool.TryParse(expression.ToString(), out testBool))
                    return testBool ? -1 : 0;

                System.DateTime testDate;
                if (System.DateTime.TryParse(expression.ToString(), out testDate))
                    return testDate.Day;

                return 0;
            }
            internal static int Val(char expression)
            {
                int testInt;
                if (int.TryParse(expression.ToString(), out testInt))
                    return testInt;
                else
                    return 0;
            }
        }

    }
}