﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;
using IntegratedAttendanceSystem_API.DataAccess;

namespace IntegratedAttendanceSystem_API.Models
{
    public class CategoryAccessModel
    {
        public int IsCheckIsNumberExpID()
        {
            int res = 0;
            DataTable dt = DataOperation.GetDataTableWithoutParameter("TblCategoryMaster_CheckIsNumberExpID");
            if (dt.Rows.Count > 0)
            {
                for (int index = 0; index < dt.Rows.Count; index++)
                {
                    res = Convert.ToInt32(dt.Rows[index]["cnt"].ToString());
                }
            }
            return res;
        }
        //To View all Category details    
        public IEnumerable<CategoryModel> GetAllCategory()
        {
            DateTime date = System.DateTime.Now;
            List<CategoryModel> lstCategory = new List<CategoryModel>();
            DataTable dt = DataOperation.GetDataTableWithoutParameter("TblCategoryMaster_AllRec");
            if (dt.Rows.Count > 0)
            {
                for (int index = 0; index < dt.Rows.Count; index++)
                {
                    CategoryModel CateModel = new CategoryModel();
                    CateModel.IsActive = dt.Rows[index]["IsActive"].ToString();
                    CateModel.CategoryCode = dt.Rows[index]["CategoryCode"].ToString();
                    CateModel.CategoryName = dt.Rows[index]["CategoryName"].ToString();
                    CateModel.CreatedDate = Convert.ToDateTime(dt.Rows[index]["CreatedDate"].ToString());
                    CateModel.LastModifiedBy = dt.Rows[index]["LastModifiedBy"].ToString();
                    CateModel.LastModifiedDate = Convert.ToDateTime(dt.Rows[index]["LastModifiedDate"].ToString());
                    lstCategory.Add(CateModel);
                }
            }
            return lstCategory;
        }
        //To Add new Category record    
        public int Add(CategoryModel CateMst)
        {
            int res = DataOperation.InsUpdDel("TblCategoryMaster_Add",
                new SqlParameter("IsActive", CateMst.IsActive),
                new SqlParameter("CategoryCode", CateMst.CategoryCode),
                new SqlParameter("CategoryName", CateMst.CategoryName),
                new SqlParameter("LastModifiedBy", CateMst.LastModifiedBy.Trim()));
            if (res == 1)
            {
                CategoryModel oldobject = GetCategoryData(CateMst.CategoryCode);
                AuditLog.CreateAuditTrail(CategoryModel.CATEGORYMASTER, CategoryModel.CATEGORYCODE, CateMst.CategoryCode, AppConstant.CREATED, CateMst.LastModifiedBy, CateMst.LoginTerminalNameIP, oldobject, CateMst);
            }
            return res;
        }
        //To Update the records of a particluar Category 
        public int Update(CategoryModel CateMst)
        {
            CategoryModel objgetexpmast = GetCategoryData(CateMst.CategoryCode);
            int res = DataOperation.InsUpdDel("TblCategoryMaster_Upd",
                new SqlParameter("IsActive", CateMst.IsActive),
                new SqlParameter("CategoryCode", CateMst.CategoryCode),
                new SqlParameter("CategoryName", CateMst.CategoryName),
                new SqlParameter("LastModifiedBy", CateMst.LastModifiedBy.Trim()));
            if (res == 1)
            {
                CategoryCloneModel objAuditold = new CategoryCloneModel();
                CategoryCloneModel objAuditnew = new CategoryCloneModel();
                //code for clone copy create for old object
                objAuditold.IsActive = objgetexpmast.IsActive;
                objAuditold.CategoryCode = objgetexpmast.CategoryCode;
                objAuditold.CategoryName = objgetexpmast.CategoryName;
                //code for clone copy create for new  object
                objAuditnew.IsActive = CateMst.IsActive;
                objAuditnew.CategoryCode = CateMst.CategoryCode;
                objAuditnew.CategoryName = CateMst.CategoryName;
               
                //objAuditnew.LastModifiedBy = Convert.ToString(HttpContext.Current.Session["UserName"]);
                //objAuditnew.LastModifiedDate = DateTime.Now;
                AuditLog.CreateAuditTrail(CategoryModel.CATEGORYMASTER, CategoryModel.CATEGORYCODE, CateMst.CategoryCode, AppConstant.MODIFIED, CateMst.LastModifiedBy, CateMst.LoginTerminalNameIP, objAuditold, objAuditnew);
            }
            return res;
        }
        //Get the details of a particular Category Data  
        public CategoryModel GetCategoryData(string CategoryCode)
        {

            CategoryModel CateMstMod = new CategoryModel();
            DataTable dt = DataOperation.GetDataTableWithParameter("TblCategoryMaster_GetCategoryData", new SqlParameter("CategoryCode", CategoryCode.Trim()));
            if (dt.Rows.Count > 0)
            {
                for (int index = 0; index < dt.Rows.Count; index++)
                {
                    CateMstMod.IsActive = dt.Rows[index]["IsActive"].ToString();
                    CateMstMod.CategoryCode = dt.Rows[index]["CategoryCode"].ToString();
                    CateMstMod.CategoryName = dt.Rows[index]["CategoryName"].ToString();
                    CateMstMod.CreatedDate = Convert.ToDateTime(dt.Rows[index]["CreatedDate"].ToString());
                    CateMstMod.LastModifiedBy = dt.Rows[index]["LastModifiedBy"].ToString();
                    CateMstMod.LastModifiedDate = Convert.ToDateTime(dt.Rows[index]["LastModifiedDate"].ToString());
                }
            }
            return CateMstMod;
        }
        //To Delete the record on a particular  Category  
        public int Deleteold(string CategoryCode)
        {
            string oldCode = CategoryCode;
            CategoryModel oldobject = GetCategoryData(oldCode.Trim());
            CategoryCloneModel objAuditold = new CategoryCloneModel();
            CategoryCloneModel objAuditnew = new CategoryCloneModel();
            int res = DataOperation.InsUpdDel("TblCategoryMaster_Del", new SqlParameter("CategoryCode", CategoryCode.Trim()));
            return res;
        }
        public int Delete(CategoryModel CateMst)
        {
            int res = -1;
            CategoryCloneModel objAuditold = new CategoryCloneModel();
            CategoryCloneModel objAuditnew = new CategoryCloneModel();
            CategoryModel objgetexpmast = GetCategoryData(CateMst.CategoryCode.Trim());
            //code for clone copy create for old object
            objAuditold.IsActive = objgetexpmast.IsActive;
            objAuditold.CategoryCode = objgetexpmast.CategoryCode;
            objAuditold.CategoryName = objgetexpmast.CategoryName;
            
            //code for clone copy create for new  object
            objAuditnew.IsActive = "";
            objAuditnew.CategoryCode = "";
            objAuditnew.CategoryName = "";
           
            res = DataOperation.InsUpdDel("TblCategoryMaster_Del", new SqlParameter("CategoryCode", CateMst.CategoryCode.Trim()));
            if (res == 1)
            {
                AuditLog.CreateAuditTrail(CategoryModel.CATEGORYMASTER, CategoryModel.CATEGORYCODE, CateMst.CategoryCode.Trim(), AppConstant.DELETED, CateMst.LastModifiedBy, CateMst.LoginTerminalNameIP, objAuditold, objAuditnew);
            }
            // }
            return res;
        }
        //Get the details of a particular  Category Code  
        public int GetCategoryCode(string Code)
        {

            int res = 0;
            DataTable dt = DataOperation.GetDataTableWithParameter("TblCategoryMaster_CheckIsCode", new SqlParameter("CategoryCode", Code.Trim()));
            if (dt.Rows.Count > 0)
            {
                for (int index = 0; index < dt.Rows.Count; index++)
                {
                    res = Convert.ToInt32(dt.Rows[index]["cnt"].ToString());
                }
            }
            return res;
        }

        public List<CategoryModel> ListAllDesignation()
        {
            DateTime date = System.DateTime.Now;
            List<CategoryModel> lstCat = new List<CategoryModel>();
            DataTable dt = DataOperation.GetDataTableWithoutParameter("TblCategory_SelectAll");
            if (dt.Rows.Count > 0)
            {
                for (int index = 0; index < dt.Rows.Count; index++)
                {
                    CategoryModel CategoryMdl = new CategoryModel();
                    CategoryMdl.CategoryCode = dt.Rows[index]["CategoryCode"].ToString();
                    CategoryMdl.CategoryName = dt.Rows[index]["CategoryName"].ToString();
                    lstCat.Add(CategoryMdl);
                }
            }
            return lstCat;
        }
    }
}