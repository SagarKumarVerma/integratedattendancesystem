﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IntegratedAttendanceSystem_API.Models
{
    public class CategoryCloneModel
    {
        public string IsActive { get; set; }
        public string CategoryCode { get; set; }
        public string CategoryName { get; set; }
    }
}