﻿using IntegratedAttendanceSystem_API.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IntegratedAttendanceSystem_API.Models
{
    public class ShiftMasterModel
    {
        public List<Shift_list> GetShiftList()
        {
            List<Shift_list> Shift = new List<Shift_list>();
            DataTable dt = DataOperation.GetDataTableWithoutParameter("tblShiftmaster_SelectAll");
            if (dt.Rows.Count > 0)
            {
                for (int index = 0; index < dt.Rows.Count; index++)
                {
                    Shift.Add(new Shift_list
                    {
                        ShiftCode = Convert.ToString(dt.Rows[index]["Shift"]).Trim(),
                        ShiftName = Convert.ToString(dt.Rows[index]["ShiftName"]).Trim(),
                    });
                }
            }
            return Shift;
        }
    }
}