﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IntegratedAttendanceSystem_API.Models
{
    public class GradeCloneModel
    {
        public string IsActive { get; set; }
        public string GradeCode { get; set; }
        public string GradeName { get; set; }
        public string CompanyCode { get; set; }
        public string CreatedDate { get; set; }
        public string LastModifiedBy { get; set; }
        public string LastModifiedDate { get; set; }
        public string LoginTerminalNameIP { get; set; }
        public string AutoGeneratedFlag { get; set; }
    }
}