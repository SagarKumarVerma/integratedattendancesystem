﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;
using IntegratedAttendanceSystem_API.DataAccess;

namespace IntegratedAttendanceSystem_API.Models
{
    public class HODAccessModel
    {
        public int IsCheckIsNumberExpID()
        {
            int res = 0;
            DataTable dt = DataOperation.GetDataTableWithoutParameter("TblHODMaster_CheckIsNumberExpID");
            if (dt.Rows.Count > 0)
            {
                for (int index = 0; index < dt.Rows.Count; index++)
                {
                    res = Convert.ToInt32(dt.Rows[index]["cnt"].ToString());
                }
            }
            return res;
        }
        //To View all HOD details    
        public IEnumerable<HODModel> GetAllHOD()
        {
            DateTime date = System.DateTime.Now;
            List<HODModel> lstHOD = new List<HODModel>();
            DataTable dt = DataOperation.GetDataTableWithoutParameter("TblHODMaster_AllRec");
            if (dt.Rows.Count > 0)
            {
                for (int index = 0; index < dt.Rows.Count; index++)
                {
                    HODModel GrdModel = new HODModel();
                    
                    GrdModel.HODCode = dt.Rows[index]["HODCode"].ToString();
                    GrdModel.HODName = dt.Rows[index]["HODName"].ToString();
                    GrdModel.HODMail = dt.Rows[index]["HODMail"].ToString();
                    GrdModel.Paycode = dt.Rows[index]["Paycode"].ToString();
                    GrdModel.CreatedDate = dt.Rows[index]["CreatedDate"].ToString(); // Convert.ToDateTime(dt.Rows[index]["CreatedDate"].ToString());
                    GrdModel.LastModifiedBy = dt.Rows[index]["LastModifiedBy"].ToString();
                    GrdModel.LastModifiedDate = dt.Rows[index]["LastModifiedDate"].ToString(); // Convert.ToDateTime(dt.Rows[index]["LastModifiedDate"].ToString());
                    lstHOD.Add(GrdModel);
                }
            }
            return lstHOD;
        }
        //To Add new HOD record    
        public int Add(HODModel GrdMst)
        {
            int res = DataOperation.InsUpdDel("TblHODMaster_Add",
                
                new SqlParameter("HODCode", GrdMst.HODCode),
                new SqlParameter("HODName", GrdMst.HODName),
                new SqlParameter("HODMail", GrdMst.HODMail),
                new SqlParameter("Paycode", GrdMst.Paycode),
                new SqlParameter("LastModifiedBy", GrdMst.LastModifiedBy.Trim()));
            if (res == 1)
            {
                HODModel oldobject = GetHODData(GrdMst.HODCode);
                AuditLog.CreateAuditTrail(HODModel.HODMASTER, HODModel.HODCODE, GrdMst.HODCode, AppConstant.CREATED, GrdMst.LastModifiedBy, GrdMst.LoginTerminalNameIP, oldobject, GrdMst);
            }
            return res;
        }
        //To Update the records of a particluar HOD 
        public int Update(HODModel GrdMst)
        {
            HODModel objgetexpmast = GetHODData(GrdMst.HODCode);
            int res = DataOperation.InsUpdDel("TblHODMaster_Upd",                
                new SqlParameter("HODCode", GrdMst.HODCode),
                new SqlParameter("HODName", GrdMst.HODName),
                new SqlParameter("Paycode", GrdMst.Paycode),
                new SqlParameter("HODMail", GrdMst.HODMail),
                new SqlParameter("LastModifiedBy", GrdMst.LastModifiedBy.Trim()));
            if (res == 1)
            {
                HODCloneModel objAuditold = new HODCloneModel();
                HODCloneModel objAuditnew = new HODCloneModel();

                //code for clone copy create for old object
                
                objAuditold.HODCode = objgetexpmast.HODCode;
                objAuditold.HODName = objgetexpmast.HODName;
                objAuditold.Paycode = objgetexpmast.Paycode;
                objAuditold.HODMail = objgetexpmast.HODMail;

                //code for clone copy create for new  object
                
                objAuditnew.HODCode = GrdMst.HODCode;
                objAuditnew.HODName = GrdMst.HODName;
                objAuditnew.Paycode = GrdMst.Paycode;
                objAuditnew.HODMail = GrdMst.HODMail;

                //objAuditnew.LastModifiedBy = Convert.ToString(HttpContext.Current.Session["UserName"]);
                //objAuditnew.LastModifiedDate = DateTime.Now;
                AuditLog.CreateAuditTrail(HODModel.HODMASTER, HODModel.HODCODE, GrdMst.HODCode, AppConstant.MODIFIED, GrdMst.LastModifiedBy, GrdMst.LoginTerminalNameIP, objAuditold, objAuditnew);
            }
            return res;
        }
        //Get the details of a particular HOD Data  
        public HODModel GetHODData(string HODCode)
        {
            HODModel GrdMstMod = new HODModel();
            DataTable dt = DataOperation.GetDataTableWithParameter("TblHODMaster_GetHODData", new SqlParameter("HODCode", HODCode.Trim()));
            if (dt.Rows.Count > 0)
            {
                for (int index = 0; index < dt.Rows.Count; index++)
                {                    
                    GrdMstMod.HODCode = dt.Rows[index]["HODCode"].ToString().Trim();
                    GrdMstMod.HODName = dt.Rows[index]["HODName"].ToString().Trim();
                    GrdMstMod.Paycode = dt.Rows[index]["Paycode"].ToString().Trim();
                    GrdMstMod.HODMail = dt.Rows[index]["HODMail"].ToString().Trim();
                    GrdMstMod.CreatedDate = dt.Rows[index]["CreatedDate"].ToString().Trim(); // Convert.ToDateTime(dt.Rows[index]["CreatedDate"].ToString());
                    GrdMstMod.LastModifiedBy = dt.Rows[index]["LastModifiedBy"].ToString();
                    GrdMstMod.LastModifiedDate = dt.Rows[index]["LastModifiedDate"].ToString().Trim(); // Convert.ToDateTime(dt.Rows[index]["LastModifiedDate"].ToString());
                }
            }
            return GrdMstMod;
        }
        //To Delete the record on a particular  HOD  
        public int Deleteold(string HODCode)
        {
            string oldCode = HODCode;
            HODModel oldobject = GetHODData(oldCode.Trim());
            HODCloneModel objAuditold = new HODCloneModel();
            HODCloneModel objAuditnew = new HODCloneModel();
            int res = DataOperation.InsUpdDel("TblHODMaster_Del", new SqlParameter("HODCode", HODCode.Trim()));
            return res;
        }
        public int Delete(HODModel GrdMst)
        {
            int res = -1;
            HODCloneModel objAuditold = new HODCloneModel();
            HODCloneModel objAuditnew = new HODCloneModel();
            HODModel objgetexpmast = GetHODData(GrdMst.HODCode.Trim());
            //code for clone copy create for old object
            //objAuditold.IsActive = objgetexpmast.IsActive;
            objAuditold.HODCode = objgetexpmast.HODCode;
            objAuditold.HODName = objgetexpmast.HODName;
            objAuditold.Paycode = objgetexpmast.Paycode;
            objAuditold.HODMail = objgetexpmast.HODMail;
            
            //code for clone copy create for new  object
            //objAuditnew.IsActive = "";
            objAuditnew.HODCode = "";
            objAuditnew.HODName = "";
            objAuditnew.Paycode = "";
            objAuditnew.HODMail = "";
                       
            res = DataOperation.InsUpdDel("TblHODMaster_Del", new SqlParameter("HODCode", GrdMst.HODCode.Trim()));
            if (res == 1)
            {
                AuditLog.CreateAuditTrail(HODModel.HODMASTER, HODModel.HODCODE, GrdMst.HODCode.Trim(), AppConstant.DELETED, GrdMst.LastModifiedBy, GrdMst.LoginTerminalNameIP, objAuditold, objAuditnew);
            }
            // }
            return res;
        }
        //Get the details of a particular  HOD Code  
        public int GetHODCode(string Code)
        {

            int res = 0;
            DataTable dt = DataOperation.GetDataTableWithParameter("TblHODMaster_CheckIsCode", new SqlParameter("HODCode", Code.Trim()));
            if (dt.Rows.Count > 0)
            {
                for (int index = 0; index < dt.Rows.Count; index++)
                {
                    res = Convert.ToInt32(dt.Rows[index]["cnt"].ToString());
                }
            }
            return res;
        }

        public IEnumerable<DataMaintenanceModel.EmployeeCode_list> GetAllEmployeeCode()
        {
            List<DataMaintenanceModel.EmployeeCode_list> lstEmployeeCode = new List<DataMaintenanceModel.EmployeeCode_list>();
            DataTable dt = DataOperation.GetDataTableWithoutParameter("tblEmployeeMaster_Details_DDL");
            if (dt.Rows.Count > 0)
            {
                for (int index = 0; index < dt.Rows.Count; index++)
                {
                    lstEmployeeCode.Add(new DataMaintenanceModel.EmployeeCode_list
                    {
                        EmployeeCode = Convert.ToString(dt.Rows[index]["Paycode"]).Trim(),
                        EmployeeName = Convert.ToString(dt.Rows[index]["EmpName"]).Trim()
                    });
                }
            }
            return lstEmployeeCode;
        }


    }
}