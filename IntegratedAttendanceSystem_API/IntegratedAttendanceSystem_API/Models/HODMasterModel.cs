﻿using IntegratedAttendanceSystem_API.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IntegratedAttendanceSystem_API.Models
{
    public class HODMasterModel
    {
        public List<HOD_list> GetHODList()
        {
            List<HOD_list> hod = new List<HOD_list>();
            DataTable dt = DataOperation.GetDataTableWithoutParameter("TblHOD_SelectAll");
            if (dt.Rows.Count > 0)
            {
                for (int index = 0; index < dt.Rows.Count; index++)
                {
                    hod.Add(new HOD_list
                    {
                        HODCode = Convert.ToString(dt.Rows[index]["HODCode"]).Trim(),
                        HODName = Convert.ToString(dt.Rows[index]["HODName"]).Trim(),
                    });
                }
            }
            return hod;
        }
    }
}