﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;
using IntegratedAttendanceSystem_API.DataAccess;


namespace IntegratedAttendanceSystem_API.Models
{
    public class ShiftAccessModel
    {
        public int IsCheckIsNumberExpID()
        {
            int res = 0;
            DataTable dt = DataOperation.GetDataTableWithoutParameter("TblShiftMaster_CheckIsNumberExpID");
            if (dt.Rows.Count > 0)
            {
                for (int index = 0; index < dt.Rows.Count; index++)
                {
                    res = Convert.ToInt32(dt.Rows[index]["cnt"].ToString());
                }
            }
            return res;
        }
        //To View all Shift details    
        public IEnumerable<ShiftModel> GetAllShift(string CompanyCode, string UserType)
        {
            DateTime date = System.DateTime.Now;
            List<ShiftModel> lstShift = new List<ShiftModel>();
            ShiftModel SM = new ShiftModel();
            DataTable dt;
            if (UserType == "Administrator")
                dt = DataOperation.GetDataTableWithoutParameter("TblShiftMaster_Admin_AllRec");
            else
                dt = DataOperation.GetDataTableWithoutParameter("TblShiftMaster_AllRec");

            if (dt.Rows.Count > 0)
            {
                for (int index = 0; index < dt.Rows.Count; index++)
                {
                    ShiftModel Shift = new ShiftModel();
                    //Shift.IsActive = dt.Rows[index]["IsActive"].ToString();
                    Shift.SHIFT = dt.Rows[index]["SHIFT"].ToString();
                    Shift.ShiftDiscription = dt.Rows[index]["ShiftDiscription"].ToString();
                    Shift.CompanyCode = dt.Rows[index]["CompanyCode"].ToString();
                    Shift.ShiftStartTime = dt.Rows[index]["StartTime"].ToString();
                    Shift.ShiftEndTime = dt.Rows[index]["EndTime"].ToString(); //dt.Rows[index]["EndTime"].ToString();
                    Shift.ShiftHours = dt.Rows[index]["SHIFTDURATION"].ToString(); // Convert.ToInt32(dt.Rows[index]["SHIFTDURATION"].ToString());
                    Shift.LunchStartTime = dt.Rows[index]["LUNCHTIME"].ToString(); //dt.Rows[index]["LUNCHTIME"].ToString();
                    Shift.LunchDuration = dt.Rows[index]["LunchDuration"].ToString();//Convert.ToInt32(dt.Rows[index]["LunchDuration"].ToString());
                    Shift.LunchEndTime = dt.Rows[index]["LunchEndTime"].ToString(); //dt.Rows[index]["LunchEndTime"].ToString();
                    Shift.LunchDeduction = dt.Rows[index]["LunchDeduction"].ToString(); // Convert.ToInt32(dt.Rows[index]["LunchDeduction"].ToString());  //dt.Rows[index]["LunchDeduction"].ToString();
                    Shift.Overtimededuction = dt.Rows[index]["OTDEDUCTHRS"].ToString(); //Convert.ToInt32(dt.Rows[index]["OTDEDUCTHRS"].ToString());
                    Shift.Overtimedeductafter = dt.Rows[index]["OTDEDUCTAFTER"].ToString(); //float.Parse(dt.Rows[index]["OTDEDUCTAFTER"].ToString());
                    Shift.Overtimestartafter = dt.Rows[index]["OTSTARTAFTER"].ToString(); //float.Parse(dt.Rows[index]["OTSTARTAFTER"].ToString());
                    Shift.ShiftPosition = dt.Rows[index]["ShiftPosition"].ToString();

                    //Shift.CreatedDate = Convert.ToDateTime(dt.Rows[index]["CreatedDate"].ToString());
                    //Shift.LastModifiedBy = dt.Rows[index]["LastModifiedBy"].ToString();
                    //Shift.LastModifiedDate = Convert.ToDateTime(dt.Rows[index]["LastModifiedDate"].ToString());

                    lstShift.Add(Shift);
                }
            }
            return lstShift;
        }
        //To Add new Shift record    
        public int Add(ShiftModel Shift)
        {
            Shift.CompanyCode = (String.IsNullOrEmpty(Shift.CompanyCode) ? "" : Shift.CompanyCode);
            Shift.SHIFT = (String.IsNullOrEmpty(Shift.SHIFT) ? "" : Shift.SHIFT);
            Shift.ShiftDiscription = (String.IsNullOrEmpty(Shift.ShiftDiscription) ? "" : Shift.ShiftDiscription);
            Shift.ShiftStartTime = Shift.ShiftStartTime;
            Shift.ShiftEndTime = Shift.ShiftEndTime;
            Shift.ShiftHours = Shift.ShiftHours;
            Shift.LunchStartTime = Shift.LunchStartTime;
            Shift.LunchDuration = Shift.LunchDuration;
            Shift.LunchEndTime = Shift.LunchEndTime;
            Shift.LunchDeduction = Shift.LunchDeduction;

            Shift.Overtimedeductafter = Shift.Overtimedeductafter;
            Shift.Overtimestartafter = Shift.Overtimestartafter;
            //Shift.Overtimededuction = Shift.Overtimededuction;
            Shift.Overtimededuction = (String.IsNullOrEmpty(Shift.Overtimededuction) ? "0" : Shift.ShiftPosition);
            Shift.ShiftPosition = (String.IsNullOrEmpty(Shift.ShiftPosition) ? "" : Shift.ShiftPosition);
            Shift.LastModifiedBy = (String.IsNullOrEmpty(Shift.LastModifiedBy) ? "" : Shift.LastModifiedBy);

            int res = DataOperation.InsUpdDel("TblShiftMaster_Add",
                new SqlParameter("CompanyCode", Shift.CompanyCode),
                new SqlParameter("SHIFT", Shift.SHIFT),
                new SqlParameter("ShiftDiscription", Shift.ShiftDiscription),
                new SqlParameter("StartTime", Shift.ShiftStartTime),
                new SqlParameter("EndTime", Shift.ShiftEndTime),
                new SqlParameter("SHIFTDURATION", Shift.ShiftHours),
                new SqlParameter("LUNCHTIME", Shift.LunchStartTime),
                new SqlParameter("LunchDuration", Shift.LunchDuration),
                new SqlParameter("LunchEndTime", Shift.LunchEndTime),
                new SqlParameter("LunchDeduction", Shift.LunchDeduction),
                new SqlParameter("OTDEDUCTAFTER", Shift.Overtimedeductafter),
                new SqlParameter("OTSTARTAFTER", Shift.Overtimestartafter),
                new SqlParameter("OTDEDUCTHRS", Shift.Overtimededuction),
                new SqlParameter("ShiftPosition", Shift.ShiftPosition.Trim()),
                new SqlParameter("LastModifiedBy", Shift.LastModifiedBy)
                );
            if (res == 1)
            {
                ShiftModel oldobject = GetShiftData(Shift.SHIFT, Shift.CompanyCode);
                AuditLog.CreateAuditTrail(ShiftModel.SHIFTMASTER, ShiftModel.SHIFTCODE, Shift.SHIFT, AppConstant.CREATED, Shift.LastModifiedBy, Shift.LoginTerminalNameIP, oldobject, Shift);
            }
            return res;
        }
        //To Update the records of a particluar  Shift  
        public int Update(ShiftModel Shift)
        {
            Shift.CompanyCode = (String.IsNullOrEmpty(Shift.CompanyCode) ? "" : Shift.CompanyCode);
            Shift.SHIFT = (String.IsNullOrEmpty(Shift.SHIFT) ? "" : Shift.SHIFT);
            Shift.ShiftStartTime = Shift.ShiftStartTime;
            Shift.ShiftEndTime = Shift.ShiftEndTime;
            Shift.ShiftHours = Shift.ShiftHours;
            Shift.LunchStartTime = Shift.LunchStartTime;
            //Shift.LunchDuration = Shift.LunchDuration;
            Shift.LunchDuration = (String.IsNullOrEmpty(Shift.LunchDuration) ? "00:00" : Shift.LunchDuration);
            Shift.LunchEndTime = Shift.LunchEndTime;
            Shift.LunchDeduction = Shift.LunchDeduction;

            //Shift.Overtimedeductafter = Shift.Overtimedeductafter;
            //Shift.Overtimestartafter = Shift.Overtimestartafter;
            //Shift.Overtimededuction = Shift.Overtimededuction;

            Shift.Overtimedeductafter = (String.IsNullOrEmpty(Shift.Overtimedeductafter) ? "0" : Shift.Overtimedeductafter);
            Shift.Overtimestartafter = (String.IsNullOrEmpty(Shift.Overtimestartafter) ? "0" : Shift.Overtimestartafter);
            Shift.Overtimededuction = (String.IsNullOrEmpty(Shift.Overtimededuction) ? "0" : Shift.Overtimededuction);
            Shift.ShiftPosition = (String.IsNullOrEmpty(Shift.ShiftPosition) ? "" : Shift.ShiftPosition);

            ShiftModel objgetexpmast = GetShiftData(Shift.SHIFT, Shift.CompanyCode);
            int res = DataOperation.InsUpdDel("TblShiftMaster_Upd",
               new SqlParameter("CompanyCode", Shift.CompanyCode.Trim()),
                new SqlParameter("Shift", Shift.SHIFT.Trim()),
                new SqlParameter("ShiftDiscription", Shift.ShiftDiscription.Trim()),
                new SqlParameter("StartTime", Shift.ShiftStartTime.Trim()),
                new SqlParameter("EndTime", Shift.ShiftEndTime.Trim()),
                new SqlParameter("SHIFTDURATION", Shift.ShiftHours.Trim()),
                new SqlParameter("LUNCHTIME", Shift.LunchStartTime.Trim()),
                new SqlParameter("LunchDuration", Shift.LunchDuration.Trim()),
                new SqlParameter("LunchEndTime", Shift.LunchEndTime.Trim()),
                new SqlParameter("LunchDeduction", Shift.LunchDeduction.Trim()),
                new SqlParameter("OTSTARTAFTER", Shift.Overtimedeductafter.Trim()),
                new SqlParameter("OTDEDUCTAFTER", Shift.Overtimestartafter.Trim()),
                new SqlParameter("OTDEDUCTHRS", Shift.Overtimededuction.Trim()),
                new SqlParameter("ShiftPosition", Shift.ShiftPosition.Trim()),
                new SqlParameter("LastModifiedBy", Shift.LastModifiedBy.Trim()));
            if (res == 1)
            {
                ShiftCloneModel objAuditold = new ShiftCloneModel();
                ShiftCloneModel objAuditnew = new ShiftCloneModel();

                //code for clone copy create for old object
                //if (objAuditold.IsActive == "True")
                //{
                //    objAuditold.IsActive = "Y";
                //}
                //else
                //{
                //    objAuditold.IsActive = "N";
                //}
                objAuditold.CompanyCode = objgetexpmast.CompanyCode;
                objAuditold.SHIFT = objgetexpmast.SHIFT;
                objAuditold.ShiftDiscription = objgetexpmast.ShiftDiscription;
                objAuditold.ShiftStartTime = objgetexpmast.ShiftStartTime;
                objAuditold.ShiftEndTime = objgetexpmast.ShiftEndTime;
                objAuditold.ShiftHours = objgetexpmast.ShiftHours;
                objAuditold.LunchStartTime = objgetexpmast.LunchStartTime;
                objAuditold.LunchDuration = objgetexpmast.LunchDuration;
                objAuditold.LunchEndTime = objgetexpmast.LunchEndTime;
                objAuditold.LunchDeduction = objgetexpmast.LunchDeduction;
                objAuditold.Overtimedeductafter = objgetexpmast.Overtimedeductafter;
                objAuditold.Overtimestartafter = objgetexpmast.Overtimestartafter;
                objAuditold.Overtimededuction = objgetexpmast.Overtimededuction;
                objAuditold.ShiftPosition = objgetexpmast.ShiftPosition;
                objAuditold.LastModifiedBy = objgetexpmast.LastModifiedBy;

                //code for clone copy create for new  object
                objAuditold.CompanyCode = Shift.CompanyCode;
                objAuditold.SHIFT = Shift.SHIFT;
                objAuditold.ShiftStartTime = Shift.ShiftStartTime;
                objAuditold.ShiftEndTime = Shift.ShiftEndTime;
                objAuditold.ShiftHours = Shift.ShiftHours;
                objAuditold.LunchStartTime = Shift.LunchStartTime;
                objAuditold.LunchDuration = Shift.LunchDuration;
                objAuditold.LunchEndTime = Shift.LunchEndTime;
                objAuditold.LunchDeduction = Shift.LunchDeduction;
                objAuditold.Overtimedeductafter = Shift.Overtimedeductafter;
                objAuditold.Overtimestartafter = Shift.Overtimestartafter;
                objAuditold.Overtimededuction = Shift.Overtimededuction;
                objAuditold.ShiftPosition = Shift.ShiftPosition;
                objAuditold.LastModifiedBy = Shift.LastModifiedBy;
                //objAuditnew.LastModifiedBy = Convert.ToString(HttpContext.Current.Session["UserName"]);
                //objAuditnew.LastModifiedDate = DateTime.Now;
                AuditLog.CreateAuditTrail(ShiftModel.SHIFTMASTER, ShiftModel.SHIFTCODE, Shift.SHIFT, AppConstant.MODIFIED, Shift.LastModifiedBy, Shift.LoginTerminalNameIP, objAuditold, objAuditnew);
            }
            return res;
        }
        //Get the details of a particular  Shift Data  
        public ShiftModel GetShiftData(string Code, string Companycode)
        {
            ShiftModel Shift = new ShiftModel();
            DataTable dt = DataOperation.GetDataTableWithParameter("TblShiftMaster_GetData", new SqlParameter("SHIFT", Code.Trim()), new SqlParameter("CompanyCode", Companycode.Trim()));
            if (dt.Rows.Count > 0)
            {
                for (int index = 0; index < dt.Rows.Count; index++)
                {
                    //if (dt.Rows[index]["IsActive"].ToString() == "Y")
                    //{
                    //    Shift.IsActive = "True";
                    //}
                    //else
                    //{
                    //    Shift.IsActive = "False";
                    //}

                    Shift.SHIFT = dt.Rows[index]["SHIFT"].ToString().Trim();
                    Shift.ShiftDiscription = dt.Rows[index]["ShiftDiscription"].ToString().Trim();
                    Shift.CompanyCode = dt.Rows[index]["CompanyCode"].ToString().Trim();
                    Shift.ShiftStartTime = dt.Rows[index]["StartTime"].ToString().Trim();
                    Shift.ShiftEndTime = dt.Rows[index]["EndTime"].ToString().Trim(); //dt.Rows[index]["EndTime"].ToString();
                    Shift.ShiftHours = dt.Rows[index]["SHIFTDURATION"].ToString().Trim(); // Convert.ToInt32(dt.Rows[index]["SHIFTDURATION"].ToString());
                    Shift.LunchStartTime = dt.Rows[index]["LUNCHTIME"].ToString().Trim(); //dt.Rows[index]["LUNCHTIME"].ToString();
                    Shift.LunchDuration = dt.Rows[index]["LunchDuration"].ToString().Trim(); // Convert.ToInt32(dt.Rows[index]["LunchDuration"].ToString());
                    Shift.LunchEndTime = dt.Rows[index]["LunchEndTime"].ToString().Trim(); //dt.Rows[index]["LunchEndTime"].ToString();
                    Shift.LunchDeduction = dt.Rows[index]["LunchDeduction"].ToString().Trim(); // Convert.ToInt32(dt.Rows[index]["LunchDeduction"].ToString());  //dt.Rows[index]["LunchDeduction"].ToString();
                    Shift.Overtimededuction = dt.Rows[index]["OTDEDUCTHRS"].ToString().Trim(); //Convert.ToInt32(dt.Rows[index]["OTDEDUCTHRS"].ToString());
                    Shift.Overtimedeductafter = dt.Rows[index]["OTDEDUCTAFTER"].ToString().Trim(); //float.Parse(dt.Rows[index]["OTDEDUCTAFTER"].ToString());
                    Shift.Overtimestartafter = dt.Rows[index]["OTSTARTAFTER"].ToString().Trim(); //float.Parse(dt.Rows[index]["OTSTARTAFTER"].ToString());
                    Shift.ShiftPosition = dt.Rows[index]["ShiftPosition"].ToString().Trim();

                    //Shift.CreatedDate = Convert.ToDateTime(dt.Rows[index]["CreatedDate"].ToString());
                    //Shift.LastModifiedBy = dt.Rows[index]["LastModifiedBy"].ToString();
                    //Shift.LastModifiedDate = Convert.ToDateTime(dt.Rows[index]["LastModifiedDate"].ToString());
                    //lstShift.Add(Shift);
                }
            }
            return Shift;
        }
        //Get the details of a particular  Shift Code  
        public int GetShiftCode(string Code, string CompanyCode)
        {
            int res = 0;
            DataTable dt = DataOperation.GetDataTableWithParameter("TblShiftMaster_CheckIsCode", new SqlParameter("SHIFT", Code.Trim()));
            if (dt.Rows.Count > 0)
            {
                for (int index = 0; index < dt.Rows.Count; index++)
                {
                    res = Convert.ToInt32(dt.Rows[index]["cnt"].ToString());
                }
            }
            return res;
        }
        //To Delete the record on a particular Copmpany  
        public int DeleteShif(string Code, string CompanyCode)
        {
            //string oldCode = Code;
            ShiftModel oldobject = GetShiftData(Code.Trim(), CompanyCode);
            ShiftCloneModel objAuditold = new ShiftCloneModel();
            ShiftCloneModel objAuditnew = new ShiftCloneModel();
            int res = DataOperation.InsUpdDel("TblShiftMaster_Del", new SqlParameter("SHIFT", Code.Trim()), new SqlParameter("CompanyCode", CompanyCode.Trim()));
            return res;
        }
        //public int Delete(ShiftModel Shift)
        //{
        //    int res = -1;
        //    ShiftCloneModel objAuditold = new ShiftCloneModel();
        //    ShiftCloneModel objAuditnew = new ShiftCloneModel();
        //    ShiftModel objgetexpmast = GetShiftData(Shift.Code.Trim());
        //    //code for clone copy create for old object
        //    objAuditold.IsActive = objgetexpmast.IsActive;
        //    objAuditold.Code = objgetexpmast.Code;
        //    objAuditold.Name = objgetexpmast.Name;
        //    objAuditold.ShortName = objgetexpmast.ShortName;
        //    objAuditold.IndustryNature = objgetexpmast.IndustryNature;
        //    objAuditold.Address = objgetexpmast.Address;
        //    objAuditold.PhoneNo = objgetexpmast.PhoneNo;
        //    objAuditold.EmailId = objgetexpmast.EmailId;
        //    objAuditold.RegistationNo = objgetexpmast.RegistationNo;
        //    objAuditold.PANNo = objgetexpmast.PANNo;
        //    objAuditold.TANNo = objgetexpmast.TANNo;
        //    objAuditold.PFNo = objgetexpmast.PFNo;
        //    objAuditold.LCNo = objgetexpmast.LCNo;
        //    //code for clone copy create for new  object
        //    objAuditnew.IsActive = "";
        //    objAuditnew.Code = "";
        //    objAuditnew.Name = "";
        //    objAuditnew.ShortName = "";
        //    objAuditnew.IndustryNature = "";
        //    objAuditnew.Address = "";
        //    objAuditnew.PhoneNo = "";
        //    objAuditnew.EmailId = "";
        //    objAuditnew.RegistationNo = "";
        //    objAuditnew.PANNo = "";
        //    objAuditnew.TANNo = "";
        //    objAuditnew.PFNo = "";
        //    objAuditnew.LCNo = "";

        //    res = DataOperation.InsUpdDel("TblShiftMaster_Del", new SqlParameter("ShiftCode", Shift.Code.Trim()));
        //    if (res == 1)
        //    {
        //        AuditLog.CreateAuditTrail(ShiftModel.ShiftMASTER, ShiftModel.ShiftCODE, Shift.Code.Trim(), AppConstant.DELETED, Shift.LastModifiedBy, Shift.LoginTerminalNameIP, objAuditold, objAuditnew);
        //    }
        //    // }
        //    return res;
        //}
    }
}