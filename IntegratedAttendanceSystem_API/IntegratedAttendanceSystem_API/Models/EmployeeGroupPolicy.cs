﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IntegratedAttendanceSystem_API.Models
{
    public class EmployeeGroupPolicy
    {
        public const string EmployeeGroupPolicyMaster = "EmployeeGroupPolicy";
        public const string PayCode = "GroupID";

        public string sGroupID { get; set; }
        public string sGroupName { get; set; }
        public string sSHIFT { get; set; }
        public string sSHIFTTYPE { get; set; }
        public string sSHIFTPATTERN { get; set; }
        public string sSHIFTREMAINDAYS { get; set; }
        public string sLASTSHIFTPERFORMED { get; set; }
        public string sINONLY { get; set; }
        public string sISPUNCHALL { get; set; }
        public string sISTIMELOSSALLOWED { get; set; }
        public string sALTERNATE_OFF_DAYS { get; set; }
        public string sCDAYS { get; set; }
        public string sISROUNDTHECLOCKWORK { get; set; }
        public string sISOT { get; set; }
        public string sOTRATE { get; set; }
        public string sFIRSTOFFDAY { get; set; }
        public string sSECONDOFFTYPE { get; set; }
        public string sHALFDAYSHIFT { get; set; }
        public string sALTERNATE_OFF_DAYS_1 { get; set; }
        public string sALTERNATE_OFF_DAYS_2 { get; set; }
        public string sALTERNATE_OFF_DAYS_3 { get; set; }
        public string sALTERNATE_OFF_DAYS_4 { get; set; }
        public string sALTERNATE_OFF_DAYS_5 { get; set; }
        public string sSECONDOFFDAY { get; set; }
        public string sPERMISLATEARRIVAL { get; set; }
        public TimeSpan tsPERMISLATEARRIVAL { get; set; }
        public string sPERMISEARLYDEPRT { get; set; }
        public TimeSpan tsPERMISEARLYDEPRT { get; set; }
        public string sISAUTOSHIFT { get; set; }
        public string sISOUTWORK { get; set; }
        public string sMAXDAYMIN { get; set; }
        public string sISOS { get; set; }
        public string sAUTH_SHIFTS { get; set; }
        public string sTIME { get; set; }
        public TimeSpan tsTIME { get; set; }
        public string sSHORT { get; set; }
        public TimeSpan tsSHORT { get; set; }
        public string sHALF { get; set; }
        public TimeSpan tsHALF { get; set; }
        public string sISHALFDAY { get; set; }
        public string sISSHORT { get; set; }
        public string sTWO { get; set; }
        public string sisReleaver { get; set; }
        public string sisWorker { get; set; }
        public string sisFlexi { get; set; }
        public string sSSN { get; set; }
        public string sMIS { get; set; }
        public string sIsCOF { get; set; }
        public string sHLFAfter { get; set; }
        public TimeSpan tsHLFAfter { get; set; }
        public string sHLFBefore { get; set; }
        public TimeSpan tsHLFBefore { get; set; }
        public string sResignedAfter { get; set; }
        public string sEnableAutoResign { get; set; }
        public string sS_END { get; set; }
        public string sS_OUT { get; set; }
        public string sAUTOSHIFT_LOW { get; set; }
        public TimeSpan tsAUTOSHIFT_LOW { get; set; }
        public string sAUTOSHIFT_UP { get; set; }
        public TimeSpan tsAUTOSHIFT_UP { get; set; }
        public string sISPRESENTONWOPRESENT { get; set; }
        public string sISPRESENTONHLDPRESENT { get; set; }
        public string sNightShiftFourPunch { get; set; }
        public string sISAUTOABSENT { get; set; }
        public string sISAWA { get; set; }
        public string sISWA { get; set; }
        public string sISAW { get; set; }
        public string sISPREWO { get; set; }
        public string sISOTOUTMINUSSHIFTENDTIME { get; set; }
        public string sISOTWRKGHRSMINUSSHIFTHRS { get; set; }
        public string sISOTEARLYCOMEPLUSLATEDEP { get; set; }
        public string sDEDUCTHOLIDAYOT { get; set; }
        public TimeSpan tsDEDUCTHOLIDAYOT { get; set; }
        public string sDEDUCTWOOT { get; set; }
        public TimeSpan tsDEDUCTWOOT { get; set; }
        public string sISOTEARLYCOMING { get; set; }
        public string sOTMinus { get; set; }
        public string sOTROUND { get; set; }
        public string sOTEARLYDUR { get; set; }
        public TimeSpan tsOTEARLYDUR { get; set; }
        public string sOTLATECOMINGDUR { get; set; }
        public TimeSpan tsOTLATECOMINGDUR { get; set; }
        public string sOTRESTRICTENDDUR { get; set; }
        public TimeSpan tsOTRESTRICTENDDUR { get; set; }
        public string sDUPLICATECHECKMIN { get; set; }
        public TimeSpan tsDUPLICATECHECKMIN { get; set; }
        public string sPREWO { get; set; }
        public TimeSpan tsPREWO { get; set; }
        public string sCOMPANYCODE { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime LastModifiedDate { get; set; }
        public string LoginTerminalNameIP { get; set; }
        public List<Company_list> Company { get; set; }

        public List<Shift_list> Shift { get; set; }
    }
    public class Shift_list
    {
        public string ShiftCode { get; set; }
        public string ShiftName { get; set; }
    }

    //public class Company_list
    //{
    //    public string CompanyCode { get; set; }
    //    public string CompanyName { get; set; }
    //}
    //public class Department_list
    //{
    //    public string DepartmentCode { get; set; }
    //    public string DepartmentName { get; set; }
    //}
    //public class Group_list
    //{
    //    public string GroupCode { get; set; }
    //    public string GroupName { get; set; }
    //}
    //public class Designation_list
    //{
    //    public string DesignationCode { get; set; }
    //    public string DesignationName { get; set; }

    //}
    //public class Grade_list
    //{
    //    public string GradeCode { get; set; }
    //    public string GradeName { get; set; }
    //}
    //public class Bank_list
    //{
    //    public string BankCode { get; set; }
    //    public string BankName { get; set; }
    //}
    //public class Division_list
    //{
    //    public string DivisionCode { get; set; }
    //    public string DivisionName { get; set; }
    //}
    //public class CatMaster_list
    //{
    //    public string CategoryCode { get; set; }
    //    public string CategoryName { get; set; }
    //}
    //public class HOD_list
    //{
    //    public string HODCode { get; set; }
    //    public string HODName { get; set; }
    //}
    //public class EmployeeGroup_list
    //{
    //    public string GroupID { get; set; }
    //    public string GroupName { get; set; }
    //}



    //public class JobPosJDMaster_list
    //{
    //    public string JobMasterId { get; set; }
    //    public string JobMasterName { get; set; }
    //}
    //public class JobPosAgeMaster_list
    //{
    //    public string AgeId { get; set; }
    //    public string AgeDesc { get; set; }
    //}    
    //public class JobPosIMMaster_list
    //{
    //    public string IMCode { get; set; }
    //    public string IMDesc { get; set; }
    //}
    //public class JobPosQuaification_list
    //{
    //    public string QID { get; set; }
    //    public string QDescc { get; set; }
    //}
    //public class JobPosLanguage_list
    //{
    //    public string LangCode { get; set; }
    //    public string LangDesc { get; set; }
    //}
    //public class JobPosCertificate_list
    //{
    //    public string CertCode { get; set; }
    //    public string CertName { get; set; }
    //}
    //public class JobPosSpeclzn_list
    //{
    //    public string SpecLznID { get; set; }
    //    public string SpecLznDesc { get; set; }
    //}
    //public class JobPosPrimarySkill_list
    //{
    //    public string pskillID { get; set; }
    //    public string pskillName { get; set; }
    //}
    //public class JobPosSecondarySkill_list
    //{
    //    public string sskillID { get; set; }
    //    public string sskillName { get; set; }
    //}
    //public class JobPosTotalExp_list
    //{
    //    public string ExpId { get; set; }
    //    public string ExpDesc { get; set; }
    //}
    //public class JobPosRelv_list
    //{
    //    public string RelvExpId { get; set; }
    //    public string RelvExpDesc { get; set; }
    //}
}