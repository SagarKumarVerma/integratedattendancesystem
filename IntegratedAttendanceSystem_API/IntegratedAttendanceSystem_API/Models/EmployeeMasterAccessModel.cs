﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;
using IntegratedAttendanceSystem_API.DataAccess;

namespace IntegratedAttendanceSystem_API.Models
{
    public class EmployeeMasterAccessModel
    {
        public int IsCheckIsNumberExpID()
        {
            int res = 0;
            DataTable dt = DataOperation.GetDataTableWithoutParameter("TblEmployeeMaster_CheckIsNumberExpID");
            if (dt.Rows.Count > 0)
            {
                for (int index = 0; index < dt.Rows.Count; index++)
                {
                    res = Convert.ToInt32(dt.Rows[index]["cnt"].ToString());
                }
            }
            return res;
        }
        //To View all EmployeeMaster details
        public IEnumerable<EmployeeMasterModel> GetAllEmployeeMaster()
        {
            DateTime date = System.DateTime.Now;
            List<EmployeeMasterModel> lstEmployeeMaster = new List<EmployeeMasterModel>();
            DataTable dt = DataOperation.GetDataTableWithoutParameter("TblEmployeeMaster_AllRec");
            if (dt.Rows.Count > 0)
            {
                for (int index = 0; index < dt.Rows.Count; index++)
                {
                    EmployeeMasterModel EmployeeMasterMdl = new EmployeeMasterModel();

                    EmployeeMasterMdl.sACTIVE = dt.Rows[index]["ACTIVE"].ToString();
                    EmployeeMasterMdl.sPAYCODE = dt.Rows[index]["PAYCODE"].ToString();
                    EmployeeMasterMdl.sEMPNAME = dt.Rows[index]["EMPNAME"].ToString();
                    EmployeeMasterMdl.sGUARDIANNAME = dt.Rows[index]["GUARDIANNAME"].ToString();
                    EmployeeMasterMdl.sDateOFBIRTH = dt.Rows[index]["DateOFBIRTH"].ToString();
                    EmployeeMasterMdl.sDateOFJOIN = dt.Rows[index]["DateOFJOIN"].ToString();
                    EmployeeMasterMdl.sPRESENTCARDNO = dt.Rows[index]["PRESENTCARDNO"].ToString();
                    //EmployeeMasterMdl.sCompany = dt.Rows[index]["COMPANYCODE"].ToString();
                    //EmployeeMasterMdl.sDivisionCode = dt.Rows[index]["DivisionCode"].ToString();
                    //EmployeeMasterMdl.sCAT = dt.Rows[index]["CAT"].ToString();
                    EmployeeMasterMdl.sSEX = dt.Rows[index]["SEX"].ToString();
                    EmployeeMasterMdl.sISMARRIED = dt.Rows[index]["ISMARRIED"].ToString();
                    EmployeeMasterMdl.sBUS = dt.Rows[index]["BUS"].ToString();
                    EmployeeMasterMdl.sQUALIFICATION = dt.Rows[index]["QUALIFICATION"].ToString();
                    EmployeeMasterMdl.sEXPERIENCE = dt.Rows[index]["EXPERIENCE"].ToString();
                    //EmployeeMasterMdl.sDESIGNATION = dt.Rows[index]["DESIGNATION"].ToString();
                    EmployeeMasterMdl.sADDRESS1 = dt.Rows[index]["ADDRESS1"].ToString();
                    EmployeeMasterMdl.sPINCODE1 = dt.Rows[index]["PINCODE1"].ToString();
                    EmployeeMasterMdl.sTELEPHONE1 = dt.Rows[index]["TELEPHONE1"].ToString();
                    EmployeeMasterMdl.sE_MAIL1 = dt.Rows[index]["E_MAIL1"].ToString();
                    EmployeeMasterMdl.sADDRESS2 = dt.Rows[index]["ADDRESS2"].ToString();
                    EmployeeMasterMdl.sPINCODE2 = dt.Rows[index]["PINCODE2"].ToString();
                    EmployeeMasterMdl.sTELEPHONE2 = dt.Rows[index]["TELEPHONE2"].ToString();
                    EmployeeMasterMdl.sBLOODGROUP = dt.Rows[index]["BLOODGROUP"].ToString();
                    EmployeeMasterMdl.sEMPPHOTO = dt.Rows[index]["EMPPHOTO"].ToString();
                    EmployeeMasterMdl.sEMPSIGNATURE = dt.Rows[index]["EMPSIGNATURE"].ToString();
                    //EmployeeMasterMdl.sDepartmentCode = dt.Rows[index]["DepartmentCode"].ToString();
                    //EmployeeMasterMdl.sGradeCode = dt.Rows[index]["GradeCode"].ToString();
                    EmployeeMasterMdl.sLeavingdate = dt.Rows[index]["Leavingdate"].ToString();
                    EmployeeMasterMdl.sLeavingReason = dt.Rows[index]["LeavingReason"].ToString();
                    EmployeeMasterMdl.sVehicleNo = dt.Rows[index]["VehicleNo"].ToString();
                    EmployeeMasterMdl.sPFNO = dt.Rows[index]["PFNO"].ToString();
                    EmployeeMasterMdl.sPF_NO = dt.Rows[index]["PF_NO"].ToString();
                    EmployeeMasterMdl.sESINO = dt.Rows[index]["ESINO"].ToString();
                    EmployeeMasterMdl.sAUTHORISEDMACHINE = dt.Rows[index]["AUTHORISEDMACHINE"].ToString();
                    EmployeeMasterMdl.sEMPTYPE = dt.Rows[index]["EMPTYPE"].ToString();
                    EmployeeMasterMdl.sBankAcc = dt.Rows[index]["BankAcc"].ToString();
                    //EmployeeMasterMdl.sbankCODE = dt.Rows[index]["bankCODE"].ToString();
                    EmployeeMasterMdl.sReporting1 = dt.Rows[index]["Reporting1"].ToString();
                    EmployeeMasterMdl.sReporting2 = dt.Rows[index]["Reporting2"].ToString();
                    EmployeeMasterMdl.sBRANCHCODE = dt.Rows[index]["BRANCHCODE"].ToString();
                    EmployeeMasterMdl.sDESPANSARYCODE = dt.Rows[index]["DESPANSARYCODE"].ToString();
                    EmployeeMasterMdl.sheadid = dt.Rows[index]["headid"].ToString();
                    EmployeeMasterMdl.sEmail_CC = dt.Rows[index]["Email_CC"].ToString();
                    EmployeeMasterMdl.sLeaveApprovalStages = dt.Rows[index]["LeaveApprovalStages"].ToString();
                    EmployeeMasterMdl.sIsFixedShift = dt.Rows[index]["IsFixedShift"].ToString();
                    EmployeeMasterMdl.sinvoluntary = dt.Rows[index]["involuntary"].ToString();
                    EmployeeMasterMdl.sEmpBioData = dt.Rows[index]["EmpBioData"].ToString();
                    EmployeeMasterMdl.sdateofexp = dt.Rows[index]["dateofexp"].ToString();
                    EmployeeMasterMdl.sDisable = dt.Rows[index]["Disable"].ToString();
                    EmployeeMasterMdl.sGroup_Index = dt.Rows[index]["Group_Index"].ToString();
                    EmployeeMasterMdl.sOnRoll = dt.Rows[index]["OnRoll"].ToString();
                    EmployeeMasterMdl.steamid = dt.Rows[index]["teamid"].ToString();
                    EmployeeMasterMdl.sPSCODE = dt.Rows[index]["PSCODE"].ToString();
                    EmployeeMasterMdl.sSUBAREACODE = dt.Rows[index]["SUBAREACODE"].ToString();
                    EmployeeMasterMdl.sSECTIONID = dt.Rows[index]["SECTIONID"].ToString();
                    EmployeeMasterMdl.sPOSITIONCODE = dt.Rows[index]["POSITIONCODE"].ToString();
                    EmployeeMasterMdl.strainer = dt.Rows[index]["trainer"].ToString();
                    EmployeeMasterMdl.strainee = dt.Rows[index]["trainee"].ToString();
                    EmployeeMasterMdl.sTraineeLevel = dt.Rows[index]["TraineeLevel"].ToString();
                    EmployeeMasterMdl.sRockWellid = dt.Rows[index]["RockWellid"].ToString();
                    EmployeeMasterMdl.sTemplate_Send = dt.Rows[index]["Template_Send"].ToString();
                    //EmployeeMasterMdl.sGrade = dt.Rows[index]["Grade"].ToString();
                    EmployeeMasterMdl.sheadid_2 = dt.Rows[index]["headid_2"].ToString();
                    EmployeeMasterMdl.shead_id = dt.Rows[index]["head_id"].ToString();
                    EmployeeMasterMdl.sHOD_Codd = dt.Rows[index]["HOD_Codd"].ToString();
                    EmployeeMasterMdl.sHOD_Code = dt.Rows[index]["HOD_Code"].ToString();
                    EmployeeMasterMdl.slogid = dt.Rows[index]["logid"].ToString();
                    //EmployeeMasterMdl.sGroupCode = dt.Rows[index]["GroupCode"].ToString();
                    EmployeeMasterMdl.sClassCode = dt.Rows[index]["ClassCode"].ToString();
                    EmployeeMasterMdl.sReligionCode = dt.Rows[index]["ReligionCode"].ToString();
                    EmployeeMasterMdl.sValidityStartDate = dt.Rows[index]["ValidityStartDate"].ToString();
                    EmployeeMasterMdl.sValidityEndDate = dt.Rows[index]["ValidityEndDate"].ToString();
                    EmployeeMasterMdl.sSSN = dt.Rows[index]["SSN"].ToString();
                    EmployeeMasterMdl.sDeviceGroupID = dt.Rows[index]["DeviceGroupID"].ToString();
                    EmployeeMasterMdl.sClientID = dt.Rows[index]["ClientID"].ToString();
                    //EmployeeMasterMdl.sGroupID = dt.Rows[index]["GroupID"].ToString();


                    //EmployeeMasterMdl.CreatedDate = Convert.ToDateTime(dt.Rows[index]["CreatedDate"].ToString());
                    //EmployeeMasterMdl.LastModifiedBy = dt.Rows[index]["LastModifiedBy"].ToString();
                    //EmployeeMasterMdl.LastModifiedDate = Convert.ToDateTime(dt.Rows[index]["LastModifiedDate"].ToString());


                    lstEmployeeMaster.Add(EmployeeMasterMdl);
                }
            }
            return lstEmployeeMaster;
        }
        //To Add new EmployeeMaster record    
        //public int Add(EmployeeMasterModel EmployeeMasterMst)
        //{
        //    EmployeeMasterMst.EmployeeMasterCity = (String.IsNullOrEmpty(EmployeeMasterMst.EmployeeMasterCity) ? "" : EmployeeMasterMst.EmployeeMasterCity);
        //    EmployeeMasterMst.EmployeeMasterState = (String.IsNullOrEmpty(EmployeeMasterMst.EmployeeMasterState) ? "" : EmployeeMasterMst.EmployeeMasterState);
        //    EmployeeMasterMst.EmployeeMasterPostalCode = (String.IsNullOrEmpty(EmployeeMasterMst.EmployeeMasterPostalCode) ? "" : EmployeeMasterMst.EmployeeMasterPostalCode);
        //    EmployeeMasterMst.EmployeeMasterCountry = (String.IsNullOrEmpty(EmployeeMasterMst.EmployeeMasterCountry) ? "" : EmployeeMasterMst.EmployeeMasterCountry);
        //    EmployeeMasterMst.EmployeeMasterAddress1 = (String.IsNullOrEmpty(EmployeeMasterMst.EmployeeMasterAddress1) ? "" : EmployeeMasterMst.EmployeeMasterAddress1);
        //    EmployeeMasterMst.EmployeeMasterAddress2 = (String.IsNullOrEmpty(EmployeeMasterMst.EmployeeMasterAddress2) ? "" : EmployeeMasterMst.EmployeeMasterAddress2);
        //    EmployeeMasterMst.EmployeeMasterContactEmailID = (String.IsNullOrEmpty(EmployeeMasterMst.EmployeeMasterContactEmailID) ? "" : EmployeeMasterMst.EmployeeMasterContactEmailID);

        //    int res = DataOperation.InsUpdDel("TblEmployeeMaster_Add",
        //        new SqlParameter("EmployeeMasterCode", EmployeeMasterMst.EmployeeMasterCode),
        //        new SqlParameter("EmployeeMasterName", EmployeeMasterMst.EmployeeMasterName),
        //        new SqlParameter("City", EmployeeMasterMst.EmployeeMasterCity),
        //        new SqlParameter("State", EmployeeMasterMst.EmployeeMasterState),
        //        new SqlParameter("PostalCode", EmployeeMasterMst.EmployeeMasterPostalCode),
        //        new SqlParameter("Country", EmployeeMasterMst.EmployeeMasterCountry),
        //        new SqlParameter("Address1", EmployeeMasterMst.EmployeeMasterAddress1),
        //        new SqlParameter("Address2", EmployeeMasterMst.EmployeeMasterAddress2),
        //        new SqlParameter("ContactEmailID", EmployeeMasterMst.EmployeeMasterContactEmailID),
        //        new SqlParameter("LastModifiedBy", EmployeeMasterMst.LastModifiedBy.Trim()));
        //    if (res == 1)
        //    {
        //        EmployeeMasterModel oldobject = GetEmployeeMasterData(EmployeeMasterMst.EmployeeMasterCode);
        //        AuditLog.CreateAuditTrail(EmployeeMasterModel.EmployeeMasterMASTER, EmployeeMasterModel.EmployeeMasterCODE, EmployeeMasterMst.EmployeeMasterCode, AppConstant.CREATED, EmployeeMasterMst.LastModifiedBy, EmployeeMasterMst.LoginTerminalNameIP, oldobject, EmployeeMasterMst);
        //    }
        //    return res;
        //}
        //To Update the records of a particluar EmployeeMaster 
        //public int Update(EmployeeMasterModel EmployeeMasterMst)
        //{
        //    EmployeeMasterMst.EmployeeMasterCity = (String.IsNullOrEmpty(EmployeeMasterMst.EmployeeMasterCity) ? "" : EmployeeMasterMst.EmployeeMasterCity);
        //    EmployeeMasterMst.EmployeeMasterState = (String.IsNullOrEmpty(EmployeeMasterMst.EmployeeMasterState) ? "" : EmployeeMasterMst.EmployeeMasterState);
        //    EmployeeMasterMst.EmployeeMasterPostalCode = (String.IsNullOrEmpty(EmployeeMasterMst.EmployeeMasterPostalCode) ? "" : EmployeeMasterMst.EmployeeMasterPostalCode);
        //    EmployeeMasterMst.EmployeeMasterCountry = (String.IsNullOrEmpty(EmployeeMasterMst.EmployeeMasterCountry) ? "" : EmployeeMasterMst.EmployeeMasterCountry);
        //    EmployeeMasterMst.EmployeeMasterAddress1 = (String.IsNullOrEmpty(EmployeeMasterMst.EmployeeMasterAddress1) ? "" : EmployeeMasterMst.EmployeeMasterAddress1);
        //    EmployeeMasterMst.EmployeeMasterAddress2 = (String.IsNullOrEmpty(EmployeeMasterMst.EmployeeMasterAddress2) ? "" : EmployeeMasterMst.EmployeeMasterAddress2);
        //    EmployeeMasterMst.EmployeeMasterContactEmailID = (String.IsNullOrEmpty(EmployeeMasterMst.EmployeeMasterContactEmailID) ? "" : EmployeeMasterMst.EmployeeMasterContactEmailID);

        //    EmployeeMasterModel objgetexpmast = GetEmployeeMasterData(EmployeeMasterMst.EmployeeMasterCode);
        //    int res = DataOperation.InsUpdDel("TblEmployeeMaster_Upd",
        //        new SqlParameter("EmployeeMasterCode", EmployeeMasterMst.EmployeeMasterCode),
        //        new SqlParameter("EmployeeMasterName", EmployeeMasterMst.EmployeeMasterName),
        //        new SqlParameter("City", EmployeeMasterMst.EmployeeMasterCity),
        //        new SqlParameter("State", EmployeeMasterMst.EmployeeMasterState),
        //        new SqlParameter("PostalCode", EmployeeMasterMst.EmployeeMasterPostalCode),
        //        new SqlParameter("Country", EmployeeMasterMst.EmployeeMasterCountry),
        //        new SqlParameter("Address1", EmployeeMasterMst.EmployeeMasterAddress1),
        //        new SqlParameter("Address2", EmployeeMasterMst.EmployeeMasterAddress2),
        //        new SqlParameter("ContactEmailID", EmployeeMasterMst.EmployeeMasterContactEmailID),
        //        new SqlParameter("LastModifiedBy", EmployeeMasterMst.LastModifiedBy.Trim()));
        //    if (res == 1)
        //    {
        //        EmployeeMasterCloneModel objAuditold = new EmployeeMasterCloneModel();
        //        EmployeeMasterCloneModel objAuditnew = new EmployeeMasterCloneModel();
        //        //code for clone copy create for old object
        //        objAuditold.EmployeeMasterCode = objgetexpmast.EmployeeMasterCode;
        //        objAuditold.EmployeeMasterName = objgetexpmast.EmployeeMasterName;
        //        objAuditold.EmployeeMasterCity = objgetexpmast.EmployeeMasterCity;
        //        objAuditold.EmployeeMasterState = objgetexpmast.EmployeeMasterState;
        //        objAuditold.EmployeeMasterPostalCode = objgetexpmast.EmployeeMasterPostalCode;
        //        objAuditold.EmployeeMasterCountry = objgetexpmast.EmployeeMasterCountry;
        //        objAuditold.EmployeeMasterAddress1 = objgetexpmast.EmployeeMasterAddress1;
        //        objAuditold.EmployeeMasterAddress2 = objgetexpmast.EmployeeMasterAddress2;
        //        objAuditold.EmployeeMasterContactEmailID = objgetexpmast.EmployeeMasterContactEmailID;
        //        //code for clone copy create for new  object
        //        objAuditnew.EmployeeMasterCode = EmployeeMasterMst.EmployeeMasterCode;
        //        objAuditnew.EmployeeMasterName = EmployeeMasterMst.EmployeeMasterName;
        //        objAuditnew.EmployeeMasterCity = EmployeeMasterMst.EmployeeMasterCity;
        //        objAuditnew.EmployeeMasterState = EmployeeMasterMst.EmployeeMasterState;
        //        objAuditnew.EmployeeMasterPostalCode = EmployeeMasterMst.EmployeeMasterPostalCode;
        //        objAuditnew.EmployeeMasterCountry = EmployeeMasterMst.EmployeeMasterCountry;
        //        objAuditnew.EmployeeMasterAddress1 = EmployeeMasterMst.EmployeeMasterAddress1;
        //        objAuditnew.EmployeeMasterAddress2 = EmployeeMasterMst.EmployeeMasterAddress2;
        //        objAuditnew.EmployeeMasterContactEmailID = EmployeeMasterMst.EmployeeMasterContactEmailID;

        //        //objAuditnew.LastModifiedBy = Convert.ToString(HttpContext.Current.Session["UserName"]);
        //        //objAuditnew.LastModifiedDate = DateTime.Now;
        //        AuditLog.CreateAuditTrail(EmployeeMasterModel.EmployeeMasterMASTER, EmployeeMasterModel.EmployeeMasterCODE, EmployeeMasterMst.EmployeeMasterCode, AppConstant.MODIFIED, EmployeeMasterMst.LastModifiedBy, EmployeeMasterMst.LoginTerminalNameIP, objAuditold, objAuditnew);
        //    }
        //    return res;
        //}
        //Get the details of a particular EmployeeMaster Data  
        //public EmployeeMasterModel GetEmployeeMasterData(string EmployeeMasterCode)
        //{

        //    EmployeeMasterModel EmployeeMasterMstMod = new EmployeeMasterModel();
        //    DataTable dt = DataOperation.GetDataTableWithParameter("TblEmployeeMaster_GetEmployeeMasterData", new SqlParameter("EmployeeMasterCode", EmployeeMasterCode.Trim()));
        //    if (dt.Rows.Count > 0)
        //    {
        //        for (int index = 0; index < dt.Rows.Count; index++)
        //        {
        //            EmployeeMasterMstMod.EmployeeMasterCode = dt.Rows[index]["EmployeeMasterCode"].ToString();
        //            EmployeeMasterMstMod.EmployeeMasterName = dt.Rows[index]["EmployeeMasterName"].ToString();
        //            EmployeeMasterMstMod.EmployeeMasterCity = dt.Rows[index]["City"].ToString();
        //            EmployeeMasterMstMod.EmployeeMasterState = dt.Rows[index]["State"].ToString();
        //            EmployeeMasterMstMod.EmployeeMasterPostalCode = dt.Rows[index]["PostalCode"].ToString();
        //            EmployeeMasterMstMod.EmployeeMasterCountry = dt.Rows[index]["Country"].ToString();
        //            EmployeeMasterMstMod.EmployeeMasterAddress1 = dt.Rows[index]["Address1"].ToString();
        //            EmployeeMasterMstMod.EmployeeMasterAddress2 = dt.Rows[index]["Address2"].ToString();
        //            EmployeeMasterMstMod.EmployeeMasterContactEmailID = dt.Rows[index]["ContactEmailID"].ToString();
        //            EmployeeMasterMstMod.CreatedDate = Convert.ToDateTime(dt.Rows[index]["CreatedDate"].ToString());
        //            EmployeeMasterMstMod.LastModifiedBy = dt.Rows[index]["LastModifiedBy"].ToString();
        //            EmployeeMasterMstMod.LastModifiedDate = Convert.ToDateTime(dt.Rows[index]["LastModifiedDate"].ToString());
        //        }
        //    }
        //    return EmployeeMasterMstMod;
        //}
        //Get the details of a particular  Company Code  
        //public int GetEmployeeMasterCode(string Code)
        //{
        //    int res = 0;
        //    DataTable dt = DataOperation.GetDataTableWithParameter("TblEmployeeMaster_CheckIsCode", new SqlParameter("EmployeeMasterCode", Code.Trim()));
        //    if (dt.Rows.Count > 0)
        //    {
        //        for (int index = 0; index < dt.Rows.Count; index++)
        //        {
        //            res = Convert.ToInt32(dt.Rows[index]["cnt"].ToString());
        //        }
        //    }
        //    return res;
        //}
        //To Delete the record on a particular  EmployeeMaster 
        //public int Deleteold(string EmployeeMasterCode)
        //{
        //    string oldCode = EmployeeMasterCode;
        //    EmployeeMasterModel oldobject = GetEmployeeMasterData(oldCode.Trim());
        //    EmployeeMasterCloneModel objAuditold = new EmployeeMasterCloneModel();
        //    EmployeeMasterCloneModel objAuditnew = new EmployeeMasterCloneModel();
        //    int res = DataOperation.InsUpdDel("TblEmployeeMaster_Del", new SqlParameter("EmployeeMasterCode", EmployeeMasterCode.Trim()));
        //    return res;
        //}
        //public int Delete(EmployeeMasterModel EmployeeMasterMst)
        //{
        //    int res = -1;
        //    EmployeeMasterCloneModel objAuditold = new EmployeeMasterCloneModel();
        //    EmployeeMasterCloneModel objAuditnew = new EmployeeMasterCloneModel();
        //    EmployeeMasterModel objgetexpmast = GetEmployeeMasterData(EmployeeMasterMst.EmployeeMasterCode.Trim());
        //    //code for clone copy create for old object
        //    objAuditold.EmployeeMasterCode = objgetexpmast.EmployeeMasterCode;
        //    objAuditold.EmployeeMasterName = objgetexpmast.EmployeeMasterName;
        //    objAuditold.EmployeeMasterCity = objgetexpmast.EmployeeMasterCity;
        //    objAuditold.EmployeeMasterState = objgetexpmast.EmployeeMasterState;
        //    objAuditold.EmployeeMasterPostalCode = objgetexpmast.EmployeeMasterPostalCode;
        //    objAuditold.EmployeeMasterCountry = objgetexpmast.EmployeeMasterCountry;
        //    objAuditold.EmployeeMasterAddress1 = objgetexpmast.EmployeeMasterAddress1;
        //    objAuditold.EmployeeMasterAddress2 = objgetexpmast.EmployeeMasterAddress2;
        //    objAuditold.EmployeeMasterContactEmailID = objgetexpmast.EmployeeMasterContactEmailID;

        //    //code for clone copy create for new  object
        //    objAuditnew.EmployeeMasterCode = "";
        //    objAuditnew.EmployeeMasterName = "";
        //    objAuditnew.EmployeeMasterCity = "";
        //    objAuditnew.EmployeeMasterState = "";
        //    objAuditnew.EmployeeMasterPostalCode = "";
        //    objAuditnew.EmployeeMasterCountry = "";
        //    objAuditnew.EmployeeMasterAddress1 = "";
        //    objAuditnew.EmployeeMasterAddress2 = "";
        //    objAuditnew.EmployeeMasterContactEmailID = "";

        //    res = DataOperation.InsUpdDel("TblEmployeeMaster_Del", new SqlParameter("EmployeeMasterCode", EmployeeMasterMst.EmployeeMasterCode.Trim()));
        //    if (res == 1)
        //    {
        //        AuditLog.CreateAuditTrail(EmployeeMasterModel.EmployeeMasterMASTER, EmployeeMasterModel.EmployeeMasterCODE, EmployeeMasterMst.EmployeeMasterCode.Trim(), AppConstant.DELETED, EmployeeMasterMst.LastModifiedBy, EmployeeMasterMst.LoginTerminalNameIP, objAuditold, objAuditnew);
        //    }
        //    // }
        //    return res;
        //}

        public List<Company_list> ListAllCompany()
        {
            List<Company_list> lst = new List<Company_list>();
            DataTable dt = DataOperation.GetDataTableWithoutParameter("TblCompanyMaster_AllRec");

            if (dt.Rows.Count > 0)
            {
                for (int index = 0; index < dt.Rows.Count; index++)
                {
                    lst.Add(new Company_list
                    {
                        CompanyCode = dt.Rows[index]["CompanyCode"].ToString(),
                        CompanyName = dt.Rows[index]["CompanyName"].ToString(),
                    });
                }
            }
            return lst;
        }
    }
}