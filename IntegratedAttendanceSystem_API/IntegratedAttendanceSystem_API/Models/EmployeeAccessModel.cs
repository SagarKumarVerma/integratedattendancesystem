﻿using IntegratedAttendanceSystem_API.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace IntegratedAttendanceSystem_API.Models
{
    public class EmployeeAccessModel
    {
        /// <summary>
        /// code for bind jd dropdown
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        //public IEnumerable<JobPosJDMaster_list> BindJD(string compcode, string depcode, string Action)
        //{
        //    Employee jpm = new Employee();
        //    List<JobPosJDMaster_list> lst = new List<JobPosJDMaster_list>();

        //    DataTable dt = DataOperation.GetDataTableWithParameter("UspJobPos_BindJD",
        //                                                            new SqlParameter("CompanyCode", compcode.Trim()),
        //                                                            new SqlParameter("DepartmentCode", depcode.Trim()),
        //                                                              new SqlParameter("Action", Action.Trim()));
        //    if (dt.Rows.Count > 0)
        //    {

        //        for (int index = 0; index < dt.Rows.Count; index++)
        //        {
        //            lst.Add(new JobPosJDMaster_list
        //            {
        //                JobMasterId = Convert.ToString(dt.Rows[index]["JobId"]).Trim(),
        //                JobMasterName = Convert.ToString(dt.Rows[index]["JobName"]).Trim(),
        //            });
        //        }

        //    }
        //    return lst;
        //}
        /// <summary>
        /// code bind age as per the selection of JD
        /// </summary>
        /// <param name="compcode"></param>
        /// <param name="depcode"></param>
        /// <returns></returns>
        //public IEnumerable<JobPosAgeMaster_list> BindAge(string JobID)
        //{
        //    Employee jpm = new Employee();
        //    List<JobPosAgeMaster_list> lst = new List<JobPosAgeMaster_list>();

        //    DataTable dt = DataOperation.GetDataTableWithParameter("UspJobPosAgeSelect",
        //                                                            new SqlParameter("JobId", JobID.Trim()));
        //    if (dt.Rows.Count > 0)
        //    {

        //        for (int index = 0; index < dt.Rows.Count; index++)
        //        {
        //            lst.Add(new JobPosAgeMaster_list
        //            {
        //                AgeId = Convert.ToString(dt.Rows[index]["AgeId"]).Trim(),
        //                AgeDesc = Convert.ToString(dt.Rows[index]["AgeDesc"]).Trim(),
        //            });
        //        }

        //    }
        //    return lst;
        //}

        //public IEnumerable<CatMaster_list> BindJobCat(string isInsert)
        //{
        //    Employee jpm = new Employee();
        //    List<CatMaster_list> lst = new List<CatMaster_list>();

        //    DataTable dt = DataOperation.GetDataTableWithParameter("TblCategory_SelectAll",
        //                                                            new SqlParameter("Action", isInsert.Trim()));
        //    if (dt.Rows.Count > 0)
        //    {

        //        for (int index = 0; index < dt.Rows.Count; index++)
        //        {
        //            lst.Add(new CatMaster_list
        //            {
        //                CategoryCode = Convert.ToString(dt.Rows[index]["CategoryCode"]).Trim(),
        //                CategoryName  = Convert.ToString(dt.Rows[index]["CategoryName"]).Trim(),
        //            });
        //        }

        //    }
        //    return lst;
        //}

        //public IEnumerable<JobPosIMMaster_list> BindIM(string isInsert)
        //{
        //    Employee jpm = new Employee();
        //    List<JobPosIMMaster_list> lst = new List<JobPosIMMaster_list>();

        //    DataTable dt = DataOperation.GetDataTableWithParameter("UspJobPos_BindIM",
        //                                                            new SqlParameter("Action", isInsert.Trim()));
        //    if (dt.Rows.Count > 0)
        //    {

        //        for (int index = 0; index < dt.Rows.Count; index++)
        //        {
        //            lst.Add(new JobPosIMMaster_list
        //            {
        //                IMCode = Convert.ToString(dt.Rows[index]["InterModeCode"]).Trim(),
        //                IMDesc = Convert.ToString(dt.Rows[index]["InterModeDesc"]).Trim(),
        //            });
        //        }

        //    }
        //    return lst;
        //}

        //public IEnumerable<JobPosQuaification_list> BindQualification(string isInsert)
        //{
        //    Employee jpm = new Employee();
        //    List<JobPosQuaification_list> lst = new List<JobPosQuaification_list>();

        //    DataTable dt = DataOperation.GetDataTableWithParameter("UspJobPos_BindQualification",
        //                                                            new SqlParameter("Action", isInsert.Trim()));
        //    if (dt.Rows.Count > 0)
        //    {

        //        for (int index = 0; index < dt.Rows.Count; index++)
        //        {
        //            lst.Add(new JobPosQuaification_list
        //            {
        //                QID = Convert.ToString(dt.Rows[index]["QualId"]).Trim(),
        //                QDescc = Convert.ToString(dt.Rows[index]["QualDesc"]).Trim(),
        //            });
        //        }

        //    }
        //    return lst;
        //}

        //public IEnumerable<JobPosLanguage_list> BindLanguage(string isInsert)
        //{
        //    Employee jpm = new Employee();
        //    List<JobPosLanguage_list> lst = new List<JobPosLanguage_list>();

        //    DataTable dt = DataOperation.GetDataTableWithParameter("UspJobPos_BindLanguage",
        //                                                            new SqlParameter("Action", isInsert.Trim()));
        //    if (dt.Rows.Count > 0)
        //    {

        //        for (int index = 0; index < dt.Rows.Count; index++)
        //        {
        //            lst.Add(new JobPosLanguage_list
        //            {
        //                LangCode = Convert.ToString(dt.Rows[index]["LanguageCode"]).Trim(),
        //                LangDesc = Convert.ToString(dt.Rows[index]["LanguageDesc"]).Trim(),
        //            });
        //        }

        //    }
        //    return lst;
        //}

        //public IEnumerable<JobPosCertificate_list> BindCertificate(string isInsert)
        //{
        //    Employee jpm = new Employee();
        //    List<JobPosCertificate_list> lst = new List<JobPosCertificate_list>();

        //    DataTable dt = DataOperation.GetDataTableWithParameter("UspJobPos_BindCertification",
        //                                                            new SqlParameter("Action", isInsert.Trim()));
        //    if (dt.Rows.Count > 0)
        //    {

        //        for (int index = 0; index < dt.Rows.Count; index++)
        //        {
        //            lst.Add(new JobPosCertificate_list
        //            {
        //                CertCode = Convert.ToString(dt.Rows[index]["CertCode"]).Trim(),
        //                CertName = Convert.ToString(dt.Rows[index]["CertName"]).Trim(),
        //            });
        //        }

        //    }
        //    return lst;
        //}

        //public IEnumerable<JobPosSpeclzn_list> BindSpecializationField(string qID)
        //{
        //    Employee jpm = new Employee();
        //    List<JobPosSpeclzn_list> lst = new List<JobPosSpeclzn_list>();

        //    DataTable dt = DataOperation.GetDataTableWithParameter("UspJobPos_SpecializationFiled",
        //                                                            new SqlParameter("QID", qID.Trim()));
        //    if (dt.Rows.Count > 0)
        //    {

        //        for (int index = 0; index < dt.Rows.Count; index++)
        //        {
        //            lst.Add(new JobPosSpeclzn_list
        //            {
        //                SpecLznID = Convert.ToString(dt.Rows[index]["MqualId"]).Trim(),
        //                SpecLznDesc = Convert.ToString(dt.Rows[index]["MqualDesc"]).Trim(),
        //            });
        //        }

        //    }
        //    return lst;
        //}

        public IEnumerable<Employee> BindEmployee()
        {
            List<Employee> lst = new List<Employee>();
            DataTable dt = DataOperation.GetDataTableWithoutParameter("tblemployee_AllRec");
            if (dt.Rows.Count > 0)
            {
                for (int index = 0; index < dt.Rows.Count; index++)
                {
                    Employee em = new Employee();
                    em.sACTIVE = dt.Rows[index]["ACTIVE"].ToString().Trim();
                    em.sPAYCODE = dt.Rows[index]["PAYCODE"].ToString().Trim();
                    em.sEMPNAME = dt.Rows[index]["EMPNAME"].ToString().Trim();
                    em.sGUARDIANNAME = dt.Rows[index]["GUARDIANNAME"].ToString().Trim();
                    em.sDateOFBIRTH = dt.Rows[index]["DateOFBIRTH"].ToString().Trim();
                    em.sDateOFJOIN = dt.Rows[index]["DateOFJOIN"].ToString().Trim();
                    em.sPRESENTCARDNO = dt.Rows[index]["PRESENTCARDNO"].ToString().Trim();
                    em.sCOMPANYCODE = dt.Rows[index]["COMPANYCODE"].ToString().Trim();
                    em.sDivisionCode = dt.Rows[index]["DivisionCode"].ToString().Trim();
                    em.sCATCode = dt.Rows[index]["CAT"].ToString().Trim();
                    em.sSEX = dt.Rows[index]["SEX"].ToString().Trim();
                    em.sISMARRIED = dt.Rows[index]["ISMARRIED"].ToString().Trim();
                    //em.sBUS = dt.Rows[index]["BUS"].ToString();
                    em.sQUALIFICATION = dt.Rows[index]["QUALIFICATION"].ToString().Trim();
                    em.sEXPERIENCE = dt.Rows[index]["EXPERIENCE"].ToString().Trim();
                    em.sDESIGNATIONCODE = dt.Rows[index]["DESIGNATION"].ToString().Trim();
                    em.sADDRESS1 = dt.Rows[index]["ADDRESS1"].ToString().Trim();
                    em.sPINCODE1 = dt.Rows[index]["PINCODE1"].ToString().Trim();
                    em.sTELEPHONE1 = dt.Rows[index]["TELEPHONE1"].ToString().Trim();
                    em.sE_MAIL1 = dt.Rows[index]["E_MAIL1"].ToString().Trim();
                    em.sADDRESS2 = dt.Rows[index]["ADDRESS2"].ToString().Trim();
                    em.sPINCODE2 = dt.Rows[index]["PINCODE2"].ToString().Trim();
                    em.sTELEPHONE2 = dt.Rows[index]["TELEPHONE2"].ToString().Trim();
                    em.sBLOODGROUP = dt.Rows[index]["BLOODGROUP"].ToString().Trim();
                    em.sEMPPHOTO = dt.Rows[index]["EMPPHOTO"].ToString().Trim();
                    em.sEMPSIGNATURE = dt.Rows[index]["EMPSIGNATURE"].ToString().Trim();
                    em.sDepartmentCode = dt.Rows[index]["DepartmentCode"].ToString().Trim();
                    em.sGradeCode = dt.Rows[index]["GradeCode"].ToString().Trim();
                    em.sLeavingdate = dt.Rows[index]["Leavingdate"].ToString().Trim();
                    em.sLeavingReason = dt.Rows[index]["LeavingReason"].ToString().Trim();
                    em.sVehicleNo = dt.Rows[index]["VehicleNo"].ToString().Trim();
                    em.sPFNO = dt.Rows[index]["PFNO"].ToString().Trim();
                    em.sPF_NO = dt.Rows[index]["PF_NO"].ToString().Trim();
                    em.sESINO = dt.Rows[index]["ESINO"].ToString().Trim();
                    em.sEMPTYPE = dt.Rows[index]["EMPTYPE"].ToString().Trim();
                    em.sBankAcc = dt.Rows[index]["BankAcc"].ToString().Trim();
                    em.BankCode = dt.Rows[index]["BankCode"].ToString().Trim();
                    em.sBRANCHCODE = dt.Rows[index]["BRANCHCODE"].ToString().Trim();
                    em.sDESPANSARYCODE = dt.Rows[index]["DESPANSARYCODE"].ToString().Trim();
                    em.sheadid = dt.Rows[index]["headid"].ToString().Trim();
                    em.sEmail_CC = dt.Rows[index]["Email_CC"].ToString().Trim();
                    em.sLeaveApprovalStages = dt.Rows[index]["LeaveApprovalStages"].ToString().Trim();
                    em.sRockWellid = dt.Rows[index]["RockWellid"].ToString().Trim();
                    //em.sGrade = dt.Rows[index]["Grade"].ToString();
                    //em.sheadid_2 = dt.Rows[index]["headid_2"].ToString().Trim();
                    //em.shead_id = dt.Rows[index]["head_id"].ToString().Trim();
                    //em.sValidityStartDate = dt.Rows[index]["ValidityStartDate"].ToString().Trim();
                    //em.sValidityEndDate = dt.Rows[index]["ValidityEndDate"].ToString().Trim();
                    em.sSSN = dt.Rows[index]["SSN"].ToString().Trim();
                    em.sDeviceGroupID = dt.Rows[index]["DeviceGroupID"].ToString().Trim();
                    em.sClientID = dt.Rows[index]["ClientID"].ToString().Trim();
                    em.sGroupID = dt.Rows[index]["GroupID"].ToString().Trim();

                    lst.Add(em);
                }
            }
            return lst;
        }
        public Employee GetEmployee(string ID)
        {
            Employee em = new Employee();
            DataTable dt = DataOperation.GetDataTableWithParameter("Employee_SingleList", new SqlParameter("SSN", ID.ToString().Trim()));
            if (dt.Rows.Count > 0)
            {
                for (int index = 0; index < dt.Rows.Count; index++)
                {
                    em.sACTIVE = dt.Rows[index]["ACTIVE"].ToString().Trim();
                    em.sPAYCODE = dt.Rows[index]["PAYCODE"].ToString().Trim();
                    em.sEMPNAME = dt.Rows[index]["EMPNAME"].ToString().Trim();
                    em.sGUARDIANNAME = dt.Rows[index]["GUARDIANNAME"].ToString().Trim();
                    em.sDateOFBIRTH = dt.Rows[index]["DateOFBIRTH"].ToString().Trim();
                    em.sDateOFJOIN = dt.Rows[index]["DateOFJOIN"].ToString().Trim();
                    em.sPRESENTCARDNO = dt.Rows[index]["PRESENTCARDNO"].ToString().Trim();
                    em.sCOMPANYCODE = dt.Rows[index]["COMPANYCODE"].ToString().Trim();
                    em.sDivisionCode = dt.Rows[index]["DivisionCode"].ToString().Trim();
                    em.sCATCode = dt.Rows[index]["CAT"].ToString().Trim();
                    em.sSEX = dt.Rows[index]["SEX"].ToString().Trim();
                    em.sISMARRIED = dt.Rows[index]["ISMARRIED"].ToString().Trim();
                    //em.sBUS = dt.Rows[index]["BUS"].ToString();
                    em.sQUALIFICATION = dt.Rows[index]["QUALIFICATION"].ToString().Trim();
                    em.sEXPERIENCE = dt.Rows[index]["EXPERIENCE"].ToString().Trim();
                    em.DesignationCode = dt.Rows[index]["DESIGNATION"].ToString().Trim();

                    em.sADDRESS1 = dt.Rows[index]["ADDRESS1"].ToString().Trim().Trim();
                    em.sPINCODE1 = dt.Rows[index]["PINCODE1"].ToString().Trim();
                    em.sTELEPHONE1 = dt.Rows[index]["TELEPHONE1"].ToString().Trim();
                    em.sE_MAIL1 = dt.Rows[index]["E_MAIL1"].ToString().Trim();
                    em.sADDRESS2 = dt.Rows[index]["ADDRESS2"].ToString().Trim();
                    em.sPINCODE2 = dt.Rows[index]["PINCODE2"].ToString().Trim();
                    em.sTELEPHONE2 = dt.Rows[index]["TELEPHONE2"].ToString().Trim();
                    em.sBLOODGROUP = dt.Rows[index]["BLOODGROUP"].ToString().Trim();
                    em.sEMPPHOTO = dt.Rows[index]["EMPPHOTO"].ToString().Trim();
                    em.sEMPSIGNATURE = dt.Rows[index]["EMPSIGNATURE"].ToString().Trim();
                    em.sDepartmentCode = dt.Rows[index]["DepartmentCode"].ToString().Trim();
                    em.sGradeCode = dt.Rows[index]["GradeCode"].ToString().Trim();
                    em.sLeavingdate = dt.Rows[index]["Leavingdate"].ToString().Trim();
                    em.sLeavingReason = dt.Rows[index]["LeavingReason"].ToString().Trim();
                    em.sVehicleNo = dt.Rows[index]["VehicleNo"].ToString().Trim();
                    em.sPFNO = dt.Rows[index]["PFNO"].ToString().Trim();
                    em.sPF_NO = dt.Rows[index]["PF_NO"].ToString().Trim();
                    em.sESINO = dt.Rows[index]["ESINO"].ToString().Trim();
                    em.sEMPTYPE = dt.Rows[index]["EMPTYPE"].ToString().Trim();
                    em.sBankAcc = dt.Rows[index]["BankAcc"].ToString().Trim();
                    em.BankCode = dt.Rows[index]["bankCODE"].ToString().Trim();
                    em.sBRANCHCODE = dt.Rows[index]["BRANCHCODE"].ToString().Trim();
                    em.sDESPANSARYCODE = dt.Rows[index]["DESPANSARYCODE"].ToString().Trim();
                    em.sheadid = dt.Rows[index]["headid"].ToString().Trim();
                    em.sEmail_CC = dt.Rows[index]["Email_CC"].ToString().Trim();
                    em.sLeaveApprovalStages = dt.Rows[index]["LeaveApprovalStages"].ToString().Trim();
                    em.sRockWellid = dt.Rows[index]["RockWellid"].ToString().Trim();
                    em.sGradeCode = dt.Rows[index]["GradeCode"].ToString().Trim();
                    //em.sheadid_2 = dt.Rows[index]["headid_2"].ToString().Trim();
                    //em.shead_id = dt.Rows[index]["head_id"].ToString().Trim();
                    //em.sValidityStartDate = dt.Rows[index]["ValidityStartDate"].ToString().Trim();
                    //em.sValidityEndDate = dt.Rows[index]["ValidityEndDate"].ToString().Trim();
                    em.sSSN = dt.Rows[index]["SSN"].ToString().Trim();
                    em.sDeviceGroupID = dt.Rows[index]["DeviceGroupID"].ToString().Trim();
                    em.sClientID = dt.Rows[index]["ClientID"].ToString().Trim();
                    em.sGroupID = dt.Rows[index]["GroupID"].ToString().Trim();
                    em.UID = dt.Rows[index]["UID"].ToString().Trim();
                    em.sPANNo = dt.Rows[index]["PANNO"].ToString().Trim();
                }
            }
            return em;
        }

        //public string JobPosCreate(Employee em)
        //{
        //    string result = "";
        //    string IMCode = JPM.IMCode.Remove(JPM.IMCode.Length - 1, 1);
        //    string QID = JPM.QID.Remove(JPM.QID.Length - 1, 1);
        //    string Splzfld = JPM.SpecLznID.Remove(JPM.SpecLznID.Length - 1, 1);
        //    string lang = JPM.LangCode.Remove(JPM.LangCode.Length - 1, 1);
        //    string pss = JPM.pskillID.Remove(JPM.pskillID.Length - 1, 1);
        //    string sss = JPM.sskillID.Remove(JPM.sskillID.Length - 1, 1);
        //    string exp = JPM.ExpId.Remove(JPM.ExpId.Length - 1, 1);
        //    string relvexp = JPM.RelvExpId.Remove(JPM.RelvExpId.Length - 1, 1);
        //    string certcode = JPM.CertCode.Remove(JPM.CertCode.Length - 1, 1);
        //    JPM.JobPosCode = GetAutoID(JPM);
        //    int res = DataOperation.InsUpdDel("UspJobPos_Create", new SqlParameter("IsActive", JPM.IsActive), new SqlParameter("JobPosCode", JPM.JobPosCode.Trim()),
        //        new SqlParameter("JobPosName", JPM.JobPosName.Trim()),
        //        new SqlParameter("JobPosDesc", JPM.JobPosDesc.Trim()),
        //        new SqlParameter("Gender", JPM.Gender.Trim()),
        //        new SqlParameter("DesignationCode", JPM.DesignCode.Trim()),
        //        new SqlParameter("JobId", JPM.JobId.Trim()),
        //        new SqlParameter("JobCatCode", JPM.CatCode.Trim()),
        //        new SqlParameter("AgeId", JPM.AgeId.Trim()),
        //        new SqlParameter("InterModeCode", IMCode.Trim()),
        //        new SqlParameter("MultiQID", QID.Trim()),
        //        new SqlParameter("SpecLznID", Splzfld.Trim()),
        //        new SqlParameter("LangCode", lang.Trim()),
        //        new SqlParameter("pskillID", pss.Trim()),
        //        new SqlParameter("sskillID", sss.Trim()),
        //        new SqlParameter("ExpId", exp.Trim()),
        //        new SqlParameter("RelvExpId", relvexp.Trim()),
        //        new SqlParameter("CertCode", certcode.Trim()),
        //        new SqlParameter("LastModifiedBy", JPM.LastModifiedBy.Trim()));
        //    result = res.ToString();
        //    if (res == 1)
        //    {
        //        Employee oldobject = GetJobPos(JPM.JobPosCode);
        //        AuditLog.CreateAuditTrail(Employee.EMPLOYEEMASTER, Employee.PAYCODE, JPM.JobPosCode, AppConstant.CREATED, JPM.LastModifiedBy, JPM.LoginTerminalNameIP, oldobject, JPM);
        //        result = JPM.JobPosCode;
        //    }
        //    return result;
        //}

        //public int JobPosUpdate(Employee JPM)
        //{
        //    string IMCode = JPM.IMCode.Remove(JPM.IMCode.Length - 1, 1);
        //    string QID = JPM.QID.Remove(JPM.QID.Length - 1, 1);
        //    string Splzfld = JPM.SpecLznID.Remove(JPM.SpecLznID.Length - 1, 1);
        //    string lang = JPM.LangCode.Remove(JPM.LangCode.Length - 1, 1);
        //    string pss = JPM.pskillID.Remove(JPM.pskillID.Length - 1, 1);
        //    string sss = JPM.sskillID.Remove(JPM.sskillID.Length - 1, 1);
        //    string exp = JPM.ExpId.Remove(JPM.ExpId.Length - 1, 1);
        //    string relvexp = JPM.RelvExpId.Remove(JPM.RelvExpId.Length - 1, 1);
        //    string certcode = JPM.CertCode.Remove(JPM.CertCode.Length - 1, 1);
        //    Employee objjpm = GetJobPos(JPM.JobPosCode);
        //    int res = DataOperation.InsUpdDel("UspJobPos_Update", new SqlParameter("IsActive", JPM.IsActive), new SqlParameter("JobPosCode", JPM.JobPosCode),
        //        new SqlParameter("JobPosName", JPM.JobPosName.Trim()),
        //        new SqlParameter("JobPosDesc", JPM.JobPosDesc.Trim()),
        //        new SqlParameter("Gender", JPM.Gender.Trim()),
        //        new SqlParameter("DesignationCode", JPM.DesignCode.Trim()),
        //        new SqlParameter("JobId", JPM.JobId.Trim()),
        //        new SqlParameter("JobCatCode", JPM.CatCode.Trim()),
        //        new SqlParameter("AgeId", JPM.AgeId.Trim()),
        //        new SqlParameter("InterModeCode",IMCode.Trim()),
        //        new SqlParameter("MultiQID", QID.Trim()),
        //        new SqlParameter("SpecLznID", Splzfld.Trim()),
        //        new SqlParameter("LangCode", lang.Trim()),
        //        new SqlParameter("pskillID", pss.Trim()),
        //        new SqlParameter("sskillID", sss.Trim()),
        //        new SqlParameter("ExpId", exp.Trim()),
        //        new SqlParameter("RelvExpId", relvexp.Trim()),
        //        new SqlParameter("CertCode", certcode.Trim()),
        //        new SqlParameter("LastModifiedBy", JPM.LastModifiedBy.Trim()));
        //    if (res == 1)
        //    {
        //        EmployeeClone oldobject = new EmployeeClone();
        //        //code for clone copy create for old object
        //        oldobject.IsActive = objjpm.IsActive;
        //        oldobject.JobPosCode = objjpm.JobPosCode;
        //        oldobject.JobPosName = objjpm.JobPosName;
        //        oldobject.JobPosDesc = objjpm.JobPosDesc;
        //        oldobject.Gender = objjpm.Gender;
        //        oldobject.CompCode = objjpm.CompCode;
        //        oldobject.DepCode = objjpm.DepCode;
        //        oldobject.DesignCode = objjpm.DesignCode;
        //        oldobject.JobId = objjpm.JobId;
        //        oldobject.AgeId = objjpm.AgeId;
        //        oldobject.CatCode = objjpm.CatCode;
        //        oldobject.IMCode = objjpm.IMCode;
        //        oldobject.QID = objjpm.QID;
        //        oldobject.LangCode = objjpm.LangCode;
        //        oldobject.SpecLznID = objjpm.SpecLznID;
        //        oldobject.pskillID = objjpm.pskillID;
        //        oldobject.sskillID = objjpm.sskillID;
        //        oldobject.ExpId = objjpm.ExpId;
        //        oldobject.RelvExpId = objjpm.RelvExpId;
        //        oldobject.CertCode = objjpm.CertCode;
        //        EmployeeClone newobject = new EmployeeClone();
        //        //code for clone copy create for new object
        //        newobject.IsActive = JPM.IsActive;
        //        newobject.JobPosCode = JPM.JobPosCode;
        //        newobject.JobPosName = JPM.JobPosName;
        //        newobject.JobPosDesc = JPM.JobPosDesc;
        //        newobject.Gender = JPM.Gender;
        //        newobject.CompCode = JPM.CompCode;
        //        newobject.DepCode = JPM.DepCode;
        //        newobject.DesignCode = JPM.DesignCode;
        //        newobject.JobId = JPM.JobId;
        //        newobject.AgeId = JPM.AgeId;
        //        newobject.CatCode = JPM.CatCode;
        //        newobject.IMCode = JPM.IMCode;
        //        newobject.QID = QID;
        //        newobject.LangCode = lang;
        //        newobject.SpecLznID = Splzfld;
        //        newobject.pskillID = pss;
        //        newobject.sskillID = sss;
        //        newobject.ExpId = exp;
        //        newobject.RelvExpId = relvexp;
        //        newobject.CertCode = certcode;
        //        //code for compare IM code between old and new
        //        string[] arrIMold = objjpm.IMCode.Split(',');
        //        string[] arrIMnew = IMCode.Split(',');
        //        bool isSameIM = ArrayCompare(arrIMold, arrIMnew);
        //        if (isSameIM == true)
        //        {
        //            oldobject.IMCode = "";
        //            newobject.IMCode = "";
        //        }
        //        //code for comapre QID betwenn old and new
        //        string[] arrQIDold = objjpm.QID.Split(',');
        //        string[] arrQIDnew = QID.Split(',');
        //        bool isSameQID = ArrayCompare(arrQIDold, arrQIDnew);
        //        if (isSameQID == true)
        //        {
        //            oldobject.QID = "";
        //            newobject.QID = "";
        //        }
        //        //code for compare language betweenn old and new
        //        string[] arrLangold = objjpm.LangCode.Split(',');
        //        string[] arrLangnew = lang.Split(',');
        //        bool isSameLang = ArrayCompare(arrLangold, arrLangnew);
        //        if (isSameLang == true)
        //        {
        //            oldobject.LangCode = "";
        //            newobject.LangCode = "";
        //        }
        //        //code for compare Specialiazation betweenn old and new
        //        string[] arrsplznold = objjpm.SpecLznID.Split(',');
        //        string[] arrsplznnew = Splzfld.Split(',');
        //        bool isSplzfld = ArrayCompare(arrsplznold, arrsplznnew);
        //        if (isSplzfld == true)
        //        {
        //            oldobject.SpecLznID = "";
        //            newobject.SpecLznID = "";
        //        }
        //        //code for compare primary skill sets betweenn old and new
        //        string[] arrPSSold = objjpm.pskillID.Split(',');
        //        string[] arrPSSnew = pss.Split(',');
        //        bool isPSS = ArrayCompare(arrPSSold, arrPSSnew);
        //        if (isPSS == true)
        //        {
        //            oldobject.pskillID = "";
        //            newobject.pskillID = "";
        //        }
        //        //code for compare secondary  skill sets betweenn old and new
        //        string[] arrSSSold = objjpm.pskillID.Split(',');
        //        string[] arrSSSnew = pss.Split(',');
        //        bool isSSS = ArrayCompare(arrSSSold, arrSSSnew);
        //        if (isSSS == true)
        //        {
        //            oldobject.sskillID = "";
        //            newobject.sskillID = "";
        //        }
        //        //code for compare total experiencce betweenn old and new
        //        string[] arrEXPold = objjpm.ExpId.Split(',');
        //        string[] arrEXPnew = exp.Split(',');
        //        bool isEXP = ArrayCompare(arrEXPold, arrEXPnew);
        //        if (isEXP == true)
        //        {
        //            oldobject.ExpId = "";
        //            newobject.ExpId = "";
        //        }
        //        //code for compare rrelevant experiencce betweenn old and new
        //        string[] arrREXPold = objjpm.RelvExpId.Split(',');
        //        string[] arrREXPnew = relvexp.Split(',');
        //        bool isREXP = ArrayCompare(arrREXPold, arrREXPnew);
        //        if (isREXP == true)
        //        {
        //            oldobject.RelvExpId = "";
        //            newobject.RelvExpId = "";
        //        }
        //        //code for compare certificate code   betweenn old and new
        //        string[] arrCertold = objjpm.CertCode.Split(',');
        //        string[] arrCertnew = certcode.Split(',');
        //        bool isCert = ArrayCompare(arrCertold, arrCertnew);
        //        if (isCert == true)
        //        {
        //            oldobject.CertCode = "";
        //            newobject.CertCode = "";
        //        }
        //        AuditLog.CreateAuditTrail(Employee.EMPLOYEEMASTER, Employee.PAYCODE, JPM.JobPosCode, AppConstant.MODIFIED, JPM.LastModifiedBy, JPM.LoginTerminalNameIP, oldobject, newobject);
        //    }
        //    return res;
        //}

        public bool ArrayCompare(string[] arrQIDold, string[] arrQIDnew)
        {
            bool hasDuplicate = false;
            foreach (var numberA in arrQIDold)
            {
                foreach (var numberB in arrQIDnew)
                {
                    if (numberA == numberB)
                    {
                        hasDuplicate = true;
                        break;
                    }
                    else
                    {
                        hasDuplicate = false;
                    }
                }
                if (hasDuplicate == false)
                {
                    break;
                }
            }
            return hasDuplicate;
        }

        //public IEnumerable<JobPosPrimarySkill_list> BindPrimarySkill(string jobId)
        //{

        //    Employee jpm = new Employee();
        //    List<JobPosPrimarySkill_list> lst = new List<JobPosPrimarySkill_list>();
        //    DataTable dt = DataOperation.GetDataTableWithParameter("UspJobPos_PrimarySkillset",
        //                                                            new SqlParameter("JobID", jobId.Trim()));
        //    if (dt.Rows.Count > 0)
        //    {

        //        for (int index = 0; index < dt.Rows.Count; index++)
        //        {
        //            lst.Add(new JobPosPrimarySkill_list
        //            {
        //                pskillID = Convert.ToString(dt.Rows[index]["SkillId"]).Trim(),
        //                pskillName = Convert.ToString(dt.Rows[index]["SkillName"]).Trim(),

        //            });
        //        }

        //    }
        //    return lst;

        //}
        //public IEnumerable<JobPosSecondarySkill_list> BindSecondarySkill(string jobId)
        //{
        //    Employee jpm = new Employee();
        //    List<JobPosSecondarySkill_list> lst = new List<JobPosSecondarySkill_list>();
        //    DataTable dt = DataOperation.GetDataTableWithParameter("UspJobPos_SecondarySkillset",
        //                                                            new SqlParameter("JobID", jobId.Trim()));
        //    if (dt.Rows.Count > 0)
        //    {

        //        for (int index = 0; index < dt.Rows.Count; index++)
        //        {
        //            lst.Add(new JobPosSecondarySkill_list
        //            {
        //                sskillID = Convert.ToString(dt.Rows[index]["SkillId"]).Trim(),
        //                sskillName = Convert.ToString(dt.Rows[index]["SkillName"]).Trim(),

        //            });
        //        }

        //    }
        //    return lst;
        //}

        //public List<JobPosTotalExp_list> BindTotalExp(string jobId)
        //{
        //    Employee jpm = new Employee();
        //    List<JobPosTotalExp_list> lst = new List<JobPosTotalExp_list>();

        //    DataTable dt = DataOperation.GetDataTableWithParameter("UspJobPos_TotalExp",
        //                                                            new SqlParameter("JobID", jobId.Trim()));
        //    if (dt.Rows.Count > 0)
        //    {

        //        for (int index = 0; index < dt.Rows.Count; index++)
        //        {
        //            lst.Add(new JobPosTotalExp_list
        //            {
        //                ExpId = Convert.ToString(dt.Rows[index]["ExpId"]).Trim(),
        //                ExpDesc = Convert.ToString(dt.Rows[index]["ExpDesc"]).Trim(),
        //            });
        //        }

        //    }
        //    return lst;
        //}
        //public List<JobPosRelv_list> BindRelvExp(string jobId)
        //{
        //    Employee jpm = new Employee();
        //    List<JobPosRelv_list> lst = new List<JobPosRelv_list>();

        //    DataTable dt = DataOperation.GetDataTableWithParameter("UspJobPos_RelvExp",
        //                                                            new SqlParameter("JobID", jobId.Trim()));
        //    if (dt.Rows.Count > 0)
        //    {

        //        for (int index = 0; index < dt.Rows.Count; index++)
        //        {
        //            lst.Add(new JobPosRelv_list
        //            {
        //                RelvExpId = Convert.ToString(dt.Rows[index]["RelvExpid"]).Trim(),
        //                RelvExpDesc = Convert.ToString(dt.Rows[index]["RelvExpDesc"]).Trim(),
        //            });
        //        }

        //    }
        //    return lst;
        //}

        //public int DeleteEmployee(Employee JPM)
        //{
        //    int res = -1;
        //    Employee objjpm = GetJobPos(JPM.JobPosCode);
        //    EmployeeClone auditoldobject = new EmployeeClone();
        //    //code for clone copy create for old object
        //    auditoldobject.IsActive = objjpm.IsActive;
        //    auditoldobject.JobPosCode = objjpm.JobPosCode;
        //    auditoldobject.JobPosName = objjpm.JobPosName;
        //    auditoldobject.JobPosDesc = objjpm.JobPosDesc;
        //    auditoldobject.Gender = objjpm.Gender;
        //    auditoldobject.CompCode = objjpm.CompCode;
        //    auditoldobject.DepCode = objjpm.DepCode;
        //    auditoldobject.DesignCode = objjpm.DesignCode;
        //    auditoldobject.JobId = objjpm.JobId;
        //    auditoldobject.AgeId = objjpm.AgeId;
        //    auditoldobject.CatCode = objjpm.CatCode;
        //    auditoldobject.IMCode = objjpm.IMCode;
        //    auditoldobject.QID = objjpm.QID;
        //    auditoldobject.LangCode = objjpm.LangCode;
        //    auditoldobject.SpecLznID = objjpm.SpecLznID;
        //    auditoldobject.pskillID = objjpm.pskillID;
        //    auditoldobject.sskillID = objjpm.sskillID;
        //    auditoldobject.ExpId = objjpm.ExpId;
        //    auditoldobject.RelvExpId = objjpm.RelvExpId;
        //    auditoldobject.CertCode = objjpm.CertCode;
        //    EmployeeClone auditnewobject = new EmployeeClone();
        //    //code for clone copy create for new object
        //    auditnewobject.IsActive = "";
        //    auditnewobject.JobPosCode = "";
        //    auditnewobject.JobPosName = "";
        //    auditnewobject.JobPosDesc = "";
        //    auditnewobject.Gender = "";
        //    auditnewobject.CompCode = "";
        //    auditnewobject.DepCode = "";
        //    auditnewobject.DesignCode = "";
        //    auditnewobject.JobId = "";
        //    auditnewobject.AgeId = "";
        //    auditnewobject.CatCode = "";
        //    auditnewobject.IMCode = "";
        //    auditnewobject.QID = "";
        //    auditnewobject.LangCode = "";
        //    auditnewobject.SpecLznID = "";
        //    auditnewobject.pskillID = "";
        //    auditnewobject.sskillID = "";
        //    auditnewobject.ExpId = "";
        //    auditnewobject.RelvExpId = "";
        //    auditnewobject.CertCode = "";

        //    DataTable dt = DataOperation.GetDataTableWithParameter("Usp_CheckJobPosRefUse", new SqlParameter("JobPosCode", JPM.JobPosCode.Trim()));
        //    if (Convert.ToInt32(dt.Rows[0]["Column1"]) > 0)
        //    {
        //        res = 2;
        //    }
        //    else
        //    {
        //        res = DataOperation.InsUpdDel("Employee_Delete", new SqlParameter("JobPosCode", JPM.JobPosCode.Trim()));
        //        if (res == 1)
        //        {
        //            AuditLog.CreateAuditTrail(Employee.EMPLOYEEMASTER, Employee.PAYCODE, JPM.JobPosCode.Trim(), AppConstant.DELETED, JPM.LastModifiedBy, JPM.LoginTerminalNameIP, auditoldobject, auditnewobject);
        //        }
        //    }
        //    return res;
        //}

        public string GetAutoID(Employee JPM)
        {
            string res = "";
            DataTable dt = DataOperation.GetDataTableWithoutParameter("JobPosGenerateAutoCode");

            res = dt.Rows[0]["AutoID"].ToString();
            return res;

        }

        //To Add new Employee record    
        public int Add(Employee employee)
        {
            employee.sEMPNAME = (String.IsNullOrEmpty(employee.sEMPNAME) ? "" : employee.sEMPNAME);
            employee.sGUARDIANNAME = (String.IsNullOrEmpty(employee.sGUARDIANNAME) ? "" : employee.sGUARDIANNAME);
            //employee.sDateOFBIRTH = (String.IsNullOrEmpty(employee.sDateOFBIRTH) ? "" : employee.sDateOFBIRTH);
            employee.sDateOFJOIN = (String.IsNullOrEmpty(employee.sDateOFJOIN) ? "" : employee.sDateOFJOIN);
            employee.sPRESENTCARDNO = (String.IsNullOrEmpty(employee.sPRESENTCARDNO) ? "" : employee.sPRESENTCARDNO);
            employee.sQUALIFICATION = (String.IsNullOrEmpty(employee.sQUALIFICATION) ? "" : employee.sQUALIFICATION);
            employee.sEXPERIENCE = (String.IsNullOrEmpty(employee.sEXPERIENCE) ? "" : employee.sEXPERIENCE);
            employee.sBLOODGROUP = (String.IsNullOrEmpty(employee.sBLOODGROUP) ? "" : employee.sBLOODGROUP);
            employee.sADDRESS1 = (String.IsNullOrEmpty(employee.sADDRESS1) ? "" : employee.sADDRESS1);
            employee.sPINCODE1 = (String.IsNullOrEmpty(employee.sPINCODE1) ? "" : employee.sPINCODE1);
            employee.sTELEPHONE1 = (String.IsNullOrEmpty(employee.sTELEPHONE1) ? "" : employee.sTELEPHONE1);
            employee.sE_MAIL1 = (String.IsNullOrEmpty(employee.sE_MAIL1) ? "" : employee.sE_MAIL1);
            employee.sADDRESS2 = (String.IsNullOrEmpty(employee.sADDRESS2) ? "" : employee.sADDRESS2);
            employee.sPINCODE2 = (String.IsNullOrEmpty(employee.sPINCODE2) ? "" : employee.sPINCODE2);
            employee.sTELEPHONE2 = (String.IsNullOrEmpty(employee.sTELEPHONE2) ? "" : employee.sTELEPHONE2);
            employee.sPFNO = (String.IsNullOrEmpty(employee.sPFNO) ? "" : employee.sPFNO);
            employee.sPF_NO = (String.IsNullOrEmpty(employee.sPF_NO) ? "" : employee.sPF_NO);
            employee.sESINO = (String.IsNullOrEmpty(employee.sESINO) ? "" : employee.sESINO);
            employee.sAUTHORISEDMACHINE = (String.IsNullOrEmpty(employee.sAUTHORISEDMACHINE) ? "" : employee.sAUTHORISEDMACHINE);
            employee.sEMPTYPE = (String.IsNullOrEmpty(employee.sEMPTYPE) ? "" : employee.sEMPTYPE);
            employee.sEMPPHOTO = (String.IsNullOrEmpty(employee.sEMPPHOTO) ? "" : employee.sEMPPHOTO);
            employee.sEMPSIGNATURE = (String.IsNullOrEmpty(employee.sEMPSIGNATURE) ? "" : employee.sEMPSIGNATURE);
            employee.sBankAcc = (String.IsNullOrEmpty(employee.sBankAcc) ? "" : employee.sBankAcc);
            employee.BankCode = (String.IsNullOrEmpty(employee.BankCode) ? "" : employee.BankCode);
            employee.sHOD_Code = (String.IsNullOrEmpty(employee.sHOD_Code) ? "" : employee.sHOD_Code);
            employee.sGroupCode = (String.IsNullOrEmpty(employee.sGroupCode) ? "" : employee.sGroupCode);
            employee.sSSN = employee.sCOMPANYCODE.ToString().Trim() + "_" + employee.sPAYCODE.ToString().Trim();
            employee.UID = (String.IsNullOrEmpty(employee.UID) ? "" : employee.UID);
            employee.sPANNo = (String.IsNullOrEmpty(employee.sPANNo) ? "" : employee.sPANNo);

            //employee. = (String.IsNullOrEmpty(employee.sEMPNAME) ? "" : employee.sEMPNAME);
            //employee.sSEX = (String.IsNullOrEmpty(employee.sSEX) ? "" : employee.sSEX);
            //employee.sISMARRIED = (String.IsNullOrEmpty(employee.sISMARRIED) ? "" : employee.sISMARRIED);
            //employee.sQUALIFICATION = (String.IsNullOrEmpty(employee.sQUALIFICATION) ? "" : employee.sQUALIFICATION);
            //employee.sEXPERIENCE = (String.IsNullOrEmpty(employee.sEXPERIENCE) ? "" : employee.sEXPERIENCE);
            //employee.sBLOODGROUP = (String.IsNullOrEmpty(employee.sBLOODGROUP) ? "" : employee.sBLOODGROUP);
            //employee.sEMPPHOTO = (String.IsNullOrEmpty(employee.sEMPPHOTO) ? "" : employee.sEMPPHOTO);
            //employee.sEMPSIGNATURE = (String.IsNullOrEmpty(employee.sEMPSIGNATURE) ? "" : employee.sEMPSIGNATURE);
            //employee.sPFNO = (String.IsNullOrEmpty(employee.sPFNO) ? "" : employee.sPFNO);
            //employee.sESINO = (String.IsNullOrEmpty(employee.sESINO) ? "" : employee.sESINO);
            //employee.sEMPNAME = (String.IsNullOrEmpty(employee.sEMPNAME) ? "" : employee.sEMPNAME);
            //employee.sEMPNAME = (String.IsNullOrEmpty(employee.sEMPNAME) ? "" : employee.sEMPNAME);

            int res = DataOperation.InsUpdDel("tblemployee_Add",
                new SqlParameter("ACTIVE", employee.sACTIVE),
                new SqlParameter("PAYCODE", employee.sPAYCODE),
                new SqlParameter("EMPNAME", employee.sEMPNAME),
                new SqlParameter("GUARDIANNAME", employee.sGUARDIANNAME),
                new SqlParameter("DateOFBIRTH", Convert.ToDateTime(employee.sDateOFBIRTH)),
                new SqlParameter("DateOFJOIN", Convert.ToDateTime(employee.sDateOFJOIN)),
                new SqlParameter("PRESENTCARDNO", employee.sPRESENTCARDNO),
                new SqlParameter("SSN", employee.sSSN),//),
                new SqlParameter("COMPANYCODE", employee.sCOMPANYCODE),   //,
                new SqlParameter("DivisionCode", employee.sDivisionCode),
                new SqlParameter("CAT", employee.sCATCode),
                new SqlParameter("SEX", employee.sSEX),
                new SqlParameter("ISMARRIED", employee.sISMARRIED),
                new SqlParameter("QUALIFICATION", employee.sQUALIFICATION),
                new SqlParameter("EXPERIENCE", employee.sEXPERIENCE),
                new SqlParameter("DESIGNATION", employee.sDESIGNATIONCODE),
                new SqlParameter("ADDRESS1", employee.sADDRESS1),
                new SqlParameter("PINCODE1", employee.sPINCODE1),
                new SqlParameter("TELEPHONE1", employee.sTELEPHONE1),
                new SqlParameter("E_MAIL1", employee.sE_MAIL1),
                new SqlParameter("ADDRESS2", employee.sADDRESS2),
                new SqlParameter("PINCODE2", employee.sPINCODE2),
                new SqlParameter("TELEPHONE2", employee.sTELEPHONE2),
                new SqlParameter("BLOODGROUP", employee.sBLOODGROUP),
                new SqlParameter("EMPPHOTO", employee.sEMPPHOTO),
                new SqlParameter("EMPSIGNATURE", employee.sEMPSIGNATURE),
                new SqlParameter("DepartmentCode", employee.sDepartmentCode),
                new SqlParameter("GradeCode", employee.sGradeCode),
                new SqlParameter("BankAcc", employee.sBankAcc),
                new SqlParameter("bankCODE", employee.BankCode),
                new SqlParameter("headid", employee.sHOD_Code),
                new SqlParameter("GroupID", employee.sGroupCode),
                new SqlParameter("UID", employee.UID),
                new SqlParameter("PANNO", employee.sPANNo)

                //new SqlParameter("LastModifiedBy", employee.LastModifiedBy.Trim()
                //)
                //new SqlParameter("BUS", employee.sBUS),
                //new SqlParameter("Leavingdate", employee.sLeavingdate),
                //new SqlParameter("LeavingReason", employee.sLeavingReason),
                //new SqlParameter("VehicleNo", employee.sVehicleNo),
                //new SqlParameter("PFNO", employee.sPFNO),
                //new SqlParameter("PF_NO", employee.sPF_NO),
                //new SqlParameter("ESINO", employee.sESINO),
                //new SqlParameter("EMPTYPE", employee.sEMPTYPE),
                //new SqlParameter("BRANCHCODE", employee.sBRANCHCODE),
                //new SqlParameter("DESPANSARYCODE", employee.sDESPANSARYCODE),
                //new SqlParameter("Email_CC", employee.sEmail_CC),
                //new SqlParameter("LeaveApprovalStages", employee.sLeaveApprovalStages),
                //new SqlParameter("RockWellid", employee.sRockWellid),
                //new SqlParameter("Grade", employee.sGrade),
                //new SqlParameter("headid_2", employee.sheadid_2),
                //new SqlParameter("head_id", employee.shead_id),
                //new SqlParameter("ValidityStartDate", employee.sValidityStartDate),
                //new SqlParameter("ValidityEndDate", employee.sValidityEndDate),

                ////new SqlParameter("DeviceGroupID", employee.sDeviceGroupID),
                ////new SqlParameter("ClientID", employee.sClientID),

                //new SqlParameter("SSN", employee.sPAYCODE.ToString().Trim() + employee.Company.ToString().Trim()),

                );
            if (res == 1)
            {
                int resEmpShiftMat_Insert = DataOperation.InsUpdDel("InsertEmployeeShiftMaster",
                         new SqlParameter("@Paycode", employee.sPAYCODE),
                         new SqlParameter("@CARDNO", employee.sPRESENTCARDNO),
                         new SqlParameter("@SSN", employee.sSSN),
                         new SqlParameter("@GroupID", employee.sGroupCode)
                        );

                int resRoster_Create = DataOperation.InsUpdDel("ProcessCreateDutyRoster",
                            new SqlParameter("PayCode", employee.sPAYCODE),
                            new SqlParameter("CompanyCode", employee.sCOMPANYCODE),
                            new SqlParameter("FromDate", employee.sDateOFJOIN),
                            new SqlParameter("LocationCode", employee.sDivisionCode)
                        );
            }
            return res;
        }

        public int Update(Employee employee)
        {
            employee.sEMPNAME = (String.IsNullOrEmpty(employee.sEMPNAME) ? "" : employee.sEMPNAME);
            employee.sGUARDIANNAME = (String.IsNullOrEmpty(employee.sGUARDIANNAME) ? "" : employee.sGUARDIANNAME);
            //employee.sDateOFBIRTH = (String.IsNullOrEmpty(employee.sDateOFBIRTH) ? "" : employee.sDateOFBIRTH);
            employee.sDateOFJOIN = (String.IsNullOrEmpty(employee.sDateOFJOIN) ? "" : employee.sDateOFJOIN);
            employee.sPRESENTCARDNO = (String.IsNullOrEmpty(employee.sPRESENTCARDNO) ? "" : employee.sPRESENTCARDNO);
            employee.sQUALIFICATION = (String.IsNullOrEmpty(employee.sQUALIFICATION) ? "" : employee.sQUALIFICATION);
            employee.sEXPERIENCE = (String.IsNullOrEmpty(employee.sEXPERIENCE) ? "" : employee.sEXPERIENCE);
            employee.sBLOODGROUP = (String.IsNullOrEmpty(employee.sBLOODGROUP) ? "" : employee.sBLOODGROUP);
            employee.sADDRESS1 = (String.IsNullOrEmpty(employee.sADDRESS1) ? "" : employee.sADDRESS1);
            employee.sPINCODE1 = (String.IsNullOrEmpty(employee.sPINCODE1) ? "" : employee.sPINCODE1);
            employee.sTELEPHONE1 = (String.IsNullOrEmpty(employee.sTELEPHONE1) ? "" : employee.sTELEPHONE1);
            employee.sE_MAIL1 = (String.IsNullOrEmpty(employee.sE_MAIL1) ? "" : employee.sE_MAIL1);
            employee.sADDRESS2 = (String.IsNullOrEmpty(employee.sADDRESS2) ? "" : employee.sADDRESS2);
            employee.sPINCODE2 = (String.IsNullOrEmpty(employee.sPINCODE2) ? "" : employee.sPINCODE2);
            employee.sTELEPHONE2 = (String.IsNullOrEmpty(employee.sTELEPHONE2) ? "" : employee.sTELEPHONE2);
            employee.sPFNO = (String.IsNullOrEmpty(employee.sPFNO) ? "" : employee.sPFNO);
            employee.sPF_NO = (String.IsNullOrEmpty(employee.sPF_NO) ? "" : employee.sPF_NO);
            employee.sESINO = (String.IsNullOrEmpty(employee.sESINO) ? "" : employee.sESINO);
            employee.sAUTHORISEDMACHINE = (String.IsNullOrEmpty(employee.sAUTHORISEDMACHINE) ? "" : employee.sAUTHORISEDMACHINE);
            employee.sEMPTYPE = (String.IsNullOrEmpty(employee.sEMPTYPE) ? "" : employee.sEMPTYPE);
            employee.sEMPPHOTO = (String.IsNullOrEmpty(employee.sEMPPHOTO) ? "" : employee.sEMPPHOTO);
            employee.sEMPSIGNATURE = (String.IsNullOrEmpty(employee.sEMPSIGNATURE) ? "" : employee.sEMPSIGNATURE);
            employee.sBankAcc = (String.IsNullOrEmpty(employee.sBankAcc) ? "" : employee.sBankAcc);
            employee.BankCode = (String.IsNullOrEmpty(employee.BankCode) ? "" : employee.BankCode);
            employee.sHOD_Code = (String.IsNullOrEmpty(employee.sHOD_Code) ? "" : employee.sHOD_Code);
            employee.sGroupCode = (String.IsNullOrEmpty(employee.sGroupCode) ? "" : employee.sGroupCode);
            employee.sSSN = employee.sCOMPANYCODE.ToString().Trim() + "_" + employee.sPAYCODE.ToString().Trim();
            employee.UID = (String.IsNullOrEmpty(employee.UID) ? "" : employee.UID);
            employee.sPANNo = (String.IsNullOrEmpty(employee.sPANNo) ? "" : employee.sPANNo);

            //employee. = (String.IsNullOrEmpty(employee.sEMPNAME) ? "" : employee.sEMPNAME);
            //employee.sSEX = (String.IsNullOrEmpty(employee.sSEX) ? "" : employee.sSEX);
            //employee.sISMARRIED = (String.IsNullOrEmpty(employee.sISMARRIED) ? "" : employee.sISMARRIED);
            //employee.sQUALIFICATION = (String.IsNullOrEmpty(employee.sQUALIFICATION) ? "" : employee.sQUALIFICATION);
            //employee.sEXPERIENCE = (String.IsNullOrEmpty(employee.sEXPERIENCE) ? "" : employee.sEXPERIENCE);
            //employee.sBLOODGROUP = (String.IsNullOrEmpty(employee.sBLOODGROUP) ? "" : employee.sBLOODGROUP);
            //employee.sEMPPHOTO = (String.IsNullOrEmpty(employee.sEMPPHOTO) ? "" : employee.sEMPPHOTO);
            //employee.sEMPSIGNATURE = (String.IsNullOrEmpty(employee.sEMPSIGNATURE) ? "" : employee.sEMPSIGNATURE);
            //employee.sPFNO = (String.IsNullOrEmpty(employee.sPFNO) ? "" : employee.sPFNO);
            //employee.sESINO = (String.IsNullOrEmpty(employee.sESINO) ? "" : employee.sESINO);
            //employee.sEMPNAME = (String.IsNullOrEmpty(employee.sEMPNAME) ? "" : employee.sEMPNAME);
            //employee.sEMPNAME = (String.IsNullOrEmpty(employee.sEMPNAME) ? "" : employee.sEMPNAME);

            int res = DataOperation.InsUpdDel("tblemployee_Upd",
                new SqlParameter("ACTIVE", employee.sACTIVE),
                //new SqlParameter("PAYCODE", employee.sPAYCODE),
                new SqlParameter("EMPNAME", employee.sEMPNAME),
                new SqlParameter("GUARDIANNAME", employee.sGUARDIANNAME),
                new SqlParameter("DateOFBIRTH", Convert.ToDateTime(employee.sDateOFBIRTH)),
                new SqlParameter("DateOFJOIN", Convert.ToDateTime(employee.sDateOFJOIN)),
                //new SqlParameter("PRESENTCARDNO", employee.sPRESENTCARDNO),
                new SqlParameter("SSN", employee.sSSN),//),
                new SqlParameter("COMPANYCODE", employee.sCOMPANYCODE),   //,
                new SqlParameter("DivisionCode", employee.sDivisionCode),
                new SqlParameter("CAT", employee.sCATCode),
                new SqlParameter("SEX", employee.sSEX),
                new SqlParameter("ISMARRIED", employee.sISMARRIED),
                new SqlParameter("QUALIFICATION", employee.sQUALIFICATION),
                new SqlParameter("EXPERIENCE", employee.sEXPERIENCE),
                new SqlParameter("DESIGNATION", employee.sDESIGNATIONCODE),
                new SqlParameter("ADDRESS1", employee.sADDRESS1),
                new SqlParameter("PINCODE1", employee.sPINCODE1),
                new SqlParameter("TELEPHONE1", employee.sTELEPHONE1),
                new SqlParameter("E_MAIL1", employee.sE_MAIL1),
                new SqlParameter("ADDRESS2", employee.sADDRESS2),
                new SqlParameter("PINCODE2", employee.sPINCODE2),
                new SqlParameter("TELEPHONE2", employee.sTELEPHONE2),
                new SqlParameter("BLOODGROUP", employee.sBLOODGROUP),
                new SqlParameter("EMPPHOTO", employee.sEMPPHOTO),
                new SqlParameter("EMPSIGNATURE", employee.sEMPSIGNATURE),
                new SqlParameter("DepartmentCode", employee.sDepartmentCode),
                new SqlParameter("GradeCode", employee.sGradeCode),
                new SqlParameter("BankAcc", employee.sBankAcc),
                new SqlParameter("bankCODE", employee.BankCode),
                new SqlParameter("headid", employee.sHOD_Code),
                new SqlParameter("GroupID", employee.sGroupCode),
                new SqlParameter("UID", employee.UID),
                new SqlParameter("PANNO", employee.sPANNo)

                //new SqlParameter("LastModifiedBy", employee.LastModifiedBy.Trim()
                //)
                //new SqlParameter("BUS", employee.sBUS),
                //new SqlParameter("Leavingdate", employee.sLeavingdate),
                //new SqlParameter("LeavingReason", employee.sLeavingReason),
                //new SqlParameter("VehicleNo", employee.sVehicleNo),
                //new SqlParameter("PFNO", employee.sPFNO),
                //new SqlParameter("PF_NO", employee.sPF_NO),
                //new SqlParameter("ESINO", employee.sESINO),
                //new SqlParameter("EMPTYPE", employee.sEMPTYPE),
                //new SqlParameter("BRANCHCODE", employee.sBRANCHCODE),
                //new SqlParameter("DESPANSARYCODE", employee.sDESPANSARYCODE),
                //new SqlParameter("Email_CC", employee.sEmail_CC),
                //new SqlParameter("LeaveApprovalStages", employee.sLeaveApprovalStages),
                //new SqlParameter("RockWellid", employee.sRockWellid),
                //new SqlParameter("Grade", employee.sGrade),
                //new SqlParameter("headid_2", employee.sheadid_2),
                //new SqlParameter("head_id", employee.shead_id),
                //new SqlParameter("ValidityStartDate", employee.sValidityStartDate),
                //new SqlParameter("ValidityEndDate", employee.sValidityEndDate),

                ////new SqlParameter("DeviceGroupID", employee.sDeviceGroupID),
                ////new SqlParameter("ClientID", employee.sClientID),

                //new SqlParameter("SSN", employee.sPAYCODE.ToString().Trim() + employee.Company.ToString().Trim()),

                );
            if (res == 1)
            {
                //int resEmpShiftMat_Insert = DataOperation.InsUpdDel("InsertEmployeeShiftMaster",
                //         new SqlParameter("@Paycode", employee.sPAYCODE),
                //         new SqlParameter("@CARDNO", employee.sPRESENTCARDNO),
                //         new SqlParameter("@SSN", employee.sSSN),
                //         new SqlParameter("@GroupID", employee.sGroupCode)
                //        );

                //int resRoster_Create = DataOperation.InsUpdDel("ProcessCreateDutyRoster",
                //            new SqlParameter("PayCode", employee.sPAYCODE),
                //            new SqlParameter("CompanyCode", employee.sCOMPANYCODE),
                //            new SqlParameter("FromDate", employee.sDateOFJOIN),
                //            new SqlParameter("LocationCode", employee.sDivisionCode)
                //        );
            }
            return res;
        }

        public int GetEmployeeCode(string Code)
        {

            int res = 0;
            DataTable dt = DataOperation.GetDataTableWithParameter("TblEmployeeMaster_CheckIsCode", new SqlParameter("paycode", Code.Trim()));
            if (dt.Rows.Count > 0)
            {
                for (int index = 0; index < dt.Rows.Count; index++)
                {
                    res = Convert.ToInt32(dt.Rows[index]["cnt"].ToString());
                }
            }
            return res;
        }

        public int DeleteEmployee(string SSN)
        {
            //string oldCode = Code;
            Employee oldobject = GetEmployeeData(SSN.Trim());
            ShiftCloneModel objAuditold = new ShiftCloneModel();
            ShiftCloneModel objAuditnew = new ShiftCloneModel();
            int res = DataOperation.InsUpdDel("TblEmployeeMaster_Del", new SqlParameter("SSN", SSN.Trim()));
            return res;
        }

        public Employee GetEmployeeData(string SSN)
        {
            Employee Emp = new Employee();
            DataTable dt = DataOperation.GetDataTableWithParameter("Employee_SingleList", new SqlParameter("SSN", SSN.Trim()));
            if (dt.Rows.Count > 0)
            {
                for (int index = 0; index < dt.Rows.Count; index++)
                {
                    //Emp.sSSN = dt.Rows[index]["SHIFT"].ToString().Trim();
                    //Emp.sPAYCODE = dt.Rows[index]["ShiftDiscription"].ToString().Trim();

                    Emp.sACTIVE = dt.Rows[index]["ACTIVE"].ToString().Trim();
                    Emp.sPAYCODE = dt.Rows[index]["PAYCODE"].ToString().Trim();
                    Emp.sEMPNAME = dt.Rows[index]["EMPNAME"].ToString().Trim();
                    Emp.sGUARDIANNAME = dt.Rows[index]["GUARDIANNAME"].ToString().Trim();
                    Emp.sDateOFBIRTH = dt.Rows[index]["DateOFBIRTH"].ToString().Trim();
                    Emp.sDateOFJOIN = dt.Rows[index]["DateOFJOIN"].ToString().Trim();
                    Emp.sPRESENTCARDNO = dt.Rows[index]["PRESENTCARDNO"].ToString().Trim();
                    Emp.sCOMPANYCODE = dt.Rows[index]["COMPANYCODE"].ToString().Trim();
                    Emp.sDivisionCode = dt.Rows[index]["DivisionCode"].ToString().Trim();
                    Emp.sCATCode = dt.Rows[index]["CAT"].ToString().Trim();
                    Emp.sSEX = dt.Rows[index]["SEX"].ToString().Trim();
                    Emp.sISMARRIED = dt.Rows[index]["ISMARRIED"].ToString().Trim();
                    //em.sBUS = dt.Rows[index]["BUS"].ToString();
                    Emp.sQUALIFICATION = dt.Rows[index]["QUALIFICATION"].ToString().Trim();
                    Emp.sEXPERIENCE = dt.Rows[index]["EXPERIENCE"].ToString().Trim();
                    Emp.DesignationCode = dt.Rows[index]["DESIGNATION"].ToString().Trim();

                    Emp.sADDRESS1 = dt.Rows[index]["ADDRESS1"].ToString().Trim().Trim();
                    Emp.sPINCODE1 = dt.Rows[index]["PINCODE1"].ToString().Trim();
                    Emp.sTELEPHONE1 = dt.Rows[index]["TELEPHONE1"].ToString().Trim();
                    Emp.sE_MAIL1 = dt.Rows[index]["E_MAIL1"].ToString().Trim();
                    Emp.sADDRESS2 = dt.Rows[index]["ADDRESS2"].ToString().Trim();
                    Emp.sPINCODE2 = dt.Rows[index]["PINCODE2"].ToString().Trim();
                    Emp.sTELEPHONE2 = dt.Rows[index]["TELEPHONE2"].ToString().Trim();
                    Emp.sBLOODGROUP = dt.Rows[index]["BLOODGROUP"].ToString().Trim();
                    Emp.sEMPPHOTO = dt.Rows[index]["EMPPHOTO"].ToString().Trim();
                    Emp.sEMPSIGNATURE = dt.Rows[index]["EMPSIGNATURE"].ToString().Trim();
                    Emp.sDepartmentCode = dt.Rows[index]["DepartmentCode"].ToString().Trim();
                    Emp.sGradeCode = dt.Rows[index]["GradeCode"].ToString().Trim();
                    Emp.sLeavingdate = dt.Rows[index]["Leavingdate"].ToString().Trim();
                    Emp.sLeavingReason = dt.Rows[index]["LeavingReason"].ToString().Trim();
                    Emp.sVehicleNo = dt.Rows[index]["VehicleNo"].ToString().Trim();
                    Emp.sPFNO = dt.Rows[index]["PFNO"].ToString().Trim();
                    Emp.sPF_NO = dt.Rows[index]["PF_NO"].ToString().Trim();
                    Emp.sESINO = dt.Rows[index]["ESINO"].ToString().Trim();
                    Emp.sEMPTYPE = dt.Rows[index]["EMPTYPE"].ToString().Trim();
                    Emp.sBankAcc = dt.Rows[index]["BankAcc"].ToString().Trim();
                    Emp.BankCode = dt.Rows[index]["bankCODE"].ToString().Trim();
                    Emp.sBRANCHCODE = dt.Rows[index]["BRANCHCODE"].ToString().Trim();
                    Emp.sDESPANSARYCODE = dt.Rows[index]["DESPANSARYCODE"].ToString().Trim();
                    Emp.sheadid = dt.Rows[index]["headid"].ToString().Trim();
                    Emp.sEmail_CC = dt.Rows[index]["Email_CC"].ToString().Trim();
                    Emp.sLeaveApprovalStages = dt.Rows[index]["LeaveApprovalStages"].ToString().Trim();
                    Emp.sRockWellid = dt.Rows[index]["RockWellid"].ToString().Trim();
                    Emp.sGradeCode = dt.Rows[index]["GradeCode"].ToString().Trim();
                    //em.sheadid_2 = dt.Rows[index]["headid_2"].ToString().Trim();
                    //em.shead_id = dt.Rows[index]["head_id"].ToString().Trim();
                    //em.sValidityStartDate = dt.Rows[index]["ValidityStartDate"].ToString().Trim();
                    //em.sValidityEndDate = dt.Rows[index]["ValidityEndDate"].ToString().Trim();
                    Emp.sSSN = dt.Rows[index]["SSN"].ToString().Trim();
                    Emp.sDeviceGroupID = dt.Rows[index]["DeviceGroupID"].ToString().Trim();
                    Emp.sClientID = dt.Rows[index]["ClientID"].ToString().Trim();
                    Emp.sGroupID = dt.Rows[index]["GroupID"].ToString().Trim();
                    Emp.UID = dt.Rows[index]["UID"].ToString().Trim();
                    Emp.sPANNo = dt.Rows[index]["PANNO"].ToString().Trim();

                    //lstShift.Add(Emp);
                }
            }
            return Emp;
        }
    }
}