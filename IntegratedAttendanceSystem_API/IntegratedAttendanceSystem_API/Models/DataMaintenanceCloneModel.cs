﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IntegratedAttendanceSystem_API.Models
{
    public class DataMaintenanceCloneModel
    {
        public List<EmployeeCode_list> EmployeeCodeList { get; set; }
        public string EnrollmentCode { get; set; }
        public string Company { get; set; }
        public string Department { get; set; }
        public string Category { get; set; }
        public string EmpCode { get; set; }
        public string SSN { get; set; }
        public string EmpName { get; set; }
        public string Status { get; set; }
        public DateTime DateOffice { get; set; }
        public string In1 { get; set; }
        public string Out1 { get; set; }
        public string In2 { get; set; }
        public string Out2 { get; set; }
        public string PunchTime { get; set; }
        public string HoursWorked { get; set; }
        public string LATEARRIVAL { get; set; }
        public string EARLYDEPARTURE { get; set; }
        public string LastModifiedBy { get; set; }
        public string LoginTerminalNameIP { get; set; }

        public class EmployeeCode_list
        {
            public string EmployeeCode { get; set; }
            public string EmployeeName { get; set; }
        }
    }
}