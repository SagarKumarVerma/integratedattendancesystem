﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace IntegratedAttendanceSystem_API.Models
{
    public class DepartmentModel
    {
        #region ///****Use for Department Master constant*******/////
        public const string DEPARTMENTMASTER = "Department Master";
        public const string DEPARTMENTCODE = "DepartmentCode";
        #endregion
        public string IsActive { get; set; }
        public string DepartmentCode { get; set; }
        public string DepartmentName { get; set; }
        public string CompanyCode { get; set; }
        //public string DepartmentHODID { get; set; }
        //public string HODEmailID { get; set; }
        public DateTime CreatedDate { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime LastModifiedDate { get; set; }
        //#
        public string LoginTerminalNameIP { get; set; }
    }
}