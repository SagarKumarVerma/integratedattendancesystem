﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;
using IntegratedAttendanceSystem_API.DataAccess;


namespace IntegratedAttendanceSystem_API.Models
{
    public class RosterAccessModel
    {
        public int IsCheckIsNumberExpID()
        {
            int res = 0;
            DataTable dt = DataOperation.GetDataTableWithoutParameter("TblCompanyMaster_CheckIsNumberExpID");
            if (dt.Rows.Count > 0)
            {
                for (int index = 0; index < dt.Rows.Count; index++)
                {
                    res = Convert.ToInt32(dt.Rows[index]["cnt"].ToString());
                }
            }
            return res;
        }
        //To View all Company details    
        public IEnumerable<CompanyModel> GetAllCompany()
        {
            DateTime date = System.DateTime.Now;
            List<CompanyModel> lstCompany = new List<CompanyModel>();
            DataTable dt = DataOperation.GetDataTableWithoutParameter("TblCompanyMaster_AllRec");
            if (dt.Rows.Count > 0)
            {
                for (int index = 0; index < dt.Rows.Count; index++)
                {
                    CompanyModel Company = new CompanyModel();
                    Company.IsActive = dt.Rows[index]["IsActive"].ToString();
                    Company.Code = dt.Rows[index]["CompanyCode"].ToString();
                    Company.Name = dt.Rows[index]["CompanyName"].ToString();
                    Company.ShortName = dt.Rows[index]["ShortName"].ToString();
                    Company.IndustryNature = dt.Rows[index]["IndustryNature"].ToString();
                    Company.Address = dt.Rows[index]["CompanyAddress"].ToString();
                    Company.PhoneNo = dt.Rows[index]["PhoneNo"].ToString();
                    Company.EmailId = dt.Rows[index]["EmailId"].ToString();
                    Company.RegistationNo = dt.Rows[index]["RegistationNo"].ToString();
                    Company.PANNo = dt.Rows[index]["PANNo"].ToString();
                    Company.TANNo = dt.Rows[index]["TANNo"].ToString();
                    Company.PFNo = dt.Rows[index]["PFNo"].ToString();
                    Company.LCNo = dt.Rows[index]["LCNo"].ToString();
                    Company.CreatedDate = Convert.ToDateTime(dt.Rows[index]["CreatedDate"].ToString());
                    Company.LastModifiedBy = dt.Rows[index]["LastModifiedBy"].ToString();
                    Company.LastModifiedDate = Convert.ToDateTime(dt.Rows[index]["LastModifiedDate"].ToString());
                    lstCompany.Add(Company);
                }
            }
            return lstCompany;
        }

        //To Creater tblTimeregister    
        public int CreateTable(RosterModel C_Roster)
        {
            int tableCreate = DataOperation.CreateTables("tablecreation");
            //if (tableCreate == 1)
            //{
            //    CompanyModel oldobject = GetCompanyData(Roster.Code);
            //    AuditLog.CreateAuditTrail(CompanyModel.COMPANYMASTER, CompanyModel.COMPANYCODE, Company.Code, AppConstant.CREATED, Company.LastModifiedBy, Company.LoginTerminalNameIP, oldobject, Company);
            //}
            return tableCreate;
        }


        public int CreateRoster(RosterModel Roster)
        {
            int res = 0;
            Roster.sSelectionMode = (String.IsNullOrEmpty(Roster.sSelectionMode) ? "" : Roster.sSelectionMode);
            //ster.sFromDate = Roster.sFromDate;

            if (Roster.sSelectionMode == "A")
            {
                Roster.sEmployeeCode = "";
                Roster.sLocationCode = "";

                string[] sCompanyCodeList = Roster.sCompanyCodeGlobel.Split(',');

                if (sCompanyCodeList.Length >= 0)
                {
                    for (int k = 0; k < sCompanyCodeList.Length; k++)
                    {
                        res = DataOperation.InsUpdDel("ProcessCreateDutyRoster",
                            new SqlParameter("PayCode", Roster.sEmployeeCode),
                            new SqlParameter("CompanyCode", Roster.sCompanyCodeGlobel),
                            new SqlParameter("FromDate", Roster.sFromDate),
                            new SqlParameter("LocationCode", Roster.sLocationCode)
                            );
                    }
                }
                else
                {
                    Roster.sCompanyCode = Roster.sCompanyCodeGlobel;
                    res = DataOperation.InsUpdDel("ProcessCreateDutyRoster",
                            new SqlParameter("PayCode", Roster.sEmployeeCode),
                            new SqlParameter("CompanyCode", Roster.sCompanyCodeGlobel),
                            new SqlParameter("FromDate", Roster.sProcessDate),
                            new SqlParameter("LocationCode", Roster.sLocationCode)
                            );
                }
                //DataTable dt = DataOperation.0GetDataTableWithoutParameter("select LTRIM(RTRIM(PAYCODE))PAYCODE,LTRIM(RTRIM(COMPANYCODE))COMPANYCODE,LTRIM(RTRIM(DivisionCode))DivisionCode from tblemployee");
                //if (dt.Rows.Count > 0)
                //{
                //    for (int index = 0; index < dt.Rows.Count; index++)
                //    {
                //        Roster.sEmployeeCode = dt.Rows[index]["PAYCODE"].ToString();
                //        Roster.sEmployeeCode = dt.Rows[index]["COMPANYCODE"].ToString();
                //        Roster.sLocationCode = dt.Rows[index]["DivisionCode"].ToString();
                //    }
                //}
            }
            else
            {
                Roster.sEmployeeCode = (String.IsNullOrEmpty(Roster.sEmployeeCode) ? "" : Roster.sEmployeeCode);
                Roster.sCompanyCode = (String.IsNullOrEmpty(Roster.sCompanyCode) ? "" : Roster.sCompanyCode);
                Roster.sLocationCode = (String.IsNullOrEmpty(Roster.sLocationCode) ? "" : Roster.sLocationCode);

                res = DataOperation.InsUpdDel("ProcessCreateDutyRoster",
                new SqlParameter("PayCode", Roster.sEmployeeCode),
                new SqlParameter("CompanyCode", Roster.sCompanyCodeGlobel),
                new SqlParameter("FromDate", Roster.sProcessDate),
                new SqlParameter("LocationCode", Roster.sLocationCode)
                );
            }

            if (res == 1)
            {
                CompanyModel oldobject = GetCompanyData(Roster.sEmployeeCode);
                AuditLog.CreateAuditTrail(RosterModel.ROSTER, Roster.sCompanyCode, Roster.sEmployeeCode, AppConstant.CREATED, Roster.LastModifiedBy, Roster.LoginTerminalNameIP, oldobject, Roster);
            }
            return res;
        }
        //To Update the records of a particluar  Company  
        public int UpdateRoster(RosterModel U_Roster)
        {
            int res = 0;
            U_Roster.sSelectionMode = (String.IsNullOrEmpty(U_Roster.sSelectionMode) ? "" : U_Roster.sSelectionMode);
            //ster.sFromDate = Roster.sFromDate;

            if (U_Roster.sSelectionMode == "A")
            {
                U_Roster.sEmployeeCode = "";
                U_Roster.sLocationCode = "";
                //string[] sCompanyCodeList = sCompanyCodeList;// = U_Roster.sCompanyCodeGlobel;
                //String[] stringarr = new stringarr;

                //if (U_Roster.sCompanyCodeGlobel != "")
                string[] sCompanyCodeList = U_Roster.sCompanyCodeGlobel.Split(',');

                if (sCompanyCodeList.Length > 1)
                {
                    sCompanyCodeList = U_Roster.sCompanyCodeGlobel.Split(',');
                    for (int k = 0; k < sCompanyCodeList.Length; k++)
                    {
                        res = DataOperation.InsUpdDel("ProcessDutyRosterForUpdate",
                            new SqlParameter("PayCode", U_Roster.sEmployeeCode),
                            new SqlParameter("CompanyCode", U_Roster.sCompanyCodeGlobel),
                            new SqlParameter("FromDate", U_Roster.sFromDate),
                            new SqlParameter("LocationCode", U_Roster.sLocationCode)
                            );
                    }
                }
                else
                {
                    U_Roster.sCompanyCode = U_Roster.sCompanyCodeGlobel;
                    res = DataOperation.InsUpdDel("ProcessDutyRosterForUpdate",
                            new SqlParameter("PayCode", U_Roster.sEmployeeCode),
                            new SqlParameter("CompanyCode", U_Roster.sCompanyCodeGlobel),
                            new SqlParameter("FromDate", U_Roster.sProcessDate),
                            new SqlParameter("LocationCode", U_Roster.sLocationCode)
                            );
                }
                //DataTable dt = DataOperation.0GetDataTableWithoutParameter("select LTRIM(RTRIM(PAYCODE))PAYCODE,LTRIM(RTRIM(COMPANYCODE))COMPANYCODE,LTRIM(RTRIM(DivisionCode))DivisionCode from tblemployee");
                //if (dt.Rows.Count > 0)
                //{
                //    for (int index = 0; index < dt.Rows.Count; index++)
                //    {
                //        Roster.sEmployeeCode = dt.Rows[index]["PAYCODE"].ToString();
                //        Roster.sEmployeeCode = dt.Rows[index]["COMPANYCODE"].ToString();
                //        Roster.sLocationCode = dt.Rows[index]["DivisionCode"].ToString();
                //    }
                //}
            }
            else
            {
                U_Roster.sEmployeeCode = (String.IsNullOrEmpty(U_Roster.sEmployeeCode) ? "" : U_Roster.sEmployeeCode);
                U_Roster.sCompanyCode = (String.IsNullOrEmpty(U_Roster.sCompanyCode) ? "" : U_Roster.sCompanyCode);
                U_Roster.sLocationCode = (String.IsNullOrEmpty(U_Roster.sLocationCode) ? "" : U_Roster.sLocationCode);

                res = DataOperation.InsUpdDel("[ProcessDutyRosterForUpdate]",
                new SqlParameter("PayCode", U_Roster.sEmployeeCode),
                new SqlParameter("CompanyCode", U_Roster.sCompanyCodeGlobel),
                new SqlParameter("FromDate", U_Roster.sProcessDate),
                new SqlParameter("LocationCode", U_Roster.sLocationCode)
                );
            }

            if (res == 1)
            {
                CompanyModel oldobject = GetCompanyData(U_Roster.sEmployeeCode);
                AuditLog.CreateAuditTrail(CompanyModel.COMPANYMASTER, CompanyModel.COMPANYCODE, U_Roster.sEmployeeCode, AppConstant.CREATED, U_Roster.LastModifiedBy, U_Roster.LoginTerminalNameIP, oldobject, U_Roster);
            }
            return res;
        }
        //Get the details of a particular  Company Data  
        public CompanyModel GetCompanyData(string Code)
        {
            CompanyModel Company = new CompanyModel();
            DataTable dt = DataOperation.GetDataTableWithParameter("TblCompanyMaster_GetData", new SqlParameter("CompanyCode", Code.Trim()));
            if (dt.Rows.Count > 0)
            {
                for (int index = 0; index < dt.Rows.Count; index++)
                {
                    if (dt.Rows[index]["IsActive"].ToString() == "Y")
                    {
                        Company.IsActive = "True";
                    }
                    else
                    {
                        Company.IsActive = "False";
                    }
                    Company.Code = dt.Rows[index]["CompanyCode"].ToString();
                    Company.Name = dt.Rows[index]["CompanyName"].ToString();
                    Company.ShortName = dt.Rows[index]["ShortName"].ToString();
                    Company.IndustryNature = dt.Rows[index]["IndustryNature"].ToString();
                    Company.Address = dt.Rows[index]["CompanyAddress"].ToString();
                    Company.PhoneNo = dt.Rows[index]["PhoneNo"].ToString();
                    Company.EmailId = dt.Rows[index]["EmailId"].ToString();
                    Company.RegistationNo = dt.Rows[index]["RegistationNo"].ToString();
                    Company.PANNo = dt.Rows[index]["PANNo"].ToString();
                    Company.TANNo = dt.Rows[index]["TANNo"].ToString();
                    Company.PFNo = dt.Rows[index]["PFNo"].ToString();
                    Company.LCNo = dt.Rows[index]["LCNo"].ToString();
                    Company.CreatedDate = Convert.ToDateTime(dt.Rows[index]["CreatedDate"].ToString());
                    Company.LastModifiedBy = dt.Rows[index]["LastModifiedBy"].ToString();
                    Company.LastModifiedDate = Convert.ToDateTime(dt.Rows[index]["LastModifiedDate"].ToString());
                }
            }
            return Company;
        }
        //Get the details of a particular  Company Code  
        public int GetCompanyCode(string Code)
        {

            int res = 0;
            DataTable dt = DataOperation.GetDataTableWithParameter("TblCompanyMaster_CheckIsCode", new SqlParameter("CompanyCode", Code.Trim()));
            if (dt.Rows.Count > 0)
            {
                for (int index = 0; index < dt.Rows.Count; index++)
                {
                    res = Convert.ToInt32(dt.Rows[index]["cnt"].ToString());
                }
            }
            return res;
        }
        //To Delete the record on a particular Copmpany  
        public int DeleteCompanyold(string Code)
        {
            string oldCode = Code;
            CompanyModel oldobject = GetCompanyData(oldCode.Trim());
            CompanyCloneModel objAuditold = new CompanyCloneModel();
            CompanyCloneModel objAuditnew = new CompanyCloneModel();
            int res = DataOperation.InsUpdDel("TblCompanyMaster_Del", new SqlParameter("CompanyCode", Code.Trim()));
            return res;
        }
        public int Delete(CompanyModel Company)
        {
            int res = -1;
            CompanyCloneModel objAuditold = new CompanyCloneModel();
            CompanyCloneModel objAuditnew = new CompanyCloneModel();
            CompanyModel objgetexpmast = GetCompanyData(Company.Code.Trim());
            //code for clone copy create for old object
            objAuditold.IsActive = objgetexpmast.IsActive;
            objAuditold.Code = objgetexpmast.Code;
            objAuditold.Name = objgetexpmast.Name;
            objAuditold.ShortName = objgetexpmast.ShortName;
            objAuditold.IndustryNature = objgetexpmast.IndustryNature;
            objAuditold.Address = objgetexpmast.Address;
            objAuditold.PhoneNo = objgetexpmast.PhoneNo;
            objAuditold.EmailId = objgetexpmast.EmailId;
            objAuditold.RegistationNo = objgetexpmast.RegistationNo;
            objAuditold.PANNo = objgetexpmast.PANNo;
            objAuditold.TANNo = objgetexpmast.TANNo;
            objAuditold.PFNo = objgetexpmast.PFNo;
            objAuditold.LCNo = objgetexpmast.LCNo;
            //code for clone copy create for new  object
            objAuditnew.IsActive = "";
            objAuditnew.Code = "";
            objAuditnew.Name = "";
            objAuditnew.ShortName = "";
            objAuditnew.IndustryNature = "";
            objAuditnew.Address = "";
            objAuditnew.PhoneNo = "";
            objAuditnew.EmailId = "";
            objAuditnew.RegistationNo = "";
            objAuditnew.PANNo = "";
            objAuditnew.TANNo = "";
            objAuditnew.PFNo = "";
            objAuditnew.LCNo = "";

            res = DataOperation.InsUpdDel("TblCompanyMaster_Del", new SqlParameter("CompanyCode", Company.Code.Trim()));
            if (res == 1)
            {
                AuditLog.CreateAuditTrail(CompanyModel.COMPANYMASTER, CompanyModel.COMPANYCODE, Company.Code.Trim(), AppConstant.DELETED, Company.LastModifiedBy, Company.LoginTerminalNameIP, objAuditold, objAuditnew);
            }
            // }
            return res;
        }
    }
}