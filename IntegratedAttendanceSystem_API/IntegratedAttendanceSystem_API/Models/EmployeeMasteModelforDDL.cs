﻿using IntegratedAttendanceSystem_API.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IntegratedAttendanceSystem_API.Models
{
    public class EmployeeMasteModelforDDL
    {
        public List<Employee_list> GetEmployeeList()
        {
            List<Employee_list> Employee = new List<Employee_list>();
            DataTable dt = DataOperation.GetDataTableWithoutParameter("TblEmployeeMaster_SelectAll");
            if (dt.Rows.Count > 0)
            {
                for (int index = 0; index < dt.Rows.Count; index++)
                {
                    Employee.Add(new Employee_list
                    {
                        EmployeeCode = Convert.ToString(dt.Rows[index]["EmployeeCode"]).Trim(),
                        EmployeeName = Convert.ToString(dt.Rows[index]["EmployeeName"]).Trim(),
                    });
                }
            }
            return Employee;
        }
    }
}